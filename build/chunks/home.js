require("source-map-support").install();
exports.ids = [0];
exports.modules = {

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/node_modules/normalize.css/normalize.css"],"names":[],"mappings":"AAAA,4EAA4E;AAC5E;gFACgF;AAChF;;;;GAIG;AACH;EACE,kBAAkB;EAClB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,+BAA+B;EAC/B,OAAO,EAAE;AACX;gFACgF;AAChF;;GAEG;AACH;EACE,UAAU,EAAE;AACd;;GAEG;AACH;;;;;;EAME,eAAe,EAAE;AACnB;;;GAGG;AACH;EACE,eAAe;EACf,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;EAGE,OAAO;EACP,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;;GAGG;AACH;EACE,gCAAgC;UACxB,wBAAwB;EAChC,OAAO;EACP,UAAU;EACV,OAAO;EACP,kBAAkB;EAClB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,sCAAsC;EACtC,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,oBAAoB;EACpB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,0CAA0C;UAClC,kCAAkC;EAC1C,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,qBAAqB,EAAE;AACzB;;GAEG;AACH;;EAEE,oBAAoB,EAAE;AACxB;;;GAGG;AACH;;;EAGE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,uBAAuB;EACvB,YAAY,EAAE;AAChB;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,eAAe;EACf,eAAe;EACf,mBAAmB;EACnB,yBAAyB,EAAE;AAC7B;EACE,gBAAgB,EAAE;AACpB;EACE,YAAY,EAAE;AAChB;gFACgF;AAChF;;GAEG;AACH;;EAEE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc;EACd,UAAU,EAAE;AACd;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;;;EAKE,wBAAwB;EACxB,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,kBAAkB;EAClB,OAAO;EACP,UAAU;EACV,OAAO,EAAE;AACX;;;GAGG;AACH;;EAEE,OAAO;EACP,kBAAkB,EAAE;AACtB;;;GAGG;AACH;;EAEE,OAAO;EACP,qBAAqB,EAAE;AACzB;;;;GAIG;AACH;;;;EAIE,2BAA2B;EAC3B,OAAO,EAAE;AACX;;GAEG;AACH;;;;EAIE,mBAAmB;EACnB,WAAW,EAAE;AACf;;GAEG;AACH;;;;EAIE,+BAA+B,EAAE;AACnC;;GAEG;AACH;EACE,+BAA+B,EAAE;AACnC;;;;;GAKG;AACH;EACE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,eAAe;EACf,OAAO;EACP,eAAe;EACf,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,WAAW;EACX,OAAO;EACP,oBAAoB;EACpB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,sBAAsB;EACtB,OAAO;EACP,yBAAyB;EACzB,OAAO,EAAE;AACX;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,WAAW;EACX,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,aAAa,EAAE;AACjB;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,qBAAqB;EACrB,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,yBAAyB,EAAE;AAC7B;;;GAGG;AACH;EACE,2BAA2B;EAC3B,OAAO;EACP,cAAc;EACd,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;;EAEE,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;gFACgF;AAChF;;GAEG;AACH;EACE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc,EAAE;AAClB;gFACgF;AAChF;;GAEG;AACH;EACE,cAAc,EAAE","file":"normalize.css","sourcesContent":["/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,YAAY;EACZ,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE;IACE,aAAa,EAAE,EAAE;AACrB;EACE;IACE,eAAe,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HomePage/main.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n@-webkit-keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@-webkit-keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n@keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n.main-form {\n  position: absolute;\n  top: 14vh; }\n.search-button {\n  font-family: Shabnam !important; }\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.second-part {\n  margin-top: 4%; }\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15); }\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  color: black; }\n.search-input::-webkit-input-placeholder {\n  padding-right: 4%; }\n.search-input:-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::placeholder {\n  padding-right: 4%; }\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl; }\n.deal-type-section {\n  text-align: right; }\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%; }\n  .search-button {\n    display: block;\n    margin: 5% auto; } }\nbody {\n  font-family: Shabnam;\n  overflow-x: hidden; }\n.logo-section {\n  margin-bottom: 7%;\n  text-align: center; }\n.logo-image {\n  max-width: 120px; }\n.slider {\n  height: 100vh;\n  margin: 0 auto;\n  position: relative; }\n.header-main {\n  z-index: 999;\n  top: 3vh;\n  position: absolute; }\n.header-main-boarder {\n  border: thin solid white;\n  border-radius: 10px; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 0;\n  top: 6vh; }\n.slider-column {\n  padding: 0; }\n.slide1, .slide2, .slide3, .slide4 {\n  position: absolute;\n  width: 100%;\n  height: 100%; }\n.slide1 {\n  background: url(" + escape(__webpack_require__("./src/components/HomePage/michal-kubalczyk-260909-unsplash.jpg")) + ") no-repeat 90% 80%;\n  background-size: cover;\n  animation: fade 10s infinite;\n  -webkit-animation: fade 10s infinite; }\n.slide2 {\n  background: url(" + escape(__webpack_require__("./src/components/HomePage/mahdiar-mahmoodi-452489-unsplash.jpg")) + ") no-repeat center;\n  background-size: cover;\n  animation: fade2 10s infinite;\n  -webkit-animation: fade2 10s infinite; }\n.slide3 {\n  background: url(" + escape(__webpack_require__("./src/components/HomePage/casey-horner-533586-unsplash.jpg")) + ") no-repeat center;\n  background-size: cover;\n  animation: fade3 10s infinite;\n  -webkit-animation: fade3 10s infinite; }\n.slide4 {\n  background: url(" + escape(__webpack_require__("./src/components/HomePage/luke-van-zyl-504032-unsplash.jpg")) + ") no-repeat center;\n  background-size: cover;\n  animation: fade4 10s infinite;\n  -webkit-animation: fade4 10s infinite; }\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n  text-decoration: none;\n  outline: none;\n  cursor: pointer; }\n.features-section {\n  position: relative;\n  margin-top: -5vh; }\ninput[type=radio] {\n  font-size: 16px; }\n.input-label {\n  font-weight: lighter;\n  font-size: 16px; }\n.feature-box {\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  margin-left: 2%;\n  height: 36vh;\n  width: 29% !important;\n  min-height: 42vh; }\n.features-container > .feature-box:nth-child(2) {\n  margin-left: 5%; }\n.features-container > .feature-box:nth-child(3) {\n  margin-left: 5%; }\n.feature-box > div > img {\n  max-width: 70px; }\n.why-us {\n  margin-top: 6%;\n  margin-bottom: 6%; }\n.why-us-img {\n  max-width: 300px; }\n.why-list {\n  list-style: none;\n  display: block;\n  color: black;\n  padding: 0; }\n.why-list > li {\n  margin-top: 3%; }\n.why-list > li > i {\n  margin-left: 2%; }\n.why-us-img-column {\n  text-align: right; }\n.reasons-section {\n  text-align: right;\n  direction: rtl;\n  padding: 0; }\n@media (max-width: 768px) {\n  .header-main-boarder {\n    width: 40%;\n    margin-left: 1%; }\n  .mobile-header {\n    margin-top: -12%; }\n  .logo-image {\n    max-width: 90px; }\n  .main-title {\n    font-size: 20px; }\n  .feature-box {\n    width: 100% !important;\n    margin-bottom: 5%;\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(2) {\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(3) {\n    margin-left: 0; }\n  .why-us-img {\n    max-width: 247px; }\n  .features-section {\n    position: relative;\n    margin-top: 2vh; } }\n@media (max-width: 320px) {\n  .mobile-header {\n    margin-top: -12%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/HomePage/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;EACE,mBAAmB;EACnB,UAAU,EAAE;AACd;EACE,gCAAgC,EAAE;AACpC;EACE,aAAa;EACb,qCAAqC;EACrC,YAAY;EACZ,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,eAAe,EAAE;AACnB;EACE,eAAe;EACf,aAAa;EACb,eAAe,EAAE;AACnB;EACE,WAAW;EACX,gBAAgB;EAChB,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,iDAAiD;UACzC,yCAAyC,EAAE;AACrD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,YAAY;EACZ,oBAAoB;EACpB,gBAAgB;EAChB,aAAa,EAAE;AACjB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,YAAY;EACZ,aAAa;EACb,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE;IACE,gBAAgB,EAAE;EACpB;IACE,eAAe;IACf,gBAAgB,EAAE,EAAE;AACxB;EACE,qBAAqB;EACrB,mBAAmB,EAAE;AACvB;EACE,kBAAkB;EAClB,mBAAmB,EAAE;AACvB;EACE,iBAAiB,EAAE;AACrB;EACE,cAAc;EACd,eAAe;EACf,mBAAmB,EAAE;AACvB;EACE,aAAa;EACb,SAAS;EACT,mBAAmB,EAAE;AACvB;EACE,yBAAyB;EACzB,oBAAoB,EAAE;AACxB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,oDAAoD;UAC5C,4CAA4C;EACpD,mBAAmB;EACnB,WAAW;EACX,QAAQ;EACR,SAAS,EAAE;AACb;EACE,WAAW,EAAE;AACf;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa,EAAE;AACjB;EACE,4DAAwE;EACxE,uBAAuB;EACvB,6BAA6B;EAC7B,qCAAqC,EAAE;AACzC;EACE,2DAAuE;EACvE,uBAAuB;EACvB,8BAA8B;EAC9B,sCAAsC,EAAE;AAC1C;EACE,2DAAmE;EACnE,uBAAuB;EACvB,8BAA8B;EAC9B,sCAAsC,EAAE;AAC1C;EACE,2DAAmE;EACnE,uBAAuB;EACvB,8BAA8B;EAC9B,sCAAsC,EAAE;AAC1C;;;EAGE,sBAAsB;EACtB,cAAc;EACd,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,iBAAiB,EAAE;AACrB;EACE,gBAAgB,EAAE;AACpB;EACE,qBAAqB;EACrB,gBAAgB,EAAE;AACpB;EACE,kDAAkD;UAC1C,0CAA0C;EAClD,gBAAgB;EAChB,aAAa;EACb,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE,gBAAgB,EAAE;AACpB;EACE,gBAAgB,EAAE;AACpB;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,kBAAkB,EAAE;AACtB;EACE,iBAAiB,EAAE;AACrB;EACE,iBAAiB;EACjB,eAAe;EACf,aAAa;EACb,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,gBAAgB,EAAE;AACpB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB;EAClB,eAAe;EACf,WAAW,EAAE;AACf;EACE;IACE,WAAW;IACX,gBAAgB,EAAE;EACpB;IACE,iBAAiB,EAAE;EACrB;IACE,gBAAgB,EAAE;EACpB;IACE,gBAAgB,EAAE;EACpB;IACE,uBAAuB;IACvB,kBAAkB;IAClB,eAAe,EAAE;EACnB;IACE,eAAe,EAAE;EACnB;IACE,eAAe,EAAE;EACnB;IACE,iBAAiB,EAAE;EACrB;IACE,mBAAmB;IACnB,gBAAgB,EAAE,EAAE;AACxB;EACE;IACE,iBAAiB,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n@-webkit-keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@-webkit-keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n@keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n.main-form {\n  position: absolute;\n  top: 14vh; }\n.search-button {\n  font-family: Shabnam !important; }\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.second-part {\n  margin-top: 4%; }\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15); }\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  color: black; }\n.search-input::-webkit-input-placeholder {\n  padding-right: 4%; }\n.search-input:-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::placeholder {\n  padding-right: 4%; }\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl; }\n.deal-type-section {\n  text-align: right; }\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%; }\n  .search-button {\n    display: block;\n    margin: 5% auto; } }\nbody {\n  font-family: Shabnam;\n  overflow-x: hidden; }\n.logo-section {\n  margin-bottom: 7%;\n  text-align: center; }\n.logo-image {\n  max-width: 120px; }\n.slider {\n  height: 100vh;\n  margin: 0 auto;\n  position: relative; }\n.header-main {\n  z-index: 999;\n  top: 3vh;\n  position: absolute; }\n.header-main-boarder {\n  border: thin solid white;\n  border-radius: 10px; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 0;\n  top: 6vh; }\n.slider-column {\n  padding: 0; }\n.slide1, .slide2, .slide3, .slide4 {\n  position: absolute;\n  width: 100%;\n  height: 100%; }\n.slide1 {\n  background: url(michal-kubalczyk-260909-unsplash.jpg) no-repeat 90% 80%;\n  background-size: cover;\n  animation: fade 10s infinite;\n  -webkit-animation: fade 10s infinite; }\n.slide2 {\n  background: url(mahdiar-mahmoodi-452489-unsplash.jpg) no-repeat center;\n  background-size: cover;\n  animation: fade2 10s infinite;\n  -webkit-animation: fade2 10s infinite; }\n.slide3 {\n  background: url(casey-horner-533586-unsplash.jpg) no-repeat center;\n  background-size: cover;\n  animation: fade3 10s infinite;\n  -webkit-animation: fade3 10s infinite; }\n.slide4 {\n  background: url(luke-van-zyl-504032-unsplash.jpg) no-repeat center;\n  background-size: cover;\n  animation: fade4 10s infinite;\n  -webkit-animation: fade4 10s infinite; }\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n  text-decoration: none;\n  outline: none;\n  cursor: pointer; }\n.features-section {\n  position: relative;\n  margin-top: -5vh; }\ninput[type=radio] {\n  font-size: 16px; }\n.input-label {\n  font-weight: lighter;\n  font-size: 16px; }\n.feature-box {\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  margin-left: 2%;\n  height: 36vh;\n  width: 29% !important;\n  min-height: 42vh; }\n.features-container > .feature-box:nth-child(2) {\n  margin-left: 5%; }\n.features-container > .feature-box:nth-child(3) {\n  margin-left: 5%; }\n.feature-box > div > img {\n  max-width: 70px; }\n.why-us {\n  margin-top: 6%;\n  margin-bottom: 6%; }\n.why-us-img {\n  max-width: 300px; }\n.why-list {\n  list-style: none;\n  display: block;\n  color: black;\n  padding: 0; }\n.why-list > li {\n  margin-top: 3%; }\n.why-list > li > i {\n  margin-left: 2%; }\n.why-us-img-column {\n  text-align: right; }\n.reasons-section {\n  text-align: right;\n  direction: rtl;\n  padding: 0; }\n@media (max-width: 768px) {\n  .header-main-boarder {\n    width: 40%;\n    margin-left: 1%; }\n  .mobile-header {\n    margin-top: -12%; }\n  .logo-image {\n    max-width: 90px; }\n  .main-title {\n    font-size: 20px; }\n  .feature-box {\n    width: 100% !important;\n    margin-bottom: 5%;\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(2) {\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(3) {\n    margin-left: 0; }\n  .why-us-img {\n    max-width: 247px; }\n  .features-section {\n    position: relative;\n    margin-top: 2vh; } }\n@media (max-width: 320px) {\n  .mobile-header {\n    margin-top: -12%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n.browserupgrade {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA,iBAAiB;AACjB;;;;;;;GAOG;AACH;;;;;;;GAOG;AACH;EACE;;gFAE8E;;EAE9E,kEAAkE;;EAElE;;gFAE8E;;EAE9E,4BAA4B;;EAE5B;;gFAE8E;;EAE9E,uBAAuB,EAAE,gCAAgC;EACzD,uBAAuB,EAAE,2BAA2B;EACpD,uBAAuB,EAAE,6BAA6B;EACtD,wBAAwB,CAAC,iCAAiC;CAC3D;AACD;EACE,qBAAqB;EACrB,mCAAmC;EACnC,uKAAuK;EACvK,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,6BAA6B;EAC7B,qJAAqJ;EACrJ,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,kCAAkC;EAClC,oKAAoK;EACpK,iBAAiB,EAAE;AACrB;;;;GAIG;AACH;;gFAEgF;AAChF;EACE,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;EACnB,WAAW,EAAE;AACf;EACE,UAAU;EACV,qBAAqB,EAAE;AACzB;EACE,eAAe,EAAE;AACnB;;;;;;GAMG;AACH;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;;GAEG;AACH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW,EAAE;AACf;;;;GAIG;AACH;;;;;;EAME,uBAAuB,EAAE;AAC3B;;GAEG;AACH;EACE,UAAU;EACV,UAAU;EACV,WAAW,EAAE;AACf;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;gFAEgF;AAChF;EACE,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,iBAAiB,EAAE;AACrB;;;;gFAIgF;AAChF;EACE;;;IAGE,mCAAmC;IACnC,uBAAuB;IACvB,+DAA+D;IAC/D,oCAAoC;YAC5B,4BAA4B;IACpC,6BAA6B,EAAE;EACjC;;IAEE,2BAA2B,EAAE;EAC/B;IACE,6BAA6B,EAAE;EACjC;IACE,8BAA8B,EAAE;EAClC;;;KAGG;EACH;;IAEE,YAAY,EAAE;EAChB;;IAEE,uBAAuB;IACvB,yBAAyB,EAAE;EAC7B;;;KAGG;EACH;IACE,4BAA4B,EAAE;EAChC;;IAEE,yBAAyB,EAAE;EAC7B;IACE,2BAA2B,EAAE;EAC/B;;;IAGE,WAAW;IACX,UAAU,EAAE;EACd;;IAEE,wBAAwB,EAAE,EAAE","file":"Layout.css","sourcesContent":["@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n:global(.browserupgrade) {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".loader {\n  position: fixed;\n  font-family: IRSans, serif;\n  z-index: 10;\n  padding: 20px;\n  border-radius: 10px;\n  background-color: white;\n  display: none;\n  top: calc(50% - 85px);\n  left: calc(50% - 70px);\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n  .loader img {\n    width: 100px; }\n  .loader .loader-info {\n    direction: rtl;\n    padding-top: 10px;\n    text-align: center; }\n  .loader.loading {\n    display: block; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Loader/Loader.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,2BAA2B;EAC3B,YAAY;EACZ,cAAc;EACd,oBAAoB;EACpB,wBAAwB;EACxB,cAAc;EACd,sBAAsB;EACtB,uBAAuB;EACvB,wHAAwH;UAChH,gHAAgH,EAAE;EAC1H;IACE,aAAa,EAAE;EACjB;IACE,eAAe;IACf,kBAAkB;IAClB,mBAAmB,EAAE;EACvB;IACE,eAAe,EAAE","file":"Loader.scss","sourcesContent":[".loader {\n  position: fixed;\n  font-family: IRSans, serif;\n  z-index: 10;\n  padding: 20px;\n  border-radius: 10px;\n  background-color: white;\n  display: none;\n  top: calc(50% - 85px);\n  left: calc(50% - 70px);\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n  .loader img {\n    width: 100px; }\n  .loader .loader-info {\n    direction: rtl;\n    padding-top: 10px;\n    text-align: center; }\n  .loader.loading {\n    display: block; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB;EACE,gCAAgC,EAAE;AACpC,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.second-part {\n  margin-top: 4%; }\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15); }\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  font-family: Shabnam !important;\n  color: black !important; }\n.search-input::-webkit-input-placeholder {\n  padding-right: 4%; }\n.search-input:-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::placeholder {\n  padding-right: 4%; }\n.error {\n  border: 1px solid red !important; }\n.err-msg {\n  text-align: right;\n  direction: rtl;\n  color: #f16464; }\n.search-button:disabled {\n  background-color: #b3aeae; }\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl; }\n.deal-type-section {\n  text-align: right; }\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%; }\n  .search-button {\n    display: block;\n    margin: 5% auto; } }\nbody {\n  font-family: Shabnam;\n  overflow-x: hidden; }\n.logo-section {\n  margin-bottom: 7%;\n  text-align: center; }\n.logo-image {\n  max-width: 120px; }\n.slider {\n  height: 100vh;\n  margin: 0 auto;\n  position: relative; }\n.header-main {\n  z-index: 999;\n  top: 3vh;\n  position: absolute; }\n.header-main-boarder {\n  border: thin solid white;\n  border-radius: 10px; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 0;\n  top: 6vh; }\n.slider-column {\n  padding: 0; }\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n  text-decoration: none;\n  outline: none;\n  cursor: pointer; }\n.features-section {\n  position: relative;\n  margin-top: -5vh; }\ninput[type=radio] {\n  font-size: 16px; }\n.input-label {\n  font-weight: lighter;\n  font-size: 16px;\n  margin-left: 15%; }\n.feature-box {\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  margin-left: 2%;\n  height: 36vh;\n  width: 29% !important;\n  min-height: 42vh; }\n.features-container > .feature-box:nth-child(2) {\n  margin-left: 5%; }\n.features-container > .feature-box:nth-child(3) {\n  margin-left: 5%; }\n.feature-box > div > img {\n  max-width: 70px; }\n.why-us {\n  margin-top: 6%;\n  margin-bottom: 6%; }\n.why-us-img {\n  max-width: 300px; }\n.why-list {\n  list-style: none;\n  display: block;\n  color: black;\n  padding: 0; }\n.why-list > li {\n  margin-top: 3%; }\n.why-list > li > i {\n  margin-left: 2%; }\n.why-us-img-column {\n  text-align: right; }\n.reasons-section {\n  text-align: right;\n  direction: rtl;\n  padding: 0; }\n.building-type-label {\n  height: 5vh; }\n@media (max-width: 768px) {\n  .header-main-boarder {\n    width: 40%;\n    margin-left: 1%; }\n  .mobile-header {\n    margin-top: -12%; }\n  .logo-image {\n    max-width: 90px; }\n  .main-title {\n    font-size: 20px; }\n  .feature-box {\n    width: 100% !important;\n    margin-bottom: 5%;\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(2) {\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(3) {\n    margin-left: 0; }\n  .why-us-img {\n    max-width: 247px; } }\n@media (max-width: 320px) {\n  .mobile-header {\n    margin-top: -12%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/SearchBox/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,aAAa;EACb,qCAAqC;EACrC,YAAY;EACZ,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,eAAe,EAAE;AACnB;EACE,eAAe;EACf,aAAa;EACb,eAAe,EAAE;AACnB;EACE,WAAW;EACX,gBAAgB;EAChB,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,iDAAiD;UACzC,yCAAyC,EAAE;AACrD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,YAAY;EACZ,oBAAoB;EACpB,gBAAgB;EAChB,gCAAgC;EAChC,wBAAwB,EAAE;AAC5B;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,iCAAiC,EAAE;AACrC;EACE,kBAAkB;EAClB,eAAe;EACf,eAAe,EAAE;AACnB;EACE,0BAA0B,EAAE;AAC9B;EACE,YAAY;EACZ,aAAa;EACb,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE;IACE,gBAAgB,EAAE;EACpB;IACE,eAAe;IACf,gBAAgB,EAAE,EAAE;AACxB;EACE,qBAAqB;EACrB,mBAAmB,EAAE;AACvB;EACE,kBAAkB;EAClB,mBAAmB,EAAE;AACvB;EACE,iBAAiB,EAAE;AACrB;EACE,cAAc;EACd,eAAe;EACf,mBAAmB,EAAE;AACvB;EACE,aAAa;EACb,SAAS;EACT,mBAAmB,EAAE;AACvB;EACE,yBAAyB;EACzB,oBAAoB,EAAE;AACxB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,oDAAoD;UAC5C,4CAA4C;EACpD,mBAAmB;EACnB,WAAW;EACX,QAAQ;EACR,SAAS,EAAE;AACb;EACE,WAAW,EAAE;AACf;;;EAGE,sBAAsB;EACtB,cAAc;EACd,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,iBAAiB,EAAE;AACrB;EACE,gBAAgB,EAAE;AACpB;EACE,qBAAqB;EACrB,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,kDAAkD;UAC1C,0CAA0C;EAClD,gBAAgB;EAChB,aAAa;EACb,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE,gBAAgB,EAAE;AACpB;EACE,gBAAgB,EAAE;AACpB;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,kBAAkB,EAAE;AACtB;EACE,iBAAiB,EAAE;AACrB;EACE,iBAAiB;EACjB,eAAe;EACf,aAAa;EACb,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,gBAAgB,EAAE;AACpB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB;EAClB,eAAe;EACf,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE;IACE,WAAW;IACX,gBAAgB,EAAE;EACpB;IACE,iBAAiB,EAAE;EACrB;IACE,gBAAgB,EAAE;EACpB;IACE,gBAAgB,EAAE;EACpB;IACE,uBAAuB;IACvB,kBAAkB;IAClB,eAAe,EAAE;EACnB;IACE,eAAe,EAAE;EACnB;IACE,eAAe,EAAE;EACnB;IACE,iBAAiB,EAAE,EAAE;AACzB;EACE;IACE,iBAAiB,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.second-part {\n  margin-top: 4%; }\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15); }\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  font-family: Shabnam !important;\n  color: black !important; }\n.search-input::-webkit-input-placeholder {\n  padding-right: 4%; }\n.search-input:-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::placeholder {\n  padding-right: 4%; }\n.error {\n  border: 1px solid red !important; }\n.err-msg {\n  text-align: right;\n  direction: rtl;\n  color: #f16464; }\n.search-button:disabled {\n  background-color: #b3aeae; }\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl; }\n.deal-type-section {\n  text-align: right; }\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%; }\n  .search-button {\n    display: block;\n    margin: 5% auto; } }\nbody {\n  font-family: Shabnam;\n  overflow-x: hidden; }\n.logo-section {\n  margin-bottom: 7%;\n  text-align: center; }\n.logo-image {\n  max-width: 120px; }\n.slider {\n  height: 100vh;\n  margin: 0 auto;\n  position: relative; }\n.header-main {\n  z-index: 999;\n  top: 3vh;\n  position: absolute; }\n.header-main-boarder {\n  border: thin solid white;\n  border-radius: 10px; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 0;\n  top: 6vh; }\n.slider-column {\n  padding: 0; }\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n  text-decoration: none;\n  outline: none;\n  cursor: pointer; }\n.features-section {\n  position: relative;\n  margin-top: -5vh; }\ninput[type=radio] {\n  font-size: 16px; }\n.input-label {\n  font-weight: lighter;\n  font-size: 16px;\n  margin-left: 15%; }\n.feature-box {\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  margin-left: 2%;\n  height: 36vh;\n  width: 29% !important;\n  min-height: 42vh; }\n.features-container > .feature-box:nth-child(2) {\n  margin-left: 5%; }\n.features-container > .feature-box:nth-child(3) {\n  margin-left: 5%; }\n.feature-box > div > img {\n  max-width: 70px; }\n.why-us {\n  margin-top: 6%;\n  margin-bottom: 6%; }\n.why-us-img {\n  max-width: 300px; }\n.why-list {\n  list-style: none;\n  display: block;\n  color: black;\n  padding: 0; }\n.why-list > li {\n  margin-top: 3%; }\n.why-list > li > i {\n  margin-left: 2%; }\n.why-us-img-column {\n  text-align: right; }\n.reasons-section {\n  text-align: right;\n  direction: rtl;\n  padding: 0; }\n.building-type-label {\n  height: 5vh; }\n@media (max-width: 768px) {\n  .header-main-boarder {\n    width: 40%;\n    margin-left: 1%; }\n  .mobile-header {\n    margin-top: -12%; }\n  .logo-image {\n    max-width: 90px; }\n  .main-title {\n    font-size: 20px; }\n  .feature-box {\n    width: 100% !important;\n    margin-bottom: 5%;\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(2) {\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(3) {\n    margin-left: 0; }\n  .why-us-img {\n    max-width: 247px; } }\n@media (max-width: 320px) {\n  .mobile-header {\n    margin-top: -12%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/url/escape.js":
/***/ (function(module, exports) {

module.exports = function escape(url) {
    if (typeof url !== 'string') {
        return url
    }
    // If url is already wrapped in quotes, remove them
    if (/^['"].*['"]$/.test(url)) {
        url = url.slice(1, -1);
    }
    // Should url be wrapped?
    // See https://drafts.csswg.org/css-values-3/#urls
    if (/["'() \t\n]/.test(url)) {
        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
    }

    return url
}


/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/actions/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["loginRequest"] = loginRequest;
/* harmony export (immutable) */ __webpack_exports__["loginSuccess"] = loginSuccess;
/* harmony export (immutable) */ __webpack_exports__["loginFailure"] = loginFailure;
/* harmony export (immutable) */ __webpack_exports__["loadLoginStatus"] = loadLoginStatus;
/* harmony export (immutable) */ __webpack_exports__["loginUser"] = loginUser;
/* harmony export (immutable) */ __webpack_exports__["logoutRequest"] = logoutRequest;
/* harmony export (immutable) */ __webpack_exports__["logoutSuccess"] = logoutSuccess;
/* harmony export (immutable) */ __webpack_exports__["logoutFailure"] = logoutFailure;
/* harmony export (immutable) */ __webpack_exports__["logoutUser"] = logoutUser;
/* harmony export (immutable) */ __webpack_exports__["initiateRequest"] = initiateRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateSuccess"] = initiateSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateFailure"] = initiateFailure;
/* harmony export (immutable) */ __webpack_exports__["initiate"] = initiate;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersRequest"] = initiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersSuccess"] = initiateUsersSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersFailure"] = initiateUsersFailure;
/* harmony export (immutable) */ __webpack_exports__["initiateUsers"] = initiateUsers;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserRequest"] = getCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserSuccess"] = getCurrentUserSuccess;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserFailure"] = getCurrentUserFailure;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUser"] = getCurrentUser;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditRequest"] = increaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditSuccess"] = increaseCreditSuccess;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditFailure"] = increaseCreditFailure;
/* harmony export (immutable) */ __webpack_exports__["increaseCredit"] = increaseCredit;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__ = __webpack_require__("./src/api-handlers/authentication.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function loginRequest(username, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["K" /* USER_LOGIN_REQUEST */],
    payload: {
      username,
      password
    }
  };
}

function loginSuccess(info) {
  console.log(info);
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["L" /* USER_LOGIN_SUCCESS */],
    payload: { info }
  };
}

function loginFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["J" /* USER_LOGIN_FAILURE */],
    payload: { error }
  };
}

function loadLoginStatus() {
  const loginInfo = Object(__WEBPACK_IMPORTED_MODULE_0__utils__["c" /* getLoginInfo */])();
  if (loginInfo) {
    return loginSuccess(loginInfo);
  }
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["I" /* USER_IS_NOT_LOGGED_IN */]
  };
}

function getCaptchaToken() {
  if (false) {
    const loginForm = $('#loginForm').get(0);
    if (loginForm && typeof loginForm['g-recaptcha-response'] === 'object') {
      return loginForm['g-recaptcha-response'].value;
    }
  }
  return null;
}

function loginUser(username, password) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(loginRequest(username, password));
      try {
        // const captchaToken = getCaptchaToken();
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["c" /* sendLoginRequest */])(username, password);
        dispatch(loginSuccess(res));
        localStorage.setItem('loginInfo', JSON.stringify(res));
        // console.log(location.pathname);
        const { pathname } = getState().house;
        console.log(pathname);
        __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push(pathname);
      } catch (err) {
        dispatch(loginFailure(err));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}

function logoutRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["N" /* USER_LOGOUT_REQUEST */],
    payload: {}
  };
}

function logoutSuccess() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["O" /* USER_LOGOUT_SUCCESS */],
    payload: {
      message: 'خروج موفقیت آمیز'
    }
  };
}

function logoutFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["M" /* USER_LOGOUT_FAILURE */],
    payload: { error }
  };
}

function logoutUser() {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(logoutRequest());
      try {
        dispatch(logoutSuccess());
        localStorage.clear();
        location.assign('/login');
      } catch (err) {
        dispatch(logoutFailure(err));
      }
    });

    return function (_x3) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function initiateRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["q" /* GET_INIT_REQUEST */]
  };
}

function initiateSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["r" /* GET_INIT_SUCCESS */],
    payload: { response }
  };
}

function initiateFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["p" /* GET_INIT_FAILURE */],
    payload: { err }
  };
}

function initiate() {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["a" /* sendInitiateRequest */])();
        dispatch(initiateSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateFailure(error));
      }
    });

    return function (_x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function initiateUsersRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["t" /* GET_INIT_USER_REQUEST */]
  };
}

function initiateUsersSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["u" /* GET_INIT_USER_SUCCESS */],
    payload: { response }
  };
}

function initiateUsersFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["s" /* GET_INIT_USER_FAILURE */],
    payload: { err }
  };
}

function initiateUsers() {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateUsersRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["b" /* sendInitiateUsersRequest */])();
        dispatch(initiateUsersSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateUsersFailure(error));
      }
    });

    return function (_x5) {
      return _ref4.apply(this, arguments);
    };
  })();
}

function getCurrentUserRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["e" /* GET_CURR_USER_REQUEST */]
  };
}

function getCurrentUserSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["f" /* GET_CURR_USER_SUCCESS */],
    payload: { response }
  };
}

function getCurrentUserFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["d" /* GET_CURR_USER_FAILURE */],
    payload: { err }
  };
}

function getCurrentUser(userId) {
  return (() => {
    var _ref5 = _asyncToGenerator(function* (dispatch) {
      dispatch(getCurrentUserRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["d" /* sendgetCurrentUserRequest */])(userId);
        dispatch(getCurrentUserSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getCurrentUserFailure(error));
      }
    });

    return function (_x6) {
      return _ref5.apply(this, arguments);
    };
  })();
}

function increaseCreditRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["z" /* INCREASE_CREDIT_REQUEST */]
  };
}

function increaseCreditSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["A" /* INCREASE_CREDIT_SUCCESS */],
    payload: { response }
  };
}

function increaseCreditFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["y" /* INCREASE_CREDIT_FAILURE */],
    payload: { err }
  };
}

function increaseCredit(amount) {
  return (() => {
    var _ref6 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(increaseCreditRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["e" /* sendincreaseCreditRequest */])(amount, currUser.userId);
        dispatch(increaseCreditSuccess(res));
        dispatch(getCurrentUser(currUser.userId));
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["f" /* notifySuccess */])('افزایش اعتبار با موفقیت انجام شد.');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(increaseCreditFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref6.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/actions/search.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["setSearchObjRequest"] = setSearchObjRequest;
/* harmony export (immutable) */ __webpack_exports__["setSearchObjSuccess"] = setSearchObjSuccess;
/* harmony export (immutable) */ __webpack_exports__["setSearchObjFailure"] = setSearchObjFailure;
/* harmony export (immutable) */ __webpack_exports__["setSearchObj"] = setSearchObj;
/* harmony export (immutable) */ __webpack_exports__["searchHousesRequest"] = searchHousesRequest;
/* harmony export (immutable) */ __webpack_exports__["searchHousesSuccess"] = searchHousesSuccess;
/* harmony export (immutable) */ __webpack_exports__["searchHousesFailure"] = searchHousesFailure;
/* harmony export (immutable) */ __webpack_exports__["searchHouses"] = searchHouses;
/* harmony export (immutable) */ __webpack_exports__["pollHousesRequest"] = pollHousesRequest;
/* harmony export (immutable) */ __webpack_exports__["pollHousesSuccess"] = pollHousesSuccess;
/* harmony export (immutable) */ __webpack_exports__["pollHousesFailure"] = pollHousesFailure;
/* harmony export (immutable) */ __webpack_exports__["pollHouses"] = pollHouses;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_handlers_search__ = __webpack_require__("./src/api-handlers/search.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils__ = __webpack_require__("./src/utils/index.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function setSearchObjRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["G" /* SET_SEARCH_OBJ_REQUEST */]
  };
}

function setSearchObjSuccess(minArea, dealType, buildingType, maxPrice) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["H" /* SET_SEARCH_OBJ_SUCCESS */],
    payload: { minArea, dealType, buildingType, maxPrice }
  };
}

function setSearchObjFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["F" /* SET_SEARCH_OBJ_FAILURE */],
    payload: { err }
  };
}

function setSearchObj(minArea, dealType, buildingType, maxPrice) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch) {
      dispatch(setSearchObjSuccess(minArea, dealType, buildingType, maxPrice));
      __WEBPACK_IMPORTED_MODULE_1__history__["a" /* default */].push('/houses');
    });

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  })();
}
function searchHousesRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["h" /* GET_HOUSES_REQUEST */]
  };
}

function searchHousesSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["i" /* GET_HOUSES_SUCCESS */],
    payload: { response }
  };
}

function searchHousesFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["g" /* GET_HOUSES_FAILURE */],
    payload: { err }
  };
}

function searchHouses(minArea, dealType, buildingType, maxPrice) {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(searchHousesRequest());
      try {
        if (!buildingType) {
          buildingType = null;
        }
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_search__["a" /* sendSearchHousesRequest */])(parseFloat(minArea), parseFloat(dealType), buildingType, parseFloat(maxPrice));
        dispatch(searchHousesSuccess(res));
        dispatch(setSearchObj(minArea, dealType, buildingType, maxPrice));
        if (res.length === 0) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["f" /* notifySuccess */])('خانه ای با این این مشخصات ثبت نشده است');
        }
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(searchHousesFailure(error));
      }
    });

    return function (_x2) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function pollHousesRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["C" /* POLL_HOUSES_REQUEST */]
  };
}

function pollHousesSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["D" /* POLL_HOUSES_SUCCESS */],
    payload: { response }
  };
}

function pollHousesFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["B" /* POLL_HOUSES_FAILURE */],
    payload: { err }
  };
}

function pollHouses(minArea, dealType, buildingType, maxPrice) {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(pollHousesRequest());
      try {
        if (!buildingType) {
          buildingType = null;
        }
        let res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_search__["b" /* sendpollHousesRequest */])(parseFloat(minArea), parseFloat(dealType), buildingType, parseFloat(maxPrice));
        console.log(res);
        if (!res) {
          res = getState().search.houses;
          console.log('hala');
          console.log(res);
        }
        dispatch(pollHousesSuccess(res));
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])('اطلاعات جست و جوی شما بروز شد.');

        dispatch(setSearchObj(minArea, dealType, buildingType, maxPrice));
        if (res.length === 0) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["f" /* notifySuccess */])('خانه ای با این این مشخصات ثبت نشده است');
        }
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(pollHousesFailure(error));
      }
    });

    return function (_x3, _x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/api-handlers/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = sendincreaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["a"] = sendInitiateRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendInitiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendLoginRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_paths__ = __webpack_require__("./src/config/paths.js");



function sendincreaseCreditRequest(amount, id) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["g" /* increaseCreditUrl */])(amount, id), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendgetCurrentUserRequest(userId) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["b" /* getCurrentUserUrl */])(userId), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateUsersRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendLoginRequest(username, password) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["d" /* getPostConfig */])({
    username,
    password
  }));
}

/***/ }),

/***/ "./src/api-handlers/search.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = sendSearchHousesRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendpollHousesRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_paths__ = __webpack_require__("./src/config/paths.js");



/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

function sendSearchHousesRequest(minArea, dealType, buildingType, maxPrice) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_2__config_paths__["f" /* getSearchHousesUrl */], Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])({ minArea, dealType, buildingType, maxPrice }));
}

function sendpollHousesRequest(minArea, dealType, buildingType, maxPrice) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_2__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])({ minArea, dealType, buildingType, maxPrice }));
}

/***/ }),

/***/ "./src/components/Footer/200px-Instagram_logo_2016.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Instagram_logo_2016.svg.png?2fc2d5c1";

/***/ }),

/***/ "./src/components/Footer/200px-Telegram_logo.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Telegram_logo.svg.png?6fa2804c";

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__ = __webpack_require__("./src/components/Footer/Twitter_bird_logo_2012.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__ = __webpack_require__("./src/components/Footer/200px-Instagram_logo_2016.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__ = __webpack_require__("./src/components/Footer/200px-Telegram_logo.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Footer/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/Footer.js';
/* eslint-disable react/no-danger */








class Footer extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'footer',
      { className: 'site-footer center-column dark-green-background white', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'row', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4 text-align-left', __source: {
              fileName: _jsxFileName,
              lineNumber: 15
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'ul',
            { className: 'social-icons-list no-list-style', __source: {
                fileName: _jsxFileName,
                lineNumber: 16
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 17
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'instagram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 18
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 24
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'twitter-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 25
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 31
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'telegram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              })
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-8 text-align-right', __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 41
              },
              __self: this
            },
            '\u062A\u0645\u0627\u0645\u06CC \u062D\u0642\u0648\u0642 \u0627\u06CC\u0646 \u0648\u0628\u200C\u0633\u0627\u06CC\u062A \u0645\u062A\u0639\u0644\u0642 \u0628\u0647 \u0645\u0647\u0631\u0646\u0627\u0632 \u062B\u0627\u0628\u062A \u0648 \u0647\u0648\u0645\u0646 \u0634\u0631\u06CC\u0641 \u0632\u0627\u062F\u0647 \u0645\u06CC\u200C\u0628\u0627\u0634\u062F.'
          )
        )
      )
    );
  }
}

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Twitter_bird_logo_2012.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/Twitter_bird_logo_2012.svg.png?248bfa8d";

/***/ }),

/***/ "./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/HomePage/726446.svg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/726446.svg?ea76a6b9";

/***/ }),

/***/ "./src/components/HomePage/726488.svg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/726488.svg?9d200971";

/***/ }),

/***/ "./src/components/HomePage/726499.svg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/726499.svg?538ecf13";

/***/ }),

/***/ "./src/components/HomePage/HomePage.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css__ = __webpack_require__("./src/components/HomePage/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__logo_png__ = __webpack_require__("./src/components/HomePage/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__726499_svg__ = __webpack_require__("./src/components/HomePage/726499.svg");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__726499_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__726499_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__726488_svg__ = __webpack_require__("./src/components/HomePage/726488.svg");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__726488_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__726488_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__726446_svg__ = __webpack_require__("./src/components/HomePage/726446.svg");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__726446_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__726446_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__why_khanebedoosh_jpg__ = __webpack_require__("./src/components/HomePage/why-khanebedoosh.jpg");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__why_khanebedoosh_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__why_khanebedoosh_jpg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__SearchBox__ = __webpack_require__("./src/components/SearchBox/SearchBox.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/HomePage/HomePage.js';
/* eslint-disable react/no-danger */












class HomePage extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'home-panel', __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'row', __source: {
            fileName: _jsxFileName,
            lineNumber: 21
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 slider-column', __source: {
              fileName: _jsxFileName,
              lineNumber: 22
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'slider center-content', __source: {
                fileName: _jsxFileName,
                lineNumber: 23
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'slide1', __source: {
                fileName: _jsxFileName,
                lineNumber: 24
              },
              __self: this
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'slide2', __source: {
                fileName: _jsxFileName,
                lineNumber: 25
              },
              __self: this
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'slide3', __source: {
                fileName: _jsxFileName,
                lineNumber: 26
              },
              __self: this
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'slide4', __source: {
                fileName: _jsxFileName,
                lineNumber: 27
              },
              __self: this
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 col-sm-12 col-md-8 main-form', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 28
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row hidden-sm visible-xs mobile-header', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'row flex-center header-main-boarder', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 31
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'col-xs-9 ', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 32
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'h5',
                        { className: 'text-align-right persian ', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 33
                          },
                          __self: this
                        },
                        '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                      ),
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'dropdown-content', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 36
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'container-fluid', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 37
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 38
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'h4',
                              { className: 'persian text-align-right blue-font ', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 39
                                },
                                __self: this
                              },
                              '\u0628\u0647\u0646\u0627\u0645 \u0647\u0645\u0627\u06CC\u0648\u0646'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 43
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-left blue-font persian', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 44
                                },
                                __self: this
                              },
                              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                                'span',
                                {
                                  __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 45
                                  },
                                  __self: this
                                },
                                '\u06F2\u06F0\u06F0\u06F0\u06F0'
                              ),
                              '\u062A\u0648\u0645\u0627\u0646'
                            ),
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-right blue-font persian text-align-right no-padding', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 47
                                },
                                __self: this
                              },
                              '\u0627\u0639\u062A\u0628\u0627\u0631'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 51
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              onClick: () => {
                                __WEBPACK_IMPORTED_MODULE_9__history__["a" /* default */].push('/increaseCredit');
                              },
                              value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                              className: '  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 52
                              },
                              __self: this
                            })
                          )
                        )
                      )
                    ),
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'no-padding  col-xs-3', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 64
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x white-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 65
                        },
                        __self: this
                      })
                    )
                  )
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 logo-section', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 70
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_4__logo_png___default.a, alt: 'logo', className: 'logo-image', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 71
                  },
                  __self: this
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'h2',
                  { className: 'main-title white font-medium', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 72
                    },
                    __self: this
                  },
                  '\u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_10__SearchBox__["a" /* default */], { handleSubmit: this.props.handleSubmit, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 75
                },
                __self: this
              }),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 submit-home-link border-radius text-align-center', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 76
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'h5',
                  { className: 'submit-home-heading white', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 77
                    },
                    __self: this
                  },
                  '\u0635\u0627\u062D\u0628 \u062E\u0627\u0646\u0647 \u0647\u0633\u062A\u06CC\u062F\u061F \u062E\u0627\u0646\u0647 \u062E\u0648\u062F \u0631\u0627',
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'a',
                    {
                      className: 'submit-house-button white',
                      onClick: () => {
                        __WEBPACK_IMPORTED_MODULE_9__history__["a" /* default */].push('/houses/new');
                      },
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 79
                      },
                      __self: this
                    },
                    '\u062B\u0628\u062A'
                  ),
                  '\u06A9\u0646\u06CC\u062F.'
                )
              )
            )
          )
        )
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid', __source: {
            fileName: _jsxFileName,
            lineNumber: 94
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row', __source: {
              fileName: _jsxFileName,
              lineNumber: 95
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'col-xs-12 features-section center-content', __source: {
                fileName: _jsxFileName,
                lineNumber: 96
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 col-sm-12 col-md-8 features-container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 97
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-sm-12 col-md-3 feature-box text-align-center border-radius white-background center-column', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 98
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 99
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_5__726499_svg___default.a, alt: 'feature-icon', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 100
                    },
                    __self: this
                  }),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h1',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 101
                      },
                      __self: this
                    },
                    '\u06AF\u0633\u062A\u0631\u062F\u0647'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'p',
                    { dir: 'rtl', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 102
                      },
                      __self: this
                    },
                    '\u062F\u0631 \u0645\u0646\u0637\u0642\u0647 \u0645\u0648\u0631\u062F \u0639\u0644\u0627\u0642\u0647 \u062E\u0648\u062F \u0635\u0627\u062D\u0628 \u062E\u0627\u0646\u0647 \u0634\u0648\u06CC\u062F'
                  )
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-sm-12 col-md-3 feature-box text-align-center border-radius white-background center-column', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 105
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 106
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_6__726488_svg___default.a, alt: 'feature-icon', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 107
                    },
                    __self: this
                  }),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h1',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 108
                      },
                      __self: this
                    },
                    '\u0645\u0637\u0645\u0626\u0646'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'p',
                    { dir: 'rtl', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 109
                      },
                      __self: this
                    },
                    '\u0628\u0627 \u062E\u06CC\u0627\u0644 \u0631\u0627\u062D\u062A \u0628\u0647 \u062F\u0646\u0628\u0627\u0644 \u062E\u0627\u0646\u0647 \u0628\u06AF\u0631\u062F\u06CC\u062F'
                  )
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-sm-12 col-md-3 feature-box text-align-center border-radius white-background center-column', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 112
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 113
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_7__726446_svg___default.a, alt: 'feature-icon', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 114
                    },
                    __self: this
                  }),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h1',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 115
                      },
                      __self: this
                    },
                    '\u0622\u0633\u0627\u0646'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'p',
                    { dir: 'rtl', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 116
                      },
                      __self: this
                    },
                    '\u0628\u0647 \u0633\u0627\u062F\u06AF\u06CC \u0635\u0627\u062D\u0628 \u062E\u0627\u0646\u0647 \u0634\u0648\u06CC\u062F'
                  )
                )
              )
            )
          )
        )
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid why-us', __source: {
            fileName: _jsxFileName,
            lineNumber: 123
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row', __source: {
              fileName: _jsxFileName,
              lineNumber: 124
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'col-xs-12 center-content', __source: {
                fileName: _jsxFileName,
                lineNumber: 125
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 col-sm-12 col-md-8 why-us-title text-align-right', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 126
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'h1',
                { className: 'reasons-head font-bold', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 127
                  },
                  __self: this
                },
                '\u0686\u0631\u0627 \u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634\u061F'
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'col-xs-12 center-content', __source: {
                fileName: _jsxFileName,
                lineNumber: 130
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 col-sm-12 col-md-8', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 131
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-sm-12 col-md-6 why-us-img-column', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 132
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_8__why_khanebedoosh_jpg___default.a, alt: 'why-img', className: 'why-us-img', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 133
                  },
                  __self: this
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-sm-12 col-md-6 reasons-section', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 135
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'ul',
                  { className: 'why-list', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 136
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 137
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 138
                      },
                      __self: this
                    }),
                    '\u0627\u0637\u0644\u0627\u0639\u0627\u062A \u06A9\u0627\u0645\u0644 \u0648 \u0635\u062D\u06CC\u062D \u0627\u0632 \u0627\u0645\u0644\u0627\u06A9 \u0642\u0627\u0628\u0644 \u0645\u0639\u0627\u0645\u0644\u0647'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 141
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 142
                      },
                      __self: this
                    }),
                    '\u0628\u062F\u0648\u0646 \u0645\u062D\u062F\u0648\u062F\u06CC\u062A\u060C \u06F2\u06F4 \u0633\u0627\u0639\u062A\u0647 \u0648 \u062F\u0631 \u062A\u0645\u0627\u0645 \u0627\u06CC\u0627\u0645 \u0647\u0641\u062A\u0647'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 145
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 146
                      },
                      __self: this
                    }),
                    '\u062C\u0633\u062A\u062C\u0648\u06CC \u0647\u0648\u0634\u0645\u0646\u062F \u0645\u0644\u06A9\u060C \u0635\u0631\u0641\u0647\u200C\u062C\u0648\u06CC\u06CC \u062F\u0631 \u0632\u0645\u0627\u0646'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 149
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 150
                      },
                      __self: this
                    }),
                    '\u062A\u0646\u0648\u0639 \u062F\u0631 \u0627\u0645\u0644\u0627\u06A9\u060C \u0627\u0641\u0632\u0627\u06CC\u0634 \u0642\u062F\u0631\u062A \u0627\u0646\u062A\u062E\u0627\u0628 \u0645\u0634\u062A\u0631\u06CC\u0627\u0646'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 153
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 154
                      },
                      __self: this
                    }),
                    '\u0628\u0627\u0646\u06A9\u06CC \u062C\u0627\u0645\u0639 \u0627\u0632 \u0627\u0637\u0644\u0627\u0639\u0627\u062A \u0647\u0632\u0627\u0631\u0627\u0646 \u0622\u06AF\u0647\u06CC \u0628\u0647 \u0631\u0648\u0632'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 157
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 158
                      },
                      __self: this
                    }),
                    '\u062F\u0633\u062A\u06CC\u0627\u0628\u06CC \u0628\u0647 \u0646\u062A\u06CC\u062C\u0647 \u0645\u0637\u0644\u0648\u0628 \u062F\u0631 \u06A9\u0645\u062A\u0631\u06CC\u0646 \u0632\u0645\u0627\u0646 \u0645\u0645\u06A9\u0646'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'li',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 161
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-check-circle purple', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 162
                      },
                      __self: this
                    }),
                    '\u0647\u0645\u06A9\u0627\u0631\u06CC \u0628\u0627 \u0645\u0634\u0627\u0648\u0631\u0627\u0646 \u0645\u062A\u062E\u0635\u0635 \u062F\u0631 \u062D\u0648\u0632\u0647 \u0627\u0645\u0644\u0627\u06A9'
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}

HomePage.propTypes = {
  handleSubmit: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__main_css___default.a)(HomePage));

/***/ }),

/***/ "./src/components/HomePage/casey-horner-533586-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/casey-horner-533586-unsplash.jpg?d5113c82";

/***/ }),

/***/ "./src/components/HomePage/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/HomePage/luke-van-zyl-504032-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/luke-van-zyl-504032-unsplash.jpg?28cbcf9b";

/***/ }),

/***/ "./src/components/HomePage/mahdiar-mahmoodi-452489-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/mahdiar-mahmoodi-452489-unsplash.jpg?587276d2";

/***/ }),

/***/ "./src/components/HomePage/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HomePage/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HomePage/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HomePage/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/HomePage/michal-kubalczyk-260909-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/michal-kubalczyk-260909-unsplash.jpg?3a246a18";

/***/ }),

/***/ "./src/components/HomePage/why-khanebedoosh.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HomePage/why-khanebedoosh.jpg?f8ff8ccc";

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css__ = __webpack_require__("./node_modules/normalize.css/normalize.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_normalize_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Layout_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Footer__ = __webpack_require__("./src/components/Footer/Footer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Panel__ = __webpack_require__("./src/components/Panel/Panel.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.js';
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */









class Layout extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-md-12 col-xs-12', style: { padding: '0' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__Panel__["a" /* default */], { mainPanel: this.props.mainPanel, __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }),
      this.props.children,
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__Footer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      })
    );
  }
}

Layout.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool
};
Layout.defaultProps = {
  mainPanel: false
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_normalize_css___default.a, __WEBPACK_IMPORTED_MODULE_4__Layout_css___default.a)(Layout));

/***/ }),

/***/ "./src/components/Loader/Loader.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Loader_scss__ = __webpack_require__("./src/components/Loader/Loader.scss");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Loader_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Loader_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_gif__ = __webpack_require__("./src/components/Loader/loader.gif");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_gif___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__loader_gif__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Loader/Loader.js';






class Loader extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: this.props.loading ? 'loader loading' : 'loader', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_4__loader_gif___default.a, alt: 'loader gif', __source: {
            fileName: _jsxFileName,
            lineNumber: 15
          },
          __self: this
        })
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'loader-info', __source: {
            fileName: _jsxFileName,
            lineNumber: 17
          },
          __self: this
        },
        '\u0645\u0646\u062A\u0638\u0631 \u0628\u0627\u0634\u06CC\u062F'
      )
    );
  }
}

Loader.propTypes = {
  loading: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__Loader_scss___default.a)(Loader));

/***/ }),

/***/ "./src/components/Loader/Loader.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Loader/loader.gif":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Loader/loader.gif?be5c3223";

/***/ }),

/***/ "./src/components/Panel/Panel.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Panel/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png__ = __webpack_require__("./src/components/Panel/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__history__ = __webpack_require__("./src/history.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/Panel.js';
/* eslint-disable react/no-danger */










class Panel extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {

  render() {
    console.log(this.props.currUser);
    console.log(this.props.isLoggedIn);
    if (this.props.mainPanel) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'header',
        { className: 'hidden-xs', __source: {
            fileName: _jsxFileName,
            lineNumber: 25
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container-fluid', __source: {
              fileName: _jsxFileName,
              lineNumber: 26
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row header-main', __source: {
                fileName: _jsxFileName,
                lineNumber: 27
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 28
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'container', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row flex-center header-main-boarder', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9 ', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 31
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h5',
                      { className: 'text-align-right white-font persian ', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 32
                        },
                        __self: this
                      },
                      '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                    ),
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'dropdown-content', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 35
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'container-fluid', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 36
                          },
                          __self: this
                        },
                        this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          {
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 38
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 39
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'h4',
                              { className: 'persian text-align-right black-font ', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 40
                                },
                                __self: this
                              },
                              this.props.currUser.username
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 44
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-left persian light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 45
                                },
                                __self: this
                              },
                              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                                'span',
                                { className: 'light-grey-font', __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 46
                                  },
                                  __self: this
                                },
                                '\u062A\u0648\u0645\u0627\u0646',
                                this.props.currUser.balance
                              )
                            ),
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 light-grey-font pull-right  persian text-align-right no-padding', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 51
                                },
                                __self: this
                              },
                              '\u0627\u0639\u062A\u0628\u0627\u0631'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 55
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                              onClick: () => {
                                __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 56
                              },
                              __self: this
                            })
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 65
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u062E\u0631\u0648\u062C',
                              onClick: () => {
                                this.props.authActions.logoutUser();
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 66
                              },
                              __self: this
                            })
                          )
                        ),
                        !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 78
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0648\u0631\u0648\u062F',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 79
                            },
                            __self: this
                          })
                        )
                      )
                    )
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding  col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 93
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x white-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 94
                      },
                      __self: this
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'header',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid', __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row nav-cl', __source: {
              fileName: _jsxFileName,
              lineNumber: 107
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown', __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 109
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row flex-center ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 110
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'col-xs-9 ', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 111
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h5',
                    { className: 'text-align-right persian purple-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 112
                      },
                      __self: this
                    },
                    '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'dropdown-content', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 115
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'container-fluid', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 116
                        },
                        __self: this
                      },
                      this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        {
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 118
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 119
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'h4',
                            { className: 'black-font persian text-align-right blue-font', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 120
                              },
                              __self: this
                            },
                            this.props.currUser.username
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 124
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: ' light-grey-font col-xs-6 pull-left blue-font persian', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 125
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'span',
                              { className: 'light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 126
                                },
                                __self: this
                              },
                              '\u062A\u0648\u0645\u0627\u0646',
                              this.props.currUser.balance
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'light-grey-font col-xs-6 pull-right blue-font persian text-align-right no-padding', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 131
                              },
                              __self: this
                            },
                            '\u0627\u0639\u062A\u0628\u0627\u0631'
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 135
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                            className: 'white-font  blue-button persian text-align-center',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                            },
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 136
                            },
                            __self: this
                          })
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 145
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u062E\u0631\u0648\u062C',
                            onClick: () => {
                              this.props.authActions.logoutUser();
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 146
                            },
                            __self: this
                          })
                        )
                      ),
                      !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'row mar-top', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 158
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                          type: 'button',
                          value: '\u0648\u0631\u0648\u062F',
                          onClick: () => {
                            __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                          },
                          className: '  white-font  blue-button persian text-align-center',
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 159
                          },
                          __self: this
                        })
                      )
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'no-padding  col-xs-3', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 173
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x purple-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 174
                    },
                    __self: this
                  })
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-4 col-md-offset-6 nav-logo col-xs-12', __source: {
                fileName: _jsxFileName,
                lineNumber: 179
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 180
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row  ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 181
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'a',
                  {
                    className: 'flex-row-rev',
                    onClick: () => {
                      __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/');
                    },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 182
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding text-align-center col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 188
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { className: 'icon-cl', src: __WEBPACK_IMPORTED_MODULE_7__logo_png___default.a, alt: 'logo', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 189
                      },
                      __self: this
                    })
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 191
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian-bold text-align-right  persian blue-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 192
                        },
                        __self: this
                      },
                      '\u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                    )
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}
Panel.propTypes = {
  currUser: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired,
  isLoggedIn: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    authActions: Object(__WEBPACK_IMPORTED_MODULE_3_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser, isLoggedIn } = authentication;
  return {
    currUser,
    isLoggedIn
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Panel)));

/***/ }),

/***/ "./src/components/Panel/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Panel/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/SearchBox/SearchBox.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css__ = __webpack_require__("./src/components/SearchBox/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/SearchBox/SearchBox.js';
/* eslint-disable react/no-danger */





class SearchBox extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  constructor(props) {
    super(props);

    this.handleSubmit = e => {
      e.preventDefault();
      this.props.handleSubmit(this.state.minArea, this.state.dealType, this.state.buildingType, this.state.maxPrice);
    };

    this.validate = (maxPrice, minArea) => {
      // true means invalid, so our conditions got reversed
      const regex = new RegExp(/[^0-9]/, 'g');
      return {
        maxPrice: maxPrice.match(regex),
        minArea: minArea.match(regex)
      };
    };

    this.state = {
      maxPrice: '',
      minArea: '',
      buildingType: '',
      dealType: ''
    };
  }

  render() {
    const errors = this.validate(this.state.maxPrice, this.state.minArea);
    const isEnabled = !Object.keys(errors).some(x => errors[x]);

    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-xs-12 search-section border-radius no-padding', __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'col-xs-12 first-part no-padding', __source: {
            fileName: _jsxFileName,
            lineNumber: 43
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4', __source: {
              fileName: _jsxFileName,
              lineNumber: 44
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { className: 'label', __source: {
                fileName: _jsxFileName,
                lineNumber: 45
              },
              __self: this
            },
            '\u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
            placeholder: '\u062D\u062F\u0627\u0642\u0644 \u0645\u062A\u0631\u0627\u0698',
            onChange: e => {
              this.setState({ [e.target.name]: e.target.value });
            },
            name: 'minArea',
            className: `search-input border-radius clear-input ${errors.minArea ? 'error' : ''}`,
            type: 'text',
            dir: 'rtl',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 46
            },
            __self: this
          }),
          errors.minArea && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            { className: 'err-msg', __source: {
                fileName: _jsxFileName,
                lineNumber: 58
              },
              __self: this
            },
            '\u0644\u0637\u0641\u0627 \u0639\u062F\u062F \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4', __source: {
              fileName: _jsxFileName,
              lineNumber: 60
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { className: 'label', __source: {
                fileName: _jsxFileName,
                lineNumber: 61
              },
              __self: this
            },
            '\u062A\u0648\u0645\u0627\u0646'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
            placeholder: '\u062D\u062F\u0627\u06A9\u062B\u0631 \u0642\u06CC\u0645\u062A',
            onChange: e => {
              this.setState({ [e.target.name]: e.target.value });
            },
            name: 'maxPrice',
            className: `search-input border-radius clear-input ${errors.maxPrice ? 'error' : ''}`,
            type: 'text',
            dir: 'rtl',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 62
            },
            __self: this
          }),
          errors.maxPrice && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            { className: 'err-msg', __source: {
                fileName: _jsxFileName,
                lineNumber: 74
              },
              __self: this
            },
            '\u0644\u0637\u0641\u0627 \u0639\u062F\u062F \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4', __source: {
              fileName: _jsxFileName,
              lineNumber: 76
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('label', { className: 'building-type-label', __source: {
              fileName: _jsxFileName,
              lineNumber: 77
            },
            __self: this
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'select',
            {
              title: 'buildingType',
              onChange: e => {
                this.setState({ [e.target.name]: e.target.value });
              },
              name: 'buildingType',
              className: 'search-input border-radius clear-input',
              __source: {
                fileName: _jsxFileName,
                lineNumber: 78
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'option',
              { disabled: true, selected: true, className: 'select-place-holder', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 86
                },
                __self: this
              },
              '\u0646\u0648\u0639 \u0645\u0644\u06A9'
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'option',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 89
                },
                __self: this
              },
              '\u0622\u067E\u0627\u0631\u062A\u0645\u0627\u0646 '
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'option',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 90
                },
                __self: this
              },
              '\u0648\u06CC\u0644\u0627\u06CC\u06CC'
            )
          )
        )
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'col-xs-12 second-part', __source: {
            fileName: _jsxFileName,
            lineNumber: 94
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'visible-md  visible-lg col-md-6 no-padding', __source: {
              fileName: _jsxFileName,
              lineNumber: 95
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'form',
            { onSubmit: this.handleSubmit, __source: {
                fileName: _jsxFileName,
                lineNumber: 96
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'button',
              {
                disabled: !isEnabled,
                type: 'submit',
                className: 'search-button has-transition border-radius clear-input white light-blue-background',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 97
                },
                __self: this
              },
              '\u062C\u0633\u062A\u062C\u0648'
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-md-6 deal-type-section no-padding', __source: {
              fileName: _jsxFileName,
              lineNumber: 106
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { htmlFor: 'rahn', className: 'label font-medium input-label', __source: {
                fileName: _jsxFileName,
                lineNumber: 107
              },
              __self: this
            },
            '\u0631\u0647\u0646 \u0648 \u0627\u062C\u0627\u0631\u0647'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', { id: 'kharid',
            value: 1,
            onChange: e => {
              this.setState({
                [e.target.name]: e.target.checked ? 0 : 1
              });
            },
            className: 'white font-light',
            type: 'radio',
            name: 'dealType',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 108
            },
            __self: this
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { htmlFor: 'kharid', className: 'label font-medium input-label', __source: {
                fileName: _jsxFileName,
                lineNumber: 119
              },
              __self: this
            },
            '\u062E\u0631\u06CC\u062F'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', { id: 'rahn',
            className: 'white font-light',
            type: 'radio',
            value: 0,
            onChange: e => {
              this.setState({
                [e.target.name]: e.target.checked ? 1 : 0
              });
            },
            name: 'dealType',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 120
            },
            __self: this
          })
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'visible-xs visible-sm hidden-md hidden-lg col-xs-12', __source: {
              fileName: _jsxFileName,
              lineNumber: 132
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'form',
            { onSubmit: this.handleSubmit, __source: {
                fileName: _jsxFileName,
                lineNumber: 133
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'button',
              {
                type: 'submit',
                className: 'search-button has-transition border-radius clear-input white light-blue-background',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 134
                },
                __self: this
              },
              '\u062C\u0633\u062A\u062C\u0648'
            )
          )
        )
      )
    );
  }
}

SearchBox.propTypes = {
  handleSubmit: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__main_css___default.a)(SearchBox));

/***/ }),

/***/ "./src/components/SearchBox/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/config/compressionConfig.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// noinspection JSAnnotator
const levels = [{
  MAX_WIDTH: 600,
  MAX_HEIGHT: 600,
  DOCUMENT_SIZE: 300000
}, {
  MAX_WIDTH: 800,
  MAX_HEIGHT: 800,
  DOCUMENT_SIZE: 500000
}];

/* harmony default export */ __webpack_exports__["a"] = ({
  getLevel(level) {
    return levels[level - 1];
  }
});

/***/ }),

/***/ "./src/config/paths.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__urls__ = __webpack_require__("./src/config/urls.js");
/* eslint-disable prettier/prettier */


const loginApiUrl = `http://localhost:8080/Turtle/login`;
/* harmony export (immutable) */ __webpack_exports__["h"] = loginApiUrl;

const logoutApiUrl = `http://localhost:8080/Turtle/login`;
/* unused harmony export logoutApiUrl */

const getSearchHousesUrl = `http://localhost:8080/Turtle/SearchServlet`;
/* harmony export (immutable) */ __webpack_exports__["f"] = getSearchHousesUrl;

const getCreateHouseUrl = `http://localhost:8080/Turtle/api/HouseServlet`;
/* harmony export (immutable) */ __webpack_exports__["a"] = getCreateHouseUrl;

const getInitiateUrl = `http://localhost:8080/Turtle/home`;
/* harmony export (immutable) */ __webpack_exports__["e"] = getInitiateUrl;

const getCurrentUserUrl = userId => `http://localhost:8080/Turtle/api/IndividualServlet?userId=${userId}`;
/* harmony export (immutable) */ __webpack_exports__["b"] = getCurrentUserUrl;

const getHouseDetailUrl = houseId => `http://localhost:8080/Turtle/api/GetSingleHouse?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["c"] = getHouseDetailUrl;

const getHousePhoneUrl = houseId => `http://localhost:8080/Turtle/api/PaymentServlet?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["d"] = getHousePhoneUrl;

const increaseCreditUrl = amount => `http://localhost:8080/Turtle/api/CreditServlet?value=${amount}`;
/* harmony export (immutable) */ __webpack_exports__["g"] = increaseCreditUrl;


const searchDriverUrl = phoneNumber => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/phoneNumber/${phoneNumber}`;
/* unused harmony export searchDriverUrl */

const getDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export getDriverAcquisitionInfoUrl */

const updateDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export updateDriverAcquisitionInfoUrl */

const updateDriverDocsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export updateDriverDocsUrl */

const approveDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/approve/${driverId}`;
/* unused harmony export approveDriverUrl */

const rejectDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/reject/${driverId}`;
/* unused harmony export rejectDriverUrl */

const getDriverDocumentUrl = documentId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/document/${documentId}`;
/* unused harmony export getDriverDocumentUrl */

const getDriverDocumentIdsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export getDriverDocumentIdsUrl */


/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__ = __webpack_require__("history/createBrowserHistory");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__);
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */



// NavigationBar manager, e.g. history.push('/home')
// https://github.com/mjackson/history
/* harmony default export */ __webpack_exports__["a"] = (false && __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default()());

/***/ }),

/***/ "./src/routes/home/homeContainer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_search__ = __webpack_require__("./src/actions/search.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_HomePage__ = __webpack_require__("./src/components/HomePage/HomePage.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_Loader__ = __webpack_require__("./src/components/Loader/Loader.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/home/homeContainer.js';









class HomeContainer extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.handleSubmit = (minArea, dealType, buildingType, maxPrice) => {
      this.props.searchActions.searchHouses(minArea, dealType, buildingType, maxPrice);
    }, _temp;
  }

  componentDidMount() {
    this.props.authActions.initiate();
  }


  render() {
    console.log(this.props.currUser);
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_HomePage__["a" /* default */], {
        loading: this.props.isFetching,
        handleSubmit: this.handleSubmit,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_Loader__["a" /* default */], { loading: this.props.isFetching, __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      })
    );
  }
}

HomeContainer.propTypes = {
  isFetching: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.bool.isRequired,
  houses: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.array.isRequired,
  searchActions: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  currUser: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired
};
HomeContainer.defaultProps = {
  isFetching: false,
  loginError: null
};
function mapDispatchToProps(dispatch) {
  return {
    searchActions: Object(__WEBPACK_IMPORTED_MODULE_2_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_4__actions_search__, dispatch),
    authActions: Object(__WEBPACK_IMPORTED_MODULE_2_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { search } = state;
  const { authentication } = state;
  const { isFetching, houses } = search;
  const { currUser } = authentication;
  return {
    isFetching,
    houses,
    currUser
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(HomeContainer));

/***/ }),

/***/ "./src/routes/home/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_Layout__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__homeContainer__ = __webpack_require__("./src/routes/home/homeContainer.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/home/index.js';




const title = 'خانه';

function action() {
  return {
    chunks: ['home'],
    title,
    component: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_1_components_Layout__["a" /* default */],
      { mainPanel: true, __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__homeContainer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      })
    )
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/utils/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export compressFile */
/* harmony export (immutable) */ __webpack_exports__["c"] = getLoginInfo;
/* harmony export (immutable) */ __webpack_exports__["d"] = getPostConfig;
/* unused harmony export getPutConfig */
/* harmony export (immutable) */ __webpack_exports__["b"] = getGetConfig;
/* unused harmony export convertNumbersToPersian */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchApi; });
/* unused harmony export setImmediate */
/* unused harmony export setCookie */
/* unused harmony export getCookie */
/* unused harmony export eraseCookie */
/* harmony export (immutable) */ __webpack_exports__["e"] = notifyError;
/* harmony export (immutable) */ __webpack_exports__["f"] = notifySuccess;
/* unused harmony export reloadSelectPicker */
/* unused harmony export loadSelectPicker */
/* unused harmony export getFormData */
/* unused harmony export loadUserLoginStatus */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js__ = __webpack_require__("exif-js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_exif_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__ = __webpack_require__("./src/config/compressionConfig.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

let processFile = (() => {
  var _ref = _asyncToGenerator(function* (dataURL, fileSize, fileType, documentType) {
    const image = new Image();
    image.src = dataURL;
    return new Promise(function (resolve, reject) {
      image.onload = function () {
        const imageMeasurements = getImageMeasurements(image);
        if (!shouldResize(imageMeasurements, fileSize, documentType)) {
          resolve(dataURLtoBlob(dataURL, fileType));
        }
        const scaledImageMeasurements = getScaledImageMeasurements(imageMeasurements.width, imageMeasurements.height, documentType);
        const scaledImageFile = getScaledImageFileFromCanvas(image, scaledImageMeasurements.width, scaledImageMeasurements.height, imageMeasurements.transform, fileType);
        resolve(scaledImageFile);
      };
      image.onerror = reject;
    });
  });

  return function processFile(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }







const compressionFirstLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(1);
const compressionSecondLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(2);
const documentsCompressionLevel = {
  profilePicture: compressionFirstLevel,
  driverLicence: compressionFirstLevel,
  carIdCard: compressionFirstLevel,
  insurance: compressionSecondLevel
};

function getCompressionLevel(documentType) {
  return documentsCompressionLevel[documentType] || compressionFirstLevel;
}

function dataURLtoBlob(dataURL, fileType) {
  const binary = atob(dataURL.split(',')[1]);
  const array = [];
  const length = binary.length;
  let i;
  for (i = 0; i < length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: fileType });
}

function getScaledImageMeasurements(width, height, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH } = getCompressionLevel(documentType);

  if (width > height) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  } else {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
  return {
    height,
    width
  };
}

function getImageOrientation(image) {
  let orientation = 0;
  __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getData(image, function () {
    orientation = __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getTag(this, 'Orientation');
  });
  return orientation;
}

function getImageMeasurements(image) {
  const orientation = getImageOrientation(image);
  let transform;
  let swap = false;
  switch (orientation) {
    case 8:
      swap = true;
      transform = 'left';
      break;
    case 6:
      swap = true;
      transform = 'right';
      break;
    case 3:
      transform = 'flip';
      break;
    default:
  }
  const width = swap ? image.height : image.width;
  const height = swap ? image.width : image.height;
  return {
    width,
    height,
    transform
  };
}

function getScaledImageFileFromCanvas(image, width, height, transform, fileType) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const context = canvas.getContext('2d');
  let transformParams;
  let swapMeasure = false;
  let imageWidth = width;
  let imageHeight = height;
  switch (transform) {
    case 'left':
      transformParams = [0, -1, 1, 0, 0, height];
      swapMeasure = true;
      break;
    case 'right':
      transformParams = [0, 1, -1, 0, width, 0];
      swapMeasure = true;
      break;
    case 'flip':
      transformParams = [1, 0, 0, -1, 0, height];
      break;
    default:
      transformParams = [1, 0, 0, 1, 0, 0];
  }
  context.setTransform(...transformParams);
  if (swapMeasure) {
    imageHeight = width;
    imageWidth = height;
  }
  context.drawImage(image, 0, 0, imageWidth, imageHeight);
  context.setTransform(1, 0, 0, 1, 0, 0);
  const dataURL = canvas.toDataURL(fileType);
  return dataURLtoBlob(dataURL, fileType);
}

function shouldResize(imageMeasurements, fileSize, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH, DOCUMENT_SIZE } = getCompressionLevel(documentType);
  if (documentType === 'insurance' && fileSize < DOCUMENT_SIZE) {
    return false;
  } else if (fileSize < DOCUMENT_SIZE) {
    return false;
  }
  return imageMeasurements.width > MAX_WIDTH || imageMeasurements.height > MAX_HEIGHT;
}

function compressFile(file, documentType = 'default') {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = _asyncToGenerator(function* () {
        const compressedFile = yield processFile(reader.result, file.size, file.type, documentType);
        resolve(compressedFile);
      });
    } catch (err) {
      reject(err);
    }
  });
}

function getLoginInfo() {
  try {
    const loginInfo = localStorage.getItem('loginInfo');
    if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isString(loginInfo)) {
      return JSON.parse(loginInfo);
    }
  } catch (err) {
    console.log('Error when parsing loginInfo object: ', err);
  }
  return null;
}
function getPostConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    mimeType: 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json', mimeType: 'application/json' };
  return _extends({
    method: 'POST',
    // mode: 'no-cors',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getPutConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    'X-Authorization': loginInfo.token
  } : { 'Content-Type': 'application/json' };
  return _extends({
    method: 'PUT',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getGetConfig() {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json' };
  return {
    method: 'GET',
    headers
  };
}

function convertNumbersToPersian(str) {
  const persianNumbers = '۰۱۲۳۴۵۶۷۸۹';
  let result = '';
  if (!str && str !== 0) {
    return result;
  }
  str = str.toString();
  let convertedChar;
  for (let i = 0; i < str.length; i++) {
    try {
      convertedChar = persianNumbers[str[i]];
    } catch (err) {
      console.log(err);
      convertedChar = str[i];
    }
    result += convertedChar;
  }
  return result;
}

let fetchApi = (() => {
  var _ref3 = _asyncToGenerator(function* (url, config) {
    let flowError;
    try {
      const res = yield fetch(url, config);
      console.log(res);
      console.log(res.status);
      let result = yield res.json();
      console.log(result);
      if (!__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.get(result, 'data'))) {
        result = result.data;
      }
      console.log(res.status);

      if (res.status !== 200) {
        flowError = result;
        console.log(res.status);

        if (res.status === 403) {
          localStorage.clear();
          flowError.message = 'لطفا دوباره وارد شوید!';
          console.log('yeees');
          setTimeout(function () {
            return __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push('/login');
          }, 3000);
        }
      } else {
        return result;
      }
    } catch (err) {
      console.log(err);
      console.debug(`Error when fetching api: ${url}`, err);
      flowError = {
        code: 'UNKNOWN_ERROR',
        message: 'خطای نامشخص لطفا دوباره سعی کنید'
      };
    }
    if (flowError) {
      throw flowError;
    }
  });

  return function fetchApi(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
})();

let setImmediate = (() => {
  var _ref4 = _asyncToGenerator(function* (fn) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        let value = null;
        if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isFunction(fn)) {
          value = fn();
        }
        resolve(value);
      }, 0);
    });
  });

  return function setImmediate(_x7) {
    return _ref4.apply(this, arguments);
  };
})();

function setCookie(name, value, days = 7) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
function getCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  const length = ca.length;
  let i = 0;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

function notifyError(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '10000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.error(message, 'خطا');
}

function notifySuccess(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.success(message, '');
}

function reloadSelectPicker() {
  try {
    $('.selectpicker').selectpicker('render');
  } catch (err) {
    console.log(err);
  }
}

function loadSelectPicker() {
  try {
    $('.selectpicker').selectpicker({});
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      $('.selectpicker').selectpicker('mobile');
    }
  } catch (err) {
    console.log(err);
  }
}

function getFormData(data) {
  if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(data)) {
    throw Error('Invalid input data to create a FormData object');
  }
  let formData;
  try {
    formData = new FormData();
    const keys = __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.keys(data);
    let i;
    const length = keys.length;
    for (i = 0; i < length; i++) {
      formData.append(keys[i], data[keys[i]]);
    }
    return formData;
  } catch (err) {
    console.debug(err);
    throw Error('FORM_DATA_BROWSER_SUPPORT_FAILURE');
  }
}

function loadUserLoginStatus(store) {
  const loginStatusAction = Object(__WEBPACK_IMPORTED_MODULE_4__actions_authentication__["loadLoginStatus"])();
  store.dispatch(loginStatusAction);
}

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL2hvbWUuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS5jc3Mvbm9ybWFsaXplLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuc2NzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL1NlYXJjaEJveC9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL3VybC9lc2NhcGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS5jc3Mvbm9ybWFsaXplLmNzcz84MjFiIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYWN0aW9ucy9hdXRoZW50aWNhdGlvbi5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2FjdGlvbnMvc2VhcmNoLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYXBpLWhhbmRsZXJzL3NlYXJjaC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyL0Zvb3Rlci5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3M/OWI0ZSIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvNzI2NDQ2LnN2ZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvNzI2NDg4LnN2ZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvNzI2NDk5LnN2ZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvSG9tZVBhZ2UuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL2Nhc2V5LWhvcm5lci01MzM1ODYtdW5zcGxhc2guanBnIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9sb2dvLnBuZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbHVrZS12YW4tenlsLTUwNDAzMi11bnNwbGFzaC5qcGciLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL21haGRpYXItbWFobW9vZGktNDUyNDg5LXVuc3BsYXNoLmpwZyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9tYWluLmNzcz83NzBkIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9taWNoYWwta3ViYWxjenlrLTI2MDkwOS11bnNwbGFzaC5qcGciLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL3doeS1raGFuZWJlZG9vc2guanBnIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzPzUwNzkiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzPzY1NjUiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9sb2FkZXIuZ2lmIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9QYW5lbC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3M/MWNmNCIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L1NlYXJjaEJveC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9TZWFyY2hCb3gvbWFpbi5jc3M/NDYwYSIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbmZpZy9jb21wcmVzc2lvbkNvbmZpZy5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbmZpZy9wYXRocy5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2hpc3RvcnkuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9yb3V0ZXMvaG9tZS9ob21lQ29udGFpbmVyLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvcm91dGVzL2hvbWUvaW5kZXguanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy91dGlscy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIvKiEgbm9ybWFsaXplLmNzcyB2Ny4wLjAgfCBNSVQgTGljZW5zZSB8IGdpdGh1Yi5jb20vbmVjb2xhcy9ub3JtYWxpemUuY3NzICovXFxuLyogRG9jdW1lbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGxpbmUgaGVpZ2h0IGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBQcmV2ZW50IGFkanVzdG1lbnRzIG9mIGZvbnQgc2l6ZSBhZnRlciBvcmllbnRhdGlvbiBjaGFuZ2VzIGluXFxuICogICAgSUUgb24gV2luZG93cyBQaG9uZSBhbmQgaW4gaU9TLlxcbiAqL1xcbmh0bWwge1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICAtbXMtdGV4dC1zaXplLWFkanVzdDogMTAwJTtcXG4gIC8qIDIgKi9cXG4gIC13ZWJraXQtdGV4dC1zaXplLWFkanVzdDogMTAwJTtcXG4gIC8qIDIgKi8gfVxcbi8qIFNlY3Rpb25zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBSZW1vdmUgdGhlIG1hcmdpbiBpbiBhbGwgYnJvd3NlcnMgKG9waW5pb25hdGVkKS5cXG4gKi9cXG5ib2R5IHtcXG4gIG1hcmdpbjogMDsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmFydGljbGUsXFxuYXNpZGUsXFxuZm9vdGVyLFxcbmhlYWRlcixcXG5uYXYsXFxuc2VjdGlvbiB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIGZvbnQgc2l6ZSBhbmQgbWFyZ2luIG9uIGBoMWAgZWxlbWVudHMgd2l0aGluIGBzZWN0aW9uYCBhbmRcXG4gKiBgYXJ0aWNsZWAgY29udGV4dHMgaW4gQ2hyb21lLCBGaXJlZm94LCBhbmQgU2FmYXJpLlxcbiAqL1xcbmgxIHtcXG4gIGZvbnQtc2l6ZTogMmVtO1xcbiAgbWFyZ2luOiAwLjY3ZW0gMDsgfVxcbi8qIEdyb3VwaW5nIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFLlxcbiAqL1xcbmZpZ2NhcHRpb24sXFxuZmlndXJlLFxcbm1haW4ge1xcbiAgLyogMSAqL1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgbWFyZ2luIGluIElFIDguXFxuICovXFxuZmlndXJlIHtcXG4gIG1hcmdpbjogMWVtIDQwcHg7IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBGaXJlZm94LlxcbiAqIDIuIFNob3cgdGhlIG92ZXJmbG93IGluIEVkZ2UgYW5kIElFLlxcbiAqL1xcbmhyIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xcbiAgLyogMSAqL1xcbiAgaGVpZ2h0OiAwO1xcbiAgLyogMSAqL1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmhlcml0YW5jZSBhbmQgc2NhbGluZyBvZiBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIENvcnJlY3QgdGhlIG9kZCBgZW1gIGZvbnQgc2l6aW5nIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5wcmUge1xcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZSwgbW9ub3NwYWNlO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiAyICovIH1cXG4vKiBUZXh0LWxldmVsIHNlbWFudGljc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBncmF5IGJhY2tncm91bmQgb24gYWN0aXZlIGxpbmtzIGluIElFIDEwLlxcbiAqIDIuIFJlbW92ZSBnYXBzIGluIGxpbmtzIHVuZGVybGluZSBpbiBpT1MgOCsgYW5kIFNhZmFyaSA4Ky5cXG4gKi9cXG5hIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgLyogMSAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb24tc2tpcDogb2JqZWN0cztcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIFJlbW92ZSB0aGUgYm90dG9tIGJvcmRlciBpbiBDaHJvbWUgNTctIGFuZCBGaXJlZm94IDM5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdGV4dCBkZWNvcmF0aW9uIGluIENocm9tZSwgRWRnZSwgSUUsIE9wZXJhLCBhbmQgU2FmYXJpLlxcbiAqL1xcbmFiYnJbdGl0bGVdIHtcXG4gIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAvKiAxICovXFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIC8qIDIgKi9cXG4gIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgZG90dGVkO1xcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBQcmV2ZW50IHRoZSBkdXBsaWNhdGUgYXBwbGljYXRpb24gb2YgYGJvbGRlcmAgYnkgdGhlIG5leHQgcnVsZSBpbiBTYWZhcmkgNi5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogaW5oZXJpdDsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHdlaWdodCBpbiBDaHJvbWUsIEVkZ2UsIGFuZCBTYWZhcmkuXFxuICovXFxuYixcXG5zdHJvbmcge1xcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmNvZGUsXFxua2JkLFxcbnNhbXAge1xcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZSwgbW9ub3NwYWNlO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCBzdHlsZSBpbiBBbmRyb2lkIDQuMy0uXFxuICovXFxuZGZuIHtcXG4gIGZvbnQtc3R5bGU6IGl0YWxpYzsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBiYWNrZ3JvdW5kIGFuZCBjb2xvciBpbiBJRSA5LS5cXG4gKi9cXG5tYXJrIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjA7XFxuICBjb2xvcjogIzAwMDsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnNtYWxsIHtcXG4gIGZvbnQtc2l6ZTogODAlOyB9XFxuLyoqXFxuICogUHJldmVudCBgc3ViYCBhbmQgYHN1cGAgZWxlbWVudHMgZnJvbSBhZmZlY3RpbmcgdGhlIGxpbmUgaGVpZ2h0IGluXFxuICogYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnN1YixcXG5zdXAge1xcbiAgZm9udC1zaXplOiA3NSU7XFxuICBsaW5lLWhlaWdodDogMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTsgfVxcbnN1YiB7XFxuICBib3R0b206IC0wLjI1ZW07IH1cXG5zdXAge1xcbiAgdG9wOiAtMC41ZW07IH1cXG4vKiBFbWJlZGRlZCBjb250ZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5hdWRpbyxcXG52aWRlbyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBpT1MgNC03LlxcbiAqL1xcbmF1ZGlvOm5vdChbY29udHJvbHNdKSB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgaGVpZ2h0OiAwOyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBib3JkZXIgb24gaW1hZ2VzIGluc2lkZSBsaW5rcyBpbiBJRSAxMC0uXFxuICovXFxuaW1nIHtcXG4gIGJvcmRlci1zdHlsZTogbm9uZTsgfVxcbi8qKlxcbiAqIEhpZGUgdGhlIG92ZXJmbG93IGluIElFLlxcbiAqL1xcbnN2Zzpub3QoOnJvb3QpIHtcXG4gIG92ZXJmbG93OiBoaWRkZW47IH1cXG4vKiBGb3Jtc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ2hhbmdlIHRoZSBmb250IHN0eWxlcyBpbiBhbGwgYnJvd3NlcnMgKG9waW5pb25hdGVkKS5cXG4gKiAyLiBSZW1vdmUgdGhlIG1hcmdpbiBpbiBGaXJlZm94IGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmlucHV0LFxcbm9wdGdyb3VwLFxcbnNlbGVjdCxcXG50ZXh0YXJlYSB7XFxuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMTAwJTtcXG4gIC8qIDEgKi9cXG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xcbiAgLyogMSAqL1xcbiAgbWFyZ2luOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogU2hvdyB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICogMS4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQge1xcbiAgLyogMSAqL1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGluaGVyaXRhbmNlIG9mIHRleHQgdHJhbnNmb3JtIGluIEVkZ2UsIEZpcmVmb3gsIGFuZCBJRS5cXG4gKiAxLiBSZW1vdmUgdGhlIGluaGVyaXRhbmNlIG9mIHRleHQgdHJhbnNmb3JtIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uLFxcbnNlbGVjdCB7XFxuICAvKiAxICovXFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTsgfVxcbi8qKlxcbiAqIDEuIFByZXZlbnQgYSBXZWJLaXQgYnVnIHdoZXJlICgyKSBkZXN0cm95cyBuYXRpdmUgYGF1ZGlvYCBhbmQgYHZpZGVvYFxcbiAqICAgIGNvbnRyb2xzIGluIEFuZHJvaWQgNC5cXG4gKiAyLiBDb3JyZWN0IHRoZSBpbmFiaWxpdHkgdG8gc3R5bGUgY2xpY2thYmxlIHR5cGVzIGluIGlPUyBhbmQgU2FmYXJpLlxcbiAqL1xcbmJ1dHRvbixcXG5odG1sIFt0eXBlPVxcXCJidXR0b25cXFwiXSxcXG5bdHlwZT1cXFwicmVzZXRcXFwiXSxcXG5bdHlwZT1cXFwic3VibWl0XFxcIl0ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIGJvcmRlciBhbmQgcGFkZGluZyBpbiBGaXJlZm94LlxcbiAqL1xcbmJ1dHRvbjo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcInJlc2V0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIGJvcmRlci1zdHlsZTogbm9uZTtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKipcXG4gKiBSZXN0b3JlIHRoZSBmb2N1cyBzdHlsZXMgdW5zZXQgYnkgdGhlIHByZXZpb3VzIHJ1bGUuXFxuICovXFxuYnV0dG9uOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06LW1vei1mb2N1c3Jpbmcge1xcbiAgb3V0bGluZTogMXB4IGRvdHRlZCBCdXR0b25UZXh0OyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgcGFkZGluZyBpbiBGaXJlZm94LlxcbiAqL1xcbmZpZWxkc2V0IHtcXG4gIHBhZGRpbmc6IDAuMzVlbSAwLjc1ZW0gMC42MjVlbTsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIHRleHQgd3JhcHBpbmcgaW4gRWRnZSBhbmQgSUUuXFxuICogMi4gQ29ycmVjdCB0aGUgY29sb3IgaW5oZXJpdGFuY2UgZnJvbSBgZmllbGRzZXRgIGVsZW1lbnRzIGluIElFLlxcbiAqIDMuIFJlbW92ZSB0aGUgcGFkZGluZyBzbyBkZXZlbG9wZXJzIGFyZSBub3QgY2F1Z2h0IG91dCB3aGVuIHRoZXkgemVybyBvdXRcXG4gKiAgICBgZmllbGRzZXRgIGVsZW1lbnRzIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5sZWdlbmQge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgLyogMSAqL1xcbiAgY29sb3I6IGluaGVyaXQ7XFxuICAvKiAyICovXFxuICBkaXNwbGF5OiB0YWJsZTtcXG4gIC8qIDEgKi9cXG4gIG1heC13aWR0aDogMTAwJTtcXG4gIC8qIDEgKi9cXG4gIHBhZGRpbmc6IDA7XFxuICAvKiAzICovXFxuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xcbiAgLyogMSAqLyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMi4gQWRkIHRoZSBjb3JyZWN0IHZlcnRpY2FsIGFsaWdubWVudCBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBPcGVyYS5cXG4gKi9cXG5wcm9ncmVzcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICAvKiAxICovXFxuICB2ZXJ0aWNhbC1hbGlnbjogYmFzZWxpbmU7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGRlZmF1bHQgdmVydGljYWwgc2Nyb2xsYmFyIGluIElFLlxcbiAqL1xcbnRleHRhcmVhIHtcXG4gIG92ZXJmbG93OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGJveCBzaXppbmcgaW4gSUUgMTAtLlxcbiAqIDIuIFJlbW92ZSB0aGUgcGFkZGluZyBpbiBJRSAxMC0uXFxuICovXFxuW3R5cGU9XFxcImNoZWNrYm94XFxcIl0sXFxuW3R5cGU9XFxcInJhZGlvXFxcIl0ge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIGN1cnNvciBzdHlsZSBvZiBpbmNyZW1lbnQgYW5kIGRlY3JlbWVudCBidXR0b25zIGluIENocm9tZS5cXG4gKi9cXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24sXFxuW3R5cGU9XFxcIm51bWJlclxcXCJdOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uIHtcXG4gIGhlaWdodDogYXV0bzsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIG9kZCBhcHBlYXJhbmNlIGluIENocm9tZSBhbmQgU2FmYXJpLlxcbiAqIDIuIENvcnJlY3QgdGhlIG91dGxpbmUgc3R5bGUgaW4gU2FmYXJpLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IHRleHRmaWVsZDtcXG4gIC8qIDEgKi9cXG4gIG91dGxpbmUtb2Zmc2V0OiAtMnB4O1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbm5lciBwYWRkaW5nIGFuZCBjYW5jZWwgYnV0dG9ucyBpbiBDaHJvbWUgYW5kIFNhZmFyaSBvbiBtYWNPUy5cXG4gKi9cXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWNhbmNlbC1idXR0b24sXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdOjotd2Via2l0LXNlYXJjaC1kZWNvcmF0aW9uIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICogMi4gQ2hhbmdlIGZvbnQgcHJvcGVydGllcyB0byBgaW5oZXJpdGAgaW4gU2FmYXJpLlxcbiAqL1xcbjo6LXdlYmtpdC1maWxlLXVwbG9hZC1idXR0b24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XFxuICAvKiAxICovXFxuICBmb250OiBpbmhlcml0O1xcbiAgLyogMiAqLyB9XFxuLyogSW50ZXJhY3RpdmVcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gRWRnZSwgSUUsIGFuZCBGaXJlZm94LlxcbiAqL1xcbmRldGFpbHMsXFxubWVudSB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnN1bW1hcnkge1xcbiAgZGlzcGxheTogbGlzdC1pdGVtOyB9XFxuLyogU2NyaXB0aW5nXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5jYW52YXMge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxudGVtcGxhdGUge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcbi8qIEhpZGRlblxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgMTAtLlxcbiAqL1xcbltoaWRkZW5dIHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBLDRFQUE0RTtBQUM1RTtnRkFDZ0Y7QUFDaEY7Ozs7R0FJRztBQUNIO0VBQ0Usa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLCtCQUErQjtFQUMvQixPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDtFQUNFLFVBQVUsRUFBRTtBQUNkOztHQUVHO0FBQ0g7Ozs7OztFQU1FLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDtFQUNFLGVBQWU7RUFDZixpQkFBaUIsRUFBRTtBQUNyQjtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7OztFQUdFLE9BQU87RUFDUCxlQUFlLEVBQUU7QUFDbkI7O0dBRUc7QUFDSDtFQUNFLGlCQUFpQixFQUFFO0FBQ3JCOzs7R0FHRztBQUNIO0VBQ0UsZ0NBQWdDO1VBQ3hCLHdCQUF3QjtFQUNoQyxPQUFPO0VBQ1AsVUFBVTtFQUNWLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsT0FBTyxFQUFFO0FBQ1g7OztHQUdHO0FBQ0g7RUFDRSxrQ0FBa0M7RUFDbEMsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7RUFDRSw4QkFBOEI7RUFDOUIsT0FBTztFQUNQLHNDQUFzQztFQUN0QyxPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDtFQUNFLG9CQUFvQjtFQUNwQixPQUFPO0VBQ1AsMkJBQTJCO0VBQzNCLE9BQU87RUFDUCwwQ0FBMEM7VUFDbEMsa0NBQWtDO0VBQzFDLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7O0VBRUUscUJBQXFCLEVBQUU7QUFDekI7O0dBRUc7QUFDSDs7RUFFRSxvQkFBb0IsRUFBRTtBQUN4Qjs7O0dBR0c7QUFDSDs7O0VBR0Usa0NBQWtDO0VBQ2xDLE9BQU87RUFDUCxlQUFlO0VBQ2YsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCOztHQUVHO0FBQ0g7RUFDRSx1QkFBdUI7RUFDdkIsWUFBWSxFQUFFO0FBQ2hCOztHQUVHO0FBQ0g7RUFDRSxlQUFlLEVBQUU7QUFDbkI7OztHQUdHO0FBQ0g7O0VBRUUsZUFBZTtFQUNmLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIseUJBQXlCLEVBQUU7QUFDN0I7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLFlBQVksRUFBRTtBQUNoQjtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDs7RUFFRSxzQkFBc0IsRUFBRTtBQUMxQjs7R0FFRztBQUNIO0VBQ0UsY0FBYztFQUNkLFVBQVUsRUFBRTtBQUNkOztHQUVHO0FBQ0g7RUFDRSxtQkFBbUIsRUFBRTtBQUN2Qjs7R0FFRztBQUNIO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7Z0ZBQ2dGO0FBQ2hGOzs7R0FHRztBQUNIOzs7OztFQUtFLHdCQUF3QjtFQUN4QixPQUFPO0VBQ1AsZ0JBQWdCO0VBQ2hCLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLFVBQVU7RUFDVixPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDs7RUFFRSxPQUFPO0VBQ1Asa0JBQWtCLEVBQUU7QUFDdEI7OztHQUdHO0FBQ0g7O0VBRUUsT0FBTztFQUNQLHFCQUFxQixFQUFFO0FBQ3pCOzs7O0dBSUc7QUFDSDs7OztFQUlFLDJCQUEyQjtFQUMzQixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOzs7O0VBSUUsbUJBQW1CO0VBQ25CLFdBQVcsRUFBRTtBQUNmOztHQUVHO0FBQ0g7Ozs7RUFJRSwrQkFBK0IsRUFBRTtBQUNuQzs7R0FFRztBQUNIO0VBQ0UsK0JBQStCLEVBQUU7QUFDbkM7Ozs7O0dBS0c7QUFDSDtFQUNFLCtCQUErQjtVQUN2Qix1QkFBdUI7RUFDL0IsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLFdBQVc7RUFDWCxPQUFPO0VBQ1Asb0JBQW9CO0VBQ3BCLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIO0VBQ0Usc0JBQXNCO0VBQ3RCLE9BQU87RUFDUCx5QkFBeUI7RUFDekIsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDs7RUFFRSwrQkFBK0I7VUFDdkIsdUJBQXVCO0VBQy9CLE9BQU87RUFDUCxXQUFXO0VBQ1gsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDs7RUFFRSxhQUFhLEVBQUU7QUFDakI7OztHQUdHO0FBQ0g7RUFDRSw4QkFBOEI7RUFDOUIsT0FBTztFQUNQLHFCQUFxQjtFQUNyQixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOztFQUVFLHlCQUF5QixFQUFFO0FBQzdCOzs7R0FHRztBQUNIO0VBQ0UsMkJBQTJCO0VBQzNCLE9BQU87RUFDUCxjQUFjO0VBQ2QsT0FBTyxFQUFFO0FBQ1g7Z0ZBQ2dGO0FBQ2hGOzs7R0FHRztBQUNIOztFQUVFLGVBQWUsRUFBRTtBQUNuQjs7R0FFRztBQUNIO0VBQ0UsbUJBQW1CLEVBQUU7QUFDdkI7Z0ZBQ2dGO0FBQ2hGOztHQUVHO0FBQ0g7RUFDRSxzQkFBc0IsRUFBRTtBQUMxQjs7R0FFRztBQUNIO0VBQ0UsY0FBYyxFQUFFO0FBQ2xCO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIO0VBQ0UsY0FBYyxFQUFFXCIsXCJmaWxlXCI6XCJub3JtYWxpemUuY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi8qISBub3JtYWxpemUuY3NzIHY3LjAuMCB8IE1JVCBMaWNlbnNlIHwgZ2l0aHViLmNvbS9uZWNvbGFzL25vcm1hbGl6ZS5jc3MgKi9cXG4vKiBEb2N1bWVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgbGluZSBoZWlnaHQgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIFByZXZlbnQgYWRqdXN0bWVudHMgb2YgZm9udCBzaXplIGFmdGVyIG9yaWVudGF0aW9uIGNoYW5nZXMgaW5cXG4gKiAgICBJRSBvbiBXaW5kb3dzIFBob25lIGFuZCBpbiBpT1MuXFxuICovXFxuaHRtbCB7XFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIC1tcy10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqLyB9XFxuLyogU2VjdGlvbnNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIFJlbW92ZSB0aGUgbWFyZ2luIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqL1xcbmJvZHkge1xcbiAgbWFyZ2luOiAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXJ0aWNsZSxcXG5hc2lkZSxcXG5mb290ZXIsXFxuaGVhZGVyLFxcbm5hdixcXG5zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgZm9udCBzaXplIGFuZCBtYXJnaW4gb24gYGgxYCBlbGVtZW50cyB3aXRoaW4gYHNlY3Rpb25gIGFuZFxcbiAqIGBhcnRpY2xlYCBjb250ZXh0cyBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBTYWZhcmkuXFxuICovXFxuaDEge1xcbiAgZm9udC1zaXplOiAyZW07XFxuICBtYXJnaW46IDAuNjdlbSAwOyB9XFxuLyogR3JvdXBpbmcgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxuZmlnY2FwdGlvbixcXG5maWd1cmUsXFxubWFpbiB7XFxuICAvKiAxICovXFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBtYXJnaW4gaW4gSUUgOC5cXG4gKi9cXG5maWd1cmUge1xcbiAgbWFyZ2luOiAxZW0gNDBweDsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIEZpcmVmb3guXFxuICogMi4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZSBhbmQgSUUuXFxuICovXFxuaHIge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAvKiAxICovXFxuICBoZWlnaHQ6IDA7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnByZSB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qIFRleHQtbGV2ZWwgc2VtYW50aWNzXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGdyYXkgYmFja2dyb3VuZCBvbiBhY3RpdmUgbGlua3MgaW4gSUUgMTAuXFxuICogMi4gUmVtb3ZlIGdhcHMgaW4gbGlua3MgdW5kZXJsaW5lIGluIGlPUyA4KyBhbmQgU2FmYXJpIDgrLlxcbiAqL1xcbmEge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAvKiAxICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbi1za2lwOiBvYmplY3RzO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBib3R0b20gYm9yZGVyIGluIENocm9tZSA1Ny0gYW5kIEZpcmVmb3ggMzktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB0ZXh0IGRlY29yYXRpb24gaW4gQ2hyb21lLCBFZGdlLCBJRSwgT3BlcmEsIGFuZCBTYWZhcmkuXFxuICovXFxuYWJiclt0aXRsZV0ge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gIC8qIDEgKi9cXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFByZXZlbnQgdGhlIGR1cGxpY2F0ZSBhcHBsaWNhdGlvbiBvZiBgYm9sZGVyYCBieSB0aGUgbmV4dCBydWxlIGluIFNhZmFyaSA2LlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBpbmhlcml0OyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgd2VpZ2h0IGluIENocm9tZSwgRWRnZSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogYm9sZGVyOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuY29kZSxcXG5rYmQsXFxuc2FtcCB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHN0eWxlIGluIEFuZHJvaWQgNC4zLS5cXG4gKi9cXG5kZm4ge1xcbiAgZm9udC1zdHlsZTogaXRhbGljOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGJhY2tncm91bmQgYW5kIGNvbG9yIGluIElFIDktLlxcbiAqL1xcbm1hcmsge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmMDtcXG4gIGNvbG9yOiAjMDAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc21hbGwge1xcbiAgZm9udC1zaXplOiA4MCU7IH1cXG4vKipcXG4gKiBQcmV2ZW50IGBzdWJgIGFuZCBgc3VwYCBlbGVtZW50cyBmcm9tIGFmZmVjdGluZyB0aGUgbGluZSBoZWlnaHQgaW5cXG4gKiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3ViLFxcbnN1cCB7XFxuICBmb250LXNpemU6IDc1JTtcXG4gIGxpbmUtaGVpZ2h0OiAwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lOyB9XFxuc3ViIHtcXG4gIGJvdHRvbTogLTAuMjVlbTsgfVxcbnN1cCB7XFxuICB0b3A6IC0wLjVlbTsgfVxcbi8qIEVtYmVkZGVkIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmF1ZGlvLFxcbnZpZGVvIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGlPUyA0LTcuXFxuICovXFxuYXVkaW86bm90KFtjb250cm9sc10pIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBoZWlnaHQ6IDA7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGJvcmRlciBvbiBpbWFnZXMgaW5zaWRlIGxpbmtzIGluIElFIDEwLS5cXG4gKi9cXG5pbWcge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lOyB9XFxuLyoqXFxuICogSGlkZSB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICovXFxuc3ZnOm5vdCg6cm9vdCkge1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjsgfVxcbi8qIEZvcm1zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDaGFuZ2UgdGhlIGZvbnQgc3R5bGVzIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqIDIuIFJlbW92ZSB0aGUgbWFyZ2luIGluIEZpcmVmb3ggYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQsXFxub3B0Z3JvdXAsXFxuc2VsZWN0LFxcbnRleHRhcmVhIHtcXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICBtYXJnaW46IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBTaG93IHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKiAxLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCB7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRWRnZSwgRmlyZWZveCwgYW5kIElFLlxcbiAqIDEuIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b24sXFxuc2VsZWN0IHtcXG4gIC8qIDEgKi9cXG4gIHRleHQtdHJhbnNmb3JtOiBub25lOyB9XFxuLyoqXFxuICogMS4gUHJldmVudCBhIFdlYktpdCBidWcgd2hlcmUgKDIpIGRlc3Ryb3lzIG5hdGl2ZSBgYXVkaW9gIGFuZCBgdmlkZW9gXFxuICogICAgY29udHJvbHMgaW4gQW5kcm9pZCA0LlxcbiAqIDIuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmh0bWwgW3R5cGU9XFxcImJ1dHRvblxcXCJdLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgYm9yZGVyIGFuZCBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qKlxcbiAqIFJlc3RvcmUgdGhlIGZvY3VzIHN0eWxlcyB1bnNldCBieSB0aGUgcHJldmlvdXMgcnVsZS5cXG4gKi9cXG5idXR0b246LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTotbW96LWZvY3VzcmluZyB7XFxuICBvdXRsaW5lOiAxcHggZG90dGVkIEJ1dHRvblRleHQ7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuZmllbGRzZXQge1xcbiAgcGFkZGluZzogMC4zNWVtIDAuNzVlbSAwLjYyNWVtOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgdGV4dCB3cmFwcGluZyBpbiBFZGdlIGFuZCBJRS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBjb2xvciBpbmhlcml0YW5jZSBmcm9tIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gSUUuXFxuICogMy4gUmVtb3ZlIHRoZSBwYWRkaW5nIHNvIGRldmVsb3BlcnMgYXJlIG5vdCBjYXVnaHQgb3V0IHdoZW4gdGhleSB6ZXJvIG91dFxcbiAqICAgIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmxlZ2VuZCB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBjb2xvcjogaW5oZXJpdDtcXG4gIC8qIDIgKi9cXG4gIGRpc3BsYXk6IHRhYmxlO1xcbiAgLyogMSAqL1xcbiAgbWF4LXdpZHRoOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDMgKi9cXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XFxuICAvKiAxICovIH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdmVydGljYWwgYWxpZ25tZW50IGluIENocm9tZSwgRmlyZWZveCwgYW5kIE9wZXJhLlxcbiAqL1xcbnByb2dyZXNzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIC8qIDEgKi9cXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgZGVmYXVsdCB2ZXJ0aWNhbCBzY3JvbGxiYXIgaW4gSUUuXFxuICovXFxudGV4dGFyZWEge1xcbiAgb3ZlcmZsb3c6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBJRSAxMC0uXFxuICogMi4gUmVtb3ZlIHRoZSBwYWRkaW5nIGluIElFIDEwLS5cXG4gKi9cXG5bdHlwZT1cXFwiY2hlY2tib3hcXFwiXSxcXG5bdHlwZT1cXFwicmFkaW9cXFwiXSB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgY3Vyc29yIHN0eWxlIG9mIGluY3JlbWVudCBhbmQgZGVjcmVtZW50IGJ1dHRvbnMgaW4gQ2hyb21lLlxcbiAqL1xcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixcXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xcbiAgaGVpZ2h0OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgb2RkIGFwcGVhcmFuY2UgaW4gQ2hyb21lIGFuZCBTYWZhcmkuXFxuICogMi4gQ29ycmVjdCB0aGUgb3V0bGluZSBzdHlsZSBpbiBTYWZhcmkuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4dGZpZWxkO1xcbiAgLyogMSAqL1xcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIHBhZGRpbmcgYW5kIGNhbmNlbCBidXR0b25zIGluIENocm9tZSBhbmQgU2FmYXJpIG9uIG1hY09TLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtY2FuY2VsLWJ1dHRvbixcXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKiAyLiBDaGFuZ2UgZm9udCBwcm9wZXJ0aWVzIHRvIGBpbmhlcml0YCBpbiBTYWZhcmkuXFxuICovXFxuOjotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQ6IGluaGVyaXQ7XFxuICAvKiAyICovIH1cXG4vKiBJbnRlcmFjdGl2ZVxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBFZGdlLCBJRSwgYW5kIEZpcmVmb3guXFxuICovXFxuZGV0YWlscyxcXG5tZW51IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3VtbWFyeSB7XFxuICBkaXNwbGF5OiBsaXN0LWl0ZW07IH1cXG4vKiBTY3JpcHRpbmdcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmNhbnZhcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG50ZW1wbGF0ZSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuLyogSGlkZGVuXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSAxMC0uXFxuICovXFxuW2hpZGRlbl0ge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zb2NpYWwtbmV0d29yay1pY29uIHtcXG4gIG1heC13aWR0aDogMjBweDsgfVxcbi5zaXRlLWZvb3RlciB7XFxuICBwYWRkaW5nOiAxJSA0JTtcXG4gIGhlaWdodDogOHZoO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc29jaWFsLWljb25zLWxpc3QgPiBsaSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuc2l0ZS1mb290ZXIge1xcbiAgICBoZWlnaHQ6IDEzdmg7IH0gfVxcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLnNpdGUtZm9vdGVyID4gLnJvdyA+IC5jb2wteHMtMTI6Zmlyc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0lBQ0kscUJBQXFCO0NBQ3hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxjQUFjO0NBQ2pCO0FBQ0Q7SUFDSSx5QkFBeUI7Q0FDNUI7QUFDRDtJQUNJLHdCQUF3QjtDQUMzQjtBQUNEO0lBQ0ksMEJBQTBCO0NBQzdCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQywwQkFBMEI7UUFDdEIsdUJBQXVCO1lBQ25CLG9CQUFvQjtDQUMvQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsNkJBQTZCO0lBQzdCLDhCQUE4QjtRQUMxQiwyQkFBMkI7WUFDdkIsdUJBQXVCO0NBQ2xDO0FBQ0Q7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGNBQWM7SUFDZCxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGdEQUFnRDtJQUNoRCx3REFBd0Q7SUFDeEQsZ0RBQWdEO0lBQ2hELDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFDeEMsNkVBQTZFO0NBQ2hGO0FBQ0Q7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLGVBQWU7RUFDZixZQUFZO0VBQ1osZUFBZSxFQUFFO0FBQ25CO0VBQ0Usc0JBQXNCO0VBQ3RCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0U7SUFDRSxhQUFhLEVBQUUsRUFBRTtBQUNyQjtFQUNFO0lBQ0UsZUFBZSxFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG4uc29jaWFsLW5ldHdvcmstaWNvbiB7XFxuICBtYXgtd2lkdGg6IDIwcHg7IH1cXG4uc2l0ZS1mb290ZXIge1xcbiAgcGFkZGluZzogMSUgNCU7XFxuICBoZWlnaHQ6IDh2aDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLnNvY2lhbC1pY29ucy1saXN0ID4gbGkge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLnNpdGUtZm9vdGVyIHtcXG4gICAgaGVpZ2h0OiAxM3ZoOyB9IH1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gIC5zaXRlLWZvb3RlciA+IC5yb3cgPiAuY29sLXhzLTEyOmZpcnN0LWNoaWxkIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwidmFyIGVzY2FwZSA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvdXJsL2VzY2FwZS5qc1wiKTtcbmV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG4gICAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICAgIGNvbG9yOiB3aGl0ZTtcXG59XFxuLmJsYWNrIHtcXG4gICAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gICAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzYWQyY2JcXG59XFxuLndoaXRlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgICBmb250LXdlaWdodDogODAwO1xcbn1cXG4uZm9udC1saWdodCB7XFxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xcbiAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5uby1saXN0LXN0eWxlIHtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gICAgb3V0bGluZTogbm9uZTtcXG4gICAgYm9yZGVyOiBub25lO1xcbn1cXG4uYm9yZGVyLXJhZGl1cyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVcXG57XFxuICAgIDAlICAge29wYWNpdHk6MX1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMH1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDF9XFxufVxcbkBrZXlmcmFtZXMgZmFkZVxcbntcXG4gICAgMCUgICB7b3BhY2l0eToxfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAwfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMX1cXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGUyXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDF9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5Aa2V5ZnJhbWVzIGZhZGUyXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDF9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZTNcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMX1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkBrZXlmcmFtZXMgZmFkZTNcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMX1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlNFxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAxfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuQGtleWZyYW1lcyBmYWRlNFxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAxfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuLm1haW4tZm9ybSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDE0dmg7IH1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLnNlYXJjaC1zZWN0aW9uLCAuc3VibWl0LWhvbWUtbGluayB7XFxuICBjb2xvcjogd2hpdGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBwYWRkaW5nOiAyJTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpOyB9XFxuLnNlY29uZC1wYXJ0IHtcXG4gIG1hcmdpbi10b3A6IDQlOyB9XFxuLnN1Ym1pdC1ob21lLWxpbmsge1xcbiAgbWFyZ2luLXRvcDogMiU7XFxuICBwYWRkaW5nOiA1cHg7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5zZWFyY2gtYnV0dG9uIHtcXG4gIHdpZHRoOiA4MCU7XFxuICBtaW4taGVpZ2h0OiA1dmg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxcbi5zZWFyY2gtYnV0dG9uOnZpc2l0ZWQge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpOyB9XFxuLnNlYXJjaC1idXR0b246aG92ZXIge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTsgfVxcbi5zZWFyY2gtaW5wdXQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgY29sb3I6IGJsYWNrOyB9XFxuLnNlYXJjaC1pbnB1dDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuLnNlYXJjaC1pbnB1dDo6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuLnNlYXJjaC1pbnB1dDo6cGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG5zZWxlY3Qge1xcbiAgaGVpZ2h0OiA1dmg7XFxuICBjb2xvcjogYmxhY2s7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5kZWFsLXR5cGUtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmlucHV0LWxhYmVsIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDElOyB9XFxuICAuc2VhcmNoLWJ1dHRvbiB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBtYXJnaW46IDUlIGF1dG87IH0gfVxcbmJvZHkge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBvdmVyZmxvdy14OiBoaWRkZW47IH1cXG4ubG9nby1zZWN0aW9uIHtcXG4gIG1hcmdpbi1ib3R0b206IDclO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XFxuLmxvZ28taW1hZ2Uge1xcbiAgbWF4LXdpZHRoOiAxMjBweDsgfVxcbi5zbGlkZXIge1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmhlYWRlci1tYWluIHtcXG4gIHotaW5kZXg6IDk5OTtcXG4gIHRvcDogM3ZoO1xcbiAgcG9zaXRpb246IGFic29sdXRlOyB9XFxuLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgYm9yZGVyOiB0aGluIHNvbGlkIHdoaXRlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAwO1xcbiAgdG9wOiA2dmg7IH1cXG4uc2xpZGVyLWNvbHVtbiB7XFxuICBwYWRkaW5nOiAwOyB9XFxuLnNsaWRlMSwgLnNsaWRlMiwgLnNsaWRlMywgLnNsaWRlNCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTsgfVxcbi5zbGlkZTEge1xcbiAgYmFja2dyb3VuZDogdXJsKFwiICsgZXNjYXBlKHJlcXVpcmUoXCIuL21pY2hhbC1rdWJhbGN6eWstMjYwOTA5LXVuc3BsYXNoLmpwZ1wiKSkgKyBcIikgbm8tcmVwZWF0IDkwJSA4MCU7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xcbiAgYW5pbWF0aW9uOiBmYWRlIDEwcyBpbmZpbml0ZTtcXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlIDEwcyBpbmZpbml0ZTsgfVxcbi5zbGlkZTIge1xcbiAgYmFja2dyb3VuZDogdXJsKFwiICsgZXNjYXBlKHJlcXVpcmUoXCIuL21haGRpYXItbWFobW9vZGktNDUyNDg5LXVuc3BsYXNoLmpwZ1wiKSkgKyBcIikgbm8tcmVwZWF0IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XFxuICBhbmltYXRpb246IGZhZGUyIDEwcyBpbmZpbml0ZTtcXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlMiAxMHMgaW5maW5pdGU7IH1cXG4uc2xpZGUzIHtcXG4gIGJhY2tncm91bmQ6IHVybChcIiArIGVzY2FwZShyZXF1aXJlKFwiLi9jYXNleS1ob3JuZXItNTMzNTg2LXVuc3BsYXNoLmpwZ1wiKSkgKyBcIikgbm8tcmVwZWF0IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XFxuICBhbmltYXRpb246IGZhZGUzIDEwcyBpbmZpbml0ZTtcXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlMyAxMHMgaW5maW5pdGU7IH1cXG4uc2xpZGU0IHtcXG4gIGJhY2tncm91bmQ6IHVybChcIiArIGVzY2FwZShyZXF1aXJlKFwiLi9sdWtlLXZhbi16eWwtNTA0MDMyLXVuc3BsYXNoLmpwZ1wiKSkgKyBcIikgbm8tcmVwZWF0IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XFxuICBhbmltYXRpb246IGZhZGU0IDEwcyBpbmZpbml0ZTtcXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlNCAxMHMgaW5maW5pdGU7IH1cXG4uc3VibWl0LWhvdXNlLWJ1dHRvbixcXG4uc3VibWl0LWhvdXNlLWJ1dHRvbjp2aXNpdGVkLFxcbi5zdWJtaXQtaG91c2UtYnV0dG9uOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBjdXJzb3I6IHBvaW50ZXI7IH1cXG4uZmVhdHVyZXMtc2VjdGlvbiB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW4tdG9wOiAtNXZoOyB9XFxuaW5wdXRbdHlwZT1yYWRpb10ge1xcbiAgZm9udC1zaXplOiAxNnB4OyB9XFxuLmlucHV0LWxhYmVsIHtcXG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xcbiAgZm9udC1zaXplOiAxNnB4OyB9XFxuLmZlYXR1cmUtYm94IHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgbWFyZ2luLWxlZnQ6IDIlO1xcbiAgaGVpZ2h0OiAzNnZoO1xcbiAgd2lkdGg6IDI5JSAhaW1wb3J0YW50O1xcbiAgbWluLWhlaWdodDogNDJ2aDsgfVxcbi5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDIpIHtcXG4gIG1hcmdpbi1sZWZ0OiA1JTsgfVxcbi5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDMpIHtcXG4gIG1hcmdpbi1sZWZ0OiA1JTsgfVxcbi5mZWF0dXJlLWJveCA+IGRpdiA+IGltZyB7XFxuICBtYXgtd2lkdGg6IDcwcHg7IH1cXG4ud2h5LXVzIHtcXG4gIG1hcmdpbi10b3A6IDYlO1xcbiAgbWFyZ2luLWJvdHRvbTogNiU7IH1cXG4ud2h5LXVzLWltZyB7XFxuICBtYXgtd2lkdGg6IDMwMHB4OyB9XFxuLndoeS1saXN0IHtcXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGNvbG9yOiBibGFjaztcXG4gIHBhZGRpbmc6IDA7IH1cXG4ud2h5LWxpc3QgPiBsaSB7XFxuICBtYXJnaW4tdG9wOiAzJTsgfVxcbi53aHktbGlzdCA+IGxpID4gaSB7XFxuICBtYXJnaW4tbGVmdDogMiU7IH1cXG4ud2h5LXVzLWltZy1jb2x1bW4ge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7IH1cXG4ucmVhc29ucy1zZWN0aW9uIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7XFxuICBwYWRkaW5nOiAwOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICAgIHdpZHRoOiA0MCU7XFxuICAgIG1hcmdpbi1sZWZ0OiAxJTsgfVxcbiAgLm1vYmlsZS1oZWFkZXIge1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlOyB9XFxuICAubG9nby1pbWFnZSB7XFxuICAgIG1heC13aWR0aDogOTBweDsgfVxcbiAgLm1haW4tdGl0bGUge1xcbiAgICBmb250LXNpemU6IDIwcHg7IH1cXG4gIC5mZWF0dXJlLWJveCB7XFxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlO1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMikge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLndoeS11cy1pbWcge1xcbiAgICBtYXgtd2lkdGg6IDI0N3B4OyB9XFxuICAuZmVhdHVyZXMtc2VjdGlvbiB7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luLXRvcDogMnZoOyB9IH1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gIC5tb2JpbGUtaGVhZGVyIHtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7SUFDSSxxQkFBcUI7Q0FDeEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGNBQWM7Q0FDakI7QUFDRDtJQUNJLHlCQUF5QjtDQUM1QjtBQUNEO0lBQ0ksd0JBQXdCO0NBQzNCO0FBQ0Q7SUFDSSwwQkFBMEI7Q0FDN0I7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDBCQUEwQjtRQUN0Qix1QkFBdUI7WUFDbkIsb0JBQW9CO0NBQy9CO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQyw2QkFBNkI7SUFDN0IsOEJBQThCO1FBQzFCLDJCQUEyQjtZQUN2Qix1QkFBdUI7Q0FDbEM7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksZ0RBQWdEO0lBQ2hELHdEQUF3RDtJQUN4RCxnREFBZ0Q7SUFDaEQsMkNBQTJDO0lBQzNDLHdDQUF3QztJQUN4Qyw2RUFBNkU7Q0FDaEY7QUFDRDs7SUFFSSxNQUFNLFNBQVMsQ0FBQztJQUNoQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixPQUFPLFVBQVUsQ0FBQztDQUNyQjtBQUNEOztJQUVJLE1BQU0sU0FBUyxDQUFDO0lBQ2hCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE9BQU8sVUFBVSxDQUFDO0NBQ3JCO0FBQ0Q7O0lBRUksTUFBTSxTQUFTLENBQUM7SUFDaEIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsT0FBTyxVQUFVLENBQUM7Q0FDckI7QUFDRDs7SUFFSSxNQUFNLFNBQVMsQ0FBQztJQUNoQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixPQUFPLFVBQVUsQ0FBQztDQUNyQjtBQUNEOztJQUVJLE1BQU0sU0FBUyxDQUFDO0lBQ2hCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE9BQU8sVUFBVSxDQUFDO0NBQ3JCO0FBQ0Q7O0lBRUksTUFBTSxTQUFTLENBQUM7SUFDaEIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsT0FBTyxVQUFVLENBQUM7Q0FDckI7QUFDRDs7SUFFSSxNQUFNLFNBQVMsQ0FBQztJQUNoQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixPQUFPLFVBQVUsQ0FBQztDQUNyQjtBQUNEOztJQUVJLE1BQU0sU0FBUyxDQUFDO0lBQ2hCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE9BQU8sVUFBVSxDQUFDO0NBQ3JCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVSxFQUFFO0FBQ2Q7RUFDRSxnQ0FBZ0MsRUFBRTtBQUNwQztFQUNFLGFBQWE7RUFDYixxQ0FBcUM7RUFDckMsWUFBWTtFQUNaLGtEQUFrRDtVQUMxQywwQ0FBMEMsRUFBRTtBQUN0RDtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixrREFBa0Q7VUFDMUMsMENBQTBDLEVBQUU7QUFDdEQ7RUFDRSxpREFBaUQ7VUFDekMseUNBQXlDLEVBQUU7QUFDckQ7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUNoQixhQUFhLEVBQUU7QUFDakI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRTtJQUNFLGdCQUFnQixFQUFFO0VBQ3BCO0lBQ0UsZUFBZTtJQUNmLGdCQUFnQixFQUFFLEVBQUU7QUFDeEI7RUFDRSxxQkFBcUI7RUFDckIsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2YsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxhQUFhO0VBQ2IsU0FBUztFQUNULG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UseUJBQXlCO0VBQ3pCLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxRQUFRO0VBQ1IsU0FBUyxFQUFFO0FBQ2I7RUFDRSxXQUFXLEVBQUU7QUFDZjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsNERBQXdFO0VBQ3hFLHVCQUF1QjtFQUN2Qiw2QkFBNkI7RUFDN0IscUNBQXFDLEVBQUU7QUFDekM7RUFDRSwyREFBdUU7RUFDdkUsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixzQ0FBc0MsRUFBRTtBQUMxQztFQUNFLDJEQUFtRTtFQUNuRSx1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLHNDQUFzQyxFQUFFO0FBQzFDO0VBQ0UsMkRBQW1FO0VBQ25FLHVCQUF1QjtFQUN2Qiw4QkFBOEI7RUFDOUIsc0NBQXNDLEVBQUU7QUFDMUM7OztFQUdFLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxtQkFBbUI7RUFDbkIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLHFCQUFxQjtFQUNyQixnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLGtEQUFrRDtVQUMxQywwQ0FBMEM7RUFDbEQsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxlQUFlO0VBQ2Ysa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsYUFBYTtFQUNiLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVyxFQUFFO0FBQ2Y7RUFDRTtJQUNFLFdBQVc7SUFDWCxnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLGlCQUFpQixFQUFFO0VBQ3JCO0lBQ0UsZ0JBQWdCLEVBQUU7RUFDcEI7SUFDRSxnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsZUFBZSxFQUFFO0VBQ25CO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0UsaUJBQWlCLEVBQUU7RUFDckI7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCLEVBQUUsRUFBRTtBQUN4QjtFQUNFO0lBQ0UsaUJBQWlCLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjF9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAxfVxcbn1cXG5Aa2V5ZnJhbWVzIGZhZGVcXG57XFxuICAgIDAlICAge29wYWNpdHk6MX1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMH1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDF9XFxufVxcbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlMlxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAxfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAwfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuQGtleWZyYW1lcyBmYWRlMlxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAxfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAwfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGUzXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDF9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5Aa2V5ZnJhbWVzIGZhZGUzXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDF9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZTRcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMH1cXG4gICAgNzUlIHsgb3BhY2l0eTogMX1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkBrZXlmcmFtZXMgZmFkZTRcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMH1cXG4gICAgNzUlIHsgb3BhY2l0eTogMX1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbi5tYWluLWZvcm0ge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAxNHZoOyB9XFxuLnNlYXJjaC1idXR0b24ge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW0gIWltcG9ydGFudDsgfVxcbi5zZWFyY2gtc2VjdGlvbiwgLnN1Ym1pdC1ob21lLWxpbmsge1xcbiAgY29sb3I6IHdoaXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgcGFkZGluZzogMiU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxcbi5zZWNvbmQtcGFydCB7XFxuICBtYXJnaW4tdG9wOiA0JTsgfVxcbi5zdWJtaXQtaG9tZS1saW5rIHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbiAgcGFkZGluZzogNXB4O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICB3aWR0aDogODAlO1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cXG4uc2VhcmNoLWJ1dHRvbjp2aXNpdGVkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5zZWFyY2gtYnV0dG9uOmhvdmVyIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7IH1cXG4uc2VhcmNoLWlucHV0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi5zZWFyY2gtaW5wdXQ6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6OnBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuc2VsZWN0IHtcXG4gIGhlaWdodDogNXZoO1xcbiAgY29sb3I6IGJsYWNrO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uZGVhbC10eXBlLXNlY3Rpb24ge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7IH1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5pbnB1dC1sYWJlbCB7XFxuICAgIG1hcmdpbi1sZWZ0OiAxJTsgfVxcbiAgLnNlYXJjaC1idXR0b24ge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgbWFyZ2luOiA1JSBhdXRvOyB9IH1cXG5ib2R5IHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgb3ZlcmZsb3cteDogaGlkZGVuOyB9XFxuLmxvZ28tc2VjdGlvbiB7XFxuICBtYXJnaW4tYm90dG9tOiA3JTtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjsgfVxcbi5sb2dvLWltYWdlIHtcXG4gIG1heC13aWR0aDogMTIwcHg7IH1cXG4uc2xpZGVyIHtcXG4gIGhlaWdodDogMTAwdmg7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxcbi5oZWFkZXItbWFpbiB7XFxuICB6LWluZGV4OiA5OTk7XFxuICB0b3A6IDN2aDtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTsgfVxcbi5oZWFkZXItbWFpbi1ib2FyZGVyIHtcXG4gIGJvcmRlcjogdGhpbiBzb2xpZCB3aGl0ZTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgOHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgOHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMDtcXG4gIHRvcDogNnZoOyB9XFxuLnNsaWRlci1jb2x1bW4ge1xcbiAgcGFkZGluZzogMDsgfVxcbi5zbGlkZTEsIC5zbGlkZTIsIC5zbGlkZTMsIC5zbGlkZTQge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7IH1cXG4uc2xpZGUxIHtcXG4gIGJhY2tncm91bmQ6IHVybChtaWNoYWwta3ViYWxjenlrLTI2MDkwOS11bnNwbGFzaC5qcGcpIG5vLXJlcGVhdCA5MCUgODAlO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcXG4gIGFuaW1hdGlvbjogZmFkZSAxMHMgaW5maW5pdGU7XFxuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZSAxMHMgaW5maW5pdGU7IH1cXG4uc2xpZGUyIHtcXG4gIGJhY2tncm91bmQ6IHVybChtYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xcbiAgYW5pbWF0aW9uOiBmYWRlMiAxMHMgaW5maW5pdGU7XFxuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZTIgMTBzIGluZmluaXRlOyB9XFxuLnNsaWRlMyB7XFxuICBiYWNrZ3JvdW5kOiB1cmwoY2FzZXktaG9ybmVyLTUzMzU4Ni11bnNwbGFzaC5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xcbiAgYW5pbWF0aW9uOiBmYWRlMyAxMHMgaW5maW5pdGU7XFxuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZTMgMTBzIGluZmluaXRlOyB9XFxuLnNsaWRlNCB7XFxuICBiYWNrZ3JvdW5kOiB1cmwobHVrZS12YW4tenlsLTUwNDAzMi11bnNwbGFzaC5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xcbiAgYW5pbWF0aW9uOiBmYWRlNCAxMHMgaW5maW5pdGU7XFxuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZTQgMTBzIGluZmluaXRlOyB9XFxuLnN1Ym1pdC1ob3VzZS1idXR0b24sXFxuLnN1Ym1pdC1ob3VzZS1idXR0b246dmlzaXRlZCxcXG4uc3VibWl0LWhvdXNlLWJ1dHRvbjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgY3Vyc29yOiBwb2ludGVyOyB9XFxuLmZlYXR1cmVzLXNlY3Rpb24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luLXRvcDogLTV2aDsgfVxcbmlucHV0W3R5cGU9cmFkaW9dIHtcXG4gIGZvbnQtc2l6ZTogMTZweDsgfVxcbi5pbnB1dC1sYWJlbCB7XFxuICBmb250LXdlaWdodDogbGlnaHRlcjtcXG4gIGZvbnQtc2l6ZTogMTZweDsgfVxcbi5mZWF0dXJlLWJveCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG1hcmdpbi1sZWZ0OiAyJTtcXG4gIGhlaWdodDogMzZ2aDtcXG4gIHdpZHRoOiAyOSUgIWltcG9ydGFudDtcXG4gIG1pbi1oZWlnaHQ6IDQydmg7IH1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgyKSB7XFxuICBtYXJnaW4tbGVmdDogNSU7IH1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgzKSB7XFxuICBtYXJnaW4tbGVmdDogNSU7IH1cXG4uZmVhdHVyZS1ib3ggPiBkaXYgPiBpbWcge1xcbiAgbWF4LXdpZHRoOiA3MHB4OyB9XFxuLndoeS11cyB7XFxuICBtYXJnaW4tdG9wOiA2JTtcXG4gIG1hcmdpbi1ib3R0b206IDYlOyB9XFxuLndoeS11cy1pbWcge1xcbiAgbWF4LXdpZHRoOiAzMDBweDsgfVxcbi53aHktbGlzdCB7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBjb2xvcjogYmxhY2s7XFxuICBwYWRkaW5nOiAwOyB9XFxuLndoeS1saXN0ID4gbGkge1xcbiAgbWFyZ2luLXRvcDogMyU7IH1cXG4ud2h5LWxpc3QgPiBsaSA+IGkge1xcbiAgbWFyZ2luLWxlZnQ6IDIlOyB9XFxuLndoeS11cy1pbWctY29sdW1uIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0OyB9XFxuLnJlYXNvbnMtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbiAgcGFkZGluZzogMDsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgICB3aWR0aDogNDAlO1xcbiAgICBtYXJnaW4tbGVmdDogMSU7IH1cXG4gIC5tb2JpbGUtaGVhZGVyIHtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLmxvZ28taW1hZ2Uge1xcbiAgICBtYXgtd2lkdGg6IDkwcHg7IH1cXG4gIC5tYWluLXRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAyMHB4OyB9XFxuICAuZmVhdHVyZS1ib3gge1xcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1JTtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDIpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDMpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC53aHktdXMtaW1nIHtcXG4gICAgbWF4LXdpZHRoOiAyNDdweDsgfVxcbiAgLmZlYXR1cmVzLXNlY3Rpb24ge1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIG1hcmdpbi10b3A6IDJ2aDsgfSB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDMyMHB4KSB7XFxuICAubW9iaWxlLWhlYWRlciB7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIkBjaGFyc2V0IFxcXCJVVEYtOFxcXCI7XFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbjpyb290IHtcXG4gIC8qXFxuICAgKiBUeXBvZ3JhcGh5XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tZm9udC1mYW1pbHktYmFzZTogJ1NlZ29lIFVJJywgJ0hlbHZldGljYU5ldWUtTGlnaHQnLCBzYW5zLXNlcmlmO1xcblxcbiAgLypcXG4gICAqIExheW91dFxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLW1heC1jb250ZW50LXdpZHRoOiAxMDAwcHg7XFxuXFxuICAvKlxcbiAgICogTWVkaWEgcXVlcmllcyBicmVha3BvaW50c1xcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLXNjcmVlbi14cy1taW46IDQ4MHB4OyAgLyogRXh0cmEgc21hbGwgc2NyZWVuIC8gcGhvbmUgKi9cXG4gIC0tc2NyZWVuLXNtLW1pbjogNzY4cHg7ICAvKiBTbWFsbCBzY3JlZW4gLyB0YWJsZXQgKi9cXG4gIC0tc2NyZWVuLW1kLW1pbjogOTkycHg7ICAvKiBNZWRpdW0gc2NyZWVuIC8gZGVza3RvcCAqL1xcbiAgLS1zY3JlZW4tbGctbWluOiAxMjAwcHg7IC8qIExhcmdlIHNjcmVlbiAvIHdpZGUgZGVza3RvcCAqL1xcbn1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogMTAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDcwMDsgfVxcbi8qXFxuICogbm9ybWFsaXplLmNzcyBpcyBpbXBvcnRlZCBpbiBKUyBmaWxlLlxcbiAqIElmIHlvdSBpbXBvcnQgZXh0ZXJuYWwgQ1NTIGZpbGUgZnJvbSB5b3VyIGludGVybmFsIENTU1xcbiAqIHRoZW4gaXQgd2lsbCBiZSBpbmxpbmVkIGFuZCBwcm9jZXNzZWQgd2l0aCBDU1MgbW9kdWxlcy5cXG4gKi9cXG4vKlxcbiAqIEJhc2Ugc3R5bGVzXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5odG1sIHtcXG4gIGNvbG9yOiAjMjIyO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDtcXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogfjE2cHg7ICovXFxuICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktYmFzZSk7XFxuICBsaW5lLWhlaWdodDogMS4zNzU7XFxuICAvKiB+MjJweCAqLyB9XFxuYm9keSB7XFxuICBtYXJnaW46IDA7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTsgfVxcbmEge1xcbiAgY29sb3I6ICMwMDc0YzI7IH1cXG4vKlxcbiAqIFJlbW92ZSB0ZXh0LXNoYWRvdyBpbiBzZWxlY3Rpb24gaGlnaGxpZ2h0OlxcbiAqIGh0dHBzOi8vdHdpdHRlci5jb20vbWlrZXRheWxyL3N0YXR1cy8xMjIyODgwNTMwMVxcbiAqXFxuICogVGhlc2Ugc2VsZWN0aW9uIHJ1bGUgc2V0cyBoYXZlIHRvIGJlIHNlcGFyYXRlLlxcbiAqIEN1c3RvbWl6ZSB0aGUgYmFja2dyb3VuZCBjb2xvciB0byBtYXRjaCB5b3VyIGRlc2lnbi5cXG4gKi9cXG46Oi1tb3otc2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbjo6c2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbi8qXFxuICogQSBiZXR0ZXIgbG9va2luZyBkZWZhdWx0IGhvcml6b250YWwgcnVsZVxcbiAqL1xcbmhyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBib3JkZXI6IDA7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcXG4gIG1hcmdpbjogMWVtIDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBSZW1vdmUgdGhlIGdhcCBiZXR3ZWVuIGF1ZGlvLCBjYW52YXMsIGlmcmFtZXMsXFxuICogaW1hZ2VzLCB2aWRlb3MgYW5kIHRoZSBib3R0b20gb2YgdGhlaXIgY29udGFpbmVyczpcXG4gKiBodHRwczovL2dpdGh1Yi5jb20vaDVicC9odG1sNS1ib2lsZXJwbGF0ZS9pc3N1ZXMvNDQwXFxuICovXFxuYXVkaW8sXFxuY2FudmFzLFxcbmlmcmFtZSxcXG5pbWcsXFxuc3ZnLFxcbnZpZGVvIHtcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7IH1cXG4vKlxcbiAqIFJlbW92ZSBkZWZhdWx0IGZpZWxkc2V0IHN0eWxlcy5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBBbGxvdyBvbmx5IHZlcnRpY2FsIHJlc2l6aW5nIG9mIHRleHRhcmVhcy5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICByZXNpemU6IHZlcnRpY2FsOyB9XFxuLypcXG4gKiBCcm93c2VyIHVwZ3JhZGUgcHJvbXB0XFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4uYnJvd3NlcnVwZ3JhZGUge1xcbiAgbWFyZ2luOiAwLjJlbSAwO1xcbiAgYmFja2dyb3VuZDogI2NjYztcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMC4yZW0gMDsgfVxcbi8qXFxuICogUHJpbnQgc3R5bGVzXFxuICogSW5saW5lZCB0byBhdm9pZCB0aGUgYWRkaXRpb25hbCBIVFRQIHJlcXVlc3Q6XFxuICogaHR0cDovL3d3dy5waHBpZWQuY29tL2RlbGF5LWxvYWRpbmcteW91ci1wcmludC1jc3MvXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5AbWVkaWEgcHJpbnQge1xcbiAgKixcXG4gICo6OmJlZm9yZSxcXG4gICo6OmFmdGVyIHtcXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcXG4gICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcXG4gICAgLyogQmxhY2sgcHJpbnRzIGZhc3RlcjogaHR0cDovL3d3dy5zYW5iZWlqaS5jb20vYXJjaGl2ZXMvOTUzICovXFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgdGV4dC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDsgfVxcbiAgYSxcXG4gIGE6dmlzaXRlZCB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyB9XFxuICBhW2hyZWZdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cihocmVmKSBcXFwiKVxcXCI7IH1cXG4gIGFiYnJbdGl0bGVdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cih0aXRsZSkgXFxcIilcXFwiOyB9XFxuICAvKlxcbiAgICogRG9uJ3Qgc2hvdyBsaW5rcyB0aGF0IGFyZSBmcmFnbWVudCBpZGVudGlmaWVycyxcXG4gICAqIG9yIHVzZSB0aGUgYGphdmFzY3JpcHQ6YCBwc2V1ZG8gcHJvdG9jb2xcXG4gICAqL1xcbiAgYVtocmVmXj0nIyddOjphZnRlcixcXG4gIGFbaHJlZl49J2phdmFzY3JpcHQ6J106OmFmdGVyIHtcXG4gICAgY29udGVudDogJyc7IH1cXG4gIHByZSxcXG4gIGJsb2NrcXVvdGUge1xcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjOTk5O1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIC8qXFxuICAgKiBQcmludGluZyBUYWJsZXM6XFxuICAgKiBodHRwOi8vY3NzLWRpc2N1c3MuaW5jdXRpby5jb20vd2lraS9QcmludGluZ19UYWJsZXNcXG4gICAqL1xcbiAgdGhlYWQge1xcbiAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7IH1cXG4gIHRyLFxcbiAgaW1nIHtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICBpbWcge1xcbiAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDsgfVxcbiAgcCxcXG4gIGgyLFxcbiAgaDMge1xcbiAgICBvcnBoYW5zOiAzO1xcbiAgICB3aWRvd3M6IDM7IH1cXG4gIGgyLFxcbiAgaDMge1xcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhdm9pZDsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsaUJBQWlCO0FBQ2pCOzs7Ozs7O0dBT0c7QUFDSDs7Ozs7OztHQU9HO0FBQ0g7RUFDRTs7Z0ZBRThFOztFQUU5RSxrRUFBa0U7O0VBRWxFOztnRkFFOEU7O0VBRTlFLDRCQUE0Qjs7RUFFNUI7O2dGQUU4RTs7RUFFOUUsdUJBQXVCLEVBQUUsZ0NBQWdDO0VBQ3pELHVCQUF1QixFQUFFLDJCQUEyQjtFQUNwRCx1QkFBdUIsRUFBRSw2QkFBNkI7RUFDdEQsd0JBQXdCLENBQUMsaUNBQWlDO0NBQzNEO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIsbUNBQW1DO0VBQ25DLHVLQUF1SztFQUN2SyxpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLHFCQUFxQjtFQUNyQiw2QkFBNkI7RUFDN0IscUpBQXFKO0VBQ3JKLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UscUJBQXFCO0VBQ3JCLGtDQUFrQztFQUNsQyxvS0FBb0s7RUFDcEssaUJBQWlCLEVBQUU7QUFDckI7Ozs7R0FJRztBQUNIOztnRkFFZ0Y7QUFDaEY7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixZQUFZO0VBQ1oscUNBQXFDO0VBQ3JDLG1CQUFtQjtFQUNuQixXQUFXLEVBQUU7QUFDZjtFQUNFLFVBQVU7RUFDVixxQkFBcUIsRUFBRTtBQUN6QjtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7Ozs7O0dBTUc7QUFDSDtFQUNFLG9CQUFvQjtFQUNwQixrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLG9CQUFvQjtFQUNwQixrQkFBa0IsRUFBRTtBQUN0Qjs7R0FFRztBQUNIO0VBQ0UsZUFBZTtFQUNmLFlBQVk7RUFDWixVQUFVO0VBQ1YsMkJBQTJCO0VBQzNCLGNBQWM7RUFDZCxXQUFXLEVBQUU7QUFDZjs7OztHQUlHO0FBQ0g7Ozs7OztFQU1FLHVCQUF1QixFQUFFO0FBQzNCOztHQUVHO0FBQ0g7RUFDRSxVQUFVO0VBQ1YsVUFBVTtFQUNWLFdBQVcsRUFBRTtBQUNmOztHQUVHO0FBQ0g7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjs7Z0ZBRWdGO0FBQ2hGO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCLEVBQUU7QUFDckI7Ozs7Z0ZBSWdGO0FBQ2hGO0VBQ0U7OztJQUdFLG1DQUFtQztJQUNuQyx1QkFBdUI7SUFDdkIsK0RBQStEO0lBQy9ELG9DQUFvQztZQUM1Qiw0QkFBNEI7SUFDcEMsNkJBQTZCLEVBQUU7RUFDakM7O0lBRUUsMkJBQTJCLEVBQUU7RUFDL0I7SUFDRSw2QkFBNkIsRUFBRTtFQUNqQztJQUNFLDhCQUE4QixFQUFFO0VBQ2xDOzs7S0FHRztFQUNIOztJQUVFLFlBQVksRUFBRTtFQUNoQjs7SUFFRSx1QkFBdUI7SUFDdkIseUJBQXlCLEVBQUU7RUFDN0I7OztLQUdHO0VBQ0g7SUFDRSw0QkFBNEIsRUFBRTtFQUNoQzs7SUFFRSx5QkFBeUIsRUFBRTtFQUM3QjtJQUNFLDJCQUEyQixFQUFFO0VBQy9COzs7SUFHRSxXQUFXO0lBQ1gsVUFBVSxFQUFFO0VBQ2Q7O0lBRUUsd0JBQXdCLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwiTGF5b3V0LmNzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCJAY2hhcnNldCBcXFwiVVRGLThcXFwiO1xcbi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG46cm9vdCB7XFxuICAvKlxcbiAgICogVHlwb2dyYXBoeVxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLWZvbnQtZmFtaWx5LWJhc2U6ICdTZWdvZSBVSScsICdIZWx2ZXRpY2FOZXVlLUxpZ2h0Jywgc2Fucy1zZXJpZjtcXG5cXG4gIC8qXFxuICAgKiBMYXlvdXRcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1tYXgtY29udGVudC13aWR0aDogMTAwMHB4O1xcblxcbiAgLypcXG4gICAqIE1lZGlhIHF1ZXJpZXMgYnJlYWtwb2ludHNcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1zY3JlZW4teHMtbWluOiA0ODBweDsgIC8qIEV4dHJhIHNtYWxsIHNjcmVlbiAvIHBob25lICovXFxuICAtLXNjcmVlbi1zbS1taW46IDc2OHB4OyAgLyogU21hbGwgc2NyZWVuIC8gdGFibGV0ICovXFxuICAtLXNjcmVlbi1tZC1taW46IDk5MnB4OyAgLyogTWVkaXVtIHNjcmVlbiAvIGRlc2t0b3AgKi9cXG4gIC0tc2NyZWVuLWxnLW1pbjogMTIwMHB4OyAvKiBMYXJnZSBzY3JlZW4gLyB3aWRlIGRlc2t0b3AgKi9cXG59XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0ud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7IH1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA3MDA7IH1cXG4vKlxcbiAqIG5vcm1hbGl6ZS5jc3MgaXMgaW1wb3J0ZWQgaW4gSlMgZmlsZS5cXG4gKiBJZiB5b3UgaW1wb3J0IGV4dGVybmFsIENTUyBmaWxlIGZyb20geW91ciBpbnRlcm5hbCBDU1NcXG4gKiB0aGVuIGl0IHdpbGwgYmUgaW5saW5lZCBhbmQgcHJvY2Vzc2VkIHdpdGggQ1NTIG1vZHVsZXMuXFxuICovXFxuLypcXG4gKiBCYXNlIHN0eWxlc1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuaHRtbCB7XFxuICBjb2xvcjogIzIyMjtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIH4xNnB4OyAqL1xcbiAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJhc2UpO1xcbiAgbGluZS1oZWlnaHQ6IDEuMzc1O1xcbiAgLyogfjIycHggKi8gfVxcbmJvZHkge1xcbiAgbWFyZ2luOiAwO1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07IH1cXG5hIHtcXG4gIGNvbG9yOiAjMDA3NGMyOyB9XFxuLypcXG4gKiBSZW1vdmUgdGV4dC1zaGFkb3cgaW4gc2VsZWN0aW9uIGhpZ2hsaWdodDpcXG4gKiBodHRwczovL3R3aXR0ZXIuY29tL21pa2V0YXlsci9zdGF0dXMvMTIyMjg4MDUzMDFcXG4gKlxcbiAqIFRoZXNlIHNlbGVjdGlvbiBydWxlIHNldHMgaGF2ZSB0byBiZSBzZXBhcmF0ZS5cXG4gKiBDdXN0b21pemUgdGhlIGJhY2tncm91bmQgY29sb3IgdG8gbWF0Y2ggeW91ciBkZXNpZ24uXFxuICovXFxuOjotbW96LXNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7IH1cXG46OnNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7IH1cXG4vKlxcbiAqIEEgYmV0dGVyIGxvb2tpbmcgZGVmYXVsdCBob3Jpem9udGFsIHJ1bGVcXG4gKi9cXG5ociB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGhlaWdodDogMXB4O1xcbiAgYm9yZGVyOiAwO1xcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XFxuICBtYXJnaW46IDFlbSAwO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qXFxuICogUmVtb3ZlIHRoZSBnYXAgYmV0d2VlbiBhdWRpbywgY2FudmFzLCBpZnJhbWVzLFxcbiAqIGltYWdlcywgdmlkZW9zIGFuZCB0aGUgYm90dG9tIG9mIHRoZWlyIGNvbnRhaW5lcnM6XFxuICogaHR0cHM6Ly9naXRodWIuY29tL2g1YnAvaHRtbDUtYm9pbGVycGxhdGUvaXNzdWVzLzQ0MFxcbiAqL1xcbmF1ZGlvLFxcbmNhbnZhcyxcXG5pZnJhbWUsXFxuaW1nLFxcbnN2ZyxcXG52aWRlbyB7XFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyB9XFxuLypcXG4gKiBSZW1vdmUgZGVmYXVsdCBmaWVsZHNldCBzdHlsZXMuXFxuICovXFxuZmllbGRzZXQge1xcbiAgYm9yZGVyOiAwO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qXFxuICogQWxsb3cgb25seSB2ZXJ0aWNhbCByZXNpemluZyBvZiB0ZXh0YXJlYXMuXFxuICovXFxudGV4dGFyZWEge1xcbiAgcmVzaXplOiB2ZXJ0aWNhbDsgfVxcbi8qXFxuICogQnJvd3NlciB1cGdyYWRlIHByb21wdFxcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuOmdsb2JhbCguYnJvd3NlcnVwZ3JhZGUpIHtcXG4gIG1hcmdpbjogMC4yZW0gMDtcXG4gIGJhY2tncm91bmQ6ICNjY2M7XFxuICBjb2xvcjogIzAwMDtcXG4gIHBhZGRpbmc6IDAuMmVtIDA7IH1cXG4vKlxcbiAqIFByaW50IHN0eWxlc1xcbiAqIElubGluZWQgdG8gYXZvaWQgdGhlIGFkZGl0aW9uYWwgSFRUUCByZXF1ZXN0OlxcbiAqIGh0dHA6Ly93d3cucGhwaWVkLmNvbS9kZWxheS1sb2FkaW5nLXlvdXItcHJpbnQtY3NzL1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuQG1lZGlhIHByaW50IHtcXG4gICosXFxuICAqOjpiZWZvcmUsXFxuICAqOjphZnRlciB7XFxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XFxuICAgIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XFxuICAgIC8qIEJsYWNrIHByaW50cyBmYXN0ZXI6IGh0dHA6Ly93d3cuc2FuYmVpamkuY29tL2FyY2hpdmVzLzk1MyAqL1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIHRleHQtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7IH1cXG4gIGEsXFxuICBhOnZpc2l0ZWQge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTsgfVxcbiAgYVtocmVmXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiBcXFwiIChcXFwiIGF0dHIoaHJlZikgXFxcIilcXFwiOyB9XFxuICBhYmJyW3RpdGxlXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiBcXFwiIChcXFwiIGF0dHIodGl0bGUpIFxcXCIpXFxcIjsgfVxcbiAgLypcXG4gICAqIERvbid0IHNob3cgbGlua3MgdGhhdCBhcmUgZnJhZ21lbnQgaWRlbnRpZmllcnMsXFxuICAgKiBvciB1c2UgdGhlIGBqYXZhc2NyaXB0OmAgcHNldWRvIHByb3RvY29sXFxuICAgKi9cXG4gIGFbaHJlZl49JyMnXTo6YWZ0ZXIsXFxuICBhW2hyZWZePSdqYXZhc2NyaXB0OiddOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6ICcnOyB9XFxuICBwcmUsXFxuICBibG9ja3F1b3RlIHtcXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICAvKlxcbiAgICogUHJpbnRpbmcgVGFibGVzOlxcbiAgICogaHR0cDovL2Nzcy1kaXNjdXNzLmluY3V0aW8uY29tL3dpa2kvUHJpbnRpbmdfVGFibGVzXFxuICAgKi9cXG4gIHRoZWFkIHtcXG4gICAgZGlzcGxheTogdGFibGUtaGVhZGVyLWdyb3VwOyB9XFxuICB0cixcXG4gIGltZyB7XFxuICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdm9pZDsgfVxcbiAgaW1nIHtcXG4gICAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7IH1cXG4gIHAsXFxuICBoMixcXG4gIGgzIHtcXG4gICAgb3JwaGFuczogMztcXG4gICAgd2lkb3dzOiAzOyB9XFxuICBoMixcXG4gIGgzIHtcXG4gICAgcGFnZS1icmVhay1hZnRlcjogYXZvaWQ7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIubG9hZGVyIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGZvbnQtZmFtaWx5OiBJUlNhbnMsIHNlcmlmO1xcbiAgei1pbmRleDogMTA7XFxuICBwYWRkaW5nOiAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHRvcDogY2FsYyg1MCUgLSA4NXB4KTtcXG4gIGxlZnQ6IGNhbGMoNTAlIC0gNzBweCk7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpOyB9XFxuICAubG9hZGVyIGltZyB7XFxuICAgIHdpZHRoOiAxMDBweDsgfVxcbiAgLmxvYWRlciAubG9hZGVyLWluZm8ge1xcbiAgICBkaXJlY3Rpb246IHJ0bDtcXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgfVxcbiAgLmxvYWRlci5sb2FkaW5nIHtcXG4gICAgZGlzcGxheTogYmxvY2s7IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsMkJBQTJCO0VBQzNCLFlBQVk7RUFDWixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHdCQUF3QjtFQUN4QixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qix3SEFBd0g7VUFDaEgsZ0hBQWdILEVBQUU7RUFDMUg7SUFDRSxhQUFhLEVBQUU7RUFDakI7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQixFQUFFO0VBQ3ZCO0lBQ0UsZUFBZSxFQUFFXCIsXCJmaWxlXCI6XCJMb2FkZXIuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIubG9hZGVyIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGZvbnQtZmFtaWx5OiBJUlNhbnMsIHNlcmlmO1xcbiAgei1pbmRleDogMTA7XFxuICBwYWRkaW5nOiAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHRvcDogY2FsYyg1MCUgLSA4NXB4KTtcXG4gIGxlZnQ6IGNhbGMoNTAlIC0gNzBweCk7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpOyB9XFxuICAubG9hZGVyIGltZyB7XFxuICAgIHdpZHRoOiAxMDBweDsgfVxcbiAgLmxvYWRlciAubG9hZGVyLWluZm8ge1xcbiAgICBkaXJlY3Rpb246IHJ0bDtcXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgfVxcbiAgLmxvYWRlci5sb2FkaW5nIHtcXG4gICAgZGlzcGxheTogYmxvY2s7IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDYiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtJQUNJLHFCQUFxQjtDQUN4QjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksY0FBYztDQUNqQjtBQUNEO0lBQ0kseUJBQXlCO0NBQzVCO0FBQ0Q7SUFDSSx3QkFBd0I7Q0FDM0I7QUFDRDtJQUNJLDBCQUEwQjtDQUM3QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsMEJBQTBCO1FBQ3RCLHVCQUF1QjtZQUNuQixvQkFBb0I7Q0FDL0I7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDZCQUE2QjtJQUM3Qiw4QkFBOEI7UUFDMUIsMkJBQTJCO1lBQ3ZCLHVCQUF1QjtDQUNsQztBQUNEO0lBQ0ksa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxnREFBZ0Q7SUFDaEQsd0RBQXdEO0lBQ3hELGdEQUFnRDtJQUNoRCwyQ0FBMkM7SUFDM0Msd0NBQXdDO0lBQ3hDLDZFQUE2RTtDQUNoRjtBQUNELGNBQWMsVUFBVSxDQUFDO0FBQ3pCO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLGFBQWE7RUFDYiwrQ0FBK0M7VUFDdkMsdUNBQXVDO0VBQy9DLHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkU7Q0FDOUU7QUFDRDtFQUNFLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0NBQzdCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QjtDQUNqQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLHVCQUF1Qjs7Q0FFeEI7QUFDRDtFQUNFLHlCQUF5QjtDQUMxQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHVCQUF1QjtDQUN4QjtBQUNEO0VBQ0UsNEJBQTRCO0NBQzdCO0FBQ0Q7RUFDRSwyQkFBMkI7Q0FDNUI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsK0JBQStCO0VBQy9CLCtCQUErQjtNQUMzQixnQ0FBZ0M7VUFDNUIsNEJBQTRCO0NBQ3JDO0FBQ0Q7RUFDRSxzQkFBc0I7Q0FDdkI7QUFDRDtFQUNFLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsWUFBWTtDQUNiO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxnQkFBZ0I7Q0FDakI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYiw0QkFBNEI7RUFDNUIsb0JBQW9CO0VBQ3BCLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsV0FBVztFQUNYLFlBQVk7O0NBRWI7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRCxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUI7Q0FDcEI7QUFDRDtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsZUFBZTtDQUNoQjtBQUNEO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVk7Q0FDYjtBQUNELDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjtFQUNEO0lBQ0UsVUFBVTtHQUNYOztFQUVEO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLG9CQUFvQjtJQUNwQixZQUFZO0dBQ2I7RUFDRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixxREFBcUQ7WUFDN0MsNkNBQTZDO0lBQ3JELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7R0FDWDs7Q0FFRjtBQUNEO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7RUFDYixrREFBa0Q7VUFDMUMsMENBQTBDO0VBQ2xELHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkUsRUFBRTtBQUNqRjtFQUNFLFdBQVc7RUFDWCxhQUFhLEVBQUU7QUFDakI7RUFDRSxhQUFhO0VBQ2Isb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxhQUFhLEVBQUU7QUFDakI7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwrQkFBK0I7RUFDL0IsOEJBQThCO01BQzFCLHdCQUF3QjtVQUNwQixvQkFBb0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYix3QkFBd0I7RUFDeEIsb0RBQW9EO1VBQzVDLDRDQUE0QztFQUNwRCxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGNBQWMsRUFBRTtBQUNsQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CLEVBQUU7QUFDaEM7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QixFQUFFO0FBQ3BDO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLG1CQUFtQjtFQUNuQixlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLCtCQUErQjtFQUMvQiwrQkFBK0I7TUFDM0IsZ0NBQWdDO1VBQzVCLDRCQUE0QixFQUFFO0FBQ3hDO0VBQ0Usc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxXQUFXLEVBQUU7QUFDZjtFQUNFLFlBQVksRUFBRTtBQUNoQjtFQUNFLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsYUFBYSxFQUFFO0FBQ2pCO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLHFDQUFxQztFQUNyQyw0QkFBNEIsRUFBRTtBQUNoQztFQUNFLGNBQWMsRUFBRTtBQUNsQjtFQUNFLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixnQ0FBZ0MsRUFBRTtBQUNwQztFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixzQkFBc0IsRUFBRTtBQUMxQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFvQjtFQUNwQixvQkFBb0I7RUFDcEIsWUFBWSxFQUFFO0FBQ2hCO0VBQ0UsV0FBVztFQUNYLFlBQVksRUFBRTtBQUNoQjtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkMsRUFBRTtBQUN2RDtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkMsRUFBRTtBQUN2RDtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsaUNBQWlDLEVBQUU7QUFDckM7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGdDQUFnQyxFQUFFO0FBQ3BDLHNCQUFzQjtBQUN0QjtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLHdEQUF3RDtVQUNoRCxnREFBZ0Q7RUFDeEQsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxVQUFVO0VBQ1YsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxzQkFBc0I7RUFDdEIsWUFBWSxFQUFFO0FBQ2hCLDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQixFQUFFO0VBQ3JCO0lBQ0UsVUFBVSxFQUFFO0VBQ2Q7SUFDRSxZQUFZO0lBQ1osWUFBWSxFQUFFO0VBQ2hCO0lBQ0UsbUJBQW1CO0lBQ25CLGdCQUFnQixFQUFFO0VBQ3BCO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0Usa0JBQWtCLEVBQUU7RUFDdEI7SUFDRSxZQUFZO0lBQ1osWUFBWTtJQUNaLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLFlBQVksRUFBRTtFQUNoQjtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQix3REFBd0Q7WUFDaEQsZ0RBQWdEO0lBQ3hELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVUsRUFBRTtFQUNkO0lBQ0Usb0RBQW9EO1lBQzVDLDRDQUE0QztJQUNwRCxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixhQUFhLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zZWFyY2gtc2VjdGlvbiwgLnN1Ym1pdC1ob21lLWxpbmsge1xcbiAgY29sb3I6IHdoaXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgcGFkZGluZzogMiU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxcbi5zZWNvbmQtcGFydCB7XFxuICBtYXJnaW4tdG9wOiA0JTsgfVxcbi5zdWJtaXQtaG9tZS1saW5rIHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbiAgcGFkZGluZzogNXB4O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICB3aWR0aDogODAlO1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cXG4uc2VhcmNoLWJ1dHRvbjp2aXNpdGVkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5zZWFyY2gtYnV0dG9uOmhvdmVyIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7IH1cXG4uc2VhcmNoLWlucHV0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtICFpbXBvcnRhbnQ7XFxuICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDsgfVxcbi5zZWFyY2gtaW5wdXQ6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6OnBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuLmVycm9yIHtcXG4gIGJvcmRlcjogMXB4IHNvbGlkIHJlZCAhaW1wb3J0YW50OyB9XFxuLmVyci1tc2cge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG4gIGNvbG9yOiAjZjE2NDY0OyB9XFxuLnNlYXJjaC1idXR0b246ZGlzYWJsZWQge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2IzYWVhZTsgfVxcbnNlbGVjdCB7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIGNvbG9yOiBibGFjaztcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLmRlYWwtdHlwZS1zZWN0aW9uIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0OyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaW5wdXQtbGFiZWwge1xcbiAgICBtYXJnaW4tbGVmdDogMSU7IH1cXG4gIC5zZWFyY2gtYnV0dG9uIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIG1hcmdpbjogNSUgYXV0bzsgfSB9XFxuYm9keSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIG92ZXJmbG93LXg6IGhpZGRlbjsgfVxcbi5sb2dvLXNlY3Rpb24ge1xcbiAgbWFyZ2luLWJvdHRvbTogNyU7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cXG4ubG9nby1pbWFnZSB7XFxuICBtYXgtd2lkdGg6IDEyMHB4OyB9XFxuLnNsaWRlciB7XFxuICBoZWlnaHQ6IDEwMHZoO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uaGVhZGVyLW1haW4ge1xcbiAgei1pbmRleDogOTk5O1xcbiAgdG9wOiAzdmg7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7IH1cXG4uaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICBib3JkZXI6IHRoaW4gc29saWQgd2hpdGU7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4OyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDA7XFxuICB0b3A6IDZ2aDsgfVxcbi5zbGlkZXItY29sdW1uIHtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uc3VibWl0LWhvdXNlLWJ1dHRvbixcXG4uc3VibWl0LWhvdXNlLWJ1dHRvbjp2aXNpdGVkLFxcbi5zdWJtaXQtaG91c2UtYnV0dG9uOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBjdXJzb3I6IHBvaW50ZXI7IH1cXG4uZmVhdHVyZXMtc2VjdGlvbiB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW4tdG9wOiAtNXZoOyB9XFxuaW5wdXRbdHlwZT1yYWRpb10ge1xcbiAgZm9udC1zaXplOiAxNnB4OyB9XFxuLmlucHV0LWxhYmVsIHtcXG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbWFyZ2luLWxlZnQ6IDE1JTsgfVxcbi5mZWF0dXJlLWJveCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG1hcmdpbi1sZWZ0OiAyJTtcXG4gIGhlaWdodDogMzZ2aDtcXG4gIHdpZHRoOiAyOSUgIWltcG9ydGFudDtcXG4gIG1pbi1oZWlnaHQ6IDQydmg7IH1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgyKSB7XFxuICBtYXJnaW4tbGVmdDogNSU7IH1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgzKSB7XFxuICBtYXJnaW4tbGVmdDogNSU7IH1cXG4uZmVhdHVyZS1ib3ggPiBkaXYgPiBpbWcge1xcbiAgbWF4LXdpZHRoOiA3MHB4OyB9XFxuLndoeS11cyB7XFxuICBtYXJnaW4tdG9wOiA2JTtcXG4gIG1hcmdpbi1ib3R0b206IDYlOyB9XFxuLndoeS11cy1pbWcge1xcbiAgbWF4LXdpZHRoOiAzMDBweDsgfVxcbi53aHktbGlzdCB7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBjb2xvcjogYmxhY2s7XFxuICBwYWRkaW5nOiAwOyB9XFxuLndoeS1saXN0ID4gbGkge1xcbiAgbWFyZ2luLXRvcDogMyU7IH1cXG4ud2h5LWxpc3QgPiBsaSA+IGkge1xcbiAgbWFyZ2luLWxlZnQ6IDIlOyB9XFxuLndoeS11cy1pbWctY29sdW1uIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0OyB9XFxuLnJlYXNvbnMtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbiAgcGFkZGluZzogMDsgfVxcbi5idWlsZGluZy10eXBlLWxhYmVsIHtcXG4gIGhlaWdodDogNXZoOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICAgIHdpZHRoOiA0MCU7XFxuICAgIG1hcmdpbi1sZWZ0OiAxJTsgfVxcbiAgLm1vYmlsZS1oZWFkZXIge1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlOyB9XFxuICAubG9nby1pbWFnZSB7XFxuICAgIG1heC13aWR0aDogOTBweDsgfVxcbiAgLm1haW4tdGl0bGUge1xcbiAgICBmb250LXNpemU6IDIwcHg7IH1cXG4gIC5mZWF0dXJlLWJveCB7XFxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlO1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMikge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLndoeS11cy1pbWcge1xcbiAgICBtYXgtd2lkdGg6IDI0N3B4OyB9IH1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gIC5tb2JpbGUtaGVhZGVyIHtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L21haW4uY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0lBQ0kscUJBQXFCO0NBQ3hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxjQUFjO0NBQ2pCO0FBQ0Q7SUFDSSx5QkFBeUI7Q0FDNUI7QUFDRDtJQUNJLHdCQUF3QjtDQUMzQjtBQUNEO0lBQ0ksMEJBQTBCO0NBQzdCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQywwQkFBMEI7UUFDdEIsdUJBQXVCO1lBQ25CLG9CQUFvQjtDQUMvQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsNkJBQTZCO0lBQzdCLDhCQUE4QjtRQUMxQiwyQkFBMkI7WUFDdkIsdUJBQXVCO0NBQ2xDO0FBQ0Q7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGNBQWM7SUFDZCxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGdEQUFnRDtJQUNoRCx3REFBd0Q7SUFDeEQsZ0RBQWdEO0lBQ2hELDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFDeEMsNkVBQTZFO0NBQ2hGO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IscUNBQXFDO0VBQ3JDLFlBQVk7RUFDWixrREFBa0Q7VUFDMUMsMENBQTBDLEVBQUU7QUFDdEQ7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxlQUFlO0VBQ2YsYUFBYTtFQUNiLGVBQWUsRUFBRTtBQUNuQjtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsa0RBQWtEO1VBQzFDLDBDQUEwQyxFQUFFO0FBQ3REO0VBQ0UsaURBQWlEO1VBQ3pDLHlDQUF5QyxFQUFFO0FBQ3JEO0VBQ0UsbURBQW1EO1VBQzNDLDJDQUEyQyxFQUFFO0FBQ3ZEO0VBQ0UsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixnQkFBZ0I7RUFDaEIsZ0NBQWdDO0VBQ2hDLHdCQUF3QixFQUFFO0FBQzVCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxpQ0FBaUMsRUFBRTtBQUNyQztFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsMEJBQTBCLEVBQUU7QUFDOUI7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0U7SUFDRSxnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLGVBQWU7SUFDZixnQkFBZ0IsRUFBRSxFQUFFO0FBQ3hCO0VBQ0UscUJBQXFCO0VBQ3JCLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsYUFBYTtFQUNiLFNBQVM7RUFDVCxtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLHlCQUF5QjtFQUN6QixvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixvREFBb0Q7VUFDNUMsNENBQTRDO0VBQ3BELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsUUFBUTtFQUNSLFNBQVMsRUFBRTtBQUNiO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7OztFQUdFLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxtQkFBbUI7RUFDbkIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLHFCQUFxQjtFQUNyQixnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxrREFBa0Q7VUFDMUMsMENBQTBDO0VBQ2xELGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGFBQWE7RUFDYixXQUFXLEVBQUU7QUFDZjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFdBQVcsRUFBRTtBQUNmO0VBQ0UsWUFBWSxFQUFFO0FBQ2hCO0VBQ0U7SUFDRSxXQUFXO0lBQ1gsZ0JBQWdCLEVBQUU7RUFDcEI7SUFDRSxpQkFBaUIsRUFBRTtFQUNyQjtJQUNFLGdCQUFnQixFQUFFO0VBQ3BCO0lBQ0UsZ0JBQWdCLEVBQUU7RUFDcEI7SUFDRSx1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGlCQUFpQixFQUFFLEVBQUU7QUFDekI7RUFDRTtJQUNFLGlCQUFpQixFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG4uc2VhcmNoLXNlY3Rpb24sIC5zdWJtaXQtaG9tZS1saW5rIHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIHBhZGRpbmc6IDIlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cXG4uc2Vjb25kLXBhcnQge1xcbiAgbWFyZ2luLXRvcDogNCU7IH1cXG4uc3VibWl0LWhvbWUtbGluayB7XFxuICBtYXJnaW4tdG9wOiAyJTtcXG4gIHBhZGRpbmc6IDVweDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLnNlYXJjaC1idXR0b24ge1xcbiAgd2lkdGg6IDgwJTtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpOyB9XFxuLnNlYXJjaC1idXR0b246dmlzaXRlZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4uc2VhcmNoLWJ1dHRvbjpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuLnNlYXJjaC1pbnB1dCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBtaW4taGVpZ2h0OiA1dmg7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50O1xcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7IH1cXG4uc2VhcmNoLWlucHV0Ojotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuLnNlYXJjaC1pbnB1dDotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0OjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0OjpwbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5lcnJvciB7XFxuICBib3JkZXI6IDFweCBzb2xpZCByZWQgIWltcG9ydGFudDsgfVxcbi5lcnItbXNnIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7XFxuICBjb2xvcjogI2YxNjQ2NDsgfVxcbi5zZWFyY2gtYnV0dG9uOmRpc2FibGVkIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNiM2FlYWU7IH1cXG5zZWxlY3Qge1xcbiAgaGVpZ2h0OiA1dmg7XFxuICBjb2xvcjogYmxhY2s7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5kZWFsLXR5cGUtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmlucHV0LWxhYmVsIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDElOyB9XFxuICAuc2VhcmNoLWJ1dHRvbiB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBtYXJnaW46IDUlIGF1dG87IH0gfVxcbmJvZHkge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBvdmVyZmxvdy14OiBoaWRkZW47IH1cXG4ubG9nby1zZWN0aW9uIHtcXG4gIG1hcmdpbi1ib3R0b206IDclO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XFxuLmxvZ28taW1hZ2Uge1xcbiAgbWF4LXdpZHRoOiAxMjBweDsgfVxcbi5zbGlkZXIge1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmhlYWRlci1tYWluIHtcXG4gIHotaW5kZXg6IDk5OTtcXG4gIHRvcDogM3ZoO1xcbiAgcG9zaXRpb246IGFic29sdXRlOyB9XFxuLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgYm9yZGVyOiB0aGluIHNvbGlkIHdoaXRlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAwO1xcbiAgdG9wOiA2dmg7IH1cXG4uc2xpZGVyLWNvbHVtbiB7XFxuICBwYWRkaW5nOiAwOyB9XFxuLnN1Ym1pdC1ob3VzZS1idXR0b24sXFxuLnN1Ym1pdC1ob3VzZS1idXR0b246dmlzaXRlZCxcXG4uc3VibWl0LWhvdXNlLWJ1dHRvbjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgY3Vyc29yOiBwb2ludGVyOyB9XFxuLmZlYXR1cmVzLXNlY3Rpb24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luLXRvcDogLTV2aDsgfVxcbmlucHV0W3R5cGU9cmFkaW9dIHtcXG4gIGZvbnQtc2l6ZTogMTZweDsgfVxcbi5pbnB1dC1sYWJlbCB7XFxuICBmb250LXdlaWdodDogbGlnaHRlcjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIG1hcmdpbi1sZWZ0OiAxNSU7IH1cXG4uZmVhdHVyZS1ib3gge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICBtYXJnaW4tbGVmdDogMiU7XFxuICBoZWlnaHQ6IDM2dmg7XFxuICB3aWR0aDogMjklICFpbXBvcnRhbnQ7XFxuICBtaW4taGVpZ2h0OiA0MnZoOyB9XFxuLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMikge1xcbiAgbWFyZ2luLWxlZnQ6IDUlOyB9XFxuLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgbWFyZ2luLWxlZnQ6IDUlOyB9XFxuLmZlYXR1cmUtYm94ID4gZGl2ID4gaW1nIHtcXG4gIG1heC13aWR0aDogNzBweDsgfVxcbi53aHktdXMge1xcbiAgbWFyZ2luLXRvcDogNiU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi53aHktdXMtaW1nIHtcXG4gIG1heC13aWR0aDogMzAwcHg7IH1cXG4ud2h5LWxpc3Qge1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgY29sb3I6IGJsYWNrO1xcbiAgcGFkZGluZzogMDsgfVxcbi53aHktbGlzdCA+IGxpIHtcXG4gIG1hcmdpbi10b3A6IDMlOyB9XFxuLndoeS1saXN0ID4gbGkgPiBpIHtcXG4gIG1hcmdpbi1sZWZ0OiAyJTsgfVxcbi53aHktdXMtaW1nLWNvbHVtbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbi5yZWFzb25zLXNlY3Rpb24ge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uYnVpbGRpbmctdHlwZS1sYWJlbCB7XFxuICBoZWlnaHQ6IDV2aDsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgICB3aWR0aDogNDAlO1xcbiAgICBtYXJnaW4tbGVmdDogMSU7IH1cXG4gIC5tb2JpbGUtaGVhZGVyIHtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLmxvZ28taW1hZ2Uge1xcbiAgICBtYXgtd2lkdGg6IDkwcHg7IH1cXG4gIC5tYWluLXRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAyMHB4OyB9XFxuICAuZmVhdHVyZS1ib3gge1xcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1JTtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDIpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDMpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC53aHktdXMtaW1nIHtcXG4gICAgbWF4LXdpZHRoOiAyNDdweDsgfSB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDMyMHB4KSB7XFxuICAubW9iaWxlLWhlYWRlciB7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9TZWFyY2hCb3gvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1NlYXJjaEJveC9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZXNjYXBlKHVybCkge1xuICAgIGlmICh0eXBlb2YgdXJsICE9PSAnc3RyaW5nJykge1xuICAgICAgICByZXR1cm4gdXJsXG4gICAgfVxuICAgIC8vIElmIHVybCBpcyBhbHJlYWR5IHdyYXBwZWQgaW4gcXVvdGVzLCByZW1vdmUgdGhlbVxuICAgIGlmICgvXlsnXCJdLipbJ1wiXSQvLnRlc3QodXJsKSkge1xuICAgICAgICB1cmwgPSB1cmwuc2xpY2UoMSwgLTEpO1xuICAgIH1cbiAgICAvLyBTaG91bGQgdXJsIGJlIHdyYXBwZWQ/XG4gICAgLy8gU2VlIGh0dHBzOi8vZHJhZnRzLmNzc3dnLm9yZy9jc3MtdmFsdWVzLTMvI3VybHNcbiAgICBpZiAoL1tcIicoKSBcXHRcXG5dLy50ZXN0KHVybCkpIHtcbiAgICAgICAgcmV0dXJuICdcIicgKyB1cmwucmVwbGFjZSgvXCIvZywgJ1xcXFxcIicpLnJlcGxhY2UoL1xcbi9nLCAnXFxcXG4nKSArICdcIidcbiAgICB9XG5cbiAgICByZXR1cm4gdXJsXG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi91cmwvZXNjYXBlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi91cmwvZXNjYXBlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub3JtYWxpemUuY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbm9ybWFsaXplLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vcm1hbGl6ZS5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImltcG9ydCB7IG5vdGlmeVN1Y2Nlc3MsIG5vdGlmeUVycm9yLCBnZXRMb2dpbkluZm8gfSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQge1xuICBHRVRfQ1VSUl9VU0VSX1JFUVVFU1QsXG4gIEdFVF9DVVJSX1VTRVJfU1VDQ0VTUyxcbiAgR0VUX0NVUlJfVVNFUl9GQUlMVVJFLFxuICBJTkNSRUFTRV9DUkVESVRfUkVRVUVTVCxcbiAgSU5DUkVBU0VfQ1JFRElUX1NVQ0NFU1MsXG4gIElOQ1JFQVNFX0NSRURJVF9GQUlMVVJFLFxuICBVU0VSX0xPR0lOX1JFUVVFU1QsXG4gIFVTRVJfTE9HSU5fU1VDQ0VTUyxcbiAgVVNFUl9MT0dJTl9GQUlMVVJFLFxuICBVU0VSX0xPR09VVF9SRVFVRVNULFxuICBVU0VSX0xPR09VVF9TVUNDRVNTLFxuICBVU0VSX0xPR09VVF9GQUlMVVJFLFxuICBVU0VSX0lTX05PVF9MT0dHRURfSU4sXG4gIEdFVF9JTklUX0ZBSUxVUkUsXG4gIEdFVF9JTklUX1JFUVVFU1QsXG4gIEdFVF9JTklUX1NVQ0NFU1MsXG4gIEdFVF9JTklUX1VTRVJfRkFJTFVSRSxcbiAgR0VUX0lOSVRfVVNFUl9SRVFVRVNULFxuICBHRVRfSU5JVF9VU0VSX1NVQ0NFU1MsXG59IGZyb20gJy4uL2NvbnN0YW50cyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi9oaXN0b3J5JztcbmltcG9ydCB7XG4gIHNlbmRnZXRDdXJyZW50VXNlclJlcXVlc3QsXG4gIHNlbmRpbmNyZWFzZUNyZWRpdFJlcXVlc3QsXG4gIHNlbmRJbml0aWF0ZVJlcXVlc3QsXG4gIHNlbmRJbml0aWF0ZVVzZXJzUmVxdWVzdCxcbiAgc2VuZExvZ2luUmVxdWVzdCxcbn0gZnJvbSAnLi4vYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uJztcblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR0lOX1JFUVVFU1QsXG4gICAgcGF5bG9hZDoge1xuICAgICAgdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5TdWNjZXNzKGluZm8pIHtcbiAgY29uc29sZS5sb2coaW5mbyk7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dJTl9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgaW5mbyB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5GYWlsdXJlKGVycm9yKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dJTl9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyb3IgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRMb2dpblN0YXR1cygpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGlmIChsb2dpbkluZm8pIHtcbiAgICByZXR1cm4gbG9naW5TdWNjZXNzKGxvZ2luSW5mbyk7XG4gIH1cbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0lTX05PVF9MT0dHRURfSU4sXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldENhcHRjaGFUb2tlbigpIHtcbiAgaWYgKHByb2Nlc3MuZW52LkJST1dTRVIpIHtcbiAgICBjb25zdCBsb2dpbkZvcm0gPSAkKCcjbG9naW5Gb3JtJykuZ2V0KDApO1xuICAgIGlmIChsb2dpbkZvcm0gJiYgdHlwZW9mIGxvZ2luRm9ybVsnZy1yZWNhcHRjaGEtcmVzcG9uc2UnXSA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJldHVybiBsb2dpbkZvcm1bJ2ctcmVjYXB0Y2hhLXJlc3BvbnNlJ10udmFsdWU7XG4gICAgfVxuICB9XG4gIHJldHVybiBudWxsO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5Vc2VyKHVzZXJuYW1lLCBwYXNzd29yZCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gsIGdldFN0YXRlKSB7XG4gICAgZGlzcGF0Y2gobG9naW5SZXF1ZXN0KHVzZXJuYW1lLCBwYXNzd29yZCkpO1xuICAgIHRyeSB7XG4gICAgICAvLyBjb25zdCBjYXB0Y2hhVG9rZW4gPSBnZXRDYXB0Y2hhVG9rZW4oKTtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRMb2dpblJlcXVlc3QodXNlcm5hbWUsIHBhc3N3b3JkKTtcbiAgICAgIGRpc3BhdGNoKGxvZ2luU3VjY2VzcyhyZXMpKTtcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdsb2dpbkluZm8nLCBKU09OLnN0cmluZ2lmeShyZXMpKTtcbiAgICAgIC8vIGNvbnNvbGUubG9nKGxvY2F0aW9uLnBhdGhuYW1lKTtcbiAgICAgIGNvbnN0IHsgcGF0aG5hbWUgfSA9IGdldFN0YXRlKCkuaG91c2U7XG4gICAgICBjb25zb2xlLmxvZyhwYXRobmFtZSk7XG4gICAgICBoaXN0b3J5LnB1c2gocGF0aG5hbWUpO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgZGlzcGF0Y2gobG9naW5GYWlsdXJlKGVycikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dFJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dPVVRfUkVRVUVTVCxcbiAgICBwYXlsb2FkOiB7fSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dFN1Y2Nlc3MoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dPVVRfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7XG4gICAgICBtZXNzYWdlOiAn2K7YsdmI2Kwg2YXZiNmB2YLbjNiqINii2YXbjNiyJyxcbiAgICB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0RmFpbHVyZShlcnJvcikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HT1VUX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnJvciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0VXNlcigpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2gobG9nb3V0UmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgZGlzcGF0Y2gobG9nb3V0U3VjY2VzcygpKTtcbiAgICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xuICAgICAgbG9jYXRpb24uYXNzaWduKCcvbG9naW4nKTtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIGRpc3BhdGNoKGxvZ291dEZhaWx1cmUoZXJyKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZSgpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goaW5pdGlhdGVSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kSW5pdGlhdGVSZXF1ZXN0KCk7XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZVN1Y2Nlc3MocmVzKSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goaW5pdGlhdGVGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfVVNFUl9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2Vyc1N1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9VU0VSX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2Vyc0ZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfVVNFUl9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzKCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChpbml0aWF0ZVVzZXJzUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZEluaXRpYXRlVXNlcnNSZXF1ZXN0KCk7XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZVVzZXJzU3VjY2VzcyhyZXMpKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZVVzZXJzRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfQ1VSUl9VU0VSX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlclN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfQ1VSUl9VU0VSX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXJGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9DVVJSX1VTRVJfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXIodXNlcklkKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCkge1xuICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGdldEN1cnJlbnRVc2VyUmVxdWVzdCh1c2VySWQpO1xuICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXJTdWNjZXNzKHJlcykpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0UmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBJTkNSRUFTRV9DUkVESVRfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0U3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IElOQ1JFQVNFX0NSRURJVF9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0RmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBJTkNSRUFTRV9DUkVESVRfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXQoYW1vdW50KSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChpbmNyZWFzZUNyZWRpdFJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgY3VyclVzZXIgfSA9IGdldFN0YXRlKCkuYXV0aGVudGljYXRpb247XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KGFtb3VudCwgY3VyclVzZXIudXNlcklkKTtcbiAgICAgIGRpc3BhdGNoKGluY3JlYXNlQ3JlZGl0U3VjY2VzcyhyZXMpKTtcbiAgICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyKGN1cnJVc2VyLnVzZXJJZCkpO1xuICAgICAgbm90aWZ5U3VjY2Vzcygn2KfZgdiy2KfbjNi0INin2LnYqtio2KfYsSDYqNinINmF2YjZgdmC24zYqiDYp9mG2KzYp9mFINi02K8uJyk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgbm90aWZ5RXJyb3IoJ9iu2LfYpyDZhdis2K/YryDYqtmE2KfYtCDaqdmG24zYrycpO1xuICAgICAgZGlzcGF0Y2goaW5jcmVhc2VDcmVkaXRGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uLmpzIiwiaW1wb3J0IHtcbiAgR0VUX0hPVVNFU19SRVFVRVNULFxuICBHRVRfSE9VU0VTX1NVQ0NFU1MsXG4gIEdFVF9IT1VTRVNfRkFJTFVSRSxcbiAgUE9MTF9IT1VTRVNfUkVRVUVTVCxcbiAgUE9MTF9IT1VTRVNfU1VDQ0VTUyxcbiAgUE9MTF9IT1VTRVNfRkFJTFVSRSxcbiAgU0VUX1NFQVJDSF9PQkpfUkVRVUVTVCxcbiAgU0VUX1NFQVJDSF9PQkpfU1VDQ0VTUyxcbiAgU0VUX1NFQVJDSF9PQkpfRkFJTFVSRSxcbn0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uL2hpc3RvcnknO1xuaW1wb3J0IHtcbiAgc2VuZFNlYXJjaEhvdXNlc1JlcXVlc3QsXG4gIHNlbmRwb2xsSG91c2VzUmVxdWVzdCxcbn0gZnJvbSAnLi4vYXBpLWhhbmRsZXJzL3NlYXJjaCc7XG5pbXBvcnQgeyBub3RpZnlFcnJvciwgbm90aWZ5U3VjY2VzcywgZ2V0Rm9ybURhdGEgfSBmcm9tICcuLi91dGlscyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRTZWFyY2hPYmpSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFNFVF9TRUFSQ0hfT0JKX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRTZWFyY2hPYmpTdWNjZXNzKG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogU0VUX1NFQVJDSF9PQkpfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRTZWFyY2hPYmpGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFNFVF9TRUFSQ0hfT0JKX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNldFNlYXJjaE9iaihtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChzZXRTZWFyY2hPYmpTdWNjZXNzKG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlKSk7XG4gICAgaGlzdG9yeS5wdXNoKCcvaG91c2VzJyk7XG4gIH07XG59XG5leHBvcnQgZnVuY3Rpb24gc2VhcmNoSG91c2VzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSE9VU0VTX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hIb3VzZXNTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0hPVVNFU19TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlYXJjaEhvdXNlc0ZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0hPVVNFU19GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hIb3VzZXMobWluQXJlYSwgZGVhbFR5cGUsIGJ1aWxkaW5nVHlwZSwgbWF4UHJpY2UpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goc2VhcmNoSG91c2VzUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgaWYgKCFidWlsZGluZ1R5cGUpIHtcbiAgICAgICAgYnVpbGRpbmdUeXBlID0gbnVsbDtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRTZWFyY2hIb3VzZXNSZXF1ZXN0KFxuICAgICAgICBwYXJzZUZsb2F0KG1pbkFyZWEpLFxuICAgICAgICBwYXJzZUZsb2F0KGRlYWxUeXBlKSxcbiAgICAgICAgYnVpbGRpbmdUeXBlLFxuICAgICAgICBwYXJzZUZsb2F0KG1heFByaWNlKSxcbiAgICAgICk7XG4gICAgICBkaXNwYXRjaChzZWFyY2hIb3VzZXNTdWNjZXNzKHJlcykpO1xuICAgICAgZGlzcGF0Y2goc2V0U2VhcmNoT2JqKG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlKSk7XG4gICAgICBpZiAocmVzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICBub3RpZnlTdWNjZXNzKCfYrtin2YbZhyDYp9uMINio2Kcg2KfbjNmGINin24zZhiDZhdi02K7Ytdin2Kog2KvYqNiqINmG2LTYr9mHINin2LPYqicpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKHNlYXJjaEhvdXNlc0ZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBwb2xsSG91c2VzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBQT0xMX0hPVVNFU19SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcG9sbEhvdXNlc1N1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBQT0xMX0hPVVNFU19TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBvbGxIb3VzZXNGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFBPTExfSE9VU0VTX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBvbGxIb3VzZXMobWluQXJlYSwgZGVhbFR5cGUsIGJ1aWxkaW5nVHlwZSwgbWF4UHJpY2UpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKHBvbGxIb3VzZXNSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBpZiAoIWJ1aWxkaW5nVHlwZSkge1xuICAgICAgICBidWlsZGluZ1R5cGUgPSBudWxsO1xuICAgICAgfVxuICAgICAgbGV0IHJlcyA9IGF3YWl0IHNlbmRwb2xsSG91c2VzUmVxdWVzdChcbiAgICAgICAgcGFyc2VGbG9hdChtaW5BcmVhKSxcbiAgICAgICAgcGFyc2VGbG9hdChkZWFsVHlwZSksXG4gICAgICAgIGJ1aWxkaW5nVHlwZSxcbiAgICAgICAgcGFyc2VGbG9hdChtYXhQcmljZSksXG4gICAgICApO1xuICAgICAgY29uc29sZS5sb2cocmVzKTtcbiAgICAgIGlmICghcmVzKSB7XG4gICAgICAgIHJlcyA9IGdldFN0YXRlKCkuc2VhcmNoLmhvdXNlcztcbiAgICAgICAgY29uc29sZS5sb2coJ2hhbGEnKTtcbiAgICAgICAgY29uc29sZS5sb2cocmVzKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKHBvbGxIb3VzZXNTdWNjZXNzKHJlcykpO1xuICAgICAgbm90aWZ5RXJyb3IoJ9in2LfZhNin2LnYp9iqINis2LPYqiDZiCDYrNmI24wg2LTZhdinINio2LHZiNiyINi02K8uJyk7XG5cbiAgICAgIGRpc3BhdGNoKHNldFNlYXJjaE9iaihtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSkpO1xuICAgICAgaWYgKHJlcy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgbm90aWZ5U3VjY2Vzcygn2K7Yp9mG2Ycg2KfbjCDYqNinINin24zZhiDYp9uM2YYg2YXYtNiu2LXYp9iqINir2KjYqiDZhti02K/ZhyDYp9iz2KonKTtcbiAgICAgIH1cbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChwb2xsSG91c2VzRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYWN0aW9ucy9zZWFyY2guanMiLCJpbXBvcnQgeyBmZXRjaEFwaSwgZ2V0R2V0Q29uZmlnLCBnZXRQb3N0Q29uZmlnIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHtcbiAgZ2V0Q3VycmVudFVzZXJVcmwsXG4gIGdldEluaXRpYXRlVXJsLFxuICBsb2dpbkFwaVVybCxcbiAgaW5jcmVhc2VDcmVkaXRVcmwsXG59IGZyb20gJy4uL2NvbmZpZy9wYXRocyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KGFtb3VudCwgaWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGluY3JlYXNlQ3JlZGl0VXJsKGFtb3VudCwgaWQpLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KHVzZXJJZCkge1xuICByZXR1cm4gZmV0Y2hBcGkoZ2V0Q3VycmVudFVzZXJVcmwodXNlcklkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZEluaXRpYXRlUmVxdWVzdCgpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEluaXRpYXRlVXJsLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSB7XG4gIHJldHVybiBmZXRjaEFwaShsb2dpbkFwaVVybCwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZExvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKFxuICAgIGxvZ2luQXBpVXJsLFxuICAgIGdldFBvc3RDb25maWcoe1xuICAgICAgdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICB9KSxcbiAgKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwiaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7XG4gIGZldGNoQXBpLFxuICBnZXRHZXRDb25maWcsXG4gIGdldFB1dENvbmZpZyxcbiAgZ2V0UG9zdENvbmZpZyxcbiAgZ2V0TG9naW5JbmZvLFxufSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQgeyBnZXRTZWFyY2hIb3VzZXNVcmwsIGdldEluaXRpYXRlVXJsIH0gZnJvbSAnLi4vY29uZmlnL3BhdGhzJztcbi8qKlxuICogQGFwaSB7Z2V0fSAvdjIvYWNxdWlzaXRpb24vZHJpdmVyL3Bob25lTnVtYmVyLzpwaG9uZU51bWJlclxuICogQGFwaU5hbWUgRHJpdmVyQWNxdWlzaXRpb25TZWFyY2hCeVBob25lTnVtYmVyXG4gKiBAYXBpVmVyc2lvbiAyLjAuMFxuICogQGFwaUdyb3VwIERyaXZlclxuICogQGFwaVBlcm1pc3Npb24gRklFTEQtQUdFTlQsIFRSQUlORVIsIEFETUlOXG4gKiBAYXBpUGFyYW0ge1N0cmluZ30gcGhvbmVOdW1iZXIgcGhvbmUgbnVtYmVyIHRvIHNlYXJjaCAoKzk4OTEyMzQ1Njc4OSwgMDkxMjM0NTY3ODkpXG4gKiBAYXBpU3VjY2VzcyB7U3RyaW5nfSByZXN1bHQgQVBJIHJlcXVlc3QgcmVzdWx0XG4gKiBAYXBpU3VjY2Vzc0V4YW1wbGUge2pzb259IFN1Y2Nlc3MtUmVzcG9uc2U6XG4ge1xuICAgXCJyZXN1bHRcIjogXCJPS1wiLFxuICAgXCJkYXRhXCI6IHtcbiAgICAgICBcImRyaXZlcnNcIjogW1xuICAgICAgICB7XG4gICAgICAgICAgXCJpZFwiOiAxMCxcbiAgICAgICAgICBcImZ1bGxOYW1lXCI6IFwi2LnZhNuM2LHYttinINmC2LHYqNin2YbbjFwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkXCI6IDEyLFxuICAgICAgICAgIFwiZnVsbE5hbWVcIjogXCLYudmE24zYsdi22Kcg2YLYsdio2KfZhtuMXCJcbiAgICAgICAgfVxuICAgICAgIF1cbiAgIH1cbiB9XG4gKi9cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRTZWFyY2hIb3VzZXNSZXF1ZXN0KFxuICBtaW5BcmVhLFxuICBkZWFsVHlwZSxcbiAgYnVpbGRpbmdUeXBlLFxuICBtYXhQcmljZSxcbikge1xuICByZXR1cm4gZmV0Y2hBcGkoXG4gICAgZ2V0U2VhcmNoSG91c2VzVXJsLFxuICAgIGdldFBvc3RDb25maWcoeyBtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSB9KSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRwb2xsSG91c2VzUmVxdWVzdChcbiAgbWluQXJlYSxcbiAgZGVhbFR5cGUsXG4gIGJ1aWxkaW5nVHlwZSxcbiAgbWF4UHJpY2UsXG4pIHtcbiAgcmV0dXJuIGZldGNoQXBpKFxuICAgIGdldEluaXRpYXRlVXJsLFxuICAgIGdldFBvc3RDb25maWcoeyBtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSB9KSxcbiAgKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL3NlYXJjaC5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmc/MmZjMmQ1YzFcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZz82ZmEyODA0Y1wiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHR3aXR0ZXJJY29uIGZyb20gJy4vVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nJztcbmltcG9ydCBpbnN0YWdyYW1JY29uIGZyb20gJy4vMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nJztcbmltcG9ydCB0ZWxlZ3JhbUljb24gZnJvbSAnLi8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmcnO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5cbmNsYXNzIEZvb3RlciBleHRlbmRzIENvbXBvbmVudCB7XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGZvb3RlciBjbGFzc05hbWU9XCJzaXRlLWZvb3RlciBjZW50ZXItY29sdW1uIGRhcmstZ3JlZW4tYmFja2dyb3VuZCB3aGl0ZVwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS00IHRleHQtYWxpZ24tbGVmdFwiPlxuICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cInNvY2lhbC1pY29ucy1saXN0IG5vLWxpc3Qtc3R5bGVcIj5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17aW5zdGFncmFtSWNvbn1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNvY2lhbC1uZXR3b3JrLWljb25cIlxuICAgICAgICAgICAgICAgICAgYWx0PVwiaW5zdGFncmFtLWljb25cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e3R3aXR0ZXJJY29ufVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic29jaWFsLW5ldHdvcmstaWNvblwiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJ0d2l0dGVyLWljb25cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e3RlbGVncmFtSWNvbn1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNvY2lhbC1uZXR3b3JrLWljb25cIlxuICAgICAgICAgICAgICAgICAgYWx0PVwidGVsZWdyYW0taWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTggdGV4dC1hbGlnbi1yaWdodFwiPlxuICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgINiq2YXYp9mF24wg2K3ZgtmI2YIg2KfbjNmGINmI2KjigIzYs9in24zYqiDZhdiq2LnZhNmCINio2Ycg2YXZh9ix2YbYp9iyINir2KfYqNiqINmIINmH2YjZhdmGINi02LHbjNmBINiy2KfYr9mHXG4gICAgICAgICAgICAgINmF24zigIzYqNin2LTYry5cbiAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Zvb3Rlcj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoRm9vdGVyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvRm9vdGVyL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZz8yNDhiZmE4ZFwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS83MjY0NDYuc3ZnP2VhNzZhNmI5XCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS83MjY0NDYuc3ZnXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlLzcyNjQ0Ni5zdmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvNzI2NDg4LnN2Zz85ZDIwMDk3MVwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvNzI2NDg4LnN2Z1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS83MjY0ODguc3ZnXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlLzcyNjQ5OS5zdmc/NTM4ZWNmMTNcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlLzcyNjQ5OS5zdmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvNzI2NDk5LnN2Z1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5pbXBvcnQgbG9nbyBmcm9tICcuL2xvZ28ucG5nJztcbmltcG9ydCBmZWF0dXJlSWNvbjEgZnJvbSAnLi83MjY0OTkuc3ZnJztcbmltcG9ydCBmZWF0dXJlSWNvbjIgZnJvbSAnLi83MjY0ODguc3ZnJztcbmltcG9ydCBmZWF0dXJlSWNvbjMgZnJvbSAnLi83MjY0NDYuc3ZnJztcbmltcG9ydCB3aHlVc0ltZyBmcm9tICcuL3doeS1raGFuZWJlZG9vc2guanBnJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uLy4uL2hpc3RvcnknO1xuaW1wb3J0IFNlYXJjaEJveCBmcm9tICcuLi9TZWFyY2hCb3gnO1xuXG5jbGFzcyBIb21lUGFnZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlU3VibWl0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaG9tZS1wYW5lbFwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIHNsaWRlci1jb2x1bW5cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2xpZGVyIGNlbnRlci1jb250ZW50XCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2xpZGUxXCIgLz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzbGlkZTJcIiAvPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNsaWRlM1wiIC8+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2xpZGU0XCIgLz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTEyIGNvbC1tZC04IG1haW4tZm9ybVwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGhpZGRlbi1zbSB2aXNpYmxlLXhzIG1vYmlsZS1oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleC1jZW50ZXIgY29sLW1kLTIgICBuYXYtdXNlciBjb2wteHMtMTIgZHJvcGRvd25cIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1jZW50ZXIgaGVhZGVyLW1haW4tYm9hcmRlclwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTkgXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBwZXJzaWFuIFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICDZhtin2K3bjNmHINqp2KfYsdio2LHbjFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9oNT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZHJvcGRvd24tY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwicGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsdWUtZm9udCBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KjZh9mG2KfZhSDZh9mF2KfbjNmI2YZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTYgcHVsbC1sZWZ0IGJsdWUtZm9udCBwZXJzaWFuXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPtuy27DbsNuw27A8L3NwYW4+2KrZiNmF2KfZhlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IHB1bGwtcmlnaHQgYmx1ZS1mb250IHBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINin2LnYqtio2KfYsVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvaW5jcmVhc2VDcmVkaXQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuby1wYWRkaW5nICBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtc21pbGUtbyBmYS0yeCB3aGl0ZS1mb250XCIgLz5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBsb2dvLXNlY3Rpb25cIj5cbiAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtsb2dvfSBhbHQ9XCJsb2dvXCIgY2xhc3NOYW1lPVwibG9nby1pbWFnZVwiIC8+XG4gICAgICAgICAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwibWFpbi10aXRsZSB3aGl0ZSBmb250LW1lZGl1bVwiPtiu2KfZhtmHINio2Ycg2K/ZiNi0PC9oMj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxTZWFyY2hCb3ggaGFuZGxlU3VibWl0PXt0aGlzLnByb3BzLmhhbmRsZVN1Ym1pdH0gLz5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBzdWJtaXQtaG9tZS1saW5rIGJvcmRlci1yYWRpdXMgdGV4dC1hbGlnbi1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJzdWJtaXQtaG9tZS1oZWFkaW5nIHdoaXRlXCI+XG4gICAgICAgICAgICAgICAgICAgINi12KfYrdioINiu2KfZhtmHINmH2LPYqtuM2K/YnyDYrtin2YbZhyDYrtmI2K8g2LHYp1xuICAgICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInN1Ym1pdC1ob3VzZS1idXR0b24gd2hpdGVcIlxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnL2hvdXNlcy9uZXcnKTtcbiAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAg2KvYqNiqXG4gICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAg2qnZhtuM2K8uXG4gICAgICAgICAgICAgICAgICA8L2g1PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgZmVhdHVyZXMtc2VjdGlvbiBjZW50ZXItY29udGVudFwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tMTIgY29sLW1kLTggZmVhdHVyZXMtY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTEyIGNvbC1tZC0zIGZlYXR1cmUtYm94IHRleHQtYWxpZ24tY2VudGVyIGJvcmRlci1yYWRpdXMgd2hpdGUtYmFja2dyb3VuZCBjZW50ZXItY29sdW1uXCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZmVhdHVyZUljb24xfSBhbHQ9XCJmZWF0dXJlLWljb25cIiAvPlxuICAgICAgICAgICAgICAgICAgICA8aDE+2q/Ys9iq2LHYr9mHPC9oMT5cbiAgICAgICAgICAgICAgICAgICAgPHAgZGlyPVwicnRsXCI+2K/YsSDZhdmG2LfZgtmHINmF2YjYsdivINi52YTYp9mC2Ycg2K7ZiNivINi12KfYrdioINiu2KfZhtmHINi02YjbjNivPC9wPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTEyIGNvbC1tZC0zIGZlYXR1cmUtYm94IHRleHQtYWxpZ24tY2VudGVyIGJvcmRlci1yYWRpdXMgd2hpdGUtYmFja2dyb3VuZCBjZW50ZXItY29sdW1uXCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZmVhdHVyZUljb24yfSBhbHQ9XCJmZWF0dXJlLWljb25cIiAvPlxuICAgICAgICAgICAgICAgICAgICA8aDE+2YXYt9mF2KbZhjwvaDE+XG4gICAgICAgICAgICAgICAgICAgIDxwIGRpcj1cInJ0bFwiPtio2Kcg2K7bjNin2YQg2LHYp9it2Kog2KjZhyDYr9mG2KjYp9mEINiu2KfZhtmHINio2q/Ysdiv24zYrzwvcD5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS0xMiBjb2wtbWQtMyBmZWF0dXJlLWJveCB0ZXh0LWFsaWduLWNlbnRlciBib3JkZXItcmFkaXVzIHdoaXRlLWJhY2tncm91bmQgY2VudGVyLWNvbHVtblwiPlxuICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2ZlYXR1cmVJY29uM30gYWx0PVwiZmVhdHVyZS1pY29uXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPGgxPtii2LPYp9mGPC9oMT5cbiAgICAgICAgICAgICAgICAgICAgPHAgZGlyPVwicnRsXCI+2KjZhyDYs9in2K/ar9uMINi12KfYrdioINiu2KfZhtmHINi02YjbjNivPC9wPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZCB3aHktdXNcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY2VudGVyLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTEyIGNvbC1tZC04IHdoeS11cy10aXRsZSB0ZXh0LWFsaWduLXJpZ2h0XCI+XG4gICAgICAgICAgICAgICAgPGgxIGNsYXNzTmFtZT1cInJlYXNvbnMtaGVhZCBmb250LWJvbGRcIj7ahtix2Kcg2K7Yp9mG2Ycg2KjZhyDYr9mI2LTYnzwvaDE+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjZW50ZXItY29udGVudFwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tMTIgY29sLW1kLThcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tMTIgY29sLW1kLTYgd2h5LXVzLWltZy1jb2x1bW5cIj5cbiAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXt3aHlVc0ltZ30gYWx0PVwid2h5LWltZ1wiIGNsYXNzTmFtZT1cIndoeS11cy1pbWdcIiAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS0xMiBjb2wtbWQtNiByZWFzb25zLXNlY3Rpb25cIj5cbiAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJ3aHktbGlzdFwiPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtY2hlY2stY2lyY2xlIHB1cnBsZVwiIC8+2KfYt9mE2KfYudin2Kog2qnYp9mF2YQg2YhcbiAgICAgICAgICAgICAgICAgICAgICDYtdit24zYrSDYp9iyINin2YXZhNin2qkg2YLYp9io2YQg2YXYudin2YXZhNmHXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1jaGVjay1jaXJjbGUgcHVycGxlXCIgLz7YqNiv2YjZhiDZhdit2K/ZiNiv24zYqtiMXG4gICAgICAgICAgICAgICAgICAgICAg27LbtCDYs9in2LnYqtmHINmIINiv2LEg2KrZhdin2YUg2KfbjNin2YUg2YfZgdiq2YdcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLWNoZWNrLWNpcmNsZSBwdXJwbGVcIiAvPtis2LPYqtis2YjbjCDZh9mI2LTZhdmG2K9cbiAgICAgICAgICAgICAgICAgICAgICDZhdmE2qnYjCDYtdix2YHZh+KAjNis2YjbjNuMINiv2LEg2LLZhdin2YZcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLWNoZWNrLWNpcmNsZSBwdXJwbGVcIiAvPtiq2YbZiNi5INiv2LEg2KfZhdmE2KfaqdiMXG4gICAgICAgICAgICAgICAgICAgICAg2KfZgdiy2KfbjNi0INmC2K/YsdiqINin2YbYqtiu2KfYqCDZhdi02KrYsduM2KfZhlxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtY2hlY2stY2lyY2xlIHB1cnBsZVwiIC8+2KjYp9mG2qnbjCDYrNin2YXYuSDYp9iyXG4gICAgICAgICAgICAgICAgICAgICAg2KfYt9mE2KfYudin2Kog2YfYstin2LHYp9mGINii2q/Zh9uMINio2Ycg2LHZiNiyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1jaGVjay1jaXJjbGUgcHVycGxlXCIgLz7Yr9iz2KrbjNin2KjbjCDYqNmHXG4gICAgICAgICAgICAgICAgICAgICAg2YbYqtuM2KzZhyDZhdi32YTZiNioINiv2LEg2qnZhdiq2LHbjNmGINiy2YXYp9mGINmF2YXaqdmGXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1jaGVjay1jaXJjbGUgcHVycGxlXCIgLz7Zh9mF2qnYp9ix24wg2KjYp1xuICAgICAgICAgICAgICAgICAgICAgINmF2LTYp9mI2LHYp9mGINmF2KrYrti12LUg2K/YsSDYrdmI2LLZhyDYp9mF2YTYp9qpXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShIb21lUGFnZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvSG9tZVBhZ2UuanMiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9jYXNleS1ob3JuZXItNTMzNTg2LXVuc3BsYXNoLmpwZz9kNTExM2M4MlwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvY2FzZXktaG9ybmVyLTUzMzU4Ni11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvY2FzZXktaG9ybmVyLTUzMzU4Ni11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbG9nby5wbmc/MmFmNzNkOGFcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL2xvZ28ucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL2xvZ28ucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL2x1a2UtdmFuLXp5bC01MDQwMzItdW5zcGxhc2guanBnPzI4Y2JjZjliXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9sdWtlLXZhbi16eWwtNTA0MDMyLXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9sdWtlLXZhbi16eWwtNTA0MDMyLXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9tYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGc/NTg3Mjc2ZDJcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL21haGRpYXItbWFobW9vZGktNDUyNDg5LXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9tYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbWljaGFsLWt1YmFsY3p5ay0yNjA5MDktdW5zcGxhc2guanBnPzNhMjQ2YTE4XCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS9taWNoYWwta3ViYWxjenlrLTI2MDkwOS11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG9tZVBhZ2UvbWljaGFsLWt1YmFsY3p5ay0yNjA5MDktdW5zcGxhc2guanBnXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0hvbWVQYWdlL3doeS1raGFuZWJlZG9vc2guanBnP2Y4ZmY4Y2NjXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS93aHkta2hhbmViZWRvb3NoLmpwZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib21lUGFnZS93aHkta2hhbmViZWRvb3NoLmpwZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xheW91dC5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xheW91dC5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLyoqXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXG4gKlxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBub3JtYWxpemVDc3MgZnJvbSAnbm9ybWFsaXplLmNzcyc7XG5pbXBvcnQgcyBmcm9tICcuL0xheW91dC5jc3MnO1xuaW1wb3J0IEZvb3RlciBmcm9tICcuLi9Gb290ZXInO1xuaW1wb3J0IFBhbmVsIGZyb20gJy4uL1BhbmVsJztcblxuY2xhc3MgTGF5b3V0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBtYWluUGFuZWw6IFByb3BUeXBlcy5ib29sLFxuICB9O1xuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIG1haW5QYW5lbDogZmFsc2UsXG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbWQtMTIgY29sLXhzLTEyXCIgc3R5bGU9e3sgcGFkZGluZzogJzAnIH19PlxuICAgICAgICA8UGFuZWwgbWFpblBhbmVsPXt0aGlzLnByb3BzLm1haW5QYW5lbH0gLz5cbiAgICAgICAge3RoaXMucHJvcHMuY2hpbGRyZW59XG4gICAgICAgIDxGb290ZXIgLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhub3JtYWxpemVDc3MsIHMpKExheW91dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5qcyIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9Mb2FkZXIuc2Nzcyc7XG5pbXBvcnQgZ2lmRmlsZSBmcm9tICcuL2xvYWRlci5naWYnO1xuXG5jbGFzcyBMb2FkZXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGxvYWRpbmc6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3RoaXMucHJvcHMubG9hZGluZyA/ICdsb2FkZXIgbG9hZGluZycgOiAnbG9hZGVyJ30+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGltZyBzcmM9e2dpZkZpbGV9IGFsdD1cImxvYWRlciBnaWZcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsb2FkZXItaW5mb1wiPtmF2YbYqti42LEg2KjYp9i024zYrzwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKExvYWRlcik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5qcyIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTG9hZGVyLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTG9hZGVyLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9Mb2FkZXIuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuc2Nzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDYiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Mb2FkZXIvbG9hZGVyLmdpZj9iZTVjMzIyM1wiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL2xvYWRlci5naWZcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL2xvYWRlci5naWZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDYiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCAqIGFzIGF1dGhBY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMvYXV0aGVudGljYXRpb24nO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5pbXBvcnQgbG9nb0ltZyBmcm9tICcuL2xvZ28ucG5nJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uLy4uL2hpc3RvcnknO1xuXG5jbGFzcyBQYW5lbCBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY3VyclVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBhdXRoQWN0aW9uczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIG1haW5QYW5lbDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgICBpc0xvZ2dlZEluOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnByb3BzLmN1cnJVc2VyKTtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnByb3BzLmlzTG9nZ2VkSW4pO1xuICAgIGlmICh0aGlzLnByb3BzLm1haW5QYW5lbCkge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPGhlYWRlciBjbGFzc05hbWU9XCJoaWRkZW4teHNcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaGVhZGVyLW1haW5cIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4LWNlbnRlciBjb2wtbWQtMiAgIG5hdi11c2VyIGNvbC14cy0xMiBkcm9wZG93biBcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1jZW50ZXIgaGVhZGVyLW1haW4tYm9hcmRlclwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy05IFwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IHdoaXRlLWZvbnQgcGVyc2lhbiBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgINmG2KfYrduM2Ycg2qnYp9ix2KjYsduMXG4gICAgICAgICAgICAgICAgICAgICAgPC9oNT5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImRyb3Bkb3duLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBibGFjay1mb250IFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLnVzZXJuYW1lfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IHB1bGwtbGVmdCBwZXJzaWFuIGxpZ2h0LWdyZXktZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImxpZ2h0LWdyZXktZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KrZiNmF2KfZhlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIuYmFsYW5jZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IGxpZ2h0LWdyZXktZm9udCBwdWxsLXJpZ2h0ICBwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINin2LnYqtio2KfYsVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItin2YHYstin24zYtCDYp9i52KrYqNin2LFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnL2luY3JlYXNlQ3JlZGl0Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2K7YsdmI2KxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuYXV0aEFjdGlvbnMubG9nb3V0VXNlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICB7IXRoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLZiNix2YjYr1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9sb2dpbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vLXBhZGRpbmcgIGNvbC14cy0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtc21pbGUtbyBmYS0yeCB3aGl0ZS1mb250XCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9oZWFkZXI+XG4gICAgICApO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgPGhlYWRlcj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBuYXYtY2xcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleC1jZW50ZXIgY29sLW1kLTIgICBuYXYtdXNlciBjb2wteHMtMTIgZHJvcGRvd25cIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBmbGV4LWNlbnRlciBcIj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTkgXCI+XG4gICAgICAgICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IHBlcnNpYW4gcHVycGxlLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICDZhtin2K3bjNmHINqp2KfYsdio2LHbjFxuICAgICAgICAgICAgICAgICAgICA8L2g1PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImRyb3Bkb3duLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwiYmxhY2stZm9udCBwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgYmx1ZS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLnVzZXJuYW1lfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCIgbGlnaHQtZ3JleS1mb250IGNvbC14cy02IHB1bGwtbGVmdCBibHVlLWZvbnQgcGVyc2lhblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYqtmI2YXYp9mGXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIuYmFsYW5jZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxpZ2h0LWdyZXktZm9udCBjb2wteHMtNiBwdWxsLXJpZ2h0IGJsdWUtZm9udCBwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYp9i52KrYqNin2LFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9pbmNyZWFzZUNyZWRpdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2K7YsdmI2KxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5hdXRoQWN0aW9ucy5sb2dvdXRVc2VyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICB7IXRoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2YjYsdmI2K9cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9sb2dpbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vLXBhZGRpbmcgIGNvbC14cy0zXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXNtaWxlLW8gZmEtMnggcHVycGxlLWZvbnRcIiAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgtY2VudGVyIGNvbC1tZC00IGNvbC1tZC1vZmZzZXQtNiBuYXYtbG9nbyBjb2wteHMtMTJcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyAgXCI+XG4gICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmbGV4LXJvdy1yZXZcIlxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvJyk7XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibm8tcGFkZGluZyB0ZXh0LWFsaWduLWNlbnRlciBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwiaWNvbi1jbFwiIHNyYz17bG9nb0ltZ30gYWx0PVwibG9nb1wiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy05XCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4tYm9sZCB0ZXh0LWFsaWduLXJpZ2h0ICBwZXJzaWFuIGJsdWUtZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAg2K7Yp9mG2Ycg2KjZhyDYr9mI2LRcbiAgICAgICAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2hlYWRlcj5cbiAgICApO1xuICB9XG59XG5mdW5jdGlvbiBtYXBEaXNwYXRjaFRvUHJvcHMoZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIHtcbiAgICBhdXRoQWN0aW9uczogYmluZEFjdGlvbkNyZWF0b3JzKGF1dGhBY3Rpb25zLCBkaXNwYXRjaCksXG4gIH07XG59XG5cbmZ1bmN0aW9uIG1hcFN0YXRlVG9Qcm9wcyhzdGF0ZSkge1xuICBjb25zdCB7IGF1dGhlbnRpY2F0aW9uIH0gPSBzdGF0ZTtcbiAgY29uc3QgeyBjdXJyVXNlcixpc0xvZ2dlZEluIH0gPSBhdXRoZW50aWNhdGlvbjtcbiAgcmV0dXJuIHtcbiAgICBjdXJyVXNlcixcbiAgICBpc0xvZ2dlZEluLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzKShQYW5lbCksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL1BhbmVsL1BhbmVsLmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmc/MmFmNzNkOGFcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLyogZXNsaW50LWRpc2FibGUgcmVhY3Qvbm8tZGFuZ2VyICovXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBzIGZyb20gJy4vbWFpbi5jc3MnO1xuXG5jbGFzcyBTZWFyY2hCb3ggZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGhhbmRsZVN1Ym1pdDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgfTtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIG1heFByaWNlOiAnJyxcbiAgICAgIG1pbkFyZWE6ICcnLFxuICAgICAgYnVpbGRpbmdUeXBlOiAnJyxcbiAgICAgIGRlYWxUeXBlOiAnJyxcbiAgICB9O1xuICB9XG4gIGhhbmRsZVN1Ym1pdCA9IGUgPT4ge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICB0aGlzLnByb3BzLmhhbmRsZVN1Ym1pdChcbiAgICAgIHRoaXMuc3RhdGUubWluQXJlYSxcbiAgICAgIHRoaXMuc3RhdGUuZGVhbFR5cGUsXG4gICAgICB0aGlzLnN0YXRlLmJ1aWxkaW5nVHlwZSxcbiAgICAgIHRoaXMuc3RhdGUubWF4UHJpY2UsXG4gICAgKTtcbiAgfTtcbiAgdmFsaWRhdGUgPSAobWF4UHJpY2UsIG1pbkFyZWEpID0+IHtcbiAgICAvLyB0cnVlIG1lYW5zIGludmFsaWQsIHNvIG91ciBjb25kaXRpb25zIGdvdCByZXZlcnNlZFxuICAgIGNvbnN0IHJlZ2V4ID0gbmV3IFJlZ0V4cCgvW14wLTldLywgJ2cnKTtcbiAgICByZXR1cm4ge1xuICAgICAgbWF4UHJpY2U6IG1heFByaWNlLm1hdGNoKHJlZ2V4KSxcbiAgICAgIG1pbkFyZWE6IG1pbkFyZWEubWF0Y2gocmVnZXgpLFxuICAgIH07XG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBlcnJvcnMgPSB0aGlzLnZhbGlkYXRlKHRoaXMuc3RhdGUubWF4UHJpY2UsIHRoaXMuc3RhdGUubWluQXJlYSk7XG4gICAgY29uc3QgaXNFbmFibGVkID0gIU9iamVjdC5rZXlzKGVycm9ycykuc29tZSh4ID0+IGVycm9yc1t4XSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgc2VhcmNoLXNlY3Rpb24gYm9yZGVyLXJhZGl1cyBuby1wYWRkaW5nXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGZpcnN0LXBhcnQgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS00XCI+XG4gICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwibGFiZWxcIj7Zhdiq2LEg2YXYsdio2Lk8L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwi2K3Yr9in2YLZhCDZhdiq2LHYp9qYXCJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBuYW1lPVwibWluQXJlYVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17YHNlYXJjaC1pbnB1dCBib3JkZXItcmFkaXVzIGNsZWFyLWlucHV0ICR7XG4gICAgICAgICAgICAgICAgZXJyb3JzLm1pbkFyZWEgPyAnZXJyb3InIDogJydcbiAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgZGlyPVwicnRsXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICB7ZXJyb3JzLm1pbkFyZWEgJiYgPHAgY2xhc3NOYW1lPVwiZXJyLW1zZ1wiPtmE2LfZgdinINi52K/YryDZiNin2LHYryDaqdmG24zYry48L3A+fVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS00XCI+XG4gICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwibGFiZWxcIj7YqtmI2YXYp9mGPC9sYWJlbD5cbiAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cItit2K/Yp9qp2KvYsSDZgtuM2YXYqlwiXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgbmFtZT1cIm1heFByaWNlXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgc2VhcmNoLWlucHV0IGJvcmRlci1yYWRpdXMgY2xlYXItaW5wdXQgJHtcbiAgICAgICAgICAgICAgICBlcnJvcnMubWF4UHJpY2UgPyAnZXJyb3InIDogJydcbiAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgZGlyPVwicnRsXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICB7ZXJyb3JzLm1heFByaWNlICYmIDxwIGNsYXNzTmFtZT1cImVyci1tc2dcIj7ZhNi32YHYpyDYudiv2K8g2YjYp9ix2K8g2qnZhtuM2K8uPC9wPn1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tNFwiPlxuICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImJ1aWxkaW5nLXR5cGUtbGFiZWxcIiAvPlxuICAgICAgICAgICAgPHNlbGVjdFxuICAgICAgICAgICAgICB0aXRsZT1cImJ1aWxkaW5nVHlwZVwiXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgbmFtZT1cImJ1aWxkaW5nVHlwZVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNlYXJjaC1pbnB1dCBib3JkZXItcmFkaXVzIGNsZWFyLWlucHV0XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPG9wdGlvbiBkaXNhYmxlZCBzZWxlY3RlZCBjbGFzc05hbWU9XCJzZWxlY3QtcGxhY2UtaG9sZGVyXCI+XG4gICAgICAgICAgICAgICAg2YbZiNi5INmF2YTaqVxuICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgPG9wdGlvbj7Yotm+2KfYsdiq2YXYp9mGIDwvb3B0aW9uPlxuICAgICAgICAgICAgICA8b3B0aW9uPtmI24zZhNin24zbjDwvb3B0aW9uPlxuICAgICAgICAgICAgPC9zZWxlY3Q+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBzZWNvbmQtcGFydFwiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidmlzaWJsZS1tZCAgdmlzaWJsZS1sZyBjb2wtbWQtNiBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5oYW5kbGVTdWJtaXR9PlxuICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFpc0VuYWJsZWR9XG4gICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2VhcmNoLWJ1dHRvbiBoYXMtdHJhbnNpdGlvbiBib3JkZXItcmFkaXVzIGNsZWFyLWlucHV0IHdoaXRlIGxpZ2h0LWJsdWUtYmFja2dyb3VuZFwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICDYrNiz2KrYrNmIXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPC9mb3JtPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1tZC02IGRlYWwtdHlwZS1zZWN0aW9uIG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwicmFoblwiIGNsYXNzTmFtZT1cImxhYmVsIGZvbnQtbWVkaXVtIGlucHV0LWxhYmVsXCI+2LHZh9mGINmIINin2KzYp9ix2Yc8L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0IGlkPVwia2hhcmlkXCJcbiAgICAgICAgICAgICAgdmFsdWU9ezF9XG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgIFtlLnRhcmdldC5uYW1lXTogZS50YXJnZXQuY2hlY2tlZCA/IDAgOiAxLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3aGl0ZSBmb250LWxpZ2h0XCJcbiAgICAgICAgICAgICAgdHlwZT1cInJhZGlvXCJcbiAgICAgICAgICAgICAgbmFtZT1cImRlYWxUeXBlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cImtoYXJpZFwiIGNsYXNzTmFtZT1cImxhYmVsIGZvbnQtbWVkaXVtIGlucHV0LWxhYmVsXCI+2K7YsduM2K88L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0IGlkPVwicmFoblwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndoaXRlIGZvbnQtbGlnaHRcIlxuICAgICAgICAgICAgICB0eXBlPVwicmFkaW9cIlxuICAgICAgICAgICAgICB2YWx1ZT17MH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC5jaGVja2VkID8gMSA6IDAsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIG5hbWU9XCJkZWFsVHlwZVwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidmlzaWJsZS14cyB2aXNpYmxlLXNtIGhpZGRlbi1tZCBoaWRkZW4tbGcgY29sLXhzLTEyXCI+XG4gICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5oYW5kbGVTdWJtaXR9PlxuICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2VhcmNoLWJ1dHRvbiBoYXMtdHJhbnNpdGlvbiBib3JkZXItcmFkaXVzIGNsZWFyLWlucHV0IHdoaXRlIGxpZ2h0LWJsdWUtYmFja2dyb3VuZFwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICDYrNiz2KrYrNmIXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPC9mb3JtPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShTZWFyY2hCb3gpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL1NlYXJjaEJveC9TZWFyY2hCb3guanMiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9TZWFyY2hCb3gvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiLy8gbm9pbnNwZWN0aW9uIEpTQW5ub3RhdG9yXG5jb25zdCBsZXZlbHMgPSBbXG4gIHtcbiAgICBNQVhfV0lEVEg6IDYwMCxcbiAgICBNQVhfSEVJR0hUOiA2MDAsXG4gICAgRE9DVU1FTlRfU0laRTogMzAwMDAwLFxuICB9LFxuICB7XG4gICAgTUFYX1dJRFRIOiA4MDAsXG4gICAgTUFYX0hFSUdIVDogODAwLFxuICAgIERPQ1VNRU5UX1NJWkU6IDUwMDAwMCxcbiAgfSxcbl07XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgZ2V0TGV2ZWwobGV2ZWwpIHtcbiAgICByZXR1cm4gbGV2ZWxzW2xldmVsIC0gMV07XG4gIH0sXG59O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb25maWcvY29tcHJlc3Npb25Db25maWcuanMiLCIvKiBlc2xpbnQtZGlzYWJsZSBwcmV0dGllci9wcmV0dGllciAqL1xuaW1wb3J0IHsgYXBpIH0gZnJvbSAnLi91cmxzJztcblxuZXhwb3J0IGNvbnN0IGxvZ2luQXBpVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvbG9naW5gO1xuZXhwb3J0IGNvbnN0IGxvZ291dEFwaVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2xvZ2luYDtcbmV4cG9ydCBjb25zdCBnZXRTZWFyY2hIb3VzZXNVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9TZWFyY2hTZXJ2bGV0YDtcbmV4cG9ydCBjb25zdCBnZXRDcmVhdGVIb3VzZVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9Ib3VzZVNlcnZsZXRgO1xuZXhwb3J0IGNvbnN0IGdldEluaXRpYXRlVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvaG9tZWA7XG5leHBvcnQgY29uc3QgZ2V0Q3VycmVudFVzZXJVcmwgPSAodXNlcklkKSA9PiBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvSW5kaXZpZHVhbFNlcnZsZXQ/dXNlcklkPSR7dXNlcklkfWA7XG5leHBvcnQgY29uc3QgZ2V0SG91c2VEZXRhaWxVcmwgPSAoaG91c2VJZCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0dldFNpbmdsZUhvdXNlP2lkPSR7aG91c2VJZH1gO1xuZXhwb3J0IGNvbnN0IGdldEhvdXNlUGhvbmVVcmwgPSAoaG91c2VJZCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL1BheW1lbnRTZXJ2bGV0P2lkPSR7aG91c2VJZH1gO1xuZXhwb3J0IGNvbnN0IGluY3JlYXNlQ3JlZGl0VXJsID0gKGFtb3VudCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0NyZWRpdFNlcnZsZXQ/dmFsdWU9JHthbW91bnR9YDtcblxuZXhwb3J0IGNvbnN0IHNlYXJjaERyaXZlclVybCA9IHBob25lTnVtYmVyID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9waG9uZU51bWJlci8ke3Bob25lTnVtYmVyfWA7XG5leHBvcnQgY29uc3QgZ2V0RHJpdmVyQWNxdWlzaXRpb25JbmZvVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCB1cGRhdGVEcml2ZXJBY3F1aXNpdGlvbkluZm9VcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IHVwZGF0ZURyaXZlckRvY3NVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvZG9jdW1lbnRzLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCBhcHByb3ZlRHJpdmVyVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2FwcHJvdmUvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IHJlamVjdERyaXZlclVybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9yZWplY3QvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IGdldERyaXZlckRvY3VtZW50VXJsID0gZG9jdW1lbnRJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvZG9jdW1lbnQvJHtkb2N1bWVudElkfWA7XG5leHBvcnQgY29uc3QgZ2V0RHJpdmVyRG9jdW1lbnRJZHNVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvZG9jdW1lbnRzLyR7ZHJpdmVySWR9YDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29uZmlnL3BhdGhzLmpzIiwiLyoqXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXG4gKlxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqL1xuXG5pbXBvcnQgY3JlYXRlQnJvd3Nlckhpc3RvcnkgZnJvbSAnaGlzdG9yeS9jcmVhdGVCcm93c2VySGlzdG9yeSc7XG5cbi8vIE5hdmlnYXRpb25CYXIgbWFuYWdlciwgZS5nLiBoaXN0b3J5LnB1c2goJy9ob21lJylcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9tamFja3Nvbi9oaXN0b3J5XG5leHBvcnQgZGVmYXVsdCBwcm9jZXNzLmVudi5CUk9XU0VSICYmIGNyZWF0ZUJyb3dzZXJIaXN0b3J5KCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2hpc3RvcnkuanMiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgKiBhcyBzZWFyY2hBY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMvc2VhcmNoJztcbmltcG9ydCAqIGFzIGF1dGhBY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMvYXV0aGVudGljYXRpb24nO1xuaW1wb3J0IEhvbWVQYWdlIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvSG9tZVBhZ2UnO1xuaW1wb3J0IExvYWRlciBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xvYWRlcic7XG5cbmNsYXNzIEhvbWVDb250YWluZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGlzRmV0Y2hpbmc6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gICAgaG91c2VzOiBQcm9wVHlwZXMuYXJyYXkuaXNSZXF1aXJlZCxcbiAgICBzZWFyY2hBY3Rpb25zOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgYXV0aEFjdGlvbnM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBjdXJyVXNlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB9O1xuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIGlzRmV0Y2hpbmc6IGZhbHNlLFxuICAgIGxvZ2luRXJyb3I6IG51bGwsXG4gIH07XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMucHJvcHMuYXV0aEFjdGlvbnMuaW5pdGlhdGUoKTtcbiAgfVxuICBoYW5kbGVTdWJtaXQgPSAobWluQXJlYSwgZGVhbFR5cGUsIGJ1aWxkaW5nVHlwZSwgbWF4UHJpY2UpID0+IHtcbiAgICB0aGlzLnByb3BzLnNlYXJjaEFjdGlvbnMuc2VhcmNoSG91c2VzKFxuICAgICAgbWluQXJlYSxcbiAgICAgIGRlYWxUeXBlLFxuICAgICAgYnVpbGRpbmdUeXBlLFxuICAgICAgbWF4UHJpY2UsXG4gICAgKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5jdXJyVXNlcik7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxIb21lUGFnZVxuICAgICAgICAgIGxvYWRpbmc9e3RoaXMucHJvcHMuaXNGZXRjaGluZ31cbiAgICAgICAgICBoYW5kbGVTdWJtaXQ9e3RoaXMuaGFuZGxlU3VibWl0fVxuICAgICAgICAvPlxuICAgICAgICA8TG9hZGVyIGxvYWRpbmc9e3RoaXMucHJvcHMuaXNGZXRjaGluZ30gLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZnVuY3Rpb24gbWFwRGlzcGF0Y2hUb1Byb3BzKGRpc3BhdGNoKSB7XG4gIHJldHVybiB7XG4gICAgc2VhcmNoQWN0aW9uczogYmluZEFjdGlvbkNyZWF0b3JzKHNlYXJjaEFjdGlvbnMsIGRpc3BhdGNoKSxcbiAgICBhdXRoQWN0aW9uczogYmluZEFjdGlvbkNyZWF0b3JzKGF1dGhBY3Rpb25zLCBkaXNwYXRjaCksXG4gIH07XG59XG5cbmZ1bmN0aW9uIG1hcFN0YXRlVG9Qcm9wcyhzdGF0ZSkge1xuICBjb25zdCB7IHNlYXJjaCB9ID0gc3RhdGU7XG4gIGNvbnN0IHsgYXV0aGVudGljYXRpb24gfSA9IHN0YXRlO1xuICBjb25zdCB7IGlzRmV0Y2hpbmcsIGhvdXNlcyB9ID0gc2VhcmNoO1xuICBjb25zdCB7IGN1cnJVc2VyIH0gPSBhdXRoZW50aWNhdGlvbjtcbiAgcmV0dXJuIHtcbiAgICBpc0ZldGNoaW5nLFxuICAgIGhvdXNlcyxcbiAgICBjdXJyVXNlcixcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoSG9tZUNvbnRhaW5lcik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3JvdXRlcy9ob21lL2hvbWVDb250YWluZXIuanMiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dCc7XG5pbXBvcnQgSG9tZUNvbnRhaW5lciBmcm9tICcuL2hvbWVDb250YWluZXInO1xuXG5jb25zdCB0aXRsZSA9ICfYrtin2YbZhyc7XG5cbmZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICBjaHVua3M6IFsnaG9tZSddLFxuICAgIHRpdGxlLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dCBtYWluUGFuZWw+XG4gICAgICAgIDxIb21lQ29udGFpbmVyIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3JvdXRlcy9ob21lL2luZGV4LmpzIiwiaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCBFWElGIGZyb20gJ2V4aWYtanMnO1xuaW1wb3J0IGhpc3RvcnkgZnJvbSAnLi4vaGlzdG9yeSc7XG5pbXBvcnQgY29tcHJlc3Npb25Db25maWcgZnJvbSAnLi4vY29uZmlnL2NvbXByZXNzaW9uQ29uZmlnJztcbmltcG9ydCB7IGxvYWRMb2dpblN0YXR1cyB9IGZyb20gJy4uL2FjdGlvbnMvYXV0aGVudGljYXRpb24nO1xuXG5jb25zdCBjb21wcmVzc2lvbkZpcnN0TGV2ZWwgPSBjb21wcmVzc2lvbkNvbmZpZy5nZXRMZXZlbCgxKTtcbmNvbnN0IGNvbXByZXNzaW9uU2Vjb25kTGV2ZWwgPSBjb21wcmVzc2lvbkNvbmZpZy5nZXRMZXZlbCgyKTtcbmNvbnN0IGRvY3VtZW50c0NvbXByZXNzaW9uTGV2ZWwgPSB7XG4gIHByb2ZpbGVQaWN0dXJlOiBjb21wcmVzc2lvbkZpcnN0TGV2ZWwsXG4gIGRyaXZlckxpY2VuY2U6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgY2FySWRDYXJkOiBjb21wcmVzc2lvbkZpcnN0TGV2ZWwsXG4gIGluc3VyYW5jZTogY29tcHJlc3Npb25TZWNvbmRMZXZlbCxcbn07XG5cbmZ1bmN0aW9uIGdldENvbXByZXNzaW9uTGV2ZWwoZG9jdW1lbnRUeXBlKSB7XG4gIHJldHVybiBkb2N1bWVudHNDb21wcmVzc2lvbkxldmVsW2RvY3VtZW50VHlwZV0gfHwgY29tcHJlc3Npb25GaXJzdExldmVsO1xufVxuXG5mdW5jdGlvbiBkYXRhVVJMdG9CbG9iKGRhdGFVUkwsIGZpbGVUeXBlKSB7XG4gIGNvbnN0IGJpbmFyeSA9IGF0b2IoZGF0YVVSTC5zcGxpdCgnLCcpWzFdKTtcbiAgY29uc3QgYXJyYXkgPSBbXTtcbiAgY29uc3QgbGVuZ3RoID0gYmluYXJ5Lmxlbmd0aDtcbiAgbGV0IGk7XG4gIGZvciAoaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgIGFycmF5LnB1c2goYmluYXJ5LmNoYXJDb2RlQXQoaSkpO1xuICB9XG4gIHJldHVybiBuZXcgQmxvYihbbmV3IFVpbnQ4QXJyYXkoYXJyYXkpXSwgeyB0eXBlOiBmaWxlVHlwZSB9KTtcbn1cblxuZnVuY3Rpb24gZ2V0U2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMod2lkdGgsIGhlaWdodCwgZG9jdW1lbnRUeXBlKSB7XG4gIGNvbnN0IHsgTUFYX0hFSUdIVCwgTUFYX1dJRFRIIH0gPSBnZXRDb21wcmVzc2lvbkxldmVsKGRvY3VtZW50VHlwZSk7XG5cbiAgaWYgKHdpZHRoID4gaGVpZ2h0KSB7XG4gICAgaGVpZ2h0ICo9IE1BWF9XSURUSCAvIHdpZHRoO1xuICAgIHdpZHRoID0gTUFYX1dJRFRIO1xuICB9IGVsc2Uge1xuICAgIHdpZHRoICo9IE1BWF9IRUlHSFQgLyBoZWlnaHQ7XG4gICAgaGVpZ2h0ID0gTUFYX0hFSUdIVDtcbiAgfVxuICByZXR1cm4ge1xuICAgIGhlaWdodCxcbiAgICB3aWR0aCxcbiAgfTtcbn1cblxuZnVuY3Rpb24gZ2V0SW1hZ2VPcmllbnRhdGlvbihpbWFnZSkge1xuICBsZXQgb3JpZW50YXRpb24gPSAwO1xuICBFWElGLmdldERhdGEoaW1hZ2UsIGZ1bmN0aW9uKCkge1xuICAgIG9yaWVudGF0aW9uID0gRVhJRi5nZXRUYWcodGhpcywgJ09yaWVudGF0aW9uJyk7XG4gIH0pO1xuICByZXR1cm4gb3JpZW50YXRpb247XG59XG5cbmZ1bmN0aW9uIGdldEltYWdlTWVhc3VyZW1lbnRzKGltYWdlKSB7XG4gIGNvbnN0IG9yaWVudGF0aW9uID0gZ2V0SW1hZ2VPcmllbnRhdGlvbihpbWFnZSk7XG4gIGxldCB0cmFuc2Zvcm07XG4gIGxldCBzd2FwID0gZmFsc2U7XG4gIHN3aXRjaCAob3JpZW50YXRpb24pIHtcbiAgICBjYXNlIDg6XG4gICAgICBzd2FwID0gdHJ1ZTtcbiAgICAgIHRyYW5zZm9ybSA9ICdsZWZ0JztcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgNjpcbiAgICAgIHN3YXAgPSB0cnVlO1xuICAgICAgdHJhbnNmb3JtID0gJ3JpZ2h0JztcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgMzpcbiAgICAgIHRyYW5zZm9ybSA9ICdmbGlwJztcbiAgICAgIGJyZWFrO1xuICAgIGRlZmF1bHQ6XG4gIH1cbiAgY29uc3Qgd2lkdGggPSBzd2FwID8gaW1hZ2UuaGVpZ2h0IDogaW1hZ2Uud2lkdGg7XG4gIGNvbnN0IGhlaWdodCA9IHN3YXAgPyBpbWFnZS53aWR0aCA6IGltYWdlLmhlaWdodDtcbiAgcmV0dXJuIHtcbiAgICB3aWR0aCxcbiAgICBoZWlnaHQsXG4gICAgdHJhbnNmb3JtLFxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRTY2FsZWRJbWFnZUZpbGVGcm9tQ2FudmFzKFxuICBpbWFnZSxcbiAgd2lkdGgsXG4gIGhlaWdodCxcbiAgdHJhbnNmb3JtLFxuICBmaWxlVHlwZSxcbikge1xuICBjb25zdCBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcbiAgY2FudmFzLndpZHRoID0gd2lkdGg7XG4gIGNhbnZhcy5oZWlnaHQgPSBoZWlnaHQ7XG4gIGNvbnN0IGNvbnRleHQgPSBjYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcbiAgbGV0IHRyYW5zZm9ybVBhcmFtcztcbiAgbGV0IHN3YXBNZWFzdXJlID0gZmFsc2U7XG4gIGxldCBpbWFnZVdpZHRoID0gd2lkdGg7XG4gIGxldCBpbWFnZUhlaWdodCA9IGhlaWdodDtcbiAgc3dpdGNoICh0cmFuc2Zvcm0pIHtcbiAgICBjYXNlICdsZWZ0JzpcbiAgICAgIHRyYW5zZm9ybVBhcmFtcyA9IFswLCAtMSwgMSwgMCwgMCwgaGVpZ2h0XTtcbiAgICAgIHN3YXBNZWFzdXJlID0gdHJ1ZTtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ3JpZ2h0JzpcbiAgICAgIHRyYW5zZm9ybVBhcmFtcyA9IFswLCAxLCAtMSwgMCwgd2lkdGgsIDBdO1xuICAgICAgc3dhcE1lYXN1cmUgPSB0cnVlO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAnZmxpcCc6XG4gICAgICB0cmFuc2Zvcm1QYXJhbXMgPSBbMSwgMCwgMCwgLTEsIDAsIGhlaWdodF07XG4gICAgICBicmVhaztcbiAgICBkZWZhdWx0OlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzEsIDAsIDAsIDEsIDAsIDBdO1xuICB9XG4gIGNvbnRleHQuc2V0VHJhbnNmb3JtKC4uLnRyYW5zZm9ybVBhcmFtcyk7XG4gIGlmIChzd2FwTWVhc3VyZSkge1xuICAgIGltYWdlSGVpZ2h0ID0gd2lkdGg7XG4gICAgaW1hZ2VXaWR0aCA9IGhlaWdodDtcbiAgfVxuICBjb250ZXh0LmRyYXdJbWFnZShpbWFnZSwgMCwgMCwgaW1hZ2VXaWR0aCwgaW1hZ2VIZWlnaHQpO1xuICBjb250ZXh0LnNldFRyYW5zZm9ybSgxLCAwLCAwLCAxLCAwLCAwKTtcbiAgY29uc3QgZGF0YVVSTCA9IGNhbnZhcy50b0RhdGFVUkwoZmlsZVR5cGUpO1xuICByZXR1cm4gZGF0YVVSTHRvQmxvYihkYXRhVVJMLCBmaWxlVHlwZSk7XG59XG5cbmZ1bmN0aW9uIHNob3VsZFJlc2l6ZShpbWFnZU1lYXN1cmVtZW50cywgZmlsZVNpemUsIGRvY3VtZW50VHlwZSkge1xuICBjb25zdCB7IE1BWF9IRUlHSFQsIE1BWF9XSURUSCwgRE9DVU1FTlRfU0laRSB9ID0gZ2V0Q29tcHJlc3Npb25MZXZlbChcbiAgICBkb2N1bWVudFR5cGUsXG4gICk7XG4gIGlmIChkb2N1bWVudFR5cGUgPT09ICdpbnN1cmFuY2UnICYmIGZpbGVTaXplIDwgRE9DVU1FTlRfU0laRSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfSBlbHNlIGlmIChmaWxlU2l6ZSA8IERPQ1VNRU5UX1NJWkUpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgcmV0dXJuIChcbiAgICBpbWFnZU1lYXN1cmVtZW50cy53aWR0aCA+IE1BWF9XSURUSCB8fCBpbWFnZU1lYXN1cmVtZW50cy5oZWlnaHQgPiBNQVhfSEVJR0hUXG4gICk7XG59XG5cbmFzeW5jIGZ1bmN0aW9uIHByb2Nlc3NGaWxlKGRhdGFVUkwsIGZpbGVTaXplLCBmaWxlVHlwZSwgZG9jdW1lbnRUeXBlKSB7XG4gIGNvbnN0IGltYWdlID0gbmV3IEltYWdlKCk7XG4gIGltYWdlLnNyYyA9IGRhdGFVUkw7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgaW1hZ2Uub25sb2FkID0gZnVuY3Rpb24oKSB7XG4gICAgICBjb25zdCBpbWFnZU1lYXN1cmVtZW50cyA9IGdldEltYWdlTWVhc3VyZW1lbnRzKGltYWdlKTtcbiAgICAgIGlmICghc2hvdWxkUmVzaXplKGltYWdlTWVhc3VyZW1lbnRzLCBmaWxlU2l6ZSwgZG9jdW1lbnRUeXBlKSkge1xuICAgICAgICByZXNvbHZlKGRhdGFVUkx0b0Jsb2IoZGF0YVVSTCwgZmlsZVR5cGUpKTtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHNjYWxlZEltYWdlTWVhc3VyZW1lbnRzID0gZ2V0U2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMoXG4gICAgICAgIGltYWdlTWVhc3VyZW1lbnRzLndpZHRoLFxuICAgICAgICBpbWFnZU1lYXN1cmVtZW50cy5oZWlnaHQsXG4gICAgICAgIGRvY3VtZW50VHlwZSxcbiAgICAgICk7XG4gICAgICBjb25zdCBzY2FsZWRJbWFnZUZpbGUgPSBnZXRTY2FsZWRJbWFnZUZpbGVGcm9tQ2FudmFzKFxuICAgICAgICBpbWFnZSxcbiAgICAgICAgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMud2lkdGgsXG4gICAgICAgIHNjYWxlZEltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCxcbiAgICAgICAgaW1hZ2VNZWFzdXJlbWVudHMudHJhbnNmb3JtLFxuICAgICAgICBmaWxlVHlwZSxcbiAgICAgICk7XG4gICAgICByZXNvbHZlKHNjYWxlZEltYWdlRmlsZSk7XG4gICAgfTtcbiAgICBpbWFnZS5vbmVycm9yID0gcmVqZWN0O1xuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNvbXByZXNzRmlsZShmaWxlLCBkb2N1bWVudFR5cGUgPSAnZGVmYXVsdCcpIHtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcbiAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuICAgICAgcmVhZGVyLm9ubG9hZGVuZCA9IGFzeW5jIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb25zdCBjb21wcmVzc2VkRmlsZSA9IGF3YWl0IHByb2Nlc3NGaWxlKFxuICAgICAgICAgIHJlYWRlci5yZXN1bHQsXG4gICAgICAgICAgZmlsZS5zaXplLFxuICAgICAgICAgIGZpbGUudHlwZSxcbiAgICAgICAgICBkb2N1bWVudFR5cGUsXG4gICAgICAgICk7XG4gICAgICAgIHJlc29sdmUoY29tcHJlc3NlZEZpbGUpO1xuICAgICAgfTtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIHJlamVjdChlcnIpO1xuICAgIH1cbiAgfSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRMb2dpbkluZm8oKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgbG9naW5JbmZvID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xvZ2luSW5mbycpO1xuICAgIGlmIChfLmlzU3RyaW5nKGxvZ2luSW5mbykpIHtcbiAgICAgIHJldHVybiBKU09OLnBhcnNlKGxvZ2luSW5mbyk7XG4gICAgfVxuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmxvZygnRXJyb3Igd2hlbiBwYXJzaW5nIGxvZ2luSW5mbyBvYmplY3Q6ICcsIGVycik7XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5leHBvcnQgZnVuY3Rpb24gZ2V0UG9zdENvbmZpZyhib2R5KSB7XG4gIGNvbnN0IGxvZ2luSW5mbyA9IGdldExvZ2luSW5mbygpO1xuICBjb25zdCBoZWFkZXJzID0gbG9naW5JbmZvXG4gICAgPyB7XG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIG1pbWVUeXBlOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2dpbkluZm8udG9rZW59YCxcbiAgICAgIH1cbiAgICA6IHsgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJywgbWltZVR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyB9O1xuICByZXR1cm4ge1xuICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgIC8vIG1vZGU6ICduby1jb3JzJyxcbiAgICBoZWFkZXJzLFxuICAgIC4uLihfLmlzRW1wdHkoYm9keSkgPyB7fSA6IHsgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSkgfSksXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRQdXRDb25maWcoYm9keSkge1xuICBjb25zdCBsb2dpbkluZm8gPSBnZXRMb2dpbkluZm8oKTtcbiAgY29uc3QgaGVhZGVycyA9IGxvZ2luSW5mb1xuICAgID8ge1xuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnWC1BdXRob3JpemF0aW9uJzogbG9naW5JbmZvLnRva2VuLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnUFVUJyxcbiAgICBoZWFkZXJzLFxuICAgIC4uLihfLmlzRW1wdHkoYm9keSkgPyB7fSA6IHsgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSkgfSksXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRHZXRDb25maWcoKSB7XG4gIGNvbnN0IGxvZ2luSW5mbyA9IGdldExvZ2luSW5mbygpO1xuICBjb25zdCBoZWFkZXJzID0gbG9naW5JbmZvXG4gICAgPyB7XG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2dpbkluZm8udG9rZW59YCxcbiAgICAgIH1cbiAgICA6IHsgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyB9O1xuICByZXR1cm4ge1xuICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgaGVhZGVycyxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNvbnZlcnROdW1iZXJzVG9QZXJzaWFuKHN0cikge1xuICBjb25zdCBwZXJzaWFuTnVtYmVycyA9ICfbsNux27Lbs9u027Xbttu327jbuSc7XG4gIGxldCByZXN1bHQgPSAnJztcbiAgaWYgKCFzdHIgJiYgc3RyICE9PSAwKSB7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuICBzdHIgPSBzdHIudG9TdHJpbmcoKTtcbiAgbGV0IGNvbnZlcnRlZENoYXI7XG4gIGZvciAobGV0IGkgPSAwOyBpIDwgc3RyLmxlbmd0aDsgaSsrKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnZlcnRlZENoYXIgPSBwZXJzaWFuTnVtYmVyc1tzdHJbaV1dO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICAgIGNvbnZlcnRlZENoYXIgPSBzdHJbaV07XG4gICAgfVxuICAgIHJlc3VsdCArPSBjb252ZXJ0ZWRDaGFyO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBmZXRjaEFwaSh1cmwsIGNvbmZpZykge1xuICBsZXQgZmxvd0Vycm9yO1xuICB0cnkge1xuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwgY29uZmlnKTtcbiAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAgIGNvbnNvbGUubG9nKHJlcy5zdGF0dXMpO1xuICAgIGxldCByZXN1bHQgPSBhd2FpdCByZXMuanNvbigpO1xuICAgIGNvbnNvbGUubG9nKHJlc3VsdCk7XG4gICAgaWYgKCFfLmlzRW1wdHkoXy5nZXQocmVzdWx0LCAnZGF0YScpKSkge1xuICAgICAgcmVzdWx0ID0gcmVzdWx0LmRhdGE7XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKHJlcy5zdGF0dXMpO1xuXG4gICAgaWYgKHJlcy5zdGF0dXMgIT09IDIwMCkge1xuICAgICAgZmxvd0Vycm9yID0gcmVzdWx0O1xuICAgICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG5cbiAgICAgIGlmIChyZXMuc3RhdHVzID09PSA0MDMpIHtcbiAgICAgICAgbG9jYWxTdG9yYWdlLmNsZWFyKCk7XG4gICAgICAgIGZsb3dFcnJvci5tZXNzYWdlID0gJ9mE2LfZgdinINiv2YjYqNin2LHZhyDZiNin2LHYryDYtNmI24zYryEnO1xuICAgICAgICBjb25zb2xlLmxvZygneWVlZXMnKTtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiBoaXN0b3J5LnB1c2goJy9sb2dpbicpLCAzMDAwKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgY29uc29sZS5kZWJ1ZyhgRXJyb3Igd2hlbiBmZXRjaGluZyBhcGk6ICR7dXJsfWAsIGVycik7XG4gICAgZmxvd0Vycm9yID0ge1xuICAgICAgY29kZTogJ1VOS05PV05fRVJST1InLFxuICAgICAgbWVzc2FnZTogJ9iu2LfYp9uMINmG2KfZhdi02K7YtSDZhNi32YHYpyDYr9mI2KjYp9ix2Ycg2LPYuduMINqp2YbbjNivJyxcbiAgICB9O1xuICB9XG4gIGlmIChmbG93RXJyb3IpIHtcbiAgICB0aHJvdyBmbG93RXJyb3I7XG4gIH1cbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIHNldEltbWVkaWF0ZShmbikge1xuICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBsZXQgdmFsdWUgPSBudWxsO1xuICAgICAgaWYgKF8uaXNGdW5jdGlvbihmbikpIHtcbiAgICAgICAgdmFsdWUgPSBmbigpO1xuICAgICAgfVxuICAgICAgcmVzb2x2ZSh2YWx1ZSk7XG4gICAgfSwgMCk7XG4gIH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2V0Q29va2llKG5hbWUsIHZhbHVlLCBkYXlzID0gNykge1xuICBsZXQgZXhwaXJlcyA9ICcnO1xuICBpZiAoZGF5cykge1xuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSgpO1xuICAgIGRhdGUuc2V0VGltZShkYXRlLmdldFRpbWUoKSArIGRheXMgKiAyNCAqIDYwICogNjAgKiAxMDAwKTtcbiAgICBleHBpcmVzID0gYDsgZXhwaXJlcz0ke2RhdGUudG9VVENTdHJpbmcoKX1gO1xuICB9XG4gIGRvY3VtZW50LmNvb2tpZSA9IGAke25hbWV9PSR7dmFsdWUgfHwgJyd9JHtleHBpcmVzfTsgcGF0aD0vYDtcbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRDb29raWUobmFtZSkge1xuICBjb25zdCBuYW1lRVEgPSBgJHtuYW1lfT1gO1xuICBjb25zdCBjYSA9IGRvY3VtZW50LmNvb2tpZS5zcGxpdCgnOycpO1xuICBjb25zdCBsZW5ndGggPSBjYS5sZW5ndGg7XG4gIGxldCBpID0gMDtcbiAgZm9yICg7IGkgPCBsZW5ndGg7IGkgKz0gMSkge1xuICAgIGxldCBjID0gY2FbaV07XG4gICAgd2hpbGUgKGMuY2hhckF0KDApID09PSAnICcpIHtcbiAgICAgIGMgPSBjLnN1YnN0cmluZygxLCBjLmxlbmd0aCk7XG4gICAgfVxuICAgIGlmIChjLmluZGV4T2YobmFtZUVRKSA9PT0gMCkge1xuICAgICAgcmV0dXJuIGMuc3Vic3RyaW5nKG5hbWVFUS5sZW5ndGgsIGMubGVuZ3RoKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBlcmFzZUNvb2tpZShuYW1lKSB7XG4gIGRvY3VtZW50LmNvb2tpZSA9IGAke25hbWV9PTsgTWF4LUFnZT0tOTk5OTk5OTk7YDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG5vdGlmeUVycm9yKG1lc3NhZ2UpIHtcbiAgdG9hc3RyLm9wdGlvbnMgPSB7XG4gICAgY2xvc2VCdXR0b246IGZhbHNlLFxuICAgIGRlYnVnOiBmYWxzZSxcbiAgICBuZXdlc3RPblRvcDogZmFsc2UsXG4gICAgcHJvZ3Jlc3NCYXI6IGZhbHNlLFxuICAgIHBvc2l0aW9uQ2xhc3M6ICd0b2FzdC10b3AtY2VudGVyJyxcbiAgICBwcmV2ZW50RHVwbGljYXRlczogdHJ1ZSxcbiAgICBvbmNsaWNrOiBudWxsLFxuICAgIHNob3dEdXJhdGlvbjogJzMwMCcsXG4gICAgaGlkZUR1cmF0aW9uOiAnMTAwMCcsXG4gICAgdGltZU91dDogJzEwMDAwJyxcbiAgICBleHRlbmRlZFRpbWVPdXQ6ICcxMDAwJyxcbiAgICBzaG93RWFzaW5nOiAnc3dpbmcnLFxuICAgIGhpZGVFYXNpbmc6ICdsaW5lYXInLFxuICAgIHNob3dNZXRob2Q6ICdzbGlkZURvd24nLFxuICAgIGhpZGVNZXRob2Q6ICdzbGlkZVVwJyxcbiAgfTtcbiAgdG9hc3RyLmVycm9yKG1lc3NhZ2UsICfYrti32KcnKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG5vdGlmeVN1Y2Nlc3MobWVzc2FnZSkge1xuICB0b2FzdHIub3B0aW9ucyA9IHtcbiAgICBjbG9zZUJ1dHRvbjogZmFsc2UsXG4gICAgZGVidWc6IGZhbHNlLFxuICAgIG5ld2VzdE9uVG9wOiBmYWxzZSxcbiAgICBwcm9ncmVzc0JhcjogZmFsc2UsXG4gICAgcG9zaXRpb25DbGFzczogJ3RvYXN0LXRvcC1jZW50ZXInLFxuICAgIHByZXZlbnREdXBsaWNhdGVzOiB0cnVlLFxuICAgIG9uY2xpY2s6IG51bGwsXG4gICAgc2hvd0R1cmF0aW9uOiAnMzAwJyxcbiAgICBoaWRlRHVyYXRpb246ICcxMDAwJyxcbiAgICB0aW1lT3V0OiAnNTAwMCcsXG4gICAgZXh0ZW5kZWRUaW1lT3V0OiAnMTAwMCcsXG4gICAgc2hvd0Vhc2luZzogJ3N3aW5nJyxcbiAgICBoaWRlRWFzaW5nOiAnbGluZWFyJyxcbiAgICBzaG93TWV0aG9kOiAnc2xpZGVEb3duJyxcbiAgICBoaWRlTWV0aG9kOiAnc2xpZGVVcCcsXG4gIH07XG4gIHRvYXN0ci5zdWNjZXNzKG1lc3NhZ2UsICcnKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHJlbG9hZFNlbGVjdFBpY2tlcigpIHtcbiAgdHJ5IHtcbiAgICAkKCcuc2VsZWN0cGlja2VyJykuc2VsZWN0cGlja2VyKCdyZW5kZXInKTtcbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9hZFNlbGVjdFBpY2tlcigpIHtcbiAgdHJ5IHtcbiAgICAkKCcuc2VsZWN0cGlja2VyJykuc2VsZWN0cGlja2VyKHt9KTtcbiAgICBpZiAoXG4gICAgICAvQW5kcm9pZHx3ZWJPU3xpUGhvbmV8aVBhZHxpUG9kfEJsYWNrQmVycnkvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpXG4gICAgKSB7XG4gICAgICAkKCcuc2VsZWN0cGlja2VyJykuc2VsZWN0cGlja2VyKCdtb2JpbGUnKTtcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUubG9nKGVycik7XG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEZvcm1EYXRhKGRhdGEpIHtcbiAgaWYgKF8uaXNFbXB0eShkYXRhKSkge1xuICAgIHRocm93IEVycm9yKCdJbnZhbGlkIGlucHV0IGRhdGEgdG8gY3JlYXRlIGEgRm9ybURhdGEgb2JqZWN0Jyk7XG4gIH1cbiAgbGV0IGZvcm1EYXRhO1xuICB0cnkge1xuICAgIGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgY29uc3Qga2V5cyA9IF8ua2V5cyhkYXRhKTtcbiAgICBsZXQgaTtcbiAgICBjb25zdCBsZW5ndGggPSBrZXlzLmxlbmd0aDtcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgIGZvcm1EYXRhLmFwcGVuZChrZXlzW2ldLCBkYXRhW2tleXNbaV1dKTtcbiAgICB9XG4gICAgcmV0dXJuIGZvcm1EYXRhO1xuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmRlYnVnKGVycik7XG4gICAgdGhyb3cgRXJyb3IoJ0ZPUk1fREFUQV9CUk9XU0VSX1NVUFBPUlRfRkFJTFVSRScpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkVXNlckxvZ2luU3RhdHVzKHN0b3JlKSB7XG4gIGNvbnN0IGxvZ2luU3RhdHVzQWN0aW9uID0gbG9hZExvZ2luU3RhdHVzKCk7XG4gIHN0b3JlLmRpc3BhdGNoKGxvZ2luU3RhdHVzQWN0aW9uKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvdXRpbHMvaW5kZXguanMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFxQkE7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoUkE7QUFXQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF4QkE7QUFBQTtBQUFBO0FBQUE7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQ0E7QUFBQTtBQUFBO0FBQUE7QUFnQ0E7Ozs7Ozs7O0FDaEpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUZBO0FBS0E7Ozs7Ozs7O0FDaENBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFPQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTBCQTtBQU1BO0FBSUE7QUFDQTtBQUNBO0FBTUE7QUFJQTs7Ozs7OztBQ3pEQTs7Ozs7OztBQ0FBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBZkE7QUFEQTtBQXlCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBMUJBO0FBREE7QUFvQ0E7QUF2Q0E7QUFDQTtBQXlDQTs7Ozs7OztBQ25EQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzdCQTs7Ozs7OztBQ0FBOzs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQWRBO0FBREE7QUFKQTtBQWdDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFqQ0E7QUFEQTtBQURBO0FBeUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFEQTtBQWhEQTtBQUxBO0FBREE7QUFEQTtBQXlFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBREE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEE7QUFEQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIQTtBQURBO0FBZkE7QUFEQTtBQURBO0FBREE7QUE2QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBekJBO0FBREE7QUFKQTtBQURBO0FBTkE7QUFEQTtBQXZHQTtBQXlKQTtBQS9KQTtBQUNBO0FBREE7QUFFQTtBQURBO0FBaUtBOzs7Ozs7O0FDL0tBOzs7Ozs7O0FDQUE7Ozs7Ozs7QUNBQTs7Ozs7OztBQ0FBOzs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDN0JBOzs7Ozs7O0FDQUE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBTUE7QUFoQkE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUZBO0FBREE7QUFNQTtBQURBO0FBY0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFPQTtBQWJBO0FBQ0E7QUFEQTtBQUVBO0FBREE7QUFlQTs7Ozs7OztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUM3QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFGQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEzQkE7QUF1Q0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUExQ0E7QUFEQTtBQUpBO0FBOERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQS9EQTtBQURBO0FBREE7QUFEQTtBQURBO0FBREE7QUE4RUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBREE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRkE7QUFEQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBM0JBO0FBdUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBMUNBO0FBREE7QUFKQTtBQThEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEvREE7QUFEQTtBQURBO0FBdUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFUQTtBQURBO0FBREE7QUFEQTtBQXhFQTtBQURBO0FBREE7QUFtR0E7QUFoTUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFpTUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7Ozs7Ozs7QUM1TkE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFVQTtBQUNBO0FBTUE7QUFDQTtBQWxCQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZEE7QUFnQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkQTtBQWdCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVpBO0FBRkE7QUFqQ0E7QUFtREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBREE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkQTtBQTBCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQXRDQTtBQXBEQTtBQXVHQTtBQTFJQTtBQUNBO0FBREE7QUFFQTtBQURBO0FBNElBOzs7Ozs7O0FDbkpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7Ozs7Ozs7O0FDZEE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7Ozs7O0FDcEJBO0FBQUE7QUFBQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDYkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQWdCQTtBQU1BO0FBdEJBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBUUE7QUFuQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFTQTtBQUNBO0FBRkE7QUE4QkE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNsRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUpBO0FBU0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3NIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBMUJBOzs7Ozs7O0FBeElBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF0Q0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXNDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWZBO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=