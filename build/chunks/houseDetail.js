require("source-map-support").install();
exports.ids = [3];
exports.modules = {

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/node_modules/normalize.css/normalize.css"],"names":[],"mappings":"AAAA,4EAA4E;AAC5E;gFACgF;AAChF;;;;GAIG;AACH;EACE,kBAAkB;EAClB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,+BAA+B;EAC/B,OAAO,EAAE;AACX;gFACgF;AAChF;;GAEG;AACH;EACE,UAAU,EAAE;AACd;;GAEG;AACH;;;;;;EAME,eAAe,EAAE;AACnB;;;GAGG;AACH;EACE,eAAe;EACf,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;EAGE,OAAO;EACP,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;;GAGG;AACH;EACE,gCAAgC;UACxB,wBAAwB;EAChC,OAAO;EACP,UAAU;EACV,OAAO;EACP,kBAAkB;EAClB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,sCAAsC;EACtC,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,oBAAoB;EACpB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,0CAA0C;UAClC,kCAAkC;EAC1C,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,qBAAqB,EAAE;AACzB;;GAEG;AACH;;EAEE,oBAAoB,EAAE;AACxB;;;GAGG;AACH;;;EAGE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,uBAAuB;EACvB,YAAY,EAAE;AAChB;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,eAAe;EACf,eAAe;EACf,mBAAmB;EACnB,yBAAyB,EAAE;AAC7B;EACE,gBAAgB,EAAE;AACpB;EACE,YAAY,EAAE;AAChB;gFACgF;AAChF;;GAEG;AACH;;EAEE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc;EACd,UAAU,EAAE;AACd;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;;;EAKE,wBAAwB;EACxB,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,kBAAkB;EAClB,OAAO;EACP,UAAU;EACV,OAAO,EAAE;AACX;;;GAGG;AACH;;EAEE,OAAO;EACP,kBAAkB,EAAE;AACtB;;;GAGG;AACH;;EAEE,OAAO;EACP,qBAAqB,EAAE;AACzB;;;;GAIG;AACH;;;;EAIE,2BAA2B;EAC3B,OAAO,EAAE;AACX;;GAEG;AACH;;;;EAIE,mBAAmB;EACnB,WAAW,EAAE;AACf;;GAEG;AACH;;;;EAIE,+BAA+B,EAAE;AACnC;;GAEG;AACH;EACE,+BAA+B,EAAE;AACnC;;;;;GAKG;AACH;EACE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,eAAe;EACf,OAAO;EACP,eAAe;EACf,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,WAAW;EACX,OAAO;EACP,oBAAoB;EACpB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,sBAAsB;EACtB,OAAO;EACP,yBAAyB;EACzB,OAAO,EAAE;AACX;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,WAAW;EACX,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,aAAa,EAAE;AACjB;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,qBAAqB;EACrB,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,yBAAyB,EAAE;AAC7B;;;GAGG;AACH;EACE,2BAA2B;EAC3B,OAAO;EACP,cAAc;EACd,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;;EAEE,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;gFACgF;AAChF;;GAEG;AACH;EACE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc,EAAE;AAClB;gFACgF;AAChF;;GAEG;AACH;EACE,cAAc,EAAE","file":"normalize.css","sourcesContent":["/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,YAAY;EACZ,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE;IACE,aAAa,EAAE,EAAE;AACrB;EACE;IACE,eAAe,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousePage/main.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n  font-family: Shabnam;\n}\n.white {\n  color: white;\n}\n.black {\n  color: black;\n}\n.purple {\n  color: #4c0d6b\n}\n.light-blue-background {\n  background-color: #3ad2cb\n}\n.white-background {\n  background-color: white;\n}\n.dark-green-background {\n  background-color: #185956;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-light {\n  font-weight: 200;\n}\n.font-medium {\n  font-weight: 600;\n}\n.center-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.center-column {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.text-align-right {\n  text-align: right;\n}\n.text-align-left {\n  text-align: left;\n}\n.text-align-center {\n  text-align: center;\n}\n.no-list-style {\n  list-style: none;\n}\n.clear-input {\n  outline: none;\n  border: none;\n}\n.border-radius {\n  border-radius: 9px;\n}\n.has-transition {\n  -webkit-transition: box-shadow 0.3s ease-in-out;\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.search-theme{\n  background: url(" + escape(__webpack_require__("./src/components/HousePage/geometry2.png")) + ");\n  height: 15vh;\n  margin-top: 10vh;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.yellow-background {\n  background: #dedc2f; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.padd {\n  padding: 2% !important; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  /*background: url(\"../assets/images/pattern/geometry2.png\");*/\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n/*#img-1{*/\n/*background-image: url(../assets/images/villa1.jpg);*/\n/*}*/\n/*#img-2{*/\n/*background-image: url(../assets/images/villa2.jpg);*/\n/*}*/\n/*#img-3, #img-4, #img-5{*/\n/*background-image: url(../assets/images/underwater.jpg);*/\n/*}*/\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n.house-img {\n  border-radius: 5px;\n  width: 100%;\n  height: 100%;\n  margin-top: 5%;\n  margin-bottom: 5%; }\n.phone-display {\n  margin: 0%;\n  width: 100% !important;\n  padding: 2%;\n  font-family: shabnam; }\n.house-img-mar {\n  margin: 5% 0%; }\n.info-label {\n  padding: 2%;\n  border-radius: 5px;\n  margin: 5% 0; }\n.house-info {\n  margin: 5% 0;\n  margin-right: 2%; }\n.info-lines {\n  border-bottom: thin solid;\n  border-bottom-color: lightgrey;\n  padding: 3%;\n  margin-left: 3%;\n  margin-top: 5%; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/HousePage/main.css"],"names":[],"mappings":"AAAA;EACE,qBAAqB;CACtB;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,cAAc;CACf;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,wBAAwB;CACzB;AACD;EACE,0BAA0B;CAC3B;AACD;EACE,iBAAiB;CAClB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;EAChC,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;EAChC,6BAA6B;EAC7B,8BAA8B;MAC1B,2BAA2B;UACvB,uBAAuB;CAChC;AACD;EACE,kBAAkB;CACnB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,mBAAmB;CACpB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,cAAc;EACd,aAAa;CACd;AACD;EACE,mBAAmB;CACpB;AACD;EACE,gDAAgD;EAChD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,0CAAmC;EACnC,aAAa;EACb,iBAAiB;CAClB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,WAAW;EACX,gBAAgB;EAChB,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,uBAAuB,EAAE;AAC3B;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,8DAA8D;EAC9D,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB,WAAW;AACX,uDAAuD;AACvD,KAAK;AACL,WAAW;AACX,uDAAuD;AACvD,KAAK;AACL,2BAA2B;AAC3B,2DAA2D;AAC3D,KAAK;AACL,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE;AACrB;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,eAAe;EACf,kBAAkB,EAAE;AACtB;EACE,WAAW;EACX,uBAAuB;EACvB,YAAY;EACZ,qBAAqB,EAAE;AACzB;EACE,cAAc,EAAE;AAClB;EACE,YAAY;EACZ,mBAAmB;EACnB,aAAa,EAAE;AACjB;EACE,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,0BAA0B;EAC1B,+BAA+B;EAC/B,YAAY;EACZ,gBAAgB;EAChB,eAAe,EAAE","file":"main.css","sourcesContent":["body {\n  font-family: Shabnam;\n}\n.white {\n  color: white;\n}\n.black {\n  color: black;\n}\n.purple {\n  color: #4c0d6b\n}\n.light-blue-background {\n  background-color: #3ad2cb\n}\n.white-background {\n  background-color: white;\n}\n.dark-green-background {\n  background-color: #185956;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-light {\n  font-weight: 200;\n}\n.font-medium {\n  font-weight: 600;\n}\n.center-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.center-column {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.text-align-right {\n  text-align: right;\n}\n.text-align-left {\n  text-align: left;\n}\n.text-align-center {\n  text-align: center;\n}\n.no-list-style {\n  list-style: none;\n}\n.clear-input {\n  outline: none;\n  border: none;\n}\n.border-radius {\n  border-radius: 9px;\n}\n.has-transition {\n  -webkit-transition: box-shadow 0.3s ease-in-out;\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.search-theme{\n  background: url(\"./geometry2.png\");\n  height: 15vh;\n  margin-top: 10vh;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.yellow-background {\n  background: #dedc2f; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.padd {\n  padding: 2% !important; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  /*background: url(\"../assets/images/pattern/geometry2.png\");*/\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n/*#img-1{*/\n/*background-image: url(../assets/images/villa1.jpg);*/\n/*}*/\n/*#img-2{*/\n/*background-image: url(../assets/images/villa2.jpg);*/\n/*}*/\n/*#img-3, #img-4, #img-5{*/\n/*background-image: url(../assets/images/underwater.jpg);*/\n/*}*/\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n.house-img {\n  border-radius: 5px;\n  width: 100%;\n  height: 100%;\n  margin-top: 5%;\n  margin-bottom: 5%; }\n.phone-display {\n  margin: 0%;\n  width: 100% !important;\n  padding: 2%;\n  font-family: shabnam; }\n.house-img-mar {\n  margin: 5% 0%; }\n.info-label {\n  padding: 2%;\n  border-radius: 5px;\n  margin: 5% 0; }\n.house-info {\n  margin: 5% 0;\n  margin-right: 2%; }\n.info-lines {\n  border-bottom: thin solid;\n  border-bottom-color: lightgrey;\n  padding: 3%;\n  margin-left: 3%;\n  margin-top: 5%; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n.browserupgrade {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA,iBAAiB;AACjB;;;;;;;GAOG;AACH;;;;;;;GAOG;AACH;EACE;;gFAE8E;;EAE9E,kEAAkE;;EAElE;;gFAE8E;;EAE9E,4BAA4B;;EAE5B;;gFAE8E;;EAE9E,uBAAuB,EAAE,gCAAgC;EACzD,uBAAuB,EAAE,2BAA2B;EACpD,uBAAuB,EAAE,6BAA6B;EACtD,wBAAwB,CAAC,iCAAiC;CAC3D;AACD;EACE,qBAAqB;EACrB,mCAAmC;EACnC,uKAAuK;EACvK,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,6BAA6B;EAC7B,qJAAqJ;EACrJ,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,kCAAkC;EAClC,oKAAoK;EACpK,iBAAiB,EAAE;AACrB;;;;GAIG;AACH;;gFAEgF;AAChF;EACE,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;EACnB,WAAW,EAAE;AACf;EACE,UAAU;EACV,qBAAqB,EAAE;AACzB;EACE,eAAe,EAAE;AACnB;;;;;;GAMG;AACH;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;;GAEG;AACH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW,EAAE;AACf;;;;GAIG;AACH;;;;;;EAME,uBAAuB,EAAE;AAC3B;;GAEG;AACH;EACE,UAAU;EACV,UAAU;EACV,WAAW,EAAE;AACf;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;gFAEgF;AAChF;EACE,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,iBAAiB,EAAE;AACrB;;;;gFAIgF;AAChF;EACE;;;IAGE,mCAAmC;IACnC,uBAAuB;IACvB,+DAA+D;IAC/D,oCAAoC;YAC5B,4BAA4B;IACpC,6BAA6B,EAAE;EACjC;;IAEE,2BAA2B,EAAE;EAC/B;IACE,6BAA6B,EAAE;EACjC;IACE,8BAA8B,EAAE;EAClC;;;KAGG;EACH;;IAEE,YAAY,EAAE;EAChB;;IAEE,uBAAuB;IACvB,yBAAyB,EAAE;EAC7B;;;KAGG;EACH;IACE,4BAA4B,EAAE;EAChC;;IAEE,yBAAyB,EAAE;EAC7B;IACE,2BAA2B,EAAE;EAC/B;;;IAGE,WAAW;IACX,UAAU,EAAE;EACd;;IAEE,wBAAwB,EAAE,EAAE","file":"Layout.css","sourcesContent":["@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n:global(.browserupgrade) {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".loader {\n  position: fixed;\n  font-family: IRSans, serif;\n  z-index: 10;\n  padding: 20px;\n  border-radius: 10px;\n  background-color: white;\n  display: none;\n  top: calc(50% - 85px);\n  left: calc(50% - 70px);\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n  .loader img {\n    width: 100px; }\n  .loader .loader-info {\n    direction: rtl;\n    padding-top: 10px;\n    text-align: center; }\n  .loader.loading {\n    display: block; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Loader/Loader.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,2BAA2B;EAC3B,YAAY;EACZ,cAAc;EACd,oBAAoB;EACpB,wBAAwB;EACxB,cAAc;EACd,sBAAsB;EACtB,uBAAuB;EACvB,wHAAwH;UAChH,gHAAgH,EAAE;EAC1H;IACE,aAAa,EAAE;EACjB;IACE,eAAe;IACf,kBAAkB;IAClB,mBAAmB,EAAE;EACvB;IACE,eAAe,EAAE","file":"Loader.scss","sourcesContent":[".loader {\n  position: fixed;\n  font-family: IRSans, serif;\n  z-index: 10;\n  padding: 20px;\n  border-radius: 10px;\n  background-color: white;\n  display: none;\n  top: calc(50% - 85px);\n  left: calc(50% - 70px);\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n  .loader img {\n    width: 100px; }\n  .loader .loader-info {\n    direction: rtl;\n    padding-top: 10px;\n    text-align: center; }\n  .loader.loading {\n    display: block; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB;EACE,gCAAgC,EAAE;AACpC,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/url/escape.js":
/***/ (function(module, exports) {

module.exports = function escape(url) {
    if (typeof url !== 'string') {
        return url
    }
    // If url is already wrapped in quotes, remove them
    if (/^['"].*['"]$/.test(url)) {
        url = url.slice(1, -1);
    }
    // Should url be wrapped?
    // See https://drafts.csswg.org/css-values-3/#urls
    if (/["'() \t\n]/.test(url)) {
        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
    }

    return url
}


/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/actions/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["loginRequest"] = loginRequest;
/* harmony export (immutable) */ __webpack_exports__["loginSuccess"] = loginSuccess;
/* harmony export (immutable) */ __webpack_exports__["loginFailure"] = loginFailure;
/* harmony export (immutable) */ __webpack_exports__["loadLoginStatus"] = loadLoginStatus;
/* harmony export (immutable) */ __webpack_exports__["loginUser"] = loginUser;
/* harmony export (immutable) */ __webpack_exports__["logoutRequest"] = logoutRequest;
/* harmony export (immutable) */ __webpack_exports__["logoutSuccess"] = logoutSuccess;
/* harmony export (immutable) */ __webpack_exports__["logoutFailure"] = logoutFailure;
/* harmony export (immutable) */ __webpack_exports__["logoutUser"] = logoutUser;
/* harmony export (immutable) */ __webpack_exports__["initiateRequest"] = initiateRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateSuccess"] = initiateSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateFailure"] = initiateFailure;
/* harmony export (immutable) */ __webpack_exports__["initiate"] = initiate;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersRequest"] = initiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersSuccess"] = initiateUsersSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersFailure"] = initiateUsersFailure;
/* harmony export (immutable) */ __webpack_exports__["initiateUsers"] = initiateUsers;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserRequest"] = getCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserSuccess"] = getCurrentUserSuccess;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserFailure"] = getCurrentUserFailure;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUser"] = getCurrentUser;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditRequest"] = increaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditSuccess"] = increaseCreditSuccess;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditFailure"] = increaseCreditFailure;
/* harmony export (immutable) */ __webpack_exports__["increaseCredit"] = increaseCredit;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__ = __webpack_require__("./src/api-handlers/authentication.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function loginRequest(username, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["K" /* USER_LOGIN_REQUEST */],
    payload: {
      username,
      password
    }
  };
}

function loginSuccess(info) {
  console.log(info);
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["L" /* USER_LOGIN_SUCCESS */],
    payload: { info }
  };
}

function loginFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["J" /* USER_LOGIN_FAILURE */],
    payload: { error }
  };
}

function loadLoginStatus() {
  const loginInfo = Object(__WEBPACK_IMPORTED_MODULE_0__utils__["c" /* getLoginInfo */])();
  if (loginInfo) {
    return loginSuccess(loginInfo);
  }
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["I" /* USER_IS_NOT_LOGGED_IN */]
  };
}

function getCaptchaToken() {
  if (false) {
    const loginForm = $('#loginForm').get(0);
    if (loginForm && typeof loginForm['g-recaptcha-response'] === 'object') {
      return loginForm['g-recaptcha-response'].value;
    }
  }
  return null;
}

function loginUser(username, password) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(loginRequest(username, password));
      try {
        // const captchaToken = getCaptchaToken();
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["c" /* sendLoginRequest */])(username, password);
        dispatch(loginSuccess(res));
        localStorage.setItem('loginInfo', JSON.stringify(res));
        // console.log(location.pathname);
        const { pathname } = getState().house;
        console.log(pathname);
        __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push(pathname);
      } catch (err) {
        dispatch(loginFailure(err));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}

function logoutRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["N" /* USER_LOGOUT_REQUEST */],
    payload: {}
  };
}

function logoutSuccess() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["O" /* USER_LOGOUT_SUCCESS */],
    payload: {
      message: 'خروج موفقیت آمیز'
    }
  };
}

function logoutFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["M" /* USER_LOGOUT_FAILURE */],
    payload: { error }
  };
}

function logoutUser() {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(logoutRequest());
      try {
        dispatch(logoutSuccess());
        localStorage.clear();
        location.assign('/login');
      } catch (err) {
        dispatch(logoutFailure(err));
      }
    });

    return function (_x3) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function initiateRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["q" /* GET_INIT_REQUEST */]
  };
}

function initiateSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["r" /* GET_INIT_SUCCESS */],
    payload: { response }
  };
}

function initiateFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["p" /* GET_INIT_FAILURE */],
    payload: { err }
  };
}

function initiate() {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["a" /* sendInitiateRequest */])();
        dispatch(initiateSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateFailure(error));
      }
    });

    return function (_x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function initiateUsersRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["t" /* GET_INIT_USER_REQUEST */]
  };
}

function initiateUsersSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["u" /* GET_INIT_USER_SUCCESS */],
    payload: { response }
  };
}

function initiateUsersFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["s" /* GET_INIT_USER_FAILURE */],
    payload: { err }
  };
}

function initiateUsers() {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateUsersRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["b" /* sendInitiateUsersRequest */])();
        dispatch(initiateUsersSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateUsersFailure(error));
      }
    });

    return function (_x5) {
      return _ref4.apply(this, arguments);
    };
  })();
}

function getCurrentUserRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["e" /* GET_CURR_USER_REQUEST */]
  };
}

function getCurrentUserSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["f" /* GET_CURR_USER_SUCCESS */],
    payload: { response }
  };
}

function getCurrentUserFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["d" /* GET_CURR_USER_FAILURE */],
    payload: { err }
  };
}

function getCurrentUser(userId) {
  return (() => {
    var _ref5 = _asyncToGenerator(function* (dispatch) {
      dispatch(getCurrentUserRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["d" /* sendgetCurrentUserRequest */])(userId);
        dispatch(getCurrentUserSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getCurrentUserFailure(error));
      }
    });

    return function (_x6) {
      return _ref5.apply(this, arguments);
    };
  })();
}

function increaseCreditRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["z" /* INCREASE_CREDIT_REQUEST */]
  };
}

function increaseCreditSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["A" /* INCREASE_CREDIT_SUCCESS */],
    payload: { response }
  };
}

function increaseCreditFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["y" /* INCREASE_CREDIT_FAILURE */],
    payload: { err }
  };
}

function increaseCredit(amount) {
  return (() => {
    var _ref6 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(increaseCreditRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["e" /* sendincreaseCreditRequest */])(amount, currUser.userId);
        dispatch(increaseCreditSuccess(res));
        dispatch(getCurrentUser(currUser.userId));
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["f" /* notifySuccess */])('افزایش اعتبار با موفقیت انجام شد.');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(increaseCreditFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref6.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/actions/house.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["createHouseRequest"] = createHouseRequest;
/* harmony export (immutable) */ __webpack_exports__["createHouseSuccess"] = createHouseSuccess;
/* harmony export (immutable) */ __webpack_exports__["createHouseFailure"] = createHouseFailure;
/* harmony export (immutable) */ __webpack_exports__["createHouse"] = createHouse;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatusRequest"] = getPhoneStatusRequest;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatusSuccess"] = getPhoneStatusSuccess;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatusFailure"] = getPhoneStatusFailure;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatus"] = getPhoneStatus;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetailRequest"] = getHouseDetailRequest;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetailSuccess"] = getHouseDetailSuccess;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetailFailure"] = getHouseDetailFailure;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetail"] = getHouseDetail;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumberRequest"] = getHousePhoneNumberRequest;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumberSuccess"] = getHousePhoneNumberSuccess;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumberFailure"] = getHousePhoneNumberFailure;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumber"] = getHousePhoneNumber;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_handlers_house__ = __webpack_require__("./src/api-handlers/house.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils__ = __webpack_require__("./src/utils/index.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function createHouseRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["b" /* CREATE_HOUSE_REQUEST */]
  };
}

function createHouseSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["c" /* CREATE_HOUSE_SUCCESS */],
    payload: { response }
  };
}

function createHouseFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["a" /* CREATE_HOUSE_FAILURE */],
    payload: { err }
  };
}

function createHouse(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, imageURL) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(createHouseRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["a" /* sendcreateHouseRequest */])(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, imageURL);
        dispatch(createHouseSuccess(res));
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["f" /* notifySuccess */])('خانه با موفقیت ثبت شد');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(createHouseFailure(error));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}
function getPhoneStatusRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["w" /* GET_PHONE_STATUS_REQUEST */]
  };
}

function getPhoneStatusSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["x" /* GET_PHONE_STATUS_SUCCESS */],
    payload: { response }
  };
}

function getPhoneStatusFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["v" /* GET_PHONE_STATUS_FAILURE */],
    payload: { err }
  };
}

function getPhoneStatus(houseId) {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(getPhoneStatusRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["d" /* sendgetPhoneStatusRequest */])(houseId);
        dispatch(getPhoneStatusSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getPhoneStatusFailure(error));
      }
    });

    return function (_x3, _x4) {
      return _ref2.apply(this, arguments);
    };
  })();
}
function getHouseDetailRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["n" /* GET_HOUSE_REQUEST */]
  };
}

function getHouseDetailSuccess(response, pathname) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["o" /* GET_HOUSE_SUCCESS */],
    payload: { response, pathname }
  };
}

function getHouseDetailFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["j" /* GET_HOUSE_FAILURE */],
    payload: { err }
  };
}

function getHouseDetail(houseId, pathname) {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(getHouseDetailRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["b" /* sendgetHouseDetailRequest */])(houseId);
        dispatch(getHouseDetailSuccess(res, pathname));
        if (currUser) {
          dispatch(getPhoneStatus(houseId));
        }
      } catch (error) {
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        dispatch(getHouseDetailFailure(error));
      }
    });

    return function (_x5, _x6) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function getHousePhoneNumberRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["l" /* GET_HOUSE_PHONE_REQUEST */]
  };
}

function getHousePhoneNumberSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["m" /* GET_HOUSE_PHONE_SUCCESS */],
    payload: { response }
  };
}

function getHousePhoneNumberFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["k" /* GET_HOUSE_PHONE_FAILURE */],
    payload: { err }
  };
}

function getHousePhoneNumber(houseId) {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(getHousePhoneNumberRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["c" /* sendgetHousePhoneNumberRequest */])(houseId);
        console.log(res);
        if (!res.hasOwnProperty('message') && res !== null) {
          dispatch(getHousePhoneNumberSuccess('payed'));
          dispatch(Object(__WEBPACK_IMPORTED_MODULE_1__actions_authentication__["getCurrentUser"])(currUser.userId));
        } else if (res.hasOwnProperty('message')) {
          dispatch(getHousePhoneNumberSuccess('no_credit'));
        }
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getHousePhoneNumberFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref4.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/api-handlers/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = sendincreaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["a"] = sendInitiateRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendInitiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendLoginRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_paths__ = __webpack_require__("./src/config/paths.js");



function sendincreaseCreditRequest(amount, id) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["g" /* increaseCreditUrl */])(amount, id), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendgetCurrentUserRequest(userId) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["b" /* getCurrentUserUrl */])(userId), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateUsersRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendLoginRequest(username, password) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["d" /* getPostConfig */])({
    username,
    password
  }));
}

/***/ }),

/***/ "./src/api-handlers/house.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = sendcreateHouseRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendgetHouseDetailRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendgetHousePhoneNumberRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetPhoneStatusRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_paths__ = __webpack_require__("./src/config/paths.js");



/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

function sendcreateHouseRequest(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, imageURL) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_2__config_paths__["a" /* getCreateHouseUrl */], Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])({
    area,
    buildingType,
    address,
    dealType,
    basePrice,
    rentPrice,
    sellPrice,
    phone,
    description,
    expireTime,
    imageURL
  }));
}

/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

function sendgetHouseDetailRequest(houseId) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_2__config_paths__["c" /* getHouseDetailUrl */])(houseId), Object(__WEBPACK_IMPORTED_MODULE_1__utils__["b" /* getGetConfig */])());
}

function sendgetHousePhoneNumberRequest(houseId) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_2__config_paths__["d" /* getHousePhoneUrl */])(houseId), Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])());
}

function sendgetPhoneStatusRequest(houseId) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_2__config_paths__["d" /* getHousePhoneUrl */])(houseId), Object(__WEBPACK_IMPORTED_MODULE_1__utils__["b" /* getGetConfig */])());
}

/***/ }),

/***/ "./src/components/Footer/200px-Instagram_logo_2016.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Instagram_logo_2016.svg.png?2fc2d5c1";

/***/ }),

/***/ "./src/components/Footer/200px-Telegram_logo.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Telegram_logo.svg.png?6fa2804c";

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__ = __webpack_require__("./src/components/Footer/Twitter_bird_logo_2012.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__ = __webpack_require__("./src/components/Footer/200px-Instagram_logo_2016.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__ = __webpack_require__("./src/components/Footer/200px-Telegram_logo.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Footer/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/Footer.js';
/* eslint-disable react/no-danger */








class Footer extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'footer',
      { className: 'site-footer center-column dark-green-background white', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'row', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4 text-align-left', __source: {
              fileName: _jsxFileName,
              lineNumber: 15
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'ul',
            { className: 'social-icons-list no-list-style', __source: {
                fileName: _jsxFileName,
                lineNumber: 16
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 17
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'instagram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 18
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 24
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'twitter-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 25
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 31
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'telegram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              })
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-8 text-align-right', __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 41
              },
              __self: this
            },
            '\u062A\u0645\u0627\u0645\u06CC \u062D\u0642\u0648\u0642 \u0627\u06CC\u0646 \u0648\u0628\u200C\u0633\u0627\u06CC\u062A \u0645\u062A\u0639\u0644\u0642 \u0628\u0647 \u0645\u0647\u0631\u0646\u0627\u0632 \u062B\u0627\u0628\u062A \u0648 \u0647\u0648\u0645\u0646 \u0634\u0631\u06CC\u0641 \u0632\u0627\u062F\u0647 \u0645\u06CC\u200C\u0628\u0627\u0634\u062F.'
          )
        )
      )
    );
  }
}

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Twitter_bird_logo_2012.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/Twitter_bird_logo_2012.svg.png?248bfa8d";

/***/ }),

/***/ "./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/HousePage/HousePage.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css__ = __webpack_require__("./src/components/HousePage/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/HousePage/HousePage.js';
/* eslint-disable react/no-danger */





class HousePage extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.handleSubmit = e => {
      e.preventDefault();
      this.props.getPhone();
    }, _temp;
  }

  render() {
    console.log(this.props.houseMessage);
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'home-panel', __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'section',
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 23
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'search-theme flex-center', __source: {
              fileName: _jsxFileName,
              lineNumber: 24
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'container', __source: {
                fileName: _jsxFileName,
                lineNumber: 25
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'row ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 26
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 27
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'h3',
                  { className: 'text-align-right persian-bold blue-font flex-center', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 28
                    },
                    __self: this
                  },
                  '\u0645\u0634\u062E\u0635\u0627\u062A \u06A9\u0627\u0645\u0644 \u0645\u0644\u06A9',
                  ' '
                )
              )
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container', __source: {
              fileName: _jsxFileName,
              lineNumber: 36
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row', __source: {
                fileName: _jsxFileName,
                lineNumber: 37
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 col-md-7 house-img-mar', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 38
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 39
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                  alt: 'house-img',
                  src: this.props.house.imageURL,
                  className: 'img house-img',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 40
                  },
                  __self: this
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row flex-middle', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 46
                  },
                  __self: this
                },
                !this.props.phoneStatus && this.props.houseMessage === 'no_credit' && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'search-button persian text-align-center padd border-radius clear-input black-font yellow-background phone-display', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 49
                    },
                    __self: this
                  },
                  '\u0627\u0639\u062A\u0628\u0627\u0631 \u0634\u0645\u0627 \u0628\u0631\u0627\u06CC \u0645\u0634\u0627\u0647\u062F\u0647 \u0634\u0645\u0627\u0631\u0647 \u0645\u0627\u0644\u06A9\\\u0645\u0634\u0627\u0648\u0631 \u0627\u06CC\u0646 \u0645\u0644\u06A9 \u06A9\u0627\u0641\u06CC \u0646\u06CC\u0633\u062A'
                ),
                !this.props.phoneStatus && this.props.houseMessage !== 'no_credit' && this.props.houseMessage !== 'payed' && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'button',
                  {
                    onClick: this.handleSubmit,
                    type: 'submit',
                    className: 'padd search-button persian has-transition text-align-center padd border-radius clear-input white light-blue-background phone-display',
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 58
                    },
                    __self: this
                  },
                  '\u0645\u0634\u0627\u0647\u062F\u0647 \u0634\u0645\u0627\u0631\u0647 \u0645\u0627\u0644\u06A9\\\u0645\u0634\u0627\u0648\u0631'
                )
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 col-md-4 house-info pull-right', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 68
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 69
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'white-font col-xs-5 col-md-3 pull-right text-align-center purple-background persian info-label', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 70
                    },
                    __self: this
                  },
                  this.props.house.dealType === 1 ? 'اجاره' : 'فروش'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 74
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: ' text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 75
                    },
                    __self: this
                  },
                  this.props.phoneStatus || this.props.houseMessage === 'payed' ? this.props.house.phone : '۰۹۲۳۳***۲۳'
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: ' text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 81
                    },
                    __self: this
                  },
                  '\u0634\u0645\u0627\u0631\u0647 \u0645\u0627\u0644\u06A9'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 85
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 86
                    },
                    __self: this
                  },
                  this.props.house.buildingType
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 89
                    },
                    __self: this
                  },
                  '\u0646\u0648\u0639 \u0633\u0627\u062E\u062A\u0645\u0627\u0646'
                )
              ),
              this.props.house.dealType === 1 && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 100
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 101
                    },
                    __self: this
                  },
                  this.props.house.rentPrice,
                  ' ',
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'span',
                    { className: 'persian black-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 103
                      },
                      __self: this
                    },
                    '\u062A\u0648\u0645\u0627\u0646'
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 105
                    },
                    __self: this
                  },
                  '\u0627\u062C\u0627\u0631\u0647'
                )
              ),
              this.props.house.dealType === 0 && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 111
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 112
                    },
                    __self: this
                  },
                  this.props.house.sellPrice,
                  ' ',
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'span',
                    { className: 'persian black-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 114
                      },
                      __self: this
                    },
                    '\u062A\u0648\u0645\u0627\u0646'
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 116
                    },
                    __self: this
                  },
                  '\u0642\u06CC\u0645\u062A'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 121
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 122
                    },
                    __self: this
                  },
                  this.props.house.address
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 125
                    },
                    __self: this
                  },
                  '\u0622\u062F\u0631\u0633'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 129
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 130
                    },
                    __self: this
                  },
                  this.props.house.area,
                  ' ',
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'span',
                    { className: 'persian black-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 132
                      },
                      __self: this
                    },
                    '\u0645\u062A\u0631'
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 134
                    },
                    __self: this
                  },
                  '\u0645\u062A\u0631\u0627\u0698'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row info-lines', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 138
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-7 persian black-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 139
                    },
                    __self: this
                  },
                  this.props.house.description
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'text-align-right col-xs-5 persian light-grey-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 142
                    },
                    __self: this
                  },
                  '\u062A\u0648\u0636\u06CC\u062D\u0627\u062A'
                )
              )
            )
          )
        )
      )
    );
  }
}

HousePage.propTypes = {
  house: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  currUser: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  houseMessage: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string.isRequired,
  getPhone: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired,
  phoneStatus: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__main_css___default.a)(HousePage));

/***/ }),

/***/ "./src/components/HousePage/geometry2.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAMAAAC3Ycb+AAAAM1BMVEX5+vvr7O3y8/Tu7/D4+fns7e73+Pjt7/D29vf3+fnt7u/09fXx8vPv8PH19vfy8vPz9PUQ6VWYAAAH4klEQVR4Xu3d2XYTvxLF4doahx7f/2mPnUA6eJn/SaAdKeb33QC+yYKNqiR1W7J/DwAAAAAAAAAAAAAg2FDgfbFxoEi6RoJ18xfLXq2rOusbRVJWe5Cp6SefiOSjiuL8iEyC10X23kddOPsgIonSAzJJTYqu2lWZJc32MUQySzo9k5AlH+ynEj+cCJFM0rycnclyE0CK0scCJ5IgbRamUzMpUrZfJKmZDRLJbEPLamZ2ZLKfMkCS/cpJkw0SSbWhOSkdMyP5cMagW+5+9hHspCRpt6uwnDQ/LfdGw6JoH4IobxcpS3E9aczV+x9+BGYpmE1RyvWsInh32BT7CEzSapukOdgAgSBIS5biZIeeJQtZFy3ZoWtTxy5pCXboO+1Fkpwdui8MkYsdBt86Qf/NRfTffse4D6jQ/xEuHv+SA3gN6PEAAAAAAMC6LKsNA6suuiVS3iR7gUUX3vooOiTrjkCyDt7eULIm62KX5vJq+Vk3sXrfKY8QFYO9qlKzvrBJu/3kJGf94HZQhKZYrSN4qdx8N9P6QZGWOwH1gibV24S8dUbFGqZmoUrZbNCuzqy3/7wXx7rwqkY16wq7tNlPg+ydMNEaaIqFImX3KkvJesOiw2zdoerNj/4OAAAAAAAAAAAAAAAAAJvkg40Cky4WGwWcFCUbBXZdZBsFQpa02jAQpr1aRwAAAAAAAAAAAKhtsoHAS5t9Aoq7VexEe/xcInC65exMKQ712J9ALH32eDACsfck94CLU2wcBGJeqvYQayWQP7BKuz1AmBVXAvkDTd7Ol5ou5kAgnzZLj3mPtknKiUA+a5KSnSt4SVtIWZ+vhwRSpGKnKlGv/SPMkpZgXRHIJslXe7FGKRbrh0BqluRu/9gLPWSKUis3o1y5WgfMso6mcaeldMA6pElxt1thUZ9dZVbqTTH9ZqTv7PZ+TJbCmROs+sHP7yOQmjXbadJvRkJT5onhR7lg57n/L596tRDcr02blKwH3J8iNDXrA1F5pIqF+U7N2vtVLKzSZDdyx4qFqMV+VXtWLMxSGLliUbP6Vyxq1kgVC7Pk3ut8+TZW3YrWEYJuzfYdAAAAwF0VGwX+6/UCEAgkf1ogYIQQCAAAAAAAAAAAAAAAAAAAAAAAAAAAQCrFxoC0L00XNgBU1/SDdYc66yp6tw9QsuCipLYlGwFSluRX+77W1Z7IGqU42Vd4VDnMyvY0pi+7/7U2KQc7X5W2p8pjsi8xP+oIll1K5PF5XhfezucV7UmkeOTxbUdIkGZ7DqEdeXzfHjJJqz2H7Yv/b5VkD7BI9hyS1MJz3ADyHLxUnuSOnKeQJP8st0g9hfkJpu+n3SQc+m/lhecYIEnaT6rfnTOZnqD2nnj3+S7phEyovVntrJEW+2bSOlSskTcWm3Lds65y6NNCnH1/u5TOrBhOUq7WQXmODQeveOZ6ZpM0h17rqWIdDLuxGKQlq99Mx0nWwcAbi14XLRmBDLKxuB/PTnuopdh7bCzWwaY5bCzmYk+FxS1czDYUVPs0AAAAAAAAAAAAAAAAAMktXsp+W60/TE1vogvWH4fPKfrZ+dff7dYRJklxS/YiTE3SHKwXTLcB7FHKJDLQYWc1S7N1gRSlZDdCllbrAfnuyQ81qlkHmN4VpylLWl7jcX1O6EBTDMfc94UPZha6nB+GSXLHQY1xc3OUvF0sivblsLwNkPnH3Lf+aCpOqvbV8NZBquTDSyJFWl5/KfbFUN5at5PK/rocbGqdAsFRl7yaeekIxEnB0O0gDi//4wieVVo6NXXMvwSySX6dmjR1mvbCKx/RhBr1wvdfGFKyJslZarpYglnqvHVCINYUk1lxU+23uYjpmNuuUvyZQWpsv/c/rniT5KdkdZ0lZUPvA72nqDebdYH5/Y5VnaNeLMX6QLppFmV3rgTDmJeNgOt4sEmLjQPh/7zy45J9KYT4XwcNz4odEuHNrJzsnuo1dCB7sIdbvV87VC25cP9C9jZwHpPi/vifcfHlicySoqv2XtibjmI2JC+plYf/jB7TntJ0sezJXqVp0UWcbGhTlOTrEwZyfGWnee/zt/nmTnCSylOVrMM6Rx20TPYNFMk/uqkvq/WSduev3F7se2hStWFgH+pJAUJUDDYMbFIrNgy4KMmPEwnqrIu52lCIpNgwMP1+IQ0mWpil3YaBImUbB8KsZCNBtREBAAAAAAAAAAAAAAAAAIBarmwUcLqyu0AgcJL/ZCBghBAImGUBAAAAAAAAAAAAAAAAkrL3fnbOTaVU6wz6hbM/EuwskPzV3wXSTksE70JIf1qykpydiED+1i5VGweBzJK3cRBIlrTaKAgk6GKcvk4gRfI6q6/7q4lA/oaT6mm3qOrKEcjfWBTPu2dYav9aIKGkcwOJWsyWP+3rBFL0k7/4+0DqS6Y1qgVK1t8FolMK9iqV107iaOp/GMjsrjZ/yt9+k8Jf3I6OSUp2Iq9sF3/a1+EkO5M028XR17sikCR5txY7+npXBLLrVfRzHmB+RCBl9jp07OsEcghlcovXlbe+COQ2l2qDSvvmXyxurYMH8vzWJeq9to2TyaJm/5bgol5kf9X0whcbg//X6vxrHMuejtK6NV34ZCPY8j8VSMqS2h5uP551sdkXwyQpTnZHnSXlal8Js6Qt2H2lSTHZF8KsuNpvhfmrE8GW7L9sUrOBYPrRRQAAAAAAAAAAAP4Hwd4wVc8Xf9oAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/components/HousePage/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousePage/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousePage/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousePage/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css__ = __webpack_require__("./node_modules/normalize.css/normalize.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_normalize_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Layout_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Footer__ = __webpack_require__("./src/components/Footer/Footer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Panel__ = __webpack_require__("./src/components/Panel/Panel.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.js';
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */









class Layout extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-md-12 col-xs-12', style: { padding: '0' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__Panel__["a" /* default */], { mainPanel: this.props.mainPanel, __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }),
      this.props.children,
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__Footer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      })
    );
  }
}

Layout.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool
};
Layout.defaultProps = {
  mainPanel: false
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_normalize_css___default.a, __WEBPACK_IMPORTED_MODULE_4__Layout_css___default.a)(Layout));

/***/ }),

/***/ "./src/components/Loader/Loader.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Loader_scss__ = __webpack_require__("./src/components/Loader/Loader.scss");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Loader_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Loader_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_gif__ = __webpack_require__("./src/components/Loader/loader.gif");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_gif___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__loader_gif__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Loader/Loader.js';






class Loader extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: this.props.loading ? 'loader loading' : 'loader', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_4__loader_gif___default.a, alt: 'loader gif', __source: {
            fileName: _jsxFileName,
            lineNumber: 15
          },
          __self: this
        })
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'loader-info', __source: {
            fileName: _jsxFileName,
            lineNumber: 17
          },
          __self: this
        },
        '\u0645\u0646\u062A\u0638\u0631 \u0628\u0627\u0634\u06CC\u062F'
      )
    );
  }
}

Loader.propTypes = {
  loading: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__Loader_scss___default.a)(Loader));

/***/ }),

/***/ "./src/components/Loader/Loader.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Loader/loader.gif":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Loader/loader.gif?be5c3223";

/***/ }),

/***/ "./src/components/Panel/Panel.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Panel/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png__ = __webpack_require__("./src/components/Panel/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__history__ = __webpack_require__("./src/history.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/Panel.js';
/* eslint-disable react/no-danger */










class Panel extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {

  render() {
    console.log(this.props.currUser);
    console.log(this.props.isLoggedIn);
    if (this.props.mainPanel) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'header',
        { className: 'hidden-xs', __source: {
            fileName: _jsxFileName,
            lineNumber: 25
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container-fluid', __source: {
              fileName: _jsxFileName,
              lineNumber: 26
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row header-main', __source: {
                fileName: _jsxFileName,
                lineNumber: 27
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 28
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'container', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row flex-center header-main-boarder', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9 ', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 31
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h5',
                      { className: 'text-align-right white-font persian ', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 32
                        },
                        __self: this
                      },
                      '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                    ),
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'dropdown-content', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 35
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'container-fluid', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 36
                          },
                          __self: this
                        },
                        this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          {
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 38
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 39
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'h4',
                              { className: 'persian text-align-right black-font ', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 40
                                },
                                __self: this
                              },
                              this.props.currUser.username
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 44
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-left persian light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 45
                                },
                                __self: this
                              },
                              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                                'span',
                                { className: 'light-grey-font', __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 46
                                  },
                                  __self: this
                                },
                                '\u062A\u0648\u0645\u0627\u0646',
                                this.props.currUser.balance
                              )
                            ),
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 light-grey-font pull-right  persian text-align-right no-padding', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 51
                                },
                                __self: this
                              },
                              '\u0627\u0639\u062A\u0628\u0627\u0631'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 55
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                              onClick: () => {
                                __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 56
                              },
                              __self: this
                            })
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 65
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u062E\u0631\u0648\u062C',
                              onClick: () => {
                                this.props.authActions.logoutUser();
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 66
                              },
                              __self: this
                            })
                          )
                        ),
                        !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 78
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0648\u0631\u0648\u062F',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 79
                            },
                            __self: this
                          })
                        )
                      )
                    )
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding  col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 93
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x white-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 94
                      },
                      __self: this
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'header',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid', __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row nav-cl', __source: {
              fileName: _jsxFileName,
              lineNumber: 107
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown', __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 109
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row flex-center ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 110
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'col-xs-9 ', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 111
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h5',
                    { className: 'text-align-right persian purple-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 112
                      },
                      __self: this
                    },
                    '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'dropdown-content', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 115
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'container-fluid', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 116
                        },
                        __self: this
                      },
                      this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        {
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 118
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 119
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'h4',
                            { className: 'black-font persian text-align-right blue-font', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 120
                              },
                              __self: this
                            },
                            this.props.currUser.username
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 124
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: ' light-grey-font col-xs-6 pull-left blue-font persian', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 125
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'span',
                              { className: 'light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 126
                                },
                                __self: this
                              },
                              '\u062A\u0648\u0645\u0627\u0646',
                              this.props.currUser.balance
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'light-grey-font col-xs-6 pull-right blue-font persian text-align-right no-padding', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 131
                              },
                              __self: this
                            },
                            '\u0627\u0639\u062A\u0628\u0627\u0631'
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 135
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                            className: 'white-font  blue-button persian text-align-center',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                            },
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 136
                            },
                            __self: this
                          })
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 145
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u062E\u0631\u0648\u062C',
                            onClick: () => {
                              this.props.authActions.logoutUser();
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 146
                            },
                            __self: this
                          })
                        )
                      ),
                      !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'row mar-top', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 158
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                          type: 'button',
                          value: '\u0648\u0631\u0648\u062F',
                          onClick: () => {
                            __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                          },
                          className: '  white-font  blue-button persian text-align-center',
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 159
                          },
                          __self: this
                        })
                      )
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'no-padding  col-xs-3', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 173
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x purple-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 174
                    },
                    __self: this
                  })
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-4 col-md-offset-6 nav-logo col-xs-12', __source: {
                fileName: _jsxFileName,
                lineNumber: 179
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 180
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row  ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 181
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'a',
                  {
                    className: 'flex-row-rev',
                    onClick: () => {
                      __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/');
                    },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 182
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding text-align-center col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 188
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { className: 'icon-cl', src: __WEBPACK_IMPORTED_MODULE_7__logo_png___default.a, alt: 'logo', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 189
                      },
                      __self: this
                    })
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 191
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian-bold text-align-right  persian blue-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 192
                        },
                        __self: this
                      },
                      '\u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                    )
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}
Panel.propTypes = {
  currUser: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired,
  isLoggedIn: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    authActions: Object(__WEBPACK_IMPORTED_MODULE_3_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser, isLoggedIn } = authentication;
  return {
    currUser,
    isLoggedIn
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Panel)));

/***/ }),

/***/ "./src/components/Panel/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Panel/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/config/compressionConfig.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// noinspection JSAnnotator
const levels = [{
  MAX_WIDTH: 600,
  MAX_HEIGHT: 600,
  DOCUMENT_SIZE: 300000
}, {
  MAX_WIDTH: 800,
  MAX_HEIGHT: 800,
  DOCUMENT_SIZE: 500000
}];

/* harmony default export */ __webpack_exports__["a"] = ({
  getLevel(level) {
    return levels[level - 1];
  }
});

/***/ }),

/***/ "./src/config/paths.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__urls__ = __webpack_require__("./src/config/urls.js");
/* eslint-disable prettier/prettier */


const loginApiUrl = `http://localhost:8080/Turtle/login`;
/* harmony export (immutable) */ __webpack_exports__["h"] = loginApiUrl;

const logoutApiUrl = `http://localhost:8080/Turtle/login`;
/* unused harmony export logoutApiUrl */

const getSearchHousesUrl = `http://localhost:8080/Turtle/SearchServlet`;
/* harmony export (immutable) */ __webpack_exports__["f"] = getSearchHousesUrl;

const getCreateHouseUrl = `http://localhost:8080/Turtle/api/HouseServlet`;
/* harmony export (immutable) */ __webpack_exports__["a"] = getCreateHouseUrl;

const getInitiateUrl = `http://localhost:8080/Turtle/home`;
/* harmony export (immutable) */ __webpack_exports__["e"] = getInitiateUrl;

const getCurrentUserUrl = userId => `http://localhost:8080/Turtle/api/IndividualServlet?userId=${userId}`;
/* harmony export (immutable) */ __webpack_exports__["b"] = getCurrentUserUrl;

const getHouseDetailUrl = houseId => `http://localhost:8080/Turtle/api/GetSingleHouse?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["c"] = getHouseDetailUrl;

const getHousePhoneUrl = houseId => `http://localhost:8080/Turtle/api/PaymentServlet?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["d"] = getHousePhoneUrl;

const increaseCreditUrl = amount => `http://localhost:8080/Turtle/api/CreditServlet?value=${amount}`;
/* harmony export (immutable) */ __webpack_exports__["g"] = increaseCreditUrl;


const searchDriverUrl = phoneNumber => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/phoneNumber/${phoneNumber}`;
/* unused harmony export searchDriverUrl */

const getDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export getDriverAcquisitionInfoUrl */

const updateDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export updateDriverAcquisitionInfoUrl */

const updateDriverDocsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export updateDriverDocsUrl */

const approveDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/approve/${driverId}`;
/* unused harmony export approveDriverUrl */

const rejectDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/reject/${driverId}`;
/* unused harmony export rejectDriverUrl */

const getDriverDocumentUrl = documentId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/document/${documentId}`;
/* unused harmony export getDriverDocumentUrl */

const getDriverDocumentIdsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export getDriverDocumentIdsUrl */


/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__ = __webpack_require__("history/createBrowserHistory");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__);
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */



// NavigationBar manager, e.g. history.push('/home')
// https://github.com/mjackson/history
/* harmony default export */ __webpack_exports__["a"] = (false && __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default()());

/***/ }),

/***/ "./src/routes/houses/houseDetail/HouseDetailContainer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_house__ = __webpack_require__("./src/actions/house.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_HousePage__ = __webpack_require__("./src/components/HousePage/HousePage.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_Loader__ = __webpack_require__("./src/components/Loader/Loader.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/houses/houseDetail/HouseDetailContainer.js';









class HouseDetailContainer extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.handleGetPhone = () => {
      this.props.houseActions.getHousePhoneNumber(this.props.houseId);
    }, _temp;
  }

  componentDidMount() {
    this.props.houseActions.getHouseDetail(this.props.houseId, location.pathname);
  }

  render() {
    console.log(this.props.houseMessage);
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_HousePage__["a" /* default */], {
        house: this.props.houseDetail,
        currUser: this.props.currUser,
        getPhone: this.handleGetPhone,
        houseMessage: this.props.houseMessage,
        phoneStatus: this.props.phoneStatus,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        },
        __self: this
      }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_Loader__["a" /* default */], { loading: this.props.isFetching, __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      })
    );
  }
}

HouseDetailContainer.propTypes = {
  houseActions: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  isFetching: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.bool.isRequired,
  houseId: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.string.isRequired,
  houseDetail: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  houseMessage: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.string.isRequired,
  currUser: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  phoneStatus: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.string.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    houseActions: Object(__WEBPACK_IMPORTED_MODULE_2_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_4__actions_house__, dispatch),
    authActions: Object(__WEBPACK_IMPORTED_MODULE_2_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { house } = state;
  const { authentication } = state;
  const { isFetching, houseDetail, houseMessage, phoneStatus } = house;
  const { currUser } = authentication;
  return {
    isFetching,
    houseDetail,
    houseMessage,
    currUser,
    phoneStatus
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(HouseDetailContainer));

/***/ }),

/***/ "./src/routes/houses/houseDetail/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__HouseDetailContainer__ = __webpack_require__("./src/routes/houses/houseDetail/HouseDetailContainer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Layout__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/houses/houseDetail/index.js';




const title = 'اضافه کردن خانه';

function action({ params: { houseId } }) {
  return {
    chunks: ['houseDetail'],
    title,
    component: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_2__components_Layout__["a" /* default */],
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__HouseDetailContainer__["a" /* default */], { houseId: houseId, __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      }),
      ','
    )
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/utils/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export compressFile */
/* harmony export (immutable) */ __webpack_exports__["c"] = getLoginInfo;
/* harmony export (immutable) */ __webpack_exports__["d"] = getPostConfig;
/* unused harmony export getPutConfig */
/* harmony export (immutable) */ __webpack_exports__["b"] = getGetConfig;
/* unused harmony export convertNumbersToPersian */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchApi; });
/* unused harmony export setImmediate */
/* unused harmony export setCookie */
/* unused harmony export getCookie */
/* unused harmony export eraseCookie */
/* harmony export (immutable) */ __webpack_exports__["e"] = notifyError;
/* harmony export (immutable) */ __webpack_exports__["f"] = notifySuccess;
/* unused harmony export reloadSelectPicker */
/* unused harmony export loadSelectPicker */
/* unused harmony export getFormData */
/* unused harmony export loadUserLoginStatus */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js__ = __webpack_require__("exif-js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_exif_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__ = __webpack_require__("./src/config/compressionConfig.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

let processFile = (() => {
  var _ref = _asyncToGenerator(function* (dataURL, fileSize, fileType, documentType) {
    const image = new Image();
    image.src = dataURL;
    return new Promise(function (resolve, reject) {
      image.onload = function () {
        const imageMeasurements = getImageMeasurements(image);
        if (!shouldResize(imageMeasurements, fileSize, documentType)) {
          resolve(dataURLtoBlob(dataURL, fileType));
        }
        const scaledImageMeasurements = getScaledImageMeasurements(imageMeasurements.width, imageMeasurements.height, documentType);
        const scaledImageFile = getScaledImageFileFromCanvas(image, scaledImageMeasurements.width, scaledImageMeasurements.height, imageMeasurements.transform, fileType);
        resolve(scaledImageFile);
      };
      image.onerror = reject;
    });
  });

  return function processFile(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }







const compressionFirstLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(1);
const compressionSecondLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(2);
const documentsCompressionLevel = {
  profilePicture: compressionFirstLevel,
  driverLicence: compressionFirstLevel,
  carIdCard: compressionFirstLevel,
  insurance: compressionSecondLevel
};

function getCompressionLevel(documentType) {
  return documentsCompressionLevel[documentType] || compressionFirstLevel;
}

function dataURLtoBlob(dataURL, fileType) {
  const binary = atob(dataURL.split(',')[1]);
  const array = [];
  const length = binary.length;
  let i;
  for (i = 0; i < length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: fileType });
}

function getScaledImageMeasurements(width, height, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH } = getCompressionLevel(documentType);

  if (width > height) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  } else {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
  return {
    height,
    width
  };
}

function getImageOrientation(image) {
  let orientation = 0;
  __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getData(image, function () {
    orientation = __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getTag(this, 'Orientation');
  });
  return orientation;
}

function getImageMeasurements(image) {
  const orientation = getImageOrientation(image);
  let transform;
  let swap = false;
  switch (orientation) {
    case 8:
      swap = true;
      transform = 'left';
      break;
    case 6:
      swap = true;
      transform = 'right';
      break;
    case 3:
      transform = 'flip';
      break;
    default:
  }
  const width = swap ? image.height : image.width;
  const height = swap ? image.width : image.height;
  return {
    width,
    height,
    transform
  };
}

function getScaledImageFileFromCanvas(image, width, height, transform, fileType) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const context = canvas.getContext('2d');
  let transformParams;
  let swapMeasure = false;
  let imageWidth = width;
  let imageHeight = height;
  switch (transform) {
    case 'left':
      transformParams = [0, -1, 1, 0, 0, height];
      swapMeasure = true;
      break;
    case 'right':
      transformParams = [0, 1, -1, 0, width, 0];
      swapMeasure = true;
      break;
    case 'flip':
      transformParams = [1, 0, 0, -1, 0, height];
      break;
    default:
      transformParams = [1, 0, 0, 1, 0, 0];
  }
  context.setTransform(...transformParams);
  if (swapMeasure) {
    imageHeight = width;
    imageWidth = height;
  }
  context.drawImage(image, 0, 0, imageWidth, imageHeight);
  context.setTransform(1, 0, 0, 1, 0, 0);
  const dataURL = canvas.toDataURL(fileType);
  return dataURLtoBlob(dataURL, fileType);
}

function shouldResize(imageMeasurements, fileSize, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH, DOCUMENT_SIZE } = getCompressionLevel(documentType);
  if (documentType === 'insurance' && fileSize < DOCUMENT_SIZE) {
    return false;
  } else if (fileSize < DOCUMENT_SIZE) {
    return false;
  }
  return imageMeasurements.width > MAX_WIDTH || imageMeasurements.height > MAX_HEIGHT;
}

function compressFile(file, documentType = 'default') {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = _asyncToGenerator(function* () {
        const compressedFile = yield processFile(reader.result, file.size, file.type, documentType);
        resolve(compressedFile);
      });
    } catch (err) {
      reject(err);
    }
  });
}

function getLoginInfo() {
  try {
    const loginInfo = localStorage.getItem('loginInfo');
    if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isString(loginInfo)) {
      return JSON.parse(loginInfo);
    }
  } catch (err) {
    console.log('Error when parsing loginInfo object: ', err);
  }
  return null;
}
function getPostConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    mimeType: 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json', mimeType: 'application/json' };
  return _extends({
    method: 'POST',
    // mode: 'no-cors',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getPutConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    'X-Authorization': loginInfo.token
  } : { 'Content-Type': 'application/json' };
  return _extends({
    method: 'PUT',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getGetConfig() {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json' };
  return {
    method: 'GET',
    headers
  };
}

function convertNumbersToPersian(str) {
  const persianNumbers = '۰۱۲۳۴۵۶۷۸۹';
  let result = '';
  if (!str && str !== 0) {
    return result;
  }
  str = str.toString();
  let convertedChar;
  for (let i = 0; i < str.length; i++) {
    try {
      convertedChar = persianNumbers[str[i]];
    } catch (err) {
      console.log(err);
      convertedChar = str[i];
    }
    result += convertedChar;
  }
  return result;
}

let fetchApi = (() => {
  var _ref3 = _asyncToGenerator(function* (url, config) {
    let flowError;
    try {
      const res = yield fetch(url, config);
      console.log(res);
      console.log(res.status);
      let result = yield res.json();
      console.log(result);
      if (!__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.get(result, 'data'))) {
        result = result.data;
      }
      console.log(res.status);

      if (res.status !== 200) {
        flowError = result;
        console.log(res.status);

        if (res.status === 403) {
          localStorage.clear();
          flowError.message = 'لطفا دوباره وارد شوید!';
          console.log('yeees');
          setTimeout(function () {
            return __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push('/login');
          }, 3000);
        }
      } else {
        return result;
      }
    } catch (err) {
      console.log(err);
      console.debug(`Error when fetching api: ${url}`, err);
      flowError = {
        code: 'UNKNOWN_ERROR',
        message: 'خطای نامشخص لطفا دوباره سعی کنید'
      };
    }
    if (flowError) {
      throw flowError;
    }
  });

  return function fetchApi(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
})();

let setImmediate = (() => {
  var _ref4 = _asyncToGenerator(function* (fn) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        let value = null;
        if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isFunction(fn)) {
          value = fn();
        }
        resolve(value);
      }, 0);
    });
  });

  return function setImmediate(_x7) {
    return _ref4.apply(this, arguments);
  };
})();

function setCookie(name, value, days = 7) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
function getCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  const length = ca.length;
  let i = 0;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

function notifyError(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '10000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.error(message, 'خطا');
}

function notifySuccess(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.success(message, '');
}

function reloadSelectPicker() {
  try {
    $('.selectpicker').selectpicker('render');
  } catch (err) {
    console.log(err);
  }
}

function loadSelectPicker() {
  try {
    $('.selectpicker').selectpicker({});
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      $('.selectpicker').selectpicker('mobile');
    }
  } catch (err) {
    console.log(err);
  }
}

function getFormData(data) {
  if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(data)) {
    throw Error('Invalid input data to create a FormData object');
  }
  let formData;
  try {
    formData = new FormData();
    const keys = __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.keys(data);
    let i;
    const length = keys.length;
    for (i = 0; i < length; i++) {
      formData.append(keys[i], data[keys[i]]);
    }
    return formData;
  } catch (err) {
    console.debug(err);
    throw Error('FORM_DATA_BROWSER_SUPPORT_FAILURE');
  }
}

function loadUserLoginStatus(store) {
  const loginStatusAction = Object(__WEBPACK_IMPORTED_MODULE_4__actions_authentication__["loadLoginStatus"])();
  store.dispatch(loginStatusAction);
}

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL2hvdXNlRGV0YWlsLmpzIiwic291cmNlcyI6WyIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG91c2VQYWdlL21haW4uY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL3VybC9lc2NhcGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS5jc3Mvbm9ybWFsaXplLmNzcz84MjFiIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYWN0aW9ucy9hdXRoZW50aWNhdGlvbi5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2FjdGlvbnMvaG91c2UuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hcGktaGFuZGxlcnMvYXV0aGVudGljYXRpb24uanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hcGktaGFuZGxlcnMvaG91c2UuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmciLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmciLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzPzliNGUiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlUGFnZS9Ib3VzZVBhZ2UuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlUGFnZS9nZW9tZXRyeTIucG5nIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0hvdXNlUGFnZS9tYWluLmNzcz85NDRlIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzPzUwNzkiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzPzY1NjUiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9sb2FkZXIuZ2lmIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9QYW5lbC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3M/MWNmNCIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbmZpZy9jb21wcmVzc2lvbkNvbmZpZy5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbmZpZy9wYXRocy5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2hpc3RvcnkuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9yb3V0ZXMvaG91c2VzL2hvdXNlRGV0YWlsL0hvdXNlRGV0YWlsQ29udGFpbmVyLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvcm91dGVzL2hvdXNlcy9ob3VzZURldGFpbC9pbmRleC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL3V0aWxzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi8qISBub3JtYWxpemUuY3NzIHY3LjAuMCB8IE1JVCBMaWNlbnNlIHwgZ2l0aHViLmNvbS9uZWNvbGFzL25vcm1hbGl6ZS5jc3MgKi9cXG4vKiBEb2N1bWVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgbGluZSBoZWlnaHQgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIFByZXZlbnQgYWRqdXN0bWVudHMgb2YgZm9udCBzaXplIGFmdGVyIG9yaWVudGF0aW9uIGNoYW5nZXMgaW5cXG4gKiAgICBJRSBvbiBXaW5kb3dzIFBob25lIGFuZCBpbiBpT1MuXFxuICovXFxuaHRtbCB7XFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIC1tcy10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqLyB9XFxuLyogU2VjdGlvbnNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIFJlbW92ZSB0aGUgbWFyZ2luIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqL1xcbmJvZHkge1xcbiAgbWFyZ2luOiAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXJ0aWNsZSxcXG5hc2lkZSxcXG5mb290ZXIsXFxuaGVhZGVyLFxcbm5hdixcXG5zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgZm9udCBzaXplIGFuZCBtYXJnaW4gb24gYGgxYCBlbGVtZW50cyB3aXRoaW4gYHNlY3Rpb25gIGFuZFxcbiAqIGBhcnRpY2xlYCBjb250ZXh0cyBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBTYWZhcmkuXFxuICovXFxuaDEge1xcbiAgZm9udC1zaXplOiAyZW07XFxuICBtYXJnaW46IDAuNjdlbSAwOyB9XFxuLyogR3JvdXBpbmcgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxuZmlnY2FwdGlvbixcXG5maWd1cmUsXFxubWFpbiB7XFxuICAvKiAxICovXFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBtYXJnaW4gaW4gSUUgOC5cXG4gKi9cXG5maWd1cmUge1xcbiAgbWFyZ2luOiAxZW0gNDBweDsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIEZpcmVmb3guXFxuICogMi4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZSBhbmQgSUUuXFxuICovXFxuaHIge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAvKiAxICovXFxuICBoZWlnaHQ6IDA7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnByZSB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qIFRleHQtbGV2ZWwgc2VtYW50aWNzXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGdyYXkgYmFja2dyb3VuZCBvbiBhY3RpdmUgbGlua3MgaW4gSUUgMTAuXFxuICogMi4gUmVtb3ZlIGdhcHMgaW4gbGlua3MgdW5kZXJsaW5lIGluIGlPUyA4KyBhbmQgU2FmYXJpIDgrLlxcbiAqL1xcbmEge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAvKiAxICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbi1za2lwOiBvYmplY3RzO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBib3R0b20gYm9yZGVyIGluIENocm9tZSA1Ny0gYW5kIEZpcmVmb3ggMzktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB0ZXh0IGRlY29yYXRpb24gaW4gQ2hyb21lLCBFZGdlLCBJRSwgT3BlcmEsIGFuZCBTYWZhcmkuXFxuICovXFxuYWJiclt0aXRsZV0ge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gIC8qIDEgKi9cXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFByZXZlbnQgdGhlIGR1cGxpY2F0ZSBhcHBsaWNhdGlvbiBvZiBgYm9sZGVyYCBieSB0aGUgbmV4dCBydWxlIGluIFNhZmFyaSA2LlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBpbmhlcml0OyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgd2VpZ2h0IGluIENocm9tZSwgRWRnZSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogYm9sZGVyOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuY29kZSxcXG5rYmQsXFxuc2FtcCB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHN0eWxlIGluIEFuZHJvaWQgNC4zLS5cXG4gKi9cXG5kZm4ge1xcbiAgZm9udC1zdHlsZTogaXRhbGljOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGJhY2tncm91bmQgYW5kIGNvbG9yIGluIElFIDktLlxcbiAqL1xcbm1hcmsge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmMDtcXG4gIGNvbG9yOiAjMDAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc21hbGwge1xcbiAgZm9udC1zaXplOiA4MCU7IH1cXG4vKipcXG4gKiBQcmV2ZW50IGBzdWJgIGFuZCBgc3VwYCBlbGVtZW50cyBmcm9tIGFmZmVjdGluZyB0aGUgbGluZSBoZWlnaHQgaW5cXG4gKiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3ViLFxcbnN1cCB7XFxuICBmb250LXNpemU6IDc1JTtcXG4gIGxpbmUtaGVpZ2h0OiAwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lOyB9XFxuc3ViIHtcXG4gIGJvdHRvbTogLTAuMjVlbTsgfVxcbnN1cCB7XFxuICB0b3A6IC0wLjVlbTsgfVxcbi8qIEVtYmVkZGVkIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmF1ZGlvLFxcbnZpZGVvIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGlPUyA0LTcuXFxuICovXFxuYXVkaW86bm90KFtjb250cm9sc10pIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBoZWlnaHQ6IDA7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGJvcmRlciBvbiBpbWFnZXMgaW5zaWRlIGxpbmtzIGluIElFIDEwLS5cXG4gKi9cXG5pbWcge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lOyB9XFxuLyoqXFxuICogSGlkZSB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICovXFxuc3ZnOm5vdCg6cm9vdCkge1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjsgfVxcbi8qIEZvcm1zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDaGFuZ2UgdGhlIGZvbnQgc3R5bGVzIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqIDIuIFJlbW92ZSB0aGUgbWFyZ2luIGluIEZpcmVmb3ggYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQsXFxub3B0Z3JvdXAsXFxuc2VsZWN0LFxcbnRleHRhcmVhIHtcXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICBtYXJnaW46IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBTaG93IHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKiAxLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCB7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRWRnZSwgRmlyZWZveCwgYW5kIElFLlxcbiAqIDEuIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b24sXFxuc2VsZWN0IHtcXG4gIC8qIDEgKi9cXG4gIHRleHQtdHJhbnNmb3JtOiBub25lOyB9XFxuLyoqXFxuICogMS4gUHJldmVudCBhIFdlYktpdCBidWcgd2hlcmUgKDIpIGRlc3Ryb3lzIG5hdGl2ZSBgYXVkaW9gIGFuZCBgdmlkZW9gXFxuICogICAgY29udHJvbHMgaW4gQW5kcm9pZCA0LlxcbiAqIDIuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmh0bWwgW3R5cGU9XFxcImJ1dHRvblxcXCJdLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgYm9yZGVyIGFuZCBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qKlxcbiAqIFJlc3RvcmUgdGhlIGZvY3VzIHN0eWxlcyB1bnNldCBieSB0aGUgcHJldmlvdXMgcnVsZS5cXG4gKi9cXG5idXR0b246LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTotbW96LWZvY3VzcmluZyB7XFxuICBvdXRsaW5lOiAxcHggZG90dGVkIEJ1dHRvblRleHQ7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuZmllbGRzZXQge1xcbiAgcGFkZGluZzogMC4zNWVtIDAuNzVlbSAwLjYyNWVtOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgdGV4dCB3cmFwcGluZyBpbiBFZGdlIGFuZCBJRS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBjb2xvciBpbmhlcml0YW5jZSBmcm9tIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gSUUuXFxuICogMy4gUmVtb3ZlIHRoZSBwYWRkaW5nIHNvIGRldmVsb3BlcnMgYXJlIG5vdCBjYXVnaHQgb3V0IHdoZW4gdGhleSB6ZXJvIG91dFxcbiAqICAgIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmxlZ2VuZCB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBjb2xvcjogaW5oZXJpdDtcXG4gIC8qIDIgKi9cXG4gIGRpc3BsYXk6IHRhYmxlO1xcbiAgLyogMSAqL1xcbiAgbWF4LXdpZHRoOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDMgKi9cXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XFxuICAvKiAxICovIH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdmVydGljYWwgYWxpZ25tZW50IGluIENocm9tZSwgRmlyZWZveCwgYW5kIE9wZXJhLlxcbiAqL1xcbnByb2dyZXNzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIC8qIDEgKi9cXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgZGVmYXVsdCB2ZXJ0aWNhbCBzY3JvbGxiYXIgaW4gSUUuXFxuICovXFxudGV4dGFyZWEge1xcbiAgb3ZlcmZsb3c6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBJRSAxMC0uXFxuICogMi4gUmVtb3ZlIHRoZSBwYWRkaW5nIGluIElFIDEwLS5cXG4gKi9cXG5bdHlwZT1cXFwiY2hlY2tib3hcXFwiXSxcXG5bdHlwZT1cXFwicmFkaW9cXFwiXSB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgY3Vyc29yIHN0eWxlIG9mIGluY3JlbWVudCBhbmQgZGVjcmVtZW50IGJ1dHRvbnMgaW4gQ2hyb21lLlxcbiAqL1xcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixcXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xcbiAgaGVpZ2h0OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgb2RkIGFwcGVhcmFuY2UgaW4gQ2hyb21lIGFuZCBTYWZhcmkuXFxuICogMi4gQ29ycmVjdCB0aGUgb3V0bGluZSBzdHlsZSBpbiBTYWZhcmkuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4dGZpZWxkO1xcbiAgLyogMSAqL1xcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIHBhZGRpbmcgYW5kIGNhbmNlbCBidXR0b25zIGluIENocm9tZSBhbmQgU2FmYXJpIG9uIG1hY09TLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtY2FuY2VsLWJ1dHRvbixcXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKiAyLiBDaGFuZ2UgZm9udCBwcm9wZXJ0aWVzIHRvIGBpbmhlcml0YCBpbiBTYWZhcmkuXFxuICovXFxuOjotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQ6IGluaGVyaXQ7XFxuICAvKiAyICovIH1cXG4vKiBJbnRlcmFjdGl2ZVxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBFZGdlLCBJRSwgYW5kIEZpcmVmb3guXFxuICovXFxuZGV0YWlscyxcXG5tZW51IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3VtbWFyeSB7XFxuICBkaXNwbGF5OiBsaXN0LWl0ZW07IH1cXG4vKiBTY3JpcHRpbmdcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmNhbnZhcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG50ZW1wbGF0ZSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuLyogSGlkZGVuXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSAxMC0uXFxuICovXFxuW2hpZGRlbl0ge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsNEVBQTRFO0FBQzVFO2dGQUNnRjtBQUNoRjs7OztHQUlHO0FBQ0g7RUFDRSxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLDJCQUEyQjtFQUMzQixPQUFPO0VBQ1AsK0JBQStCO0VBQy9CLE9BQU8sRUFBRTtBQUNYO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIO0VBQ0UsVUFBVSxFQUFFO0FBQ2Q7O0dBRUc7QUFDSDs7Ozs7O0VBTUUsZUFBZSxFQUFFO0FBQ25COzs7R0FHRztBQUNIO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFFO0FBQ3JCO2dGQUNnRjtBQUNoRjs7O0dBR0c7QUFDSDs7O0VBR0UsT0FBTztFQUNQLGVBQWUsRUFBRTtBQUNuQjs7R0FFRztBQUNIO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7OztHQUdHO0FBQ0g7RUFDRSxnQ0FBZ0M7VUFDeEIsd0JBQXdCO0VBQ2hDLE9BQU87RUFDUCxVQUFVO0VBQ1YsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDtFQUNFLGtDQUFrQztFQUNsQyxPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU8sRUFBRTtBQUNYO2dGQUNnRjtBQUNoRjs7O0dBR0c7QUFDSDtFQUNFLDhCQUE4QjtFQUM5QixPQUFPO0VBQ1Asc0NBQXNDO0VBQ3RDLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIO0VBQ0Usb0JBQW9CO0VBQ3BCLE9BQU87RUFDUCwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLDBDQUEwQztVQUNsQyxrQ0FBa0M7RUFDMUMsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDs7RUFFRSxxQkFBcUIsRUFBRTtBQUN6Qjs7R0FFRztBQUNIOztFQUVFLG9CQUFvQixFQUFFO0FBQ3hCOzs7R0FHRztBQUNIOzs7RUFHRSxrQ0FBa0M7RUFDbEMsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIO0VBQ0UsbUJBQW1CLEVBQUU7QUFDdkI7O0dBRUc7QUFDSDtFQUNFLHVCQUF1QjtFQUN2QixZQUFZLEVBQUU7QUFDaEI7O0dBRUc7QUFDSDtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDs7RUFFRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQix5QkFBeUIsRUFBRTtBQUM3QjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsWUFBWSxFQUFFO0FBQ2hCO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIOztFQUVFLHNCQUFzQixFQUFFO0FBQzFCOztHQUVHO0FBQ0g7RUFDRSxjQUFjO0VBQ2QsVUFBVSxFQUFFO0FBQ2Q7O0dBRUc7QUFDSDtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCOztHQUVHO0FBQ0g7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7Ozs7O0VBS0Usd0JBQXdCO0VBQ3hCLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsVUFBVTtFQUNWLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIOztFQUVFLE9BQU87RUFDUCxrQkFBa0IsRUFBRTtBQUN0Qjs7O0dBR0c7QUFDSDs7RUFFRSxPQUFPO0VBQ1AscUJBQXFCLEVBQUU7QUFDekI7Ozs7R0FJRztBQUNIOzs7O0VBSUUsMkJBQTJCO0VBQzNCLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7Ozs7RUFJRSxtQkFBbUI7RUFDbkIsV0FBVyxFQUFFO0FBQ2Y7O0dBRUc7QUFDSDs7OztFQUlFLCtCQUErQixFQUFFO0FBQ25DOztHQUVHO0FBQ0g7RUFDRSwrQkFBK0IsRUFBRTtBQUNuQzs7Ozs7R0FLRztBQUNIO0VBQ0UsK0JBQStCO1VBQ3ZCLHVCQUF1QjtFQUMvQixPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU87RUFDUCxlQUFlO0VBQ2YsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixPQUFPO0VBQ1AsV0FBVztFQUNYLE9BQU87RUFDUCxvQkFBb0I7RUFDcEIsT0FBTyxFQUFFO0FBQ1g7OztHQUdHO0FBQ0g7RUFDRSxzQkFBc0I7RUFDdEIsT0FBTztFQUNQLHlCQUF5QjtFQUN6QixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIO0VBQ0UsZUFBZSxFQUFFO0FBQ25COzs7R0FHRztBQUNIOztFQUVFLCtCQUErQjtVQUN2Qix1QkFBdUI7RUFDL0IsT0FBTztFQUNQLFdBQVc7RUFDWCxPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOztFQUVFLGFBQWEsRUFBRTtBQUNqQjs7O0dBR0c7QUFDSDtFQUNFLDhCQUE4QjtFQUM5QixPQUFPO0VBQ1AscUJBQXFCO0VBQ3JCLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7O0VBRUUseUJBQXlCLEVBQUU7QUFDN0I7OztHQUdHO0FBQ0g7RUFDRSwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLGNBQWM7RUFDZCxPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7O0VBRUUsZUFBZSxFQUFFO0FBQ25COztHQUVHO0FBQ0g7RUFDRSxtQkFBbUIsRUFBRTtBQUN2QjtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDtFQUNFLHNCQUFzQixFQUFFO0FBQzFCOztHQUVHO0FBQ0g7RUFDRSxjQUFjLEVBQUU7QUFDbEI7Z0ZBQ2dGO0FBQ2hGOztHQUVHO0FBQ0g7RUFDRSxjQUFjLEVBQUVcIixcImZpbGVcIjpcIm5vcm1hbGl6ZS5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLyohIG5vcm1hbGl6ZS5jc3MgdjcuMC4wIHwgTUlUIExpY2Vuc2UgfCBnaXRodWIuY29tL25lY29sYXMvbm9ybWFsaXplLmNzcyAqL1xcbi8qIERvY3VtZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBsaW5lIGhlaWdodCBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gUHJldmVudCBhZGp1c3RtZW50cyBvZiBmb250IHNpemUgYWZ0ZXIgb3JpZW50YXRpb24gY2hhbmdlcyBpblxcbiAqICAgIElFIG9uIFdpbmRvd3MgUGhvbmUgYW5kIGluIGlPUy5cXG4gKi9cXG5odG1sIHtcXG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xcbiAgLyogMSAqL1xcbiAgLW1zLXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7XFxuICAvKiAyICovXFxuICAtd2Via2l0LXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7XFxuICAvKiAyICovIH1cXG4vKiBTZWN0aW9uc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogUmVtb3ZlIHRoZSBtYXJnaW4gaW4gYWxsIGJyb3dzZXJzIChvcGluaW9uYXRlZCkuXFxuICovXFxuYm9keSB7XFxuICBtYXJnaW46IDA7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5hcnRpY2xlLFxcbmFzaWRlLFxcbmZvb3RlcixcXG5oZWFkZXIsXFxubmF2LFxcbnNlY3Rpb24ge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBmb250IHNpemUgYW5kIG1hcmdpbiBvbiBgaDFgIGVsZW1lbnRzIHdpdGhpbiBgc2VjdGlvbmAgYW5kXFxuICogYGFydGljbGVgIGNvbnRleHRzIGluIENocm9tZSwgRmlyZWZveCwgYW5kIFNhZmFyaS5cXG4gKi9cXG5oMSB7XFxuICBmb250LXNpemU6IDJlbTtcXG4gIG1hcmdpbjogMC42N2VtIDA7IH1cXG4vKiBHcm91cGluZyBjb250ZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG5maWdjYXB0aW9uLFxcbmZpZ3VyZSxcXG5tYWluIHtcXG4gIC8qIDEgKi9cXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IG1hcmdpbiBpbiBJRSA4LlxcbiAqL1xcbmZpZ3VyZSB7XFxuICBtYXJnaW46IDFlbSA0MHB4OyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGJveCBzaXppbmcgaW4gRmlyZWZveC5cXG4gKiAyLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlIGFuZCBJRS5cXG4gKi9cXG5ociB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gIC8qIDEgKi9cXG4gIGhlaWdodDogMDtcXG4gIC8qIDEgKi9cXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxucHJlIHtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogMiAqLyB9XFxuLyogVGV4dC1sZXZlbCBzZW1hbnRpY3NcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIFJlbW92ZSB0aGUgZ3JheSBiYWNrZ3JvdW5kIG9uIGFjdGl2ZSBsaW5rcyBpbiBJRSAxMC5cXG4gKiAyLiBSZW1vdmUgZ2FwcyBpbiBsaW5rcyB1bmRlcmxpbmUgaW4gaU9TIDgrIGFuZCBTYWZhcmkgOCsuXFxuICovXFxuYSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gIC8qIDEgKi9cXG4gIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLXNraXA6IG9iamVjdHM7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGJvdHRvbSBib3JkZXIgaW4gQ2hyb21lIDU3LSBhbmQgRmlyZWZveCAzOS0uXFxuICogMi4gQWRkIHRoZSBjb3JyZWN0IHRleHQgZGVjb3JhdGlvbiBpbiBDaHJvbWUsIEVkZ2UsIElFLCBPcGVyYSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5hYmJyW3RpdGxlXSB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgLyogMSAqL1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAvKiAyICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgZG90dGVkO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUHJldmVudCB0aGUgZHVwbGljYXRlIGFwcGxpY2F0aW9uIG9mIGBib2xkZXJgIGJ5IHRoZSBuZXh0IHJ1bGUgaW4gU2FmYXJpIDYuXFxuICovXFxuYixcXG5zdHJvbmcge1xcbiAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCB3ZWlnaHQgaW4gQ2hyb21lLCBFZGdlLCBhbmQgU2FmYXJpLlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmhlcml0YW5jZSBhbmQgc2NhbGluZyBvZiBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIENvcnJlY3QgdGhlIG9kZCBgZW1gIGZvbnQgc2l6aW5nIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5jb2RlLFxcbmtiZCxcXG5zYW1wIHtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc3R5bGUgaW4gQW5kcm9pZCA0LjMtLlxcbiAqL1xcbmRmbiB7XFxuICBmb250LXN0eWxlOiBpdGFsaWM7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgYmFja2dyb3VuZCBhbmQgY29sb3IgaW4gSUUgOS0uXFxuICovXFxubWFyayB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYwO1xcbiAgY29sb3I6ICMwMDA7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zbWFsbCB7XFxuICBmb250LXNpemU6IDgwJTsgfVxcbi8qKlxcbiAqIFByZXZlbnQgYHN1YmAgYW5kIGBzdXBgIGVsZW1lbnRzIGZyb20gYWZmZWN0aW5nIHRoZSBsaW5lIGhlaWdodCBpblxcbiAqIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zdWIsXFxuc3VwIHtcXG4gIGZvbnQtc2l6ZTogNzUlO1xcbiAgbGluZS1oZWlnaHQ6IDA7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB2ZXJ0aWNhbC1hbGlnbjogYmFzZWxpbmU7IH1cXG5zdWIge1xcbiAgYm90dG9tOiAtMC4yNWVtOyB9XFxuc3VwIHtcXG4gIHRvcDogLTAuNWVtOyB9XFxuLyogRW1iZWRkZWQgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXVkaW8sXFxudmlkZW8ge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gaU9TIDQtNy5cXG4gKi9cXG5hdWRpbzpub3QoW2NvbnRyb2xzXSkge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGhlaWdodDogMDsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgYm9yZGVyIG9uIGltYWdlcyBpbnNpZGUgbGlua3MgaW4gSUUgMTAtLlxcbiAqL1xcbmltZyB7XFxuICBib3JkZXItc3R5bGU6IG5vbmU7IH1cXG4vKipcXG4gKiBIaWRlIHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKi9cXG5zdmc6bm90KDpyb290KSB7XFxuICBvdmVyZmxvdzogaGlkZGVuOyB9XFxuLyogRm9ybXNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIENoYW5nZSB0aGUgZm9udCBzdHlsZXMgaW4gYWxsIGJyb3dzZXJzIChvcGluaW9uYXRlZCkuXFxuICogMi4gUmVtb3ZlIHRoZSBtYXJnaW4gaW4gRmlyZWZveCBhbmQgU2FmYXJpLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCxcXG5vcHRncm91cCxcXG5zZWxlY3QsXFxudGV4dGFyZWEge1xcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDEwMCU7XFxuICAvKiAxICovXFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIG1hcmdpbjogMDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFNob3cgdGhlIG92ZXJmbG93IGluIElFLlxcbiAqIDEuIFNob3cgdGhlIG92ZXJmbG93IGluIEVkZ2UuXFxuICovXFxuYnV0dG9uLFxcbmlucHV0IHtcXG4gIC8qIDEgKi9cXG4gIG92ZXJmbG93OiB2aXNpYmxlOyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbmhlcml0YW5jZSBvZiB0ZXh0IHRyYW5zZm9ybSBpbiBFZGdlLCBGaXJlZm94LCBhbmQgSUUuXFxuICogMS4gUmVtb3ZlIHRoZSBpbmhlcml0YW5jZSBvZiB0ZXh0IHRyYW5zZm9ybSBpbiBGaXJlZm94LlxcbiAqL1xcbmJ1dHRvbixcXG5zZWxlY3Qge1xcbiAgLyogMSAqL1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7IH1cXG4vKipcXG4gKiAxLiBQcmV2ZW50IGEgV2ViS2l0IGJ1ZyB3aGVyZSAoMikgZGVzdHJveXMgbmF0aXZlIGBhdWRpb2AgYW5kIGB2aWRlb2BcXG4gKiAgICBjb250cm9scyBpbiBBbmRyb2lkIDQuXFxuICogMi4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaHRtbCBbdHlwZT1cXFwiYnV0dG9uXFxcIl0sXFxuW3R5cGU9XFxcInJlc2V0XFxcIl0sXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbm5lciBib3JkZXIgYW5kIHBhZGRpbmcgaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b246Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBib3JkZXItc3R5bGU6IG5vbmU7XFxuICBwYWRkaW5nOiAwOyB9XFxuLyoqXFxuICogUmVzdG9yZSB0aGUgZm9jdXMgc3R5bGVzIHVuc2V0IGJ5IHRoZSBwcmV2aW91cyBydWxlLlxcbiAqL1xcbmJ1dHRvbjotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwiYnV0dG9uXFxcIl06LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcInJlc2V0XFxcIl06LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdOi1tb3otZm9jdXNyaW5nIHtcXG4gIG91dGxpbmU6IDFweCBkb3R0ZWQgQnV0dG9uVGV4dDsgfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIHBhZGRpbmcgaW4gRmlyZWZveC5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBwYWRkaW5nOiAwLjM1ZW0gMC43NWVtIDAuNjI1ZW07IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSB0ZXh0IHdyYXBwaW5nIGluIEVkZ2UgYW5kIElFLlxcbiAqIDIuIENvcnJlY3QgdGhlIGNvbG9yIGluaGVyaXRhbmNlIGZyb20gYGZpZWxkc2V0YCBlbGVtZW50cyBpbiBJRS5cXG4gKiAzLiBSZW1vdmUgdGhlIHBhZGRpbmcgc28gZGV2ZWxvcGVycyBhcmUgbm90IGNhdWdodCBvdXQgd2hlbiB0aGV5IHplcm8gb3V0XFxuICogICAgYGZpZWxkc2V0YCBlbGVtZW50cyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxubGVnZW5kIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIC8qIDEgKi9cXG4gIGNvbG9yOiBpbmhlcml0O1xcbiAgLyogMiAqL1xcbiAgZGlzcGxheTogdGFibGU7XFxuICAvKiAxICovXFxuICBtYXgtd2lkdGg6IDEwMCU7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMyAqL1xcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcXG4gIC8qIDEgKi8gfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB2ZXJ0aWNhbCBhbGlnbm1lbnQgaW4gQ2hyb21lLCBGaXJlZm94LCBhbmQgT3BlcmEuXFxuICovXFxucHJvZ3Jlc3Mge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgLyogMSAqL1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBkZWZhdWx0IHZlcnRpY2FsIHNjcm9sbGJhciBpbiBJRS5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICBvdmVyZmxvdzogYXV0bzsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIElFIDEwLS5cXG4gKiAyLiBSZW1vdmUgdGhlIHBhZGRpbmcgaW4gSUUgMTAtLlxcbiAqL1xcblt0eXBlPVxcXCJjaGVja2JveFxcXCJdLFxcblt0eXBlPVxcXCJyYWRpb1xcXCJdIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIC8qIDEgKi9cXG4gIHBhZGRpbmc6IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBjdXJzb3Igc3R5bGUgb2YgaW5jcmVtZW50IGFuZCBkZWNyZW1lbnQgYnV0dG9ucyBpbiBDaHJvbWUuXFxuICovXFxuW3R5cGU9XFxcIm51bWJlclxcXCJdOjotd2Via2l0LWlubmVyLXNwaW4tYnV0dG9uLFxcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7XFxuICBoZWlnaHQ6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBvZGQgYXBwZWFyYW5jZSBpbiBDaHJvbWUgYW5kIFNhZmFyaS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvdXRsaW5lIHN0eWxlIGluIFNhZmFyaS5cXG4gKi9cXG5bdHlwZT1cXFwic2VhcmNoXFxcIl0ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQ7XFxuICAvKiAxICovXFxuICBvdXRsaW5lLW9mZnNldDogLTJweDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgcGFkZGluZyBhbmQgY2FuY2VsIGJ1dHRvbnMgaW4gQ2hyb21lIGFuZCBTYWZhcmkgb24gbWFjT1MuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdOjotd2Via2l0LXNlYXJjaC1jYW5jZWwtYnV0dG9uLFxcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtZGVjb3JhdGlvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmFiaWxpdHkgdG8gc3R5bGUgY2xpY2thYmxlIHR5cGVzIGluIGlPUyBhbmQgU2FmYXJpLlxcbiAqIDIuIENoYW5nZSBmb250IHByb3BlcnRpZXMgdG8gYGluaGVyaXRgIGluIFNhZmFyaS5cXG4gKi9cXG46Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9uIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xcbiAgLyogMSAqL1xcbiAgZm9udDogaW5oZXJpdDtcXG4gIC8qIDIgKi8gfVxcbi8qIEludGVyYWN0aXZlXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIEVkZ2UsIElFLCBhbmQgRmlyZWZveC5cXG4gKi9cXG5kZXRhaWxzLFxcbm1lbnUge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zdW1tYXJ5IHtcXG4gIGRpc3BsYXk6IGxpc3QtaXRlbTsgfVxcbi8qIFNjcmlwdGluZ1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuY2FudmFzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFLlxcbiAqL1xcbnRlbXBsYXRlIHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG4vKiBIaWRkZW5cXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDEwLS5cXG4gKi9cXG5baGlkZGVuXSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG4gICAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICAgIGNvbG9yOiB3aGl0ZTtcXG59XFxuLmJsYWNrIHtcXG4gICAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gICAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzYWQyY2JcXG59XFxuLndoaXRlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgICBmb250LXdlaWdodDogODAwO1xcbn1cXG4uZm9udC1saWdodCB7XFxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xcbiAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5uby1saXN0LXN0eWxlIHtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gICAgb3V0bGluZTogbm9uZTtcXG4gICAgYm9yZGVyOiBub25lO1xcbn1cXG4uYm9yZGVyLXJhZGl1cyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuLnNvY2lhbC1uZXR3b3JrLWljb24ge1xcbiAgbWF4LXdpZHRoOiAyMHB4OyB9XFxuLnNpdGUtZm9vdGVyIHtcXG4gIHBhZGRpbmc6IDElIDQlO1xcbiAgaGVpZ2h0OiA4dmg7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5zb2NpYWwtaWNvbnMtbGlzdCA+IGxpIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5zaXRlLWZvb3RlciB7XFxuICAgIGhlaWdodDogMTN2aDsgfSB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDMyMHB4KSB7XFxuICAuc2l0ZS1mb290ZXIgPiAucm93ID4gLmNvbC14cy0xMjpmaXJzdC1jaGlsZCB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7SUFDSSxxQkFBcUI7Q0FDeEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGNBQWM7Q0FDakI7QUFDRDtJQUNJLHlCQUF5QjtDQUM1QjtBQUNEO0lBQ0ksd0JBQXdCO0NBQzNCO0FBQ0Q7SUFDSSwwQkFBMEI7Q0FDN0I7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDBCQUEwQjtRQUN0Qix1QkFBdUI7WUFDbkIsb0JBQW9CO0NBQy9CO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQyw2QkFBNkI7SUFDN0IsOEJBQThCO1FBQzFCLDJCQUEyQjtZQUN2Qix1QkFBdUI7Q0FDbEM7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksZ0RBQWdEO0lBQ2hELHdEQUF3RDtJQUN4RCxnREFBZ0Q7SUFDaEQsMkNBQTJDO0lBQzNDLHdDQUF3QztJQUN4Qyw2RUFBNkU7Q0FDaEY7QUFDRDtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsZUFBZTtFQUNmLFlBQVk7RUFDWixlQUFlLEVBQUU7QUFDbkI7RUFDRSxzQkFBc0I7RUFDdEIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRTtJQUNFLGFBQWEsRUFBRSxFQUFFO0FBQ3JCO0VBQ0U7SUFDRSxlQUFlLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zb2NpYWwtbmV0d29yay1pY29uIHtcXG4gIG1heC13aWR0aDogMjBweDsgfVxcbi5zaXRlLWZvb3RlciB7XFxuICBwYWRkaW5nOiAxJSA0JTtcXG4gIGhlaWdodDogOHZoO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc29jaWFsLWljb25zLWxpc3QgPiBsaSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuc2l0ZS1mb290ZXIge1xcbiAgICBoZWlnaHQ6IDEzdmg7IH0gfVxcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLnNpdGUtZm9vdGVyID4gLnJvdyA+IC5jb2wteHMtMTI6Zmlyc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJ2YXIgZXNjYXBlID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi91cmwvZXNjYXBlLmpzXCIpO1xuZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcImJvZHkge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgb3V0bGluZTogbm9uZTtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4uc2VhcmNoLXRoZW1le1xcbiAgYmFja2dyb3VuZDogdXJsKFwiICsgZXNjYXBlKHJlcXVpcmUoXCIuL2dlb21ldHJ5Mi5wbmdcIikpICsgXCIpO1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDtcXG59XFxuLm1hci01e1xcbiAgbWFyZ2luOiA1JTtcXG59XFxuLnBhZGQtNXtcXG4gIHBhZGRpbmc6IDUlO1xcbn1cXG4ubWFyLWxlZnR7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxufVxcbi5tYXItdG9we1xcbiAgbWFyZ2luLXRvcDogM3ZoO1xcbn1cXG4ubWFyLWJvdHRvbXtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDtcXG59XFxuLmJsdWUtYnV0dG9uIHtcXG4gIHdpZHRoOiAxM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuICBtYXJnaW46IGF1dG87XFxuICBiYWNrZ3JvdW5kOiByZ2IoNjgsMjA5LDIwMik7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7XFxufVxcbi5pY29ucy1jbHtcXG4gIHdpZHRoOiAzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG5cXG59XFxuLm1hci10b3AtMTB7XFxuICBtYXJnaW4tdG9wOiAyJTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXJ7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06aG92ZXJ7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpmb2N1c3tcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpmb2N1c3tcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxufVxcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoO1xcbn1cXG4uZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOjc2OHB4KXtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7XFxuICB9XFxuICAubWFyLWxlZnR7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiByZ2IoNjgsMjA5LDIwMik7XFxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICAgIHBhZGRpbmc6IDMlO1xcbiAgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnR7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gICAgbWluLXdpZHRoOiAyMDBweDtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgICB6LWluZGV4OiAxO1xcbiAgICBsZWZ0OiAxMnZ3O1xcbiAgICB0b3A6IDEwdmg7XFxuICB9XFxuXFxufVxcbmJ1dHRvbjpmb2N1cyB7XFxuICBvdXRsaW5lOiAwOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl0ge1xcbiAgb3V0bGluZTogbm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lOyB9XFxuZm9vdGVyIHtcXG4gIGhlaWdodDogMTN2aDtcXG4gIGJhY2tncm91bmQ6ICMxYzU5NTY7IH1cXG4ubmF2LWxvZ28ge1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLnllbGxvdy1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNkZWRjMmY7IH1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICB3aWR0aDogODAlO1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cXG4ucGFkZCB7XFxuICBwYWRkaW5nOiAyJSAhaW1wb3J0YW50OyB9XFxuLm5hdi1jbCB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgaGVpZ2h0OiAxMHZoO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMzBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMzBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICBvdmVyZmxvdzogdmlzaWJsZTtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgei1pbmRleDogOTk5OTsgfVxcbi5mbGV4LWNlbnRlciB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgfVxcbi5mbGV4LW1pZGRsZSB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyB9XFxuLnB1cnBsZS1mb250IHtcXG4gIGNvbG9yOiAjNWI1NTliOyB9XFxuLmJsdWUtZm9udCB7XFxuICBjb2xvcjogIzQ0ZDFjYTsgfVxcbi5ncmV5LWZvbnQge1xcbiAgY29sb3I6ICM5Njk2OTY7IH1cXG4ubGlnaHQtZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTQ5NDk0OyB9XFxuLmJsYWNrLWZvbnQge1xcbiAgY29sb3I6IGJsYWNrOyB9XFxuLndoaXRlLWZvbnQge1xcbiAgY29sb3I6IHdoaXRlOyB9XFxuLnBpbmstZm9udCB7XFxuICBjb2xvcjogI2YwNWQ2YzsgfVxcbi5waW5rLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogI2YwNWQ2YzsgfVxcbi5wdXJwbGUtYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kOiAjNWE1NTliOyB9XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5tYXJnaW4tYXV0byB7XFxuICBtYXJnaW46IGF1dG87IH1cXG4ubm8tcGFkZGluZyB7XFxuICBwYWRkaW5nOiAwOyB9XFxuLmljb24tY2wge1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7IH1cXG4uZmxleC1yb3ctcmV2IHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTsgfVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lOyB9XFxuLnNlYXJjaC10aGVtZSB7XFxuICAvKmJhY2tncm91bmQ6IHVybChcXFwiLi4vYXNzZXRzL2ltYWdlcy9wYXR0ZXJuL2dlb21ldHJ5Mi5wbmdcXFwiKTsqL1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi8qI2ltZy0xeyovXFxuLypiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vYXNzZXRzL2ltYWdlcy92aWxsYTEuanBnKTsqL1xcbi8qfSovXFxuLyojaW1nLTJ7Ki9cXG4vKmJhY2tncm91bmQtaW1hZ2U6IHVybCguLi9hc3NldHMvaW1hZ2VzL3ZpbGxhMi5qcGcpOyovXFxuLyp9Ki9cXG4vKiNpbWctMywgI2ltZy00LCAjaW1nLTV7Ki9cXG4vKmJhY2tncm91bmQtaW1hZ2U6IHVybCguLi9hc3NldHMvaW1hZ2VzL3VuZGVyd2F0ZXIuanBnKTsqL1xcbi8qfSovXFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWFyZ2luLXRvcDogNSU7XFxuICBtYXJnaW4tYm90dG9tOiA1JTsgfVxcbi5waG9uZS1kaXNwbGF5IHtcXG4gIG1hcmdpbjogMCU7XFxuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xcbiAgcGFkZGluZzogMiU7XFxuICBmb250LWZhbWlseTogc2hhYm5hbTsgfVxcbi5ob3VzZS1pbWctbWFyIHtcXG4gIG1hcmdpbjogNSUgMCU7IH1cXG4uaW5mby1sYWJlbCB7XFxuICBwYWRkaW5nOiAyJTtcXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcXG4gIG1hcmdpbjogNSUgMDsgfVxcbi5ob3VzZS1pbmZvIHtcXG4gIG1hcmdpbjogNSUgMDtcXG4gIG1hcmdpbi1yaWdodDogMiU7IH1cXG4uaW5mby1saW5lcyB7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkO1xcbiAgYm9yZGVyLWJvdHRvbS1jb2xvcjogbGlnaHRncmV5O1xcbiAgcGFkZGluZzogMyU7XFxuICBtYXJnaW4tbGVmdDogMyU7XFxuICBtYXJnaW4tdG9wOiA1JTsgfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlUGFnZS9tYWluLmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLHFCQUFxQjtDQUN0QjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLGNBQWM7Q0FDZjtBQUNEO0VBQ0UseUJBQXlCO0NBQzFCO0FBQ0Q7RUFDRSx3QkFBd0I7Q0FDekI7QUFDRDtFQUNFLDBCQUEwQjtDQUMzQjtBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCO0FBQ0Q7RUFDRSxpQkFBaUI7Q0FDbEI7QUFDRDtFQUNFLGlCQUFpQjtDQUNsQjtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QseUJBQXlCO01BQ3JCLHNCQUFzQjtVQUNsQix3QkFBd0I7RUFDaEMsMEJBQTBCO01BQ3RCLHVCQUF1QjtVQUNuQixvQkFBb0I7Q0FDN0I7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHlCQUF5QjtNQUNyQixzQkFBc0I7VUFDbEIsd0JBQXdCO0VBQ2hDLDZCQUE2QjtFQUM3Qiw4QkFBOEI7TUFDMUIsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtDQUNoQztBQUNEO0VBQ0Usa0JBQWtCO0NBQ25CO0FBQ0Q7RUFDRSxpQkFBaUI7Q0FDbEI7QUFDRDtFQUNFLG1CQUFtQjtDQUNwQjtBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCO0FBQ0Q7RUFDRSxjQUFjO0VBQ2QsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxtQkFBbUI7Q0FDcEI7QUFDRDtFQUNFLGdEQUFnRDtFQUNoRCx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0QsY0FBYyxVQUFVLENBQUM7QUFDekI7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLGFBQWE7RUFDYiwrQ0FBK0M7VUFDdkMsdUNBQXVDO0VBQy9DLHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkU7Q0FDOUU7QUFDRDtFQUNFLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7QUFDRDtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsYUFBYTtFQUNiLCtDQUErQztVQUN2Qyx1Q0FBdUM7RUFDL0Msd0RBQXdEO0VBQ3hELGdEQUFnRDtFQUNoRCwyQ0FBMkM7RUFDM0Msd0NBQXdDO0VBQ3hDLDZFQUE2RTtDQUM5RTtBQUNEO0VBQ0UsV0FBVztFQUNYLGFBQWE7Q0FDZDtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMEJBQTBCO01BQ3RCLHVCQUF1QjtVQUNuQixvQkFBb0I7Q0FDN0I7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHlCQUF5QjtNQUNyQixzQkFBc0I7VUFDbEIsd0JBQXdCO0NBQ2pDO0FBQ0Q7RUFDRSxzQkFBc0I7Q0FDdkI7QUFDRDtFQUNFLHVCQUF1QjtDQUN4QjtBQUNEO0VBQ0UsdUJBQXVCOztDQUV4QjtBQUNEO0VBQ0UseUJBQXlCO0NBQzFCO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsdUJBQXVCO0NBQ3hCO0FBQ0Q7RUFDRSw0QkFBNEI7Q0FDN0I7QUFDRDtFQUNFLDJCQUEyQjtDQUM1QjtBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtFQUN0QiwrQkFBK0I7RUFDL0IsK0JBQStCO01BQzNCLGdDQUFnQztVQUM1Qiw0QkFBNEI7Q0FDckM7QUFDRDtFQUNFLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsMENBQW1DO0VBQ25DLGFBQWE7RUFDYixpQkFBaUI7Q0FDbEI7QUFDRDtFQUNFLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsWUFBWTtDQUNiO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxnQkFBZ0I7Q0FDakI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYiw0QkFBNEI7RUFDNUIsb0JBQW9CO0VBQ3BCLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsV0FBVztFQUNYLFlBQVk7O0NBRWI7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRCxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUI7Q0FDcEI7QUFDRDtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsZUFBZTtDQUNoQjtBQUNEO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVk7Q0FDYjtBQUNELDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjtFQUNEO0lBQ0UsVUFBVTtHQUNYOztFQUVEO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLG9CQUFvQjtJQUNwQixZQUFZO0dBQ2I7RUFDRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixxREFBcUQ7WUFDN0MsNkNBQTZDO0lBQ3JELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7R0FDWDs7Q0FFRjtBQUNEO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7RUFDYixrREFBa0Q7VUFDMUMsMENBQTBDO0VBQ2xELHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkUsRUFBRTtBQUNqRjtFQUNFLFdBQVc7RUFDWCxhQUFhLEVBQUU7QUFDakI7RUFDRSxhQUFhO0VBQ2Isb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxhQUFhLEVBQUU7QUFDakI7RUFDRSxvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsa0RBQWtEO1VBQzFDLDBDQUEwQyxFQUFFO0FBQ3REO0VBQ0UsdUJBQXVCLEVBQUU7QUFDM0I7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwrQkFBK0I7RUFDL0IsOEJBQThCO01BQzFCLHdCQUF3QjtVQUNwQixvQkFBb0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYix3QkFBd0I7RUFDeEIsb0RBQW9EO1VBQzVDLDRDQUE0QztFQUNwRCxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGNBQWMsRUFBRTtBQUNsQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CLEVBQUU7QUFDaEM7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QixFQUFFO0FBQ3BDO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLG1CQUFtQjtFQUNuQixlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLCtCQUErQjtFQUMvQiwrQkFBK0I7TUFDM0IsZ0NBQWdDO1VBQzVCLDRCQUE0QixFQUFFO0FBQ3hDO0VBQ0Usc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSw4REFBOEQ7RUFDOUQsYUFBYTtFQUNiLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxZQUFZLEVBQUU7QUFDaEI7RUFDRSxvREFBb0Q7VUFDNUMsNENBQTRDO0VBQ3BELGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLGFBQWEsRUFBRTtBQUNqQjtFQUNFLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0Usc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixxQ0FBcUM7RUFDckMsNEJBQTRCLEVBQUU7QUFDaEM7RUFDRSxjQUFjLEVBQUU7QUFDbEI7RUFDRSxtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZ0NBQWdDLEVBQUU7QUFDcEM7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLFlBQVksRUFBRTtBQUNoQjtFQUNFLFdBQVc7RUFDWCxZQUFZLEVBQUU7QUFDaEI7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLGlDQUFpQyxFQUFFO0FBQ3JDO0VBQ0UsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUU7QUFDdEIsV0FBVztBQUNYLHVEQUF1RDtBQUN2RCxLQUFLO0FBQ0wsV0FBVztBQUNYLHVEQUF1RDtBQUN2RCxLQUFLO0FBQ0wsMkJBQTJCO0FBQzNCLDJEQUEyRDtBQUMzRCxLQUFLO0FBQ0wsc0JBQXNCO0FBQ3RCO0VBQ0UsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIsd0RBQXdEO1VBQ2hELGdEQUFnRDtFQUN4RCxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFVBQVU7RUFDVixXQUFXLEVBQUU7QUFDZjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLHNCQUFzQjtFQUN0QixZQUFZLEVBQUU7QUFDaEIsMENBQTBDO0FBQzFDO0VBQ0U7SUFDRSxtQkFBbUI7SUFDbkIsaUJBQWlCLEVBQUU7RUFDckI7SUFDRSxVQUFVLEVBQUU7RUFDZDtJQUNFLFlBQVk7SUFDWixZQUFZLEVBQUU7RUFDaEI7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCLEVBQUU7RUFDcEI7SUFDRSxlQUFlLEVBQUU7RUFDbkI7SUFDRSxrQkFBa0IsRUFBRTtFQUN0QjtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1osYUFBYTtJQUNiLG9CQUFvQjtJQUNwQixvQkFBb0I7SUFDcEIsWUFBWSxFQUFFO0VBQ2hCO0lBQ0UsY0FBYztJQUNkLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsaUJBQWlCO0lBQ2pCLHdEQUF3RDtZQUNoRCxnREFBZ0Q7SUFDeEQsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxXQUFXO0lBQ1gsVUFBVSxFQUFFO0VBQ2Q7SUFDRSxvREFBb0Q7WUFDNUMsNENBQTRDO0lBQ3BELGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGFBQWEsRUFBRSxFQUFFO0FBQ3JCO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixhQUFhO0VBQ2IsZUFBZTtFQUNmLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1oscUJBQXFCLEVBQUU7QUFDekI7RUFDRSxjQUFjLEVBQUU7QUFDbEI7RUFDRSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGFBQWE7RUFDYixpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLDBCQUEwQjtFQUMxQiwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixlQUFlLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgb3V0bGluZTogbm9uZTtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4uc2VhcmNoLXRoZW1le1xcbiAgYmFja2dyb3VuZDogdXJsKFxcXCIuL2dlb21ldHJ5Mi5wbmdcXFwiKTtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi55ZWxsb3ctYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kOiAjZGVkYzJmOyB9XFxuLnNlYXJjaC1idXR0b24ge1xcbiAgd2lkdGg6IDgwJTtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpOyB9XFxuLnBhZGQge1xcbiAgcGFkZGluZzogMiUgIWltcG9ydGFudDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgLypiYWNrZ3JvdW5kOiB1cmwoXFxcIi4uL2Fzc2V0cy9pbWFnZXMvcGF0dGVybi9nZW9tZXRyeTIucG5nXFxcIik7Ki9cXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLWhlaWdodCB7XFxuICBoZWlnaHQ6IDIwMHB4OyB9XFxuLmJvcmRlci1ib3R0b20ge1xcbiAgbWFyZ2luLWJvdHRvbTogMnZoO1xcbiAgbWFyZ2luLWxlZnQ6IDF2dztcXG4gIG1hcmdpbi1yaWdodDogMXZ3O1xcbiAgbWFyZ2luLXRvcDogMnZoO1xcbiAgYm9yZGVyLWJvdHRvbTogdGhpbiBzb2xpZCBibGFjazsgfVxcbi5tYXItbGVmdCB7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7IH1cXG4ubWFyLXRvcCB7XFxuICBtYXJnaW4tdG9wOiAzdmg7IH1cXG4ubWFyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7IH1cXG4uaWNvbnMtY2wge1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXIge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3VzIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4ubWFyLXRvcC0xMCB7XFxuICBtYXJnaW4tdG9wOiAyJTsgfVxcbi5wYWRkaW5nLXJpZ2h0LXByaWNlIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDclOyB9XFxuLm1hcmdpbi0xIHtcXG4gIG1hcmdpbi1sZWZ0OiAzJTtcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG4uaW1nLWJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7IH1cXG4ubWFpbi1mb3JtLXNlYXJjaC1yZXN1bHQge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgbWFyZ2luLXRvcDogLTclO1xcbiAgbWFyZ2luLWJvdHRvbTogNiU7IH1cXG4vKiNpbWctMXsqL1xcbi8qYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2Fzc2V0cy9pbWFnZXMvdmlsbGExLmpwZyk7Ki9cXG4vKn0qL1xcbi8qI2ltZy0yeyovXFxuLypiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vYXNzZXRzL2ltYWdlcy92aWxsYTIuanBnKTsqL1xcbi8qfSovXFxuLyojaW1nLTMsICNpbWctNCwgI2ltZy01eyovXFxuLypiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vYXNzZXRzL2ltYWdlcy91bmRlcndhdGVyLmpwZyk7Ki9cXG4vKn0qL1xcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDsgfVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH1cXG4gIC5tYXItbGVmdCB7XFxuICAgIG1hcmdpbjogMDsgfVxcbiAgLmljb25zLWNsIHtcXG4gICAgd2lkdGg6IDEzdnc7XFxuICAgIGhlaWdodDogN3ZoOyB9XFxuICAuZm9vdGVyLXRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTJweDsgfVxcbiAgLmljb25zLW1hciB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlOyB9XFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7IH1cXG4gIC5kcm9wZG93bi1jb250ZW50IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgaGVpZ2h0OiAzNTBweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIGZsb2F0OiByaWdodDsgfSB9XFxuLmhvdXNlLWltZyB7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1hcmdpbi10b3A6IDUlO1xcbiAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4ucGhvbmUtZGlzcGxheSB7XFxuICBtYXJnaW46IDAlO1xcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcXG4gIHBhZGRpbmc6IDIlO1xcbiAgZm9udC1mYW1pbHk6IHNoYWJuYW07IH1cXG4uaG91c2UtaW1nLW1hciB7XFxuICBtYXJnaW46IDUlIDAlOyB9XFxuLmluZm8tbGFiZWwge1xcbiAgcGFkZGluZzogMiU7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICBtYXJnaW46IDUlIDA7IH1cXG4uaG91c2UtaW5mbyB7XFxuICBtYXJnaW46IDUlIDA7XFxuICBtYXJnaW4tcmlnaHQ6IDIlOyB9XFxuLmluZm8tbGluZXMge1xcbiAgYm9yZGVyLWJvdHRvbTogdGhpbiBzb2xpZDtcXG4gIGJvcmRlci1ib3R0b20tY29sb3I6IGxpZ2h0Z3JleTtcXG4gIHBhZGRpbmc6IDMlO1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXRvcDogNSU7IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvSG91c2VQYWdlL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Ib3VzZVBhZ2UvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIkBjaGFyc2V0IFxcXCJVVEYtOFxcXCI7XFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbjpyb290IHtcXG4gIC8qXFxuICAgKiBUeXBvZ3JhcGh5XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tZm9udC1mYW1pbHktYmFzZTogJ1NlZ29lIFVJJywgJ0hlbHZldGljYU5ldWUtTGlnaHQnLCBzYW5zLXNlcmlmO1xcblxcbiAgLypcXG4gICAqIExheW91dFxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLW1heC1jb250ZW50LXdpZHRoOiAxMDAwcHg7XFxuXFxuICAvKlxcbiAgICogTWVkaWEgcXVlcmllcyBicmVha3BvaW50c1xcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLXNjcmVlbi14cy1taW46IDQ4MHB4OyAgLyogRXh0cmEgc21hbGwgc2NyZWVuIC8gcGhvbmUgKi9cXG4gIC0tc2NyZWVuLXNtLW1pbjogNzY4cHg7ICAvKiBTbWFsbCBzY3JlZW4gLyB0YWJsZXQgKi9cXG4gIC0tc2NyZWVuLW1kLW1pbjogOTkycHg7ICAvKiBNZWRpdW0gc2NyZWVuIC8gZGVza3RvcCAqL1xcbiAgLS1zY3JlZW4tbGctbWluOiAxMjAwcHg7IC8qIExhcmdlIHNjcmVlbiAvIHdpZGUgZGVza3RvcCAqL1xcbn1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogMTAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDcwMDsgfVxcbi8qXFxuICogbm9ybWFsaXplLmNzcyBpcyBpbXBvcnRlZCBpbiBKUyBmaWxlLlxcbiAqIElmIHlvdSBpbXBvcnQgZXh0ZXJuYWwgQ1NTIGZpbGUgZnJvbSB5b3VyIGludGVybmFsIENTU1xcbiAqIHRoZW4gaXQgd2lsbCBiZSBpbmxpbmVkIGFuZCBwcm9jZXNzZWQgd2l0aCBDU1MgbW9kdWxlcy5cXG4gKi9cXG4vKlxcbiAqIEJhc2Ugc3R5bGVzXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5odG1sIHtcXG4gIGNvbG9yOiAjMjIyO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDtcXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogfjE2cHg7ICovXFxuICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktYmFzZSk7XFxuICBsaW5lLWhlaWdodDogMS4zNzU7XFxuICAvKiB+MjJweCAqLyB9XFxuYm9keSB7XFxuICBtYXJnaW46IDA7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTsgfVxcbmEge1xcbiAgY29sb3I6ICMwMDc0YzI7IH1cXG4vKlxcbiAqIFJlbW92ZSB0ZXh0LXNoYWRvdyBpbiBzZWxlY3Rpb24gaGlnaGxpZ2h0OlxcbiAqIGh0dHBzOi8vdHdpdHRlci5jb20vbWlrZXRheWxyL3N0YXR1cy8xMjIyODgwNTMwMVxcbiAqXFxuICogVGhlc2Ugc2VsZWN0aW9uIHJ1bGUgc2V0cyBoYXZlIHRvIGJlIHNlcGFyYXRlLlxcbiAqIEN1c3RvbWl6ZSB0aGUgYmFja2dyb3VuZCBjb2xvciB0byBtYXRjaCB5b3VyIGRlc2lnbi5cXG4gKi9cXG46Oi1tb3otc2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbjo6c2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbi8qXFxuICogQSBiZXR0ZXIgbG9va2luZyBkZWZhdWx0IGhvcml6b250YWwgcnVsZVxcbiAqL1xcbmhyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBib3JkZXI6IDA7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcXG4gIG1hcmdpbjogMWVtIDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBSZW1vdmUgdGhlIGdhcCBiZXR3ZWVuIGF1ZGlvLCBjYW52YXMsIGlmcmFtZXMsXFxuICogaW1hZ2VzLCB2aWRlb3MgYW5kIHRoZSBib3R0b20gb2YgdGhlaXIgY29udGFpbmVyczpcXG4gKiBodHRwczovL2dpdGh1Yi5jb20vaDVicC9odG1sNS1ib2lsZXJwbGF0ZS9pc3N1ZXMvNDQwXFxuICovXFxuYXVkaW8sXFxuY2FudmFzLFxcbmlmcmFtZSxcXG5pbWcsXFxuc3ZnLFxcbnZpZGVvIHtcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7IH1cXG4vKlxcbiAqIFJlbW92ZSBkZWZhdWx0IGZpZWxkc2V0IHN0eWxlcy5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBBbGxvdyBvbmx5IHZlcnRpY2FsIHJlc2l6aW5nIG9mIHRleHRhcmVhcy5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICByZXNpemU6IHZlcnRpY2FsOyB9XFxuLypcXG4gKiBCcm93c2VyIHVwZ3JhZGUgcHJvbXB0XFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4uYnJvd3NlcnVwZ3JhZGUge1xcbiAgbWFyZ2luOiAwLjJlbSAwO1xcbiAgYmFja2dyb3VuZDogI2NjYztcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMC4yZW0gMDsgfVxcbi8qXFxuICogUHJpbnQgc3R5bGVzXFxuICogSW5saW5lZCB0byBhdm9pZCB0aGUgYWRkaXRpb25hbCBIVFRQIHJlcXVlc3Q6XFxuICogaHR0cDovL3d3dy5waHBpZWQuY29tL2RlbGF5LWxvYWRpbmcteW91ci1wcmludC1jc3MvXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5AbWVkaWEgcHJpbnQge1xcbiAgKixcXG4gICo6OmJlZm9yZSxcXG4gICo6OmFmdGVyIHtcXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcXG4gICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcXG4gICAgLyogQmxhY2sgcHJpbnRzIGZhc3RlcjogaHR0cDovL3d3dy5zYW5iZWlqaS5jb20vYXJjaGl2ZXMvOTUzICovXFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgdGV4dC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDsgfVxcbiAgYSxcXG4gIGE6dmlzaXRlZCB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyB9XFxuICBhW2hyZWZdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cihocmVmKSBcXFwiKVxcXCI7IH1cXG4gIGFiYnJbdGl0bGVdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cih0aXRsZSkgXFxcIilcXFwiOyB9XFxuICAvKlxcbiAgICogRG9uJ3Qgc2hvdyBsaW5rcyB0aGF0IGFyZSBmcmFnbWVudCBpZGVudGlmaWVycyxcXG4gICAqIG9yIHVzZSB0aGUgYGphdmFzY3JpcHQ6YCBwc2V1ZG8gcHJvdG9jb2xcXG4gICAqL1xcbiAgYVtocmVmXj0nIyddOjphZnRlcixcXG4gIGFbaHJlZl49J2phdmFzY3JpcHQ6J106OmFmdGVyIHtcXG4gICAgY29udGVudDogJyc7IH1cXG4gIHByZSxcXG4gIGJsb2NrcXVvdGUge1xcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjOTk5O1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIC8qXFxuICAgKiBQcmludGluZyBUYWJsZXM6XFxuICAgKiBodHRwOi8vY3NzLWRpc2N1c3MuaW5jdXRpby5jb20vd2lraS9QcmludGluZ19UYWJsZXNcXG4gICAqL1xcbiAgdGhlYWQge1xcbiAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7IH1cXG4gIHRyLFxcbiAgaW1nIHtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICBpbWcge1xcbiAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDsgfVxcbiAgcCxcXG4gIGgyLFxcbiAgaDMge1xcbiAgICBvcnBoYW5zOiAzO1xcbiAgICB3aWRvd3M6IDM7IH1cXG4gIGgyLFxcbiAgaDMge1xcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhdm9pZDsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsaUJBQWlCO0FBQ2pCOzs7Ozs7O0dBT0c7QUFDSDs7Ozs7OztHQU9HO0FBQ0g7RUFDRTs7Z0ZBRThFOztFQUU5RSxrRUFBa0U7O0VBRWxFOztnRkFFOEU7O0VBRTlFLDRCQUE0Qjs7RUFFNUI7O2dGQUU4RTs7RUFFOUUsdUJBQXVCLEVBQUUsZ0NBQWdDO0VBQ3pELHVCQUF1QixFQUFFLDJCQUEyQjtFQUNwRCx1QkFBdUIsRUFBRSw2QkFBNkI7RUFDdEQsd0JBQXdCLENBQUMsaUNBQWlDO0NBQzNEO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIsbUNBQW1DO0VBQ25DLHVLQUF1SztFQUN2SyxpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLHFCQUFxQjtFQUNyQiw2QkFBNkI7RUFDN0IscUpBQXFKO0VBQ3JKLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UscUJBQXFCO0VBQ3JCLGtDQUFrQztFQUNsQyxvS0FBb0s7RUFDcEssaUJBQWlCLEVBQUU7QUFDckI7Ozs7R0FJRztBQUNIOztnRkFFZ0Y7QUFDaEY7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixZQUFZO0VBQ1oscUNBQXFDO0VBQ3JDLG1CQUFtQjtFQUNuQixXQUFXLEVBQUU7QUFDZjtFQUNFLFVBQVU7RUFDVixxQkFBcUIsRUFBRTtBQUN6QjtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7Ozs7O0dBTUc7QUFDSDtFQUNFLG9CQUFvQjtFQUNwQixrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLG9CQUFvQjtFQUNwQixrQkFBa0IsRUFBRTtBQUN0Qjs7R0FFRztBQUNIO0VBQ0UsZUFBZTtFQUNmLFlBQVk7RUFDWixVQUFVO0VBQ1YsMkJBQTJCO0VBQzNCLGNBQWM7RUFDZCxXQUFXLEVBQUU7QUFDZjs7OztHQUlHO0FBQ0g7Ozs7OztFQU1FLHVCQUF1QixFQUFFO0FBQzNCOztHQUVHO0FBQ0g7RUFDRSxVQUFVO0VBQ1YsVUFBVTtFQUNWLFdBQVcsRUFBRTtBQUNmOztHQUVHO0FBQ0g7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjs7Z0ZBRWdGO0FBQ2hGO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCLEVBQUU7QUFDckI7Ozs7Z0ZBSWdGO0FBQ2hGO0VBQ0U7OztJQUdFLG1DQUFtQztJQUNuQyx1QkFBdUI7SUFDdkIsK0RBQStEO0lBQy9ELG9DQUFvQztZQUM1Qiw0QkFBNEI7SUFDcEMsNkJBQTZCLEVBQUU7RUFDakM7O0lBRUUsMkJBQTJCLEVBQUU7RUFDL0I7SUFDRSw2QkFBNkIsRUFBRTtFQUNqQztJQUNFLDhCQUE4QixFQUFFO0VBQ2xDOzs7S0FHRztFQUNIOztJQUVFLFlBQVksRUFBRTtFQUNoQjs7SUFFRSx1QkFBdUI7SUFDdkIseUJBQXlCLEVBQUU7RUFDN0I7OztLQUdHO0VBQ0g7SUFDRSw0QkFBNEIsRUFBRTtFQUNoQzs7SUFFRSx5QkFBeUIsRUFBRTtFQUM3QjtJQUNFLDJCQUEyQixFQUFFO0VBQy9COzs7SUFHRSxXQUFXO0lBQ1gsVUFBVSxFQUFFO0VBQ2Q7O0lBRUUsd0JBQXdCLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwiTGF5b3V0LmNzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCJAY2hhcnNldCBcXFwiVVRGLThcXFwiO1xcbi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG46cm9vdCB7XFxuICAvKlxcbiAgICogVHlwb2dyYXBoeVxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLWZvbnQtZmFtaWx5LWJhc2U6ICdTZWdvZSBVSScsICdIZWx2ZXRpY2FOZXVlLUxpZ2h0Jywgc2Fucy1zZXJpZjtcXG5cXG4gIC8qXFxuICAgKiBMYXlvdXRcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1tYXgtY29udGVudC13aWR0aDogMTAwMHB4O1xcblxcbiAgLypcXG4gICAqIE1lZGlhIHF1ZXJpZXMgYnJlYWtwb2ludHNcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1zY3JlZW4teHMtbWluOiA0ODBweDsgIC8qIEV4dHJhIHNtYWxsIHNjcmVlbiAvIHBob25lICovXFxuICAtLXNjcmVlbi1zbS1taW46IDc2OHB4OyAgLyogU21hbGwgc2NyZWVuIC8gdGFibGV0ICovXFxuICAtLXNjcmVlbi1tZC1taW46IDk5MnB4OyAgLyogTWVkaXVtIHNjcmVlbiAvIGRlc2t0b3AgKi9cXG4gIC0tc2NyZWVuLWxnLW1pbjogMTIwMHB4OyAvKiBMYXJnZSBzY3JlZW4gLyB3aWRlIGRlc2t0b3AgKi9cXG59XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0ud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7IH1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA3MDA7IH1cXG4vKlxcbiAqIG5vcm1hbGl6ZS5jc3MgaXMgaW1wb3J0ZWQgaW4gSlMgZmlsZS5cXG4gKiBJZiB5b3UgaW1wb3J0IGV4dGVybmFsIENTUyBmaWxlIGZyb20geW91ciBpbnRlcm5hbCBDU1NcXG4gKiB0aGVuIGl0IHdpbGwgYmUgaW5saW5lZCBhbmQgcHJvY2Vzc2VkIHdpdGggQ1NTIG1vZHVsZXMuXFxuICovXFxuLypcXG4gKiBCYXNlIHN0eWxlc1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuaHRtbCB7XFxuICBjb2xvcjogIzIyMjtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIH4xNnB4OyAqL1xcbiAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJhc2UpO1xcbiAgbGluZS1oZWlnaHQ6IDEuMzc1O1xcbiAgLyogfjIycHggKi8gfVxcbmJvZHkge1xcbiAgbWFyZ2luOiAwO1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07IH1cXG5hIHtcXG4gIGNvbG9yOiAjMDA3NGMyOyB9XFxuLypcXG4gKiBSZW1vdmUgdGV4dC1zaGFkb3cgaW4gc2VsZWN0aW9uIGhpZ2hsaWdodDpcXG4gKiBodHRwczovL3R3aXR0ZXIuY29tL21pa2V0YXlsci9zdGF0dXMvMTIyMjg4MDUzMDFcXG4gKlxcbiAqIFRoZXNlIHNlbGVjdGlvbiBydWxlIHNldHMgaGF2ZSB0byBiZSBzZXBhcmF0ZS5cXG4gKiBDdXN0b21pemUgdGhlIGJhY2tncm91bmQgY29sb3IgdG8gbWF0Y2ggeW91ciBkZXNpZ24uXFxuICovXFxuOjotbW96LXNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7IH1cXG46OnNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7IH1cXG4vKlxcbiAqIEEgYmV0dGVyIGxvb2tpbmcgZGVmYXVsdCBob3Jpem9udGFsIHJ1bGVcXG4gKi9cXG5ociB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGhlaWdodDogMXB4O1xcbiAgYm9yZGVyOiAwO1xcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XFxuICBtYXJnaW46IDFlbSAwO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qXFxuICogUmVtb3ZlIHRoZSBnYXAgYmV0d2VlbiBhdWRpbywgY2FudmFzLCBpZnJhbWVzLFxcbiAqIGltYWdlcywgdmlkZW9zIGFuZCB0aGUgYm90dG9tIG9mIHRoZWlyIGNvbnRhaW5lcnM6XFxuICogaHR0cHM6Ly9naXRodWIuY29tL2g1YnAvaHRtbDUtYm9pbGVycGxhdGUvaXNzdWVzLzQ0MFxcbiAqL1xcbmF1ZGlvLFxcbmNhbnZhcyxcXG5pZnJhbWUsXFxuaW1nLFxcbnN2ZyxcXG52aWRlbyB7XFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyB9XFxuLypcXG4gKiBSZW1vdmUgZGVmYXVsdCBmaWVsZHNldCBzdHlsZXMuXFxuICovXFxuZmllbGRzZXQge1xcbiAgYm9yZGVyOiAwO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qXFxuICogQWxsb3cgb25seSB2ZXJ0aWNhbCByZXNpemluZyBvZiB0ZXh0YXJlYXMuXFxuICovXFxudGV4dGFyZWEge1xcbiAgcmVzaXplOiB2ZXJ0aWNhbDsgfVxcbi8qXFxuICogQnJvd3NlciB1cGdyYWRlIHByb21wdFxcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuOmdsb2JhbCguYnJvd3NlcnVwZ3JhZGUpIHtcXG4gIG1hcmdpbjogMC4yZW0gMDtcXG4gIGJhY2tncm91bmQ6ICNjY2M7XFxuICBjb2xvcjogIzAwMDtcXG4gIHBhZGRpbmc6IDAuMmVtIDA7IH1cXG4vKlxcbiAqIFByaW50IHN0eWxlc1xcbiAqIElubGluZWQgdG8gYXZvaWQgdGhlIGFkZGl0aW9uYWwgSFRUUCByZXF1ZXN0OlxcbiAqIGh0dHA6Ly93d3cucGhwaWVkLmNvbS9kZWxheS1sb2FkaW5nLXlvdXItcHJpbnQtY3NzL1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuQG1lZGlhIHByaW50IHtcXG4gICosXFxuICAqOjpiZWZvcmUsXFxuICAqOjphZnRlciB7XFxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XFxuICAgIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XFxuICAgIC8qIEJsYWNrIHByaW50cyBmYXN0ZXI6IGh0dHA6Ly93d3cuc2FuYmVpamkuY29tL2FyY2hpdmVzLzk1MyAqL1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIHRleHQtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7IH1cXG4gIGEsXFxuICBhOnZpc2l0ZWQge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTsgfVxcbiAgYVtocmVmXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiBcXFwiIChcXFwiIGF0dHIoaHJlZikgXFxcIilcXFwiOyB9XFxuICBhYmJyW3RpdGxlXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiBcXFwiIChcXFwiIGF0dHIodGl0bGUpIFxcXCIpXFxcIjsgfVxcbiAgLypcXG4gICAqIERvbid0IHNob3cgbGlua3MgdGhhdCBhcmUgZnJhZ21lbnQgaWRlbnRpZmllcnMsXFxuICAgKiBvciB1c2UgdGhlIGBqYXZhc2NyaXB0OmAgcHNldWRvIHByb3RvY29sXFxuICAgKi9cXG4gIGFbaHJlZl49JyMnXTo6YWZ0ZXIsXFxuICBhW2hyZWZePSdqYXZhc2NyaXB0OiddOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6ICcnOyB9XFxuICBwcmUsXFxuICBibG9ja3F1b3RlIHtcXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICAvKlxcbiAgICogUHJpbnRpbmcgVGFibGVzOlxcbiAgICogaHR0cDovL2Nzcy1kaXNjdXNzLmluY3V0aW8uY29tL3dpa2kvUHJpbnRpbmdfVGFibGVzXFxuICAgKi9cXG4gIHRoZWFkIHtcXG4gICAgZGlzcGxheTogdGFibGUtaGVhZGVyLWdyb3VwOyB9XFxuICB0cixcXG4gIGltZyB7XFxuICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdm9pZDsgfVxcbiAgaW1nIHtcXG4gICAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7IH1cXG4gIHAsXFxuICBoMixcXG4gIGgzIHtcXG4gICAgb3JwaGFuczogMztcXG4gICAgd2lkb3dzOiAzOyB9XFxuICBoMixcXG4gIGgzIHtcXG4gICAgcGFnZS1icmVhay1hZnRlcjogYXZvaWQ7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIubG9hZGVyIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGZvbnQtZmFtaWx5OiBJUlNhbnMsIHNlcmlmO1xcbiAgei1pbmRleDogMTA7XFxuICBwYWRkaW5nOiAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHRvcDogY2FsYyg1MCUgLSA4NXB4KTtcXG4gIGxlZnQ6IGNhbGMoNTAlIC0gNzBweCk7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpOyB9XFxuICAubG9hZGVyIGltZyB7XFxuICAgIHdpZHRoOiAxMDBweDsgfVxcbiAgLmxvYWRlciAubG9hZGVyLWluZm8ge1xcbiAgICBkaXJlY3Rpb246IHJ0bDtcXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgfVxcbiAgLmxvYWRlci5sb2FkaW5nIHtcXG4gICAgZGlzcGxheTogYmxvY2s7IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsMkJBQTJCO0VBQzNCLFlBQVk7RUFDWixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHdCQUF3QjtFQUN4QixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qix3SEFBd0g7VUFDaEgsZ0hBQWdILEVBQUU7RUFDMUg7SUFDRSxhQUFhLEVBQUU7RUFDakI7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQixFQUFFO0VBQ3ZCO0lBQ0UsZUFBZSxFQUFFXCIsXCJmaWxlXCI6XCJMb2FkZXIuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIubG9hZGVyIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGZvbnQtZmFtaWx5OiBJUlNhbnMsIHNlcmlmO1xcbiAgei1pbmRleDogMTA7XFxuICBwYWRkaW5nOiAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHRvcDogY2FsYyg1MCUgLSA4NXB4KTtcXG4gIGxlZnQ6IGNhbGMoNTAlIC0gNzBweCk7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpOyB9XFxuICAubG9hZGVyIGltZyB7XFxuICAgIHdpZHRoOiAxMDBweDsgfVxcbiAgLmxvYWRlciAubG9hZGVyLWluZm8ge1xcbiAgICBkaXJlY3Rpb246IHJ0bDtcXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgfVxcbiAgLmxvYWRlci5sb2FkaW5nIHtcXG4gICAgZGlzcGxheTogYmxvY2s7IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDYiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtJQUNJLHFCQUFxQjtDQUN4QjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksY0FBYztDQUNqQjtBQUNEO0lBQ0kseUJBQXlCO0NBQzVCO0FBQ0Q7SUFDSSx3QkFBd0I7Q0FDM0I7QUFDRDtJQUNJLDBCQUEwQjtDQUM3QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsMEJBQTBCO1FBQ3RCLHVCQUF1QjtZQUNuQixvQkFBb0I7Q0FDL0I7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDZCQUE2QjtJQUM3Qiw4QkFBOEI7UUFDMUIsMkJBQTJCO1lBQ3ZCLHVCQUF1QjtDQUNsQztBQUNEO0lBQ0ksa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxnREFBZ0Q7SUFDaEQsd0RBQXdEO0lBQ3hELGdEQUFnRDtJQUNoRCwyQ0FBMkM7SUFDM0Msd0NBQXdDO0lBQ3hDLDZFQUE2RTtDQUNoRjtBQUNELGNBQWMsVUFBVSxDQUFDO0FBQ3pCO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLGFBQWE7RUFDYiwrQ0FBK0M7VUFDdkMsdUNBQXVDO0VBQy9DLHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkU7Q0FDOUU7QUFDRDtFQUNFLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0NBQzdCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QjtDQUNqQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLHVCQUF1Qjs7Q0FFeEI7QUFDRDtFQUNFLHlCQUF5QjtDQUMxQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHVCQUF1QjtDQUN4QjtBQUNEO0VBQ0UsNEJBQTRCO0NBQzdCO0FBQ0Q7RUFDRSwyQkFBMkI7Q0FDNUI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsK0JBQStCO0VBQy9CLCtCQUErQjtNQUMzQixnQ0FBZ0M7VUFDNUIsNEJBQTRCO0NBQ3JDO0FBQ0Q7RUFDRSxzQkFBc0I7Q0FDdkI7QUFDRDtFQUNFLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsWUFBWTtDQUNiO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxnQkFBZ0I7Q0FDakI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYiw0QkFBNEI7RUFDNUIsb0JBQW9CO0VBQ3BCLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsV0FBVztFQUNYLFlBQVk7O0NBRWI7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRCxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUI7Q0FDcEI7QUFDRDtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsZUFBZTtDQUNoQjtBQUNEO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVk7Q0FDYjtBQUNELDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjtFQUNEO0lBQ0UsVUFBVTtHQUNYOztFQUVEO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLG9CQUFvQjtJQUNwQixZQUFZO0dBQ2I7RUFDRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixxREFBcUQ7WUFDN0MsNkNBQTZDO0lBQ3JELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7R0FDWDs7Q0FFRjtBQUNEO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7RUFDYixrREFBa0Q7VUFDMUMsMENBQTBDO0VBQ2xELHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkUsRUFBRTtBQUNqRjtFQUNFLFdBQVc7RUFDWCxhQUFhLEVBQUU7QUFDakI7RUFDRSxhQUFhO0VBQ2Isb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxhQUFhLEVBQUU7QUFDakI7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwrQkFBK0I7RUFDL0IsOEJBQThCO01BQzFCLHdCQUF3QjtVQUNwQixvQkFBb0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYix3QkFBd0I7RUFDeEIsb0RBQW9EO1VBQzVDLDRDQUE0QztFQUNwRCxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGNBQWMsRUFBRTtBQUNsQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CLEVBQUU7QUFDaEM7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QixFQUFFO0FBQ3BDO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLG1CQUFtQjtFQUNuQixlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLCtCQUErQjtFQUMvQiwrQkFBK0I7TUFDM0IsZ0NBQWdDO1VBQzVCLDRCQUE0QixFQUFFO0FBQ3hDO0VBQ0Usc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxXQUFXLEVBQUU7QUFDZjtFQUNFLFlBQVksRUFBRTtBQUNoQjtFQUNFLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsYUFBYSxFQUFFO0FBQ2pCO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLHFDQUFxQztFQUNyQyw0QkFBNEIsRUFBRTtBQUNoQztFQUNFLGNBQWMsRUFBRTtBQUNsQjtFQUNFLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixnQ0FBZ0MsRUFBRTtBQUNwQztFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixzQkFBc0IsRUFBRTtBQUMxQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFvQjtFQUNwQixvQkFBb0I7RUFDcEIsWUFBWSxFQUFFO0FBQ2hCO0VBQ0UsV0FBVztFQUNYLFlBQVksRUFBRTtBQUNoQjtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkMsRUFBRTtBQUN2RDtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkMsRUFBRTtBQUN2RDtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsaUNBQWlDLEVBQUU7QUFDckM7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGdDQUFnQyxFQUFFO0FBQ3BDLHNCQUFzQjtBQUN0QjtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLHdEQUF3RDtVQUNoRCxnREFBZ0Q7RUFDeEQsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxVQUFVO0VBQ1YsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxzQkFBc0I7RUFDdEIsWUFBWSxFQUFFO0FBQ2hCLDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQixFQUFFO0VBQ3JCO0lBQ0UsVUFBVSxFQUFFO0VBQ2Q7SUFDRSxZQUFZO0lBQ1osWUFBWSxFQUFFO0VBQ2hCO0lBQ0UsbUJBQW1CO0lBQ25CLGdCQUFnQixFQUFFO0VBQ3BCO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0Usa0JBQWtCLEVBQUU7RUFDdEI7SUFDRSxZQUFZO0lBQ1osWUFBWTtJQUNaLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLFlBQVksRUFBRTtFQUNoQjtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQix3REFBd0Q7WUFDaEQsZ0RBQWdEO0lBQ3hELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVUsRUFBRTtFQUNkO0lBQ0Usb0RBQW9EO1lBQzVDLDRDQUE0QztJQUNwRCxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixhQUFhLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGVzY2FwZSh1cmwpIHtcbiAgICBpZiAodHlwZW9mIHVybCAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgcmV0dXJuIHVybFxuICAgIH1cbiAgICAvLyBJZiB1cmwgaXMgYWxyZWFkeSB3cmFwcGVkIGluIHF1b3RlcywgcmVtb3ZlIHRoZW1cbiAgICBpZiAoL15bJ1wiXS4qWydcIl0kLy50ZXN0KHVybCkpIHtcbiAgICAgICAgdXJsID0gdXJsLnNsaWNlKDEsIC0xKTtcbiAgICB9XG4gICAgLy8gU2hvdWxkIHVybCBiZSB3cmFwcGVkP1xuICAgIC8vIFNlZSBodHRwczovL2RyYWZ0cy5jc3N3Zy5vcmcvY3NzLXZhbHVlcy0zLyN1cmxzXG4gICAgaWYgKC9bXCInKCkgXFx0XFxuXS8udGVzdCh1cmwpKSB7XG4gICAgICAgIHJldHVybiAnXCInICsgdXJsLnJlcGxhY2UoL1wiL2csICdcXFxcXCInKS5yZXBsYWNlKC9cXG4vZywgJ1xcXFxuJykgKyAnXCInXG4gICAgfVxuXG4gICAgcmV0dXJuIHVybFxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvdXJsL2VzY2FwZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvdXJsL2VzY2FwZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbm9ybWFsaXplLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vcm1hbGl6ZS5jc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub3JtYWxpemUuY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS5jc3Mvbm9ybWFsaXplLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJpbXBvcnQgeyBub3RpZnlTdWNjZXNzLCBub3RpZnlFcnJvciwgZ2V0TG9naW5JbmZvIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHtcbiAgR0VUX0NVUlJfVVNFUl9SRVFVRVNULFxuICBHRVRfQ1VSUl9VU0VSX1NVQ0NFU1MsXG4gIEdFVF9DVVJSX1VTRVJfRkFJTFVSRSxcbiAgSU5DUkVBU0VfQ1JFRElUX1JFUVVFU1QsXG4gIElOQ1JFQVNFX0NSRURJVF9TVUNDRVNTLFxuICBJTkNSRUFTRV9DUkVESVRfRkFJTFVSRSxcbiAgVVNFUl9MT0dJTl9SRVFVRVNULFxuICBVU0VSX0xPR0lOX1NVQ0NFU1MsXG4gIFVTRVJfTE9HSU5fRkFJTFVSRSxcbiAgVVNFUl9MT0dPVVRfUkVRVUVTVCxcbiAgVVNFUl9MT0dPVVRfU1VDQ0VTUyxcbiAgVVNFUl9MT0dPVVRfRkFJTFVSRSxcbiAgVVNFUl9JU19OT1RfTE9HR0VEX0lOLFxuICBHRVRfSU5JVF9GQUlMVVJFLFxuICBHRVRfSU5JVF9SRVFVRVNULFxuICBHRVRfSU5JVF9TVUNDRVNTLFxuICBHRVRfSU5JVF9VU0VSX0ZBSUxVUkUsXG4gIEdFVF9JTklUX1VTRVJfUkVRVUVTVCxcbiAgR0VUX0lOSVRfVVNFUl9TVUNDRVNTLFxufSBmcm9tICcuLi9jb25zdGFudHMnO1xuaW1wb3J0IGhpc3RvcnkgZnJvbSAnLi4vaGlzdG9yeSc7XG5pbXBvcnQge1xuICBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0LFxuICBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0LFxuICBzZW5kSW5pdGlhdGVSZXF1ZXN0LFxuICBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QsXG4gIHNlbmRMb2dpblJlcXVlc3QsXG59IGZyb20gJy4uL2FwaS1oYW5kbGVycy9hdXRoZW50aWNhdGlvbic7XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpblJlcXVlc3QodXNlcm5hbWUsIHBhc3N3b3JkKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dJTl9SRVFVRVNULFxuICAgIHBheWxvYWQ6IHtcbiAgICAgIHVzZXJuYW1lLFxuICAgICAgcGFzc3dvcmQsXG4gICAgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ2luU3VjY2VzcyhpbmZvKSB7XG4gIGNvbnNvbGUubG9nKGluZm8pO1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HSU5fU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IGluZm8gfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ2luRmFpbHVyZShlcnJvcikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HSU5fRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVycm9yIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkTG9naW5TdGF0dXMoKSB7XG4gIGNvbnN0IGxvZ2luSW5mbyA9IGdldExvZ2luSW5mbygpO1xuICBpZiAobG9naW5JbmZvKSB7XG4gICAgcmV0dXJuIGxvZ2luU3VjY2Vzcyhsb2dpbkluZm8pO1xuICB9XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9JU19OT1RfTE9HR0VEX0lOLFxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRDYXB0Y2hhVG9rZW4oKSB7XG4gIGlmIChwcm9jZXNzLmVudi5CUk9XU0VSKSB7XG4gICAgY29uc3QgbG9naW5Gb3JtID0gJCgnI2xvZ2luRm9ybScpLmdldCgwKTtcbiAgICBpZiAobG9naW5Gb3JtICYmIHR5cGVvZiBsb2dpbkZvcm1bJ2ctcmVjYXB0Y2hhLXJlc3BvbnNlJ10gPT09ICdvYmplY3QnKSB7XG4gICAgICByZXR1cm4gbG9naW5Gb3JtWydnLXJlY2FwdGNoYS1yZXNwb25zZSddLnZhbHVlO1xuICAgIH1cbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ2luVXNlcih1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKGxvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpKTtcbiAgICB0cnkge1xuICAgICAgLy8gY29uc3QgY2FwdGNoYVRva2VuID0gZ2V0Q2FwdGNoYVRva2VuKCk7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kTG9naW5SZXF1ZXN0KHVzZXJuYW1lLCBwYXNzd29yZCk7XG4gICAgICBkaXNwYXRjaChsb2dpblN1Y2Nlc3MocmVzKSk7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbG9naW5JbmZvJywgSlNPTi5zdHJpbmdpZnkocmVzKSk7XG4gICAgICAvLyBjb25zb2xlLmxvZyhsb2NhdGlvbi5wYXRobmFtZSk7XG4gICAgICBjb25zdCB7IHBhdGhuYW1lIH0gPSBnZXRTdGF0ZSgpLmhvdXNlO1xuICAgICAgY29uc29sZS5sb2cocGF0aG5hbWUpO1xuICAgICAgaGlzdG9yeS5wdXNoKHBhdGhuYW1lKTtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIGRpc3BhdGNoKGxvZ2luRmFpbHVyZShlcnIpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dvdXRSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HT1VUX1JFUVVFU1QsXG4gICAgcGF5bG9hZDoge30sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dvdXRTdWNjZXNzKCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HT1VUX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDoge1xuICAgICAgbWVzc2FnZTogJ9iu2LHZiNisINmF2YjZgdmC24zYqiDYotmF24zYsicsXG4gICAgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dEZhaWx1cmUoZXJyb3IpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR09VVF9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyb3IgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dFVzZXIoKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCkge1xuICAgIGRpc3BhdGNoKGxvZ291dFJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGRpc3BhdGNoKGxvZ291dFN1Y2Nlc3MoKSk7XG4gICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcbiAgICAgIGxvY2F0aW9uLmFzc2lnbignL2xvZ2luJyk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBkaXNwYXRjaChsb2dvdXRGYWlsdXJlKGVycikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZUZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGUoKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCkge1xuICAgIGRpc3BhdGNoKGluaXRpYXRlUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZEluaXRpYXRlUmVxdWVzdCgpO1xuICAgICAgZGlzcGF0Y2goaW5pdGlhdGVTdWNjZXNzKHJlcykpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlVXNlcnNSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1VTRVJfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlVXNlcnNTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfVVNFUl9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlVXNlcnNGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1VTRVJfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2VycygpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goaW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRJbml0aWF0ZVVzZXJzUmVxdWVzdCgpO1xuICAgICAgZGlzcGF0Y2goaW5pdGlhdGVVc2Vyc1N1Y2Nlc3MocmVzKSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goaW5pdGlhdGVVc2Vyc0ZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlclJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0NVUlJfVVNFUl9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXJTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0NVUlJfVVNFUl9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfQ1VSUl9VU0VSX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyKHVzZXJJZCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChnZXRDdXJyZW50VXNlclJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRnZXRDdXJyZW50VXNlclJlcXVlc3QodXNlcklkKTtcbiAgICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyU3VjY2VzcyhyZXMpKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChnZXRDdXJyZW50VXNlckZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbmNyZWFzZUNyZWRpdFJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogSU5DUkVBU0VfQ1JFRElUX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbmNyZWFzZUNyZWRpdFN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBJTkNSRUFTRV9DUkVESVRfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbmNyZWFzZUNyZWRpdEZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogSU5DUkVBU0VfQ1JFRElUX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0KGFtb3VudCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gsIGdldFN0YXRlKSB7XG4gICAgZGlzcGF0Y2goaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGN1cnJVc2VyIH0gPSBnZXRTdGF0ZSgpLmF1dGhlbnRpY2F0aW9uO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGluY3JlYXNlQ3JlZGl0UmVxdWVzdChhbW91bnQsIGN1cnJVc2VyLnVzZXJJZCk7XG4gICAgICBkaXNwYXRjaChpbmNyZWFzZUNyZWRpdFN1Y2Nlc3MocmVzKSk7XG4gICAgICBkaXNwYXRjaChnZXRDdXJyZW50VXNlcihjdXJyVXNlci51c2VySWQpKTtcbiAgICAgIG5vdGlmeVN1Y2Nlc3MoJ9in2YHYstin24zYtCDYp9i52KrYqNin2LEg2KjYpyDZhdmI2YHZgtuM2Kog2KfZhtis2KfZhSDYtNivLicpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIG5vdGlmeUVycm9yKCfYrti32Kcg2YXYrNiv2K8g2KrZhNin2LQg2qnZhtuM2K8nKTtcbiAgICAgIGRpc3BhdGNoKGluY3JlYXNlQ3JlZGl0RmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYWN0aW9ucy9hdXRoZW50aWNhdGlvbi5qcyIsImltcG9ydCB7XG4gIENSRUFURV9IT1VTRV9SRVFVRVNULFxuICBDUkVBVEVfSE9VU0VfU1VDQ0VTUyxcbiAgQ1JFQVRFX0hPVVNFX0ZBSUxVUkUsXG4gIEdFVF9IT1VTRV9SRVFVRVNULFxuICBHRVRfSE9VU0VfU1VDQ0VTUyxcbiAgR0VUX0hPVVNFX0ZBSUxVUkUsXG4gIEdFVF9IT1VTRV9QSE9ORV9SRVFVRVNULFxuICBHRVRfSE9VU0VfUEhPTkVfU1VDQ0VTUyxcbiAgR0VUX0hPVVNFX1BIT05FX0ZBSUxVUkUsXG4gIEdFVF9QSE9ORV9TVEFUVVNfUkVRVUVTVCxcbiAgR0VUX1BIT05FX1NUQVRVU19TVUNDRVNTLFxuICBHRVRfUEhPTkVfU1RBVFVTX0ZBSUxVUkUsXG59IGZyb20gJy4uL2NvbnN0YW50cyc7XG5pbXBvcnQgeyBnZXRDdXJyZW50VXNlciB9IGZyb20gJy4uL2FjdGlvbnMvYXV0aGVudGljYXRpb24nO1xuaW1wb3J0IHtcbiAgc2VuZGNyZWF0ZUhvdXNlUmVxdWVzdCxcbiAgc2VuZGdldEhvdXNlRGV0YWlsUmVxdWVzdCxcbiAgc2VuZGdldEhvdXNlUGhvbmVOdW1iZXJSZXF1ZXN0LFxuICBzZW5kZ2V0UGhvbmVTdGF0dXNSZXF1ZXN0LFxufSBmcm9tICcuLi9hcGktaGFuZGxlcnMvaG91c2UnO1xuaW1wb3J0IHsgbm90aWZ5RXJyb3IsIG5vdGlmeVN1Y2Nlc3MgfSBmcm9tICcuLi91dGlscyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVIb3VzZVJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogQ1JFQVRFX0hPVVNFX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVIb3VzZVN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBDUkVBVEVfSE9VU0VfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVIb3VzZUZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogQ1JFQVRFX0hPVVNFX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUhvdXNlKFxuICBhcmVhLFxuICBidWlsZGluZ1R5cGUsXG4gIGFkZHJlc3MsXG4gIGRlYWxUeXBlLFxuICBiYXNlUHJpY2UsXG4gIHJlbnRQcmljZSxcbiAgc2VsbFByaWNlLFxuICBwaG9uZSxcbiAgZGVzY3JpcHRpb24sXG4gIGV4cGlyZVRpbWUsXG4gIGltYWdlVVJMLFxuKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChjcmVhdGVIb3VzZVJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgY3VyclVzZXIgfSA9IGdldFN0YXRlKCkuYXV0aGVudGljYXRpb247XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kY3JlYXRlSG91c2VSZXF1ZXN0KFxuICAgICAgICBhcmVhLFxuICAgICAgICBidWlsZGluZ1R5cGUsXG4gICAgICAgIGFkZHJlc3MsXG4gICAgICAgIGRlYWxUeXBlLFxuICAgICAgICBiYXNlUHJpY2UsXG4gICAgICAgIHJlbnRQcmljZSxcbiAgICAgICAgc2VsbFByaWNlLFxuICAgICAgICBwaG9uZSxcbiAgICAgICAgZGVzY3JpcHRpb24sXG4gICAgICAgIGV4cGlyZVRpbWUsXG4gICAgICAgIGltYWdlVVJMLFxuICAgICAgKTtcbiAgICAgIGRpc3BhdGNoKGNyZWF0ZUhvdXNlU3VjY2VzcyhyZXMpKTtcbiAgICAgIG5vdGlmeVN1Y2Nlc3MoJ9iu2KfZhtmHINio2Kcg2YXZiNmB2YLbjNiqINir2KjYqiDYtNivJyk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgbm90aWZ5RXJyb3IoJ9iu2LfYpyDZhdis2K/YryDYqtmE2KfYtCDaqdmG24zYrycpO1xuICAgICAgZGlzcGF0Y2goY3JlYXRlSG91c2VGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldFBob25lU3RhdHVzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfUEhPTkVfU1RBVFVTX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRQaG9uZVN0YXR1c1N1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfUEhPTkVfU1RBVFVTX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UGhvbmVTdGF0dXNGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9QSE9ORV9TVEFUVVNfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UGhvbmVTdGF0dXMoaG91c2VJZCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gsIGdldFN0YXRlKSB7XG4gICAgZGlzcGF0Y2goZ2V0UGhvbmVTdGF0dXNSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGN1cnJVc2VyIH0gPSBnZXRTdGF0ZSgpLmF1dGhlbnRpY2F0aW9uO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGdldFBob25lU3RhdHVzUmVxdWVzdChob3VzZUlkKTtcbiAgICAgIGRpc3BhdGNoKGdldFBob25lU3RhdHVzU3VjY2VzcyhyZXMpKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChnZXRQaG9uZVN0YXR1c0ZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5leHBvcnQgZnVuY3Rpb24gZ2V0SG91c2VEZXRhaWxSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9IT1VTRV9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SG91c2VEZXRhaWxTdWNjZXNzKHJlc3BvbnNlLCBwYXRobmFtZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9IT1VTRV9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UsIHBhdGhuYW1lIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRIb3VzZURldGFpbEZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0hPVVNFX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhvdXNlRGV0YWlsKGhvdXNlSWQsIHBhdGhuYW1lKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChnZXRIb3VzZURldGFpbFJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgY3VyclVzZXIgfSA9IGdldFN0YXRlKCkuYXV0aGVudGljYXRpb247XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kZ2V0SG91c2VEZXRhaWxSZXF1ZXN0KGhvdXNlSWQpO1xuICAgICAgZGlzcGF0Y2goZ2V0SG91c2VEZXRhaWxTdWNjZXNzKHJlcywgcGF0aG5hbWUpKTtcbiAgICAgIGlmIChjdXJyVXNlcikge1xuICAgICAgICBkaXNwYXRjaChnZXRQaG9uZVN0YXR1cyhob3VzZUlkKSk7XG4gICAgICB9XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgZGlzcGF0Y2goZ2V0SG91c2VEZXRhaWxGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SG91c2VQaG9uZU51bWJlclJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0hPVVNFX1BIT05FX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRIb3VzZVBob25lTnVtYmVyU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9IT1VTRV9QSE9ORV9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhvdXNlUGhvbmVOdW1iZXJGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9IT1VTRV9QSE9ORV9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRIb3VzZVBob25lTnVtYmVyKGhvdXNlSWQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKGdldEhvdXNlUGhvbmVOdW1iZXJSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGN1cnJVc2VyIH0gPSBnZXRTdGF0ZSgpLmF1dGhlbnRpY2F0aW9uO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGdldEhvdXNlUGhvbmVOdW1iZXJSZXF1ZXN0KGhvdXNlSWQpO1xuICAgICAgY29uc29sZS5sb2cocmVzKTtcbiAgICAgIGlmICghcmVzLmhhc093blByb3BlcnR5KCdtZXNzYWdlJykgJiYgcmVzICE9PSBudWxsKSB7XG4gICAgICAgIGRpc3BhdGNoKGdldEhvdXNlUGhvbmVOdW1iZXJTdWNjZXNzKCdwYXllZCcpKTtcbiAgICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXIoY3VyclVzZXIudXNlcklkKSk7XG4gICAgICB9IGVsc2UgaWYgKHJlcy5oYXNPd25Qcm9wZXJ0eSgnbWVzc2FnZScpKSB7XG4gICAgICAgIGRpc3BhdGNoKGdldEhvdXNlUGhvbmVOdW1iZXJTdWNjZXNzKCdub19jcmVkaXQnKSk7XG4gICAgICB9XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goZ2V0SG91c2VQaG9uZU51bWJlckZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2FjdGlvbnMvaG91c2UuanMiLCJpbXBvcnQgeyBmZXRjaEFwaSwgZ2V0R2V0Q29uZmlnLCBnZXRQb3N0Q29uZmlnIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHtcbiAgZ2V0Q3VycmVudFVzZXJVcmwsXG4gIGdldEluaXRpYXRlVXJsLFxuICBsb2dpbkFwaVVybCxcbiAgaW5jcmVhc2VDcmVkaXRVcmwsXG59IGZyb20gJy4uL2NvbmZpZy9wYXRocyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KGFtb3VudCwgaWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGluY3JlYXNlQ3JlZGl0VXJsKGFtb3VudCwgaWQpLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KHVzZXJJZCkge1xuICByZXR1cm4gZmV0Y2hBcGkoZ2V0Q3VycmVudFVzZXJVcmwodXNlcklkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZEluaXRpYXRlUmVxdWVzdCgpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEluaXRpYXRlVXJsLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSB7XG4gIHJldHVybiBmZXRjaEFwaShsb2dpbkFwaVVybCwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZExvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKFxuICAgIGxvZ2luQXBpVXJsLFxuICAgIGdldFBvc3RDb25maWcoe1xuICAgICAgdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICB9KSxcbiAgKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwiaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IGZldGNoQXBpLCBnZXRQb3N0Q29uZmlnLCBnZXRHZXRDb25maWcgfSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQge1xuICBnZXRDcmVhdGVIb3VzZVVybCxcbiAgZ2V0SG91c2VEZXRhaWxVcmwsXG4gIGdldEhvdXNlUGhvbmVVcmwsXG59IGZyb20gJy4uL2NvbmZpZy9wYXRocyc7XG4vKipcbiAqIEBhcGkge2dldH0gL3YyL2FjcXVpc2l0aW9uL2RyaXZlci9waG9uZU51bWJlci86cGhvbmVOdW1iZXJcbiAqIEBhcGlOYW1lIERyaXZlckFjcXVpc2l0aW9uU2VhcmNoQnlQaG9uZU51bWJlclxuICogQGFwaVZlcnNpb24gMi4wLjBcbiAqIEBhcGlHcm91cCBEcml2ZXJcbiAqIEBhcGlQZXJtaXNzaW9uIEZJRUxELUFHRU5ULCBUUkFJTkVSLCBBRE1JTlxuICogQGFwaVBhcmFtIHtTdHJpbmd9IHBob25lTnVtYmVyIHBob25lIG51bWJlciB0byBzZWFyY2ggKCs5ODkxMjM0NTY3ODksIDA5MTIzNDU2Nzg5KVxuICogQGFwaVN1Y2Nlc3Mge1N0cmluZ30gcmVzdWx0IEFQSSByZXF1ZXN0IHJlc3VsdFxuICogQGFwaVN1Y2Nlc3NFeGFtcGxlIHtqc29ufSBTdWNjZXNzLVJlc3BvbnNlOlxuIHtcbiAgIFwicmVzdWx0XCI6IFwiT0tcIixcbiAgIFwiZGF0YVwiOiB7XG4gICAgICAgXCJkcml2ZXJzXCI6IFtcbiAgICAgICAge1xuICAgICAgICAgIFwiaWRcIjogMTAsXG4gICAgICAgICAgXCJmdWxsTmFtZVwiOiBcIti52YTbjNix2LbYpyDZgtix2KjYp9mG24xcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgXCJpZFwiOiAxMixcbiAgICAgICAgICBcImZ1bGxOYW1lXCI6IFwi2LnZhNuM2LHYttinINmC2LHYqNin2YbbjFwiXG4gICAgICAgIH1cbiAgICAgICBdXG4gICB9XG4gfVxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kY3JlYXRlSG91c2VSZXF1ZXN0KFxuICBhcmVhLFxuICBidWlsZGluZ1R5cGUsXG4gIGFkZHJlc3MsXG4gIGRlYWxUeXBlLFxuICBiYXNlUHJpY2UsXG4gIHJlbnRQcmljZSxcbiAgc2VsbFByaWNlLFxuICBwaG9uZSxcbiAgZGVzY3JpcHRpb24sXG4gIGV4cGlyZVRpbWUsXG4gIGltYWdlVVJMLFxuKSB7XG4gIHJldHVybiBmZXRjaEFwaShcbiAgICBnZXRDcmVhdGVIb3VzZVVybCxcbiAgICBnZXRQb3N0Q29uZmlnKHtcbiAgICAgIGFyZWEsXG4gICAgICBidWlsZGluZ1R5cGUsXG4gICAgICBhZGRyZXNzLFxuICAgICAgZGVhbFR5cGUsXG4gICAgICBiYXNlUHJpY2UsXG4gICAgICByZW50UHJpY2UsXG4gICAgICBzZWxsUHJpY2UsXG4gICAgICBwaG9uZSxcbiAgICAgIGRlc2NyaXB0aW9uLFxuICAgICAgZXhwaXJlVGltZSxcbiAgICAgIGltYWdlVVJMLFxuICAgIH0pLFxuICApO1xufVxuXG4vKipcbiAqIEBhcGkge2dldH0gL3YyL2FjcXVpc2l0aW9uL2RyaXZlci9waG9uZU51bWJlci86cGhvbmVOdW1iZXJcbiAqIEBhcGlOYW1lIERyaXZlckFjcXVpc2l0aW9uU2VhcmNoQnlQaG9uZU51bWJlclxuICogQGFwaVZlcnNpb24gMi4wLjBcbiAqIEBhcGlHcm91cCBEcml2ZXJcbiAqIEBhcGlQZXJtaXNzaW9uIEZJRUxELUFHRU5ULCBUUkFJTkVSLCBBRE1JTlxuICogQGFwaVBhcmFtIHtTdHJpbmd9IHBob25lTnVtYmVyIHBob25lIG51bWJlciB0byBzZWFyY2ggKCs5ODkxMjM0NTY3ODksIDA5MTIzNDU2Nzg5KVxuICogQGFwaVN1Y2Nlc3Mge1N0cmluZ30gcmVzdWx0IEFQSSByZXF1ZXN0IHJlc3VsdFxuICogQGFwaVN1Y2Nlc3NFeGFtcGxlIHtqc29ufSBTdWNjZXNzLVJlc3BvbnNlOlxuIHtcbiAgIFwicmVzdWx0XCI6IFwiT0tcIixcbiAgIFwiZGF0YVwiOiB7XG4gICAgICAgXCJkcml2ZXJzXCI6IFtcbiAgICAgICAge1xuICAgICAgICAgIFwiaWRcIjogMTAsXG4gICAgICAgICAgXCJmdWxsTmFtZVwiOiBcIti52YTbjNix2LbYpyDZgtix2KjYp9mG24xcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgXCJpZFwiOiAxMixcbiAgICAgICAgICBcImZ1bGxOYW1lXCI6IFwi2LnZhNuM2LHYttinINmC2LHYqNin2YbbjFwiXG4gICAgICAgIH1cbiAgICAgICBdXG4gICB9XG4gfVxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0SG91c2VEZXRhaWxSZXF1ZXN0KGhvdXNlSWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEhvdXNlRGV0YWlsVXJsKGhvdXNlSWQpLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0SG91c2VQaG9uZU51bWJlclJlcXVlc3QoaG91c2VJZCkge1xuICByZXR1cm4gZmV0Y2hBcGkoZ2V0SG91c2VQaG9uZVVybChob3VzZUlkKSwgZ2V0UG9zdENvbmZpZygpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRnZXRQaG9uZVN0YXR1c1JlcXVlc3QoaG91c2VJZCkge1xuICByZXR1cm4gZmV0Y2hBcGkoZ2V0SG91c2VQaG9uZVVybChob3VzZUlkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9hcGktaGFuZGxlcnMvaG91c2UuanMiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nPzJmYzJkNWMxXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmc/NmZhMjgwNGNcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLyogZXNsaW50LWRpc2FibGUgcmVhY3Qvbm8tZGFuZ2VyICovXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCB0d2l0dGVySWNvbiBmcm9tICcuL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZyc7XG5pbXBvcnQgaW5zdGFncmFtSWNvbiBmcm9tICcuLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZyc7XG5pbXBvcnQgdGVsZWdyYW1JY29uIGZyb20gJy4vMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nJztcbmltcG9ydCBzIGZyb20gJy4vbWFpbi5jc3MnO1xuXG5jbGFzcyBGb290ZXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxmb290ZXIgY2xhc3NOYW1lPVwic2l0ZS1mb290ZXIgY2VudGVyLWNvbHVtbiBkYXJrLWdyZWVuLWJhY2tncm91bmQgd2hpdGVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tNCB0ZXh0LWFsaWduLWxlZnRcIj5cbiAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJzb2NpYWwtaWNvbnMtbGlzdCBuby1saXN0LXN0eWxlXCI+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e2luc3RhZ3JhbUljb259XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzb2NpYWwtbmV0d29yay1pY29uXCJcbiAgICAgICAgICAgICAgICAgIGFsdD1cImluc3RhZ3JhbS1pY29uXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXt0d2l0dGVySWNvbn1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNvY2lhbC1uZXR3b3JrLWljb25cIlxuICAgICAgICAgICAgICAgICAgYWx0PVwidHdpdHRlci1pY29uXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXt0ZWxlZ3JhbUljb259XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzb2NpYWwtbmV0d29yay1pY29uXCJcbiAgICAgICAgICAgICAgICAgIGFsdD1cInRlbGVncmFtLWljb25cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8L3VsPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS04IHRleHQtYWxpZ24tcmlnaHRcIj5cbiAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICDYqtmF2KfZhduMINit2YLZiNmCINin24zZhiDZiNio4oCM2LPYp9uM2Kog2YXYqti52YTZgiDYqNmHINmF2YfYsdmG2KfYsiDYq9in2KjYqiDZiCDZh9mI2YXZhiDYtNix24zZgSDYstin2K/Zh1xuICAgICAgICAgICAgICDZhduM4oCM2KjYp9i02K8uXG4gICAgICAgICAgICA8L3A+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9mb290ZXI+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEZvb3Rlcik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvRm9vdGVyL0Zvb3Rlci5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmc/MjQ4YmZhOGRcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLyogZXNsaW50LWRpc2FibGUgcmVhY3Qvbm8tZGFuZ2VyICovXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBzIGZyb20gJy4vbWFpbi5jc3MnO1xuXG5jbGFzcyBIb3VzZVBhZ2UgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGhvdXNlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgY3VyclVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBob3VzZU1lc3NhZ2U6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICBnZXRQaG9uZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgICBwaG9uZVN0YXR1czogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB9O1xuICBoYW5kbGVTdWJtaXQgPSBlID0+IHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgdGhpcy5wcm9wcy5nZXRQaG9uZSgpO1xuICB9O1xuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5ob3VzZU1lc3NhZ2UpO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImhvbWUtcGFuZWxcIj5cbiAgICAgICAgPHNlY3Rpb24+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzZWFyY2gtdGhlbWUgZmxleC1jZW50ZXJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgICA8aDMgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBwZXJzaWFuLWJvbGQgYmx1ZS1mb250IGZsZXgtY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgINmF2LTYrti12KfYqiDaqdin2YXZhCDZhdmE2ql7JyAnfVxuICAgICAgICAgICAgICAgICAgPC9oMz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtbWQtNyBob3VzZS1pbWctbWFyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgYWx0PVwiaG91c2UtaW1nXCJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXt0aGlzLnByb3BzLmhvdXNlLmltYWdlVVJMfVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJpbWcgaG91c2UtaW1nXCJcbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1taWRkbGVcIj5cbiAgICAgICAgICAgICAgICAgIHshdGhpcy5wcm9wcy5waG9uZVN0YXR1cyAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmhvdXNlTWVzc2FnZSA9PT0gJ25vX2NyZWRpdCcgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2VhcmNoLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyIHBhZGQgYm9yZGVyLXJhZGl1cyBjbGVhci1pbnB1dCBibGFjay1mb250IHllbGxvdy1iYWNrZ3JvdW5kIHBob25lLWRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgINin2LnYqtio2KfYsSDYtNmF2Kcg2KjYsdin24wg2YXYtNin2YfYr9mHINi02YXYp9ix2Ycg2YXYp9mE2qlcXNmF2LTYp9mI2LEg2KfbjNmGINmF2YTaqSDaqdin2YHbjFxuICAgICAgICAgICAgICAgICAgICAgICAg2YbbjNiz2KpcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgKX1cblxuICAgICAgICAgICAgICAgICAgeyF0aGlzLnByb3BzLnBob25lU3RhdHVzICYmXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuaG91c2VNZXNzYWdlICE9PSAnbm9fY3JlZGl0JyAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmhvdXNlTWVzc2FnZSAhPT0gJ3BheWVkJyAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVTdWJtaXR9XG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBhZGQgc2VhcmNoLWJ1dHRvbiBwZXJzaWFuIGhhcy10cmFuc2l0aW9uIHRleHQtYWxpZ24tY2VudGVyIHBhZGQgYm9yZGVyLXJhZGl1cyBjbGVhci1pbnB1dCB3aGl0ZSBsaWdodC1ibHVlLWJhY2tncm91bmQgcGhvbmUtZGlzcGxheVwiXG4gICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAg2YXYtNin2YfYr9mHINi02YXYp9ix2Ycg2YXYp9mE2qlcXNmF2LTYp9mI2LFcbiAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1tZC00IGhvdXNlLWluZm8gcHVsbC1yaWdodFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIndoaXRlLWZvbnQgY29sLXhzLTUgY29sLW1kLTMgcHVsbC1yaWdodCB0ZXh0LWFsaWduLWNlbnRlciBwdXJwbGUtYmFja2dyb3VuZCBwZXJzaWFuIGluZm8tbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuaG91c2UuZGVhbFR5cGUgPT09IDEgPyAn2KfYrNin2LHZhycgOiAn2YHYsdmI2LQnfVxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaW5mby1saW5lc1wiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCIgdGV4dC1hbGlnbi1yaWdodCBjb2wteHMtNyBwZXJzaWFuIGJsYWNrLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMucGhvbmVTdGF0dXMgfHxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5ob3VzZU1lc3NhZ2UgPT09ICdwYXllZCdcbiAgICAgICAgICAgICAgICAgICAgICA/IHRoaXMucHJvcHMuaG91c2UucGhvbmVcbiAgICAgICAgICAgICAgICAgICAgICA6ICfbsNu527Lbs9uzKioq27Lbsyd9XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiIHRleHQtYWxpZ24tcmlnaHQgY29sLXhzLTUgcGVyc2lhbiBsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAg2LTZhdin2LHZhyDZhdin2YTaqVxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaW5mby1saW5lc1wiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IGNvbC14cy03IHBlcnNpYW4gYmxhY2stZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5ob3VzZS5idWlsZGluZ1R5cGV9XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBjb2wteHMtNSBwZXJzaWFuIGxpZ2h0LWdyZXktZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICDZhtmI2Lkg2LPYp9iu2KrZhdin2YZcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT1cInJvdyBpbmZvLWxpbmVzXCI+ICovfVxuICAgICAgICAgICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy03IHBlcnNpYW4gYmxhY2stZm9udFwiPiAqL31cbiAgICAgICAgICAgICAgICB7Lyog27LbsNuw27DbsNuw27AgPHNwYW4gY2xhc3NOYW1lPVwicGVyc2lhbiBibGFjay1mb250XCI+2KrZiNmF2KfZhjwvc3Bhbj4gKi99XG4gICAgICAgICAgICAgICAgey8qIDwvZGl2PiAqL31cbiAgICAgICAgICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNSBwZXJzaWFuIGxpZ2h0LWdyZXktZm9udFwiPtix2YfZhjwvZGl2PiAqL31cbiAgICAgICAgICAgICAgICB7LyogPC9kaXY+ICovfVxuICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmhvdXNlLmRlYWxUeXBlID09PSAxICYmIChcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGluZm8tbGluZXNcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IGNvbC14cy03IHBlcnNpYW4gYmxhY2stZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmhvdXNlLnJlbnRQcmljZX17JyAnfVxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInBlcnNpYW4gYmxhY2stZm9udFwiPtiq2YjZhdin2YY8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRleHQtYWxpZ24tcmlnaHQgY29sLXhzLTUgcGVyc2lhbiBsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICDYp9is2KfYsdmHXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5ob3VzZS5kZWFsVHlwZSA9PT0gMCAmJiAoXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBpbmZvLWxpbmVzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBjb2wteHMtNyBwZXJzaWFuIGJsYWNrLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5ob3VzZS5zZWxsUHJpY2V9eycgJ31cbiAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwZXJzaWFuIGJsYWNrLWZvbnRcIj7YqtmI2YXYp9mGPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IGNvbC14cy01IHBlcnNpYW4gbGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAg2YLbjNmF2KpcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGluZm8tbGluZXNcIj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBjb2wteHMtNyBwZXJzaWFuIGJsYWNrLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuaG91c2UuYWRkcmVzc31cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IGNvbC14cy01IHBlcnNpYW4gbGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgINii2K/YsdizXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBpbmZvLWxpbmVzXCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRleHQtYWxpZ24tcmlnaHQgY29sLXhzLTcgcGVyc2lhbiBibGFjay1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmhvdXNlLmFyZWF9eycgJ31cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicGVyc2lhbiBibGFjay1mb250XCI+2YXYqtixPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRleHQtYWxpZ24tcmlnaHQgY29sLXhzLTUgcGVyc2lhbiBsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAg2YXYqtix2KfamFxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaW5mby1saW5lc1wiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IGNvbC14cy03IHBlcnNpYW4gYmxhY2stZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5ob3VzZS5kZXNjcmlwdGlvbn1cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IGNvbC14cy01IHBlcnNpYW4gbGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgINiq2YjYttuM2K3Yp9iqXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEhvdXNlUGFnZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvSG91c2VQYWdlL0hvdXNlUGFnZS5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCJkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQVpBQUFBR1FDQU1BQUFDM1ljYitBQUFBTTFCTVZFWDUrdnZyN08zeTgvVHU3L0Q0K2ZuczdlNzMrUGp0Ny9EMjl2ZjMrZm50N3UvMDlmWHg4dlB2OFBIMTl2Znk4dlB6OVBVUTZWV1lBQUFINGtsRVFWUjRYdTNkMlhZVHZ4TEY0ZG9haHg3Zi8ybVBuVUE2ZUpuL1NhQWRLZWIzM1FDK3lZS05xaVIxVzdKL0R3QUFBQUFBQUFBQUFBQWcyRkRnZmJGeG9FaTZSb0oxOHhmTFhxMnJPdXNiUlZKV2U1Q3A2U2VmaU9Taml1TDhpRXlDMTBYMjNrZGRPUHNnSW9uU0F6SkpUWXF1MmxXWkpjMzJNVVF5U3pvOWs1QWxIK3luRWorY0NKRk0wcnljbmNseUUwQ0swc2NDSjVJZ2JSYW1Vek1wVXJaZkpLbVpEUkxKYkVQTGFtWjJaTEtmTWtDUy9jcEprdzBTU2JXaE9Ta2RNeVA1Y01hZ1crNSs5aEhzcENScHQ2dXduRFEvTGZkR3c2Sm9INElvYnhjcFMzRTlhY3pWK3g5K0JHWXBtRTFSeXZXc0luaDMyQlQ3Q0V6U2FwdWtPZGdBZ1NCSVM1YmlaSWVlSlF0WkZ5M1pvV3RUeHk1cENYYm9PKzFGa3B3ZHVpOE1rWXNkQnQ4NlFmL05SZlRmZnNlNEQ2alEveEV1SHYrU0EzZ042UEVBQUFBQUFNQzZMS3NOQTZzdXVpVlMzaVI3Z1VVWDN2b29PaVRyamtDeUR0N2VVTEltNjJLWDV2SnErVmszc1hyZktZOFFGWU85cWxLenZyQkp1LzNrSkdmOTRIWlFoS1pZclNONHFkeDhOOVA2UVpHV093SDFnaWJWMjRTOGRVYkZHcVptb1VyWmJOQ3V6cXkzLzd3WHg3cndxa1kxNndxN3RObFBnK3lkTU5FYWFJcUZJbVgzS2t2SmVzT2l3Mnpkb2VyTmovNE9BQUFBQUFBQUFBQUFBQUFBQUp2a2c0MENreTRXR3dXY0ZDVWJCWFpkWkJzRlFwYTAyakFRcHIxYVJ3QUFBQUFBQUFBQUFLaHRzb0hBUzV0OUFvcTdWZXhFZS94Y0luQzY1ZXhNS1E3MTJKOUFMSDMyZURBQ3NmY2s5NENMVTJ3Y0JHSmVxdllRYXlXUVA3Qkt1ejFBbUJWWEF2a0RUZDdPbDVvdTVrQWduelpMajNtUHRrbktpVUErYTVLU25TdDRTVnRJV1ordmh3UlNwR0tuS2xHdi9TUE1rcFpnWFJISUpzbFhlN0ZHS1JicmgwQnFsdVJ1LzlnTFBXU0tVaXMzbzF5NVdnZk1zbzZtY2FlbGRNQTZwRWx4dDF0aFVaOWRaVmJxVFRIOVpxVHY3UForVEpiQ21ST3Mrc0hQN3lPUW1qWGJhZEp2UmtKVDVvbmhSN2xnNTduL0w1OTZ0UkRjcjAyYmxLd0gzSjhpTkRYckExRjVwSXFGK1U3TjJ2dFZMS3pTWkRkeXg0cUZxTVYrVlh0V0xNeFNHTGxpVWJQNlZ5eHExa2dWQzdQazN1dDgrVFpXM1lyV0VZSnV6ZllkQUFBQXdGMFZHd1grNi9VQ0VBZ2tmMW9nWUlRUUNBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFRQ3JGeG9DMEwwMFhOZ0JVMS9TRGRZYzY2eXA2dHc5UXN1Q2lwTFlsR3dGU2x1UlgrNzdXMVo3SUdxVTQyVmQ0VkRuTXl2WTBwaSs3LzdVMktRYzdYNVcycDhwanNpOHhQK29JbGwxSzVQRjVYaGZlenVjVjdVbWtlT1R4YlVkSWtHWjdEcUVkZVh6ZkhqSkpxejJIN1l2L2I1VmtEN0JJOWh5UzFNSnozQUR5SEx4VW51U09uS2VRSlA4c3QwZzloZmtKcHUrbjNTUWMrbS9saGVjWUlFbmFUNnJmblRPWm5xRDJubmozK1M3cGhFeW92Vm50ckpFVysyYlNPbFNza1RjV20zTGRzNjV5Nk5OQ25IMS91NVRPckJoT1VxN1dRWG1PRFFldmVPWjZacE0waDE3cnFXSWRETHV4R0tRbHE5OU14MG5Xd2NBYmkxNFhMUm1CRExLeHVCL1BUbnVvcGRoN2JDeld3YVk1YkN6bVlrK0Z4UzFjekRZVVZQczBBQUFBQUFBQUFBQUFBQUFBQU1rdFhzcCtXNjAvVEUxdm9ndldINGZQS2ZyWitkZmY3ZFlSSmtseFMvWWlURTNTSEt3WFRMY0I3RkhLSkRMUVlXYzFTN04xZ1JTbFpEZENsbGJyQWZudXlRODFxbGtIbU40VnB5bExXbDdqY1gxTzZFQlRETWZjOTRVUFpoYTZuQitHU1hMSFFZMXhjM09VdkYwc2l2YmxzTHdOa1BuSDNMZithQ3BPcXZiVjhOWkJxdVREU3lKRldsNS9LZmJGVU41YXQ1UEsvcm9jYkdxZEFzRlJsN3lhZWVrSXhFbkIwTzBnRGkvLzR3aWVWVm82TlhYTXZ3U3lTWDZkbWpSMW12YkNLeC9SaEJyMXd2ZGZHRkt5SnNsWmFycFlnbG5xdkhWQ0lOWVVrMWx4VSsyM3VZanBtTnV1VXZ5WlFXcHN2L2Mvcm5pVDVLZGtkWjBsWlVQdkE3Mm5xRGViZFlINS9ZNVZuYU5lTE1YNlFMcHBGbVYzcmdURG1KZU5nT3Q0c0VtTGpRUGgvN3p5NDVKOUtZVDRYd2NOejRvZEV1SE5ySnpzbnVvMWRDQjdzSWRidlY4N1ZDMjVjUDlDOWpad0hwUGkvdmlmY2ZIbGljeVNvcXYyWHRpYmptSTJKQytwbFlmL2pCN1RudEowc2V6SlhxVnAwVVdjYkdoVGxPVHJFd1p5ZkdXbmVlL3p0L25tVG5DU3lsT1ZyTU02UngyMFRQWU5GTWsvdXFrdnEvV1NkdWV2M0Y3c2UyaFN0V0ZnSCtwSkFVSlVERFlNYkZJck5neTRLTW1QRXducXJJdTUybENJcE5nd01QMStJUTBtV3BpbDNZYUJJbVViQjhLc1pDTkJ0UkVCQUFBQUFBQUFBQUFBQUFBQUFJQmFybXdVY0xxeXUwQWdjSkwvWkNCZ2hCQUltR1VCQUFBQUFBQUFBQUFBQUFBQWtyTDNmbmJPVGFWVTZ3ejZoYk0vRXV3c2tQelYzd1hTVGtzRTcwSklmMXF5a3B5ZGlFRCsxaTVWR3dlQnpKSzNjUkJJbHJUYUtBZ2s2R0tjdms0Z1JmSTZxNi83cTRsQS9vYVQ2bW0zcU9yS0VjamZXQlRQdTJkWWF2OWFJS0drY3dPSldzeVdQKzNyQkZMMGs3LzQrMERxUzZZMXFnVksxdDhGb2xNSzlpcVYxMDdpYU9wL0dNanNyaloveXQ5K2s4SmYzSTZPU1VwMklxOXNGMy9hMStFa081TTAyOFhSMTdzaWtDUjV0eFk3K25wWEJMTHJWZlJ6SG1CK1JDQmw5anAwN09zRWNnaGxjb3ZYbGJlK0NPUTJsMnFEU3Z2bVh5eHVyWU1IOHZ6V0plcTl0bzJUeWFKbS81YmdvbDVrZjlYMHdoY2JnLy9YNnZ4ckhNdWVqdEs2TlYzNFpDUFk4ajhWU01xUzJoNXVQNTUxc2RrWHd5UXBUblpIblNYbGFsOEpzNlF0MkgybFNUSFpGOEtzdU5wdmhmbXJFOEdXN0w5c1VyT0JZUHJSUlFBQUFBQUFBQUFBQVA0SHdkNHdWYzhYZjlvQUFBQUFTVVZPUks1Q1lJST1cIlxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG91c2VQYWdlL2dlb21ldHJ5Mi5wbmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG91c2VQYWdlL2dlb21ldHJ5Mi5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG91c2VQYWdlL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0hvdXNlUGFnZS9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xheW91dC5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xheW91dC5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLyoqXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXG4gKlxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBub3JtYWxpemVDc3MgZnJvbSAnbm9ybWFsaXplLmNzcyc7XG5pbXBvcnQgcyBmcm9tICcuL0xheW91dC5jc3MnO1xuaW1wb3J0IEZvb3RlciBmcm9tICcuLi9Gb290ZXInO1xuaW1wb3J0IFBhbmVsIGZyb20gJy4uL1BhbmVsJztcblxuY2xhc3MgTGF5b3V0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBtYWluUGFuZWw6IFByb3BUeXBlcy5ib29sLFxuICB9O1xuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIG1haW5QYW5lbDogZmFsc2UsXG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbWQtMTIgY29sLXhzLTEyXCIgc3R5bGU9e3sgcGFkZGluZzogJzAnIH19PlxuICAgICAgICA8UGFuZWwgbWFpblBhbmVsPXt0aGlzLnByb3BzLm1haW5QYW5lbH0gLz5cbiAgICAgICAge3RoaXMucHJvcHMuY2hpbGRyZW59XG4gICAgICAgIDxGb290ZXIgLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhub3JtYWxpemVDc3MsIHMpKExheW91dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5qcyIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9Mb2FkZXIuc2Nzcyc7XG5pbXBvcnQgZ2lmRmlsZSBmcm9tICcuL2xvYWRlci5naWYnO1xuXG5jbGFzcyBMb2FkZXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGxvYWRpbmc6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3RoaXMucHJvcHMubG9hZGluZyA/ICdsb2FkZXIgbG9hZGluZycgOiAnbG9hZGVyJ30+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGltZyBzcmM9e2dpZkZpbGV9IGFsdD1cImxvYWRlciBnaWZcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsb2FkZXItaW5mb1wiPtmF2YbYqti42LEg2KjYp9i024zYrzwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKExvYWRlcik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5qcyIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTG9hZGVyLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTG9hZGVyLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9Mb2FkZXIuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuc2Nzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDYiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Mb2FkZXIvbG9hZGVyLmdpZj9iZTVjMzIyM1wiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL2xvYWRlci5naWZcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL2xvYWRlci5naWZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDYiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCAqIGFzIGF1dGhBY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMvYXV0aGVudGljYXRpb24nO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5pbXBvcnQgbG9nb0ltZyBmcm9tICcuL2xvZ28ucG5nJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uLy4uL2hpc3RvcnknO1xuXG5jbGFzcyBQYW5lbCBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY3VyclVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBhdXRoQWN0aW9uczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIG1haW5QYW5lbDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgICBpc0xvZ2dlZEluOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnByb3BzLmN1cnJVc2VyKTtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnByb3BzLmlzTG9nZ2VkSW4pO1xuICAgIGlmICh0aGlzLnByb3BzLm1haW5QYW5lbCkge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPGhlYWRlciBjbGFzc05hbWU9XCJoaWRkZW4teHNcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaGVhZGVyLW1haW5cIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4LWNlbnRlciBjb2wtbWQtMiAgIG5hdi11c2VyIGNvbC14cy0xMiBkcm9wZG93biBcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1jZW50ZXIgaGVhZGVyLW1haW4tYm9hcmRlclwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy05IFwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IHdoaXRlLWZvbnQgcGVyc2lhbiBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgINmG2KfYrduM2Ycg2qnYp9ix2KjYsduMXG4gICAgICAgICAgICAgICAgICAgICAgPC9oNT5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImRyb3Bkb3duLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBibGFjay1mb250IFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLnVzZXJuYW1lfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IHB1bGwtbGVmdCBwZXJzaWFuIGxpZ2h0LWdyZXktZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImxpZ2h0LWdyZXktZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KrZiNmF2KfZhlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIuYmFsYW5jZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IGxpZ2h0LWdyZXktZm9udCBwdWxsLXJpZ2h0ICBwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINin2LnYqtio2KfYsVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItin2YHYstin24zYtCDYp9i52KrYqNin2LFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnL2luY3JlYXNlQ3JlZGl0Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2K7YsdmI2KxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuYXV0aEFjdGlvbnMubG9nb3V0VXNlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICB7IXRoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLZiNix2YjYr1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9sb2dpbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vLXBhZGRpbmcgIGNvbC14cy0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtc21pbGUtbyBmYS0yeCB3aGl0ZS1mb250XCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9oZWFkZXI+XG4gICAgICApO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgPGhlYWRlcj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBuYXYtY2xcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleC1jZW50ZXIgY29sLW1kLTIgICBuYXYtdXNlciBjb2wteHMtMTIgZHJvcGRvd25cIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBmbGV4LWNlbnRlciBcIj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTkgXCI+XG4gICAgICAgICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWFsaWduLXJpZ2h0IHBlcnNpYW4gcHVycGxlLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICDZhtin2K3bjNmHINqp2KfYsdio2LHbjFxuICAgICAgICAgICAgICAgICAgICA8L2g1PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImRyb3Bkb3duLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwiYmxhY2stZm9udCBwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgYmx1ZS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLnVzZXJuYW1lfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCIgbGlnaHQtZ3JleS1mb250IGNvbC14cy02IHB1bGwtbGVmdCBibHVlLWZvbnQgcGVyc2lhblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYqtmI2YXYp9mGXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIuYmFsYW5jZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxpZ2h0LWdyZXktZm9udCBjb2wteHMtNiBwdWxsLXJpZ2h0IGJsdWUtZm9udCBwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYp9i52KrYqNin2LFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9pbmNyZWFzZUNyZWRpdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2K7YsdmI2KxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5hdXRoQWN0aW9ucy5sb2dvdXRVc2VyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICB7IXRoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2YjYsdmI2K9cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9sb2dpbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vLXBhZGRpbmcgIGNvbC14cy0zXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXNtaWxlLW8gZmEtMnggcHVycGxlLWZvbnRcIiAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgtY2VudGVyIGNvbC1tZC00IGNvbC1tZC1vZmZzZXQtNiBuYXYtbG9nbyBjb2wteHMtMTJcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyAgXCI+XG4gICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmbGV4LXJvdy1yZXZcIlxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvJyk7XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibm8tcGFkZGluZyB0ZXh0LWFsaWduLWNlbnRlciBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwiaWNvbi1jbFwiIHNyYz17bG9nb0ltZ30gYWx0PVwibG9nb1wiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy05XCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4tYm9sZCB0ZXh0LWFsaWduLXJpZ2h0ICBwZXJzaWFuIGJsdWUtZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAg2K7Yp9mG2Ycg2KjZhyDYr9mI2LRcbiAgICAgICAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2hlYWRlcj5cbiAgICApO1xuICB9XG59XG5mdW5jdGlvbiBtYXBEaXNwYXRjaFRvUHJvcHMoZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIHtcbiAgICBhdXRoQWN0aW9uczogYmluZEFjdGlvbkNyZWF0b3JzKGF1dGhBY3Rpb25zLCBkaXNwYXRjaCksXG4gIH07XG59XG5cbmZ1bmN0aW9uIG1hcFN0YXRlVG9Qcm9wcyhzdGF0ZSkge1xuICBjb25zdCB7IGF1dGhlbnRpY2F0aW9uIH0gPSBzdGF0ZTtcbiAgY29uc3QgeyBjdXJyVXNlcixpc0xvZ2dlZEluIH0gPSBhdXRoZW50aWNhdGlvbjtcbiAgcmV0dXJuIHtcbiAgICBjdXJyVXNlcixcbiAgICBpc0xvZ2dlZEluLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzKShQYW5lbCksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL1BhbmVsL1BhbmVsLmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmc/MmFmNzNkOGFcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLy8gbm9pbnNwZWN0aW9uIEpTQW5ub3RhdG9yXG5jb25zdCBsZXZlbHMgPSBbXG4gIHtcbiAgICBNQVhfV0lEVEg6IDYwMCxcbiAgICBNQVhfSEVJR0hUOiA2MDAsXG4gICAgRE9DVU1FTlRfU0laRTogMzAwMDAwLFxuICB9LFxuICB7XG4gICAgTUFYX1dJRFRIOiA4MDAsXG4gICAgTUFYX0hFSUdIVDogODAwLFxuICAgIERPQ1VNRU5UX1NJWkU6IDUwMDAwMCxcbiAgfSxcbl07XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgZ2V0TGV2ZWwobGV2ZWwpIHtcbiAgICByZXR1cm4gbGV2ZWxzW2xldmVsIC0gMV07XG4gIH0sXG59O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb25maWcvY29tcHJlc3Npb25Db25maWcuanMiLCIvKiBlc2xpbnQtZGlzYWJsZSBwcmV0dGllci9wcmV0dGllciAqL1xuaW1wb3J0IHsgYXBpIH0gZnJvbSAnLi91cmxzJztcblxuZXhwb3J0IGNvbnN0IGxvZ2luQXBpVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvbG9naW5gO1xuZXhwb3J0IGNvbnN0IGxvZ291dEFwaVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2xvZ2luYDtcbmV4cG9ydCBjb25zdCBnZXRTZWFyY2hIb3VzZXNVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9TZWFyY2hTZXJ2bGV0YDtcbmV4cG9ydCBjb25zdCBnZXRDcmVhdGVIb3VzZVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9Ib3VzZVNlcnZsZXRgO1xuZXhwb3J0IGNvbnN0IGdldEluaXRpYXRlVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvaG9tZWA7XG5leHBvcnQgY29uc3QgZ2V0Q3VycmVudFVzZXJVcmwgPSAodXNlcklkKSA9PiBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvSW5kaXZpZHVhbFNlcnZsZXQ/dXNlcklkPSR7dXNlcklkfWA7XG5leHBvcnQgY29uc3QgZ2V0SG91c2VEZXRhaWxVcmwgPSAoaG91c2VJZCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0dldFNpbmdsZUhvdXNlP2lkPSR7aG91c2VJZH1gO1xuZXhwb3J0IGNvbnN0IGdldEhvdXNlUGhvbmVVcmwgPSAoaG91c2VJZCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL1BheW1lbnRTZXJ2bGV0P2lkPSR7aG91c2VJZH1gO1xuZXhwb3J0IGNvbnN0IGluY3JlYXNlQ3JlZGl0VXJsID0gKGFtb3VudCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0NyZWRpdFNlcnZsZXQ/dmFsdWU9JHthbW91bnR9YDtcblxuZXhwb3J0IGNvbnN0IHNlYXJjaERyaXZlclVybCA9IHBob25lTnVtYmVyID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9waG9uZU51bWJlci8ke3Bob25lTnVtYmVyfWA7XG5leHBvcnQgY29uc3QgZ2V0RHJpdmVyQWNxdWlzaXRpb25JbmZvVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCB1cGRhdGVEcml2ZXJBY3F1aXNpdGlvbkluZm9VcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IHVwZGF0ZURyaXZlckRvY3NVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvZG9jdW1lbnRzLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCBhcHByb3ZlRHJpdmVyVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2FwcHJvdmUvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IHJlamVjdERyaXZlclVybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9yZWplY3QvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IGdldERyaXZlckRvY3VtZW50VXJsID0gZG9jdW1lbnRJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvZG9jdW1lbnQvJHtkb2N1bWVudElkfWA7XG5leHBvcnQgY29uc3QgZ2V0RHJpdmVyRG9jdW1lbnRJZHNVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvZG9jdW1lbnRzLyR7ZHJpdmVySWR9YDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29uZmlnL3BhdGhzLmpzIiwiLyoqXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXG4gKlxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqL1xuXG5pbXBvcnQgY3JlYXRlQnJvd3Nlckhpc3RvcnkgZnJvbSAnaGlzdG9yeS9jcmVhdGVCcm93c2VySGlzdG9yeSc7XG5cbi8vIE5hdmlnYXRpb25CYXIgbWFuYWdlciwgZS5nLiBoaXN0b3J5LnB1c2goJy9ob21lJylcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9tamFja3Nvbi9oaXN0b3J5XG5leHBvcnQgZGVmYXVsdCBwcm9jZXNzLmVudi5CUk9XU0VSICYmIGNyZWF0ZUJyb3dzZXJIaXN0b3J5KCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2hpc3RvcnkuanMiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgKiBhcyBob3VzZUFjdGlvbnMgZnJvbSAnLi4vLi4vLi4vYWN0aW9ucy9ob3VzZSc7XG5pbXBvcnQgKiBhcyBhdXRoQWN0aW9ucyBmcm9tICcuLi8uLi8uLi9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uJztcbmltcG9ydCBIb3VzZVBhZ2UgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9Ib3VzZVBhZ2UnO1xuaW1wb3J0IExvYWRlciBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0xvYWRlcic7XG5cbmNsYXNzIEhvdXNlRGV0YWlsQ29udGFpbmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBob3VzZUFjdGlvbnM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBhdXRoQWN0aW9uczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIGlzRmV0Y2hpbmc6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gICAgaG91c2VJZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIGhvdXNlRGV0YWlsOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgaG91c2VNZXNzYWdlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gICAgY3VyclVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBwaG9uZVN0YXR1czogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB9O1xuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLnByb3BzLmhvdXNlQWN0aW9ucy5nZXRIb3VzZURldGFpbChcbiAgICAgIHRoaXMucHJvcHMuaG91c2VJZCxcbiAgICAgIGxvY2F0aW9uLnBhdGhuYW1lLFxuICAgICk7XG4gIH1cbiAgaGFuZGxlR2V0UGhvbmUgPSAoKSA9PiB7XG4gICAgdGhpcy5wcm9wcy5ob3VzZUFjdGlvbnMuZ2V0SG91c2VQaG9uZU51bWJlcih0aGlzLnByb3BzLmhvdXNlSWQpO1xuICB9O1xuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5ob3VzZU1lc3NhZ2UpO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8SG91c2VQYWdlXG4gICAgICAgICAgaG91c2U9e3RoaXMucHJvcHMuaG91c2VEZXRhaWx9XG4gICAgICAgICAgY3VyclVzZXI9e3RoaXMucHJvcHMuY3VyclVzZXJ9XG4gICAgICAgICAgZ2V0UGhvbmU9e3RoaXMuaGFuZGxlR2V0UGhvbmV9XG4gICAgICAgICAgaG91c2VNZXNzYWdlPXt0aGlzLnByb3BzLmhvdXNlTWVzc2FnZX1cbiAgICAgICAgICBwaG9uZVN0YXR1cz17dGhpcy5wcm9wcy5waG9uZVN0YXR1c31cbiAgICAgICAgLz5cbiAgICAgICAgPExvYWRlciBsb2FkaW5nPXt0aGlzLnByb3BzLmlzRmV0Y2hpbmd9IC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmZ1bmN0aW9uIG1hcERpc3BhdGNoVG9Qcm9wcyhkaXNwYXRjaCkge1xuICByZXR1cm4ge1xuICAgIGhvdXNlQWN0aW9uczogYmluZEFjdGlvbkNyZWF0b3JzKGhvdXNlQWN0aW9ucywgZGlzcGF0Y2gpLFxuICAgIGF1dGhBY3Rpb25zOiBiaW5kQWN0aW9uQ3JlYXRvcnMoYXV0aEFjdGlvbnMsIGRpc3BhdGNoKSxcbiAgfTtcbn1cblxuZnVuY3Rpb24gbWFwU3RhdGVUb1Byb3BzKHN0YXRlKSB7XG4gIGNvbnN0IHsgaG91c2UgfSA9IHN0YXRlO1xuICBjb25zdCB7IGF1dGhlbnRpY2F0aW9uIH0gPSBzdGF0ZTtcbiAgY29uc3QgeyBpc0ZldGNoaW5nLCBob3VzZURldGFpbCwgaG91c2VNZXNzYWdlLCBwaG9uZVN0YXR1cyB9ID0gaG91c2U7XG4gIGNvbnN0IHsgY3VyclVzZXIgfSA9IGF1dGhlbnRpY2F0aW9uO1xuICByZXR1cm4ge1xuICAgIGlzRmV0Y2hpbmcsXG4gICAgaG91c2VEZXRhaWwsXG4gICAgaG91c2VNZXNzYWdlLFxuICAgIGN1cnJVc2VyLFxuICAgIHBob25lU3RhdHVzLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgSG91c2VEZXRhaWxDb250YWluZXIsXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9yb3V0ZXMvaG91c2VzL2hvdXNlRGV0YWlsL0hvdXNlRGV0YWlsQ29udGFpbmVyLmpzIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBIb3VzZURldGFpbENvbnRhaW5lciBmcm9tICcuL0hvdXNlRGV0YWlsQ29udGFpbmVyJztcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9MYXlvdXQnO1xuXG5jb25zdCB0aXRsZSA9ICfYp9i22KfZgdmHINqp2LHYr9mGINiu2KfZhtmHJztcblxuZnVuY3Rpb24gYWN0aW9uKHsgcGFyYW1zOiB7IGhvdXNlSWQgfSB9KSB7XG4gIHJldHVybiB7XG4gICAgY2h1bmtzOiBbJ2hvdXNlRGV0YWlsJ10sXG4gICAgdGl0bGUsXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8SG91c2VEZXRhaWxDb250YWluZXIgaG91c2VJZD17aG91c2VJZH0gLz4sXG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3JvdXRlcy9ob3VzZXMvaG91c2VEZXRhaWwvaW5kZXguanMiLCJpbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IEVYSUYgZnJvbSAnZXhpZi1qcyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi9oaXN0b3J5JztcbmltcG9ydCBjb21wcmVzc2lvbkNvbmZpZyBmcm9tICcuLi9jb25maWcvY29tcHJlc3Npb25Db25maWcnO1xuaW1wb3J0IHsgbG9hZExvZ2luU3RhdHVzIH0gZnJvbSAnLi4vYWN0aW9ucy9hdXRoZW50aWNhdGlvbic7XG5cbmNvbnN0IGNvbXByZXNzaW9uRmlyc3RMZXZlbCA9IGNvbXByZXNzaW9uQ29uZmlnLmdldExldmVsKDEpO1xuY29uc3QgY29tcHJlc3Npb25TZWNvbmRMZXZlbCA9IGNvbXByZXNzaW9uQ29uZmlnLmdldExldmVsKDIpO1xuY29uc3QgZG9jdW1lbnRzQ29tcHJlc3Npb25MZXZlbCA9IHtcbiAgcHJvZmlsZVBpY3R1cmU6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgZHJpdmVyTGljZW5jZTogY29tcHJlc3Npb25GaXJzdExldmVsLFxuICBjYXJJZENhcmQ6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgaW5zdXJhbmNlOiBjb21wcmVzc2lvblNlY29uZExldmVsLFxufTtcblxuZnVuY3Rpb24gZ2V0Q29tcHJlc3Npb25MZXZlbChkb2N1bWVudFR5cGUpIHtcbiAgcmV0dXJuIGRvY3VtZW50c0NvbXByZXNzaW9uTGV2ZWxbZG9jdW1lbnRUeXBlXSB8fCBjb21wcmVzc2lvbkZpcnN0TGV2ZWw7XG59XG5cbmZ1bmN0aW9uIGRhdGFVUkx0b0Jsb2IoZGF0YVVSTCwgZmlsZVR5cGUpIHtcbiAgY29uc3QgYmluYXJ5ID0gYXRvYihkYXRhVVJMLnNwbGl0KCcsJylbMV0pO1xuICBjb25zdCBhcnJheSA9IFtdO1xuICBjb25zdCBsZW5ndGggPSBiaW5hcnkubGVuZ3RoO1xuICBsZXQgaTtcbiAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgYXJyYXkucHVzaChiaW5hcnkuY2hhckNvZGVBdChpKSk7XG4gIH1cbiAgcmV0dXJuIG5ldyBCbG9iKFtuZXcgVWludDhBcnJheShhcnJheSldLCB7IHR5cGU6IGZpbGVUeXBlIH0pO1xufVxuXG5mdW5jdGlvbiBnZXRTY2FsZWRJbWFnZU1lYXN1cmVtZW50cyh3aWR0aCwgaGVpZ2h0LCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgeyBNQVhfSEVJR0hULCBNQVhfV0lEVEggfSA9IGdldENvbXByZXNzaW9uTGV2ZWwoZG9jdW1lbnRUeXBlKTtcblxuICBpZiAod2lkdGggPiBoZWlnaHQpIHtcbiAgICBoZWlnaHQgKj0gTUFYX1dJRFRIIC8gd2lkdGg7XG4gICAgd2lkdGggPSBNQVhfV0lEVEg7XG4gIH0gZWxzZSB7XG4gICAgd2lkdGggKj0gTUFYX0hFSUdIVCAvIGhlaWdodDtcbiAgICBoZWlnaHQgPSBNQVhfSEVJR0hUO1xuICB9XG4gIHJldHVybiB7XG4gICAgaGVpZ2h0LFxuICAgIHdpZHRoLFxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRJbWFnZU9yaWVudGF0aW9uKGltYWdlKSB7XG4gIGxldCBvcmllbnRhdGlvbiA9IDA7XG4gIEVYSUYuZ2V0RGF0YShpbWFnZSwgZnVuY3Rpb24oKSB7XG4gICAgb3JpZW50YXRpb24gPSBFWElGLmdldFRhZyh0aGlzLCAnT3JpZW50YXRpb24nKTtcbiAgfSk7XG4gIHJldHVybiBvcmllbnRhdGlvbjtcbn1cblxuZnVuY3Rpb24gZ2V0SW1hZ2VNZWFzdXJlbWVudHMoaW1hZ2UpIHtcbiAgY29uc3Qgb3JpZW50YXRpb24gPSBnZXRJbWFnZU9yaWVudGF0aW9uKGltYWdlKTtcbiAgbGV0IHRyYW5zZm9ybTtcbiAgbGV0IHN3YXAgPSBmYWxzZTtcbiAgc3dpdGNoIChvcmllbnRhdGlvbikge1xuICAgIGNhc2UgODpcbiAgICAgIHN3YXAgPSB0cnVlO1xuICAgICAgdHJhbnNmb3JtID0gJ2xlZnQnO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSA2OlxuICAgICAgc3dhcCA9IHRydWU7XG4gICAgICB0cmFuc2Zvcm0gPSAncmlnaHQnO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAzOlxuICAgICAgdHJhbnNmb3JtID0gJ2ZsaXAnO1xuICAgICAgYnJlYWs7XG4gICAgZGVmYXVsdDpcbiAgfVxuICBjb25zdCB3aWR0aCA9IHN3YXAgPyBpbWFnZS5oZWlnaHQgOiBpbWFnZS53aWR0aDtcbiAgY29uc3QgaGVpZ2h0ID0gc3dhcCA/IGltYWdlLndpZHRoIDogaW1hZ2UuaGVpZ2h0O1xuICByZXR1cm4ge1xuICAgIHdpZHRoLFxuICAgIGhlaWdodCxcbiAgICB0cmFuc2Zvcm0sXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldFNjYWxlZEltYWdlRmlsZUZyb21DYW52YXMoXG4gIGltYWdlLFxuICB3aWR0aCxcbiAgaGVpZ2h0LFxuICB0cmFuc2Zvcm0sXG4gIGZpbGVUeXBlLFxuKSB7XG4gIGNvbnN0IGNhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO1xuICBjYW52YXMud2lkdGggPSB3aWR0aDtcbiAgY2FudmFzLmhlaWdodCA9IGhlaWdodDtcbiAgY29uc3QgY29udGV4dCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICBsZXQgdHJhbnNmb3JtUGFyYW1zO1xuICBsZXQgc3dhcE1lYXN1cmUgPSBmYWxzZTtcbiAgbGV0IGltYWdlV2lkdGggPSB3aWR0aDtcbiAgbGV0IGltYWdlSGVpZ2h0ID0gaGVpZ2h0O1xuICBzd2l0Y2ggKHRyYW5zZm9ybSkge1xuICAgIGNhc2UgJ2xlZnQnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzAsIC0xLCAxLCAwLCAwLCBoZWlnaHRdO1xuICAgICAgc3dhcE1lYXN1cmUgPSB0cnVlO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAncmlnaHQnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzAsIDEsIC0xLCAwLCB3aWR0aCwgMF07XG4gICAgICBzd2FwTWVhc3VyZSA9IHRydWU7XG4gICAgICBicmVhaztcbiAgICBjYXNlICdmbGlwJzpcbiAgICAgIHRyYW5zZm9ybVBhcmFtcyA9IFsxLCAwLCAwLCAtMSwgMCwgaGVpZ2h0XTtcbiAgICAgIGJyZWFrO1xuICAgIGRlZmF1bHQ6XG4gICAgICB0cmFuc2Zvcm1QYXJhbXMgPSBbMSwgMCwgMCwgMSwgMCwgMF07XG4gIH1cbiAgY29udGV4dC5zZXRUcmFuc2Zvcm0oLi4udHJhbnNmb3JtUGFyYW1zKTtcbiAgaWYgKHN3YXBNZWFzdXJlKSB7XG4gICAgaW1hZ2VIZWlnaHQgPSB3aWR0aDtcbiAgICBpbWFnZVdpZHRoID0gaGVpZ2h0O1xuICB9XG4gIGNvbnRleHQuZHJhd0ltYWdlKGltYWdlLCAwLCAwLCBpbWFnZVdpZHRoLCBpbWFnZUhlaWdodCk7XG4gIGNvbnRleHQuc2V0VHJhbnNmb3JtKDEsIDAsIDAsIDEsIDAsIDApO1xuICBjb25zdCBkYXRhVVJMID0gY2FudmFzLnRvRGF0YVVSTChmaWxlVHlwZSk7XG4gIHJldHVybiBkYXRhVVJMdG9CbG9iKGRhdGFVUkwsIGZpbGVUeXBlKTtcbn1cblxuZnVuY3Rpb24gc2hvdWxkUmVzaXplKGltYWdlTWVhc3VyZW1lbnRzLCBmaWxlU2l6ZSwgZG9jdW1lbnRUeXBlKSB7XG4gIGNvbnN0IHsgTUFYX0hFSUdIVCwgTUFYX1dJRFRILCBET0NVTUVOVF9TSVpFIH0gPSBnZXRDb21wcmVzc2lvbkxldmVsKFxuICAgIGRvY3VtZW50VHlwZSxcbiAgKTtcbiAgaWYgKGRvY3VtZW50VHlwZSA9PT0gJ2luc3VyYW5jZScgJiYgZmlsZVNpemUgPCBET0NVTUVOVF9TSVpFKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9IGVsc2UgaWYgKGZpbGVTaXplIDwgRE9DVU1FTlRfU0laRSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICByZXR1cm4gKFxuICAgIGltYWdlTWVhc3VyZW1lbnRzLndpZHRoID4gTUFYX1dJRFRIIHx8IGltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCA+IE1BWF9IRUlHSFRcbiAgKTtcbn1cblxuYXN5bmMgZnVuY3Rpb24gcHJvY2Vzc0ZpbGUoZGF0YVVSTCwgZmlsZVNpemUsIGZpbGVUeXBlLCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgaW1hZ2UgPSBuZXcgSW1hZ2UoKTtcbiAgaW1hZ2Uuc3JjID0gZGF0YVVSTDtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICBpbWFnZS5vbmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGNvbnN0IGltYWdlTWVhc3VyZW1lbnRzID0gZ2V0SW1hZ2VNZWFzdXJlbWVudHMoaW1hZ2UpO1xuICAgICAgaWYgKCFzaG91bGRSZXNpemUoaW1hZ2VNZWFzdXJlbWVudHMsIGZpbGVTaXplLCBkb2N1bWVudFR5cGUpKSB7XG4gICAgICAgIHJlc29sdmUoZGF0YVVSTHRvQmxvYihkYXRhVVJMLCBmaWxlVHlwZSkpO1xuICAgICAgfVxuICAgICAgY29uc3Qgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMgPSBnZXRTY2FsZWRJbWFnZU1lYXN1cmVtZW50cyhcbiAgICAgICAgaW1hZ2VNZWFzdXJlbWVudHMud2lkdGgsXG4gICAgICAgIGltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCxcbiAgICAgICAgZG9jdW1lbnRUeXBlLFxuICAgICAgKTtcbiAgICAgIGNvbnN0IHNjYWxlZEltYWdlRmlsZSA9IGdldFNjYWxlZEltYWdlRmlsZUZyb21DYW52YXMoXG4gICAgICAgIGltYWdlLFxuICAgICAgICBzY2FsZWRJbWFnZU1lYXN1cmVtZW50cy53aWR0aCxcbiAgICAgICAgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMuaGVpZ2h0LFxuICAgICAgICBpbWFnZU1lYXN1cmVtZW50cy50cmFuc2Zvcm0sXG4gICAgICAgIGZpbGVUeXBlLFxuICAgICAgKTtcbiAgICAgIHJlc29sdmUoc2NhbGVkSW1hZ2VGaWxlKTtcbiAgICB9O1xuICAgIGltYWdlLm9uZXJyb3IgPSByZWplY3Q7XG4gIH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29tcHJlc3NGaWxlKGZpbGUsIGRvY3VtZW50VHlwZSA9ICdkZWZhdWx0Jykge1xuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XG4gICAgICByZWFkZXIub25sb2FkZW5kID0gYXN5bmMgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnN0IGNvbXByZXNzZWRGaWxlID0gYXdhaXQgcHJvY2Vzc0ZpbGUoXG4gICAgICAgICAgcmVhZGVyLnJlc3VsdCxcbiAgICAgICAgICBmaWxlLnNpemUsXG4gICAgICAgICAgZmlsZS50eXBlLFxuICAgICAgICAgIGRvY3VtZW50VHlwZSxcbiAgICAgICAgKTtcbiAgICAgICAgcmVzb2x2ZShjb21wcmVzc2VkRmlsZSk7XG4gICAgICB9O1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgcmVqZWN0KGVycik7XG4gICAgfVxuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldExvZ2luSW5mbygpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCBsb2dpbkluZm8gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9naW5JbmZvJyk7XG4gICAgaWYgKF8uaXNTdHJpbmcobG9naW5JbmZvKSkge1xuICAgICAgcmV0dXJuIEpTT04ucGFyc2UobG9naW5JbmZvKTtcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUubG9nKCdFcnJvciB3aGVuIHBhcnNpbmcgbG9naW5JbmZvIG9iamVjdDogJywgZXJyKTtcbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRQb3N0Q29uZmlnKGJvZHkpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgbWltZVR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvZ2luSW5mby50b2tlbn1gLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLCBtaW1lVHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgLy8gbW9kZTogJ25vLWNvcnMnLFxuICAgIGhlYWRlcnMsXG4gICAgLi4uKF8uaXNFbXB0eShib2R5KSA/IHt9IDogeyBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KSB9KSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFB1dENvbmZpZyhib2R5KSB7XG4gIGNvbnN0IGxvZ2luSW5mbyA9IGdldExvZ2luSW5mbygpO1xuICBjb25zdCBoZWFkZXJzID0gbG9naW5JbmZvXG4gICAgPyB7XG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdYLUF1dGhvcml6YXRpb24nOiBsb2dpbkluZm8udG9rZW4sXG4gICAgICB9XG4gICAgOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfTtcbiAgcmV0dXJuIHtcbiAgICBtZXRob2Q6ICdQVVQnLFxuICAgIGhlYWRlcnMsXG4gICAgLi4uKF8uaXNFbXB0eShib2R5KSA/IHt9IDogeyBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KSB9KSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEdldENvbmZpZygpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvZ2luSW5mby50b2tlbn1gLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnR0VUJyxcbiAgICBoZWFkZXJzLFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29udmVydE51bWJlcnNUb1BlcnNpYW4oc3RyKSB7XG4gIGNvbnN0IHBlcnNpYW5OdW1iZXJzID0gJ9uw27Hbstuz27Tbtdu227fbuNu5JztcbiAgbGV0IHJlc3VsdCA9ICcnO1xuICBpZiAoIXN0ciAmJiBzdHIgIT09IDApIHtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG4gIHN0ciA9IHN0ci50b1N0cmluZygpO1xuICBsZXQgY29udmVydGVkQ2hhcjtcbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyBpKyspIHtcbiAgICB0cnkge1xuICAgICAgY29udmVydGVkQ2hhciA9IHBlcnNpYW5OdW1iZXJzW3N0cltpXV07XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgY29udmVydGVkQ2hhciA9IHN0cltpXTtcbiAgICB9XG4gICAgcmVzdWx0ICs9IGNvbnZlcnRlZENoYXI7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGZldGNoQXBpKHVybCwgY29uZmlnKSB7XG4gIGxldCBmbG93RXJyb3I7XG4gIHRyeSB7XG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCBjb25maWcpO1xuICAgIGNvbnNvbGUubG9nKHJlcyk7XG4gICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG4gICAgbGV0IHJlc3VsdCA9IGF3YWl0IHJlcy5qc29uKCk7XG4gICAgY29uc29sZS5sb2cocmVzdWx0KTtcbiAgICBpZiAoIV8uaXNFbXB0eShfLmdldChyZXN1bHQsICdkYXRhJykpKSB7XG4gICAgICByZXN1bHQgPSByZXN1bHQuZGF0YTtcbiAgICB9XG4gICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG5cbiAgICBpZiAocmVzLnN0YXR1cyAhPT0gMjAwKSB7XG4gICAgICBmbG93RXJyb3IgPSByZXN1bHQ7XG4gICAgICBjb25zb2xlLmxvZyhyZXMuc3RhdHVzKTtcblxuICAgICAgaWYgKHJlcy5zdGF0dXMgPT09IDQwMykge1xuICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcbiAgICAgICAgZmxvd0Vycm9yLm1lc3NhZ2UgPSAn2YTYt9mB2Kcg2K/ZiNio2KfYsdmHINmI2KfYsdivINi02YjbjNivISc7XG4gICAgICAgIGNvbnNvbGUubG9nKCd5ZWVlcycpO1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IGhpc3RvcnkucHVzaCgnL2xvZ2luJyksIDMwMDApO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgICBjb25zb2xlLmRlYnVnKGBFcnJvciB3aGVuIGZldGNoaW5nIGFwaTogJHt1cmx9YCwgZXJyKTtcbiAgICBmbG93RXJyb3IgPSB7XG4gICAgICBjb2RlOiAnVU5LTk9XTl9FUlJPUicsXG4gICAgICBtZXNzYWdlOiAn2K7Yt9in24wg2YbYp9mF2LTYrti1INmE2LfZgdinINiv2YjYqNin2LHZhyDYs9i524wg2qnZhtuM2K8nLFxuICAgIH07XG4gIH1cbiAgaWYgKGZsb3dFcnJvcikge1xuICAgIHRocm93IGZsb3dFcnJvcjtcbiAgfVxufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gc2V0SW1tZWRpYXRlKGZuKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGxldCB2YWx1ZSA9IG51bGw7XG4gICAgICBpZiAoXy5pc0Z1bmN0aW9uKGZuKSkge1xuICAgICAgICB2YWx1ZSA9IGZuKCk7XG4gICAgICB9XG4gICAgICByZXNvbHZlKHZhbHVlKTtcbiAgICB9LCAwKTtcbiAgfSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRDb29raWUobmFtZSwgdmFsdWUsIGRheXMgPSA3KSB7XG4gIGxldCBleHBpcmVzID0gJyc7XG4gIGlmIChkYXlzKSB7XG4gICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKCk7XG4gICAgZGF0ZS5zZXRUaW1lKGRhdGUuZ2V0VGltZSgpICsgZGF5cyAqIDI0ICogNjAgKiA2MCAqIDEwMDApO1xuICAgIGV4cGlyZXMgPSBgOyBleHBpcmVzPSR7ZGF0ZS50b1VUQ1N0cmluZygpfWA7XG4gIH1cbiAgZG9jdW1lbnQuY29va2llID0gYCR7bmFtZX09JHt2YWx1ZSB8fCAnJ30ke2V4cGlyZXN9OyBwYXRoPS9gO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldENvb2tpZShuYW1lKSB7XG4gIGNvbnN0IG5hbWVFUSA9IGAke25hbWV9PWA7XG4gIGNvbnN0IGNhID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XG4gIGNvbnN0IGxlbmd0aCA9IGNhLmxlbmd0aDtcbiAgbGV0IGkgPSAwO1xuICBmb3IgKDsgaSA8IGxlbmd0aDsgaSArPSAxKSB7XG4gICAgbGV0IGMgPSBjYVtpXTtcbiAgICB3aGlsZSAoYy5jaGFyQXQoMCkgPT09ICcgJykge1xuICAgICAgYyA9IGMuc3Vic3RyaW5nKDEsIGMubGVuZ3RoKTtcbiAgICB9XG4gICAgaWYgKGMuaW5kZXhPZihuYW1lRVEpID09PSAwKSB7XG4gICAgICByZXR1cm4gYy5zdWJzdHJpbmcobmFtZUVRLmxlbmd0aCwgYy5sZW5ndGgpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGVyYXNlQ29va2llKG5hbWUpIHtcbiAgZG9jdW1lbnQuY29va2llID0gYCR7bmFtZX09OyBNYXgtQWdlPS05OTk5OTk5OTtgO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm90aWZ5RXJyb3IobWVzc2FnZSkge1xuICB0b2FzdHIub3B0aW9ucyA9IHtcbiAgICBjbG9zZUJ1dHRvbjogZmFsc2UsXG4gICAgZGVidWc6IGZhbHNlLFxuICAgIG5ld2VzdE9uVG9wOiBmYWxzZSxcbiAgICBwcm9ncmVzc0JhcjogZmFsc2UsXG4gICAgcG9zaXRpb25DbGFzczogJ3RvYXN0LXRvcC1jZW50ZXInLFxuICAgIHByZXZlbnREdXBsaWNhdGVzOiB0cnVlLFxuICAgIG9uY2xpY2s6IG51bGwsXG4gICAgc2hvd0R1cmF0aW9uOiAnMzAwJyxcbiAgICBoaWRlRHVyYXRpb246ICcxMDAwJyxcbiAgICB0aW1lT3V0OiAnMTAwMDAnLFxuICAgIGV4dGVuZGVkVGltZU91dDogJzEwMDAnLFxuICAgIHNob3dFYXNpbmc6ICdzd2luZycsXG4gICAgaGlkZUVhc2luZzogJ2xpbmVhcicsXG4gICAgc2hvd01ldGhvZDogJ3NsaWRlRG93bicsXG4gICAgaGlkZU1ldGhvZDogJ3NsaWRlVXAnLFxuICB9O1xuICB0b2FzdHIuZXJyb3IobWVzc2FnZSwgJ9iu2LfYpycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm90aWZ5U3VjY2VzcyhtZXNzYWdlKSB7XG4gIHRvYXN0ci5vcHRpb25zID0ge1xuICAgIGNsb3NlQnV0dG9uOiBmYWxzZSxcbiAgICBkZWJ1ZzogZmFsc2UsXG4gICAgbmV3ZXN0T25Ub3A6IGZhbHNlLFxuICAgIHByb2dyZXNzQmFyOiBmYWxzZSxcbiAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtdG9wLWNlbnRlcicsXG4gICAgcHJldmVudER1cGxpY2F0ZXM6IHRydWUsXG4gICAgb25jbGljazogbnVsbCxcbiAgICBzaG93RHVyYXRpb246ICczMDAnLFxuICAgIGhpZGVEdXJhdGlvbjogJzEwMDAnLFxuICAgIHRpbWVPdXQ6ICc1MDAwJyxcbiAgICBleHRlbmRlZFRpbWVPdXQ6ICcxMDAwJyxcbiAgICBzaG93RWFzaW5nOiAnc3dpbmcnLFxuICAgIGhpZGVFYXNpbmc6ICdsaW5lYXInLFxuICAgIHNob3dNZXRob2Q6ICdzbGlkZURvd24nLFxuICAgIGhpZGVNZXRob2Q6ICdzbGlkZVVwJyxcbiAgfTtcbiAgdG9hc3RyLnN1Y2Nlc3MobWVzc2FnZSwgJycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVsb2FkU2VsZWN0UGlja2VyKCkge1xuICB0cnkge1xuICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoJ3JlbmRlcicpO1xuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmxvZyhlcnIpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkU2VsZWN0UGlja2VyKCkge1xuICB0cnkge1xuICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoe30pO1xuICAgIGlmIChcbiAgICAgIC9BbmRyb2lkfHdlYk9TfGlQaG9uZXxpUGFkfGlQb2R8QmxhY2tCZXJyeS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudClcbiAgICApIHtcbiAgICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoJ21vYmlsZScpO1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Rm9ybURhdGEoZGF0YSkge1xuICBpZiAoXy5pc0VtcHR5KGRhdGEpKSB7XG4gICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgaW5wdXQgZGF0YSB0byBjcmVhdGUgYSBGb3JtRGF0YSBvYmplY3QnKTtcbiAgfVxuICBsZXQgZm9ybURhdGE7XG4gIHRyeSB7XG4gICAgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICBjb25zdCBrZXlzID0gXy5rZXlzKGRhdGEpO1xuICAgIGxldCBpO1xuICAgIGNvbnN0IGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgZm9ybURhdGEuYXBwZW5kKGtleXNbaV0sIGRhdGFba2V5c1tpXV0pO1xuICAgIH1cbiAgICByZXR1cm4gZm9ybURhdGE7XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUuZGVidWcoZXJyKTtcbiAgICB0aHJvdyBFcnJvcignRk9STV9EQVRBX0JST1dTRVJfU1VQUE9SVF9GQUlMVVJFJyk7XG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRVc2VyTG9naW5TdGF0dXMoc3RvcmUpIHtcbiAgY29uc3QgbG9naW5TdGF0dXNBY3Rpb24gPSBsb2FkTG9naW5TdGF0dXMoKTtcbiAgc3RvcmUuZGlzcGF0Y2gobG9naW5TdGF0dXNBY3Rpb24pO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy91dGlscy9pbmRleC5qcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFxQkE7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaFJBO0FBY0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFhQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0JBO0FBQUE7QUFBQTtBQUFBO0FBMkJBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbkJBO0FBQUE7QUFBQTtBQUFBO0FBbUJBOzs7Ozs7OztBQ3BNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFGQTtBQUtBOzs7Ozs7OztBQ2hDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBS0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBO0FBYUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFjQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNwR0E7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQWZBO0FBREE7QUF5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFCQTtBQURBO0FBb0NBO0FBdkNBO0FBQ0E7QUF5Q0E7Ozs7Ozs7QUNuREE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFTQTtBQUNBO0FBQ0E7QUFYQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQURBO0FBREE7QUFEQTtBQURBO0FBWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWkE7QUFSQTtBQThCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBREE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQWNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBVUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBdEVBO0FBL0JBO0FBREE7QUFiQTtBQURBO0FBa0lBO0FBakpBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQW1KQTs7Ozs7OztBQzFKQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBTUE7QUFoQkE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUZBO0FBREE7QUFNQTtBQURBO0FBY0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFPQTtBQWJBO0FBQ0E7QUFEQTtBQUVBO0FBREE7QUFlQTs7Ozs7OztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUM3QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFGQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEzQkE7QUF1Q0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUExQ0E7QUFEQTtBQUpBO0FBOERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQS9EQTtBQURBO0FBREE7QUFEQTtBQURBO0FBREE7QUE4RUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBREE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRkE7QUFEQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBM0JBO0FBdUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBMUNBO0FBREE7QUFKQTtBQThEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEvREE7QUFEQTtBQURBO0FBdUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFUQTtBQURBO0FBREE7QUFEQTtBQXhFQTtBQURBO0FBREE7QUFtR0E7QUFoTUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFpTUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7Ozs7Ozs7QUM1TkE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBOzs7Ozs7OztBQ2RBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7Ozs7Ozs7OztBQ3BCQTtBQUFBO0FBQUE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFrQkE7QUFDQTtBQW5CQTtBQUNBO0FBVUE7QUFDQTtBQUlBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUkE7QUFXQTtBQWxDQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFvQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDbkVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUpBO0FBU0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3NIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBMUJBOzs7Ozs7O0FBeElBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF0Q0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXNDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWZBO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=