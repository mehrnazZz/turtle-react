require("source-map-support").install();
exports.ids = [2];
exports.modules = {

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/node_modules/normalize.css/normalize.css"],"names":[],"mappings":"AAAA,4EAA4E;AAC5E;gFACgF;AAChF;;;;GAIG;AACH;EACE,kBAAkB;EAClB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,+BAA+B;EAC/B,OAAO,EAAE;AACX;gFACgF;AAChF;;GAEG;AACH;EACE,UAAU,EAAE;AACd;;GAEG;AACH;;;;;;EAME,eAAe,EAAE;AACnB;;;GAGG;AACH;EACE,eAAe;EACf,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;EAGE,OAAO;EACP,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;;GAGG;AACH;EACE,gCAAgC;UACxB,wBAAwB;EAChC,OAAO;EACP,UAAU;EACV,OAAO;EACP,kBAAkB;EAClB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,sCAAsC;EACtC,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,oBAAoB;EACpB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,0CAA0C;UAClC,kCAAkC;EAC1C,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,qBAAqB,EAAE;AACzB;;GAEG;AACH;;EAEE,oBAAoB,EAAE;AACxB;;;GAGG;AACH;;;EAGE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,uBAAuB;EACvB,YAAY,EAAE;AAChB;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,eAAe;EACf,eAAe;EACf,mBAAmB;EACnB,yBAAyB,EAAE;AAC7B;EACE,gBAAgB,EAAE;AACpB;EACE,YAAY,EAAE;AAChB;gFACgF;AAChF;;GAEG;AACH;;EAEE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc;EACd,UAAU,EAAE;AACd;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;;;EAKE,wBAAwB;EACxB,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,kBAAkB;EAClB,OAAO;EACP,UAAU;EACV,OAAO,EAAE;AACX;;;GAGG;AACH;;EAEE,OAAO;EACP,kBAAkB,EAAE;AACtB;;;GAGG;AACH;;EAEE,OAAO;EACP,qBAAqB,EAAE;AACzB;;;;GAIG;AACH;;;;EAIE,2BAA2B;EAC3B,OAAO,EAAE;AACX;;GAEG;AACH;;;;EAIE,mBAAmB;EACnB,WAAW,EAAE;AACf;;GAEG;AACH;;;;EAIE,+BAA+B,EAAE;AACnC;;GAEG;AACH;EACE,+BAA+B,EAAE;AACnC;;;;;GAKG;AACH;EACE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,eAAe;EACf,OAAO;EACP,eAAe;EACf,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,WAAW;EACX,OAAO;EACP,oBAAoB;EACpB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,sBAAsB;EACtB,OAAO;EACP,yBAAyB;EACzB,OAAO,EAAE;AACX;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,WAAW;EACX,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,aAAa,EAAE;AACjB;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,qBAAqB;EACrB,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,yBAAyB,EAAE;AAC7B;;;GAGG;AACH;EACE,2BAA2B;EAC3B,OAAO;EACP,cAAc;EACd,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;;EAEE,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;gFACgF;AAChF;;GAEG;AACH;EACE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc,EAAE;AAClB;gFACgF;AAChF;;GAEG;AACH;EACE,cAAc,EAAE","file":"normalize.css","sourcesContent":["/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,YAAY;EACZ,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE;IACE,aAAa,EAAE,EAAE;AACrB;EACE;IACE,eAAe,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n.browserupgrade {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA,iBAAiB;AACjB;;;;;;;GAOG;AACH;;;;;;;GAOG;AACH;EACE;;gFAE8E;;EAE9E,kEAAkE;;EAElE;;gFAE8E;;EAE9E,4BAA4B;;EAE5B;;gFAE8E;;EAE9E,uBAAuB,EAAE,gCAAgC;EACzD,uBAAuB,EAAE,2BAA2B;EACpD,uBAAuB,EAAE,6BAA6B;EACtD,wBAAwB,CAAC,iCAAiC;CAC3D;AACD;EACE,qBAAqB;EACrB,mCAAmC;EACnC,uKAAuK;EACvK,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,6BAA6B;EAC7B,qJAAqJ;EACrJ,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,kCAAkC;EAClC,oKAAoK;EACpK,iBAAiB,EAAE;AACrB;;;;GAIG;AACH;;gFAEgF;AAChF;EACE,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;EACnB,WAAW,EAAE;AACf;EACE,UAAU;EACV,qBAAqB,EAAE;AACzB;EACE,eAAe,EAAE;AACnB;;;;;;GAMG;AACH;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;;GAEG;AACH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW,EAAE;AACf;;;;GAIG;AACH;;;;;;EAME,uBAAuB,EAAE;AAC3B;;GAEG;AACH;EACE,UAAU;EACV,UAAU;EACV,WAAW,EAAE;AACf;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;gFAEgF;AAChF;EACE,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,iBAAiB,EAAE;AACrB;;;;gFAIgF;AAChF;EACE;;;IAGE,mCAAmC;IACnC,uBAAuB;IACvB,+DAA+D;IAC/D,oCAAoC;YAC5B,4BAA4B;IACpC,6BAA6B,EAAE;EACjC;;IAEE,2BAA2B,EAAE;EAC/B;IACE,6BAA6B,EAAE;EACjC;IACE,8BAA8B,EAAE;EAClC;;;KAGG;EACH;;IAEE,YAAY,EAAE;EAChB;;IAEE,uBAAuB;IACvB,yBAAyB,EAAE;EAC7B;;;KAGG;EACH;IACE,4BAA4B,EAAE;EAChC;;IAEE,yBAAyB,EAAE;EAC7B;IACE,2BAA2B,EAAE;EAC/B;;;IAGE,WAAW;IACX,UAAU,EAAE;EACd;;IAEE,wBAAwB,EAAE,EAAE","file":"Layout.css","sourcesContent":["@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n:global(.browserupgrade) {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".loader {\n  position: fixed;\n  font-family: IRSans, serif;\n  z-index: 10;\n  padding: 20px;\n  border-radius: 10px;\n  background-color: white;\n  display: none;\n  top: calc(50% - 85px);\n  left: calc(50% - 70px);\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n  .loader img {\n    width: 100px; }\n  .loader .loader-info {\n    direction: rtl;\n    padding-top: 10px;\n    text-align: center; }\n  .loader.loading {\n    display: block; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Loader/Loader.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,2BAA2B;EAC3B,YAAY;EACZ,cAAc;EACd,oBAAoB;EACpB,wBAAwB;EACxB,cAAc;EACd,sBAAsB;EACtB,uBAAuB;EACvB,wHAAwH;UAChH,gHAAgH,EAAE;EAC1H;IACE,aAAa,EAAE;EACjB;IACE,eAAe;IACf,kBAAkB;IAClB,mBAAmB,EAAE;EACvB;IACE,eAAe,EAAE","file":"Loader.scss","sourcesContent":[".loader {\n  position: fixed;\n  font-family: IRSans, serif;\n  z-index: 10;\n  padding: 20px;\n  border-radius: 10px;\n  background-color: white;\n  display: none;\n  top: calc(50% - 85px);\n  left: calc(50% - 70px);\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n  .loader img {\n    width: 100px; }\n  .loader .loader-info {\n    direction: rtl;\n    padding-top: 10px;\n    text-align: center; }\n  .loader.loading {\n    display: block; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/NewHouseForm/main.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.search-theme{\n  background: url(" + escape(__webpack_require__("./src/components/NewHouseForm/geometry2.png")) + ");\n  height: 15vh;\n  margin-top: 10vh;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\n.radio {\n  margin: 3%;\n  margin-right: 15vw;\n  margin-bottom: 0; }\n.radio-label {\n  direction: ltr; }\n.error {\n  border: 1px solid red !important; }\n.err-msg {\n  text-align: right;\n  direction: rtl;\n  color: #f16464; }\n.add-label {\n  width: 100%;\n  text-align: left; }\n.add-input {\n  width: 100%;\n  height: 6vh;\n  border-radius: 5px;\n  border: thin solid lightgrey;\n  padding: 2%; }\n.mar-left-add {\n  margin-left: 11vw;\n  margin-bottom: 5vh; }\n.empty-label-height {\n  height: 2.5vh; }\n.flex-middle-respon {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 3vh;\n  margin-top: 2vh; }\n.form-container {\n  margin-bottom: 5vh; }\n/*title css*/\n@media all and (max-width: 768px) {\n  .mar-left-add {\n    margin-left: 0; }\n  .register-headline {\n    font-size: small; }\n  .flex-middle-respon {\n    display: inline-block;\n    margin-bottom: 3vh; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/NewHouseForm/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,0CAAmC;EACnC,aAAa;EACb,iBAAiB;CAClB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW;EACX,mBAAmB;EACnB,iBAAiB,EAAE;AACrB;EACE,eAAe,EAAE;AACnB;EACE,iCAAiC,EAAE;AACrC;EACE,kBAAkB;EAClB,eAAe;EACf,eAAe,EAAE;AACnB;EACE,YAAY;EACZ,iBAAiB,EAAE;AACrB;EACE,YAAY;EACZ,YAAY;EACZ,mBAAmB;EACnB,6BAA6B;EAC7B,YAAY,EAAE;AAChB;EACE,kBAAkB;EAClB,mBAAmB,EAAE;AACvB;EACE,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;EAChC,mBAAmB;EACnB,gBAAgB,EAAE;AACpB;EACE,mBAAmB,EAAE;AACvB,aAAa;AACb;EACE;IACE,eAAe,EAAE;EACnB;IACE,iBAAiB,EAAE;EACrB;IACE,sBAAsB;IACtB,mBAAmB,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.search-theme{\n  background: url(\"./geometry2.png\");\n  height: 15vh;\n  margin-top: 10vh;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\n.radio {\n  margin: 3%;\n  margin-right: 15vw;\n  margin-bottom: 0; }\n.radio-label {\n  direction: ltr; }\n.error {\n  border: 1px solid red !important; }\n.err-msg {\n  text-align: right;\n  direction: rtl;\n  color: #f16464; }\n.add-label {\n  width: 100%;\n  text-align: left; }\n.add-input {\n  width: 100%;\n  height: 6vh;\n  border-radius: 5px;\n  border: thin solid lightgrey;\n  padding: 2%; }\n.mar-left-add {\n  margin-left: 11vw;\n  margin-bottom: 5vh; }\n.empty-label-height {\n  height: 2.5vh; }\n.flex-middle-respon {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 3vh;\n  margin-top: 2vh; }\n.form-container {\n  margin-bottom: 5vh; }\n/*title css*/\n@media all and (max-width: 768px) {\n  .mar-left-add {\n    margin-left: 0; }\n  .register-headline {\n    font-size: small; }\n  .flex-middle-respon {\n    display: inline-block;\n    margin-bottom: 3vh; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB;EACE,gCAAgC,EAAE;AACpC,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/url/escape.js":
/***/ (function(module, exports) {

module.exports = function escape(url) {
    if (typeof url !== 'string') {
        return url
    }
    // If url is already wrapped in quotes, remove them
    if (/^['"].*['"]$/.test(url)) {
        url = url.slice(1, -1);
    }
    // Should url be wrapped?
    // See https://drafts.csswg.org/css-values-3/#urls
    if (/["'() \t\n]/.test(url)) {
        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
    }

    return url
}


/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/actions/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["loginRequest"] = loginRequest;
/* harmony export (immutable) */ __webpack_exports__["loginSuccess"] = loginSuccess;
/* harmony export (immutable) */ __webpack_exports__["loginFailure"] = loginFailure;
/* harmony export (immutable) */ __webpack_exports__["loadLoginStatus"] = loadLoginStatus;
/* harmony export (immutable) */ __webpack_exports__["loginUser"] = loginUser;
/* harmony export (immutable) */ __webpack_exports__["logoutRequest"] = logoutRequest;
/* harmony export (immutable) */ __webpack_exports__["logoutSuccess"] = logoutSuccess;
/* harmony export (immutable) */ __webpack_exports__["logoutFailure"] = logoutFailure;
/* harmony export (immutable) */ __webpack_exports__["logoutUser"] = logoutUser;
/* harmony export (immutable) */ __webpack_exports__["initiateRequest"] = initiateRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateSuccess"] = initiateSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateFailure"] = initiateFailure;
/* harmony export (immutable) */ __webpack_exports__["initiate"] = initiate;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersRequest"] = initiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersSuccess"] = initiateUsersSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersFailure"] = initiateUsersFailure;
/* harmony export (immutable) */ __webpack_exports__["initiateUsers"] = initiateUsers;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserRequest"] = getCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserSuccess"] = getCurrentUserSuccess;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserFailure"] = getCurrentUserFailure;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUser"] = getCurrentUser;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditRequest"] = increaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditSuccess"] = increaseCreditSuccess;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditFailure"] = increaseCreditFailure;
/* harmony export (immutable) */ __webpack_exports__["increaseCredit"] = increaseCredit;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__ = __webpack_require__("./src/api-handlers/authentication.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function loginRequest(username, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["K" /* USER_LOGIN_REQUEST */],
    payload: {
      username,
      password
    }
  };
}

function loginSuccess(info) {
  console.log(info);
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["L" /* USER_LOGIN_SUCCESS */],
    payload: { info }
  };
}

function loginFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["J" /* USER_LOGIN_FAILURE */],
    payload: { error }
  };
}

function loadLoginStatus() {
  const loginInfo = Object(__WEBPACK_IMPORTED_MODULE_0__utils__["c" /* getLoginInfo */])();
  if (loginInfo) {
    return loginSuccess(loginInfo);
  }
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["I" /* USER_IS_NOT_LOGGED_IN */]
  };
}

function getCaptchaToken() {
  if (false) {
    const loginForm = $('#loginForm').get(0);
    if (loginForm && typeof loginForm['g-recaptcha-response'] === 'object') {
      return loginForm['g-recaptcha-response'].value;
    }
  }
  return null;
}

function loginUser(username, password) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(loginRequest(username, password));
      try {
        // const captchaToken = getCaptchaToken();
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["c" /* sendLoginRequest */])(username, password);
        dispatch(loginSuccess(res));
        localStorage.setItem('loginInfo', JSON.stringify(res));
        // console.log(location.pathname);
        const { pathname } = getState().house;
        console.log(pathname);
        __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push(pathname);
      } catch (err) {
        dispatch(loginFailure(err));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}

function logoutRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["N" /* USER_LOGOUT_REQUEST */],
    payload: {}
  };
}

function logoutSuccess() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["O" /* USER_LOGOUT_SUCCESS */],
    payload: {
      message: 'خروج موفقیت آمیز'
    }
  };
}

function logoutFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["M" /* USER_LOGOUT_FAILURE */],
    payload: { error }
  };
}

function logoutUser() {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(logoutRequest());
      try {
        dispatch(logoutSuccess());
        localStorage.clear();
        location.assign('/login');
      } catch (err) {
        dispatch(logoutFailure(err));
      }
    });

    return function (_x3) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function initiateRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["q" /* GET_INIT_REQUEST */]
  };
}

function initiateSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["r" /* GET_INIT_SUCCESS */],
    payload: { response }
  };
}

function initiateFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["p" /* GET_INIT_FAILURE */],
    payload: { err }
  };
}

function initiate() {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["a" /* sendInitiateRequest */])();
        dispatch(initiateSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateFailure(error));
      }
    });

    return function (_x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function initiateUsersRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["t" /* GET_INIT_USER_REQUEST */]
  };
}

function initiateUsersSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["u" /* GET_INIT_USER_SUCCESS */],
    payload: { response }
  };
}

function initiateUsersFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["s" /* GET_INIT_USER_FAILURE */],
    payload: { err }
  };
}

function initiateUsers() {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateUsersRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["b" /* sendInitiateUsersRequest */])();
        dispatch(initiateUsersSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateUsersFailure(error));
      }
    });

    return function (_x5) {
      return _ref4.apply(this, arguments);
    };
  })();
}

function getCurrentUserRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["e" /* GET_CURR_USER_REQUEST */]
  };
}

function getCurrentUserSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["f" /* GET_CURR_USER_SUCCESS */],
    payload: { response }
  };
}

function getCurrentUserFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["d" /* GET_CURR_USER_FAILURE */],
    payload: { err }
  };
}

function getCurrentUser(userId) {
  return (() => {
    var _ref5 = _asyncToGenerator(function* (dispatch) {
      dispatch(getCurrentUserRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["d" /* sendgetCurrentUserRequest */])(userId);
        dispatch(getCurrentUserSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getCurrentUserFailure(error));
      }
    });

    return function (_x6) {
      return _ref5.apply(this, arguments);
    };
  })();
}

function increaseCreditRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["z" /* INCREASE_CREDIT_REQUEST */]
  };
}

function increaseCreditSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["A" /* INCREASE_CREDIT_SUCCESS */],
    payload: { response }
  };
}

function increaseCreditFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["y" /* INCREASE_CREDIT_FAILURE */],
    payload: { err }
  };
}

function increaseCredit(amount) {
  return (() => {
    var _ref6 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(increaseCreditRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["e" /* sendincreaseCreditRequest */])(amount, currUser.userId);
        dispatch(increaseCreditSuccess(res));
        dispatch(getCurrentUser(currUser.userId));
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["f" /* notifySuccess */])('افزایش اعتبار با موفقیت انجام شد.');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(increaseCreditFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref6.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/actions/house.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["createHouseRequest"] = createHouseRequest;
/* harmony export (immutable) */ __webpack_exports__["createHouseSuccess"] = createHouseSuccess;
/* harmony export (immutable) */ __webpack_exports__["createHouseFailure"] = createHouseFailure;
/* harmony export (immutable) */ __webpack_exports__["createHouse"] = createHouse;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatusRequest"] = getPhoneStatusRequest;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatusSuccess"] = getPhoneStatusSuccess;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatusFailure"] = getPhoneStatusFailure;
/* harmony export (immutable) */ __webpack_exports__["getPhoneStatus"] = getPhoneStatus;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetailRequest"] = getHouseDetailRequest;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetailSuccess"] = getHouseDetailSuccess;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetailFailure"] = getHouseDetailFailure;
/* harmony export (immutable) */ __webpack_exports__["getHouseDetail"] = getHouseDetail;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumberRequest"] = getHousePhoneNumberRequest;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumberSuccess"] = getHousePhoneNumberSuccess;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumberFailure"] = getHousePhoneNumberFailure;
/* harmony export (immutable) */ __webpack_exports__["getHousePhoneNumber"] = getHousePhoneNumber;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_handlers_house__ = __webpack_require__("./src/api-handlers/house.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils__ = __webpack_require__("./src/utils/index.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function createHouseRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["b" /* CREATE_HOUSE_REQUEST */]
  };
}

function createHouseSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["c" /* CREATE_HOUSE_SUCCESS */],
    payload: { response }
  };
}

function createHouseFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["a" /* CREATE_HOUSE_FAILURE */],
    payload: { err }
  };
}

function createHouse(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, imageURL) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(createHouseRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["a" /* sendcreateHouseRequest */])(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, imageURL);
        dispatch(createHouseSuccess(res));
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["f" /* notifySuccess */])('خانه با موفقیت ثبت شد');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(createHouseFailure(error));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}
function getPhoneStatusRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["w" /* GET_PHONE_STATUS_REQUEST */]
  };
}

function getPhoneStatusSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["x" /* GET_PHONE_STATUS_SUCCESS */],
    payload: { response }
  };
}

function getPhoneStatusFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["v" /* GET_PHONE_STATUS_FAILURE */],
    payload: { err }
  };
}

function getPhoneStatus(houseId) {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(getPhoneStatusRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["d" /* sendgetPhoneStatusRequest */])(houseId);
        dispatch(getPhoneStatusSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getPhoneStatusFailure(error));
      }
    });

    return function (_x3, _x4) {
      return _ref2.apply(this, arguments);
    };
  })();
}
function getHouseDetailRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["n" /* GET_HOUSE_REQUEST */]
  };
}

function getHouseDetailSuccess(response, pathname) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["o" /* GET_HOUSE_SUCCESS */],
    payload: { response, pathname }
  };
}

function getHouseDetailFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["j" /* GET_HOUSE_FAILURE */],
    payload: { err }
  };
}

function getHouseDetail(houseId, pathname) {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(getHouseDetailRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["b" /* sendgetHouseDetailRequest */])(houseId);
        dispatch(getHouseDetailSuccess(res, pathname));
        if (currUser) {
          dispatch(getPhoneStatus(houseId));
        }
      } catch (error) {
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        dispatch(getHouseDetailFailure(error));
      }
    });

    return function (_x5, _x6) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function getHousePhoneNumberRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["l" /* GET_HOUSE_PHONE_REQUEST */]
  };
}

function getHousePhoneNumberSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["m" /* GET_HOUSE_PHONE_SUCCESS */],
    payload: { response }
  };
}

function getHousePhoneNumberFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["k" /* GET_HOUSE_PHONE_FAILURE */],
    payload: { err }
  };
}

function getHousePhoneNumber(houseId) {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(getHousePhoneNumberRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_house__["c" /* sendgetHousePhoneNumberRequest */])(houseId);
        console.log(res);
        if (!res.hasOwnProperty('message') && res !== null) {
          dispatch(getHousePhoneNumberSuccess('payed'));
          dispatch(Object(__WEBPACK_IMPORTED_MODULE_1__actions_authentication__["getCurrentUser"])(currUser.userId));
        } else if (res.hasOwnProperty('message')) {
          dispatch(getHousePhoneNumberSuccess('no_credit'));
        }
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getHousePhoneNumberFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref4.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/api-handlers/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = sendincreaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["a"] = sendInitiateRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendInitiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendLoginRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_paths__ = __webpack_require__("./src/config/paths.js");



function sendincreaseCreditRequest(amount, id) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["g" /* increaseCreditUrl */])(amount, id), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendgetCurrentUserRequest(userId) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["b" /* getCurrentUserUrl */])(userId), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateUsersRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendLoginRequest(username, password) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["d" /* getPostConfig */])({
    username,
    password
  }));
}

/***/ }),

/***/ "./src/api-handlers/house.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = sendcreateHouseRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendgetHouseDetailRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendgetHousePhoneNumberRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetPhoneStatusRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_paths__ = __webpack_require__("./src/config/paths.js");



/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

function sendcreateHouseRequest(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, imageURL) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_2__config_paths__["a" /* getCreateHouseUrl */], Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])({
    area,
    buildingType,
    address,
    dealType,
    basePrice,
    rentPrice,
    sellPrice,
    phone,
    description,
    expireTime,
    imageURL
  }));
}

/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

function sendgetHouseDetailRequest(houseId) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_2__config_paths__["c" /* getHouseDetailUrl */])(houseId), Object(__WEBPACK_IMPORTED_MODULE_1__utils__["b" /* getGetConfig */])());
}

function sendgetHousePhoneNumberRequest(houseId) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_2__config_paths__["d" /* getHousePhoneUrl */])(houseId), Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])());
}

function sendgetPhoneStatusRequest(houseId) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_2__config_paths__["d" /* getHousePhoneUrl */])(houseId), Object(__WEBPACK_IMPORTED_MODULE_1__utils__["b" /* getGetConfig */])());
}

/***/ }),

/***/ "./src/components/Footer/200px-Instagram_logo_2016.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Instagram_logo_2016.svg.png?2fc2d5c1";

/***/ }),

/***/ "./src/components/Footer/200px-Telegram_logo.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Telegram_logo.svg.png?6fa2804c";

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__ = __webpack_require__("./src/components/Footer/Twitter_bird_logo_2012.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__ = __webpack_require__("./src/components/Footer/200px-Instagram_logo_2016.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__ = __webpack_require__("./src/components/Footer/200px-Telegram_logo.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Footer/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/Footer.js';
/* eslint-disable react/no-danger */








class Footer extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'footer',
      { className: 'site-footer center-column dark-green-background white', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'row', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4 text-align-left', __source: {
              fileName: _jsxFileName,
              lineNumber: 15
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'ul',
            { className: 'social-icons-list no-list-style', __source: {
                fileName: _jsxFileName,
                lineNumber: 16
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 17
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'instagram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 18
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 24
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'twitter-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 25
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 31
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'telegram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              })
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-8 text-align-right', __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 41
              },
              __self: this
            },
            '\u062A\u0645\u0627\u0645\u06CC \u062D\u0642\u0648\u0642 \u0627\u06CC\u0646 \u0648\u0628\u200C\u0633\u0627\u06CC\u062A \u0645\u062A\u0639\u0644\u0642 \u0628\u0647 \u0645\u0647\u0631\u0646\u0627\u0632 \u062B\u0627\u0628\u062A \u0648 \u0647\u0648\u0645\u0646 \u0634\u0631\u06CC\u0641 \u0632\u0627\u062F\u0647 \u0645\u06CC\u200C\u0628\u0627\u0634\u062F.'
          )
        )
      )
    );
  }
}

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Twitter_bird_logo_2012.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/Twitter_bird_logo_2012.svg.png?248bfa8d";

/***/ }),

/***/ "./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css__ = __webpack_require__("./node_modules/normalize.css/normalize.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_normalize_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Layout_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Footer__ = __webpack_require__("./src/components/Footer/Footer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Panel__ = __webpack_require__("./src/components/Panel/Panel.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.js';
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */









class Layout extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-md-12 col-xs-12', style: { padding: '0' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__Panel__["a" /* default */], { mainPanel: this.props.mainPanel, __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }),
      this.props.children,
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__Footer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      })
    );
  }
}

Layout.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool
};
Layout.defaultProps = {
  mainPanel: false
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_normalize_css___default.a, __WEBPACK_IMPORTED_MODULE_4__Layout_css___default.a)(Layout));

/***/ }),

/***/ "./src/components/Loader/Loader.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Loader_scss__ = __webpack_require__("./src/components/Loader/Loader.scss");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Loader_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Loader_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_gif__ = __webpack_require__("./src/components/Loader/loader.gif");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_gif___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__loader_gif__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Loader/Loader.js';






class Loader extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: this.props.loading ? 'loader loading' : 'loader', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: __WEBPACK_IMPORTED_MODULE_4__loader_gif___default.a, alt: 'loader gif', __source: {
            fileName: _jsxFileName,
            lineNumber: 15
          },
          __self: this
        })
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'loader-info', __source: {
            fileName: _jsxFileName,
            lineNumber: 17
          },
          __self: this
        },
        '\u0645\u0646\u062A\u0638\u0631 \u0628\u0627\u0634\u06CC\u062F'
      )
    );
  }
}

Loader.propTypes = {
  loading: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__Loader_scss___default.a)(Loader));

/***/ }),

/***/ "./src/components/Loader/Loader.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Loader/Loader.scss");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Loader/loader.gif":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Loader/loader.gif?be5c3223";

/***/ }),

/***/ "./src/components/NewHouseForm/NewHouseForm.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css__ = __webpack_require__("./src/components/NewHouseForm/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/NewHouseForm/NewHouseForm.js';
/* eslint-disable react/no-danger */





class NewHouseForm extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  constructor(props) {
    super(props);

    this.handleSubmit = e => {
      e.preventDefault();
      const errors = this.validate(this.state.buildingType, this.state.address, this.state.basePrice, this.state.rentPrice, this.state.sellPrice, this.state.phone, this.state.area);
      const isEnabled = !Object.keys(errors).some(x => errors[x]);
      if (isEnabled) {
        this.setState({ errors: false });
        const dealType = this.state.dealType === 'RENT' ? 1 : 0;
        let rentPrice = 0;
        let sellPrice = 0;
        let basePrice = 0;
        if (dealType) {
          sellPrice = 0;
          rentPrice = this.state.rentPrice;
          basePrice = this.state.basePrice;
        } else {
          rentPrice = 0;
          basePrice = 0;
          sellPrice = this.state.sellPrice;
        }
        this.props.handleSubmit(this.state.area, this.state.buildingType, this.state.address, dealType, basePrice, rentPrice, sellPrice, this.state.phone, this.state.description, '');
      } else {
        this.setState({ errors: true });
      }
    };

    this.handleKeyPress = e => {
      if (e.key === 'Enter') {
        this.handleSubmit(e);
      }
    };

    this.validate = (buildingType, address, basePrice, rentPrice, sellPrice, phone, area) => {
      // true means invalid, so our conditions got reversed
      const regex = new RegExp(/[^0-9]/, 'g');
      if (this.state.dealType === 'RENT') {
        return {
          buildingType: buildingType.length === 0,
          address: address.length === 0,
          basePrice: basePrice.match(regex),
          rentPrice: rentPrice.match(regex) || rentPrice.length === 0,
          sellPrice: false,
          phone: phone.match(regex) || phone.length === 0,
          area: area.match(regex) || area.length === 0
        };
      }
      return {
        buildingType: buildingType.length === 0,
        address: !address.match(regex) || address.length === 0,
        basePrice: false,
        rentPrice: false,
        sellPrice: sellPrice.match(regex) || sellPrice.length === 0,
        phone: phone.match(regex) || phone.length === 0,
        area: area.match(regex) || area.length === 0
      };
    };

    this.state = {
      area: '',
      buildingType: '',
      address: '',
      dealType: 'RENT',
      basePrice: '',
      rentPrice: '',
      sellPrice: '',
      phone: '',
      description: '',
      expireTime: '',
      errors: false
    };
  }

  render() {
    const errors = this.validate(this.state.buildingType, this.state.address, this.state.basePrice, this.state.rentPrice, this.state.sellPrice, this.state.phone, this.state.area);

    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'home-panel no-padding', __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'section',
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 121
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'search-theme flex-center', __source: {
              fileName: _jsxFileName,
              lineNumber: 122
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'container', __source: {
                fileName: _jsxFileName,
                lineNumber: 123
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'row ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 124
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 125
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'h3',
                  { className: 'text-align-right register-headline persian-bold blue-font flex-center', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 126
                    },
                    __self: this
                  },
                  '\u062B\u0628\u062A \u0645\u0644\u06A9 \u062C\u062F\u06CC\u062F \u062F\u0631 \u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                )
              )
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container-fluid form-container', __source: {
              fileName: _jsxFileName,
              lineNumber: 133
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row radio', __source: {
                fileName: _jsxFileName,
                lineNumber: 134
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container-fluid pull-right', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 135
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'label',
                { className: 'radio-inline  persian black-font radio-label', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 136
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  type: 'radio',
                  name: 'dealType',
                  id: 'inlineRadio1',
                  checked: this.state.dealType === 'RENT',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  value: 'RENT',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 137
                  },
                  __self: this
                }),
                ' ',
                '\u0631\u0647\u0646 \u0648 \u0627\u062C\u0627\u0631\u0647'
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'label',
                { className: 'radio-inline persian black-font radio-label', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 149
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  type: 'radio',
                  name: 'dealType',
                  id: 'inlineRadio2',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  value: 'SELL',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 150
                  },
                  __self: this
                }),
                ' ',
                '\u062E\u0631\u06CC\u062F'
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row flex-middle', __source: {
                fileName: _jsxFileName,
                lineNumber: 163
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-10 flex-middle-respon', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 164
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 165
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'label',
                  {
                    htmlFor: 'meter',
                    className: 'persian light-grey-font add-label',
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 166
                    },
                    __self: this
                  },
                  '\u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'meter',
                  type: 'text',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'area',

                  className: `text-align-right persian black-font add-input ${errors.area && this.state.errors ? 'error' : ''}`,
                  placeholder: '\u0645\u062A\u0631\u0627\u0698',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 172
                  },
                  __self: this
                }),
                errors.area && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 187
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0645\u062A\u0631\u0627\u0698 \u0631\u0627 \u0628\u0647 \u0639\u062F\u062F \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12  col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 190
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('label', {
                  htmlFor: 'type-inp',
                  className: 'persian light-grey-font add-label empty-label-height',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 191
                  },
                  __self: this
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'select',
                  {
                    id: 'type-inp',
                    onChange: e => {
                      this.setState({ [e.target.name]: e.target.value });
                    },
                    name: 'buildingType',

                    className: `persian text-align-right light-grey-font add-input pull-left ${errors.buildingType && this.state.errors ? 'error' : ''}`,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 195
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'option',
                    { disabled: true, selected: true, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 206
                      },
                      __self: this
                    },
                    '\u0646\u0648\u0639 \u0645\u0644\u06A9'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'option',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 209
                      },
                      __self: this
                    },
                    '\u0648\u06CC\u0644\u0627\u06CC\u06CC'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'option',
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 210
                      },
                      __self: this
                    },
                    '\u0622\u067E\u0627\u0631\u062A\u0645\u0627\u0646'
                  )
                ),
                errors.buildingType && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 214
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0646\u0648\u0639 \u0645\u0644\u06A9 \u0631\u0627 \u0627\u0646\u062A\u062E\u0627\u0628 \u06A9\u0646\u06CC\u062F.'
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row flex-middle', __source: {
                fileName: _jsxFileName,
                lineNumber: 219
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-10 flex-middle-respon', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 220
                },
                __self: this
              },
              this.state.dealType === 'RENT' && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 222
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'label',
                  {
                    htmlFor: 'price1',
                    className: 'persian light-grey-font add-label',
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 223
                    },
                    __self: this
                  },
                  '\u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'price1',
                  type: 'text',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'basePrice',

                  className: `persian text-align-right black-font add-input ${errors.basePrice && this.state.errors ? 'error' : ''}`,
                  placeholder: '\u0642\u06CC\u0645\u062A \u0631\u0647\u0646',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 229
                  },
                  __self: this
                }),
                errors.basePrice && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 244
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0642\u06CC\u0645\u062A \u0631\u0647\u0646 \u0631\u0627 \u0628\u0647 \u0639\u062F\u062F \u06A9\u0646\u06CC\u062F.'
                )
              ),
              this.state.dealType === 'SELL' && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 249
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'label',
                  {
                    htmlFor: 'price1',
                    className: 'persian light-grey-font add-label',
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 250
                    },
                    __self: this
                  },
                  '\u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'price1',
                  type: 'text',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'sellPrice',

                  className: `persian text-align-right black-font add-input ${errors.sellPrice && this.state.errors ? 'error' : ''}`,
                  placeholder: '\u0642\u06CC\u0645\u062A \u0641\u0631\u0648\u0634',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 256
                  },
                  __self: this
                }),
                errors.sellPrice && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 271
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0642\u06CC\u0645\u062A \u0641\u0631\u0648\u0634 \u0631\u0627 \u0628\u0647 \u0639\u062F\u062F \u06A9\u0646\u06CC\u062F.'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12  col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 277
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('label', {
                  htmlFor: 'address',
                  className: 'persian empty-label-height light-grey-font add-label',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 278
                  },
                  __self: this
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'address',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'address',
                  type: 'text',

                  className: `persian text-align-right black-font add-input ${errors.address && this.state.errors ? 'error' : ''}`,
                  placeholder: '\u0622\u062F\u0631\u0633',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 282
                  },
                  __self: this
                }),
                errors.address && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 297
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0622\u062F\u0631\u0633 \u0631\u0627 \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row flex-middle', __source: {
                fileName: _jsxFileName,
                lineNumber: 302
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-10 flex-middle-respon', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 303
                },
                __self: this
              },
              this.state.dealType === 'RENT' && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 305
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'label',
                  {
                    htmlFor: 'price2',
                    className: 'persian light-grey-font add-label',
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 306
                    },
                    __self: this
                  },
                  '\u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'price2',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'rentPrice',
                  type: 'text',

                  className: `persian text-align-right black-font add-input ${errors.rentPrice && this.state.errors ? 'error' : ''}`,
                  placeholder: '\u0642\u06CC\u0645\u062A \u0627\u062C\u0627\u0631\u0647',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 312
                  },
                  __self: this
                }),
                errors.rentPrice && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 327
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0642\u06CC\u0645\u062A \u0627\u062C\u0627\u0631\u0647 \u0631\u0627 \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12  col-md-5', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 331
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('label', {
                  htmlFor: 'phone',
                  className: 'persian empty-label-height light-grey-font add-label',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 332
                  },
                  __self: this
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'phone',
                  type: 'tel',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'phone',

                  className: `persian black-font  text-align-right  add-input ${errors.phone && this.state.errors ? 'error' : ''}`,
                  placeholder: '\u0634\u0645\u0627\u0631\u0647 \u062A\u0645\u0627\u0633',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 336
                  },
                  __self: this
                }),
                errors.phone && this.state.errors && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'p',
                  { className: 'err-msg', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 351
                    },
                    __self: this
                  },
                  '\u0644\u0637\u0641\u0627 \u0634\u0645\u0627\u0631\u0647 \u062A\u0645\u0627\u0633 \u0631\u0627 \u0628\u0647 \u0639\u062F\u062F \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row flex-middle', __source: {
                fileName: _jsxFileName,
                lineNumber: 358
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-10 flex-middle-respon', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 359
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 col-md-10 ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 360
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('label', {
                  htmlFor: 'expl',
                  className: 'persian light-grey-font add-label',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 361
                  },
                  __self: this
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                  id: 'expl',
                  type: 'text',
                  onChange: e => {
                    this.setState({ [e.target.name]: e.target.value });
                  },
                  name: 'description',
                  className: 'persian text-align-right black-font add-input',
                  placeholder: '\u062A\u0648\u0636\u06CC\u062D\u0627\u062A',
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 365
                  },
                  __self: this
                })
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row flex-middle', __source: {
                fileName: _jsxFileName,
                lineNumber: 378
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-10 flex-middle-respon', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 379
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-10 ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 380
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'button',
                  {
                    type: 'submit',
                    onClick: this.handleSubmit,
                    className: '  blue-button  text-align-center persian white-font',
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 381
                    },
                    __self: this
                  },
                  '\u062B\u0628\u062A \u0645\u0644\u06A9'
                )
              )
            )
          )
        )
      )
    );
  }
}

NewHouseForm.propTypes = {
  handleSubmit: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__main_css___default.a)(NewHouseForm));

/***/ }),

/***/ "./src/components/NewHouseForm/geometry2.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAMAAAC3Ycb+AAAAM1BMVEX5+vvr7O3y8/Tu7/D4+fns7e73+Pjt7/D29vf3+fnt7u/09fXx8vPv8PH19vfy8vPz9PUQ6VWYAAAH4klEQVR4Xu3d2XYTvxLF4doahx7f/2mPnUA6eJn/SaAdKeb33QC+yYKNqiR1W7J/DwAAAAAAAAAAAAAg2FDgfbFxoEi6RoJ18xfLXq2rOusbRVJWe5Cp6SefiOSjiuL8iEyC10X23kddOPsgIonSAzJJTYqu2lWZJc32MUQySzo9k5AlH+ynEj+cCJFM0rycnclyE0CK0scCJ5IgbRamUzMpUrZfJKmZDRLJbEPLamZ2ZLKfMkCS/cpJkw0SSbWhOSkdMyP5cMagW+5+9hHspCRpt6uwnDQ/LfdGw6JoH4IobxcpS3E9aczV+x9+BGYpmE1RyvWsInh32BT7CEzSapukOdgAgSBIS5biZIeeJQtZFy3ZoWtTxy5pCXboO+1Fkpwdui8MkYsdBt86Qf/NRfTffse4D6jQ/xEuHv+SA3gN6PEAAAAAAMC6LKsNA6suuiVS3iR7gUUX3vooOiTrjkCyDt7eULIm62KX5vJq+Vk3sXrfKY8QFYO9qlKzvrBJu/3kJGf94HZQhKZYrSN4qdx8N9P6QZGWOwH1gibV24S8dUbFGqZmoUrZbNCuzqy3/7wXx7rwqkY16wq7tNlPg+ydMNEaaIqFImX3KkvJesOiw2zdoerNj/4OAAAAAAAAAAAAAAAAAJvkg40Cky4WGwWcFCUbBXZdZBsFQpa02jAQpr1aRwAAAAAAAAAAAKhtsoHAS5t9Aoq7VexEe/xcInC65exMKQ712J9ALH32eDACsfck94CLU2wcBGJeqvYQayWQP7BKuz1AmBVXAvkDTd7Ol5ou5kAgnzZLj3mPtknKiUA+a5KSnSt4SVtIWZ+vhwRSpGKnKlGv/SPMkpZgXRHIJslXe7FGKRbrh0BqluRu/9gLPWSKUis3o1y5WgfMso6mcaeldMA6pElxt1thUZ9dZVbqTTH9ZqTv7PZ+TJbCmROs+sHP7yOQmjXbadJvRkJT5onhR7lg57n/L596tRDcr02blKwH3J8iNDXrA1F5pIqF+U7N2vtVLKzSZDdyx4qFqMV+VXtWLMxSGLliUbP6Vyxq1kgVC7Pk3ut8+TZW3YrWEYJuzfYdAAAAwF0VGwX+6/UCEAgkf1ogYIQQCAAAAAAAAAAAAAAAAAAAAAAAAAAAQCrFxoC0L00XNgBU1/SDdYc66yp6tw9QsuCipLYlGwFSluRX+77W1Z7IGqU42Vd4VDnMyvY0pi+7/7U2KQc7X5W2p8pjsi8xP+oIll1K5PF5XhfezucV7UmkeOTxbUdIkGZ7DqEdeXzfHjJJqz2H7Yv/b5VkD7BI9hyS1MJz3ADyHLxUnuSOnKeQJP8st0g9hfkJpu+n3SQc+m/lhecYIEnaT6rfnTOZnqD2nnj3+S7phEyovVntrJEW+2bSOlSskTcWm3Lds65y6NNCnH1/u5TOrBhOUq7WQXmODQeveOZ6ZpM0h17rqWIdDLuxGKQlq99Mx0nWwcAbi14XLRmBDLKxuB/PTnuopdh7bCzWwaY5bCzmYk+FxS1czDYUVPs0AAAAAAAAAAAAAAAAAMktXsp+W60/TE1vogvWH4fPKfrZ+dff7dYRJklxS/YiTE3SHKwXTLcB7FHKJDLQYWc1S7N1gRSlZDdCllbrAfnuyQ81qlkHmN4VpylLWl7jcX1O6EBTDMfc94UPZha6nB+GSXLHQY1xc3OUvF0sivblsLwNkPnH3Lf+aCpOqvbV8NZBquTDSyJFWl5/KfbFUN5at5PK/rocbGqdAsFRl7yaeekIxEnB0O0gDi//4wieVVo6NXXMvwSySX6dmjR1mvbCKx/RhBr1wvdfGFKyJslZarpYglnqvHVCINYUk1lxU+23uYjpmNuuUvyZQWpsv/c/rniT5KdkdZ0lZUPvA72nqDebdYH5/Y5VnaNeLMX6QLppFmV3rgTDmJeNgOt4sEmLjQPh/7zy45J9KYT4XwcNz4odEuHNrJzsnuo1dCB7sIdbvV87VC25cP9C9jZwHpPi/vifcfHlicySoqv2XtibjmI2JC+plYf/jB7TntJ0sezJXqVp0UWcbGhTlOTrEwZyfGWnee/zt/nmTnCSylOVrMM6Rx20TPYNFMk/uqkvq/WSduev3F7se2hStWFgH+pJAUJUDDYMbFIrNgy4KMmPEwnqrIu52lCIpNgwMP1+IQ0mWpil3YaBImUbB8KsZCNBtREBAAAAAAAAAAAAAAAAAIBarmwUcLqyu0AgcJL/ZCBghBAImGUBAAAAAAAAAAAAAAAAkrL3fnbOTaVU6wz6hbM/EuwskPzV3wXSTksE70JIf1qykpydiED+1i5VGweBzJK3cRBIlrTaKAgk6GKcvk4gRfI6q6/7q4lA/oaT6mm3qOrKEcjfWBTPu2dYav9aIKGkcwOJWsyWP+3rBFL0k7/4+0DqS6Y1qgVK1t8FolMK9iqV107iaOp/GMjsrjZ/yt9+k8Jf3I6OSUp2Iq9sF3/a1+EkO5M028XR17sikCR5txY7+npXBLLrVfRzHmB+RCBl9jp07OsEcghlcovXlbe+COQ2l2qDSvvmXyxurYMH8vzWJeq9to2TyaJm/5bgol5kf9X0whcbg//X6vxrHMuejtK6NV34ZCPY8j8VSMqS2h5uP551sdkXwyQpTnZHnSXlal8Js6Qt2H2lSTHZF8KsuNpvhfmrE8GW7L9sUrOBYPrRRQAAAAAAAAAAAP4Hwd4wVc8Xf9oAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/components/NewHouseForm/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/NewHouseForm/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/NewHouseForm/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/NewHouseForm/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Panel/Panel.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Panel/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png__ = __webpack_require__("./src/components/Panel/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__history__ = __webpack_require__("./src/history.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/Panel.js';
/* eslint-disable react/no-danger */










class Panel extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {

  render() {
    console.log(this.props.currUser);
    console.log(this.props.isLoggedIn);
    if (this.props.mainPanel) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'header',
        { className: 'hidden-xs', __source: {
            fileName: _jsxFileName,
            lineNumber: 25
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container-fluid', __source: {
              fileName: _jsxFileName,
              lineNumber: 26
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row header-main', __source: {
                fileName: _jsxFileName,
                lineNumber: 27
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 28
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'container', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row flex-center header-main-boarder', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9 ', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 31
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h5',
                      { className: 'text-align-right white-font persian ', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 32
                        },
                        __self: this
                      },
                      '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                    ),
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'dropdown-content', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 35
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'container-fluid', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 36
                          },
                          __self: this
                        },
                        this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          {
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 38
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 39
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'h4',
                              { className: 'persian text-align-right black-font ', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 40
                                },
                                __self: this
                              },
                              this.props.currUser.username
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 44
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-left persian light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 45
                                },
                                __self: this
                              },
                              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                                'span',
                                { className: 'light-grey-font', __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 46
                                  },
                                  __self: this
                                },
                                '\u062A\u0648\u0645\u0627\u0646',
                                this.props.currUser.balance
                              )
                            ),
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 light-grey-font pull-right  persian text-align-right no-padding', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 51
                                },
                                __self: this
                              },
                              '\u0627\u0639\u062A\u0628\u0627\u0631'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 55
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                              onClick: () => {
                                __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 56
                              },
                              __self: this
                            })
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 65
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u062E\u0631\u0648\u062C',
                              onClick: () => {
                                this.props.authActions.logoutUser();
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 66
                              },
                              __self: this
                            })
                          )
                        ),
                        !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 78
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0648\u0631\u0648\u062F',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 79
                            },
                            __self: this
                          })
                        )
                      )
                    )
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding  col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 93
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x white-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 94
                      },
                      __self: this
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'header',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid', __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row nav-cl', __source: {
              fileName: _jsxFileName,
              lineNumber: 107
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown', __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 109
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row flex-center ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 110
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'col-xs-9 ', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 111
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h5',
                    { className: 'text-align-right persian purple-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 112
                      },
                      __self: this
                    },
                    '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'dropdown-content', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 115
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'container-fluid', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 116
                        },
                        __self: this
                      },
                      this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        {
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 118
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 119
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'h4',
                            { className: 'black-font persian text-align-right blue-font', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 120
                              },
                              __self: this
                            },
                            this.props.currUser.username
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 124
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: ' light-grey-font col-xs-6 pull-left blue-font persian', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 125
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'span',
                              { className: 'light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 126
                                },
                                __self: this
                              },
                              '\u062A\u0648\u0645\u0627\u0646',
                              this.props.currUser.balance
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'light-grey-font col-xs-6 pull-right blue-font persian text-align-right no-padding', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 131
                              },
                              __self: this
                            },
                            '\u0627\u0639\u062A\u0628\u0627\u0631'
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 135
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                            className: 'white-font  blue-button persian text-align-center',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                            },
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 136
                            },
                            __self: this
                          })
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 145
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u062E\u0631\u0648\u062C',
                            onClick: () => {
                              this.props.authActions.logoutUser();
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 146
                            },
                            __self: this
                          })
                        )
                      ),
                      !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'row mar-top', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 158
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                          type: 'button',
                          value: '\u0648\u0631\u0648\u062F',
                          onClick: () => {
                            __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                          },
                          className: '  white-font  blue-button persian text-align-center',
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 159
                          },
                          __self: this
                        })
                      )
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'no-padding  col-xs-3', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 173
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x purple-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 174
                    },
                    __self: this
                  })
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-4 col-md-offset-6 nav-logo col-xs-12', __source: {
                fileName: _jsxFileName,
                lineNumber: 179
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 180
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row  ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 181
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'a',
                  {
                    className: 'flex-row-rev',
                    onClick: () => {
                      __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/');
                    },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 182
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding text-align-center col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 188
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { className: 'icon-cl', src: __WEBPACK_IMPORTED_MODULE_7__logo_png___default.a, alt: 'logo', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 189
                      },
                      __self: this
                    })
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 191
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian-bold text-align-right  persian blue-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 192
                        },
                        __self: this
                      },
                      '\u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                    )
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}
Panel.propTypes = {
  currUser: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired,
  isLoggedIn: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    authActions: Object(__WEBPACK_IMPORTED_MODULE_3_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser, isLoggedIn } = authentication;
  return {
    currUser,
    isLoggedIn
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Panel)));

/***/ }),

/***/ "./src/components/Panel/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Panel/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/config/compressionConfig.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// noinspection JSAnnotator
const levels = [{
  MAX_WIDTH: 600,
  MAX_HEIGHT: 600,
  DOCUMENT_SIZE: 300000
}, {
  MAX_WIDTH: 800,
  MAX_HEIGHT: 800,
  DOCUMENT_SIZE: 500000
}];

/* harmony default export */ __webpack_exports__["a"] = ({
  getLevel(level) {
    return levels[level - 1];
  }
});

/***/ }),

/***/ "./src/config/paths.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__urls__ = __webpack_require__("./src/config/urls.js");
/* eslint-disable prettier/prettier */


const loginApiUrl = `http://localhost:8080/Turtle/login`;
/* harmony export (immutable) */ __webpack_exports__["h"] = loginApiUrl;

const logoutApiUrl = `http://localhost:8080/Turtle/login`;
/* unused harmony export logoutApiUrl */

const getSearchHousesUrl = `http://localhost:8080/Turtle/SearchServlet`;
/* harmony export (immutable) */ __webpack_exports__["f"] = getSearchHousesUrl;

const getCreateHouseUrl = `http://localhost:8080/Turtle/api/HouseServlet`;
/* harmony export (immutable) */ __webpack_exports__["a"] = getCreateHouseUrl;

const getInitiateUrl = `http://localhost:8080/Turtle/home`;
/* harmony export (immutable) */ __webpack_exports__["e"] = getInitiateUrl;

const getCurrentUserUrl = userId => `http://localhost:8080/Turtle/api/IndividualServlet?userId=${userId}`;
/* harmony export (immutable) */ __webpack_exports__["b"] = getCurrentUserUrl;

const getHouseDetailUrl = houseId => `http://localhost:8080/Turtle/api/GetSingleHouse?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["c"] = getHouseDetailUrl;

const getHousePhoneUrl = houseId => `http://localhost:8080/Turtle/api/PaymentServlet?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["d"] = getHousePhoneUrl;

const increaseCreditUrl = amount => `http://localhost:8080/Turtle/api/CreditServlet?value=${amount}`;
/* harmony export (immutable) */ __webpack_exports__["g"] = increaseCreditUrl;


const searchDriverUrl = phoneNumber => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/phoneNumber/${phoneNumber}`;
/* unused harmony export searchDriverUrl */

const getDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export getDriverAcquisitionInfoUrl */

const updateDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export updateDriverAcquisitionInfoUrl */

const updateDriverDocsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export updateDriverDocsUrl */

const approveDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/approve/${driverId}`;
/* unused harmony export approveDriverUrl */

const rejectDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/reject/${driverId}`;
/* unused harmony export rejectDriverUrl */

const getDriverDocumentUrl = documentId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/document/${documentId}`;
/* unused harmony export getDriverDocumentUrl */

const getDriverDocumentIdsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export getDriverDocumentIdsUrl */


/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__ = __webpack_require__("history/createBrowserHistory");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__);
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */



// NavigationBar manager, e.g. history.push('/home')
// https://github.com/mjackson/history
/* harmony default export */ __webpack_exports__["a"] = (false && __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default()());

/***/ }),

/***/ "./src/routes/houses/new/NewHouseContainer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_house__ = __webpack_require__("./src/actions/house.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_NewHouseForm__ = __webpack_require__("./src/components/NewHouseForm/NewHouseForm.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_Loader__ = __webpack_require__("./src/components/Loader/Loader.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/houses/new/NewHouseContainer.js';








class NewHouseContainer extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.handleSubmit = (area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime) => {
      this.props.houseActions.createHouse(area, buildingType, address, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime, 'http://uupload.ir/files/jzmo_no-pic.jpg');
    }, _temp;
  }

  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__components_NewHouseForm__["a" /* default */], { handleSubmit: this.handleSubmit, __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_Loader__["a" /* default */], { loading: this.props.isFetching, __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      })
    );
  }
}

NewHouseContainer.propTypes = {
  houseActions: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
  isFetching: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    houseActions: Object(__WEBPACK_IMPORTED_MODULE_2_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_4__actions_house__, dispatch)
  };
}

function mapStateToProps(state) {
  const { house } = state;
  const { isFetching } = house;
  return {
    isFetching
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(NewHouseContainer));

/***/ }),

/***/ "./src/routes/houses/new/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__NewHouseContainer__ = __webpack_require__("./src/routes/houses/new/NewHouseContainer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Layout__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/houses/new/index.js';




const title = 'اضافه کردن خانه';

function action() {
  return {
    chunks: ['new-house'],
    title,
    component: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_2__components_Layout__["a" /* default */],
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__NewHouseContainer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      }),
      ','
    )
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/utils/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export compressFile */
/* harmony export (immutable) */ __webpack_exports__["c"] = getLoginInfo;
/* harmony export (immutable) */ __webpack_exports__["d"] = getPostConfig;
/* unused harmony export getPutConfig */
/* harmony export (immutable) */ __webpack_exports__["b"] = getGetConfig;
/* unused harmony export convertNumbersToPersian */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchApi; });
/* unused harmony export setImmediate */
/* unused harmony export setCookie */
/* unused harmony export getCookie */
/* unused harmony export eraseCookie */
/* harmony export (immutable) */ __webpack_exports__["e"] = notifyError;
/* harmony export (immutable) */ __webpack_exports__["f"] = notifySuccess;
/* unused harmony export reloadSelectPicker */
/* unused harmony export loadSelectPicker */
/* unused harmony export getFormData */
/* unused harmony export loadUserLoginStatus */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js__ = __webpack_require__("exif-js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_exif_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__ = __webpack_require__("./src/config/compressionConfig.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

let processFile = (() => {
  var _ref = _asyncToGenerator(function* (dataURL, fileSize, fileType, documentType) {
    const image = new Image();
    image.src = dataURL;
    return new Promise(function (resolve, reject) {
      image.onload = function () {
        const imageMeasurements = getImageMeasurements(image);
        if (!shouldResize(imageMeasurements, fileSize, documentType)) {
          resolve(dataURLtoBlob(dataURL, fileType));
        }
        const scaledImageMeasurements = getScaledImageMeasurements(imageMeasurements.width, imageMeasurements.height, documentType);
        const scaledImageFile = getScaledImageFileFromCanvas(image, scaledImageMeasurements.width, scaledImageMeasurements.height, imageMeasurements.transform, fileType);
        resolve(scaledImageFile);
      };
      image.onerror = reject;
    });
  });

  return function processFile(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }







const compressionFirstLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(1);
const compressionSecondLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(2);
const documentsCompressionLevel = {
  profilePicture: compressionFirstLevel,
  driverLicence: compressionFirstLevel,
  carIdCard: compressionFirstLevel,
  insurance: compressionSecondLevel
};

function getCompressionLevel(documentType) {
  return documentsCompressionLevel[documentType] || compressionFirstLevel;
}

function dataURLtoBlob(dataURL, fileType) {
  const binary = atob(dataURL.split(',')[1]);
  const array = [];
  const length = binary.length;
  let i;
  for (i = 0; i < length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: fileType });
}

function getScaledImageMeasurements(width, height, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH } = getCompressionLevel(documentType);

  if (width > height) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  } else {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
  return {
    height,
    width
  };
}

function getImageOrientation(image) {
  let orientation = 0;
  __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getData(image, function () {
    orientation = __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getTag(this, 'Orientation');
  });
  return orientation;
}

function getImageMeasurements(image) {
  const orientation = getImageOrientation(image);
  let transform;
  let swap = false;
  switch (orientation) {
    case 8:
      swap = true;
      transform = 'left';
      break;
    case 6:
      swap = true;
      transform = 'right';
      break;
    case 3:
      transform = 'flip';
      break;
    default:
  }
  const width = swap ? image.height : image.width;
  const height = swap ? image.width : image.height;
  return {
    width,
    height,
    transform
  };
}

function getScaledImageFileFromCanvas(image, width, height, transform, fileType) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const context = canvas.getContext('2d');
  let transformParams;
  let swapMeasure = false;
  let imageWidth = width;
  let imageHeight = height;
  switch (transform) {
    case 'left':
      transformParams = [0, -1, 1, 0, 0, height];
      swapMeasure = true;
      break;
    case 'right':
      transformParams = [0, 1, -1, 0, width, 0];
      swapMeasure = true;
      break;
    case 'flip':
      transformParams = [1, 0, 0, -1, 0, height];
      break;
    default:
      transformParams = [1, 0, 0, 1, 0, 0];
  }
  context.setTransform(...transformParams);
  if (swapMeasure) {
    imageHeight = width;
    imageWidth = height;
  }
  context.drawImage(image, 0, 0, imageWidth, imageHeight);
  context.setTransform(1, 0, 0, 1, 0, 0);
  const dataURL = canvas.toDataURL(fileType);
  return dataURLtoBlob(dataURL, fileType);
}

function shouldResize(imageMeasurements, fileSize, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH, DOCUMENT_SIZE } = getCompressionLevel(documentType);
  if (documentType === 'insurance' && fileSize < DOCUMENT_SIZE) {
    return false;
  } else if (fileSize < DOCUMENT_SIZE) {
    return false;
  }
  return imageMeasurements.width > MAX_WIDTH || imageMeasurements.height > MAX_HEIGHT;
}

function compressFile(file, documentType = 'default') {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = _asyncToGenerator(function* () {
        const compressedFile = yield processFile(reader.result, file.size, file.type, documentType);
        resolve(compressedFile);
      });
    } catch (err) {
      reject(err);
    }
  });
}

function getLoginInfo() {
  try {
    const loginInfo = localStorage.getItem('loginInfo');
    if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isString(loginInfo)) {
      return JSON.parse(loginInfo);
    }
  } catch (err) {
    console.log('Error when parsing loginInfo object: ', err);
  }
  return null;
}
function getPostConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    mimeType: 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json', mimeType: 'application/json' };
  return _extends({
    method: 'POST',
    // mode: 'no-cors',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getPutConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    'X-Authorization': loginInfo.token
  } : { 'Content-Type': 'application/json' };
  return _extends({
    method: 'PUT',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getGetConfig() {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json' };
  return {
    method: 'GET',
    headers
  };
}

function convertNumbersToPersian(str) {
  const persianNumbers = '۰۱۲۳۴۵۶۷۸۹';
  let result = '';
  if (!str && str !== 0) {
    return result;
  }
  str = str.toString();
  let convertedChar;
  for (let i = 0; i < str.length; i++) {
    try {
      convertedChar = persianNumbers[str[i]];
    } catch (err) {
      console.log(err);
      convertedChar = str[i];
    }
    result += convertedChar;
  }
  return result;
}

let fetchApi = (() => {
  var _ref3 = _asyncToGenerator(function* (url, config) {
    let flowError;
    try {
      const res = yield fetch(url, config);
      console.log(res);
      console.log(res.status);
      let result = yield res.json();
      console.log(result);
      if (!__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.get(result, 'data'))) {
        result = result.data;
      }
      console.log(res.status);

      if (res.status !== 200) {
        flowError = result;
        console.log(res.status);

        if (res.status === 403) {
          localStorage.clear();
          flowError.message = 'لطفا دوباره وارد شوید!';
          console.log('yeees');
          setTimeout(function () {
            return __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push('/login');
          }, 3000);
        }
      } else {
        return result;
      }
    } catch (err) {
      console.log(err);
      console.debug(`Error when fetching api: ${url}`, err);
      flowError = {
        code: 'UNKNOWN_ERROR',
        message: 'خطای نامشخص لطفا دوباره سعی کنید'
      };
    }
    if (flowError) {
      throw flowError;
    }
  });

  return function fetchApi(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
})();

let setImmediate = (() => {
  var _ref4 = _asyncToGenerator(function* (fn) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        let value = null;
        if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isFunction(fn)) {
          value = fn();
        }
        resolve(value);
      }, 0);
    });
  });

  return function setImmediate(_x7) {
    return _ref4.apply(this, arguments);
  };
})();

function setCookie(name, value, days = 7) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
function getCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  const length = ca.length;
  let i = 0;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

function notifyError(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '10000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.error(message, 'خطا');
}

function notifySuccess(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.success(message, '');
}

function reloadSelectPicker() {
  try {
    $('.selectpicker').selectpicker('render');
  } catch (err) {
    console.log(err);
  }
}

function loadSelectPicker() {
  try {
    $('.selectpicker').selectpicker({});
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      $('.selectpicker').selectpicker('mobile');
    }
  } catch (err) {
    console.log(err);
  }
}

function getFormData(data) {
  if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(data)) {
    throw Error('Invalid input data to create a FormData object');
  }
  let formData;
  try {
    formData = new FormData();
    const keys = __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.keys(data);
    let i;
    const length = keys.length;
    for (i = 0; i < length; i++) {
      formData.append(keys[i], data[keys[i]]);
    }
    return formData;
  } catch (err) {
    console.debug(err);
    throw Error('FORM_DATA_BROWSER_SUPPORT_FAILURE');
  }
}

function loadUserLoginStatus(store) {
  const loginStatusAction = Object(__WEBPACK_IMPORTED_MODULE_4__actions_authentication__["loadLoginStatus"])();
  store.dispatch(loginStatusAction);
}

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL25ldy1ob3VzZS5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL05ld0hvdXNlRm9ybS9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi91cmwvZXNjYXBlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3M/ODIxYiIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2FjdGlvbnMvYXV0aGVudGljYXRpb24uanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hY3Rpb25zL2hvdXNlLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYXBpLWhhbmRsZXJzL2hvdXNlLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzcz85YjRlIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzPzUwNzkiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzPzY1NjUiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9sb2FkZXIuZ2lmIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9OZXdIb3VzZUZvcm0vTmV3SG91c2VGb3JtLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9OZXdIb3VzZUZvcm0vZ2VvbWV0cnkyLnBuZyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9OZXdIb3VzZUZvcm0vbWFpbi5jc3M/M2RmYSIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvUGFuZWwuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzPzFjZjQiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb25maWcvY29tcHJlc3Npb25Db25maWcuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb25maWcvcGF0aHMuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9oaXN0b3J5LmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvcm91dGVzL2hvdXNlcy9uZXcvTmV3SG91c2VDb250YWluZXIuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9yb3V0ZXMvaG91c2VzL25ldy9pbmRleC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL3V0aWxzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi8qISBub3JtYWxpemUuY3NzIHY3LjAuMCB8IE1JVCBMaWNlbnNlIHwgZ2l0aHViLmNvbS9uZWNvbGFzL25vcm1hbGl6ZS5jc3MgKi9cXG4vKiBEb2N1bWVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgbGluZSBoZWlnaHQgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIFByZXZlbnQgYWRqdXN0bWVudHMgb2YgZm9udCBzaXplIGFmdGVyIG9yaWVudGF0aW9uIGNoYW5nZXMgaW5cXG4gKiAgICBJRSBvbiBXaW5kb3dzIFBob25lIGFuZCBpbiBpT1MuXFxuICovXFxuaHRtbCB7XFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIC1tcy10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqLyB9XFxuLyogU2VjdGlvbnNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIFJlbW92ZSB0aGUgbWFyZ2luIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqL1xcbmJvZHkge1xcbiAgbWFyZ2luOiAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXJ0aWNsZSxcXG5hc2lkZSxcXG5mb290ZXIsXFxuaGVhZGVyLFxcbm5hdixcXG5zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgZm9udCBzaXplIGFuZCBtYXJnaW4gb24gYGgxYCBlbGVtZW50cyB3aXRoaW4gYHNlY3Rpb25gIGFuZFxcbiAqIGBhcnRpY2xlYCBjb250ZXh0cyBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBTYWZhcmkuXFxuICovXFxuaDEge1xcbiAgZm9udC1zaXplOiAyZW07XFxuICBtYXJnaW46IDAuNjdlbSAwOyB9XFxuLyogR3JvdXBpbmcgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxuZmlnY2FwdGlvbixcXG5maWd1cmUsXFxubWFpbiB7XFxuICAvKiAxICovXFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBtYXJnaW4gaW4gSUUgOC5cXG4gKi9cXG5maWd1cmUge1xcbiAgbWFyZ2luOiAxZW0gNDBweDsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIEZpcmVmb3guXFxuICogMi4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZSBhbmQgSUUuXFxuICovXFxuaHIge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAvKiAxICovXFxuICBoZWlnaHQ6IDA7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnByZSB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qIFRleHQtbGV2ZWwgc2VtYW50aWNzXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGdyYXkgYmFja2dyb3VuZCBvbiBhY3RpdmUgbGlua3MgaW4gSUUgMTAuXFxuICogMi4gUmVtb3ZlIGdhcHMgaW4gbGlua3MgdW5kZXJsaW5lIGluIGlPUyA4KyBhbmQgU2FmYXJpIDgrLlxcbiAqL1xcbmEge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAvKiAxICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbi1za2lwOiBvYmplY3RzO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBib3R0b20gYm9yZGVyIGluIENocm9tZSA1Ny0gYW5kIEZpcmVmb3ggMzktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB0ZXh0IGRlY29yYXRpb24gaW4gQ2hyb21lLCBFZGdlLCBJRSwgT3BlcmEsIGFuZCBTYWZhcmkuXFxuICovXFxuYWJiclt0aXRsZV0ge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gIC8qIDEgKi9cXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFByZXZlbnQgdGhlIGR1cGxpY2F0ZSBhcHBsaWNhdGlvbiBvZiBgYm9sZGVyYCBieSB0aGUgbmV4dCBydWxlIGluIFNhZmFyaSA2LlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBpbmhlcml0OyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgd2VpZ2h0IGluIENocm9tZSwgRWRnZSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogYm9sZGVyOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuY29kZSxcXG5rYmQsXFxuc2FtcCB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHN0eWxlIGluIEFuZHJvaWQgNC4zLS5cXG4gKi9cXG5kZm4ge1xcbiAgZm9udC1zdHlsZTogaXRhbGljOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGJhY2tncm91bmQgYW5kIGNvbG9yIGluIElFIDktLlxcbiAqL1xcbm1hcmsge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmMDtcXG4gIGNvbG9yOiAjMDAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc21hbGwge1xcbiAgZm9udC1zaXplOiA4MCU7IH1cXG4vKipcXG4gKiBQcmV2ZW50IGBzdWJgIGFuZCBgc3VwYCBlbGVtZW50cyBmcm9tIGFmZmVjdGluZyB0aGUgbGluZSBoZWlnaHQgaW5cXG4gKiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3ViLFxcbnN1cCB7XFxuICBmb250LXNpemU6IDc1JTtcXG4gIGxpbmUtaGVpZ2h0OiAwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lOyB9XFxuc3ViIHtcXG4gIGJvdHRvbTogLTAuMjVlbTsgfVxcbnN1cCB7XFxuICB0b3A6IC0wLjVlbTsgfVxcbi8qIEVtYmVkZGVkIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmF1ZGlvLFxcbnZpZGVvIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGlPUyA0LTcuXFxuICovXFxuYXVkaW86bm90KFtjb250cm9sc10pIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBoZWlnaHQ6IDA7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGJvcmRlciBvbiBpbWFnZXMgaW5zaWRlIGxpbmtzIGluIElFIDEwLS5cXG4gKi9cXG5pbWcge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lOyB9XFxuLyoqXFxuICogSGlkZSB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICovXFxuc3ZnOm5vdCg6cm9vdCkge1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjsgfVxcbi8qIEZvcm1zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDaGFuZ2UgdGhlIGZvbnQgc3R5bGVzIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqIDIuIFJlbW92ZSB0aGUgbWFyZ2luIGluIEZpcmVmb3ggYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQsXFxub3B0Z3JvdXAsXFxuc2VsZWN0LFxcbnRleHRhcmVhIHtcXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICBtYXJnaW46IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBTaG93IHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKiAxLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCB7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRWRnZSwgRmlyZWZveCwgYW5kIElFLlxcbiAqIDEuIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b24sXFxuc2VsZWN0IHtcXG4gIC8qIDEgKi9cXG4gIHRleHQtdHJhbnNmb3JtOiBub25lOyB9XFxuLyoqXFxuICogMS4gUHJldmVudCBhIFdlYktpdCBidWcgd2hlcmUgKDIpIGRlc3Ryb3lzIG5hdGl2ZSBgYXVkaW9gIGFuZCBgdmlkZW9gXFxuICogICAgY29udHJvbHMgaW4gQW5kcm9pZCA0LlxcbiAqIDIuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmh0bWwgW3R5cGU9XFxcImJ1dHRvblxcXCJdLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgYm9yZGVyIGFuZCBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qKlxcbiAqIFJlc3RvcmUgdGhlIGZvY3VzIHN0eWxlcyB1bnNldCBieSB0aGUgcHJldmlvdXMgcnVsZS5cXG4gKi9cXG5idXR0b246LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTotbW96LWZvY3VzcmluZyB7XFxuICBvdXRsaW5lOiAxcHggZG90dGVkIEJ1dHRvblRleHQ7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuZmllbGRzZXQge1xcbiAgcGFkZGluZzogMC4zNWVtIDAuNzVlbSAwLjYyNWVtOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgdGV4dCB3cmFwcGluZyBpbiBFZGdlIGFuZCBJRS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBjb2xvciBpbmhlcml0YW5jZSBmcm9tIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gSUUuXFxuICogMy4gUmVtb3ZlIHRoZSBwYWRkaW5nIHNvIGRldmVsb3BlcnMgYXJlIG5vdCBjYXVnaHQgb3V0IHdoZW4gdGhleSB6ZXJvIG91dFxcbiAqICAgIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmxlZ2VuZCB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBjb2xvcjogaW5oZXJpdDtcXG4gIC8qIDIgKi9cXG4gIGRpc3BsYXk6IHRhYmxlO1xcbiAgLyogMSAqL1xcbiAgbWF4LXdpZHRoOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDMgKi9cXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XFxuICAvKiAxICovIH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdmVydGljYWwgYWxpZ25tZW50IGluIENocm9tZSwgRmlyZWZveCwgYW5kIE9wZXJhLlxcbiAqL1xcbnByb2dyZXNzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIC8qIDEgKi9cXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgZGVmYXVsdCB2ZXJ0aWNhbCBzY3JvbGxiYXIgaW4gSUUuXFxuICovXFxudGV4dGFyZWEge1xcbiAgb3ZlcmZsb3c6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBJRSAxMC0uXFxuICogMi4gUmVtb3ZlIHRoZSBwYWRkaW5nIGluIElFIDEwLS5cXG4gKi9cXG5bdHlwZT1cXFwiY2hlY2tib3hcXFwiXSxcXG5bdHlwZT1cXFwicmFkaW9cXFwiXSB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgY3Vyc29yIHN0eWxlIG9mIGluY3JlbWVudCBhbmQgZGVjcmVtZW50IGJ1dHRvbnMgaW4gQ2hyb21lLlxcbiAqL1xcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixcXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xcbiAgaGVpZ2h0OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgb2RkIGFwcGVhcmFuY2UgaW4gQ2hyb21lIGFuZCBTYWZhcmkuXFxuICogMi4gQ29ycmVjdCB0aGUgb3V0bGluZSBzdHlsZSBpbiBTYWZhcmkuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4dGZpZWxkO1xcbiAgLyogMSAqL1xcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIHBhZGRpbmcgYW5kIGNhbmNlbCBidXR0b25zIGluIENocm9tZSBhbmQgU2FmYXJpIG9uIG1hY09TLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtY2FuY2VsLWJ1dHRvbixcXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKiAyLiBDaGFuZ2UgZm9udCBwcm9wZXJ0aWVzIHRvIGBpbmhlcml0YCBpbiBTYWZhcmkuXFxuICovXFxuOjotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQ6IGluaGVyaXQ7XFxuICAvKiAyICovIH1cXG4vKiBJbnRlcmFjdGl2ZVxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBFZGdlLCBJRSwgYW5kIEZpcmVmb3guXFxuICovXFxuZGV0YWlscyxcXG5tZW51IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3VtbWFyeSB7XFxuICBkaXNwbGF5OiBsaXN0LWl0ZW07IH1cXG4vKiBTY3JpcHRpbmdcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmNhbnZhcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG50ZW1wbGF0ZSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuLyogSGlkZGVuXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSAxMC0uXFxuICovXFxuW2hpZGRlbl0ge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsNEVBQTRFO0FBQzVFO2dGQUNnRjtBQUNoRjs7OztHQUlHO0FBQ0g7RUFDRSxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLDJCQUEyQjtFQUMzQixPQUFPO0VBQ1AsK0JBQStCO0VBQy9CLE9BQU8sRUFBRTtBQUNYO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIO0VBQ0UsVUFBVSxFQUFFO0FBQ2Q7O0dBRUc7QUFDSDs7Ozs7O0VBTUUsZUFBZSxFQUFFO0FBQ25COzs7R0FHRztBQUNIO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFFO0FBQ3JCO2dGQUNnRjtBQUNoRjs7O0dBR0c7QUFDSDs7O0VBR0UsT0FBTztFQUNQLGVBQWUsRUFBRTtBQUNuQjs7R0FFRztBQUNIO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7OztHQUdHO0FBQ0g7RUFDRSxnQ0FBZ0M7VUFDeEIsd0JBQXdCO0VBQ2hDLE9BQU87RUFDUCxVQUFVO0VBQ1YsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDtFQUNFLGtDQUFrQztFQUNsQyxPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU8sRUFBRTtBQUNYO2dGQUNnRjtBQUNoRjs7O0dBR0c7QUFDSDtFQUNFLDhCQUE4QjtFQUM5QixPQUFPO0VBQ1Asc0NBQXNDO0VBQ3RDLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIO0VBQ0Usb0JBQW9CO0VBQ3BCLE9BQU87RUFDUCwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLDBDQUEwQztVQUNsQyxrQ0FBa0M7RUFDMUMsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDs7RUFFRSxxQkFBcUIsRUFBRTtBQUN6Qjs7R0FFRztBQUNIOztFQUVFLG9CQUFvQixFQUFFO0FBQ3hCOzs7R0FHRztBQUNIOzs7RUFHRSxrQ0FBa0M7RUFDbEMsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIO0VBQ0UsbUJBQW1CLEVBQUU7QUFDdkI7O0dBRUc7QUFDSDtFQUNFLHVCQUF1QjtFQUN2QixZQUFZLEVBQUU7QUFDaEI7O0dBRUc7QUFDSDtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDs7RUFFRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQix5QkFBeUIsRUFBRTtBQUM3QjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsWUFBWSxFQUFFO0FBQ2hCO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIOztFQUVFLHNCQUFzQixFQUFFO0FBQzFCOztHQUVHO0FBQ0g7RUFDRSxjQUFjO0VBQ2QsVUFBVSxFQUFFO0FBQ2Q7O0dBRUc7QUFDSDtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCOztHQUVHO0FBQ0g7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7Ozs7O0VBS0Usd0JBQXdCO0VBQ3hCLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsVUFBVTtFQUNWLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIOztFQUVFLE9BQU87RUFDUCxrQkFBa0IsRUFBRTtBQUN0Qjs7O0dBR0c7QUFDSDs7RUFFRSxPQUFPO0VBQ1AscUJBQXFCLEVBQUU7QUFDekI7Ozs7R0FJRztBQUNIOzs7O0VBSUUsMkJBQTJCO0VBQzNCLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7Ozs7RUFJRSxtQkFBbUI7RUFDbkIsV0FBVyxFQUFFO0FBQ2Y7O0dBRUc7QUFDSDs7OztFQUlFLCtCQUErQixFQUFFO0FBQ25DOztHQUVHO0FBQ0g7RUFDRSwrQkFBK0IsRUFBRTtBQUNuQzs7Ozs7R0FLRztBQUNIO0VBQ0UsK0JBQStCO1VBQ3ZCLHVCQUF1QjtFQUMvQixPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU87RUFDUCxlQUFlO0VBQ2YsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixPQUFPO0VBQ1AsV0FBVztFQUNYLE9BQU87RUFDUCxvQkFBb0I7RUFDcEIsT0FBTyxFQUFFO0FBQ1g7OztHQUdHO0FBQ0g7RUFDRSxzQkFBc0I7RUFDdEIsT0FBTztFQUNQLHlCQUF5QjtFQUN6QixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIO0VBQ0UsZUFBZSxFQUFFO0FBQ25COzs7R0FHRztBQUNIOztFQUVFLCtCQUErQjtVQUN2Qix1QkFBdUI7RUFDL0IsT0FBTztFQUNQLFdBQVc7RUFDWCxPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOztFQUVFLGFBQWEsRUFBRTtBQUNqQjs7O0dBR0c7QUFDSDtFQUNFLDhCQUE4QjtFQUM5QixPQUFPO0VBQ1AscUJBQXFCO0VBQ3JCLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7O0VBRUUseUJBQXlCLEVBQUU7QUFDN0I7OztHQUdHO0FBQ0g7RUFDRSwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLGNBQWM7RUFDZCxPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7O0VBRUUsZUFBZSxFQUFFO0FBQ25COztHQUVHO0FBQ0g7RUFDRSxtQkFBbUIsRUFBRTtBQUN2QjtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDtFQUNFLHNCQUFzQixFQUFFO0FBQzFCOztHQUVHO0FBQ0g7RUFDRSxjQUFjLEVBQUU7QUFDbEI7Z0ZBQ2dGO0FBQ2hGOztHQUVHO0FBQ0g7RUFDRSxjQUFjLEVBQUVcIixcImZpbGVcIjpcIm5vcm1hbGl6ZS5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLyohIG5vcm1hbGl6ZS5jc3MgdjcuMC4wIHwgTUlUIExpY2Vuc2UgfCBnaXRodWIuY29tL25lY29sYXMvbm9ybWFsaXplLmNzcyAqL1xcbi8qIERvY3VtZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBsaW5lIGhlaWdodCBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gUHJldmVudCBhZGp1c3RtZW50cyBvZiBmb250IHNpemUgYWZ0ZXIgb3JpZW50YXRpb24gY2hhbmdlcyBpblxcbiAqICAgIElFIG9uIFdpbmRvd3MgUGhvbmUgYW5kIGluIGlPUy5cXG4gKi9cXG5odG1sIHtcXG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xcbiAgLyogMSAqL1xcbiAgLW1zLXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7XFxuICAvKiAyICovXFxuICAtd2Via2l0LXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7XFxuICAvKiAyICovIH1cXG4vKiBTZWN0aW9uc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogUmVtb3ZlIHRoZSBtYXJnaW4gaW4gYWxsIGJyb3dzZXJzIChvcGluaW9uYXRlZCkuXFxuICovXFxuYm9keSB7XFxuICBtYXJnaW46IDA7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5hcnRpY2xlLFxcbmFzaWRlLFxcbmZvb3RlcixcXG5oZWFkZXIsXFxubmF2LFxcbnNlY3Rpb24ge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBmb250IHNpemUgYW5kIG1hcmdpbiBvbiBgaDFgIGVsZW1lbnRzIHdpdGhpbiBgc2VjdGlvbmAgYW5kXFxuICogYGFydGljbGVgIGNvbnRleHRzIGluIENocm9tZSwgRmlyZWZveCwgYW5kIFNhZmFyaS5cXG4gKi9cXG5oMSB7XFxuICBmb250LXNpemU6IDJlbTtcXG4gIG1hcmdpbjogMC42N2VtIDA7IH1cXG4vKiBHcm91cGluZyBjb250ZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG5maWdjYXB0aW9uLFxcbmZpZ3VyZSxcXG5tYWluIHtcXG4gIC8qIDEgKi9cXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IG1hcmdpbiBpbiBJRSA4LlxcbiAqL1xcbmZpZ3VyZSB7XFxuICBtYXJnaW46IDFlbSA0MHB4OyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGJveCBzaXppbmcgaW4gRmlyZWZveC5cXG4gKiAyLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlIGFuZCBJRS5cXG4gKi9cXG5ociB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gIC8qIDEgKi9cXG4gIGhlaWdodDogMDtcXG4gIC8qIDEgKi9cXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxucHJlIHtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogMiAqLyB9XFxuLyogVGV4dC1sZXZlbCBzZW1hbnRpY3NcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIFJlbW92ZSB0aGUgZ3JheSBiYWNrZ3JvdW5kIG9uIGFjdGl2ZSBsaW5rcyBpbiBJRSAxMC5cXG4gKiAyLiBSZW1vdmUgZ2FwcyBpbiBsaW5rcyB1bmRlcmxpbmUgaW4gaU9TIDgrIGFuZCBTYWZhcmkgOCsuXFxuICovXFxuYSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gIC8qIDEgKi9cXG4gIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLXNraXA6IG9iamVjdHM7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGJvdHRvbSBib3JkZXIgaW4gQ2hyb21lIDU3LSBhbmQgRmlyZWZveCAzOS0uXFxuICogMi4gQWRkIHRoZSBjb3JyZWN0IHRleHQgZGVjb3JhdGlvbiBpbiBDaHJvbWUsIEVkZ2UsIElFLCBPcGVyYSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5hYmJyW3RpdGxlXSB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgLyogMSAqL1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAvKiAyICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgZG90dGVkO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUHJldmVudCB0aGUgZHVwbGljYXRlIGFwcGxpY2F0aW9uIG9mIGBib2xkZXJgIGJ5IHRoZSBuZXh0IHJ1bGUgaW4gU2FmYXJpIDYuXFxuICovXFxuYixcXG5zdHJvbmcge1xcbiAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCB3ZWlnaHQgaW4gQ2hyb21lLCBFZGdlLCBhbmQgU2FmYXJpLlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmhlcml0YW5jZSBhbmQgc2NhbGluZyBvZiBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIENvcnJlY3QgdGhlIG9kZCBgZW1gIGZvbnQgc2l6aW5nIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5jb2RlLFxcbmtiZCxcXG5zYW1wIHtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc3R5bGUgaW4gQW5kcm9pZCA0LjMtLlxcbiAqL1xcbmRmbiB7XFxuICBmb250LXN0eWxlOiBpdGFsaWM7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgYmFja2dyb3VuZCBhbmQgY29sb3IgaW4gSUUgOS0uXFxuICovXFxubWFyayB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYwO1xcbiAgY29sb3I6ICMwMDA7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zbWFsbCB7XFxuICBmb250LXNpemU6IDgwJTsgfVxcbi8qKlxcbiAqIFByZXZlbnQgYHN1YmAgYW5kIGBzdXBgIGVsZW1lbnRzIGZyb20gYWZmZWN0aW5nIHRoZSBsaW5lIGhlaWdodCBpblxcbiAqIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zdWIsXFxuc3VwIHtcXG4gIGZvbnQtc2l6ZTogNzUlO1xcbiAgbGluZS1oZWlnaHQ6IDA7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB2ZXJ0aWNhbC1hbGlnbjogYmFzZWxpbmU7IH1cXG5zdWIge1xcbiAgYm90dG9tOiAtMC4yNWVtOyB9XFxuc3VwIHtcXG4gIHRvcDogLTAuNWVtOyB9XFxuLyogRW1iZWRkZWQgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXVkaW8sXFxudmlkZW8ge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gaU9TIDQtNy5cXG4gKi9cXG5hdWRpbzpub3QoW2NvbnRyb2xzXSkge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGhlaWdodDogMDsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgYm9yZGVyIG9uIGltYWdlcyBpbnNpZGUgbGlua3MgaW4gSUUgMTAtLlxcbiAqL1xcbmltZyB7XFxuICBib3JkZXItc3R5bGU6IG5vbmU7IH1cXG4vKipcXG4gKiBIaWRlIHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKi9cXG5zdmc6bm90KDpyb290KSB7XFxuICBvdmVyZmxvdzogaGlkZGVuOyB9XFxuLyogRm9ybXNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIENoYW5nZSB0aGUgZm9udCBzdHlsZXMgaW4gYWxsIGJyb3dzZXJzIChvcGluaW9uYXRlZCkuXFxuICogMi4gUmVtb3ZlIHRoZSBtYXJnaW4gaW4gRmlyZWZveCBhbmQgU2FmYXJpLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCxcXG5vcHRncm91cCxcXG5zZWxlY3QsXFxudGV4dGFyZWEge1xcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDEwMCU7XFxuICAvKiAxICovXFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIG1hcmdpbjogMDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFNob3cgdGhlIG92ZXJmbG93IGluIElFLlxcbiAqIDEuIFNob3cgdGhlIG92ZXJmbG93IGluIEVkZ2UuXFxuICovXFxuYnV0dG9uLFxcbmlucHV0IHtcXG4gIC8qIDEgKi9cXG4gIG92ZXJmbG93OiB2aXNpYmxlOyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbmhlcml0YW5jZSBvZiB0ZXh0IHRyYW5zZm9ybSBpbiBFZGdlLCBGaXJlZm94LCBhbmQgSUUuXFxuICogMS4gUmVtb3ZlIHRoZSBpbmhlcml0YW5jZSBvZiB0ZXh0IHRyYW5zZm9ybSBpbiBGaXJlZm94LlxcbiAqL1xcbmJ1dHRvbixcXG5zZWxlY3Qge1xcbiAgLyogMSAqL1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7IH1cXG4vKipcXG4gKiAxLiBQcmV2ZW50IGEgV2ViS2l0IGJ1ZyB3aGVyZSAoMikgZGVzdHJveXMgbmF0aXZlIGBhdWRpb2AgYW5kIGB2aWRlb2BcXG4gKiAgICBjb250cm9scyBpbiBBbmRyb2lkIDQuXFxuICogMi4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaHRtbCBbdHlwZT1cXFwiYnV0dG9uXFxcIl0sXFxuW3R5cGU9XFxcInJlc2V0XFxcIl0sXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbm5lciBib3JkZXIgYW5kIHBhZGRpbmcgaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b246Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBib3JkZXItc3R5bGU6IG5vbmU7XFxuICBwYWRkaW5nOiAwOyB9XFxuLyoqXFxuICogUmVzdG9yZSB0aGUgZm9jdXMgc3R5bGVzIHVuc2V0IGJ5IHRoZSBwcmV2aW91cyBydWxlLlxcbiAqL1xcbmJ1dHRvbjotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwiYnV0dG9uXFxcIl06LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcInJlc2V0XFxcIl06LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdOi1tb3otZm9jdXNyaW5nIHtcXG4gIG91dGxpbmU6IDFweCBkb3R0ZWQgQnV0dG9uVGV4dDsgfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIHBhZGRpbmcgaW4gRmlyZWZveC5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBwYWRkaW5nOiAwLjM1ZW0gMC43NWVtIDAuNjI1ZW07IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSB0ZXh0IHdyYXBwaW5nIGluIEVkZ2UgYW5kIElFLlxcbiAqIDIuIENvcnJlY3QgdGhlIGNvbG9yIGluaGVyaXRhbmNlIGZyb20gYGZpZWxkc2V0YCBlbGVtZW50cyBpbiBJRS5cXG4gKiAzLiBSZW1vdmUgdGhlIHBhZGRpbmcgc28gZGV2ZWxvcGVycyBhcmUgbm90IGNhdWdodCBvdXQgd2hlbiB0aGV5IHplcm8gb3V0XFxuICogICAgYGZpZWxkc2V0YCBlbGVtZW50cyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxubGVnZW5kIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIC8qIDEgKi9cXG4gIGNvbG9yOiBpbmhlcml0O1xcbiAgLyogMiAqL1xcbiAgZGlzcGxheTogdGFibGU7XFxuICAvKiAxICovXFxuICBtYXgtd2lkdGg6IDEwMCU7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMyAqL1xcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcXG4gIC8qIDEgKi8gfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB2ZXJ0aWNhbCBhbGlnbm1lbnQgaW4gQ2hyb21lLCBGaXJlZm94LCBhbmQgT3BlcmEuXFxuICovXFxucHJvZ3Jlc3Mge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgLyogMSAqL1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBkZWZhdWx0IHZlcnRpY2FsIHNjcm9sbGJhciBpbiBJRS5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICBvdmVyZmxvdzogYXV0bzsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIElFIDEwLS5cXG4gKiAyLiBSZW1vdmUgdGhlIHBhZGRpbmcgaW4gSUUgMTAtLlxcbiAqL1xcblt0eXBlPVxcXCJjaGVja2JveFxcXCJdLFxcblt0eXBlPVxcXCJyYWRpb1xcXCJdIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIC8qIDEgKi9cXG4gIHBhZGRpbmc6IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBjdXJzb3Igc3R5bGUgb2YgaW5jcmVtZW50IGFuZCBkZWNyZW1lbnQgYnV0dG9ucyBpbiBDaHJvbWUuXFxuICovXFxuW3R5cGU9XFxcIm51bWJlclxcXCJdOjotd2Via2l0LWlubmVyLXNwaW4tYnV0dG9uLFxcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7XFxuICBoZWlnaHQ6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBvZGQgYXBwZWFyYW5jZSBpbiBDaHJvbWUgYW5kIFNhZmFyaS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvdXRsaW5lIHN0eWxlIGluIFNhZmFyaS5cXG4gKi9cXG5bdHlwZT1cXFwic2VhcmNoXFxcIl0ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQ7XFxuICAvKiAxICovXFxuICBvdXRsaW5lLW9mZnNldDogLTJweDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgcGFkZGluZyBhbmQgY2FuY2VsIGJ1dHRvbnMgaW4gQ2hyb21lIGFuZCBTYWZhcmkgb24gbWFjT1MuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdOjotd2Via2l0LXNlYXJjaC1jYW5jZWwtYnV0dG9uLFxcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtZGVjb3JhdGlvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmFiaWxpdHkgdG8gc3R5bGUgY2xpY2thYmxlIHR5cGVzIGluIGlPUyBhbmQgU2FmYXJpLlxcbiAqIDIuIENoYW5nZSBmb250IHByb3BlcnRpZXMgdG8gYGluaGVyaXRgIGluIFNhZmFyaS5cXG4gKi9cXG46Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9uIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xcbiAgLyogMSAqL1xcbiAgZm9udDogaW5oZXJpdDtcXG4gIC8qIDIgKi8gfVxcbi8qIEludGVyYWN0aXZlXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIEVkZ2UsIElFLCBhbmQgRmlyZWZveC5cXG4gKi9cXG5kZXRhaWxzLFxcbm1lbnUge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zdW1tYXJ5IHtcXG4gIGRpc3BsYXk6IGxpc3QtaXRlbTsgfVxcbi8qIFNjcmlwdGluZ1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuY2FudmFzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFLlxcbiAqL1xcbnRlbXBsYXRlIHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG4vKiBIaWRkZW5cXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDEwLS5cXG4gKi9cXG5baGlkZGVuXSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG4gICAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICAgIGNvbG9yOiB3aGl0ZTtcXG59XFxuLmJsYWNrIHtcXG4gICAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gICAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzYWQyY2JcXG59XFxuLndoaXRlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgICBmb250LXdlaWdodDogODAwO1xcbn1cXG4uZm9udC1saWdodCB7XFxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xcbiAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5uby1saXN0LXN0eWxlIHtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gICAgb3V0bGluZTogbm9uZTtcXG4gICAgYm9yZGVyOiBub25lO1xcbn1cXG4uYm9yZGVyLXJhZGl1cyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuLnNvY2lhbC1uZXR3b3JrLWljb24ge1xcbiAgbWF4LXdpZHRoOiAyMHB4OyB9XFxuLnNpdGUtZm9vdGVyIHtcXG4gIHBhZGRpbmc6IDElIDQlO1xcbiAgaGVpZ2h0OiA4dmg7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5zb2NpYWwtaWNvbnMtbGlzdCA+IGxpIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5zaXRlLWZvb3RlciB7XFxuICAgIGhlaWdodDogMTN2aDsgfSB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDMyMHB4KSB7XFxuICAuc2l0ZS1mb290ZXIgPiAucm93ID4gLmNvbC14cy0xMjpmaXJzdC1jaGlsZCB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7SUFDSSxxQkFBcUI7Q0FDeEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGNBQWM7Q0FDakI7QUFDRDtJQUNJLHlCQUF5QjtDQUM1QjtBQUNEO0lBQ0ksd0JBQXdCO0NBQzNCO0FBQ0Q7SUFDSSwwQkFBMEI7Q0FDN0I7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDBCQUEwQjtRQUN0Qix1QkFBdUI7WUFDbkIsb0JBQW9CO0NBQy9CO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQyw2QkFBNkI7SUFDN0IsOEJBQThCO1FBQzFCLDJCQUEyQjtZQUN2Qix1QkFBdUI7Q0FDbEM7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksZ0RBQWdEO0lBQ2hELHdEQUF3RDtJQUN4RCxnREFBZ0Q7SUFDaEQsMkNBQTJDO0lBQzNDLHdDQUF3QztJQUN4Qyw2RUFBNkU7Q0FDaEY7QUFDRDtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsZUFBZTtFQUNmLFlBQVk7RUFDWixlQUFlLEVBQUU7QUFDbkI7RUFDRSxzQkFBc0I7RUFDdEIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRTtJQUNFLGFBQWEsRUFBRSxFQUFFO0FBQ3JCO0VBQ0U7SUFDRSxlQUFlLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zb2NpYWwtbmV0d29yay1pY29uIHtcXG4gIG1heC13aWR0aDogMjBweDsgfVxcbi5zaXRlLWZvb3RlciB7XFxuICBwYWRkaW5nOiAxJSA0JTtcXG4gIGhlaWdodDogOHZoO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc29jaWFsLWljb25zLWxpc3QgPiBsaSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuc2l0ZS1mb290ZXIge1xcbiAgICBoZWlnaHQ6IDEzdmg7IH0gfVxcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLnNpdGUtZm9vdGVyID4gLnJvdyA+IC5jb2wteHMtMTI6Zmlyc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiQGNoYXJzZXQgXFxcIlVURi04XFxcIjtcXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuOnJvb3Qge1xcbiAgLypcXG4gICAqIFR5cG9ncmFwaHlcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1mb250LWZhbWlseS1iYXNlOiAnU2Vnb2UgVUknLCAnSGVsdmV0aWNhTmV1ZS1MaWdodCcsIHNhbnMtc2VyaWY7XFxuXFxuICAvKlxcbiAgICogTGF5b3V0XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tbWF4LWNvbnRlbnQtd2lkdGg6IDEwMDBweDtcXG5cXG4gIC8qXFxuICAgKiBNZWRpYSBxdWVyaWVzIGJyZWFrcG9pbnRzXFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tc2NyZWVuLXhzLW1pbjogNDgwcHg7ICAvKiBFeHRyYSBzbWFsbCBzY3JlZW4gLyBwaG9uZSAqL1xcbiAgLS1zY3JlZW4tc20tbWluOiA3NjhweDsgIC8qIFNtYWxsIHNjcmVlbiAvIHRhYmxldCAqL1xcbiAgLS1zY3JlZW4tbWQtbWluOiA5OTJweDsgIC8qIE1lZGl1bSBzY3JlZW4gLyBkZXNrdG9wICovXFxuICAtLXNjcmVlbi1sZy1taW46IDEyMDBweDsgLyogTGFyZ2Ugc2NyZWVuIC8gd2lkZSBkZXNrdG9wICovXFxufVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7IH1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0udHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNjAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNzAwOyB9XFxuLypcXG4gKiBub3JtYWxpemUuY3NzIGlzIGltcG9ydGVkIGluIEpTIGZpbGUuXFxuICogSWYgeW91IGltcG9ydCBleHRlcm5hbCBDU1MgZmlsZSBmcm9tIHlvdXIgaW50ZXJuYWwgQ1NTXFxuICogdGhlbiBpdCB3aWxsIGJlIGlubGluZWQgYW5kIHByb2Nlc3NlZCB3aXRoIENTUyBtb2R1bGVzLlxcbiAqL1xcbi8qXFxuICogQmFzZSBzdHlsZXNcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbmh0bWwge1xcbiAgY29sb3I6ICMyMjI7XFxuICBmb250LXdlaWdodDogMTAwO1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiB+MTZweDsgKi9cXG4gIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1iYXNlKTtcXG4gIGxpbmUtaGVpZ2h0OiAxLjM3NTtcXG4gIC8qIH4yMnB4ICovIH1cXG5ib2R5IHtcXG4gIG1hcmdpbjogMDtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtOyB9XFxuYSB7XFxuICBjb2xvcjogIzAwNzRjMjsgfVxcbi8qXFxuICogUmVtb3ZlIHRleHQtc2hhZG93IGluIHNlbGVjdGlvbiBoaWdobGlnaHQ6XFxuICogaHR0cHM6Ly90d2l0dGVyLmNvbS9taWtldGF5bHIvc3RhdHVzLzEyMjI4ODA1MzAxXFxuICpcXG4gKiBUaGVzZSBzZWxlY3Rpb24gcnVsZSBzZXRzIGhhdmUgdG8gYmUgc2VwYXJhdGUuXFxuICogQ3VzdG9taXplIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIHRvIG1hdGNoIHlvdXIgZGVzaWduLlxcbiAqL1xcbjo6LW1vei1zZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lOyB9XFxuOjpzZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lOyB9XFxuLypcXG4gKiBBIGJldHRlciBsb29raW5nIGRlZmF1bHQgaG9yaXpvbnRhbCBydWxlXFxuICovXFxuaHIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDFweDtcXG4gIGJvcmRlcjogMDtcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xcbiAgbWFyZ2luOiAxZW0gMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKlxcbiAqIFJlbW92ZSB0aGUgZ2FwIGJldHdlZW4gYXVkaW8sIGNhbnZhcywgaWZyYW1lcyxcXG4gKiBpbWFnZXMsIHZpZGVvcyBhbmQgdGhlIGJvdHRvbSBvZiB0aGVpciBjb250YWluZXJzOlxcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9oNWJwL2h0bWw1LWJvaWxlcnBsYXRlL2lzc3Vlcy80NDBcXG4gKi9cXG5hdWRpbyxcXG5jYW52YXMsXFxuaWZyYW1lLFxcbmltZyxcXG5zdmcsXFxudmlkZW8ge1xcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTsgfVxcbi8qXFxuICogUmVtb3ZlIGRlZmF1bHQgZmllbGRzZXQgc3R5bGVzLlxcbiAqL1xcbmZpZWxkc2V0IHtcXG4gIGJvcmRlcjogMDtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKlxcbiAqIEFsbG93IG9ubHkgdmVydGljYWwgcmVzaXppbmcgb2YgdGV4dGFyZWFzLlxcbiAqL1xcbnRleHRhcmVhIHtcXG4gIHJlc2l6ZTogdmVydGljYWw7IH1cXG4vKlxcbiAqIEJyb3dzZXIgdXBncmFkZSBwcm9tcHRcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi5icm93c2VydXBncmFkZSB7XFxuICBtYXJnaW46IDAuMmVtIDA7XFxuICBiYWNrZ3JvdW5kOiAjY2NjO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAwLjJlbSAwOyB9XFxuLypcXG4gKiBQcmludCBzdHlsZXNcXG4gKiBJbmxpbmVkIHRvIGF2b2lkIHRoZSBhZGRpdGlvbmFsIEhUVFAgcmVxdWVzdDpcXG4gKiBodHRwOi8vd3d3LnBocGllZC5jb20vZGVsYXktbG9hZGluZy15b3VyLXByaW50LWNzcy9cXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbkBtZWRpYSBwcmludCB7XFxuICAqLFxcbiAgKjo6YmVmb3JlLFxcbiAgKjo6YWZ0ZXIge1xcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xcbiAgICAvKiBCbGFjayBwcmludHMgZmFzdGVyOiBodHRwOi8vd3d3LnNhbmJlaWppLmNvbS9hcmNoaXZlcy85NTMgKi9cXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICB0ZXh0LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50OyB9XFxuICBhLFxcbiAgYTp2aXNpdGVkIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7IH1cXG4gIGFbaHJlZl06OmFmdGVyIHtcXG4gICAgY29udGVudDogXFxcIiAoXFxcIiBhdHRyKGhyZWYpIFxcXCIpXFxcIjsgfVxcbiAgYWJiclt0aXRsZV06OmFmdGVyIHtcXG4gICAgY29udGVudDogXFxcIiAoXFxcIiBhdHRyKHRpdGxlKSBcXFwiKVxcXCI7IH1cXG4gIC8qXFxuICAgKiBEb24ndCBzaG93IGxpbmtzIHRoYXQgYXJlIGZyYWdtZW50IGlkZW50aWZpZXJzLFxcbiAgICogb3IgdXNlIHRoZSBgamF2YXNjcmlwdDpgIHBzZXVkbyBwcm90b2NvbFxcbiAgICovXFxuICBhW2hyZWZePScjJ106OmFmdGVyLFxcbiAgYVtocmVmXj0namF2YXNjcmlwdDonXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiAnJzsgfVxcbiAgcHJlLFxcbiAgYmxvY2txdW90ZSB7XFxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM5OTk7XFxuICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdm9pZDsgfVxcbiAgLypcXG4gICAqIFByaW50aW5nIFRhYmxlczpcXG4gICAqIGh0dHA6Ly9jc3MtZGlzY3Vzcy5pbmN1dGlvLmNvbS93aWtpL1ByaW50aW5nX1RhYmxlc1xcbiAgICovXFxuICB0aGVhZCB7XFxuICAgIGRpc3BsYXk6IHRhYmxlLWhlYWRlci1ncm91cDsgfVxcbiAgdHIsXFxuICBpbWcge1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIGltZyB7XFxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50OyB9XFxuICBwLFxcbiAgaDIsXFxuICBoMyB7XFxuICAgIG9ycGhhbnM6IDM7XFxuICAgIHdpZG93czogMzsgfVxcbiAgaDIsXFxuICBoMyB7XFxuICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkOyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQSxpQkFBaUI7QUFDakI7Ozs7Ozs7R0FPRztBQUNIOzs7Ozs7O0dBT0c7QUFDSDtFQUNFOztnRkFFOEU7O0VBRTlFLGtFQUFrRTs7RUFFbEU7O2dGQUU4RTs7RUFFOUUsNEJBQTRCOztFQUU1Qjs7Z0ZBRThFOztFQUU5RSx1QkFBdUIsRUFBRSxnQ0FBZ0M7RUFDekQsdUJBQXVCLEVBQUUsMkJBQTJCO0VBQ3BELHVCQUF1QixFQUFFLDZCQUE2QjtFQUN0RCx3QkFBd0IsQ0FBQyxpQ0FBaUM7Q0FDM0Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixtQ0FBbUM7RUFDbkMsdUtBQXVLO0VBQ3ZLLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UscUJBQXFCO0VBQ3JCLDZCQUE2QjtFQUM3QixxSkFBcUo7RUFDckosaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxxQkFBcUI7RUFDckIsa0NBQWtDO0VBQ2xDLG9LQUFvSztFQUNwSyxpQkFBaUIsRUFBRTtBQUNyQjs7OztHQUlHO0FBQ0g7O2dGQUVnRjtBQUNoRjtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLFlBQVk7RUFDWixxQ0FBcUM7RUFDckMsbUJBQW1CO0VBQ25CLFdBQVcsRUFBRTtBQUNmO0VBQ0UsVUFBVTtFQUNWLHFCQUFxQixFQUFFO0FBQ3pCO0VBQ0UsZUFBZSxFQUFFO0FBQ25COzs7Ozs7R0FNRztBQUNIO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQixFQUFFO0FBQ3RCOztHQUVHO0FBQ0g7RUFDRSxlQUFlO0VBQ2YsWUFBWTtFQUNaLFVBQVU7RUFDViwyQkFBMkI7RUFDM0IsY0FBYztFQUNkLFdBQVcsRUFBRTtBQUNmOzs7O0dBSUc7QUFDSDs7Ozs7O0VBTUUsdUJBQXVCLEVBQUU7QUFDM0I7O0dBRUc7QUFDSDtFQUNFLFVBQVU7RUFDVixVQUFVO0VBQ1YsV0FBVyxFQUFFO0FBQ2Y7O0dBRUc7QUFDSDtFQUNFLGlCQUFpQixFQUFFO0FBQ3JCOztnRkFFZ0Y7QUFDaEY7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixpQkFBaUIsRUFBRTtBQUNyQjs7OztnRkFJZ0Y7QUFDaEY7RUFDRTs7O0lBR0UsbUNBQW1DO0lBQ25DLHVCQUF1QjtJQUN2QiwrREFBK0Q7SUFDL0Qsb0NBQW9DO1lBQzVCLDRCQUE0QjtJQUNwQyw2QkFBNkIsRUFBRTtFQUNqQzs7SUFFRSwyQkFBMkIsRUFBRTtFQUMvQjtJQUNFLDZCQUE2QixFQUFFO0VBQ2pDO0lBQ0UsOEJBQThCLEVBQUU7RUFDbEM7OztLQUdHO0VBQ0g7O0lBRUUsWUFBWSxFQUFFO0VBQ2hCOztJQUVFLHVCQUF1QjtJQUN2Qix5QkFBeUIsRUFBRTtFQUM3Qjs7O0tBR0c7RUFDSDtJQUNFLDRCQUE0QixFQUFFO0VBQ2hDOztJQUVFLHlCQUF5QixFQUFFO0VBQzdCO0lBQ0UsMkJBQTJCLEVBQUU7RUFDL0I7OztJQUdFLFdBQVc7SUFDWCxVQUFVLEVBQUU7RUFDZDs7SUFFRSx3QkFBd0IsRUFBRSxFQUFFXCIsXCJmaWxlXCI6XCJMYXlvdXQuY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIkBjaGFyc2V0IFxcXCJVVEYtOFxcXCI7XFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbjpyb290IHtcXG4gIC8qXFxuICAgKiBUeXBvZ3JhcGh5XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tZm9udC1mYW1pbHktYmFzZTogJ1NlZ29lIFVJJywgJ0hlbHZldGljYU5ldWUtTGlnaHQnLCBzYW5zLXNlcmlmO1xcblxcbiAgLypcXG4gICAqIExheW91dFxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLW1heC1jb250ZW50LXdpZHRoOiAxMDAwcHg7XFxuXFxuICAvKlxcbiAgICogTWVkaWEgcXVlcmllcyBicmVha3BvaW50c1xcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLXNjcmVlbi14cy1taW46IDQ4MHB4OyAgLyogRXh0cmEgc21hbGwgc2NyZWVuIC8gcGhvbmUgKi9cXG4gIC0tc2NyZWVuLXNtLW1pbjogNzY4cHg7ICAvKiBTbWFsbCBzY3JlZW4gLyB0YWJsZXQgKi9cXG4gIC0tc2NyZWVuLW1kLW1pbjogOTkycHg7ICAvKiBNZWRpdW0gc2NyZWVuIC8gZGVza3RvcCAqL1xcbiAgLS1zY3JlZW4tbGctbWluOiAxMjAwcHg7IC8qIExhcmdlIHNjcmVlbiAvIHdpZGUgZGVza3RvcCAqL1xcbn1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogMTAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDcwMDsgfVxcbi8qXFxuICogbm9ybWFsaXplLmNzcyBpcyBpbXBvcnRlZCBpbiBKUyBmaWxlLlxcbiAqIElmIHlvdSBpbXBvcnQgZXh0ZXJuYWwgQ1NTIGZpbGUgZnJvbSB5b3VyIGludGVybmFsIENTU1xcbiAqIHRoZW4gaXQgd2lsbCBiZSBpbmxpbmVkIGFuZCBwcm9jZXNzZWQgd2l0aCBDU1MgbW9kdWxlcy5cXG4gKi9cXG4vKlxcbiAqIEJhc2Ugc3R5bGVzXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5odG1sIHtcXG4gIGNvbG9yOiAjMjIyO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDtcXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogfjE2cHg7ICovXFxuICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktYmFzZSk7XFxuICBsaW5lLWhlaWdodDogMS4zNzU7XFxuICAvKiB+MjJweCAqLyB9XFxuYm9keSB7XFxuICBtYXJnaW46IDA7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTsgfVxcbmEge1xcbiAgY29sb3I6ICMwMDc0YzI7IH1cXG4vKlxcbiAqIFJlbW92ZSB0ZXh0LXNoYWRvdyBpbiBzZWxlY3Rpb24gaGlnaGxpZ2h0OlxcbiAqIGh0dHBzOi8vdHdpdHRlci5jb20vbWlrZXRheWxyL3N0YXR1cy8xMjIyODgwNTMwMVxcbiAqXFxuICogVGhlc2Ugc2VsZWN0aW9uIHJ1bGUgc2V0cyBoYXZlIHRvIGJlIHNlcGFyYXRlLlxcbiAqIEN1c3RvbWl6ZSB0aGUgYmFja2dyb3VuZCBjb2xvciB0byBtYXRjaCB5b3VyIGRlc2lnbi5cXG4gKi9cXG46Oi1tb3otc2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbjo6c2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbi8qXFxuICogQSBiZXR0ZXIgbG9va2luZyBkZWZhdWx0IGhvcml6b250YWwgcnVsZVxcbiAqL1xcbmhyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBib3JkZXI6IDA7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcXG4gIG1hcmdpbjogMWVtIDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBSZW1vdmUgdGhlIGdhcCBiZXR3ZWVuIGF1ZGlvLCBjYW52YXMsIGlmcmFtZXMsXFxuICogaW1hZ2VzLCB2aWRlb3MgYW5kIHRoZSBib3R0b20gb2YgdGhlaXIgY29udGFpbmVyczpcXG4gKiBodHRwczovL2dpdGh1Yi5jb20vaDVicC9odG1sNS1ib2lsZXJwbGF0ZS9pc3N1ZXMvNDQwXFxuICovXFxuYXVkaW8sXFxuY2FudmFzLFxcbmlmcmFtZSxcXG5pbWcsXFxuc3ZnLFxcbnZpZGVvIHtcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7IH1cXG4vKlxcbiAqIFJlbW92ZSBkZWZhdWx0IGZpZWxkc2V0IHN0eWxlcy5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBBbGxvdyBvbmx5IHZlcnRpY2FsIHJlc2l6aW5nIG9mIHRleHRhcmVhcy5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICByZXNpemU6IHZlcnRpY2FsOyB9XFxuLypcXG4gKiBCcm93c2VyIHVwZ3JhZGUgcHJvbXB0XFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG46Z2xvYmFsKC5icm93c2VydXBncmFkZSkge1xcbiAgbWFyZ2luOiAwLjJlbSAwO1xcbiAgYmFja2dyb3VuZDogI2NjYztcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMC4yZW0gMDsgfVxcbi8qXFxuICogUHJpbnQgc3R5bGVzXFxuICogSW5saW5lZCB0byBhdm9pZCB0aGUgYWRkaXRpb25hbCBIVFRQIHJlcXVlc3Q6XFxuICogaHR0cDovL3d3dy5waHBpZWQuY29tL2RlbGF5LWxvYWRpbmcteW91ci1wcmludC1jc3MvXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5AbWVkaWEgcHJpbnQge1xcbiAgKixcXG4gICo6OmJlZm9yZSxcXG4gICo6OmFmdGVyIHtcXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcXG4gICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcXG4gICAgLyogQmxhY2sgcHJpbnRzIGZhc3RlcjogaHR0cDovL3d3dy5zYW5iZWlqaS5jb20vYXJjaGl2ZXMvOTUzICovXFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgdGV4dC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDsgfVxcbiAgYSxcXG4gIGE6dmlzaXRlZCB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyB9XFxuICBhW2hyZWZdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cihocmVmKSBcXFwiKVxcXCI7IH1cXG4gIGFiYnJbdGl0bGVdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cih0aXRsZSkgXFxcIilcXFwiOyB9XFxuICAvKlxcbiAgICogRG9uJ3Qgc2hvdyBsaW5rcyB0aGF0IGFyZSBmcmFnbWVudCBpZGVudGlmaWVycyxcXG4gICAqIG9yIHVzZSB0aGUgYGphdmFzY3JpcHQ6YCBwc2V1ZG8gcHJvdG9jb2xcXG4gICAqL1xcbiAgYVtocmVmXj0nIyddOjphZnRlcixcXG4gIGFbaHJlZl49J2phdmFzY3JpcHQ6J106OmFmdGVyIHtcXG4gICAgY29udGVudDogJyc7IH1cXG4gIHByZSxcXG4gIGJsb2NrcXVvdGUge1xcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjOTk5O1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIC8qXFxuICAgKiBQcmludGluZyBUYWJsZXM6XFxuICAgKiBodHRwOi8vY3NzLWRpc2N1c3MuaW5jdXRpby5jb20vd2lraS9QcmludGluZ19UYWJsZXNcXG4gICAqL1xcbiAgdGhlYWQge1xcbiAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7IH1cXG4gIHRyLFxcbiAgaW1nIHtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICBpbWcge1xcbiAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDsgfVxcbiAgcCxcXG4gIGgyLFxcbiAgaDMge1xcbiAgICBvcnBoYW5zOiAzO1xcbiAgICB3aWRvd3M6IDM7IH1cXG4gIGgyLFxcbiAgaDMge1xcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhdm9pZDsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5sb2FkZXIge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgZm9udC1mYW1pbHk6IElSU2Fucywgc2VyaWY7XFxuICB6LWluZGV4OiAxMDtcXG4gIHBhZGRpbmc6IDIwcHg7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgdG9wOiBjYWxjKDUwJSAtIDg1cHgpO1xcbiAgbGVmdDogY2FsYyg1MCUgLSA3MHB4KTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7IH1cXG4gIC5sb2FkZXIgaW1nIHtcXG4gICAgd2lkdGg6IDEwMHB4OyB9XFxuICAubG9hZGVyIC5sb2FkZXItaW5mbyB7XFxuICAgIGRpcmVjdGlvbjogcnRsO1xcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyB9XFxuICAubG9hZGVyLmxvYWRpbmcge1xcbiAgICBkaXNwbGF5OiBibG9jazsgfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQiwyQkFBMkI7RUFDM0IsWUFBWTtFQUNaLGNBQWM7RUFDZCxvQkFBb0I7RUFDcEIsd0JBQXdCO0VBQ3hCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLHdIQUF3SDtVQUNoSCxnSEFBZ0gsRUFBRTtFQUMxSDtJQUNFLGFBQWEsRUFBRTtFQUNqQjtJQUNFLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CLEVBQUU7RUFDdkI7SUFDRSxlQUFlLEVBQUVcIixcImZpbGVcIjpcIkxvYWRlci5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5sb2FkZXIge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgZm9udC1mYW1pbHk6IElSU2Fucywgc2VyaWY7XFxuICB6LWluZGV4OiAxMDtcXG4gIHBhZGRpbmc6IDIwcHg7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgdG9wOiBjYWxjKDUwJSAtIDg1cHgpO1xcbiAgbGVmdDogY2FsYyg1MCUgLSA3MHB4KTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7IH1cXG4gIC5sb2FkZXIgaW1nIHtcXG4gICAgd2lkdGg6IDEwMHB4OyB9XFxuICAubG9hZGVyIC5sb2FkZXItaW5mbyB7XFxuICAgIGRpcmVjdGlvbjogcnRsO1xcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyB9XFxuICAubG9hZGVyLmxvYWRpbmcge1xcbiAgICBkaXNwbGF5OiBibG9jazsgfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLnNjc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuc2Nzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAzIDQgNiIsInZhciBlc2NhcGUgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL3VybC9lc2NhcGUuanNcIik7XG5leHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5zZWFyY2gtdGhlbWV7XFxuICBiYWNrZ3JvdW5kOiB1cmwoXCIgKyBlc2NhcGUocmVxdWlyZShcIi4vZ2VvbWV0cnkyLnBuZ1wiKSkgKyBcIik7XFxuICBoZWlnaHQ6IDE1dmg7XFxuICBtYXJnaW4tdG9wOiAxMHZoO1xcbn1cXG4ubWFyLTV7XFxuICBtYXJnaW46IDUlO1xcbn1cXG4ucGFkZC01e1xcbiAgcGFkZGluZzogNSU7XFxufVxcbi5tYXItbGVmdHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDtcXG59XFxuLm1hci10b3B7XFxuICBtYXJnaW4tdG9wOiAzdmg7XFxufVxcbi5tYXItYm90dG9te1xcbiAgbWFyZ2luLWJvdHRvbTogMTMlO1xcbiAgbWFyZ2luLWxlZnQ6IDIuNXZ3O1xcbiAgd2lkdGg6IDkzJSAhaW1wb3J0YW50O1xcbn1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTtcXG59XFxuLmljb25zLWNse1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoO1xcblxcbn1cXG4ubWFyLXRvcC0xMHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7XFxufVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gIH1cXG4gIC5tYXItbGVmdHtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7XFxuICB9XFxuICAuZHJvcGRvd24tY29udGVudHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDtcXG4gIH1cXG5cXG59XFxuLnJhZGlvIHtcXG4gIG1hcmdpbjogMyU7XFxuICBtYXJnaW4tcmlnaHQ6IDE1dnc7XFxuICBtYXJnaW4tYm90dG9tOiAwOyB9XFxuLnJhZGlvLWxhYmVsIHtcXG4gIGRpcmVjdGlvbjogbHRyOyB9XFxuLmVycm9yIHtcXG4gIGJvcmRlcjogMXB4IHNvbGlkIHJlZCAhaW1wb3J0YW50OyB9XFxuLmVyci1tc2cge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG4gIGNvbG9yOiAjZjE2NDY0OyB9XFxuLmFkZC1sYWJlbCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHRleHQtYWxpZ246IGxlZnQ7IH1cXG4uYWRkLWlucHV0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiA2dmg7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICBib3JkZXI6IHRoaW4gc29saWQgbGlnaHRncmV5O1xcbiAgcGFkZGluZzogMiU7IH1cXG4ubWFyLWxlZnQtYWRkIHtcXG4gIG1hcmdpbi1sZWZ0OiAxMXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoOyB9XFxuLmVtcHR5LWxhYmVsLWhlaWdodCB7XFxuICBoZWlnaHQ6IDIuNXZoOyB9XFxuLmZsZXgtbWlkZGxlLXJlc3BvbiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogM3ZoO1xcbiAgbWFyZ2luLXRvcDogMnZoOyB9XFxuLmZvcm0tY29udGFpbmVyIHtcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi8qdGl0bGUgY3NzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1hci1sZWZ0LWFkZCB7XFxuICAgIG1hcmdpbi1sZWZ0OiAwOyB9XFxuICAucmVnaXN0ZXItaGVhZGxpbmUge1xcbiAgICBmb250LXNpemU6IHNtYWxsOyB9XFxuICAuZmxleC1taWRkbGUtcmVzcG9uIHtcXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgICBtYXJnaW4tYm90dG9tOiAzdmg7IH0gfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL05ld0hvdXNlRm9ybS9tYWluLmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtJQUNJLHFCQUFxQjtDQUN4QjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksY0FBYztDQUNqQjtBQUNEO0lBQ0kseUJBQXlCO0NBQzVCO0FBQ0Q7SUFDSSx3QkFBd0I7Q0FDM0I7QUFDRDtJQUNJLDBCQUEwQjtDQUM3QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsMEJBQTBCO1FBQ3RCLHVCQUF1QjtZQUNuQixvQkFBb0I7Q0FDL0I7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDZCQUE2QjtJQUM3Qiw4QkFBOEI7UUFDMUIsMkJBQTJCO1lBQ3ZCLHVCQUF1QjtDQUNsQztBQUNEO0lBQ0ksa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxnREFBZ0Q7SUFDaEQsd0RBQXdEO0lBQ3hELGdEQUFnRDtJQUNoRCwyQ0FBMkM7SUFDM0Msd0NBQXdDO0lBQ3hDLDZFQUE2RTtDQUNoRjtBQUNELGNBQWMsVUFBVSxDQUFDO0FBQ3pCO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLGFBQWE7RUFDYiwrQ0FBK0M7VUFDdkMsdUNBQXVDO0VBQy9DLHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkU7Q0FDOUU7QUFDRDtFQUNFLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0NBQzdCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QjtDQUNqQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLHVCQUF1Qjs7Q0FFeEI7QUFDRDtFQUNFLHlCQUF5QjtDQUMxQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHVCQUF1QjtDQUN4QjtBQUNEO0VBQ0UsNEJBQTRCO0NBQzdCO0FBQ0Q7RUFDRSwyQkFBMkI7Q0FDNUI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsK0JBQStCO0VBQy9CLCtCQUErQjtNQUMzQixnQ0FBZ0M7VUFDNUIsNEJBQTRCO0NBQ3JDO0FBQ0Q7RUFDRSxzQkFBc0I7Q0FDdkI7QUFDRDtFQUNFLDBDQUFtQztFQUNuQyxhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjtBQUNEO0VBQ0UsZ0JBQWdCO0NBQ2pCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLG9CQUFvQjtFQUNwQixZQUFZO0NBQ2I7QUFDRDtFQUNFLFdBQVc7RUFDWCxZQUFZOztDQUViO0FBQ0Q7RUFDRSxlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Qsc0JBQXNCO0FBQ3RCO0VBQ0UsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFVBQVU7RUFDVixXQUFXO0NBQ1o7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0NBQ2I7QUFDRCwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7RUFDRDtJQUNFLFVBQVU7R0FDWDs7RUFFRDtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1osYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixvQkFBb0I7SUFDcEIsWUFBWTtHQUNiO0VBQ0Q7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVO0dBQ1g7O0NBRUY7QUFDRDtFQUNFLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxpQ0FBaUMsRUFBRTtBQUNyQztFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFlBQVksRUFBRTtBQUNoQjtFQUNFLGtCQUFrQjtFQUNsQixtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGNBQWMsRUFBRTtBQUNsQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHlCQUF5QjtNQUNyQixzQkFBc0I7VUFDbEIsd0JBQXdCO0VBQ2hDLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCLGFBQWE7QUFDYjtFQUNFO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0UsaUJBQWlCLEVBQUU7RUFDckI7SUFDRSxzQkFBc0I7SUFDdEIsbUJBQW1CLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5zZWFyY2gtdGhlbWV7XFxuICBiYWNrZ3JvdW5kOiB1cmwoXFxcIi4vZ2VvbWV0cnkyLnBuZ1xcXCIpO1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDtcXG59XFxuLm1hci01e1xcbiAgbWFyZ2luOiA1JTtcXG59XFxuLnBhZGQtNXtcXG4gIHBhZGRpbmc6IDUlO1xcbn1cXG4ubWFyLWxlZnR7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxufVxcbi5tYXItdG9we1xcbiAgbWFyZ2luLXRvcDogM3ZoO1xcbn1cXG4ubWFyLWJvdHRvbXtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDtcXG59XFxuLmJsdWUtYnV0dG9uIHtcXG4gIHdpZHRoOiAxM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuICBtYXJnaW46IGF1dG87XFxuICBiYWNrZ3JvdW5kOiByZ2IoNjgsMjA5LDIwMik7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7XFxufVxcbi5pY29ucy1jbHtcXG4gIHdpZHRoOiAzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG5cXG59XFxuLm1hci10b3AtMTB7XFxuICBtYXJnaW4tdG9wOiAyJTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXJ7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06aG92ZXJ7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpmb2N1c3tcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpmb2N1c3tcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxufVxcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoO1xcbn1cXG4uZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOjc2OHB4KXtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7XFxuICB9XFxuICAubWFyLWxlZnR7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiByZ2IoNjgsMjA5LDIwMik7XFxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICAgIHBhZGRpbmc6IDMlO1xcbiAgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnR7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gICAgbWluLXdpZHRoOiAyMDBweDtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgICB6LWluZGV4OiAxO1xcbiAgICBsZWZ0OiAxMnZ3O1xcbiAgICB0b3A6IDEwdmg7XFxuICB9XFxuXFxufVxcbi5yYWRpbyB7XFxuICBtYXJnaW46IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAxNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogMDsgfVxcbi5yYWRpby1sYWJlbCB7XFxuICBkaXJlY3Rpb246IGx0cjsgfVxcbi5lcnJvciB7XFxuICBib3JkZXI6IDFweCBzb2xpZCByZWQgIWltcG9ydGFudDsgfVxcbi5lcnItbXNnIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7XFxuICBjb2xvcjogI2YxNjQ2NDsgfVxcbi5hZGQtbGFiZWwge1xcbiAgd2lkdGg6IDEwMCU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0OyB9XFxuLmFkZC1pbnB1dCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNnZoO1xcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xcbiAgYm9yZGVyOiB0aGluIHNvbGlkIGxpZ2h0Z3JleTtcXG4gIHBhZGRpbmc6IDIlOyB9XFxuLm1hci1sZWZ0LWFkZCB7XFxuICBtYXJnaW4tbGVmdDogMTF2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5lbXB0eS1sYWJlbC1oZWlnaHQge1xcbiAgaGVpZ2h0OiAyLjV2aDsgfVxcbi5mbGV4LW1pZGRsZS1yZXNwb24ge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDN2aDtcXG4gIG1hcmdpbi10b3A6IDJ2aDsgfVxcbi5mb3JtLWNvbnRhaW5lciB7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7IH1cXG4vKnRpdGxlIGNzcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5tYXItbGVmdC1hZGQge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLnJlZ2lzdGVyLWhlYWRsaW5lIHtcXG4gICAgZm9udC1zaXplOiBzbWFsbDsgfVxcbiAgLmZsZXgtbWlkZGxlLXJlc3BvbiB7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gICAgbWFyZ2luLWJvdHRvbTogM3ZoOyB9IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvTmV3SG91c2VGb3JtL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9OZXdIb3VzZUZvcm0vbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4ubWFyLTV7XFxuICBtYXJnaW46IDUlO1xcbn1cXG4ucGFkZC01e1xcbiAgcGFkZGluZzogNSU7XFxufVxcbi5tYXItbGVmdHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDtcXG59XFxuLm1hci10b3B7XFxuICBtYXJnaW4tdG9wOiAzdmg7XFxufVxcbi5tYXItYm90dG9te1xcbiAgbWFyZ2luLWJvdHRvbTogMTMlO1xcbiAgbWFyZ2luLWxlZnQ6IDIuNXZ3O1xcbiAgd2lkdGg6IDkzJSAhaW1wb3J0YW50O1xcbn1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTtcXG59XFxuLmljb25zLWNse1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoO1xcblxcbn1cXG4ubWFyLXRvcC0xMHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7XFxufVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gIH1cXG4gIC5tYXItbGVmdHtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7XFxuICB9XFxuICAuZHJvcGRvd24tY29udGVudHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDtcXG4gIH1cXG5cXG59XFxuYnV0dG9uOmZvY3VzIHtcXG4gIG91dGxpbmU6IDA7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXSB7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7IH1cXG5mb290ZXIge1xcbiAgaGVpZ2h0OiAxM3ZoO1xcbiAgYmFja2dyb3VuZDogIzFjNTk1NjsgfVxcbi5uYXYtbG9nbyB7XFxuICBmbG9hdDogcmlnaHQ7IH1cXG4ubmF2LWNsIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBoZWlnaHQ6IDEwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICB6LWluZGV4OiA5OTk5OyB9XFxuLmZsZXgtY2VudGVyIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyOyB9XFxuLmZsZXgtbWlkZGxlIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IH1cXG4ucHVycGxlLWZvbnQge1xcbiAgY29sb3I6ICM1YjU1OWI7IH1cXG4uYmx1ZS1mb250IHtcXG4gIGNvbG9yOiAjNDRkMWNhOyB9XFxuLmdyZXktZm9udCB7XFxuICBjb2xvcjogIzk2OTY5NjsgfVxcbi5saWdodC1ncmV5LWZvbnQge1xcbiAgY29sb3I6ICM5NDk0OTQ7IH1cXG4uYmxhY2stZm9udCB7XFxuICBjb2xvcjogYmxhY2s7IH1cXG4ud2hpdGUtZm9udCB7XFxuICBjb2xvcjogd2hpdGU7IH1cXG4ucGluay1mb250IHtcXG4gIGNvbG9yOiAjZjA1ZDZjOyB9XFxuLnBpbmstYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kOiAjZjA1ZDZjOyB9XFxuLnB1cnBsZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICM1YTU1OWI7IH1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLm1hcmdpbi1hdXRvIHtcXG4gIG1hcmdpbjogYXV0bzsgfVxcbi5uby1wYWRkaW5nIHtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uaWNvbi1jbCB7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dzsgfVxcbi5mbGV4LXJvdy1yZXYge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IHJldmVyc2U7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlOyB9XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7IH1cXG4uc2VhcmNoLXRoZW1lIHtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLWhlaWdodCB7XFxuICBoZWlnaHQ6IDIwMHB4OyB9XFxuLmJvcmRlci1ib3R0b20ge1xcbiAgbWFyZ2luLWJvdHRvbTogMnZoO1xcbiAgbWFyZ2luLWxlZnQ6IDF2dztcXG4gIG1hcmdpbi1yaWdodDogMXZ3O1xcbiAgbWFyZ2luLXRvcDogMnZoO1xcbiAgYm9yZGVyLWJvdHRvbTogdGhpbiBzb2xpZCBibGFjazsgfVxcbi5tYXItbGVmdCB7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7IH1cXG4ubWFyLXRvcCB7XFxuICBtYXJnaW4tdG9wOiAzdmg7IH1cXG4ubWFyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7IH1cXG4uaWNvbnMtY2wge1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXIge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3VzIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4ubWFyLXRvcC0xMCB7XFxuICBtYXJnaW4tdG9wOiAyJTsgfVxcbi5wYWRkaW5nLXJpZ2h0LXByaWNlIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDclOyB9XFxuLm1hcmdpbi0xIHtcXG4gIG1hcmdpbi1sZWZ0OiAzJTtcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG4uaW1nLWJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7IH1cXG4ubWFpbi1mb3JtLXNlYXJjaC1yZXN1bHQge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgbWFyZ2luLXRvcDogLTclO1xcbiAgbWFyZ2luLWJvdHRvbTogNiU7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW0gIWltcG9ydGFudDsgfVxcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDsgfVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH1cXG4gIC5tYXItbGVmdCB7XFxuICAgIG1hcmdpbjogMDsgfVxcbiAgLmljb25zLWNsIHtcXG4gICAgd2lkdGg6IDEzdnc7XFxuICAgIGhlaWdodDogN3ZoOyB9XFxuICAuZm9vdGVyLXRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTJweDsgfVxcbiAgLmljb25zLW1hciB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlOyB9XFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7IH1cXG4gIC5kcm9wZG93bi1jb250ZW50IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgaGVpZ2h0OiAzNTBweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIGZsb2F0OiByaWdodDsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7SUFDSSxxQkFBcUI7Q0FDeEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGNBQWM7Q0FDakI7QUFDRDtJQUNJLHlCQUF5QjtDQUM1QjtBQUNEO0lBQ0ksd0JBQXdCO0NBQzNCO0FBQ0Q7SUFDSSwwQkFBMEI7Q0FDN0I7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDBCQUEwQjtRQUN0Qix1QkFBdUI7WUFDbkIsb0JBQW9CO0NBQy9CO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQyw2QkFBNkI7SUFDN0IsOEJBQThCO1FBQzFCLDJCQUEyQjtZQUN2Qix1QkFBdUI7Q0FDbEM7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksZ0RBQWdEO0lBQ2hELHdEQUF3RDtJQUN4RCxnREFBZ0Q7SUFDaEQsMkNBQTJDO0lBQzNDLHdDQUF3QztJQUN4Qyw2RUFBNkU7Q0FDaEY7QUFDRCxjQUFjLFVBQVUsQ0FBQztBQUN6QjtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsYUFBYTtFQUNiLCtDQUErQztVQUN2Qyx1Q0FBdUM7RUFDL0Msd0RBQXdEO0VBQ3hELGdEQUFnRDtFQUNoRCwyQ0FBMkM7RUFDM0Msd0NBQXdDO0VBQ3hDLDZFQUE2RTtDQUM5RTtBQUNEO0VBQ0UsV0FBVztFQUNYLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsdUJBQXVCO1VBQ25CLG9CQUFvQjtDQUM3QjtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QseUJBQXlCO01BQ3JCLHNCQUFzQjtVQUNsQix3QkFBd0I7Q0FDakM7QUFDRDtFQUNFLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsdUJBQXVCO0NBQ3hCO0FBQ0Q7RUFDRSx1QkFBdUI7O0NBRXhCO0FBQ0Q7RUFDRSx5QkFBeUI7Q0FDMUI7QUFDRDtFQUNFLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLDRCQUE0QjtDQUM3QjtBQUNEO0VBQ0UsMkJBQTJCO0NBQzVCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZTtDQUNoQjtBQUNEO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsV0FBVztDQUNaO0FBQ0Q7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLCtCQUErQjtFQUMvQiwrQkFBK0I7TUFDM0IsZ0NBQWdDO1VBQzVCLDRCQUE0QjtDQUNyQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjtBQUNEO0VBQ0UsZ0JBQWdCO0NBQ2pCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLG9CQUFvQjtFQUNwQixZQUFZO0NBQ2I7QUFDRDtFQUNFLFdBQVc7RUFDWCxZQUFZOztDQUViO0FBQ0Q7RUFDRSxlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Qsc0JBQXNCO0FBQ3RCO0VBQ0UsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFVBQVU7RUFDVixXQUFXO0NBQ1o7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0NBQ2I7QUFDRCwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7RUFDRDtJQUNFLFVBQVU7R0FDWDs7RUFFRDtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1osYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixvQkFBb0I7SUFDcEIsWUFBWTtHQUNiO0VBQ0Q7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVO0dBQ1g7O0NBRUY7QUFDRDtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0RBQWtEO1VBQzFDLDBDQUEwQztFQUNsRCx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFLEVBQUU7QUFDakY7RUFDRSxXQUFXO0VBQ1gsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYTtFQUNiLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsK0JBQStCO0VBQy9CLDhCQUE4QjtNQUMxQix3QkFBd0I7VUFDcEIsb0JBQW9CO0VBQzVCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixjQUFjLEVBQUU7QUFDbEI7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsdUJBQXVCO1VBQ25CLG9CQUFvQixFQUFFO0FBQ2hDO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QseUJBQXlCO01BQ3JCLHNCQUFzQjtVQUNsQix3QkFBd0IsRUFBRTtBQUNwQztFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBRTtBQUNuQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtFQUN0QiwrQkFBK0I7RUFDL0IsK0JBQStCO01BQzNCLGdDQUFnQztVQUM1Qiw0QkFBNEIsRUFBRTtBQUN4QztFQUNFLHNCQUFzQixFQUFFO0FBQzFCO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxZQUFZLEVBQUU7QUFDaEI7RUFDRSxvREFBb0Q7VUFDNUMsNENBQTRDO0VBQ3BELGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLGFBQWEsRUFBRTtBQUNqQjtFQUNFLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0Usc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixxQ0FBcUM7RUFDckMsNEJBQTRCLEVBQUU7QUFDaEM7RUFDRSxjQUFjLEVBQUU7QUFDbEI7RUFDRSxtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZ0NBQWdDLEVBQUU7QUFDcEM7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLFlBQVksRUFBRTtBQUNoQjtFQUNFLFdBQVc7RUFDWCxZQUFZLEVBQUU7QUFDaEI7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLGlDQUFpQyxFQUFFO0FBQ3JDO0VBQ0UsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxnQ0FBZ0MsRUFBRTtBQUNwQyxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQix3REFBd0Q7VUFDaEQsZ0RBQWdEO0VBQ3hELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVksRUFBRTtBQUNoQiwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUIsRUFBRTtFQUNyQjtJQUNFLFVBQVUsRUFBRTtFQUNkO0lBQ0UsWUFBWTtJQUNaLFlBQVksRUFBRTtFQUNoQjtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGtCQUFrQixFQUFFO0VBQ3RCO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2Isb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixZQUFZLEVBQUU7RUFDaEI7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsd0RBQXdEO1lBQ2hELGdEQUFnRDtJQUN4RCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVLEVBQUU7RUFDZDtJQUNFLG9EQUFvRDtZQUM1Qyw0Q0FBNEM7SUFDcEQsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsYUFBYSxFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4ubWFyLTV7XFxuICBtYXJnaW46IDUlO1xcbn1cXG4ucGFkZC01e1xcbiAgcGFkZGluZzogNSU7XFxufVxcbi5tYXItbGVmdHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDtcXG59XFxuLm1hci10b3B7XFxuICBtYXJnaW4tdG9wOiAzdmg7XFxufVxcbi5tYXItYm90dG9te1xcbiAgbWFyZ2luLWJvdHRvbTogMTMlO1xcbiAgbWFyZ2luLWxlZnQ6IDIuNXZ3O1xcbiAgd2lkdGg6IDkzJSAhaW1wb3J0YW50O1xcbn1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTtcXG59XFxuLmljb25zLWNse1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoO1xcblxcbn1cXG4ubWFyLXRvcC0xMHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7XFxufVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gIH1cXG4gIC5tYXItbGVmdHtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7XFxuICB9XFxuICAuZHJvcGRvd24tY29udGVudHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDtcXG4gIH1cXG5cXG59XFxuYnV0dG9uOmZvY3VzIHtcXG4gIG91dGxpbmU6IDA7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXSB7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7IH1cXG5mb290ZXIge1xcbiAgaGVpZ2h0OiAxM3ZoO1xcbiAgYmFja2dyb3VuZDogIzFjNTk1NjsgfVxcbi5uYXYtbG9nbyB7XFxuICBmbG9hdDogcmlnaHQ7IH1cXG4ubmF2LWNsIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBoZWlnaHQ6IDEwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICB6LWluZGV4OiA5OTk5OyB9XFxuLmZsZXgtY2VudGVyIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyOyB9XFxuLmZsZXgtbWlkZGxlIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IH1cXG4ucHVycGxlLWZvbnQge1xcbiAgY29sb3I6ICM1YjU1OWI7IH1cXG4uYmx1ZS1mb250IHtcXG4gIGNvbG9yOiAjNDRkMWNhOyB9XFxuLmdyZXktZm9udCB7XFxuICBjb2xvcjogIzk2OTY5NjsgfVxcbi5saWdodC1ncmV5LWZvbnQge1xcbiAgY29sb3I6ICM5NDk0OTQ7IH1cXG4uYmxhY2stZm9udCB7XFxuICBjb2xvcjogYmxhY2s7IH1cXG4ud2hpdGUtZm9udCB7XFxuICBjb2xvcjogd2hpdGU7IH1cXG4ucGluay1mb250IHtcXG4gIGNvbG9yOiAjZjA1ZDZjOyB9XFxuLnBpbmstYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kOiAjZjA1ZDZjOyB9XFxuLnB1cnBsZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICM1YTU1OWI7IH1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLm1hcmdpbi1hdXRvIHtcXG4gIG1hcmdpbjogYXV0bzsgfVxcbi5uby1wYWRkaW5nIHtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uaWNvbi1jbCB7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dzsgfVxcbi5mbGV4LXJvdy1yZXYge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IHJldmVyc2U7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlOyB9XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7IH1cXG4uc2VhcmNoLXRoZW1lIHtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLWhlaWdodCB7XFxuICBoZWlnaHQ6IDIwMHB4OyB9XFxuLmJvcmRlci1ib3R0b20ge1xcbiAgbWFyZ2luLWJvdHRvbTogMnZoO1xcbiAgbWFyZ2luLWxlZnQ6IDF2dztcXG4gIG1hcmdpbi1yaWdodDogMXZ3O1xcbiAgbWFyZ2luLXRvcDogMnZoO1xcbiAgYm9yZGVyLWJvdHRvbTogdGhpbiBzb2xpZCBibGFjazsgfVxcbi5tYXItbGVmdCB7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7IH1cXG4ubWFyLXRvcCB7XFxuICBtYXJnaW4tdG9wOiAzdmg7IH1cXG4ubWFyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7IH1cXG4uaWNvbnMtY2wge1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXIge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3VzIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4ubWFyLXRvcC0xMCB7XFxuICBtYXJnaW4tdG9wOiAyJTsgfVxcbi5wYWRkaW5nLXJpZ2h0LXByaWNlIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDclOyB9XFxuLm1hcmdpbi0xIHtcXG4gIG1hcmdpbi1sZWZ0OiAzJTtcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG4uaW1nLWJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7IH1cXG4ubWFpbi1mb3JtLXNlYXJjaC1yZXN1bHQge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgbWFyZ2luLXRvcDogLTclO1xcbiAgbWFyZ2luLWJvdHRvbTogNiU7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW0gIWltcG9ydGFudDsgfVxcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDsgfVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH1cXG4gIC5tYXItbGVmdCB7XFxuICAgIG1hcmdpbjogMDsgfVxcbiAgLmljb25zLWNsIHtcXG4gICAgd2lkdGg6IDEzdnc7XFxuICAgIGhlaWdodDogN3ZoOyB9XFxuICAuZm9vdGVyLXRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTJweDsgfVxcbiAgLmljb25zLW1hciB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlOyB9XFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7IH1cXG4gIC5kcm9wZG93bi1jb250ZW50IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgaGVpZ2h0OiAzNTBweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIGZsb2F0OiByaWdodDsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBlc2NhcGUodXJsKSB7XG4gICAgaWYgKHR5cGVvZiB1cmwgIT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiB1cmxcbiAgICB9XG4gICAgLy8gSWYgdXJsIGlzIGFscmVhZHkgd3JhcHBlZCBpbiBxdW90ZXMsIHJlbW92ZSB0aGVtXG4gICAgaWYgKC9eWydcIl0uKlsnXCJdJC8udGVzdCh1cmwpKSB7XG4gICAgICAgIHVybCA9IHVybC5zbGljZSgxLCAtMSk7XG4gICAgfVxuICAgIC8vIFNob3VsZCB1cmwgYmUgd3JhcHBlZD9cbiAgICAvLyBTZWUgaHR0cHM6Ly9kcmFmdHMuY3Nzd2cub3JnL2Nzcy12YWx1ZXMtMy8jdXJsc1xuICAgIGlmICgvW1wiJygpIFxcdFxcbl0vLnRlc3QodXJsKSkge1xuICAgICAgICByZXR1cm4gJ1wiJyArIHVybC5yZXBsYWNlKC9cIi9nLCAnXFxcXFwiJykucmVwbGFjZSgvXFxuL2csICdcXFxcbicpICsgJ1wiJ1xuICAgIH1cblxuICAgIHJldHVybiB1cmxcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL3VybC9lc2NhcGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL3VybC9lc2NhcGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vcm1hbGl6ZS5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub3JtYWxpemUuY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbm9ybWFsaXplLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS5jc3Mvbm9ybWFsaXplLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiaW1wb3J0IHsgbm90aWZ5U3VjY2Vzcywgbm90aWZ5RXJyb3IsIGdldExvZ2luSW5mbyB9IGZyb20gJy4uL3V0aWxzJztcbmltcG9ydCB7XG4gIEdFVF9DVVJSX1VTRVJfUkVRVUVTVCxcbiAgR0VUX0NVUlJfVVNFUl9TVUNDRVNTLFxuICBHRVRfQ1VSUl9VU0VSX0ZBSUxVUkUsXG4gIElOQ1JFQVNFX0NSRURJVF9SRVFVRVNULFxuICBJTkNSRUFTRV9DUkVESVRfU1VDQ0VTUyxcbiAgSU5DUkVBU0VfQ1JFRElUX0ZBSUxVUkUsXG4gIFVTRVJfTE9HSU5fUkVRVUVTVCxcbiAgVVNFUl9MT0dJTl9TVUNDRVNTLFxuICBVU0VSX0xPR0lOX0ZBSUxVUkUsXG4gIFVTRVJfTE9HT1VUX1JFUVVFU1QsXG4gIFVTRVJfTE9HT1VUX1NVQ0NFU1MsXG4gIFVTRVJfTE9HT1VUX0ZBSUxVUkUsXG4gIFVTRVJfSVNfTk9UX0xPR0dFRF9JTixcbiAgR0VUX0lOSVRfRkFJTFVSRSxcbiAgR0VUX0lOSVRfUkVRVUVTVCxcbiAgR0VUX0lOSVRfU1VDQ0VTUyxcbiAgR0VUX0lOSVRfVVNFUl9GQUlMVVJFLFxuICBHRVRfSU5JVF9VU0VSX1JFUVVFU1QsXG4gIEdFVF9JTklUX1VTRVJfU1VDQ0VTUyxcbn0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uL2hpc3RvcnknO1xuaW1wb3J0IHtcbiAgc2VuZGdldEN1cnJlbnRVc2VyUmVxdWVzdCxcbiAgc2VuZGluY3JlYXNlQ3JlZGl0UmVxdWVzdCxcbiAgc2VuZEluaXRpYXRlUmVxdWVzdCxcbiAgc2VuZEluaXRpYXRlVXNlcnNSZXF1ZXN0LFxuICBzZW5kTG9naW5SZXF1ZXN0LFxufSBmcm9tICcuLi9hcGktaGFuZGxlcnMvYXV0aGVudGljYXRpb24nO1xuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5SZXF1ZXN0KHVzZXJuYW1lLCBwYXNzd29yZCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HSU5fUkVRVUVTVCxcbiAgICBwYXlsb2FkOiB7XG4gICAgICB1c2VybmFtZSxcbiAgICAgIHBhc3N3b3JkLFxuICAgIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpblN1Y2Nlc3MoaW5mbykge1xuICBjb25zb2xlLmxvZyhpbmZvKTtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR0lOX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyBpbmZvIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpbkZhaWx1cmUoZXJyb3IpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR0lOX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnJvciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9hZExvZ2luU3RhdHVzKCkge1xuICBjb25zdCBsb2dpbkluZm8gPSBnZXRMb2dpbkluZm8oKTtcbiAgaWYgKGxvZ2luSW5mbykge1xuICAgIHJldHVybiBsb2dpblN1Y2Nlc3MobG9naW5JbmZvKTtcbiAgfVxuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfSVNfTk9UX0xPR0dFRF9JTixcbiAgfTtcbn1cblxuZnVuY3Rpb24gZ2V0Q2FwdGNoYVRva2VuKCkge1xuICBpZiAocHJvY2Vzcy5lbnYuQlJPV1NFUikge1xuICAgIGNvbnN0IGxvZ2luRm9ybSA9ICQoJyNsb2dpbkZvcm0nKS5nZXQoMCk7XG4gICAgaWYgKGxvZ2luRm9ybSAmJiB0eXBlb2YgbG9naW5Gb3JtWydnLXJlY2FwdGNoYS1yZXNwb25zZSddID09PSAnb2JqZWN0Jykge1xuICAgICAgcmV0dXJuIGxvZ2luRm9ybVsnZy1yZWNhcHRjaGEtcmVzcG9uc2UnXS52YWx1ZTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpblVzZXIodXNlcm5hbWUsIHBhc3N3b3JkKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChsb2dpblJlcXVlc3QodXNlcm5hbWUsIHBhc3N3b3JkKSk7XG4gICAgdHJ5IHtcbiAgICAgIC8vIGNvbnN0IGNhcHRjaGFUb2tlbiA9IGdldENhcHRjaGFUb2tlbigpO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZExvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpO1xuICAgICAgZGlzcGF0Y2gobG9naW5TdWNjZXNzKHJlcykpO1xuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2xvZ2luSW5mbycsIEpTT04uc3RyaW5naWZ5KHJlcykpO1xuICAgICAgLy8gY29uc29sZS5sb2cobG9jYXRpb24ucGF0aG5hbWUpO1xuICAgICAgY29uc3QgeyBwYXRobmFtZSB9ID0gZ2V0U3RhdGUoKS5ob3VzZTtcbiAgICAgIGNvbnNvbGUubG9nKHBhdGhuYW1lKTtcbiAgICAgIGhpc3RvcnkucHVzaChwYXRobmFtZSk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBkaXNwYXRjaChsb2dpbkZhaWx1cmUoZXJyKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0UmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR09VVF9SRVFVRVNULFxuICAgIHBheWxvYWQ6IHt9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0U3VjY2VzcygpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR09VVF9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHtcbiAgICAgIG1lc3NhZ2U6ICfYrtix2YjYrCDZhdmI2YHZgtuM2Kog2KLZhduM2LInLFxuICAgIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dvdXRGYWlsdXJlKGVycm9yKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dPVVRfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVycm9yIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dvdXRVc2VyKCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChsb2dvdXRSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBkaXNwYXRjaChsb2dvdXRTdWNjZXNzKCkpO1xuICAgICAgbG9jYWxTdG9yYWdlLmNsZWFyKCk7XG4gICAgICBsb2NhdGlvbi5hc3NpZ24oJy9sb2dpbicpO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgZGlzcGF0Y2gobG9nb3V0RmFpbHVyZShlcnIpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlKCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChpbml0aWF0ZVJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRJbml0aWF0ZVJlcXVlc3QoKTtcbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlU3VjY2VzcyhyZXMpKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZUZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9VU0VSX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1VTRVJfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9VU0VSX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlVXNlcnMoKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCkge1xuICAgIGRpc3BhdGNoKGluaXRpYXRlVXNlcnNSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QoKTtcbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlVXNlcnNTdWNjZXNzKHJlcykpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlVXNlcnNGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9DVVJSX1VTRVJfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9DVVJSX1VTRVJfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlckZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0NVUlJfVVNFUl9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlcih1c2VySWQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KHVzZXJJZCk7XG4gICAgICBkaXNwYXRjaChnZXRDdXJyZW50VXNlclN1Y2Nlc3MocmVzKSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXJGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IElOQ1JFQVNFX0NSRURJVF9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXRTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogSU5DUkVBU0VfQ1JFRElUX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXRGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IElOQ1JFQVNFX0NSRURJVF9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbmNyZWFzZUNyZWRpdChhbW91bnQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKGluY3JlYXNlQ3JlZGl0UmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBjdXJyVXNlciB9ID0gZ2V0U3RhdGUoKS5hdXRoZW50aWNhdGlvbjtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRpbmNyZWFzZUNyZWRpdFJlcXVlc3QoYW1vdW50LCBjdXJyVXNlci51c2VySWQpO1xuICAgICAgZGlzcGF0Y2goaW5jcmVhc2VDcmVkaXRTdWNjZXNzKHJlcykpO1xuICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXIoY3VyclVzZXIudXNlcklkKSk7XG4gICAgICBub3RpZnlTdWNjZXNzKCfYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixINio2Kcg2YXZiNmB2YLbjNiqINin2YbYrNin2YUg2LTYry4nKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBub3RpZnlFcnJvcign2K7Yt9inINmF2KzYr9ivINiq2YTYp9i0INqp2YbbjNivJyk7XG4gICAgICBkaXNwYXRjaChpbmNyZWFzZUNyZWRpdEZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2FjdGlvbnMvYXV0aGVudGljYXRpb24uanMiLCJpbXBvcnQge1xuICBDUkVBVEVfSE9VU0VfUkVRVUVTVCxcbiAgQ1JFQVRFX0hPVVNFX1NVQ0NFU1MsXG4gIENSRUFURV9IT1VTRV9GQUlMVVJFLFxuICBHRVRfSE9VU0VfUkVRVUVTVCxcbiAgR0VUX0hPVVNFX1NVQ0NFU1MsXG4gIEdFVF9IT1VTRV9GQUlMVVJFLFxuICBHRVRfSE9VU0VfUEhPTkVfUkVRVUVTVCxcbiAgR0VUX0hPVVNFX1BIT05FX1NVQ0NFU1MsXG4gIEdFVF9IT1VTRV9QSE9ORV9GQUlMVVJFLFxuICBHRVRfUEhPTkVfU1RBVFVTX1JFUVVFU1QsXG4gIEdFVF9QSE9ORV9TVEFUVVNfU1VDQ0VTUyxcbiAgR0VUX1BIT05FX1NUQVRVU19GQUlMVVJFLFxufSBmcm9tICcuLi9jb25zdGFudHMnO1xuaW1wb3J0IHsgZ2V0Q3VycmVudFVzZXIgfSBmcm9tICcuLi9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uJztcbmltcG9ydCB7XG4gIHNlbmRjcmVhdGVIb3VzZVJlcXVlc3QsXG4gIHNlbmRnZXRIb3VzZURldGFpbFJlcXVlc3QsXG4gIHNlbmRnZXRIb3VzZVBob25lTnVtYmVyUmVxdWVzdCxcbiAgc2VuZGdldFBob25lU3RhdHVzUmVxdWVzdCxcbn0gZnJvbSAnLi4vYXBpLWhhbmRsZXJzL2hvdXNlJztcbmltcG9ydCB7IG5vdGlmeUVycm9yLCBub3RpZnlTdWNjZXNzIH0gZnJvbSAnLi4vdXRpbHMnO1xuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlSG91c2VSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IENSRUFURV9IT1VTRV9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlSG91c2VTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogQ1JFQVRFX0hPVVNFX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlSG91c2VGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IENSRUFURV9IT1VTRV9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVIb3VzZShcbiAgYXJlYSxcbiAgYnVpbGRpbmdUeXBlLFxuICBhZGRyZXNzLFxuICBkZWFsVHlwZSxcbiAgYmFzZVByaWNlLFxuICByZW50UHJpY2UsXG4gIHNlbGxQcmljZSxcbiAgcGhvbmUsXG4gIGRlc2NyaXB0aW9uLFxuICBleHBpcmVUaW1lLFxuICBpbWFnZVVSTCxcbikge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gsIGdldFN0YXRlKSB7XG4gICAgZGlzcGF0Y2goY3JlYXRlSG91c2VSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGN1cnJVc2VyIH0gPSBnZXRTdGF0ZSgpLmF1dGhlbnRpY2F0aW9uO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGNyZWF0ZUhvdXNlUmVxdWVzdChcbiAgICAgICAgYXJlYSxcbiAgICAgICAgYnVpbGRpbmdUeXBlLFxuICAgICAgICBhZGRyZXNzLFxuICAgICAgICBkZWFsVHlwZSxcbiAgICAgICAgYmFzZVByaWNlLFxuICAgICAgICByZW50UHJpY2UsXG4gICAgICAgIHNlbGxQcmljZSxcbiAgICAgICAgcGhvbmUsXG4gICAgICAgIGRlc2NyaXB0aW9uLFxuICAgICAgICBleHBpcmVUaW1lLFxuICAgICAgICBpbWFnZVVSTCxcbiAgICAgICk7XG4gICAgICBkaXNwYXRjaChjcmVhdGVIb3VzZVN1Y2Nlc3MocmVzKSk7XG4gICAgICBub3RpZnlTdWNjZXNzKCfYrtin2YbZhyDYqNinINmF2YjZgdmC24zYqiDYq9io2Kog2LTYrycpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIG5vdGlmeUVycm9yKCfYrti32Kcg2YXYrNiv2K8g2KrZhNin2LQg2qnZhtuM2K8nKTtcbiAgICAgIGRpc3BhdGNoKGNyZWF0ZUhvdXNlRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRQaG9uZVN0YXR1c1JlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX1BIT05FX1NUQVRVU19SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UGhvbmVTdGF0dXNTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX1BIT05FX1NUQVRVU19TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFBob25lU3RhdHVzRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfUEhPTkVfU1RBVFVTX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFBob25lU3RhdHVzKGhvdXNlSWQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKGdldFBob25lU3RhdHVzUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBjdXJyVXNlciB9ID0gZ2V0U3RhdGUoKS5hdXRoZW50aWNhdGlvbjtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRnZXRQaG9uZVN0YXR1c1JlcXVlc3QoaG91c2VJZCk7XG4gICAgICBkaXNwYXRjaChnZXRQaG9uZVN0YXR1c1N1Y2Nlc3MocmVzKSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goZ2V0UGhvbmVTdGF0dXNGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldEhvdXNlRGV0YWlsUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSE9VU0VfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhvdXNlRGV0YWlsU3VjY2VzcyhyZXNwb25zZSwgcGF0aG5hbWUpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSE9VU0VfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlLCBwYXRobmFtZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SG91c2VEZXRhaWxGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9IT1VTRV9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRIb3VzZURldGFpbChob3VzZUlkLCBwYXRobmFtZSkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gsIGdldFN0YXRlKSB7XG4gICAgZGlzcGF0Y2goZ2V0SG91c2VEZXRhaWxSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGN1cnJVc2VyIH0gPSBnZXRTdGF0ZSgpLmF1dGhlbnRpY2F0aW9uO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGdldEhvdXNlRGV0YWlsUmVxdWVzdChob3VzZUlkKTtcbiAgICAgIGRpc3BhdGNoKGdldEhvdXNlRGV0YWlsU3VjY2VzcyhyZXMsIHBhdGhuYW1lKSk7XG4gICAgICBpZiAoY3VyclVzZXIpIHtcbiAgICAgICAgZGlzcGF0Y2goZ2V0UGhvbmVTdGF0dXMoaG91c2VJZCkpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIGRpc3BhdGNoKGdldEhvdXNlRGV0YWlsRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhvdXNlUGhvbmVOdW1iZXJSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9IT1VTRV9QSE9ORV9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SG91c2VQaG9uZU51bWJlclN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSE9VU0VfUEhPTkVfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRIb3VzZVBob25lTnVtYmVyRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSE9VU0VfUEhPTkVfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SG91c2VQaG9uZU51bWJlcihob3VzZUlkKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChnZXRIb3VzZVBob25lTnVtYmVyUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBjdXJyVXNlciB9ID0gZ2V0U3RhdGUoKS5hdXRoZW50aWNhdGlvbjtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRnZXRIb3VzZVBob25lTnVtYmVyUmVxdWVzdChob3VzZUlkKTtcbiAgICAgIGNvbnNvbGUubG9nKHJlcyk7XG4gICAgICBpZiAoIXJlcy5oYXNPd25Qcm9wZXJ0eSgnbWVzc2FnZScpICYmIHJlcyAhPT0gbnVsbCkge1xuICAgICAgICBkaXNwYXRjaChnZXRIb3VzZVBob25lTnVtYmVyU3VjY2VzcygncGF5ZWQnKSk7XG4gICAgICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyKGN1cnJVc2VyLnVzZXJJZCkpO1xuICAgICAgfSBlbHNlIGlmIChyZXMuaGFzT3duUHJvcGVydHkoJ21lc3NhZ2UnKSkge1xuICAgICAgICBkaXNwYXRjaChnZXRIb3VzZVBob25lTnVtYmVyU3VjY2Vzcygnbm9fY3JlZGl0JykpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKGdldEhvdXNlUGhvbmVOdW1iZXJGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9hY3Rpb25zL2hvdXNlLmpzIiwiaW1wb3J0IHsgZmV0Y2hBcGksIGdldEdldENvbmZpZywgZ2V0UG9zdENvbmZpZyB9IGZyb20gJy4uL3V0aWxzJztcbmltcG9ydCB7XG4gIGdldEN1cnJlbnRVc2VyVXJsLFxuICBnZXRJbml0aWF0ZVVybCxcbiAgbG9naW5BcGlVcmwsXG4gIGluY3JlYXNlQ3JlZGl0VXJsLFxufSBmcm9tICcuLi9jb25maWcvcGF0aHMnO1xuXG5leHBvcnQgZnVuY3Rpb24gc2VuZGluY3JlYXNlQ3JlZGl0UmVxdWVzdChhbW91bnQsIGlkKSB7XG4gIHJldHVybiBmZXRjaEFwaShpbmNyZWFzZUNyZWRpdFVybChhbW91bnQsIGlkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZGdldEN1cnJlbnRVc2VyUmVxdWVzdCh1c2VySWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEN1cnJlbnRVc2VyVXJsKHVzZXJJZCksIGdldEdldENvbmZpZygpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRJbml0aWF0ZVJlcXVlc3QoKSB7XG4gIHJldHVybiBmZXRjaEFwaShnZXRJbml0aWF0ZVVybCwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZEluaXRpYXRlVXNlcnNSZXF1ZXN0KCkge1xuICByZXR1cm4gZmV0Y2hBcGkobG9naW5BcGlVcmwsIGdldEdldENvbmZpZygpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRMb2dpblJlcXVlc3QodXNlcm5hbWUsIHBhc3N3b3JkKSB7XG4gIHJldHVybiBmZXRjaEFwaShcbiAgICBsb2dpbkFwaVVybCxcbiAgICBnZXRQb3N0Q29uZmlnKHtcbiAgICAgIHVzZXJuYW1lLFxuICAgICAgcGFzc3dvcmQsXG4gICAgfSksXG4gICk7XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2FwaS1oYW5kbGVycy9hdXRoZW50aWNhdGlvbi5qcyIsImltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBmZXRjaEFwaSwgZ2V0UG9zdENvbmZpZywgZ2V0R2V0Q29uZmlnIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHtcbiAgZ2V0Q3JlYXRlSG91c2VVcmwsXG4gIGdldEhvdXNlRGV0YWlsVXJsLFxuICBnZXRIb3VzZVBob25lVXJsLFxufSBmcm9tICcuLi9jb25maWcvcGF0aHMnO1xuLyoqXG4gKiBAYXBpIHtnZXR9IC92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvcGhvbmVOdW1iZXIvOnBob25lTnVtYmVyXG4gKiBAYXBpTmFtZSBEcml2ZXJBY3F1aXNpdGlvblNlYXJjaEJ5UGhvbmVOdW1iZXJcbiAqIEBhcGlWZXJzaW9uIDIuMC4wXG4gKiBAYXBpR3JvdXAgRHJpdmVyXG4gKiBAYXBpUGVybWlzc2lvbiBGSUVMRC1BR0VOVCwgVFJBSU5FUiwgQURNSU5cbiAqIEBhcGlQYXJhbSB7U3RyaW5nfSBwaG9uZU51bWJlciBwaG9uZSBudW1iZXIgdG8gc2VhcmNoICgrOTg5MTIzNDU2Nzg5LCAwOTEyMzQ1Njc4OSlcbiAqIEBhcGlTdWNjZXNzIHtTdHJpbmd9IHJlc3VsdCBBUEkgcmVxdWVzdCByZXN1bHRcbiAqIEBhcGlTdWNjZXNzRXhhbXBsZSB7anNvbn0gU3VjY2Vzcy1SZXNwb25zZTpcbiB7XG4gICBcInJlc3VsdFwiOiBcIk9LXCIsXG4gICBcImRhdGFcIjoge1xuICAgICAgIFwiZHJpdmVyc1wiOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkXCI6IDEwLFxuICAgICAgICAgIFwiZnVsbE5hbWVcIjogXCLYudmE24zYsdi22Kcg2YLYsdio2KfZhtuMXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIFwiaWRcIjogMTIsXG4gICAgICAgICAgXCJmdWxsTmFtZVwiOiBcIti52YTbjNix2LbYpyDZgtix2KjYp9mG24xcIlxuICAgICAgICB9XG4gICAgICAgXVxuICAgfVxuIH1cbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gc2VuZGNyZWF0ZUhvdXNlUmVxdWVzdChcbiAgYXJlYSxcbiAgYnVpbGRpbmdUeXBlLFxuICBhZGRyZXNzLFxuICBkZWFsVHlwZSxcbiAgYmFzZVByaWNlLFxuICByZW50UHJpY2UsXG4gIHNlbGxQcmljZSxcbiAgcGhvbmUsXG4gIGRlc2NyaXB0aW9uLFxuICBleHBpcmVUaW1lLFxuICBpbWFnZVVSTCxcbikge1xuICByZXR1cm4gZmV0Y2hBcGkoXG4gICAgZ2V0Q3JlYXRlSG91c2VVcmwsXG4gICAgZ2V0UG9zdENvbmZpZyh7XG4gICAgICBhcmVhLFxuICAgICAgYnVpbGRpbmdUeXBlLFxuICAgICAgYWRkcmVzcyxcbiAgICAgIGRlYWxUeXBlLFxuICAgICAgYmFzZVByaWNlLFxuICAgICAgcmVudFByaWNlLFxuICAgICAgc2VsbFByaWNlLFxuICAgICAgcGhvbmUsXG4gICAgICBkZXNjcmlwdGlvbixcbiAgICAgIGV4cGlyZVRpbWUsXG4gICAgICBpbWFnZVVSTCxcbiAgICB9KSxcbiAgKTtcbn1cblxuLyoqXG4gKiBAYXBpIHtnZXR9IC92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvcGhvbmVOdW1iZXIvOnBob25lTnVtYmVyXG4gKiBAYXBpTmFtZSBEcml2ZXJBY3F1aXNpdGlvblNlYXJjaEJ5UGhvbmVOdW1iZXJcbiAqIEBhcGlWZXJzaW9uIDIuMC4wXG4gKiBAYXBpR3JvdXAgRHJpdmVyXG4gKiBAYXBpUGVybWlzc2lvbiBGSUVMRC1BR0VOVCwgVFJBSU5FUiwgQURNSU5cbiAqIEBhcGlQYXJhbSB7U3RyaW5nfSBwaG9uZU51bWJlciBwaG9uZSBudW1iZXIgdG8gc2VhcmNoICgrOTg5MTIzNDU2Nzg5LCAwOTEyMzQ1Njc4OSlcbiAqIEBhcGlTdWNjZXNzIHtTdHJpbmd9IHJlc3VsdCBBUEkgcmVxdWVzdCByZXN1bHRcbiAqIEBhcGlTdWNjZXNzRXhhbXBsZSB7anNvbn0gU3VjY2Vzcy1SZXNwb25zZTpcbiB7XG4gICBcInJlc3VsdFwiOiBcIk9LXCIsXG4gICBcImRhdGFcIjoge1xuICAgICAgIFwiZHJpdmVyc1wiOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkXCI6IDEwLFxuICAgICAgICAgIFwiZnVsbE5hbWVcIjogXCLYudmE24zYsdi22Kcg2YLYsdio2KfZhtuMXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIFwiaWRcIjogMTIsXG4gICAgICAgICAgXCJmdWxsTmFtZVwiOiBcIti52YTbjNix2LbYpyDZgtix2KjYp9mG24xcIlxuICAgICAgICB9XG4gICAgICAgXVxuICAgfVxuIH1cbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gc2VuZGdldEhvdXNlRGV0YWlsUmVxdWVzdChob3VzZUlkKSB7XG4gIHJldHVybiBmZXRjaEFwaShnZXRIb3VzZURldGFpbFVybChob3VzZUlkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZGdldEhvdXNlUGhvbmVOdW1iZXJSZXF1ZXN0KGhvdXNlSWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEhvdXNlUGhvbmVVcmwoaG91c2VJZCksIGdldFBvc3RDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0UGhvbmVTdGF0dXNSZXF1ZXN0KGhvdXNlSWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEhvdXNlUGhvbmVVcmwoaG91c2VJZCksIGdldEdldENvbmZpZygpKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL2hvdXNlLmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZz8yZmMyZDVjMVwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nPzZmYTI4MDRjXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L25vLWRhbmdlciAqL1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgdHdpdHRlckljb24gZnJvbSAnLi9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmcnO1xuaW1wb3J0IGluc3RhZ3JhbUljb24gZnJvbSAnLi8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmcnO1xuaW1wb3J0IHRlbGVncmFtSWNvbiBmcm9tICcuLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZyc7XG5pbXBvcnQgcyBmcm9tICcuL21haW4uY3NzJztcblxuY2xhc3MgRm9vdGVyIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8Zm9vdGVyIGNsYXNzTmFtZT1cInNpdGUtZm9vdGVyIGNlbnRlci1jb2x1bW4gZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHdoaXRlXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTQgdGV4dC1hbGlnbi1sZWZ0XCI+XG4gICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwic29jaWFsLWljb25zLWxpc3Qgbm8tbGlzdC1zdHlsZVwiPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXtpbnN0YWdyYW1JY29ufVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic29jaWFsLW5ldHdvcmstaWNvblwiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJpbnN0YWdyYW0taWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17dHdpdHRlckljb259XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzb2NpYWwtbmV0d29yay1pY29uXCJcbiAgICAgICAgICAgICAgICAgIGFsdD1cInR3aXR0ZXItaWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17dGVsZWdyYW1JY29ufVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic29jaWFsLW5ldHdvcmstaWNvblwiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJ0ZWxlZ3JhbS1pY29uXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tOCB0ZXh0LWFsaWduLXJpZ2h0XCI+XG4gICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAg2KrZhdin2YXbjCDYrdmC2YjZgiDYp9uM2YYg2YjYqOKAjNiz2KfbjNiqINmF2KrYudmE2YIg2KjZhyDZhdmH2LHZhtin2LIg2KvYp9io2Kog2Ygg2YfZiNmF2YYg2LTYsduM2YEg2LLYp9iv2YdcbiAgICAgICAgICAgICAg2YXbjOKAjNio2KfYtNivLlxuICAgICAgICAgICAgPC9wPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZm9vdGVyPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShGb290ZXIpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuanMiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nPzI0OGJmYThkXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9MYXlvdXQuY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCIvKipcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcbiAqXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IG5vcm1hbGl6ZUNzcyBmcm9tICdub3JtYWxpemUuY3NzJztcbmltcG9ydCBzIGZyb20gJy4vTGF5b3V0LmNzcyc7XG5pbXBvcnQgRm9vdGVyIGZyb20gJy4uL0Zvb3Rlcic7XG5pbXBvcnQgUGFuZWwgZnJvbSAnLi4vUGFuZWwnO1xuXG5jbGFzcyBMYXlvdXQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMubm9kZS5pc1JlcXVpcmVkLFxuICAgIG1haW5QYW5lbDogUHJvcFR5cGVzLmJvb2wsXG4gIH07XG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgbWFpblBhbmVsOiBmYWxzZSxcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC0xMiBjb2wteHMtMTJcIiBzdHlsZT17eyBwYWRkaW5nOiAnMCcgfX0+XG4gICAgICAgIDxQYW5lbCBtYWluUGFuZWw9e3RoaXMucHJvcHMubWFpblBhbmVsfSAvPlxuICAgICAgICB7dGhpcy5wcm9wcy5jaGlsZHJlbn1cbiAgICAgICAgPEZvb3RlciAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKG5vcm1hbGl6ZUNzcywgcykoTGF5b3V0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmpzIiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgcyBmcm9tICcuL0xvYWRlci5zY3NzJztcbmltcG9ydCBnaWZGaWxlIGZyb20gJy4vbG9hZGVyLmdpZic7XG5cbmNsYXNzIExvYWRlciBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgbG9hZGluZzogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17dGhpcy5wcm9wcy5sb2FkaW5nID8gJ2xvYWRlciBsb2FkaW5nJyA6ICdsb2FkZXInfT5cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8aW1nIHNyYz17Z2lmRmlsZX0gYWx0PVwibG9hZGVyIGdpZlwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxvYWRlci1pbmZvXCI+2YXZhtiq2LjYsSDYqNin2LTbjNivPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoTG9hZGVyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9Mb2FkZXIvTG9hZGVyLmpzIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9Mb2FkZXIuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9Mb2FkZXIuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xvYWRlci5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvTG9hZGVyL0xvYWRlci5zY3NzXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0xvYWRlci9Mb2FkZXIuc2Nzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAzIDQgNiIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0xvYWRlci9sb2FkZXIuZ2lmP2JlNWMzMjIzXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvbG9hZGVyLmdpZlxuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Mb2FkZXIvbG9hZGVyLmdpZlxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAzIDQgNiIsIi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L25vLWRhbmdlciAqL1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgcyBmcm9tICcuL21haW4uY3NzJztcblxuY2xhc3MgTmV3SG91c2VGb3JtIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBoYW5kbGVTdWJtaXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBhcmVhOiAnJyxcbiAgICAgIGJ1aWxkaW5nVHlwZTogJycsXG4gICAgICBhZGRyZXNzOiAnJyxcbiAgICAgIGRlYWxUeXBlOiAnUkVOVCcsXG4gICAgICBiYXNlUHJpY2U6ICcnLFxuICAgICAgcmVudFByaWNlOiAnJyxcbiAgICAgIHNlbGxQcmljZTogJycsXG4gICAgICBwaG9uZTogJycsXG4gICAgICBkZXNjcmlwdGlvbjogJycsXG4gICAgICBleHBpcmVUaW1lOiAnJyxcbiAgICAgIGVycm9yczogZmFsc2UsXG4gICAgfTtcbiAgfVxuICBoYW5kbGVTdWJtaXQgPSBlID0+IHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgZXJyb3JzID0gdGhpcy52YWxpZGF0ZShcbiAgICAgIHRoaXMuc3RhdGUuYnVpbGRpbmdUeXBlLFxuICAgICAgdGhpcy5zdGF0ZS5hZGRyZXNzLFxuICAgICAgdGhpcy5zdGF0ZS5iYXNlUHJpY2UsXG4gICAgICB0aGlzLnN0YXRlLnJlbnRQcmljZSxcbiAgICAgIHRoaXMuc3RhdGUuc2VsbFByaWNlLFxuICAgICAgdGhpcy5zdGF0ZS5waG9uZSxcbiAgICAgIHRoaXMuc3RhdGUuYXJlYSxcbiAgICApO1xuICAgIGNvbnN0IGlzRW5hYmxlZCA9ICFPYmplY3Qua2V5cyhlcnJvcnMpLnNvbWUoeCA9PiBlcnJvcnNbeF0pO1xuICAgIGlmIChpc0VuYWJsZWQpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBlcnJvcnM6IGZhbHNlIH0pO1xuICAgICAgY29uc3QgZGVhbFR5cGUgPSB0aGlzLnN0YXRlLmRlYWxUeXBlID09PSAnUkVOVCcgPyAxIDogMDtcbiAgICAgIGxldCByZW50UHJpY2UgPSAwO1xuICAgICAgbGV0IHNlbGxQcmljZSA9IDA7XG4gICAgICBsZXQgYmFzZVByaWNlID0gMDtcbiAgICAgIGlmIChkZWFsVHlwZSkge1xuICAgICAgICBzZWxsUHJpY2UgPSAwO1xuICAgICAgICByZW50UHJpY2UgPSB0aGlzLnN0YXRlLnJlbnRQcmljZTtcbiAgICAgICAgYmFzZVByaWNlID0gdGhpcy5zdGF0ZS5iYXNlUHJpY2U7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZW50UHJpY2UgPSAwO1xuICAgICAgICBiYXNlUHJpY2UgPSAwO1xuICAgICAgICBzZWxsUHJpY2UgPSB0aGlzLnN0YXRlLnNlbGxQcmljZTtcbiAgICAgIH1cbiAgICAgIHRoaXMucHJvcHMuaGFuZGxlU3VibWl0KFxuICAgICAgICB0aGlzLnN0YXRlLmFyZWEsXG4gICAgICAgIHRoaXMuc3RhdGUuYnVpbGRpbmdUeXBlLFxuICAgICAgICB0aGlzLnN0YXRlLmFkZHJlc3MsXG4gICAgICAgIGRlYWxUeXBlLFxuICAgICAgICBiYXNlUHJpY2UsXG4gICAgICAgIHJlbnRQcmljZSxcbiAgICAgICAgc2VsbFByaWNlLFxuICAgICAgICB0aGlzLnN0YXRlLnBob25lLFxuICAgICAgICB0aGlzLnN0YXRlLmRlc2NyaXB0aW9uLFxuICAgICAgICAnJyxcbiAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBlcnJvcnM6IHRydWUgfSk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZUtleVByZXNzID0gZSA9PiB7XG4gICAgaWYgKGUua2V5ID09PSAnRW50ZXInKSB7XG4gICAgICB0aGlzLmhhbmRsZVN1Ym1pdChlKTtcbiAgICB9XG4gIH07XG4gIHZhbGlkYXRlID0gKFxuICAgIGJ1aWxkaW5nVHlwZSxcbiAgICBhZGRyZXNzLFxuICAgIGJhc2VQcmljZSxcbiAgICByZW50UHJpY2UsXG4gICAgc2VsbFByaWNlLFxuICAgIHBob25lLFxuICAgIGFyZWEsXG4gICkgPT4ge1xuICAgIC8vIHRydWUgbWVhbnMgaW52YWxpZCwgc28gb3VyIGNvbmRpdGlvbnMgZ290IHJldmVyc2VkXG4gICAgY29uc3QgcmVnZXggPSBuZXcgUmVnRXhwKC9bXjAtOV0vLCAnZycpO1xuICAgIGlmICh0aGlzLnN0YXRlLmRlYWxUeXBlID09PSAnUkVOVCcpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGJ1aWxkaW5nVHlwZTogYnVpbGRpbmdUeXBlLmxlbmd0aCA9PT0gMCxcbiAgICAgICAgYWRkcmVzczogYWRkcmVzcy5sZW5ndGggPT09IDAsXG4gICAgICAgIGJhc2VQcmljZTogYmFzZVByaWNlLm1hdGNoKHJlZ2V4KSxcbiAgICAgICAgcmVudFByaWNlOiByZW50UHJpY2UubWF0Y2gocmVnZXgpIHx8IHJlbnRQcmljZS5sZW5ndGggPT09IDAsXG4gICAgICAgIHNlbGxQcmljZTogZmFsc2UsXG4gICAgICAgIHBob25lOiBwaG9uZS5tYXRjaChyZWdleCkgfHwgcGhvbmUubGVuZ3RoID09PSAwLFxuICAgICAgICBhcmVhOiBhcmVhLm1hdGNoKHJlZ2V4KSB8fCBhcmVhLmxlbmd0aCA9PT0gMCxcbiAgICAgIH07XG4gICAgfVxuICAgIHJldHVybiB7XG4gICAgICBidWlsZGluZ1R5cGU6IGJ1aWxkaW5nVHlwZS5sZW5ndGggPT09IDAsXG4gICAgICBhZGRyZXNzOiAhYWRkcmVzcy5tYXRjaChyZWdleCkgfHwgYWRkcmVzcy5sZW5ndGggPT09IDAsXG4gICAgICBiYXNlUHJpY2U6IGZhbHNlLFxuICAgICAgcmVudFByaWNlOiBmYWxzZSxcbiAgICAgIHNlbGxQcmljZTogc2VsbFByaWNlLm1hdGNoKHJlZ2V4KSB8fCBzZWxsUHJpY2UubGVuZ3RoID09PSAwLFxuICAgICAgcGhvbmU6IHBob25lLm1hdGNoKHJlZ2V4KSB8fCBwaG9uZS5sZW5ndGggPT09IDAsXG4gICAgICBhcmVhOiBhcmVhLm1hdGNoKHJlZ2V4KSB8fCBhcmVhLmxlbmd0aCA9PT0gMCxcbiAgICB9O1xuICB9O1xuICByZW5kZXIoKSB7XG4gICAgY29uc3QgZXJyb3JzID0gdGhpcy52YWxpZGF0ZShcbiAgICAgIHRoaXMuc3RhdGUuYnVpbGRpbmdUeXBlLFxuICAgICAgdGhpcy5zdGF0ZS5hZGRyZXNzLFxuICAgICAgdGhpcy5zdGF0ZS5iYXNlUHJpY2UsXG4gICAgICB0aGlzLnN0YXRlLnJlbnRQcmljZSxcbiAgICAgIHRoaXMuc3RhdGUuc2VsbFByaWNlLFxuICAgICAgdGhpcy5zdGF0ZS5waG9uZSxcbiAgICAgIHRoaXMuc3RhdGUuYXJlYSxcbiAgICApO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaG9tZS1wYW5lbCBuby1wYWRkaW5nXCI+XG4gICAgICAgIDxzZWN0aW9uPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2VhcmNoLXRoZW1lIGZsZXgtY2VudGVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICAgICAgPGgzIGNsYXNzTmFtZT1cInRleHQtYWxpZ24tcmlnaHQgcmVnaXN0ZXItaGVhZGxpbmUgcGVyc2lhbi1ib2xkIGJsdWUtZm9udCBmbGV4LWNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICDYq9io2Kog2YXZhNqpINis2K/bjNivINiv2LEg2K7Yp9mG2Ycg2KjZhyDYr9mI2LRcbiAgICAgICAgICAgICAgICAgIDwvaDM+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWQgZm9ybS1jb250YWluZXJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IHJhZGlvXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkIHB1bGwtcmlnaHRcIj5cbiAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwicmFkaW8taW5saW5lICBwZXJzaWFuIGJsYWNrLWZvbnQgcmFkaW8tbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwicmFkaW9cIlxuICAgICAgICAgICAgICAgICAgICBuYW1lPVwiZGVhbFR5cGVcIlxuICAgICAgICAgICAgICAgICAgICBpZD1cImlubGluZVJhZGlvMVwiXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3RoaXMuc3RhdGUuZGVhbFR5cGUgPT09ICdSRU5UJ31cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cIlJFTlRcIlxuICAgICAgICAgICAgICAgICAgLz57JyAnfVxuICAgICAgICAgICAgICAgICAg2LHZh9mGINmIINin2KzYp9ix2YdcbiAgICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJyYWRpby1pbmxpbmUgcGVyc2lhbiBibGFjay1mb250IHJhZGlvLWxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInJhZGlvXCJcbiAgICAgICAgICAgICAgICAgICAgbmFtZT1cImRlYWxUeXBlXCJcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJpbmxpbmVSYWRpbzJcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IFtlLnRhcmdldC5uYW1lXTogZS50YXJnZXQudmFsdWUgfSk7XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPVwiU0VMTFwiXG4gICAgICAgICAgICAgICAgICAvPnsnICd9XG4gICAgICAgICAgICAgICAgICDYrtix24zYr1xuICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBmbGV4LW1pZGRsZVwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMCBmbGV4LW1pZGRsZS1yZXNwb25cIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtbWQtNVwiPlxuICAgICAgICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgICAgICAgIGh0bWxGb3I9XCJtZXRlclwiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBlcnNpYW4gbGlnaHQtZ3JleS1mb250IGFkZC1sYWJlbFwiXG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgINmF2KrYsSDZhdix2KjYuVxuICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICBpZD1cIm1ldGVyXCJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IFtlLnRhcmdldC5uYW1lXTogZS50YXJnZXQudmFsdWUgfSk7XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgIG5hbWU9XCJhcmVhXCJcblxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2B0ZXh0LWFsaWduLXJpZ2h0IHBlcnNpYW4gYmxhY2stZm9udCBhZGQtaW5wdXQgJHtcbiAgICAgICAgICAgICAgICAgICAgICBlcnJvcnMuYXJlYSAmJiB0aGlzLnN0YXRlLmVycm9ycyA/ICdlcnJvcicgOiAnJ1xuICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCLZhdiq2LHYp9qYXCJcbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICB7ZXJyb3JzLmFyZWEgJiZcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImVyci1tc2dcIj7ZhNi32YHYpyDZhdiq2LHYp9qYINix2Kcg2KjZhyDYudiv2K8g2YjYp9ix2K8g2qnZhtuM2K8uPC9wPlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyICBjb2wtbWQtNVwiPlxuICAgICAgICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgICAgICAgIGh0bWxGb3I9XCJ0eXBlLWlucFwiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBlcnNpYW4gbGlnaHQtZ3JleS1mb250IGFkZC1sYWJlbCBlbXB0eS1sYWJlbC1oZWlnaHRcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDxzZWxlY3RcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJ0eXBlLWlucFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgbmFtZT1cImJ1aWxkaW5nVHlwZVwiXG5cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGxpZ2h0LWdyZXktZm9udCBhZGQtaW5wdXQgcHVsbC1sZWZ0ICR7XG4gICAgICAgICAgICAgICAgICAgICAgZXJyb3JzLmJ1aWxkaW5nVHlwZSAmJiB0aGlzLnN0YXRlLmVycm9ycyA/ICdlcnJvcicgOiAnJ1xuICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiBkaXNhYmxlZCBzZWxlY3RlZD5cbiAgICAgICAgICAgICAgICAgICAgICDZhtmI2Lkg2YXZhNqpXG4gICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPtmI24zZhNin24zbjDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPtii2b7Yp9ix2KrZhdin2YY8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxuICAgICAgICAgICAgICAgICAge2Vycm9ycy5idWlsZGluZ1R5cGUgJiZcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImVyci1tc2dcIj7ZhNi32YHYpyDZhtmI2Lkg2YXZhNqpINix2Kcg2KfZhtiq2K7Yp9ioINqp2YbbjNivLjwvcD5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGZsZXgtbWlkZGxlXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEwIGZsZXgtbWlkZGxlLXJlc3BvblwiPlxuICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLmRlYWxUeXBlID09PSAnUkVOVCcgJiYgKFxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLW1kLTVcIj5cbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgICAgICAgICAgaHRtbEZvcj1cInByaWNlMVwiXG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicGVyc2lhbiBsaWdodC1ncmV5LWZvbnQgYWRkLWxhYmVsXCJcbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgINmF2KrYsSDZhdix2KjYuVxuICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICBpZD1cInByaWNlMVwiXG4gICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgbmFtZT1cImJhc2VQcmljZVwiXG5cbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2BwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgYmxhY2stZm9udCBhZGQtaW5wdXQgJHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9ycy5iYXNlUHJpY2UgJiYgdGhpcy5zdGF0ZS5lcnJvcnMgPyAnZXJyb3InIDogJydcbiAgICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cItmC24zZhdiqINix2YfZhlwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIHtlcnJvcnMuYmFzZVByaWNlICYmXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiZXJyLW1zZ1wiPtmE2LfZgdinINmC24zZhdiqINix2YfZhiDYsdinINio2Ycg2LnYr9ivINqp2YbbjNivLjwvcD5cbiAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS5kZWFsVHlwZSA9PT0gJ1NFTEwnICYmIChcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1tZC01XCI+XG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbFxuICAgICAgICAgICAgICAgICAgICAgIGh0bWxGb3I9XCJwcmljZTFcIlxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBlcnNpYW4gbGlnaHQtZ3JleS1mb250IGFkZC1sYWJlbFwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICDZhdiq2LEg2YXYsdio2LlcbiAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgaWQ9XCJwcmljZTFcIlxuICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJzZWxsUHJpY2VcIlxuXG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsYWNrLWZvbnQgYWRkLWlucHV0ICR7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnMuc2VsbFByaWNlICYmIHRoaXMuc3RhdGUuZXJyb3JzID8gJ2Vycm9yJyA6ICcnXG4gICAgICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCLZgtuM2YXYqiDZgdix2YjYtFwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIHtlcnJvcnMuc2VsbFByaWNlICYmXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiZXJyLW1zZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICDZhNi32YHYpyDZgtuM2YXYqiDZgdix2YjYtCDYsdinINio2Ycg2LnYr9ivINqp2YbbjNivLlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyICBjb2wtbWQtNVwiPlxuICAgICAgICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgICAgICAgIGh0bWxGb3I9XCJhZGRyZXNzXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicGVyc2lhbiBlbXB0eS1sYWJlbC1oZWlnaHQgbGlnaHQtZ3JleS1mb250IGFkZC1sYWJlbFwiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgIGlkPVwiYWRkcmVzc1wiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgbmFtZT1cImFkZHJlc3NcIlxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG5cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsYWNrLWZvbnQgYWRkLWlucHV0ICR7XG4gICAgICAgICAgICAgICAgICAgICAgZXJyb3JzLmFkZHJlc3MgJiYgdGhpcy5zdGF0ZS5lcnJvcnMgPyAnZXJyb3InIDogJydcbiAgICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwi2KLYr9ix2LNcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIHtlcnJvcnMuYWRkcmVzcyAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmVycm9ycyAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiZXJyLW1zZ1wiPtmE2LfZgdinINii2K/YsdizINix2Kcg2YjYp9ix2K8g2qnZhtuM2K8uPC9wPlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1taWRkbGVcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTAgZmxleC1taWRkbGUtcmVzcG9uXCI+XG4gICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuZGVhbFR5cGUgPT09ICdSRU5UJyAmJiAoXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtbWQtNVwiPlxuICAgICAgICAgICAgICAgICAgICA8bGFiZWxcbiAgICAgICAgICAgICAgICAgICAgICBodG1sRm9yPVwicHJpY2UyXCJcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwZXJzaWFuIGxpZ2h0LWdyZXktZm9udCBhZGQtbGFiZWxcIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAg2YXYqtixINmF2LHYqNi5XG4gICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgIGlkPVwicHJpY2UyXCJcbiAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJyZW50UHJpY2VcIlxuICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcblxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YHBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBibGFjay1mb250IGFkZC1pbnB1dCAke1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzLnJlbnRQcmljZSAmJiB0aGlzLnN0YXRlLmVycm9ycyA/ICdlcnJvcicgOiAnJ1xuICAgICAgICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwi2YLbjNmF2Kog2KfYrNin2LHZh1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIHtlcnJvcnMucmVudFByaWNlICYmXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiZXJyLW1zZ1wiPtmE2LfZgdinINmC24zZhdiqINin2KzYp9ix2Ycg2LHYpyDZiNin2LHYryDaqdmG24zYry48L3A+XG4gICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgIGNvbC1tZC01XCI+XG4gICAgICAgICAgICAgICAgICA8bGFiZWxcbiAgICAgICAgICAgICAgICAgICAgaHRtbEZvcj1cInBob25lXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicGVyc2lhbiBlbXB0eS1sYWJlbC1oZWlnaHQgbGlnaHQtZ3JleS1mb250IGFkZC1sYWJlbFwiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgIGlkPVwicGhvbmVcIlxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGVsXCJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICBuYW1lPVwicGhvbmVcIlxuXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YHBlcnNpYW4gYmxhY2stZm9udCAgdGV4dC1hbGlnbi1yaWdodCAgYWRkLWlucHV0ICR7XG4gICAgICAgICAgICAgICAgICAgICAgZXJyb3JzLnBob25lICYmIHRoaXMuc3RhdGUuZXJyb3JzID8gJ2Vycm9yJyA6ICcnXG4gICAgICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIti02YXYp9ix2Ycg2KrZhdin2LNcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIHtlcnJvcnMucGhvbmUgJiZcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImVyci1tc2dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgINmE2LfZgdinINi02YXYp9ix2Ycg2KrZhdin2LMg2LHYpyDYqNmHINi52K/YryDZiNin2LHYryDaqdmG24zYry5cbiAgICAgICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBmbGV4LW1pZGRsZVwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMCBmbGV4LW1pZGRsZS1yZXNwb25cIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtbWQtMTAgXCI+XG4gICAgICAgICAgICAgICAgICA8bGFiZWxcbiAgICAgICAgICAgICAgICAgICAgaHRtbEZvcj1cImV4cGxcIlxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwZXJzaWFuIGxpZ2h0LWdyZXktZm9udCBhZGQtbGFiZWxcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICBpZD1cImV4cGxcIlxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgbmFtZT1cImRlc2NyaXB0aW9uXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsYWNrLWZvbnQgYWRkLWlucHV0XCJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCLYqtmI2LbbjNit2KfYqlwiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1taWRkbGVcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTAgZmxleC1taWRkbGUtcmVzcG9uXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTAgXCI+XG4gICAgICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIlxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZVN1Ym1pdH1cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICBibHVlLWJ1dHRvbiAgdGV4dC1hbGlnbi1jZW50ZXIgcGVyc2lhbiB3aGl0ZS1mb250XCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAg2KvYqNiqINmF2YTaqVxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvc2VjdGlvbj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShOZXdIb3VzZUZvcm0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL05ld0hvdXNlRm9ybS9OZXdIb3VzZUZvcm0uanMiLCJtb2R1bGUuZXhwb3J0cyA9IFwiZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFaQUFBQUdRQ0FNQUFBQzNZY2IrQUFBQU0xQk1WRVg1K3Z2cjdPM3k4L1R1Ny9ENCtmbnM3ZTczK1BqdDcvRDI5dmYzK2ZudDd1LzA5Zlh4OHZQdjhQSDE5dmZ5OHZQejlQVVE2VldZQUFBSDRrbEVRVlI0WHUzZDJYWVR2eExGNGRvYWh4N2YvMm1QblVBNmVKbi9TYUFkS2ViMzNRQyt5WUtOcWlSMVc3Si9Ed0FBQUFBQUFBQUFBQUFnMkZEZ2ZiRnhvRWk2Um9KMTh4ZkxYcTJyT3VzYlJWSldlNUNwNlNlZmlPU2ppdUw4aUV5QzEwWDIza2RkT1BzZ0lvblNBekpKVFlxdTJsV1pKYzMyTVVReVN6bzlrNUFsSCt5bkVqK2NDSkZNMHJ5Y25jbHlFMENLMHNjQ0o1SWdiUmFtVXpNcFVyWmZKS21aRFJMSmJFUExhbVoyWkxLZk1rQ1MvY3BKa3cwU1NiV2hPU2tkTXlQNWNNYWdXKzUrOWhIc3BDUnB0NnV3bkRRL0xmZEd3NkpvSDRJb2J4Y3BTM0U5YWN6Vit4OStCR1lwbUUxUnl2V3NJbmgzMkJUN0NFelNhcHVrT2RnQWdTQklTNWJpWkllZUpRdFpGeTNab1d0VHh5NXBDWGJvTysxRmtwd2R1aThNa1lzZEJ0ODZRZi9OUmZUZmZzZTRENmpRL3hFdUh2K1NBM2dONlBFQUFBQUFBTUM2TEtzTkE2c3V1aVZTM2lSN2dVVVgzdm9vT2lUcmprQ3lEdDdlVUxJbTYyS1g1dkpxK1ZrM3NYcmZLWThRRllPOXFsS3p2ckJKdS8za0pHZjk0SFpRaEtaWXJTTjRxZHg4TjlQNlFaR1dPd0gxZ2liVjI0UzhkVWJGR3FabW9VclpiTkN1enF5My83d1h4N3J3cWtZMTZ3cTd0TmxQZyt5ZE1ORWFhSXFGSW1YM0trdkplc09pdzJ6ZG9lck5qLzRPQUFBQUFBQUFBQUFBQUFBQUFKdmtnNDBDa3k0V0d3V2NGQ1ViQlhaZFpCc0ZRcGEwMmpBUXByMWFSd0FBQUFBQUFBQUFBS2h0c29IQVM1dDlBb3E3VmV4RWUveGNJbkM2NWV4TUtRNzEySjlBTEgzMmVEQUNzZmNrOTRDTFUyd2NCR0plcXZZUWF5V1FQN0JLdXoxQW1CVlhBdmtEVGQ3T2w1b3U1a0FnbnpaTGozbVB0a25LaVVBK2E1S1NuU3Q0U1Z0SVdaK3Zod1JTcEdLbktsR3YvU1BNa3BaZ1hSSElKc2xYZTdGR0tSYnJoMEJxbHVSdS85Z0xQV1NLVWlzM28xeTVXZ2ZNc282bWNhZWxkTUE2cEVseHQxdGhVWjlkWlZicVRUSDlacVR2N1BaK1RKYkNtUk9zK3NIUDd5T1FtalhiYWRKdlJrSlQ1b25oUjdsZzU3bi9MNTk2dFJEY3IwMmJsS3dIM0o4aU5EWHJBMUY1cElxRitVN04ydnRWTEt6U1pEZHl4NHFGcU1WK1ZYdFdMTXhTR0xsaVViUDZWeXhxMWtnVkM3UGszdXQ4K1RaVzNZcldFWUp1emZZZEFBQUF3RjBWR3dYKzYvVUNFQWdrZjFvZ1lJUVFDQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBUUNyRnhvQzBMMDBYTmdCVTEvU0RkWWM2NnlwNnR3OVFzdUNpcExZbEd3RlNsdVJYKzc3VzFaN0lHcVU0MlZkNFZEbk15dlkwcGkrNy83VTJLUWM3WDVXMnA4cGpzaTh4UCtvSWxsMUs1UEY1WGhmZXp1Y1Y3VW1rZU9UeGJVZElrR1o3RHFFZGVYemZIakpKcXoySDdZdi9iNVZrRDdCSTloeVMxTUp6M0FEeUhMeFVudVNPbktlUUpQOHN0MGc5aGZrSnB1K24zU1FjK20vbGhlY1lJRW5hVDZyZm5UT1pucUQybm5qMytTN3BoRXlvdlZudHJKRVcrMmJTT2xTc2tUY1dtM0xkczY1eTZOTkNuSDEvdTVUT3JCaE9VcTdXUVhtT0RRZXZlT1o2WnBNMGgxN3JxV0lkREx1eEdLUWxxOTlNeDBuV3djQWJpMTRYTFJtQkRMS3h1Qi9QVG51b3BkaDdiQ3pXd2FZNWJDem1ZaytGeFMxY3pEWVVWUHMwQUFBQUFBQUFBQUFBQUFBQUFNa3RYc3ArVzYwL1RFMXZvZ3ZXSDRmUEtmclorZGZmN2RZUkprbHhTL1lpVEUzU0hLd1hUTGNCN0ZIS0pETFFZV2MxUzdOMWdSU2xaRGRDbGxickFmbnV5UTgxcWxrSG1ONFZweWxMV2w3amNYMU82RUJURE1mYzk0VVBaaGE2bkIrR1NYTEhRWTF4YzNPVXZGMHNpdmJsc0x3TmtQbkgzTGYrYUNwT3F2YlY4TlpCcXVURFN5SkZXbDUvS2ZiRlVONWF0NVBLL3JvY2JHcWRBc0ZSbDd5YWVla0l4RW5CME8wZ0RpLy80d2llVlZvNk5YWE12d1N5U1g2ZG1qUjFtdmJDS3gvUmhCcjF3dmRmR0ZLeUpzbFphcnBZZ2xucXZIVkNJTllVazFseFUrMjN1WWpwbU51dVV2eVpRV3Bzdi9jL3JuaVQ1S2RrZFowbFpVUHZBNzJucURlYmRZSDUvWTVWbmFOZUxNWDZRTHBwRm1WM3JnVERtSmVOZ090NHNFbUxqUVBoLzd6eTQ1SjlLWVQ0WHdjTno0b2RFdUhOckp6c251bzFkQ0I3c0lkYnZWODdWQzI1Y1A5QzlqWndIcFBpL3ZpZmNmSGxpY3lTb3F2Mlh0aWJqbUkySkMrcGxZZi9qQjdUbnRKMHNlekpYcVZwMFVXY2JHaFRsT1RyRXdaeWZHV25lZS96dC9ubVRuQ1N5bE9Wck1NNlJ4MjBUUFlORk1rL3Vxa3ZxL1dTZHVldjNGN3NlMmhTdFdGZ0grcEpBVUpVRERZTWJGSXJOZ3k0S01tUEV3bnFySXU1MmxDSXBOZ3dNUDErSVEwbVdwaWwzWWFCSW1VYkI4S3NaQ05CdFJFQkFBQUFBQUFBQUFBQUFBQUFBSUJhcm13VWNMcXl1MEFnY0pML1pDQmdoQkFJbUdVQkFBQUFBQUFBQUFBQUFBQUFrckwzZm5iT1RhVlU2d3o2aGJNL0V1d3NrUHpWM3dYU1Rrc0U3MEpJZjFxeWtweWRpRUQrMWk1Vkd3ZUJ6SkszY1JCSWxyVGFLQWdrNkdLY3ZrNGdSZkk2cTYvN3E0bEEvb2FUNm1tM3FPcktFY2pmV0JUUHUyZFlhdjlhSUtHa2N3T0pXc3lXUCszckJGTDBrNy80KzBEcVM2WTFxZ1ZLMXQ4Rm9sTUs5aXFWMTA3aWFPcC9HTWpzcmpaL3l0OStrOEpmM0k2T1NVcDJJcTlzRjMvYTErRWtPNU0wMjhYUjE3c2lrQ1I1dHhZNytucFhCTExyVmZSekhtQitSQ0JsOWpwMDdPc0VjZ2hsY292WGxiZStDT1EybDJxRFN2dm1YeXh1cllNSDh2eldKZXE5dG8yVHlhSm0vNWJnb2w1a2Y5WDB3aGNiZy8vWDZ2eHJITXVlanRLNk5WMzRaQ1BZOGo4VlNNcVMyaDV1UDU1MXNka1h3eVFwVG5aSG5TWGxhbDhKczZRdDJIMmxTVEhaRjhLc3VOcHZoZm1yRThHVzdMOXNVck9CWVByUlJRQUFBQUFBQUFBQUFQNEh3ZDR3VmM4WGY5b0FBQUFBU1VWT1JLNUNZSUk9XCJcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL05ld0hvdXNlRm9ybS9nZW9tZXRyeTIucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL05ld0hvdXNlRm9ybS9nZW9tZXRyeTIucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL05ld0hvdXNlRm9ybS9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9OZXdIb3VzZUZvcm0vbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiLyogZXNsaW50LWRpc2FibGUgcmVhY3Qvbm8tZGFuZ2VyICovXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgKiBhcyBhdXRoQWN0aW9ucyBmcm9tICcuLi8uLi9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uJztcbmltcG9ydCBzIGZyb20gJy4vbWFpbi5jc3MnO1xuaW1wb3J0IGxvZ29JbWcgZnJvbSAnLi9sb2dvLnBuZyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi8uLi9oaXN0b3J5JztcblxuY2xhc3MgUGFuZWwgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGN1cnJVc2VyOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgYXV0aEFjdGlvbnM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBtYWluUGFuZWw6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gICAgaXNMb2dnZWRJbjogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5jdXJyVXNlcik7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5pc0xvZ2dlZEluKTtcbiAgICBpZiAodGhpcy5wcm9wcy5tYWluUGFuZWwpIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPVwiaGlkZGVuLXhzXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGhlYWRlci1tYWluXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleC1jZW50ZXIgY29sLW1kLTIgICBuYXYtdXNlciBjb2wteHMtMTIgZHJvcGRvd24gXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGZsZXgtY2VudGVyIGhlYWRlci1tYWluLWJvYXJkZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtOSBcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCB3aGl0ZS1mb250IHBlcnNpYW4gXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICDZhtin2K3bjNmHINqp2KfYsdio2LHbjFxuICAgICAgICAgICAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkcm9wZG93bi1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlciAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgYmxhY2stZm9udCBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlci51c2VybmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNiBwdWxsLWxlZnQgcGVyc2lhbiBsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINiq2YjZhdin2YZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLmJhbGFuY2V9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNiBsaWdodC1ncmV5LWZvbnQgcHVsbC1yaWdodCAgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYp9i52KrYqNin2LFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9pbmNyZWFzZUNyZWRpdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItiu2LHZiNisXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmF1dGhBY3Rpb25zLmxvZ291dFVzZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgeyF0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2YjYsdmI2K9cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvbG9naW4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuby1wYWRkaW5nICBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXNtaWxlLW8gZmEtMnggd2hpdGUtZm9udFwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvaGVhZGVyPlxuICAgICAgKTtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgIDxoZWFkZXI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbmF2LWNsXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgtY2VudGVyIGNvbC1tZC0yICAgbmF2LXVzZXIgY29sLXhzLTEyIGRyb3Bkb3duXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1jZW50ZXIgXCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy05IFwiPlxuICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBwZXJzaWFuIHB1cnBsZS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAg2YbYp9it24zZhyDaqdin2LHYqNix24xcbiAgICAgICAgICAgICAgICAgICAgPC9oNT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkcm9wZG93bi1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cImJsYWNrLWZvbnQgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsdWUtZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlci51c2VybmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiIGxpZ2h0LWdyZXktZm9udCBjb2wteHMtNiBwdWxsLWxlZnQgYmx1ZS1mb250IHBlcnNpYW5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KrZiNmF2KfZhlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLmJhbGFuY2V9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsaWdodC1ncmV5LWZvbnQgY29sLXhzLTYgcHVsbC1yaWdodCBibHVlLWZvbnQgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KfYudiq2KjYp9ixXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2KfZgdiy2KfbjNi0INin2LnYqtio2KfYsVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvaW5jcmVhc2VDcmVkaXQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItiu2LHZiNisXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuYXV0aEFjdGlvbnMubG9nb3V0VXNlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgeyF0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItmI2LHZiNivXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvbG9naW4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuby1wYWRkaW5nICBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1zbWlsZS1vIGZhLTJ4IHB1cnBsZS1mb250XCIgLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4LWNlbnRlciBjb2wtbWQtNCBjb2wtbWQtb2Zmc2V0LTYgbmF2LWxvZ28gY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgIFwiPlxuICAgICAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZmxleC1yb3ctcmV2XCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnLycpO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vLXBhZGRpbmcgdGV4dC1hbGlnbi1jZW50ZXIgY29sLXhzLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cImljb24tY2xcIiBzcmM9e2xvZ29JbWd9IGFsdD1cImxvZ29cIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtOVwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJwZXJzaWFuLWJvbGQgdGV4dC1hbGlnbi1yaWdodCAgcGVyc2lhbiBibHVlLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgINiu2KfZhtmHINio2Ycg2K/ZiNi0XG4gICAgICAgICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9oZWFkZXI+XG4gICAgKTtcbiAgfVxufVxuZnVuY3Rpb24gbWFwRGlzcGF0Y2hUb1Byb3BzKGRpc3BhdGNoKSB7XG4gIHJldHVybiB7XG4gICAgYXV0aEFjdGlvbnM6IGJpbmRBY3Rpb25DcmVhdG9ycyhhdXRoQWN0aW9ucywgZGlzcGF0Y2gpLFxuICB9O1xufVxuXG5mdW5jdGlvbiBtYXBTdGF0ZVRvUHJvcHMoc3RhdGUpIHtcbiAgY29uc3QgeyBhdXRoZW50aWNhdGlvbiB9ID0gc3RhdGU7XG4gIGNvbnN0IHsgY3VyclVzZXIsaXNMb2dnZWRJbiB9ID0gYXV0aGVudGljYXRpb247XG4gIHJldHVybiB7XG4gICAgY3VyclVzZXIsXG4gICAgaXNMb2dnZWRJbixcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMocykoUGFuZWwpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9QYW5lbC9QYW5lbC5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nPzJhZjczZDhhXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9sb2dvLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9sb2dvLnBuZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIi8vIG5vaW5zcGVjdGlvbiBKU0Fubm90YXRvclxuY29uc3QgbGV2ZWxzID0gW1xuICB7XG4gICAgTUFYX1dJRFRIOiA2MDAsXG4gICAgTUFYX0hFSUdIVDogNjAwLFxuICAgIERPQ1VNRU5UX1NJWkU6IDMwMDAwMCxcbiAgfSxcbiAge1xuICAgIE1BWF9XSURUSDogODAwLFxuICAgIE1BWF9IRUlHSFQ6IDgwMCxcbiAgICBET0NVTUVOVF9TSVpFOiA1MDAwMDAsXG4gIH0sXG5dO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGdldExldmVsKGxldmVsKSB7XG4gICAgcmV0dXJuIGxldmVsc1tsZXZlbCAtIDFdO1xuICB9LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29uZmlnL2NvbXByZXNzaW9uQ29uZmlnLmpzIiwiLyogZXNsaW50LWRpc2FibGUgcHJldHRpZXIvcHJldHRpZXIgKi9cbmltcG9ydCB7IGFwaSB9IGZyb20gJy4vdXJscyc7XG5cbmV4cG9ydCBjb25zdCBsb2dpbkFwaVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2xvZ2luYDtcbmV4cG9ydCBjb25zdCBsb2dvdXRBcGlVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9sb2dpbmA7XG5leHBvcnQgY29uc3QgZ2V0U2VhcmNoSG91c2VzVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvU2VhcmNoU2VydmxldGA7XG5leHBvcnQgY29uc3QgZ2V0Q3JlYXRlSG91c2VVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvSG91c2VTZXJ2bGV0YDtcbmV4cG9ydCBjb25zdCBnZXRJbml0aWF0ZVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2hvbWVgO1xuZXhwb3J0IGNvbnN0IGdldEN1cnJlbnRVc2VyVXJsID0gKHVzZXJJZCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0luZGl2aWR1YWxTZXJ2bGV0P3VzZXJJZD0ke3VzZXJJZH1gO1xuZXhwb3J0IGNvbnN0IGdldEhvdXNlRGV0YWlsVXJsID0gKGhvdXNlSWQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9HZXRTaW5nbGVIb3VzZT9pZD0ke2hvdXNlSWR9YDtcbmV4cG9ydCBjb25zdCBnZXRIb3VzZVBob25lVXJsID0gKGhvdXNlSWQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9QYXltZW50U2VydmxldD9pZD0ke2hvdXNlSWR9YDtcbmV4cG9ydCBjb25zdCBpbmNyZWFzZUNyZWRpdFVybCA9IChhbW91bnQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9DcmVkaXRTZXJ2bGV0P3ZhbHVlPSR7YW1vdW50fWA7XG5cbmV4cG9ydCBjb25zdCBzZWFyY2hEcml2ZXJVcmwgPSBwaG9uZU51bWJlciA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvcGhvbmVOdW1iZXIvJHtwaG9uZU51bWJlcn1gO1xuZXhwb3J0IGNvbnN0IGdldERyaXZlckFjcXVpc2l0aW9uSW5mb1VybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgdXBkYXRlRHJpdmVyQWNxdWlzaXRpb25JbmZvVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCB1cGRhdGVEcml2ZXJEb2NzVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2RvY3VtZW50cy8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgYXBwcm92ZURyaXZlclVybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9hcHByb3ZlLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCByZWplY3REcml2ZXJVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvcmVqZWN0LyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCBnZXREcml2ZXJEb2N1bWVudFVybCA9IGRvY3VtZW50SWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2RvY3VtZW50LyR7ZG9jdW1lbnRJZH1gO1xuZXhwb3J0IGNvbnN0IGdldERyaXZlckRvY3VtZW50SWRzVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2RvY3VtZW50cy8ke2RyaXZlcklkfWA7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbmZpZy9wYXRocy5qcyIsIi8qKlxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxuICpcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKi9cblxuaW1wb3J0IGNyZWF0ZUJyb3dzZXJIaXN0b3J5IGZyb20gJ2hpc3RvcnkvY3JlYXRlQnJvd3Nlckhpc3RvcnknO1xuXG4vLyBOYXZpZ2F0aW9uQmFyIG1hbmFnZXIsIGUuZy4gaGlzdG9yeS5wdXNoKCcvaG9tZScpXG4vLyBodHRwczovL2dpdGh1Yi5jb20vbWphY2tzb24vaGlzdG9yeVxuZXhwb3J0IGRlZmF1bHQgcHJvY2Vzcy5lbnYuQlJPV1NFUiAmJiBjcmVhdGVCcm93c2VySGlzdG9yeSgpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9oaXN0b3J5LmpzIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0ICogYXMgaG91c2VBY3Rpb25zIGZyb20gJy4uLy4uLy4uL2FjdGlvbnMvaG91c2UnO1xuaW1wb3J0IE5ld0hvdXNlRm9ybSBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL05ld0hvdXNlRm9ybSc7XG5pbXBvcnQgTG9hZGVyIGZyb20gJy4uLy4uLy4uL2NvbXBvbmVudHMvTG9hZGVyJztcblxuY2xhc3MgTmV3SG91c2VDb250YWluZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGhvdXNlQWN0aW9uczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIGlzRmV0Y2hpbmc6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIH07XG4gIGhhbmRsZVN1Ym1pdCA9IChcbiAgICBhcmVhLFxuICAgIGJ1aWxkaW5nVHlwZSxcbiAgICBhZGRyZXNzLFxuICAgIGRlYWxUeXBlLFxuICAgIGJhc2VQcmljZSxcbiAgICByZW50UHJpY2UsXG4gICAgc2VsbFByaWNlLFxuICAgIHBob25lLFxuICAgIGRlc2NyaXB0aW9uLFxuICAgIGV4cGlyZVRpbWUsXG4gICkgPT4ge1xuICAgIHRoaXMucHJvcHMuaG91c2VBY3Rpb25zLmNyZWF0ZUhvdXNlKFxuICAgICAgYXJlYSxcbiAgICAgIGJ1aWxkaW5nVHlwZSxcbiAgICAgIGFkZHJlc3MsXG4gICAgICBkZWFsVHlwZSxcbiAgICAgIGJhc2VQcmljZSxcbiAgICAgIHJlbnRQcmljZSxcbiAgICAgIHNlbGxQcmljZSxcbiAgICAgIHBob25lLFxuICAgICAgZGVzY3JpcHRpb24sXG4gICAgICBleHBpcmVUaW1lLFxuICAgICAgJ2h0dHA6Ly91dXBsb2FkLmlyL2ZpbGVzL2p6bW9fbm8tcGljLmpwZycsXG4gICAgKTtcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8TmV3SG91c2VGb3JtIGhhbmRsZVN1Ym1pdD17dGhpcy5oYW5kbGVTdWJtaXR9IC8+XG4gICAgICAgIDxMb2FkZXIgbG9hZGluZz17dGhpcy5wcm9wcy5pc0ZldGNoaW5nfSAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBtYXBEaXNwYXRjaFRvUHJvcHMoZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIHtcbiAgICBob3VzZUFjdGlvbnM6IGJpbmRBY3Rpb25DcmVhdG9ycyhob3VzZUFjdGlvbnMsIGRpc3BhdGNoKSxcbiAgfTtcbn1cblxuZnVuY3Rpb24gbWFwU3RhdGVUb1Byb3BzKHN0YXRlKSB7XG4gIGNvbnN0IHsgaG91c2UgfSA9IHN0YXRlO1xuICBjb25zdCB7IGlzRmV0Y2hpbmcgfSA9IGhvdXNlO1xuICByZXR1cm4ge1xuICAgIGlzRmV0Y2hpbmcsXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKE5ld0hvdXNlQ29udGFpbmVyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvcm91dGVzL2hvdXNlcy9uZXcvTmV3SG91c2VDb250YWluZXIuanMiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IE5ld0hvdXNlQ29udGFpbmVyIGZyb20gJy4vTmV3SG91c2VDb250YWluZXInO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmNvbnN0IHRpdGxlID0gJ9in2LbYp9mB2Ycg2qnYsdiv2YYg2K7Yp9mG2YcnO1xuXG5mdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgY2h1bmtzOiBbJ25ldy1ob3VzZSddLFxuICAgIHRpdGxlLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPE5ld0hvdXNlQ29udGFpbmVyICAvPixcbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvcm91dGVzL2hvdXNlcy9uZXcvaW5kZXguanMiLCJpbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IEVYSUYgZnJvbSAnZXhpZi1qcyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi9oaXN0b3J5JztcbmltcG9ydCBjb21wcmVzc2lvbkNvbmZpZyBmcm9tICcuLi9jb25maWcvY29tcHJlc3Npb25Db25maWcnO1xuaW1wb3J0IHsgbG9hZExvZ2luU3RhdHVzIH0gZnJvbSAnLi4vYWN0aW9ucy9hdXRoZW50aWNhdGlvbic7XG5cbmNvbnN0IGNvbXByZXNzaW9uRmlyc3RMZXZlbCA9IGNvbXByZXNzaW9uQ29uZmlnLmdldExldmVsKDEpO1xuY29uc3QgY29tcHJlc3Npb25TZWNvbmRMZXZlbCA9IGNvbXByZXNzaW9uQ29uZmlnLmdldExldmVsKDIpO1xuY29uc3QgZG9jdW1lbnRzQ29tcHJlc3Npb25MZXZlbCA9IHtcbiAgcHJvZmlsZVBpY3R1cmU6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgZHJpdmVyTGljZW5jZTogY29tcHJlc3Npb25GaXJzdExldmVsLFxuICBjYXJJZENhcmQ6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgaW5zdXJhbmNlOiBjb21wcmVzc2lvblNlY29uZExldmVsLFxufTtcblxuZnVuY3Rpb24gZ2V0Q29tcHJlc3Npb25MZXZlbChkb2N1bWVudFR5cGUpIHtcbiAgcmV0dXJuIGRvY3VtZW50c0NvbXByZXNzaW9uTGV2ZWxbZG9jdW1lbnRUeXBlXSB8fCBjb21wcmVzc2lvbkZpcnN0TGV2ZWw7XG59XG5cbmZ1bmN0aW9uIGRhdGFVUkx0b0Jsb2IoZGF0YVVSTCwgZmlsZVR5cGUpIHtcbiAgY29uc3QgYmluYXJ5ID0gYXRvYihkYXRhVVJMLnNwbGl0KCcsJylbMV0pO1xuICBjb25zdCBhcnJheSA9IFtdO1xuICBjb25zdCBsZW5ndGggPSBiaW5hcnkubGVuZ3RoO1xuICBsZXQgaTtcbiAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgYXJyYXkucHVzaChiaW5hcnkuY2hhckNvZGVBdChpKSk7XG4gIH1cbiAgcmV0dXJuIG5ldyBCbG9iKFtuZXcgVWludDhBcnJheShhcnJheSldLCB7IHR5cGU6IGZpbGVUeXBlIH0pO1xufVxuXG5mdW5jdGlvbiBnZXRTY2FsZWRJbWFnZU1lYXN1cmVtZW50cyh3aWR0aCwgaGVpZ2h0LCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgeyBNQVhfSEVJR0hULCBNQVhfV0lEVEggfSA9IGdldENvbXByZXNzaW9uTGV2ZWwoZG9jdW1lbnRUeXBlKTtcblxuICBpZiAod2lkdGggPiBoZWlnaHQpIHtcbiAgICBoZWlnaHQgKj0gTUFYX1dJRFRIIC8gd2lkdGg7XG4gICAgd2lkdGggPSBNQVhfV0lEVEg7XG4gIH0gZWxzZSB7XG4gICAgd2lkdGggKj0gTUFYX0hFSUdIVCAvIGhlaWdodDtcbiAgICBoZWlnaHQgPSBNQVhfSEVJR0hUO1xuICB9XG4gIHJldHVybiB7XG4gICAgaGVpZ2h0LFxuICAgIHdpZHRoLFxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRJbWFnZU9yaWVudGF0aW9uKGltYWdlKSB7XG4gIGxldCBvcmllbnRhdGlvbiA9IDA7XG4gIEVYSUYuZ2V0RGF0YShpbWFnZSwgZnVuY3Rpb24oKSB7XG4gICAgb3JpZW50YXRpb24gPSBFWElGLmdldFRhZyh0aGlzLCAnT3JpZW50YXRpb24nKTtcbiAgfSk7XG4gIHJldHVybiBvcmllbnRhdGlvbjtcbn1cblxuZnVuY3Rpb24gZ2V0SW1hZ2VNZWFzdXJlbWVudHMoaW1hZ2UpIHtcbiAgY29uc3Qgb3JpZW50YXRpb24gPSBnZXRJbWFnZU9yaWVudGF0aW9uKGltYWdlKTtcbiAgbGV0IHRyYW5zZm9ybTtcbiAgbGV0IHN3YXAgPSBmYWxzZTtcbiAgc3dpdGNoIChvcmllbnRhdGlvbikge1xuICAgIGNhc2UgODpcbiAgICAgIHN3YXAgPSB0cnVlO1xuICAgICAgdHJhbnNmb3JtID0gJ2xlZnQnO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSA2OlxuICAgICAgc3dhcCA9IHRydWU7XG4gICAgICB0cmFuc2Zvcm0gPSAncmlnaHQnO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAzOlxuICAgICAgdHJhbnNmb3JtID0gJ2ZsaXAnO1xuICAgICAgYnJlYWs7XG4gICAgZGVmYXVsdDpcbiAgfVxuICBjb25zdCB3aWR0aCA9IHN3YXAgPyBpbWFnZS5oZWlnaHQgOiBpbWFnZS53aWR0aDtcbiAgY29uc3QgaGVpZ2h0ID0gc3dhcCA/IGltYWdlLndpZHRoIDogaW1hZ2UuaGVpZ2h0O1xuICByZXR1cm4ge1xuICAgIHdpZHRoLFxuICAgIGhlaWdodCxcbiAgICB0cmFuc2Zvcm0sXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldFNjYWxlZEltYWdlRmlsZUZyb21DYW52YXMoXG4gIGltYWdlLFxuICB3aWR0aCxcbiAgaGVpZ2h0LFxuICB0cmFuc2Zvcm0sXG4gIGZpbGVUeXBlLFxuKSB7XG4gIGNvbnN0IGNhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO1xuICBjYW52YXMud2lkdGggPSB3aWR0aDtcbiAgY2FudmFzLmhlaWdodCA9IGhlaWdodDtcbiAgY29uc3QgY29udGV4dCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICBsZXQgdHJhbnNmb3JtUGFyYW1zO1xuICBsZXQgc3dhcE1lYXN1cmUgPSBmYWxzZTtcbiAgbGV0IGltYWdlV2lkdGggPSB3aWR0aDtcbiAgbGV0IGltYWdlSGVpZ2h0ID0gaGVpZ2h0O1xuICBzd2l0Y2ggKHRyYW5zZm9ybSkge1xuICAgIGNhc2UgJ2xlZnQnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzAsIC0xLCAxLCAwLCAwLCBoZWlnaHRdO1xuICAgICAgc3dhcE1lYXN1cmUgPSB0cnVlO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAncmlnaHQnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzAsIDEsIC0xLCAwLCB3aWR0aCwgMF07XG4gICAgICBzd2FwTWVhc3VyZSA9IHRydWU7XG4gICAgICBicmVhaztcbiAgICBjYXNlICdmbGlwJzpcbiAgICAgIHRyYW5zZm9ybVBhcmFtcyA9IFsxLCAwLCAwLCAtMSwgMCwgaGVpZ2h0XTtcbiAgICAgIGJyZWFrO1xuICAgIGRlZmF1bHQ6XG4gICAgICB0cmFuc2Zvcm1QYXJhbXMgPSBbMSwgMCwgMCwgMSwgMCwgMF07XG4gIH1cbiAgY29udGV4dC5zZXRUcmFuc2Zvcm0oLi4udHJhbnNmb3JtUGFyYW1zKTtcbiAgaWYgKHN3YXBNZWFzdXJlKSB7XG4gICAgaW1hZ2VIZWlnaHQgPSB3aWR0aDtcbiAgICBpbWFnZVdpZHRoID0gaGVpZ2h0O1xuICB9XG4gIGNvbnRleHQuZHJhd0ltYWdlKGltYWdlLCAwLCAwLCBpbWFnZVdpZHRoLCBpbWFnZUhlaWdodCk7XG4gIGNvbnRleHQuc2V0VHJhbnNmb3JtKDEsIDAsIDAsIDEsIDAsIDApO1xuICBjb25zdCBkYXRhVVJMID0gY2FudmFzLnRvRGF0YVVSTChmaWxlVHlwZSk7XG4gIHJldHVybiBkYXRhVVJMdG9CbG9iKGRhdGFVUkwsIGZpbGVUeXBlKTtcbn1cblxuZnVuY3Rpb24gc2hvdWxkUmVzaXplKGltYWdlTWVhc3VyZW1lbnRzLCBmaWxlU2l6ZSwgZG9jdW1lbnRUeXBlKSB7XG4gIGNvbnN0IHsgTUFYX0hFSUdIVCwgTUFYX1dJRFRILCBET0NVTUVOVF9TSVpFIH0gPSBnZXRDb21wcmVzc2lvbkxldmVsKFxuICAgIGRvY3VtZW50VHlwZSxcbiAgKTtcbiAgaWYgKGRvY3VtZW50VHlwZSA9PT0gJ2luc3VyYW5jZScgJiYgZmlsZVNpemUgPCBET0NVTUVOVF9TSVpFKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9IGVsc2UgaWYgKGZpbGVTaXplIDwgRE9DVU1FTlRfU0laRSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICByZXR1cm4gKFxuICAgIGltYWdlTWVhc3VyZW1lbnRzLndpZHRoID4gTUFYX1dJRFRIIHx8IGltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCA+IE1BWF9IRUlHSFRcbiAgKTtcbn1cblxuYXN5bmMgZnVuY3Rpb24gcHJvY2Vzc0ZpbGUoZGF0YVVSTCwgZmlsZVNpemUsIGZpbGVUeXBlLCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgaW1hZ2UgPSBuZXcgSW1hZ2UoKTtcbiAgaW1hZ2Uuc3JjID0gZGF0YVVSTDtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICBpbWFnZS5vbmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGNvbnN0IGltYWdlTWVhc3VyZW1lbnRzID0gZ2V0SW1hZ2VNZWFzdXJlbWVudHMoaW1hZ2UpO1xuICAgICAgaWYgKCFzaG91bGRSZXNpemUoaW1hZ2VNZWFzdXJlbWVudHMsIGZpbGVTaXplLCBkb2N1bWVudFR5cGUpKSB7XG4gICAgICAgIHJlc29sdmUoZGF0YVVSTHRvQmxvYihkYXRhVVJMLCBmaWxlVHlwZSkpO1xuICAgICAgfVxuICAgICAgY29uc3Qgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMgPSBnZXRTY2FsZWRJbWFnZU1lYXN1cmVtZW50cyhcbiAgICAgICAgaW1hZ2VNZWFzdXJlbWVudHMud2lkdGgsXG4gICAgICAgIGltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCxcbiAgICAgICAgZG9jdW1lbnRUeXBlLFxuICAgICAgKTtcbiAgICAgIGNvbnN0IHNjYWxlZEltYWdlRmlsZSA9IGdldFNjYWxlZEltYWdlRmlsZUZyb21DYW52YXMoXG4gICAgICAgIGltYWdlLFxuICAgICAgICBzY2FsZWRJbWFnZU1lYXN1cmVtZW50cy53aWR0aCxcbiAgICAgICAgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMuaGVpZ2h0LFxuICAgICAgICBpbWFnZU1lYXN1cmVtZW50cy50cmFuc2Zvcm0sXG4gICAgICAgIGZpbGVUeXBlLFxuICAgICAgKTtcbiAgICAgIHJlc29sdmUoc2NhbGVkSW1hZ2VGaWxlKTtcbiAgICB9O1xuICAgIGltYWdlLm9uZXJyb3IgPSByZWplY3Q7XG4gIH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29tcHJlc3NGaWxlKGZpbGUsIGRvY3VtZW50VHlwZSA9ICdkZWZhdWx0Jykge1xuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XG4gICAgICByZWFkZXIub25sb2FkZW5kID0gYXN5bmMgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnN0IGNvbXByZXNzZWRGaWxlID0gYXdhaXQgcHJvY2Vzc0ZpbGUoXG4gICAgICAgICAgcmVhZGVyLnJlc3VsdCxcbiAgICAgICAgICBmaWxlLnNpemUsXG4gICAgICAgICAgZmlsZS50eXBlLFxuICAgICAgICAgIGRvY3VtZW50VHlwZSxcbiAgICAgICAgKTtcbiAgICAgICAgcmVzb2x2ZShjb21wcmVzc2VkRmlsZSk7XG4gICAgICB9O1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgcmVqZWN0KGVycik7XG4gICAgfVxuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldExvZ2luSW5mbygpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCBsb2dpbkluZm8gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9naW5JbmZvJyk7XG4gICAgaWYgKF8uaXNTdHJpbmcobG9naW5JbmZvKSkge1xuICAgICAgcmV0dXJuIEpTT04ucGFyc2UobG9naW5JbmZvKTtcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUubG9nKCdFcnJvciB3aGVuIHBhcnNpbmcgbG9naW5JbmZvIG9iamVjdDogJywgZXJyKTtcbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRQb3N0Q29uZmlnKGJvZHkpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgbWltZVR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvZ2luSW5mby50b2tlbn1gLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLCBtaW1lVHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgLy8gbW9kZTogJ25vLWNvcnMnLFxuICAgIGhlYWRlcnMsXG4gICAgLi4uKF8uaXNFbXB0eShib2R5KSA/IHt9IDogeyBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KSB9KSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFB1dENvbmZpZyhib2R5KSB7XG4gIGNvbnN0IGxvZ2luSW5mbyA9IGdldExvZ2luSW5mbygpO1xuICBjb25zdCBoZWFkZXJzID0gbG9naW5JbmZvXG4gICAgPyB7XG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdYLUF1dGhvcml6YXRpb24nOiBsb2dpbkluZm8udG9rZW4sXG4gICAgICB9XG4gICAgOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfTtcbiAgcmV0dXJuIHtcbiAgICBtZXRob2Q6ICdQVVQnLFxuICAgIGhlYWRlcnMsXG4gICAgLi4uKF8uaXNFbXB0eShib2R5KSA/IHt9IDogeyBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KSB9KSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEdldENvbmZpZygpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvZ2luSW5mby50b2tlbn1gLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnR0VUJyxcbiAgICBoZWFkZXJzLFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29udmVydE51bWJlcnNUb1BlcnNpYW4oc3RyKSB7XG4gIGNvbnN0IHBlcnNpYW5OdW1iZXJzID0gJ9uw27Hbstuz27Tbtdu227fbuNu5JztcbiAgbGV0IHJlc3VsdCA9ICcnO1xuICBpZiAoIXN0ciAmJiBzdHIgIT09IDApIHtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG4gIHN0ciA9IHN0ci50b1N0cmluZygpO1xuICBsZXQgY29udmVydGVkQ2hhcjtcbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyBpKyspIHtcbiAgICB0cnkge1xuICAgICAgY29udmVydGVkQ2hhciA9IHBlcnNpYW5OdW1iZXJzW3N0cltpXV07XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgY29udmVydGVkQ2hhciA9IHN0cltpXTtcbiAgICB9XG4gICAgcmVzdWx0ICs9IGNvbnZlcnRlZENoYXI7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGZldGNoQXBpKHVybCwgY29uZmlnKSB7XG4gIGxldCBmbG93RXJyb3I7XG4gIHRyeSB7XG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCBjb25maWcpO1xuICAgIGNvbnNvbGUubG9nKHJlcyk7XG4gICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG4gICAgbGV0IHJlc3VsdCA9IGF3YWl0IHJlcy5qc29uKCk7XG4gICAgY29uc29sZS5sb2cocmVzdWx0KTtcbiAgICBpZiAoIV8uaXNFbXB0eShfLmdldChyZXN1bHQsICdkYXRhJykpKSB7XG4gICAgICByZXN1bHQgPSByZXN1bHQuZGF0YTtcbiAgICB9XG4gICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG5cbiAgICBpZiAocmVzLnN0YXR1cyAhPT0gMjAwKSB7XG4gICAgICBmbG93RXJyb3IgPSByZXN1bHQ7XG4gICAgICBjb25zb2xlLmxvZyhyZXMuc3RhdHVzKTtcblxuICAgICAgaWYgKHJlcy5zdGF0dXMgPT09IDQwMykge1xuICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcbiAgICAgICAgZmxvd0Vycm9yLm1lc3NhZ2UgPSAn2YTYt9mB2Kcg2K/ZiNio2KfYsdmHINmI2KfYsdivINi02YjbjNivISc7XG4gICAgICAgIGNvbnNvbGUubG9nKCd5ZWVlcycpO1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IGhpc3RvcnkucHVzaCgnL2xvZ2luJyksIDMwMDApO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgICBjb25zb2xlLmRlYnVnKGBFcnJvciB3aGVuIGZldGNoaW5nIGFwaTogJHt1cmx9YCwgZXJyKTtcbiAgICBmbG93RXJyb3IgPSB7XG4gICAgICBjb2RlOiAnVU5LTk9XTl9FUlJPUicsXG4gICAgICBtZXNzYWdlOiAn2K7Yt9in24wg2YbYp9mF2LTYrti1INmE2LfZgdinINiv2YjYqNin2LHZhyDYs9i524wg2qnZhtuM2K8nLFxuICAgIH07XG4gIH1cbiAgaWYgKGZsb3dFcnJvcikge1xuICAgIHRocm93IGZsb3dFcnJvcjtcbiAgfVxufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gc2V0SW1tZWRpYXRlKGZuKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGxldCB2YWx1ZSA9IG51bGw7XG4gICAgICBpZiAoXy5pc0Z1bmN0aW9uKGZuKSkge1xuICAgICAgICB2YWx1ZSA9IGZuKCk7XG4gICAgICB9XG4gICAgICByZXNvbHZlKHZhbHVlKTtcbiAgICB9LCAwKTtcbiAgfSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRDb29raWUobmFtZSwgdmFsdWUsIGRheXMgPSA3KSB7XG4gIGxldCBleHBpcmVzID0gJyc7XG4gIGlmIChkYXlzKSB7XG4gICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKCk7XG4gICAgZGF0ZS5zZXRUaW1lKGRhdGUuZ2V0VGltZSgpICsgZGF5cyAqIDI0ICogNjAgKiA2MCAqIDEwMDApO1xuICAgIGV4cGlyZXMgPSBgOyBleHBpcmVzPSR7ZGF0ZS50b1VUQ1N0cmluZygpfWA7XG4gIH1cbiAgZG9jdW1lbnQuY29va2llID0gYCR7bmFtZX09JHt2YWx1ZSB8fCAnJ30ke2V4cGlyZXN9OyBwYXRoPS9gO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldENvb2tpZShuYW1lKSB7XG4gIGNvbnN0IG5hbWVFUSA9IGAke25hbWV9PWA7XG4gIGNvbnN0IGNhID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XG4gIGNvbnN0IGxlbmd0aCA9IGNhLmxlbmd0aDtcbiAgbGV0IGkgPSAwO1xuICBmb3IgKDsgaSA8IGxlbmd0aDsgaSArPSAxKSB7XG4gICAgbGV0IGMgPSBjYVtpXTtcbiAgICB3aGlsZSAoYy5jaGFyQXQoMCkgPT09ICcgJykge1xuICAgICAgYyA9IGMuc3Vic3RyaW5nKDEsIGMubGVuZ3RoKTtcbiAgICB9XG4gICAgaWYgKGMuaW5kZXhPZihuYW1lRVEpID09PSAwKSB7XG4gICAgICByZXR1cm4gYy5zdWJzdHJpbmcobmFtZUVRLmxlbmd0aCwgYy5sZW5ndGgpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGVyYXNlQ29va2llKG5hbWUpIHtcbiAgZG9jdW1lbnQuY29va2llID0gYCR7bmFtZX09OyBNYXgtQWdlPS05OTk5OTk5OTtgO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm90aWZ5RXJyb3IobWVzc2FnZSkge1xuICB0b2FzdHIub3B0aW9ucyA9IHtcbiAgICBjbG9zZUJ1dHRvbjogZmFsc2UsXG4gICAgZGVidWc6IGZhbHNlLFxuICAgIG5ld2VzdE9uVG9wOiBmYWxzZSxcbiAgICBwcm9ncmVzc0JhcjogZmFsc2UsXG4gICAgcG9zaXRpb25DbGFzczogJ3RvYXN0LXRvcC1jZW50ZXInLFxuICAgIHByZXZlbnREdXBsaWNhdGVzOiB0cnVlLFxuICAgIG9uY2xpY2s6IG51bGwsXG4gICAgc2hvd0R1cmF0aW9uOiAnMzAwJyxcbiAgICBoaWRlRHVyYXRpb246ICcxMDAwJyxcbiAgICB0aW1lT3V0OiAnMTAwMDAnLFxuICAgIGV4dGVuZGVkVGltZU91dDogJzEwMDAnLFxuICAgIHNob3dFYXNpbmc6ICdzd2luZycsXG4gICAgaGlkZUVhc2luZzogJ2xpbmVhcicsXG4gICAgc2hvd01ldGhvZDogJ3NsaWRlRG93bicsXG4gICAgaGlkZU1ldGhvZDogJ3NsaWRlVXAnLFxuICB9O1xuICB0b2FzdHIuZXJyb3IobWVzc2FnZSwgJ9iu2LfYpycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm90aWZ5U3VjY2VzcyhtZXNzYWdlKSB7XG4gIHRvYXN0ci5vcHRpb25zID0ge1xuICAgIGNsb3NlQnV0dG9uOiBmYWxzZSxcbiAgICBkZWJ1ZzogZmFsc2UsXG4gICAgbmV3ZXN0T25Ub3A6IGZhbHNlLFxuICAgIHByb2dyZXNzQmFyOiBmYWxzZSxcbiAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtdG9wLWNlbnRlcicsXG4gICAgcHJldmVudER1cGxpY2F0ZXM6IHRydWUsXG4gICAgb25jbGljazogbnVsbCxcbiAgICBzaG93RHVyYXRpb246ICczMDAnLFxuICAgIGhpZGVEdXJhdGlvbjogJzEwMDAnLFxuICAgIHRpbWVPdXQ6ICc1MDAwJyxcbiAgICBleHRlbmRlZFRpbWVPdXQ6ICcxMDAwJyxcbiAgICBzaG93RWFzaW5nOiAnc3dpbmcnLFxuICAgIGhpZGVFYXNpbmc6ICdsaW5lYXInLFxuICAgIHNob3dNZXRob2Q6ICdzbGlkZURvd24nLFxuICAgIGhpZGVNZXRob2Q6ICdzbGlkZVVwJyxcbiAgfTtcbiAgdG9hc3RyLnN1Y2Nlc3MobWVzc2FnZSwgJycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVsb2FkU2VsZWN0UGlja2VyKCkge1xuICB0cnkge1xuICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoJ3JlbmRlcicpO1xuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmxvZyhlcnIpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkU2VsZWN0UGlja2VyKCkge1xuICB0cnkge1xuICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoe30pO1xuICAgIGlmIChcbiAgICAgIC9BbmRyb2lkfHdlYk9TfGlQaG9uZXxpUGFkfGlQb2R8QmxhY2tCZXJyeS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudClcbiAgICApIHtcbiAgICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoJ21vYmlsZScpO1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Rm9ybURhdGEoZGF0YSkge1xuICBpZiAoXy5pc0VtcHR5KGRhdGEpKSB7XG4gICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgaW5wdXQgZGF0YSB0byBjcmVhdGUgYSBGb3JtRGF0YSBvYmplY3QnKTtcbiAgfVxuICBsZXQgZm9ybURhdGE7XG4gIHRyeSB7XG4gICAgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICBjb25zdCBrZXlzID0gXy5rZXlzKGRhdGEpO1xuICAgIGxldCBpO1xuICAgIGNvbnN0IGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgZm9ybURhdGEuYXBwZW5kKGtleXNbaV0sIGRhdGFba2V5c1tpXV0pO1xuICAgIH1cbiAgICByZXR1cm4gZm9ybURhdGE7XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUuZGVidWcoZXJyKTtcbiAgICB0aHJvdyBFcnJvcignRk9STV9EQVRBX0JST1dTRVJfU1VQUE9SVF9GQUlMVVJFJyk7XG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRVc2VyTG9naW5TdGF0dXMoc3RvcmUpIHtcbiAgY29uc3QgbG9naW5TdGF0dXNBY3Rpb24gPSBsb2FkTG9naW5TdGF0dXMoKTtcbiAgc3RvcmUuZGlzcGF0Y2gobG9naW5TdGF0dXNBY3Rpb24pO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy91dGlscy9pbmRleC5qcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFxQkE7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaFJBO0FBY0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFhQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0JBO0FBQUE7QUFBQTtBQUFBO0FBMkJBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbkJBO0FBQUE7QUFBQTtBQUFBO0FBbUJBOzs7Ozs7OztBQ3BNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFGQTtBQUtBOzs7Ozs7OztBQ2hDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBS0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBO0FBYUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFjQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNwR0E7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQWZBO0FBREE7QUF5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFCQTtBQURBO0FBb0NBO0FBdkNBO0FBQ0E7QUF5Q0E7Ozs7Ozs7QUNuREE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIQTtBQU1BO0FBaEJBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFEQTtBQWNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBT0E7QUFiQTtBQUNBO0FBREE7QUFFQTtBQURBO0FBZUE7Ozs7Ozs7QUN0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDN0JBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFpQkE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0RBO0FBNkRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqRUE7QUEwRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBL0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBYUE7QUFDQTtBQWlGQTtBQUNBO0FBQ0E7QUFTQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQURBO0FBREE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFWQTtBQUFBO0FBYUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQVRBO0FBQUE7QUFkQTtBQURBO0FBNkJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXRCQTtBQXlCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZkE7QUFpQkE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF4QkE7QUExQkE7QUFEQTtBQXdEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF0QkE7QUEwQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXRCQTtBQTRCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXBCQTtBQXpEQTtBQURBO0FBbUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXRCQTtBQTBCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXBCQTtBQTVCQTtBQURBO0FBd0RBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBREE7QUFEQTtBQW9CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQURBO0FBclBBO0FBWkE7QUFEQTtBQW1SQTtBQXBZQTtBQUNBO0FBREE7QUFFQTtBQURBO0FBc1lBOzs7Ozs7O0FDN1lBOzs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUZBO0FBREE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTNCQTtBQXVDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFDQTtBQURBO0FBSkE7QUE4REE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBL0RBO0FBREE7QUFEQTtBQURBO0FBREE7QUFEQTtBQThFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFGQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEzQkE7QUF1Q0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUExQ0E7QUFEQTtBQUpBO0FBOERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQS9EQTtBQURBO0FBREE7QUF1RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVRBO0FBREE7QUFEQTtBQURBO0FBeEVBO0FBREE7QUFEQTtBQW1HQTtBQWhNQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQWlNQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTs7Ozs7OztBQzVOQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7Ozs7Ozs7O0FDZEE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7Ozs7O0FDcEJBO0FBQUE7QUFBQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQWlCQTtBQWFBO0FBOUJBO0FBQ0E7QUE4QkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUtBO0FBdENBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFGQTtBQXdDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDL0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBSkE7QUFTQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDc0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBS0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExQkE7Ozs7Ozs7QUF4SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBc0NBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==