require("source-map-support").install();
exports.ids = [1];
exports.modules = {

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/node_modules/normalize.css/normalize.css"],"names":[],"mappings":"AAAA,4EAA4E;AAC5E;gFACgF;AAChF;;;;GAIG;AACH;EACE,kBAAkB;EAClB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,+BAA+B;EAC/B,OAAO,EAAE;AACX;gFACgF;AAChF;;GAEG;AACH;EACE,UAAU,EAAE;AACd;;GAEG;AACH;;;;;;EAME,eAAe,EAAE;AACnB;;;GAGG;AACH;EACE,eAAe;EACf,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;EAGE,OAAO;EACP,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;;GAGG;AACH;EACE,gCAAgC;UACxB,wBAAwB;EAChC,OAAO;EACP,UAAU;EACV,OAAO;EACP,kBAAkB;EAClB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,sCAAsC;EACtC,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,oBAAoB;EACpB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,0CAA0C;UAClC,kCAAkC;EAC1C,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,qBAAqB,EAAE;AACzB;;GAEG;AACH;;EAEE,oBAAoB,EAAE;AACxB;;;GAGG;AACH;;;EAGE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,uBAAuB;EACvB,YAAY,EAAE;AAChB;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,eAAe;EACf,eAAe;EACf,mBAAmB;EACnB,yBAAyB,EAAE;AAC7B;EACE,gBAAgB,EAAE;AACpB;EACE,YAAY,EAAE;AAChB;gFACgF;AAChF;;GAEG;AACH;;EAEE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc;EACd,UAAU,EAAE;AACd;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;;;EAKE,wBAAwB;EACxB,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,kBAAkB;EAClB,OAAO;EACP,UAAU;EACV,OAAO,EAAE;AACX;;;GAGG;AACH;;EAEE,OAAO;EACP,kBAAkB,EAAE;AACtB;;;GAGG;AACH;;EAEE,OAAO;EACP,qBAAqB,EAAE;AACzB;;;;GAIG;AACH;;;;EAIE,2BAA2B;EAC3B,OAAO,EAAE;AACX;;GAEG;AACH;;;;EAIE,mBAAmB;EACnB,WAAW,EAAE;AACf;;GAEG;AACH;;;;EAIE,+BAA+B,EAAE;AACnC;;GAEG;AACH;EACE,+BAA+B,EAAE;AACnC;;;;;GAKG;AACH;EACE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,eAAe;EACf,OAAO;EACP,eAAe;EACf,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,WAAW;EACX,OAAO;EACP,oBAAoB;EACpB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,sBAAsB;EACtB,OAAO;EACP,yBAAyB;EACzB,OAAO,EAAE;AACX;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,WAAW;EACX,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,aAAa,EAAE;AACjB;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,qBAAqB;EACrB,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,yBAAyB,EAAE;AAC7B;;;GAGG;AACH;EACE,2BAA2B;EAC3B,OAAO;EACP,cAAc;EACd,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;;EAEE,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;gFACgF;AAChF;;GAEG;AACH;EACE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc,EAAE;AAClB;gFACgF;AAChF;;GAEG;AACH;EACE,cAAc,EAAE","file":"normalize.css","sourcesContent":["/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,YAAY;EACZ,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE;IACE,aAAa,EAAE,EAAE;AACrB;EACE;IACE,eAAe,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousesPage/main.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n  font-family: Shabnam;\n}\n.white {\n  color: white;\n}\n.black {\n  color: black;\n}\n.purple {\n  color: #4c0d6b\n}\n.light-blue-background {\n  background-color: #3ad2cb\n}\n.white-background {\n  background-color: white;\n}\n.dark-green-background {\n  background-color: #185956;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-light {\n  font-weight: 200;\n}\n.font-medium {\n  font-weight: 600;\n}\n.center-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.center-column {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.text-align-right {\n  text-align: right;\n}\n.text-align-left {\n  text-align: left;\n}\n.text-align-center {\n  text-align: center;\n}\n.no-list-style {\n  list-style: none;\n}\n.clear-input {\n  outline: none;\n  border: none;\n}\n.border-radius {\n  border-radius: 9px;\n}\n.has-transition {\n  -webkit-transition: box-shadow 0.3s ease-in-out;\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.search-theme{\n  background: url(" + escape(__webpack_require__("./src/components/HousesPage/geometry2.png")) + ");\n  height: 15vh;\n  margin-top: 10vh;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbody {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n@-webkit-keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@-webkit-keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n@keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n.main-form {\n  position: absolute;\n  top: 14vh;\n}\n.search-button {\n  font-family: Shabnam !important;\n}\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n}\n.second-part {\n  margin-top: 4%;\n}\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl;\n}\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n}\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n}\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n}\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  color: black;\n}\n.search-input::-webkit-input-placeholder{\n  padding-right: 4%;\n}\n.search-input:-ms-input-placeholder{\n  padding-right: 4%;\n}\n.search-input::-ms-input-placeholder{\n  padding-right: 4%;\n}\n.search-input::placeholder{\n  padding-right: 4%;\n}\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl;\n}\n.deal-type-section {\n  text-align: right;\n}\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%;\n  }\n  .search-button {\n    display: block;\n    margin: 5% auto;\n  }\n}\nbody {\n    font-family: Shabnam;\n    overflow-x: hidden;\n}\n.logo-section {\n    margin-bottom: 7%;\n    text-align: center;\n}\n.logo-image {\n    max-width: 120px;\n}\n.slider {\n    height: 100vh;\n    margin: 0 auto;\n    position: relative;\n}\n.header-main {\n    z-index: 999;\n    top: 3vh;\n    position: absolute;\n}\n.header-main-boarder {\n    border: thin solid white;\n    border-radius: 10px;\n}\n.dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 0;\n    top: 6vh;\n}\n.slider-column {\n    padding: 0;\n}\n.slide1,.slide2,.slide3,.slide4 {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n}\n.slide1 {\n    background: url(" + escape(__webpack_require__("./src/components/HousesPage/michal-kubalczyk-260909-unsplash.jpg")) + ") no-repeat 90% 80%;\n    background-size: cover;\n    animation:fade 10s infinite;\n    -webkit-animation:fade 10s infinite;\n\n\n}\n.slide2 {\n    background: url(" + escape(__webpack_require__("./src/components/HousesPage/mahdiar-mahmoodi-452489-unsplash.jpg")) + ") no-repeat center;\n    background-size: cover;\n    animation:fade2 10s infinite;\n    -webkit-animation:fade2 10s infinite;\n}\n.slide3 {\n    background: url(" + escape(__webpack_require__("./src/components/HousesPage/casey-horner-533586-unsplash.jpg")) + ") no-repeat center;\n    background-size: cover;\n    animation:fade3 10s infinite;\n    -webkit-animation:fade3 10s infinite;\n}\n.slide4 {\n    background: url(" + escape(__webpack_require__("./src/components/HousesPage/luke-van-zyl-504032-unsplash.jpg")) + ") no-repeat center;\n    background-size: cover;\n    animation:fade4 10s infinite;\n    -webkit-animation:fade4 10s infinite;\n}\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n    text-decoration: none;\n    outline: none;\n    cursor: pointer;\n}\n.features-section {\n    position: relative;\n    margin-top: -5vh;\n}\ninput[type=radio] {\n    font-size: 16px;\n}\n.input-label {\n    font-weight: lighter;\n    font-size: 16px;\n}\n.feature-box {\n    -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n    margin-left: 2%;\n    height: 36vh;\n    width: 29% !important;\n    min-height: 42vh;\n}\n.features-container > .feature-box:nth-child(2) {\n    margin-left: 5%;\n}\n.features-container > .feature-box:nth-child(3) {\n    margin-left: 5%;\n}\n.feature-box > div > img {\n    max-width: 70px;\n}\n.why-us {\n    margin-top: 6%;\n    margin-bottom: 6%;\n}\n.why-us-img {\n    max-width: 300px;\n}\n.why-list {\n    list-style: none;\n    display: block;\n    color: black;\n    padding: 0;\n}\n.why-list > li {\n    margin-top: 3%;\n}\n.why-list > li > i {\n    margin-left: 2%;\n}\n.why-us-img-column {\n    text-align: right;\n}\n.reasons-section {\n    text-align: right;\n    direction: rtl;\n    padding: 0;\n}\n@media (max-width: 768px) {\n    .header-main-boarder {\n        width: 40%;\n        margin-left: 1%;\n    }\n    .mobile-header {\n        margin-top: -12%;\n    }\n    .logo-image {\n        max-width: 90px;\n    }\n    .main-title {\n        font-size: 20px;\n    }\n    .feature-box {\n        width: 100% !important;\n        margin-bottom: 5%;\n        margin-left: 0;\n    }\n    .features-container > .feature-box:nth-child(2) {\n        margin-left: 0;\n    }\n    .features-container > .feature-box:nth-child(3) {\n        margin-left: 0;\n    }\n    .why-us-img {\n        max-width: 247px;\n    }\n  .features-section {\n    position: relative;\n     margin-top: 2vh;\n  }\n}\n@media (max-width: 320px) {\n    .mobile-header {\n        margin-top: -12%;\n    }\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  /*background: url(\"../assets/images/pattern/geometry2.png\");*/\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-tag {\n  width: 100%;\n  height: 60%;\n  border-radius: 10px;\n  position: absolute;\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 6vh;\n  border-bottom: thin solid black; }\n.price-div {\n  position: absolute;\n  top: 85%;\n  width: 100%; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%;\n  text-align: right; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n/*#img-1{*/\n/*background-image: url(../assets/images/villa1.jpg);*/\n/*}*/\n/*#img-2{*/\n/*background-image: url(../assets/images/villa2.jpg);*/\n/*}*/\n/*#img-3, #img-4, #img-5{*/\n/*background-image: url(../assets/images/underwater.jpg);*/\n/*}*/\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/HousesPage/main.css"],"names":[],"mappings":"AAAA;EACE,qBAAqB;CACtB;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,cAAc;CACf;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,wBAAwB;CACzB;AACD;EACE,0BAA0B;CAC3B;AACD;EACE,iBAAiB;CAClB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;EAChC,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;EAChC,6BAA6B;EAC7B,8BAA8B;MAC1B,2BAA2B;UACvB,uBAAuB;CAChC;AACD;EACE,kBAAkB;CACnB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,mBAAmB;CACpB;AACD;EACE,iBAAiB;CAClB;AACD;EACE,cAAc;EACd,aAAa;CACd;AACD;EACE,mBAAmB;CACpB;AACD;EACE,gDAAgD;EAChD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,0CAAmC;EACnC,aAAa;EACb,iBAAiB;CAClB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;;IAEI,MAAM,SAAS,CAAC;IAChB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,MAAM,UAAU,CAAC;IACjB,OAAO,UAAU,CAAC;CACrB;AACD;EACE,mBAAmB;EACnB,UAAU;CACX;AACD;EACE,gCAAgC;CACjC;AACD;EACE,aAAa;EACb,qCAAqC;EACrC,YAAY;EACZ,kDAAkD;UAC1C,0CAA0C;CACnD;AACD;EACE,eAAe;CAChB;AACD;EACE,eAAe;EACf,aAAa;EACb,eAAe;CAChB;AACD;EACE,WAAW;EACX,gBAAgB;EAChB,kDAAkD;UAC1C,0CAA0C;CACnD;AACD;EACE,iDAAiD;UACzC,yCAAyC;CAClD;AACD;EACE,mDAAmD;UAC3C,2CAA2C;CACpD;AACD;EACE,YAAY;EACZ,oBAAoB;EACpB,gBAAgB;EAChB,aAAa;CACd;AACD;EACE,kBAAkB;CACnB;AACD;EACE,kBAAkB;CACnB;AACD;EACE,kBAAkB;CACnB;AACD;EACE,kBAAkB;CACnB;AACD;EACE,YAAY;EACZ,aAAa;EACb,eAAe;CAChB;AACD;EACE,kBAAkB;CACnB;AACD;EACE;IACE,gBAAgB;GACjB;EACD;IACE,eAAe;IACf,gBAAgB;GACjB;CACF;AACD;IACI,qBAAqB;IACrB,mBAAmB;CACtB;AACD;IACI,kBAAkB;IAClB,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,eAAe;IACf,mBAAmB;CACtB;AACD;IACI,aAAa;IACb,SAAS;IACT,mBAAmB;CACtB;AACD;IACI,yBAAyB;IACzB,oBAAoB;CACvB;AACD;IACI,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,iDAAiD;YACzC,yCAAyC;IACjD,mBAAmB;IACnB,WAAW;IACX,QAAQ;IACR,SAAS;CACZ;AACD;IACI,WAAW;CACd;AACD;IACI,mBAAmB;IACnB,YAAY;IACZ,aAAa;CAChB;AACD;IACI,4DAAwE;IACxE,uBAAuB;IACvB,4BAA4B;IAC5B,oCAAoC;;;CAGvC;AACD;IACI,2DAAuE;IACvE,uBAAuB;IACvB,6BAA6B;IAC7B,qCAAqC;CACxC;AACD;IACI,2DAAmE;IACnE,uBAAuB;IACvB,6BAA6B;IAC7B,qCAAqC;CACxC;AACD;IACI,2DAAmE;IACnE,uBAAuB;IACvB,6BAA6B;IAC7B,qCAAqC;CACxC;AACD;;;IAGI,sBAAsB;IACtB,cAAc;IACd,gBAAgB;CACnB;AACD;IACI,mBAAmB;IACnB,iBAAiB;CACpB;AACD;IACI,gBAAgB;CACnB;AACD;IACI,qBAAqB;IACrB,gBAAgB;CACnB;AACD;IACI,kDAAkD;YAC1C,0CAA0C;IAClD,gBAAgB;IAChB,aAAa;IACb,sBAAsB;IACtB,iBAAiB;CACpB;AACD;IACI,gBAAgB;CACnB;AACD;IACI,gBAAgB;CACnB;AACD;IACI,gBAAgB;CACnB;AACD;IACI,eAAe;IACf,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;IACjB,eAAe;IACf,aAAa;IACb,WAAW;CACd;AACD;IACI,eAAe;CAClB;AACD;IACI,gBAAgB;CACnB;AACD;IACI,kBAAkB;CACrB;AACD;IACI,kBAAkB;IAClB,eAAe;IACf,WAAW;CACd;AACD;IACI;QACI,WAAW;QACX,gBAAgB;KACnB;IACD;QACI,iBAAiB;KACpB;IACD;QACI,gBAAgB;KACnB;IACD;QACI,gBAAgB;KACnB;IACD;QACI,uBAAuB;QACvB,kBAAkB;QAClB,eAAe;KAClB;IACD;QACI,eAAe;KAClB;IACD;QACI,eAAe;KAClB;IACD;QACI,iBAAiB;KACpB;EACH;IACE,mBAAmB;KAClB,gBAAgB;GAClB;CACF;AACD;IACI;QACI,iBAAiB;KACpB;CACJ;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,8DAA8D;EAC9D,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,YAAY;EACZ,YAAY;EACZ,oBAAoB;EACpB,mBAAmB;EACnB,6BAA6B;EAC7B,8BAA8B,EAAE;AAClC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,SAAS;EACT,YAAY,EAAE;AAChB;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB,WAAW;AACX,uDAAuD;AACvD,KAAK;AACL,WAAW;AACX,uDAAuD;AACvD,KAAK;AACL,2BAA2B;AAC3B,2DAA2D;AAC3D,KAAK;AACL,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n  font-family: Shabnam;\n}\n.white {\n  color: white;\n}\n.black {\n  color: black;\n}\n.purple {\n  color: #4c0d6b\n}\n.light-blue-background {\n  background-color: #3ad2cb\n}\n.white-background {\n  background-color: white;\n}\n.dark-green-background {\n  background-color: #185956;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-light {\n  font-weight: 200;\n}\n.font-medium {\n  font-weight: 600;\n}\n.center-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.center-column {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.text-align-right {\n  text-align: right;\n}\n.text-align-left {\n  text-align: left;\n}\n.text-align-center {\n  text-align: center;\n}\n.no-list-style {\n  list-style: none;\n}\n.clear-input {\n  outline: none;\n  border: none;\n}\n.border-radius {\n  border-radius: 9px;\n}\n.has-transition {\n  -webkit-transition: box-shadow 0.3s ease-in-out;\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.search-theme{\n  background: url(\"./geometry2.png\");\n  height: 15vh;\n  margin-top: 10vh;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbody {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n@-webkit-keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@keyframes fade\n{\n    0%   {opacity:1}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 1}\n}\n@-webkit-keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade2\n{\n    0%   {opacity:0}\n    25% { opacity: 1}\n    50% { opacity: 0}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@keyframes fade3\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 1}\n    75% { opacity: 0}\n    100% { opacity: 0}\n}\n@-webkit-keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n@keyframes fade4\n{\n    0%   {opacity:0}\n    25% { opacity: 0}\n    50% { opacity: 0}\n    75% { opacity: 1}\n    100% { opacity: 0}\n}\n.main-form {\n  position: absolute;\n  top: 14vh;\n}\n.search-button {\n  font-family: Shabnam !important;\n}\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n}\n.second-part {\n  margin-top: 4%;\n}\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl;\n}\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n}\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n}\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n}\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  color: black;\n}\n.search-input::-webkit-input-placeholder{\n  padding-right: 4%;\n}\n.search-input:-ms-input-placeholder{\n  padding-right: 4%;\n}\n.search-input::-ms-input-placeholder{\n  padding-right: 4%;\n}\n.search-input::placeholder{\n  padding-right: 4%;\n}\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl;\n}\n.deal-type-section {\n  text-align: right;\n}\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%;\n  }\n  .search-button {\n    display: block;\n    margin: 5% auto;\n  }\n}\nbody {\n    font-family: Shabnam;\n    overflow-x: hidden;\n}\n.logo-section {\n    margin-bottom: 7%;\n    text-align: center;\n}\n.logo-image {\n    max-width: 120px;\n}\n.slider {\n    height: 100vh;\n    margin: 0 auto;\n    position: relative;\n}\n.header-main {\n    z-index: 999;\n    top: 3vh;\n    position: absolute;\n}\n.header-main-boarder {\n    border: thin solid white;\n    border-radius: 10px;\n}\n.dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 0;\n    top: 6vh;\n}\n.slider-column {\n    padding: 0;\n}\n.slide1,.slide2,.slide3,.slide4 {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n}\n.slide1 {\n    background: url(michal-kubalczyk-260909-unsplash.jpg) no-repeat 90% 80%;\n    background-size: cover;\n    animation:fade 10s infinite;\n    -webkit-animation:fade 10s infinite;\n\n\n}\n.slide2 {\n    background: url(mahdiar-mahmoodi-452489-unsplash.jpg) no-repeat center;\n    background-size: cover;\n    animation:fade2 10s infinite;\n    -webkit-animation:fade2 10s infinite;\n}\n.slide3 {\n    background: url(casey-horner-533586-unsplash.jpg) no-repeat center;\n    background-size: cover;\n    animation:fade3 10s infinite;\n    -webkit-animation:fade3 10s infinite;\n}\n.slide4 {\n    background: url(luke-van-zyl-504032-unsplash.jpg) no-repeat center;\n    background-size: cover;\n    animation:fade4 10s infinite;\n    -webkit-animation:fade4 10s infinite;\n}\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n    text-decoration: none;\n    outline: none;\n    cursor: pointer;\n}\n.features-section {\n    position: relative;\n    margin-top: -5vh;\n}\ninput[type=radio] {\n    font-size: 16px;\n}\n.input-label {\n    font-weight: lighter;\n    font-size: 16px;\n}\n.feature-box {\n    -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n    margin-left: 2%;\n    height: 36vh;\n    width: 29% !important;\n    min-height: 42vh;\n}\n.features-container > .feature-box:nth-child(2) {\n    margin-left: 5%;\n}\n.features-container > .feature-box:nth-child(3) {\n    margin-left: 5%;\n}\n.feature-box > div > img {\n    max-width: 70px;\n}\n.why-us {\n    margin-top: 6%;\n    margin-bottom: 6%;\n}\n.why-us-img {\n    max-width: 300px;\n}\n.why-list {\n    list-style: none;\n    display: block;\n    color: black;\n    padding: 0;\n}\n.why-list > li {\n    margin-top: 3%;\n}\n.why-list > li > i {\n    margin-left: 2%;\n}\n.why-us-img-column {\n    text-align: right;\n}\n.reasons-section {\n    text-align: right;\n    direction: rtl;\n    padding: 0;\n}\n@media (max-width: 768px) {\n    .header-main-boarder {\n        width: 40%;\n        margin-left: 1%;\n    }\n    .mobile-header {\n        margin-top: -12%;\n    }\n    .logo-image {\n        max-width: 90px;\n    }\n    .main-title {\n        font-size: 20px;\n    }\n    .feature-box {\n        width: 100% !important;\n        margin-bottom: 5%;\n        margin-left: 0;\n    }\n    .features-container > .feature-box:nth-child(2) {\n        margin-left: 0;\n    }\n    .features-container > .feature-box:nth-child(3) {\n        margin-left: 0;\n    }\n    .why-us-img {\n        max-width: 247px;\n    }\n  .features-section {\n    position: relative;\n     margin-top: 2vh;\n  }\n}\n@media (max-width: 320px) {\n    .mobile-header {\n        margin-top: -12%;\n    }\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  /*background: url(\"../assets/images/pattern/geometry2.png\");*/\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-tag {\n  width: 100%;\n  height: 60%;\n  border-radius: 10px;\n  position: absolute;\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 6vh;\n  border-bottom: thin solid black; }\n.price-div {\n  position: absolute;\n  top: 85%;\n  width: 100%; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%;\n  text-align: right; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n/*#img-1{*/\n/*background-image: url(../assets/images/villa1.jpg);*/\n/*}*/\n/*#img-2{*/\n/*background-image: url(../assets/images/villa2.jpg);*/\n/*}*/\n/*#img-3, #img-4, #img-5{*/\n/*background-image: url(../assets/images/underwater.jpg);*/\n/*}*/\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n.browserupgrade {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA,iBAAiB;AACjB;;;;;;;GAOG;AACH;;;;;;;GAOG;AACH;EACE;;gFAE8E;;EAE9E,kEAAkE;;EAElE;;gFAE8E;;EAE9E,4BAA4B;;EAE5B;;gFAE8E;;EAE9E,uBAAuB,EAAE,gCAAgC;EACzD,uBAAuB,EAAE,2BAA2B;EACpD,uBAAuB,EAAE,6BAA6B;EACtD,wBAAwB,CAAC,iCAAiC;CAC3D;AACD;EACE,qBAAqB;EACrB,mCAAmC;EACnC,uKAAuK;EACvK,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,6BAA6B;EAC7B,qJAAqJ;EACrJ,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,kCAAkC;EAClC,oKAAoK;EACpK,iBAAiB,EAAE;AACrB;;;;GAIG;AACH;;gFAEgF;AAChF;EACE,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;EACnB,WAAW,EAAE;AACf;EACE,UAAU;EACV,qBAAqB,EAAE;AACzB;EACE,eAAe,EAAE;AACnB;;;;;;GAMG;AACH;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;;GAEG;AACH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW,EAAE;AACf;;;;GAIG;AACH;;;;;;EAME,uBAAuB,EAAE;AAC3B;;GAEG;AACH;EACE,UAAU;EACV,UAAU;EACV,WAAW,EAAE;AACf;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;gFAEgF;AAChF;EACE,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,iBAAiB,EAAE;AACrB;;;;gFAIgF;AAChF;EACE;;;IAGE,mCAAmC;IACnC,uBAAuB;IACvB,+DAA+D;IAC/D,oCAAoC;YAC5B,4BAA4B;IACpC,6BAA6B,EAAE;EACjC;;IAEE,2BAA2B,EAAE;EAC/B;IACE,6BAA6B,EAAE;EACjC;IACE,8BAA8B,EAAE;EAClC;;;KAGG;EACH;;IAEE,YAAY,EAAE;EAChB;;IAEE,uBAAuB;IACvB,yBAAyB,EAAE;EAC7B;;;KAGG;EACH;IACE,4BAA4B,EAAE;EAChC;;IAEE,yBAAyB,EAAE;EAC7B;IACE,2BAA2B,EAAE;EAC/B;;;IAGE,WAAW;IACX,UAAU,EAAE;EACd;;IAEE,wBAAwB,EAAE,EAAE","file":"Layout.css","sourcesContent":["@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n:global(.browserupgrade) {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB;EACE,gCAAgC,EAAE;AACpC,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.second-part {\n  margin-top: 4%; }\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15); }\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  font-family: Shabnam !important;\n  color: black !important; }\n.search-input::-webkit-input-placeholder {\n  padding-right: 4%; }\n.search-input:-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::placeholder {\n  padding-right: 4%; }\n.error {\n  border: 1px solid red !important; }\n.err-msg {\n  text-align: right;\n  direction: rtl;\n  color: #f16464; }\n.search-button:disabled {\n  background-color: #b3aeae; }\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl; }\n.deal-type-section {\n  text-align: right; }\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%; }\n  .search-button {\n    display: block;\n    margin: 5% auto; } }\nbody {\n  font-family: Shabnam;\n  overflow-x: hidden; }\n.logo-section {\n  margin-bottom: 7%;\n  text-align: center; }\n.logo-image {\n  max-width: 120px; }\n.slider {\n  height: 100vh;\n  margin: 0 auto;\n  position: relative; }\n.header-main {\n  z-index: 999;\n  top: 3vh;\n  position: absolute; }\n.header-main-boarder {\n  border: thin solid white;\n  border-radius: 10px; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 0;\n  top: 6vh; }\n.slider-column {\n  padding: 0; }\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n  text-decoration: none;\n  outline: none;\n  cursor: pointer; }\n.features-section {\n  position: relative;\n  margin-top: -5vh; }\ninput[type=radio] {\n  font-size: 16px; }\n.input-label {\n  font-weight: lighter;\n  font-size: 16px;\n  margin-left: 15%; }\n.feature-box {\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  margin-left: 2%;\n  height: 36vh;\n  width: 29% !important;\n  min-height: 42vh; }\n.features-container > .feature-box:nth-child(2) {\n  margin-left: 5%; }\n.features-container > .feature-box:nth-child(3) {\n  margin-left: 5%; }\n.feature-box > div > img {\n  max-width: 70px; }\n.why-us {\n  margin-top: 6%;\n  margin-bottom: 6%; }\n.why-us-img {\n  max-width: 300px; }\n.why-list {\n  list-style: none;\n  display: block;\n  color: black;\n  padding: 0; }\n.why-list > li {\n  margin-top: 3%; }\n.why-list > li > i {\n  margin-left: 2%; }\n.why-us-img-column {\n  text-align: right; }\n.reasons-section {\n  text-align: right;\n  direction: rtl;\n  padding: 0; }\n.building-type-label {\n  height: 5vh; }\n@media (max-width: 768px) {\n  .header-main-boarder {\n    width: 40%;\n    margin-left: 1%; }\n  .mobile-header {\n    margin-top: -12%; }\n  .logo-image {\n    max-width: 90px; }\n  .main-title {\n    font-size: 20px; }\n  .feature-box {\n    width: 100% !important;\n    margin-bottom: 5%;\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(2) {\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(3) {\n    margin-left: 0; }\n  .why-us-img {\n    max-width: 247px; } }\n@media (max-width: 320px) {\n  .mobile-header {\n    margin-top: -12%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/SearchBox/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,aAAa;EACb,qCAAqC;EACrC,YAAY;EACZ,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,eAAe,EAAE;AACnB;EACE,eAAe;EACf,aAAa;EACb,eAAe,EAAE;AACnB;EACE,WAAW;EACX,gBAAgB;EAChB,kDAAkD;UAC1C,0CAA0C,EAAE;AACtD;EACE,iDAAiD;UACzC,yCAAyC,EAAE;AACrD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,YAAY;EACZ,oBAAoB;EACpB,gBAAgB;EAChB,gCAAgC;EAChC,wBAAwB,EAAE;AAC5B;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB,EAAE;AACtB;EACE,iCAAiC,EAAE;AACrC;EACE,kBAAkB;EAClB,eAAe;EACf,eAAe,EAAE;AACnB;EACE,0BAA0B,EAAE;AAC9B;EACE,YAAY;EACZ,aAAa;EACb,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE;IACE,gBAAgB,EAAE;EACpB;IACE,eAAe;IACf,gBAAgB,EAAE,EAAE;AACxB;EACE,qBAAqB;EACrB,mBAAmB,EAAE;AACvB;EACE,kBAAkB;EAClB,mBAAmB,EAAE;AACvB;EACE,iBAAiB,EAAE;AACrB;EACE,cAAc;EACd,eAAe;EACf,mBAAmB,EAAE;AACvB;EACE,aAAa;EACb,SAAS;EACT,mBAAmB,EAAE;AACvB;EACE,yBAAyB;EACzB,oBAAoB,EAAE;AACxB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,oDAAoD;UAC5C,4CAA4C;EACpD,mBAAmB;EACnB,WAAW;EACX,QAAQ;EACR,SAAS,EAAE;AACb;EACE,WAAW,EAAE;AACf;;;EAGE,sBAAsB;EACtB,cAAc;EACd,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,iBAAiB,EAAE;AACrB;EACE,gBAAgB,EAAE;AACpB;EACE,qBAAqB;EACrB,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,kDAAkD;UAC1C,0CAA0C;EAClD,gBAAgB;EAChB,aAAa;EACb,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE,gBAAgB,EAAE;AACpB;EACE,gBAAgB,EAAE;AACpB;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,kBAAkB,EAAE;AACtB;EACE,iBAAiB,EAAE;AACrB;EACE,iBAAiB;EACjB,eAAe;EACf,aAAa;EACb,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,gBAAgB,EAAE;AACpB;EACE,kBAAkB,EAAE;AACtB;EACE,kBAAkB;EAClB,eAAe;EACf,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE;IACE,WAAW;IACX,gBAAgB,EAAE;EACpB;IACE,iBAAiB,EAAE;EACrB;IACE,gBAAgB,EAAE;EACpB;IACE,gBAAgB,EAAE;EACpB;IACE,uBAAuB;IACvB,kBAAkB;IAClB,eAAe,EAAE;EACnB;IACE,eAAe,EAAE;EACnB;IACE,eAAe,EAAE;EACnB;IACE,iBAAiB,EAAE,EAAE;AACzB;EACE;IACE,iBAAiB,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.search-section, .submit-home-link {\n  color: white;\n  background-color: rgba(0, 0, 0, 0.4);\n  padding: 2%;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.second-part {\n  margin-top: 4%; }\n.submit-home-link {\n  margin-top: 2%;\n  padding: 5px;\n  direction: rtl; }\n.search-button {\n  width: 80%;\n  min-height: 5vh;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1); }\n.search-button:visited {\n  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0 40px rgba(0, 0, 0, 0.15); }\n.search-button:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\n.search-input {\n  width: 100%;\n  border-radius: 10px;\n  min-height: 5vh;\n  font-family: Shabnam !important;\n  color: black !important; }\n.search-input::-webkit-input-placeholder {\n  padding-right: 4%; }\n.search-input:-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::-ms-input-placeholder {\n  padding-right: 4%; }\n.search-input::placeholder {\n  padding-right: 4%; }\n.error {\n  border: 1px solid red !important; }\n.err-msg {\n  text-align: right;\n  direction: rtl;\n  color: #f16464; }\n.search-button:disabled {\n  background-color: #b3aeae; }\nselect {\n  height: 5vh;\n  color: black;\n  direction: rtl; }\n.deal-type-section {\n  text-align: right; }\n@media (max-width: 768px) {\n  .input-label {\n    margin-left: 1%; }\n  .search-button {\n    display: block;\n    margin: 5% auto; } }\nbody {\n  font-family: Shabnam;\n  overflow-x: hidden; }\n.logo-section {\n  margin-bottom: 7%;\n  text-align: center; }\n.logo-image {\n  max-width: 120px; }\n.slider {\n  height: 100vh;\n  margin: 0 auto;\n  position: relative; }\n.header-main {\n  z-index: 999;\n  top: 3vh;\n  position: absolute; }\n.header-main-boarder {\n  border: thin solid white;\n  border-radius: 10px; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 0;\n  top: 6vh; }\n.slider-column {\n  padding: 0; }\n.submit-house-button,\n.submit-house-button:visited,\n.submit-house-button:hover {\n  text-decoration: none;\n  outline: none;\n  cursor: pointer; }\n.features-section {\n  position: relative;\n  margin-top: -5vh; }\ninput[type=radio] {\n  font-size: 16px; }\n.input-label {\n  font-weight: lighter;\n  font-size: 16px;\n  margin-left: 15%; }\n.feature-box {\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  margin-left: 2%;\n  height: 36vh;\n  width: 29% !important;\n  min-height: 42vh; }\n.features-container > .feature-box:nth-child(2) {\n  margin-left: 5%; }\n.features-container > .feature-box:nth-child(3) {\n  margin-left: 5%; }\n.feature-box > div > img {\n  max-width: 70px; }\n.why-us {\n  margin-top: 6%;\n  margin-bottom: 6%; }\n.why-us-img {\n  max-width: 300px; }\n.why-list {\n  list-style: none;\n  display: block;\n  color: black;\n  padding: 0; }\n.why-list > li {\n  margin-top: 3%; }\n.why-list > li > i {\n  margin-left: 2%; }\n.why-us-img-column {\n  text-align: right; }\n.reasons-section {\n  text-align: right;\n  direction: rtl;\n  padding: 0; }\n.building-type-label {\n  height: 5vh; }\n@media (max-width: 768px) {\n  .header-main-boarder {\n    width: 40%;\n    margin-left: 1%; }\n  .mobile-header {\n    margin-top: -12%; }\n  .logo-image {\n    max-width: 90px; }\n  .main-title {\n    font-size: 20px; }\n  .feature-box {\n    width: 100% !important;\n    margin-bottom: 5%;\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(2) {\n    margin-left: 0; }\n  .features-container > .feature-box:nth-child(3) {\n    margin-left: 0; }\n  .why-us-img {\n    max-width: 247px; } }\n@media (max-width: 320px) {\n  .mobile-header {\n    margin-top: -12%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/url/escape.js":
/***/ (function(module, exports) {

module.exports = function escape(url) {
    if (typeof url !== 'string') {
        return url
    }
    // If url is already wrapped in quotes, remove them
    if (/^['"].*['"]$/.test(url)) {
        url = url.slice(1, -1);
    }
    // Should url be wrapped?
    // See https://drafts.csswg.org/css-values-3/#urls
    if (/["'() \t\n]/.test(url)) {
        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
    }

    return url
}


/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/actions/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["loginRequest"] = loginRequest;
/* harmony export (immutable) */ __webpack_exports__["loginSuccess"] = loginSuccess;
/* harmony export (immutable) */ __webpack_exports__["loginFailure"] = loginFailure;
/* harmony export (immutable) */ __webpack_exports__["loadLoginStatus"] = loadLoginStatus;
/* harmony export (immutable) */ __webpack_exports__["loginUser"] = loginUser;
/* harmony export (immutable) */ __webpack_exports__["logoutRequest"] = logoutRequest;
/* harmony export (immutable) */ __webpack_exports__["logoutSuccess"] = logoutSuccess;
/* harmony export (immutable) */ __webpack_exports__["logoutFailure"] = logoutFailure;
/* harmony export (immutable) */ __webpack_exports__["logoutUser"] = logoutUser;
/* harmony export (immutable) */ __webpack_exports__["initiateRequest"] = initiateRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateSuccess"] = initiateSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateFailure"] = initiateFailure;
/* harmony export (immutable) */ __webpack_exports__["initiate"] = initiate;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersRequest"] = initiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersSuccess"] = initiateUsersSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersFailure"] = initiateUsersFailure;
/* harmony export (immutable) */ __webpack_exports__["initiateUsers"] = initiateUsers;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserRequest"] = getCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserSuccess"] = getCurrentUserSuccess;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserFailure"] = getCurrentUserFailure;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUser"] = getCurrentUser;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditRequest"] = increaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditSuccess"] = increaseCreditSuccess;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditFailure"] = increaseCreditFailure;
/* harmony export (immutable) */ __webpack_exports__["increaseCredit"] = increaseCredit;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__ = __webpack_require__("./src/api-handlers/authentication.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function loginRequest(username, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["K" /* USER_LOGIN_REQUEST */],
    payload: {
      username,
      password
    }
  };
}

function loginSuccess(info) {
  console.log(info);
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["L" /* USER_LOGIN_SUCCESS */],
    payload: { info }
  };
}

function loginFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["J" /* USER_LOGIN_FAILURE */],
    payload: { error }
  };
}

function loadLoginStatus() {
  const loginInfo = Object(__WEBPACK_IMPORTED_MODULE_0__utils__["c" /* getLoginInfo */])();
  if (loginInfo) {
    return loginSuccess(loginInfo);
  }
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["I" /* USER_IS_NOT_LOGGED_IN */]
  };
}

function getCaptchaToken() {
  if (false) {
    const loginForm = $('#loginForm').get(0);
    if (loginForm && typeof loginForm['g-recaptcha-response'] === 'object') {
      return loginForm['g-recaptcha-response'].value;
    }
  }
  return null;
}

function loginUser(username, password) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(loginRequest(username, password));
      try {
        // const captchaToken = getCaptchaToken();
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["c" /* sendLoginRequest */])(username, password);
        dispatch(loginSuccess(res));
        localStorage.setItem('loginInfo', JSON.stringify(res));
        // console.log(location.pathname);
        const { pathname } = getState().house;
        console.log(pathname);
        __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push(pathname);
      } catch (err) {
        dispatch(loginFailure(err));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}

function logoutRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["N" /* USER_LOGOUT_REQUEST */],
    payload: {}
  };
}

function logoutSuccess() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["O" /* USER_LOGOUT_SUCCESS */],
    payload: {
      message: 'خروج موفقیت آمیز'
    }
  };
}

function logoutFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["M" /* USER_LOGOUT_FAILURE */],
    payload: { error }
  };
}

function logoutUser() {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(logoutRequest());
      try {
        dispatch(logoutSuccess());
        localStorage.clear();
        location.assign('/login');
      } catch (err) {
        dispatch(logoutFailure(err));
      }
    });

    return function (_x3) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function initiateRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["q" /* GET_INIT_REQUEST */]
  };
}

function initiateSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["r" /* GET_INIT_SUCCESS */],
    payload: { response }
  };
}

function initiateFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["p" /* GET_INIT_FAILURE */],
    payload: { err }
  };
}

function initiate() {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["a" /* sendInitiateRequest */])();
        dispatch(initiateSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateFailure(error));
      }
    });

    return function (_x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function initiateUsersRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["t" /* GET_INIT_USER_REQUEST */]
  };
}

function initiateUsersSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["u" /* GET_INIT_USER_SUCCESS */],
    payload: { response }
  };
}

function initiateUsersFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["s" /* GET_INIT_USER_FAILURE */],
    payload: { err }
  };
}

function initiateUsers() {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateUsersRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["b" /* sendInitiateUsersRequest */])();
        dispatch(initiateUsersSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateUsersFailure(error));
      }
    });

    return function (_x5) {
      return _ref4.apply(this, arguments);
    };
  })();
}

function getCurrentUserRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["e" /* GET_CURR_USER_REQUEST */]
  };
}

function getCurrentUserSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["f" /* GET_CURR_USER_SUCCESS */],
    payload: { response }
  };
}

function getCurrentUserFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["d" /* GET_CURR_USER_FAILURE */],
    payload: { err }
  };
}

function getCurrentUser(userId) {
  return (() => {
    var _ref5 = _asyncToGenerator(function* (dispatch) {
      dispatch(getCurrentUserRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["d" /* sendgetCurrentUserRequest */])(userId);
        dispatch(getCurrentUserSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getCurrentUserFailure(error));
      }
    });

    return function (_x6) {
      return _ref5.apply(this, arguments);
    };
  })();
}

function increaseCreditRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["z" /* INCREASE_CREDIT_REQUEST */]
  };
}

function increaseCreditSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["A" /* INCREASE_CREDIT_SUCCESS */],
    payload: { response }
  };
}

function increaseCreditFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["y" /* INCREASE_CREDIT_FAILURE */],
    payload: { err }
  };
}

function increaseCredit(amount) {
  return (() => {
    var _ref6 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(increaseCreditRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["e" /* sendincreaseCreditRequest */])(amount, currUser.userId);
        dispatch(increaseCreditSuccess(res));
        dispatch(getCurrentUser(currUser.userId));
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["f" /* notifySuccess */])('افزایش اعتبار با موفقیت انجام شد.');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(increaseCreditFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref6.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/actions/search.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["setSearchObjRequest"] = setSearchObjRequest;
/* harmony export (immutable) */ __webpack_exports__["setSearchObjSuccess"] = setSearchObjSuccess;
/* harmony export (immutable) */ __webpack_exports__["setSearchObjFailure"] = setSearchObjFailure;
/* harmony export (immutable) */ __webpack_exports__["setSearchObj"] = setSearchObj;
/* harmony export (immutable) */ __webpack_exports__["searchHousesRequest"] = searchHousesRequest;
/* harmony export (immutable) */ __webpack_exports__["searchHousesSuccess"] = searchHousesSuccess;
/* harmony export (immutable) */ __webpack_exports__["searchHousesFailure"] = searchHousesFailure;
/* harmony export (immutable) */ __webpack_exports__["searchHouses"] = searchHouses;
/* harmony export (immutable) */ __webpack_exports__["pollHousesRequest"] = pollHousesRequest;
/* harmony export (immutable) */ __webpack_exports__["pollHousesSuccess"] = pollHousesSuccess;
/* harmony export (immutable) */ __webpack_exports__["pollHousesFailure"] = pollHousesFailure;
/* harmony export (immutable) */ __webpack_exports__["pollHouses"] = pollHouses;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_handlers_search__ = __webpack_require__("./src/api-handlers/search.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils__ = __webpack_require__("./src/utils/index.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function setSearchObjRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["G" /* SET_SEARCH_OBJ_REQUEST */]
  };
}

function setSearchObjSuccess(minArea, dealType, buildingType, maxPrice) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["H" /* SET_SEARCH_OBJ_SUCCESS */],
    payload: { minArea, dealType, buildingType, maxPrice }
  };
}

function setSearchObjFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["F" /* SET_SEARCH_OBJ_FAILURE */],
    payload: { err }
  };
}

function setSearchObj(minArea, dealType, buildingType, maxPrice) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch) {
      dispatch(setSearchObjSuccess(minArea, dealType, buildingType, maxPrice));
      __WEBPACK_IMPORTED_MODULE_1__history__["a" /* default */].push('/houses');
    });

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  })();
}
function searchHousesRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["h" /* GET_HOUSES_REQUEST */]
  };
}

function searchHousesSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["i" /* GET_HOUSES_SUCCESS */],
    payload: { response }
  };
}

function searchHousesFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["g" /* GET_HOUSES_FAILURE */],
    payload: { err }
  };
}

function searchHouses(minArea, dealType, buildingType, maxPrice) {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(searchHousesRequest());
      try {
        if (!buildingType) {
          buildingType = null;
        }
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_search__["a" /* sendSearchHousesRequest */])(parseFloat(minArea), parseFloat(dealType), buildingType, parseFloat(maxPrice));
        dispatch(searchHousesSuccess(res));
        dispatch(setSearchObj(minArea, dealType, buildingType, maxPrice));
        if (res.length === 0) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["f" /* notifySuccess */])('خانه ای با این این مشخصات ثبت نشده است');
        }
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(searchHousesFailure(error));
      }
    });

    return function (_x2) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function pollHousesRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["C" /* POLL_HOUSES_REQUEST */]
  };
}

function pollHousesSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["D" /* POLL_HOUSES_SUCCESS */],
    payload: { response }
  };
}

function pollHousesFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__constants__["B" /* POLL_HOUSES_FAILURE */],
    payload: { err }
  };
}

function pollHouses(minArea, dealType, buildingType, maxPrice) {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(pollHousesRequest());
      try {
        if (!buildingType) {
          buildingType = null;
        }
        let res = yield Object(__WEBPACK_IMPORTED_MODULE_2__api_handlers_search__["b" /* sendpollHousesRequest */])(parseFloat(minArea), parseFloat(dealType), buildingType, parseFloat(maxPrice));
        console.log(res);
        if (!res) {
          res = getState().search.houses;
          console.log('hala');
          console.log(res);
        }
        dispatch(pollHousesSuccess(res));
        Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])('اطلاعات جست و جوی شما بروز شد.');

        dispatch(setSearchObj(minArea, dealType, buildingType, maxPrice));
        if (res.length === 0) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["f" /* notifySuccess */])('خانه ای با این این مشخصات ثبت نشده است');
        }
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_3__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(pollHousesFailure(error));
      }
    });

    return function (_x3, _x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/api-handlers/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = sendincreaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["a"] = sendInitiateRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendInitiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendLoginRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_paths__ = __webpack_require__("./src/config/paths.js");



function sendincreaseCreditRequest(amount, id) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["g" /* increaseCreditUrl */])(amount, id), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendgetCurrentUserRequest(userId) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["b" /* getCurrentUserUrl */])(userId), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateUsersRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendLoginRequest(username, password) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["d" /* getPostConfig */])({
    username,
    password
  }));
}

/***/ }),

/***/ "./src/api-handlers/search.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = sendSearchHousesRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendpollHousesRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_paths__ = __webpack_require__("./src/config/paths.js");



/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

function sendSearchHousesRequest(minArea, dealType, buildingType, maxPrice) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_2__config_paths__["f" /* getSearchHousesUrl */], Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])({ minArea, dealType, buildingType, maxPrice }));
}

function sendpollHousesRequest(minArea, dealType, buildingType, maxPrice) {
  return Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_2__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* getPostConfig */])({ minArea, dealType, buildingType, maxPrice }));
}

/***/ }),

/***/ "./src/components/Footer/200px-Instagram_logo_2016.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Instagram_logo_2016.svg.png?2fc2d5c1";

/***/ }),

/***/ "./src/components/Footer/200px-Telegram_logo.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Telegram_logo.svg.png?6fa2804c";

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__ = __webpack_require__("./src/components/Footer/Twitter_bird_logo_2012.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__ = __webpack_require__("./src/components/Footer/200px-Instagram_logo_2016.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__ = __webpack_require__("./src/components/Footer/200px-Telegram_logo.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Footer/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/Footer.js';
/* eslint-disable react/no-danger */








class Footer extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'footer',
      { className: 'site-footer center-column dark-green-background white', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'row', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4 text-align-left', __source: {
              fileName: _jsxFileName,
              lineNumber: 15
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'ul',
            { className: 'social-icons-list no-list-style', __source: {
                fileName: _jsxFileName,
                lineNumber: 16
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 17
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'instagram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 18
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 24
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'twitter-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 25
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 31
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'telegram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              })
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-8 text-align-right', __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 41
              },
              __self: this
            },
            '\u062A\u0645\u0627\u0645\u06CC \u062D\u0642\u0648\u0642 \u0627\u06CC\u0646 \u0648\u0628\u200C\u0633\u0627\u06CC\u062A \u0645\u062A\u0639\u0644\u0642 \u0628\u0647 \u0645\u0647\u0631\u0646\u0627\u0632 \u062B\u0627\u0628\u062A \u0648 \u0647\u0648\u0645\u0646 \u0634\u0631\u06CC\u0641 \u0632\u0627\u062F\u0647 \u0645\u06CC\u200C\u0628\u0627\u0634\u062F.'
          )
        )
      )
    );
  }
}

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Twitter_bird_logo_2012.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/Twitter_bird_logo_2012.svg.png?248bfa8d";

/***/ }),

/***/ "./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/HousesPage/HousesPage.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css__ = __webpack_require__("./src/components/HousesPage/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__logo_png__ = __webpack_require__("./src/components/HousesPage/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__SearchBox__ = __webpack_require__("./src/components/SearchBox/SearchBox.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/HousesPage/HousesPage.js';
/* eslint-disable react/no-danger */








class HousesPage extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.getPriceComponent = house => {
      if (house.dealType) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12', __source: {
              fileName: _jsxFileName,
              lineNumber: 21
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'col-xs-6 flex-center flex-row-rev padding-right-price ', __source: {
                fileName: _jsxFileName,
                lineNumber: 22
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'h4',
              { className: 'persian black-font margin-1 price-heading', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 23
                },
                __self: this
              },
              '\u0631\u0647\u0646 ',
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'span',
                { className: 'margin-1', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                  },
                  __self: this
                },
                house.basePrice
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'span',
                { className: 'persian light-grey-font margin-1', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 25
                  },
                  __self: this
                },
                ' \u062A\u0648\u0645\u0627\u0646 '
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'col-xs-6 flex-center flex-row-rev ', __source: {
                fileName: _jsxFileName,
                lineNumber: 28
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'h4',
              { className: 'persian black-font margin-1 price-heading', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 29
                },
                __self: this
              },
              '\u0627\u062C\u0627\u0631\u0647 ',
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'span',
                { className: 'margin-1', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 30
                  },
                  __self: this
                },
                house.rentPrice
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'span',
                { className: 'persian light-grey-font margin-1', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 31
                  },
                  __self: this
                },
                ' \u062A\u0648\u0645\u0627\u0646 '
              )
            )
          )
        );
      }
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'col-xs-7 flex-center flex-row-rev padding-right-price', __source: {
            fileName: _jsxFileName,
            lineNumber: 38
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'h4',
          { className: 'persian black-font margin-1 price-heading', __source: {
              fileName: _jsxFileName,
              lineNumber: 39
            },
            __self: this
          },
          '\u0642\u06CC\u0645\u062A',
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'span',
            { className: 'margin-1', __source: {
                fileName: _jsxFileName,
                lineNumber: 41
              },
              __self: this
            },
            house.sellPrice
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'span',
            { className: 'persian light-grey-font margin-1', __source: {
                fileName: _jsxFileName,
                lineNumber: 42
              },
              __self: this
            },
            '\u062A\u0648\u0645\u0627\u0646'
          )
        )
      );
    }, _temp;
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
  }

  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'home-panel', __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'section',
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 50
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'search-theme flex-center', __source: {
              fileName: _jsxFileName,
              lineNumber: 51
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'container', __source: {
                fileName: _jsxFileName,
                lineNumber: 52
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'row ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 53
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 54
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'h3',
                  { className: 'text-align-right persian-bold blue-font flex-center', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 55
                    },
                    __self: this
                  },
                  '\u0646\u062A\u0627\u06CC\u062C \u062C\u0633\u062A\u062C\u0648'
                )
              )
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container', __source: {
              fileName: _jsxFileName,
              lineNumber: 62
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row', __source: {
                fileName: _jsxFileName,
                lineNumber: 63
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'h4',
              { className: 'persian grey-font text-align-center mar-5', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 64
                },
                __self: this
              },
              '\u0628\u0631\u0627\u06CC \u0645\u0634\u0627\u0647\u062F\u0647 \u0627\u0637\u0644\u0627\u0639\u0627\u062A \u0628\u06CC\u0634\u062A\u0631 \u062F\u0631\u0628\u0627\u0631\u0647 \u06CC \u0647\u0631 \u0645\u0644\u06A9 \u0631\u0648\u06CC \u0622\u0646 \u06A9\u0644\u06CC\u06A9 \u06A9\u0646\u06CC\u062F'
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row ', __source: {
                fileName: _jsxFileName,
                lineNumber: 68
              },
              __self: this
            },
            this.props.houses.map(house => __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              {
                onClick: () => {
                  __WEBPACK_IMPORTED_MODULE_5__history__["a" /* default */].push(`/houses/${house.id}`);
                },
                className: 'col-xs-12  col-md-5 no-padding house-card border-radius',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 70
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'container-fluid img-height', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 76
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row house-img img-border-radius  ', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 77
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { className: 'img-tag', src: house.imageURL, alt: 'house-img', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 78
                    },
                    __self: this
                  }),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-md-4 white-font mar-top-10 col-xs-8 border-radius', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 79
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h6',
                      {
                        className: `mar-5 white-font padd-5 persian text-align-center border-radius ${house.dealType === 1 ? 'pink-background' : 'purple-background'}`,
                        __source: {
                          fileName: _jsxFileName,
                          lineNumber: 80
                        },
                        __self: this
                      },
                      house.dealType === 1 ? 'اجاره' : 'فروش'
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row border-bottom', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 91
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-6 flex-center text-align-left', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 92
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian black-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 93
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-map-marker purple-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 94
                        },
                        __self: this
                      }),
                      '\xA0',
                      house.address,
                      '\xA0'
                    )
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-6 pull-right flex-center flex-row-rev', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 100
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian black-font text-align-right', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 101
                        },
                        __self: this
                      },
                      house.area,
                      ' \u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row flex-row-rev', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 106
                    },
                    __self: this
                  },
                  this.getPriceComponent(house)
                )
              )
            ))
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row mar-top', __source: {
                fileName: _jsxFileName,
                lineNumber: 113
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 114
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'h4',
                { className: 'persian light-grey-font text-align-center', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 115
                  },
                  __self: this
                },
                '\u062C\u0633\u062A\u062C\u0648\u06CC \u0645\u062C\u062F\u062F'
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'col-xs-12 flex-center mar-bottom no-padding', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 119
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'col-xs-12 main-form main-form-search-result no-padding', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 120
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__SearchBox__["a" /* default */], { handleSubmit: this.props.handleSubmit, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 121
                  },
                  __self: this
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'col-xs-12 submit-home-link border-radius text-align-center', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 122
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h5',
                    { className: 'submit-home-heading white', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 123
                      },
                      __self: this
                    },
                    '\u0635\u0627\u062D\u0628 \u062E\u0627\u0646\u0647 \u0647\u0633\u062A\u06CC\u062F\u061F \u062E\u0627\u0646\u0647 \u062E\u0648\u062F \u0631\u0627',
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'a',
                      {
                        className: 'submit-house-button white',
                        onClick: () => {
                          __WEBPACK_IMPORTED_MODULE_5__history__["a" /* default */].push('/houses/new');
                        },
                        __source: {
                          fileName: _jsxFileName,
                          lineNumber: 125
                        },
                        __self: this
                      },
                      '\u062B\u0628\u062A'
                    ),
                    '\u06A9\u0646\u06CC\u062F.'
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}

HousesPage.propTypes = {
  houses: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.array.isRequired,
  handleSubmit: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__main_css___default.a)(HousesPage));

/***/ }),

/***/ "./src/components/HousesPage/casey-horner-533586-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HousesPage/casey-horner-533586-unsplash.jpg?d5113c82";

/***/ }),

/***/ "./src/components/HousesPage/geometry2.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAMAAAC3Ycb+AAAAM1BMVEX5+vvr7O3y8/Tu7/D4+fns7e73+Pjt7/D29vf3+fnt7u/09fXx8vPv8PH19vfy8vPz9PUQ6VWYAAAH4klEQVR4Xu3d2XYTvxLF4doahx7f/2mPnUA6eJn/SaAdKeb33QC+yYKNqiR1W7J/DwAAAAAAAAAAAAAg2FDgfbFxoEi6RoJ18xfLXq2rOusbRVJWe5Cp6SefiOSjiuL8iEyC10X23kddOPsgIonSAzJJTYqu2lWZJc32MUQySzo9k5AlH+ynEj+cCJFM0rycnclyE0CK0scCJ5IgbRamUzMpUrZfJKmZDRLJbEPLamZ2ZLKfMkCS/cpJkw0SSbWhOSkdMyP5cMagW+5+9hHspCRpt6uwnDQ/LfdGw6JoH4IobxcpS3E9aczV+x9+BGYpmE1RyvWsInh32BT7CEzSapukOdgAgSBIS5biZIeeJQtZFy3ZoWtTxy5pCXboO+1Fkpwdui8MkYsdBt86Qf/NRfTffse4D6jQ/xEuHv+SA3gN6PEAAAAAAMC6LKsNA6suuiVS3iR7gUUX3vooOiTrjkCyDt7eULIm62KX5vJq+Vk3sXrfKY8QFYO9qlKzvrBJu/3kJGf94HZQhKZYrSN4qdx8N9P6QZGWOwH1gibV24S8dUbFGqZmoUrZbNCuzqy3/7wXx7rwqkY16wq7tNlPg+ydMNEaaIqFImX3KkvJesOiw2zdoerNj/4OAAAAAAAAAAAAAAAAAJvkg40Cky4WGwWcFCUbBXZdZBsFQpa02jAQpr1aRwAAAAAAAAAAAKhtsoHAS5t9Aoq7VexEe/xcInC65exMKQ712J9ALH32eDACsfck94CLU2wcBGJeqvYQayWQP7BKuz1AmBVXAvkDTd7Ol5ou5kAgnzZLj3mPtknKiUA+a5KSnSt4SVtIWZ+vhwRSpGKnKlGv/SPMkpZgXRHIJslXe7FGKRbrh0BqluRu/9gLPWSKUis3o1y5WgfMso6mcaeldMA6pElxt1thUZ9dZVbqTTH9ZqTv7PZ+TJbCmROs+sHP7yOQmjXbadJvRkJT5onhR7lg57n/L596tRDcr02blKwH3J8iNDXrA1F5pIqF+U7N2vtVLKzSZDdyx4qFqMV+VXtWLMxSGLliUbP6Vyxq1kgVC7Pk3ut8+TZW3YrWEYJuzfYdAAAAwF0VGwX+6/UCEAgkf1ogYIQQCAAAAAAAAAAAAAAAAAAAAAAAAAAAQCrFxoC0L00XNgBU1/SDdYc66yp6tw9QsuCipLYlGwFSluRX+77W1Z7IGqU42Vd4VDnMyvY0pi+7/7U2KQc7X5W2p8pjsi8xP+oIll1K5PF5XhfezucV7UmkeOTxbUdIkGZ7DqEdeXzfHjJJqz2H7Yv/b5VkD7BI9hyS1MJz3ADyHLxUnuSOnKeQJP8st0g9hfkJpu+n3SQc+m/lhecYIEnaT6rfnTOZnqD2nnj3+S7phEyovVntrJEW+2bSOlSskTcWm3Lds65y6NNCnH1/u5TOrBhOUq7WQXmODQeveOZ6ZpM0h17rqWIdDLuxGKQlq99Mx0nWwcAbi14XLRmBDLKxuB/PTnuopdh7bCzWwaY5bCzmYk+FxS1czDYUVPs0AAAAAAAAAAAAAAAAAMktXsp+W60/TE1vogvWH4fPKfrZ+dff7dYRJklxS/YiTE3SHKwXTLcB7FHKJDLQYWc1S7N1gRSlZDdCllbrAfnuyQ81qlkHmN4VpylLWl7jcX1O6EBTDMfc94UPZha6nB+GSXLHQY1xc3OUvF0sivblsLwNkPnH3Lf+aCpOqvbV8NZBquTDSyJFWl5/KfbFUN5at5PK/rocbGqdAsFRl7yaeekIxEnB0O0gDi//4wieVVo6NXXMvwSySX6dmjR1mvbCKx/RhBr1wvdfGFKyJslZarpYglnqvHVCINYUk1lxU+23uYjpmNuuUvyZQWpsv/c/rniT5KdkdZ0lZUPvA72nqDebdYH5/Y5VnaNeLMX6QLppFmV3rgTDmJeNgOt4sEmLjQPh/7zy45J9KYT4XwcNz4odEuHNrJzsnuo1dCB7sIdbvV87VC25cP9C9jZwHpPi/vifcfHlicySoqv2XtibjmI2JC+plYf/jB7TntJ0sezJXqVp0UWcbGhTlOTrEwZyfGWnee/zt/nmTnCSylOVrMM6Rx20TPYNFMk/uqkvq/WSduev3F7se2hStWFgH+pJAUJUDDYMbFIrNgy4KMmPEwnqrIu52lCIpNgwMP1+IQ0mWpil3YaBImUbB8KsZCNBtREBAAAAAAAAAAAAAAAAAIBarmwUcLqyu0AgcJL/ZCBghBAImGUBAAAAAAAAAAAAAAAAkrL3fnbOTaVU6wz6hbM/EuwskPzV3wXSTksE70JIf1qykpydiED+1i5VGweBzJK3cRBIlrTaKAgk6GKcvk4gRfI6q6/7q4lA/oaT6mm3qOrKEcjfWBTPu2dYav9aIKGkcwOJWsyWP+3rBFL0k7/4+0DqS6Y1qgVK1t8FolMK9iqV107iaOp/GMjsrjZ/yt9+k8Jf3I6OSUp2Iq9sF3/a1+EkO5M028XR17sikCR5txY7+npXBLLrVfRzHmB+RCBl9jp07OsEcghlcovXlbe+COQ2l2qDSvvmXyxurYMH8vzWJeq9to2TyaJm/5bgol5kf9X0whcbg//X6vxrHMuejtK6NV34ZCPY8j8VSMqS2h5uP551sdkXwyQpTnZHnSXlal8Js6Qt2H2lSTHZF8KsuNpvhfmrE8GW7L9sUrOBYPrRRQAAAAAAAAAAAP4Hwd4wVc8Xf9oAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/components/HousesPage/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HousesPage/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/HousesPage/luke-van-zyl-504032-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HousesPage/luke-van-zyl-504032-unsplash.jpg?28cbcf9b";

/***/ }),

/***/ "./src/components/HousesPage/mahdiar-mahmoodi-452489-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HousesPage/mahdiar-mahmoodi-452489-unsplash.jpg?587276d2";

/***/ }),

/***/ "./src/components/HousesPage/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousesPage/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousesPage/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/HousesPage/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/HousesPage/michal-kubalczyk-260909-unsplash.jpg":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/HousesPage/michal-kubalczyk-260909-unsplash.jpg?3a246a18";

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css__ = __webpack_require__("./node_modules/normalize.css/normalize.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_normalize_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Layout_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Footer__ = __webpack_require__("./src/components/Footer/Footer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Panel__ = __webpack_require__("./src/components/Panel/Panel.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.js';
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */









class Layout extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-md-12 col-xs-12', style: { padding: '0' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__Panel__["a" /* default */], { mainPanel: this.props.mainPanel, __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }),
      this.props.children,
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__Footer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      })
    );
  }
}

Layout.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool
};
Layout.defaultProps = {
  mainPanel: false
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_normalize_css___default.a, __WEBPACK_IMPORTED_MODULE_4__Layout_css___default.a)(Layout));

/***/ }),

/***/ "./src/components/Panel/Panel.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Panel/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png__ = __webpack_require__("./src/components/Panel/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__history__ = __webpack_require__("./src/history.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/Panel.js';
/* eslint-disable react/no-danger */










class Panel extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {

  render() {
    console.log(this.props.currUser);
    console.log(this.props.isLoggedIn);
    if (this.props.mainPanel) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'header',
        { className: 'hidden-xs', __source: {
            fileName: _jsxFileName,
            lineNumber: 25
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container-fluid', __source: {
              fileName: _jsxFileName,
              lineNumber: 26
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row header-main', __source: {
                fileName: _jsxFileName,
                lineNumber: 27
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 28
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'container', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row flex-center header-main-boarder', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9 ', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 31
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h5',
                      { className: 'text-align-right white-font persian ', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 32
                        },
                        __self: this
                      },
                      '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                    ),
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'dropdown-content', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 35
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'container-fluid', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 36
                          },
                          __self: this
                        },
                        this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          {
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 38
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 39
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'h4',
                              { className: 'persian text-align-right black-font ', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 40
                                },
                                __self: this
                              },
                              this.props.currUser.username
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 44
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-left persian light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 45
                                },
                                __self: this
                              },
                              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                                'span',
                                { className: 'light-grey-font', __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 46
                                  },
                                  __self: this
                                },
                                '\u062A\u0648\u0645\u0627\u0646',
                                this.props.currUser.balance
                              )
                            ),
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 light-grey-font pull-right  persian text-align-right no-padding', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 51
                                },
                                __self: this
                              },
                              '\u0627\u0639\u062A\u0628\u0627\u0631'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 55
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                              onClick: () => {
                                __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 56
                              },
                              __self: this
                            })
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 65
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u062E\u0631\u0648\u062C',
                              onClick: () => {
                                this.props.authActions.logoutUser();
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 66
                              },
                              __self: this
                            })
                          )
                        ),
                        !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 78
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0648\u0631\u0648\u062F',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 79
                            },
                            __self: this
                          })
                        )
                      )
                    )
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding  col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 93
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x white-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 94
                      },
                      __self: this
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'header',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid', __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row nav-cl', __source: {
              fileName: _jsxFileName,
              lineNumber: 107
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown', __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 109
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row flex-center ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 110
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'col-xs-9 ', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 111
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h5',
                    { className: 'text-align-right persian purple-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 112
                      },
                      __self: this
                    },
                    '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'dropdown-content', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 115
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'container-fluid', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 116
                        },
                        __self: this
                      },
                      this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        {
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 118
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 119
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'h4',
                            { className: 'black-font persian text-align-right blue-font', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 120
                              },
                              __self: this
                            },
                            this.props.currUser.username
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 124
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: ' light-grey-font col-xs-6 pull-left blue-font persian', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 125
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'span',
                              { className: 'light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 126
                                },
                                __self: this
                              },
                              '\u062A\u0648\u0645\u0627\u0646',
                              this.props.currUser.balance
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'light-grey-font col-xs-6 pull-right blue-font persian text-align-right no-padding', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 131
                              },
                              __self: this
                            },
                            '\u0627\u0639\u062A\u0628\u0627\u0631'
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 135
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                            className: 'white-font  blue-button persian text-align-center',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                            },
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 136
                            },
                            __self: this
                          })
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 145
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u062E\u0631\u0648\u062C',
                            onClick: () => {
                              this.props.authActions.logoutUser();
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 146
                            },
                            __self: this
                          })
                        )
                      ),
                      !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'row mar-top', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 158
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                          type: 'button',
                          value: '\u0648\u0631\u0648\u062F',
                          onClick: () => {
                            __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                          },
                          className: '  white-font  blue-button persian text-align-center',
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 159
                          },
                          __self: this
                        })
                      )
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'no-padding  col-xs-3', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 173
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x purple-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 174
                    },
                    __self: this
                  })
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-4 col-md-offset-6 nav-logo col-xs-12', __source: {
                fileName: _jsxFileName,
                lineNumber: 179
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 180
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row  ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 181
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'a',
                  {
                    className: 'flex-row-rev',
                    onClick: () => {
                      __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/');
                    },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 182
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding text-align-center col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 188
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { className: 'icon-cl', src: __WEBPACK_IMPORTED_MODULE_7__logo_png___default.a, alt: 'logo', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 189
                      },
                      __self: this
                    })
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 191
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian-bold text-align-right  persian blue-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 192
                        },
                        __self: this
                      },
                      '\u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                    )
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}
Panel.propTypes = {
  currUser: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired,
  isLoggedIn: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    authActions: Object(__WEBPACK_IMPORTED_MODULE_3_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser, isLoggedIn } = authentication;
  return {
    currUser,
    isLoggedIn
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Panel)));

/***/ }),

/***/ "./src/components/Panel/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Panel/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/SearchBox/SearchBox.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css__ = __webpack_require__("./src/components/SearchBox/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/SearchBox/SearchBox.js';
/* eslint-disable react/no-danger */





class SearchBox extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  constructor(props) {
    super(props);

    this.handleSubmit = e => {
      e.preventDefault();
      this.props.handleSubmit(this.state.minArea, this.state.dealType, this.state.buildingType, this.state.maxPrice);
    };

    this.validate = (maxPrice, minArea) => {
      // true means invalid, so our conditions got reversed
      const regex = new RegExp(/[^0-9]/, 'g');
      return {
        maxPrice: maxPrice.match(regex),
        minArea: minArea.match(regex)
      };
    };

    this.state = {
      maxPrice: '',
      minArea: '',
      buildingType: '',
      dealType: ''
    };
  }

  render() {
    const errors = this.validate(this.state.maxPrice, this.state.minArea);
    const isEnabled = !Object.keys(errors).some(x => errors[x]);

    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-xs-12 search-section border-radius no-padding', __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'col-xs-12 first-part no-padding', __source: {
            fileName: _jsxFileName,
            lineNumber: 43
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4', __source: {
              fileName: _jsxFileName,
              lineNumber: 44
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { className: 'label', __source: {
                fileName: _jsxFileName,
                lineNumber: 45
              },
              __self: this
            },
            '\u0645\u062A\u0631 \u0645\u0631\u0628\u0639'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
            placeholder: '\u062D\u062F\u0627\u0642\u0644 \u0645\u062A\u0631\u0627\u0698',
            onChange: e => {
              this.setState({ [e.target.name]: e.target.value });
            },
            name: 'minArea',
            className: `search-input border-radius clear-input ${errors.minArea ? 'error' : ''}`,
            type: 'text',
            dir: 'rtl',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 46
            },
            __self: this
          }),
          errors.minArea && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            { className: 'err-msg', __source: {
                fileName: _jsxFileName,
                lineNumber: 58
              },
              __self: this
            },
            '\u0644\u0637\u0641\u0627 \u0639\u062F\u062F \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4', __source: {
              fileName: _jsxFileName,
              lineNumber: 60
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { className: 'label', __source: {
                fileName: _jsxFileName,
                lineNumber: 61
              },
              __self: this
            },
            '\u062A\u0648\u0645\u0627\u0646'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
            placeholder: '\u062D\u062F\u0627\u06A9\u062B\u0631 \u0642\u06CC\u0645\u062A',
            onChange: e => {
              this.setState({ [e.target.name]: e.target.value });
            },
            name: 'maxPrice',
            className: `search-input border-radius clear-input ${errors.maxPrice ? 'error' : ''}`,
            type: 'text',
            dir: 'rtl',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 62
            },
            __self: this
          }),
          errors.maxPrice && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            { className: 'err-msg', __source: {
                fileName: _jsxFileName,
                lineNumber: 74
              },
              __self: this
            },
            '\u0644\u0637\u0641\u0627 \u0639\u062F\u062F \u0648\u0627\u0631\u062F \u06A9\u0646\u06CC\u062F.'
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4', __source: {
              fileName: _jsxFileName,
              lineNumber: 76
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('label', { className: 'building-type-label', __source: {
              fileName: _jsxFileName,
              lineNumber: 77
            },
            __self: this
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'select',
            {
              title: 'buildingType',
              onChange: e => {
                this.setState({ [e.target.name]: e.target.value });
              },
              name: 'buildingType',
              className: 'search-input border-radius clear-input',
              __source: {
                fileName: _jsxFileName,
                lineNumber: 78
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'option',
              { disabled: true, selected: true, className: 'select-place-holder', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 86
                },
                __self: this
              },
              '\u0646\u0648\u0639 \u0645\u0644\u06A9'
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'option',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 89
                },
                __self: this
              },
              '\u0622\u067E\u0627\u0631\u062A\u0645\u0627\u0646 '
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'option',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 90
                },
                __self: this
              },
              '\u0648\u06CC\u0644\u0627\u06CC\u06CC'
            )
          )
        )
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'col-xs-12 second-part', __source: {
            fileName: _jsxFileName,
            lineNumber: 94
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'visible-md  visible-lg col-md-6 no-padding', __source: {
              fileName: _jsxFileName,
              lineNumber: 95
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'form',
            { onSubmit: this.handleSubmit, __source: {
                fileName: _jsxFileName,
                lineNumber: 96
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'button',
              {
                disabled: !isEnabled,
                type: 'submit',
                className: 'search-button has-transition border-radius clear-input white light-blue-background',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 97
                },
                __self: this
              },
              '\u062C\u0633\u062A\u062C\u0648'
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-md-6 deal-type-section no-padding', __source: {
              fileName: _jsxFileName,
              lineNumber: 106
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { htmlFor: 'rahn', className: 'label font-medium input-label', __source: {
                fileName: _jsxFileName,
                lineNumber: 107
              },
              __self: this
            },
            '\u0631\u0647\u0646 \u0648 \u0627\u062C\u0627\u0631\u0647'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', { id: 'kharid',
            value: 1,
            onChange: e => {
              this.setState({
                [e.target.name]: e.target.checked ? 0 : 1
              });
            },
            className: 'white font-light',
            type: 'radio',
            name: 'dealType',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 108
            },
            __self: this
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'label',
            { htmlFor: 'kharid', className: 'label font-medium input-label', __source: {
                fileName: _jsxFileName,
                lineNumber: 119
              },
              __self: this
            },
            '\u062E\u0631\u06CC\u062F'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', { id: 'rahn',
            className: 'white font-light',
            type: 'radio',
            value: 0,
            onChange: e => {
              this.setState({
                [e.target.name]: e.target.checked ? 1 : 0
              });
            },
            name: 'dealType',
            __source: {
              fileName: _jsxFileName,
              lineNumber: 120
            },
            __self: this
          })
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'visible-xs visible-sm hidden-md hidden-lg col-xs-12', __source: {
              fileName: _jsxFileName,
              lineNumber: 132
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'form',
            { onSubmit: this.handleSubmit, __source: {
                fileName: _jsxFileName,
                lineNumber: 133
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'button',
              {
                type: 'submit',
                className: 'search-button has-transition border-radius clear-input white light-blue-background',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 134
                },
                __self: this
              },
              '\u062C\u0633\u062A\u062C\u0648'
            )
          )
        )
      )
    );
  }
}

SearchBox.propTypes = {
  handleSubmit: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__main_css___default.a)(SearchBox));

/***/ }),

/***/ "./src/components/SearchBox/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/SearchBox/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/config/compressionConfig.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// noinspection JSAnnotator
const levels = [{
  MAX_WIDTH: 600,
  MAX_HEIGHT: 600,
  DOCUMENT_SIZE: 300000
}, {
  MAX_WIDTH: 800,
  MAX_HEIGHT: 800,
  DOCUMENT_SIZE: 500000
}];

/* harmony default export */ __webpack_exports__["a"] = ({
  getLevel(level) {
    return levels[level - 1];
  }
});

/***/ }),

/***/ "./src/config/paths.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__urls__ = __webpack_require__("./src/config/urls.js");
/* eslint-disable prettier/prettier */


const loginApiUrl = `http://localhost:8080/Turtle/login`;
/* harmony export (immutable) */ __webpack_exports__["h"] = loginApiUrl;

const logoutApiUrl = `http://localhost:8080/Turtle/login`;
/* unused harmony export logoutApiUrl */

const getSearchHousesUrl = `http://localhost:8080/Turtle/SearchServlet`;
/* harmony export (immutable) */ __webpack_exports__["f"] = getSearchHousesUrl;

const getCreateHouseUrl = `http://localhost:8080/Turtle/api/HouseServlet`;
/* harmony export (immutable) */ __webpack_exports__["a"] = getCreateHouseUrl;

const getInitiateUrl = `http://localhost:8080/Turtle/home`;
/* harmony export (immutable) */ __webpack_exports__["e"] = getInitiateUrl;

const getCurrentUserUrl = userId => `http://localhost:8080/Turtle/api/IndividualServlet?userId=${userId}`;
/* harmony export (immutable) */ __webpack_exports__["b"] = getCurrentUserUrl;

const getHouseDetailUrl = houseId => `http://localhost:8080/Turtle/api/GetSingleHouse?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["c"] = getHouseDetailUrl;

const getHousePhoneUrl = houseId => `http://localhost:8080/Turtle/api/PaymentServlet?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["d"] = getHousePhoneUrl;

const increaseCreditUrl = amount => `http://localhost:8080/Turtle/api/CreditServlet?value=${amount}`;
/* harmony export (immutable) */ __webpack_exports__["g"] = increaseCreditUrl;


const searchDriverUrl = phoneNumber => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/phoneNumber/${phoneNumber}`;
/* unused harmony export searchDriverUrl */

const getDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export getDriverAcquisitionInfoUrl */

const updateDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export updateDriverAcquisitionInfoUrl */

const updateDriverDocsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export updateDriverDocsUrl */

const approveDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/approve/${driverId}`;
/* unused harmony export approveDriverUrl */

const rejectDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/reject/${driverId}`;
/* unused harmony export rejectDriverUrl */

const getDriverDocumentUrl = documentId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/document/${documentId}`;
/* unused harmony export getDriverDocumentUrl */

const getDriverDocumentIdsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export getDriverDocumentIdsUrl */


/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__ = __webpack_require__("history/createBrowserHistory");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__);
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */



// NavigationBar manager, e.g. history.push('/home')
// https://github.com/mjackson/history
/* harmony default export */ __webpack_exports__["a"] = (false && __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default()());

/***/ }),

/***/ "./src/routes/houses/HousesContainer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_search__ = __webpack_require__("./src/actions/search.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_HousesPage__ = __webpack_require__("./src/components/HousesPage/HousesPage.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__utils__ = __webpack_require__("./src/utils/index.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/houses/HousesContainer.js';









class HousesContainer extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.handleSubmit = (minArea, dealType, buildingType, maxPrice) => {
      this.props.searchActions.searchHouses(minArea, dealType, buildingType, maxPrice);
    }, _temp;
  }

  componentWillMount() {
    this.props.searchActions.searchHouses(this.props.minArea, this.props.dealType, this.props.buildingType, this.props.maxPrice);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.houses !== this.props.houses) {
      clearTimeout(this.timeout);

      // Optionally do something with data

      if (!nextProps.isFetching) {
        this.startPoll();
      }
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  startPoll() {
    console.log(this.props.minArea);
    this.timeout = setTimeout(() => {
      this.props.searchActions.searchHouses(this.props.minArea, this.props.dealType, this.props.buildingType, this.props.maxPrice);
    }, 25000);
  }
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_HousesPage__["a" /* default */], {
      loading: this.props.isFetching,
      houses: this.props.houses,
      handleSubmit: this.handleSubmit,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    });
  }
}

HousesContainer.propTypes = {
  searchActions: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.object.isRequired,
  houses: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.array.isRequired,
  minArea: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.number.isRequired,
  dealType: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.number.isRequired,
  buildingType: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.string.isRequired,
  maxPrice: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.number.isRequired,
  isFetching: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    searchActions: Object(__WEBPACK_IMPORTED_MODULE_3_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_search__, dispatch)
  };
}

function mapStateToProps(state) {
  const { search } = state;
  const {
    isFetching,
    houses,
    minArea,
    dealType,
    buildingType,
    maxPrice
  } = search;
  return {
    isFetching,
    houses,
    minArea,
    dealType,
    buildingType,
    maxPrice
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(HousesContainer));

/***/ }),

/***/ "./src/routes/houses/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__HousesContainer__ = __webpack_require__("./src/routes/houses/HousesContainer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Layout__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/houses/index.js';




const title = 'خانه ها';

function action() {
  return {
    chunks: ['houses'],
    title,
    component: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_2__components_Layout__["a" /* default */],
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__HousesContainer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      }),
      ','
    )
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/utils/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export compressFile */
/* harmony export (immutable) */ __webpack_exports__["c"] = getLoginInfo;
/* harmony export (immutable) */ __webpack_exports__["d"] = getPostConfig;
/* unused harmony export getPutConfig */
/* harmony export (immutable) */ __webpack_exports__["b"] = getGetConfig;
/* unused harmony export convertNumbersToPersian */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchApi; });
/* unused harmony export setImmediate */
/* unused harmony export setCookie */
/* unused harmony export getCookie */
/* unused harmony export eraseCookie */
/* harmony export (immutable) */ __webpack_exports__["e"] = notifyError;
/* harmony export (immutable) */ __webpack_exports__["f"] = notifySuccess;
/* unused harmony export reloadSelectPicker */
/* unused harmony export loadSelectPicker */
/* unused harmony export getFormData */
/* unused harmony export loadUserLoginStatus */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js__ = __webpack_require__("exif-js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_exif_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__ = __webpack_require__("./src/config/compressionConfig.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

let processFile = (() => {
  var _ref = _asyncToGenerator(function* (dataURL, fileSize, fileType, documentType) {
    const image = new Image();
    image.src = dataURL;
    return new Promise(function (resolve, reject) {
      image.onload = function () {
        const imageMeasurements = getImageMeasurements(image);
        if (!shouldResize(imageMeasurements, fileSize, documentType)) {
          resolve(dataURLtoBlob(dataURL, fileType));
        }
        const scaledImageMeasurements = getScaledImageMeasurements(imageMeasurements.width, imageMeasurements.height, documentType);
        const scaledImageFile = getScaledImageFileFromCanvas(image, scaledImageMeasurements.width, scaledImageMeasurements.height, imageMeasurements.transform, fileType);
        resolve(scaledImageFile);
      };
      image.onerror = reject;
    });
  });

  return function processFile(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }







const compressionFirstLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(1);
const compressionSecondLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(2);
const documentsCompressionLevel = {
  profilePicture: compressionFirstLevel,
  driverLicence: compressionFirstLevel,
  carIdCard: compressionFirstLevel,
  insurance: compressionSecondLevel
};

function getCompressionLevel(documentType) {
  return documentsCompressionLevel[documentType] || compressionFirstLevel;
}

function dataURLtoBlob(dataURL, fileType) {
  const binary = atob(dataURL.split(',')[1]);
  const array = [];
  const length = binary.length;
  let i;
  for (i = 0; i < length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: fileType });
}

function getScaledImageMeasurements(width, height, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH } = getCompressionLevel(documentType);

  if (width > height) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  } else {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
  return {
    height,
    width
  };
}

function getImageOrientation(image) {
  let orientation = 0;
  __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getData(image, function () {
    orientation = __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getTag(this, 'Orientation');
  });
  return orientation;
}

function getImageMeasurements(image) {
  const orientation = getImageOrientation(image);
  let transform;
  let swap = false;
  switch (orientation) {
    case 8:
      swap = true;
      transform = 'left';
      break;
    case 6:
      swap = true;
      transform = 'right';
      break;
    case 3:
      transform = 'flip';
      break;
    default:
  }
  const width = swap ? image.height : image.width;
  const height = swap ? image.width : image.height;
  return {
    width,
    height,
    transform
  };
}

function getScaledImageFileFromCanvas(image, width, height, transform, fileType) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const context = canvas.getContext('2d');
  let transformParams;
  let swapMeasure = false;
  let imageWidth = width;
  let imageHeight = height;
  switch (transform) {
    case 'left':
      transformParams = [0, -1, 1, 0, 0, height];
      swapMeasure = true;
      break;
    case 'right':
      transformParams = [0, 1, -1, 0, width, 0];
      swapMeasure = true;
      break;
    case 'flip':
      transformParams = [1, 0, 0, -1, 0, height];
      break;
    default:
      transformParams = [1, 0, 0, 1, 0, 0];
  }
  context.setTransform(...transformParams);
  if (swapMeasure) {
    imageHeight = width;
    imageWidth = height;
  }
  context.drawImage(image, 0, 0, imageWidth, imageHeight);
  context.setTransform(1, 0, 0, 1, 0, 0);
  const dataURL = canvas.toDataURL(fileType);
  return dataURLtoBlob(dataURL, fileType);
}

function shouldResize(imageMeasurements, fileSize, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH, DOCUMENT_SIZE } = getCompressionLevel(documentType);
  if (documentType === 'insurance' && fileSize < DOCUMENT_SIZE) {
    return false;
  } else if (fileSize < DOCUMENT_SIZE) {
    return false;
  }
  return imageMeasurements.width > MAX_WIDTH || imageMeasurements.height > MAX_HEIGHT;
}

function compressFile(file, documentType = 'default') {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = _asyncToGenerator(function* () {
        const compressedFile = yield processFile(reader.result, file.size, file.type, documentType);
        resolve(compressedFile);
      });
    } catch (err) {
      reject(err);
    }
  });
}

function getLoginInfo() {
  try {
    const loginInfo = localStorage.getItem('loginInfo');
    if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isString(loginInfo)) {
      return JSON.parse(loginInfo);
    }
  } catch (err) {
    console.log('Error when parsing loginInfo object: ', err);
  }
  return null;
}
function getPostConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    mimeType: 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json', mimeType: 'application/json' };
  return _extends({
    method: 'POST',
    // mode: 'no-cors',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getPutConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    'X-Authorization': loginInfo.token
  } : { 'Content-Type': 'application/json' };
  return _extends({
    method: 'PUT',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getGetConfig() {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json' };
  return {
    method: 'GET',
    headers
  };
}

function convertNumbersToPersian(str) {
  const persianNumbers = '۰۱۲۳۴۵۶۷۸۹';
  let result = '';
  if (!str && str !== 0) {
    return result;
  }
  str = str.toString();
  let convertedChar;
  for (let i = 0; i < str.length; i++) {
    try {
      convertedChar = persianNumbers[str[i]];
    } catch (err) {
      console.log(err);
      convertedChar = str[i];
    }
    result += convertedChar;
  }
  return result;
}

let fetchApi = (() => {
  var _ref3 = _asyncToGenerator(function* (url, config) {
    let flowError;
    try {
      const res = yield fetch(url, config);
      console.log(res);
      console.log(res.status);
      let result = yield res.json();
      console.log(result);
      if (!__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.get(result, 'data'))) {
        result = result.data;
      }
      console.log(res.status);

      if (res.status !== 200) {
        flowError = result;
        console.log(res.status);

        if (res.status === 403) {
          localStorage.clear();
          flowError.message = 'لطفا دوباره وارد شوید!';
          console.log('yeees');
          setTimeout(function () {
            return __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push('/login');
          }, 3000);
        }
      } else {
        return result;
      }
    } catch (err) {
      console.log(err);
      console.debug(`Error when fetching api: ${url}`, err);
      flowError = {
        code: 'UNKNOWN_ERROR',
        message: 'خطای نامشخص لطفا دوباره سعی کنید'
      };
    }
    if (flowError) {
      throw flowError;
    }
  });

  return function fetchApi(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
})();

let setImmediate = (() => {
  var _ref4 = _asyncToGenerator(function* (fn) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        let value = null;
        if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isFunction(fn)) {
          value = fn();
        }
        resolve(value);
      }, 0);
    });
  });

  return function setImmediate(_x7) {
    return _ref4.apply(this, arguments);
  };
})();

function setCookie(name, value, days = 7) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
function getCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  const length = ca.length;
  let i = 0;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

function notifyError(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '10000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.error(message, 'خطا');
}

function notifySuccess(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.success(message, '');
}

function reloadSelectPicker() {
  try {
    $('.selectpicker').selectpicker('render');
  } catch (err) {
    console.log(err);
  }
}

function loadSelectPicker() {
  try {
    $('.selectpicker').selectpicker({});
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      $('.selectpicker').selectpicker('mobile');
    }
  } catch (err) {
    console.log(err);
  }
}

function getFormData(data) {
  if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(data)) {
    throw Error('Invalid input data to create a FormData object');
  }
  let formData;
  try {
    formData = new FormData();
    const keys = __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.keys(data);
    let i;
    const length = keys.length;
    for (i = 0; i < length; i++) {
      formData.append(keys[i], data[keys[i]]);
    }
    return formData;
  } catch (err) {
    console.debug(err);
    throw Error('FORM_DATA_BROWSER_SUPPORT_FAILURE');
  }
}

function loadUserLoginStatus(store) {
  const loginStatusAction = Object(__WEBPACK_IMPORTED_MODULE_4__actions_authentication__["loadLoginStatus"])();
  store.dispatch(loginStatusAction);
}

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL2hvdXNlcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvbWFpbi5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L21haW4uY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvdXJsL2VzY2FwZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzPzgyMWIiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYWN0aW9ucy9zZWFyY2guanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hcGktaGFuZGxlcnMvYXV0aGVudGljYXRpb24uanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hcGktaGFuZGxlcnMvc2VhcmNoLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzcz85YjRlIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL0hvdXNlc1BhZ2UuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvY2FzZXktaG9ybmVyLTUzMzU4Ni11bnNwbGFzaC5qcGciLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvZ2VvbWV0cnkyLnBuZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9sb2dvLnBuZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9sdWtlLXZhbi16eWwtNTA0MDMyLXVuc3BsYXNoLmpwZyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9tYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGciLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9tYWluLmNzcz8zM2IzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21pY2hhbC1rdWJhbGN6eWstMjYwOTA5LXVuc3BsYXNoLmpwZyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzcz81MDc5IiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9QYW5lbC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3M/MWNmNCIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L1NlYXJjaEJveC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9TZWFyY2hCb3gvbWFpbi5jc3M/NDYwYSIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbmZpZy9jb21wcmVzc2lvbkNvbmZpZy5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbmZpZy9wYXRocy5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2hpc3RvcnkuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9yb3V0ZXMvaG91c2VzL0hvdXNlc0NvbnRhaW5lci5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL3JvdXRlcy9ob3VzZXMvaW5kZXguanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy91dGlscy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIvKiEgbm9ybWFsaXplLmNzcyB2Ny4wLjAgfCBNSVQgTGljZW5zZSB8IGdpdGh1Yi5jb20vbmVjb2xhcy9ub3JtYWxpemUuY3NzICovXFxuLyogRG9jdW1lbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGxpbmUgaGVpZ2h0IGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBQcmV2ZW50IGFkanVzdG1lbnRzIG9mIGZvbnQgc2l6ZSBhZnRlciBvcmllbnRhdGlvbiBjaGFuZ2VzIGluXFxuICogICAgSUUgb24gV2luZG93cyBQaG9uZSBhbmQgaW4gaU9TLlxcbiAqL1xcbmh0bWwge1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICAtbXMtdGV4dC1zaXplLWFkanVzdDogMTAwJTtcXG4gIC8qIDIgKi9cXG4gIC13ZWJraXQtdGV4dC1zaXplLWFkanVzdDogMTAwJTtcXG4gIC8qIDIgKi8gfVxcbi8qIFNlY3Rpb25zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBSZW1vdmUgdGhlIG1hcmdpbiBpbiBhbGwgYnJvd3NlcnMgKG9waW5pb25hdGVkKS5cXG4gKi9cXG5ib2R5IHtcXG4gIG1hcmdpbjogMDsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmFydGljbGUsXFxuYXNpZGUsXFxuZm9vdGVyLFxcbmhlYWRlcixcXG5uYXYsXFxuc2VjdGlvbiB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIGZvbnQgc2l6ZSBhbmQgbWFyZ2luIG9uIGBoMWAgZWxlbWVudHMgd2l0aGluIGBzZWN0aW9uYCBhbmRcXG4gKiBgYXJ0aWNsZWAgY29udGV4dHMgaW4gQ2hyb21lLCBGaXJlZm94LCBhbmQgU2FmYXJpLlxcbiAqL1xcbmgxIHtcXG4gIGZvbnQtc2l6ZTogMmVtO1xcbiAgbWFyZ2luOiAwLjY3ZW0gMDsgfVxcbi8qIEdyb3VwaW5nIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFLlxcbiAqL1xcbmZpZ2NhcHRpb24sXFxuZmlndXJlLFxcbm1haW4ge1xcbiAgLyogMSAqL1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgbWFyZ2luIGluIElFIDguXFxuICovXFxuZmlndXJlIHtcXG4gIG1hcmdpbjogMWVtIDQwcHg7IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBGaXJlZm94LlxcbiAqIDIuIFNob3cgdGhlIG92ZXJmbG93IGluIEVkZ2UgYW5kIElFLlxcbiAqL1xcbmhyIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xcbiAgLyogMSAqL1xcbiAgaGVpZ2h0OiAwO1xcbiAgLyogMSAqL1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmhlcml0YW5jZSBhbmQgc2NhbGluZyBvZiBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIENvcnJlY3QgdGhlIG9kZCBgZW1gIGZvbnQgc2l6aW5nIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5wcmUge1xcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZSwgbW9ub3NwYWNlO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiAyICovIH1cXG4vKiBUZXh0LWxldmVsIHNlbWFudGljc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBncmF5IGJhY2tncm91bmQgb24gYWN0aXZlIGxpbmtzIGluIElFIDEwLlxcbiAqIDIuIFJlbW92ZSBnYXBzIGluIGxpbmtzIHVuZGVybGluZSBpbiBpT1MgOCsgYW5kIFNhZmFyaSA4Ky5cXG4gKi9cXG5hIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgLyogMSAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb24tc2tpcDogb2JqZWN0cztcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIFJlbW92ZSB0aGUgYm90dG9tIGJvcmRlciBpbiBDaHJvbWUgNTctIGFuZCBGaXJlZm94IDM5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdGV4dCBkZWNvcmF0aW9uIGluIENocm9tZSwgRWRnZSwgSUUsIE9wZXJhLCBhbmQgU2FmYXJpLlxcbiAqL1xcbmFiYnJbdGl0bGVdIHtcXG4gIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAvKiAxICovXFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIC8qIDIgKi9cXG4gIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgZG90dGVkO1xcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBQcmV2ZW50IHRoZSBkdXBsaWNhdGUgYXBwbGljYXRpb24gb2YgYGJvbGRlcmAgYnkgdGhlIG5leHQgcnVsZSBpbiBTYWZhcmkgNi5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogaW5oZXJpdDsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHdlaWdodCBpbiBDaHJvbWUsIEVkZ2UsIGFuZCBTYWZhcmkuXFxuICovXFxuYixcXG5zdHJvbmcge1xcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmNvZGUsXFxua2JkLFxcbnNhbXAge1xcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZSwgbW9ub3NwYWNlO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCBzdHlsZSBpbiBBbmRyb2lkIDQuMy0uXFxuICovXFxuZGZuIHtcXG4gIGZvbnQtc3R5bGU6IGl0YWxpYzsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBiYWNrZ3JvdW5kIGFuZCBjb2xvciBpbiBJRSA5LS5cXG4gKi9cXG5tYXJrIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjA7XFxuICBjb2xvcjogIzAwMDsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnNtYWxsIHtcXG4gIGZvbnQtc2l6ZTogODAlOyB9XFxuLyoqXFxuICogUHJldmVudCBgc3ViYCBhbmQgYHN1cGAgZWxlbWVudHMgZnJvbSBhZmZlY3RpbmcgdGhlIGxpbmUgaGVpZ2h0IGluXFxuICogYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnN1YixcXG5zdXAge1xcbiAgZm9udC1zaXplOiA3NSU7XFxuICBsaW5lLWhlaWdodDogMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTsgfVxcbnN1YiB7XFxuICBib3R0b206IC0wLjI1ZW07IH1cXG5zdXAge1xcbiAgdG9wOiAtMC41ZW07IH1cXG4vKiBFbWJlZGRlZCBjb250ZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5hdWRpbyxcXG52aWRlbyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBpT1MgNC03LlxcbiAqL1xcbmF1ZGlvOm5vdChbY29udHJvbHNdKSB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgaGVpZ2h0OiAwOyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBib3JkZXIgb24gaW1hZ2VzIGluc2lkZSBsaW5rcyBpbiBJRSAxMC0uXFxuICovXFxuaW1nIHtcXG4gIGJvcmRlci1zdHlsZTogbm9uZTsgfVxcbi8qKlxcbiAqIEhpZGUgdGhlIG92ZXJmbG93IGluIElFLlxcbiAqL1xcbnN2Zzpub3QoOnJvb3QpIHtcXG4gIG92ZXJmbG93OiBoaWRkZW47IH1cXG4vKiBGb3Jtc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ2hhbmdlIHRoZSBmb250IHN0eWxlcyBpbiBhbGwgYnJvd3NlcnMgKG9waW5pb25hdGVkKS5cXG4gKiAyLiBSZW1vdmUgdGhlIG1hcmdpbiBpbiBGaXJlZm94IGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmlucHV0LFxcbm9wdGdyb3VwLFxcbnNlbGVjdCxcXG50ZXh0YXJlYSB7XFxuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMTAwJTtcXG4gIC8qIDEgKi9cXG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xcbiAgLyogMSAqL1xcbiAgbWFyZ2luOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogU2hvdyB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICogMS4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQge1xcbiAgLyogMSAqL1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGluaGVyaXRhbmNlIG9mIHRleHQgdHJhbnNmb3JtIGluIEVkZ2UsIEZpcmVmb3gsIGFuZCBJRS5cXG4gKiAxLiBSZW1vdmUgdGhlIGluaGVyaXRhbmNlIG9mIHRleHQgdHJhbnNmb3JtIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uLFxcbnNlbGVjdCB7XFxuICAvKiAxICovXFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTsgfVxcbi8qKlxcbiAqIDEuIFByZXZlbnQgYSBXZWJLaXQgYnVnIHdoZXJlICgyKSBkZXN0cm95cyBuYXRpdmUgYGF1ZGlvYCBhbmQgYHZpZGVvYFxcbiAqICAgIGNvbnRyb2xzIGluIEFuZHJvaWQgNC5cXG4gKiAyLiBDb3JyZWN0IHRoZSBpbmFiaWxpdHkgdG8gc3R5bGUgY2xpY2thYmxlIHR5cGVzIGluIGlPUyBhbmQgU2FmYXJpLlxcbiAqL1xcbmJ1dHRvbixcXG5odG1sIFt0eXBlPVxcXCJidXR0b25cXFwiXSxcXG5bdHlwZT1cXFwicmVzZXRcXFwiXSxcXG5bdHlwZT1cXFwic3VibWl0XFxcIl0ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIGJvcmRlciBhbmQgcGFkZGluZyBpbiBGaXJlZm94LlxcbiAqL1xcbmJ1dHRvbjo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcInJlc2V0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIGJvcmRlci1zdHlsZTogbm9uZTtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKipcXG4gKiBSZXN0b3JlIHRoZSBmb2N1cyBzdHlsZXMgdW5zZXQgYnkgdGhlIHByZXZpb3VzIHJ1bGUuXFxuICovXFxuYnV0dG9uOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06LW1vei1mb2N1c3Jpbmcge1xcbiAgb3V0bGluZTogMXB4IGRvdHRlZCBCdXR0b25UZXh0OyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgcGFkZGluZyBpbiBGaXJlZm94LlxcbiAqL1xcbmZpZWxkc2V0IHtcXG4gIHBhZGRpbmc6IDAuMzVlbSAwLjc1ZW0gMC42MjVlbTsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIHRleHQgd3JhcHBpbmcgaW4gRWRnZSBhbmQgSUUuXFxuICogMi4gQ29ycmVjdCB0aGUgY29sb3IgaW5oZXJpdGFuY2UgZnJvbSBgZmllbGRzZXRgIGVsZW1lbnRzIGluIElFLlxcbiAqIDMuIFJlbW92ZSB0aGUgcGFkZGluZyBzbyBkZXZlbG9wZXJzIGFyZSBub3QgY2F1Z2h0IG91dCB3aGVuIHRoZXkgemVybyBvdXRcXG4gKiAgICBgZmllbGRzZXRgIGVsZW1lbnRzIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5sZWdlbmQge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgLyogMSAqL1xcbiAgY29sb3I6IGluaGVyaXQ7XFxuICAvKiAyICovXFxuICBkaXNwbGF5OiB0YWJsZTtcXG4gIC8qIDEgKi9cXG4gIG1heC13aWR0aDogMTAwJTtcXG4gIC8qIDEgKi9cXG4gIHBhZGRpbmc6IDA7XFxuICAvKiAzICovXFxuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xcbiAgLyogMSAqLyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMi4gQWRkIHRoZSBjb3JyZWN0IHZlcnRpY2FsIGFsaWdubWVudCBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBPcGVyYS5cXG4gKi9cXG5wcm9ncmVzcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICAvKiAxICovXFxuICB2ZXJ0aWNhbC1hbGlnbjogYmFzZWxpbmU7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGRlZmF1bHQgdmVydGljYWwgc2Nyb2xsYmFyIGluIElFLlxcbiAqL1xcbnRleHRhcmVhIHtcXG4gIG92ZXJmbG93OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGJveCBzaXppbmcgaW4gSUUgMTAtLlxcbiAqIDIuIFJlbW92ZSB0aGUgcGFkZGluZyBpbiBJRSAxMC0uXFxuICovXFxuW3R5cGU9XFxcImNoZWNrYm94XFxcIl0sXFxuW3R5cGU9XFxcInJhZGlvXFxcIl0ge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIGN1cnNvciBzdHlsZSBvZiBpbmNyZW1lbnQgYW5kIGRlY3JlbWVudCBidXR0b25zIGluIENocm9tZS5cXG4gKi9cXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24sXFxuW3R5cGU9XFxcIm51bWJlclxcXCJdOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uIHtcXG4gIGhlaWdodDogYXV0bzsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIG9kZCBhcHBlYXJhbmNlIGluIENocm9tZSBhbmQgU2FmYXJpLlxcbiAqIDIuIENvcnJlY3QgdGhlIG91dGxpbmUgc3R5bGUgaW4gU2FmYXJpLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IHRleHRmaWVsZDtcXG4gIC8qIDEgKi9cXG4gIG91dGxpbmUtb2Zmc2V0OiAtMnB4O1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbm5lciBwYWRkaW5nIGFuZCBjYW5jZWwgYnV0dG9ucyBpbiBDaHJvbWUgYW5kIFNhZmFyaSBvbiBtYWNPUy5cXG4gKi9cXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWNhbmNlbC1idXR0b24sXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdOjotd2Via2l0LXNlYXJjaC1kZWNvcmF0aW9uIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTsgfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICogMi4gQ2hhbmdlIGZvbnQgcHJvcGVydGllcyB0byBgaW5oZXJpdGAgaW4gU2FmYXJpLlxcbiAqL1xcbjo6LXdlYmtpdC1maWxlLXVwbG9hZC1idXR0b24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XFxuICAvKiAxICovXFxuICBmb250OiBpbmhlcml0O1xcbiAgLyogMiAqLyB9XFxuLyogSW50ZXJhY3RpdmVcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gRWRnZSwgSUUsIGFuZCBGaXJlZm94LlxcbiAqL1xcbmRldGFpbHMsXFxubWVudSB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnN1bW1hcnkge1xcbiAgZGlzcGxheTogbGlzdC1pdGVtOyB9XFxuLyogU2NyaXB0aW5nXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5jYW52YXMge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxudGVtcGxhdGUge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcbi8qIEhpZGRlblxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgMTAtLlxcbiAqL1xcbltoaWRkZW5dIHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBLDRFQUE0RTtBQUM1RTtnRkFDZ0Y7QUFDaEY7Ozs7R0FJRztBQUNIO0VBQ0Usa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLCtCQUErQjtFQUMvQixPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDtFQUNFLFVBQVUsRUFBRTtBQUNkOztHQUVHO0FBQ0g7Ozs7OztFQU1FLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDtFQUNFLGVBQWU7RUFDZixpQkFBaUIsRUFBRTtBQUNyQjtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7OztFQUdFLE9BQU87RUFDUCxlQUFlLEVBQUU7QUFDbkI7O0dBRUc7QUFDSDtFQUNFLGlCQUFpQixFQUFFO0FBQ3JCOzs7R0FHRztBQUNIO0VBQ0UsZ0NBQWdDO1VBQ3hCLHdCQUF3QjtFQUNoQyxPQUFPO0VBQ1AsVUFBVTtFQUNWLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsT0FBTyxFQUFFO0FBQ1g7OztHQUdHO0FBQ0g7RUFDRSxrQ0FBa0M7RUFDbEMsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7RUFDRSw4QkFBOEI7RUFDOUIsT0FBTztFQUNQLHNDQUFzQztFQUN0QyxPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDtFQUNFLG9CQUFvQjtFQUNwQixPQUFPO0VBQ1AsMkJBQTJCO0VBQzNCLE9BQU87RUFDUCwwQ0FBMEM7VUFDbEMsa0NBQWtDO0VBQzFDLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7O0VBRUUscUJBQXFCLEVBQUU7QUFDekI7O0dBRUc7QUFDSDs7RUFFRSxvQkFBb0IsRUFBRTtBQUN4Qjs7O0dBR0c7QUFDSDs7O0VBR0Usa0NBQWtDO0VBQ2xDLE9BQU87RUFDUCxlQUFlO0VBQ2YsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCOztHQUVHO0FBQ0g7RUFDRSx1QkFBdUI7RUFDdkIsWUFBWSxFQUFFO0FBQ2hCOztHQUVHO0FBQ0g7RUFDRSxlQUFlLEVBQUU7QUFDbkI7OztHQUdHO0FBQ0g7O0VBRUUsZUFBZTtFQUNmLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIseUJBQXlCLEVBQUU7QUFDN0I7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLFlBQVksRUFBRTtBQUNoQjtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDs7RUFFRSxzQkFBc0IsRUFBRTtBQUMxQjs7R0FFRztBQUNIO0VBQ0UsY0FBYztFQUNkLFVBQVUsRUFBRTtBQUNkOztHQUVHO0FBQ0g7RUFDRSxtQkFBbUIsRUFBRTtBQUN2Qjs7R0FFRztBQUNIO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7Z0ZBQ2dGO0FBQ2hGOzs7R0FHRztBQUNIOzs7OztFQUtFLHdCQUF3QjtFQUN4QixPQUFPO0VBQ1AsZ0JBQWdCO0VBQ2hCLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLFVBQVU7RUFDVixPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDs7RUFFRSxPQUFPO0VBQ1Asa0JBQWtCLEVBQUU7QUFDdEI7OztHQUdHO0FBQ0g7O0VBRUUsT0FBTztFQUNQLHFCQUFxQixFQUFFO0FBQ3pCOzs7O0dBSUc7QUFDSDs7OztFQUlFLDJCQUEyQjtFQUMzQixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOzs7O0VBSUUsbUJBQW1CO0VBQ25CLFdBQVcsRUFBRTtBQUNmOztHQUVHO0FBQ0g7Ozs7RUFJRSwrQkFBK0IsRUFBRTtBQUNuQzs7R0FFRztBQUNIO0VBQ0UsK0JBQStCLEVBQUU7QUFDbkM7Ozs7O0dBS0c7QUFDSDtFQUNFLCtCQUErQjtVQUN2Qix1QkFBdUI7RUFDL0IsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLFdBQVc7RUFDWCxPQUFPO0VBQ1Asb0JBQW9CO0VBQ3BCLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIO0VBQ0Usc0JBQXNCO0VBQ3RCLE9BQU87RUFDUCx5QkFBeUI7RUFDekIsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDs7RUFFRSwrQkFBK0I7VUFDdkIsdUJBQXVCO0VBQy9CLE9BQU87RUFDUCxXQUFXO0VBQ1gsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDs7RUFFRSxhQUFhLEVBQUU7QUFDakI7OztHQUdHO0FBQ0g7RUFDRSw4QkFBOEI7RUFDOUIsT0FBTztFQUNQLHFCQUFxQjtFQUNyQixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOztFQUVFLHlCQUF5QixFQUFFO0FBQzdCOzs7R0FHRztBQUNIO0VBQ0UsMkJBQTJCO0VBQzNCLE9BQU87RUFDUCxjQUFjO0VBQ2QsT0FBTyxFQUFFO0FBQ1g7Z0ZBQ2dGO0FBQ2hGOzs7R0FHRztBQUNIOztFQUVFLGVBQWUsRUFBRTtBQUNuQjs7R0FFRztBQUNIO0VBQ0UsbUJBQW1CLEVBQUU7QUFDdkI7Z0ZBQ2dGO0FBQ2hGOztHQUVHO0FBQ0g7RUFDRSxzQkFBc0IsRUFBRTtBQUMxQjs7R0FFRztBQUNIO0VBQ0UsY0FBYyxFQUFFO0FBQ2xCO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIO0VBQ0UsY0FBYyxFQUFFXCIsXCJmaWxlXCI6XCJub3JtYWxpemUuY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi8qISBub3JtYWxpemUuY3NzIHY3LjAuMCB8IE1JVCBMaWNlbnNlIHwgZ2l0aHViLmNvbS9uZWNvbGFzL25vcm1hbGl6ZS5jc3MgKi9cXG4vKiBEb2N1bWVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgbGluZSBoZWlnaHQgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIFByZXZlbnQgYWRqdXN0bWVudHMgb2YgZm9udCBzaXplIGFmdGVyIG9yaWVudGF0aW9uIGNoYW5nZXMgaW5cXG4gKiAgICBJRSBvbiBXaW5kb3dzIFBob25lIGFuZCBpbiBpT1MuXFxuICovXFxuaHRtbCB7XFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIC1tcy10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqLyB9XFxuLyogU2VjdGlvbnNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIFJlbW92ZSB0aGUgbWFyZ2luIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqL1xcbmJvZHkge1xcbiAgbWFyZ2luOiAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXJ0aWNsZSxcXG5hc2lkZSxcXG5mb290ZXIsXFxuaGVhZGVyLFxcbm5hdixcXG5zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgZm9udCBzaXplIGFuZCBtYXJnaW4gb24gYGgxYCBlbGVtZW50cyB3aXRoaW4gYHNlY3Rpb25gIGFuZFxcbiAqIGBhcnRpY2xlYCBjb250ZXh0cyBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBTYWZhcmkuXFxuICovXFxuaDEge1xcbiAgZm9udC1zaXplOiAyZW07XFxuICBtYXJnaW46IDAuNjdlbSAwOyB9XFxuLyogR3JvdXBpbmcgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxuZmlnY2FwdGlvbixcXG5maWd1cmUsXFxubWFpbiB7XFxuICAvKiAxICovXFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBtYXJnaW4gaW4gSUUgOC5cXG4gKi9cXG5maWd1cmUge1xcbiAgbWFyZ2luOiAxZW0gNDBweDsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIEZpcmVmb3guXFxuICogMi4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZSBhbmQgSUUuXFxuICovXFxuaHIge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAvKiAxICovXFxuICBoZWlnaHQ6IDA7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnByZSB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qIFRleHQtbGV2ZWwgc2VtYW50aWNzXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGdyYXkgYmFja2dyb3VuZCBvbiBhY3RpdmUgbGlua3MgaW4gSUUgMTAuXFxuICogMi4gUmVtb3ZlIGdhcHMgaW4gbGlua3MgdW5kZXJsaW5lIGluIGlPUyA4KyBhbmQgU2FmYXJpIDgrLlxcbiAqL1xcbmEge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAvKiAxICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbi1za2lwOiBvYmplY3RzO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBib3R0b20gYm9yZGVyIGluIENocm9tZSA1Ny0gYW5kIEZpcmVmb3ggMzktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB0ZXh0IGRlY29yYXRpb24gaW4gQ2hyb21lLCBFZGdlLCBJRSwgT3BlcmEsIGFuZCBTYWZhcmkuXFxuICovXFxuYWJiclt0aXRsZV0ge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gIC8qIDEgKi9cXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFByZXZlbnQgdGhlIGR1cGxpY2F0ZSBhcHBsaWNhdGlvbiBvZiBgYm9sZGVyYCBieSB0aGUgbmV4dCBydWxlIGluIFNhZmFyaSA2LlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBpbmhlcml0OyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgd2VpZ2h0IGluIENocm9tZSwgRWRnZSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogYm9sZGVyOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuY29kZSxcXG5rYmQsXFxuc2FtcCB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHN0eWxlIGluIEFuZHJvaWQgNC4zLS5cXG4gKi9cXG5kZm4ge1xcbiAgZm9udC1zdHlsZTogaXRhbGljOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGJhY2tncm91bmQgYW5kIGNvbG9yIGluIElFIDktLlxcbiAqL1xcbm1hcmsge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmMDtcXG4gIGNvbG9yOiAjMDAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc21hbGwge1xcbiAgZm9udC1zaXplOiA4MCU7IH1cXG4vKipcXG4gKiBQcmV2ZW50IGBzdWJgIGFuZCBgc3VwYCBlbGVtZW50cyBmcm9tIGFmZmVjdGluZyB0aGUgbGluZSBoZWlnaHQgaW5cXG4gKiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3ViLFxcbnN1cCB7XFxuICBmb250LXNpemU6IDc1JTtcXG4gIGxpbmUtaGVpZ2h0OiAwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lOyB9XFxuc3ViIHtcXG4gIGJvdHRvbTogLTAuMjVlbTsgfVxcbnN1cCB7XFxuICB0b3A6IC0wLjVlbTsgfVxcbi8qIEVtYmVkZGVkIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmF1ZGlvLFxcbnZpZGVvIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGlPUyA0LTcuXFxuICovXFxuYXVkaW86bm90KFtjb250cm9sc10pIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBoZWlnaHQ6IDA7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGJvcmRlciBvbiBpbWFnZXMgaW5zaWRlIGxpbmtzIGluIElFIDEwLS5cXG4gKi9cXG5pbWcge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lOyB9XFxuLyoqXFxuICogSGlkZSB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICovXFxuc3ZnOm5vdCg6cm9vdCkge1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjsgfVxcbi8qIEZvcm1zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDaGFuZ2UgdGhlIGZvbnQgc3R5bGVzIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqIDIuIFJlbW92ZSB0aGUgbWFyZ2luIGluIEZpcmVmb3ggYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQsXFxub3B0Z3JvdXAsXFxuc2VsZWN0LFxcbnRleHRhcmVhIHtcXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICBtYXJnaW46IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBTaG93IHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKiAxLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCB7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRWRnZSwgRmlyZWZveCwgYW5kIElFLlxcbiAqIDEuIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b24sXFxuc2VsZWN0IHtcXG4gIC8qIDEgKi9cXG4gIHRleHQtdHJhbnNmb3JtOiBub25lOyB9XFxuLyoqXFxuICogMS4gUHJldmVudCBhIFdlYktpdCBidWcgd2hlcmUgKDIpIGRlc3Ryb3lzIG5hdGl2ZSBgYXVkaW9gIGFuZCBgdmlkZW9gXFxuICogICAgY29udHJvbHMgaW4gQW5kcm9pZCA0LlxcbiAqIDIuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmh0bWwgW3R5cGU9XFxcImJ1dHRvblxcXCJdLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgYm9yZGVyIGFuZCBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qKlxcbiAqIFJlc3RvcmUgdGhlIGZvY3VzIHN0eWxlcyB1bnNldCBieSB0aGUgcHJldmlvdXMgcnVsZS5cXG4gKi9cXG5idXR0b246LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTotbW96LWZvY3VzcmluZyB7XFxuICBvdXRsaW5lOiAxcHggZG90dGVkIEJ1dHRvblRleHQ7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuZmllbGRzZXQge1xcbiAgcGFkZGluZzogMC4zNWVtIDAuNzVlbSAwLjYyNWVtOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgdGV4dCB3cmFwcGluZyBpbiBFZGdlIGFuZCBJRS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBjb2xvciBpbmhlcml0YW5jZSBmcm9tIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gSUUuXFxuICogMy4gUmVtb3ZlIHRoZSBwYWRkaW5nIHNvIGRldmVsb3BlcnMgYXJlIG5vdCBjYXVnaHQgb3V0IHdoZW4gdGhleSB6ZXJvIG91dFxcbiAqICAgIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmxlZ2VuZCB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBjb2xvcjogaW5oZXJpdDtcXG4gIC8qIDIgKi9cXG4gIGRpc3BsYXk6IHRhYmxlO1xcbiAgLyogMSAqL1xcbiAgbWF4LXdpZHRoOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDMgKi9cXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XFxuICAvKiAxICovIH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdmVydGljYWwgYWxpZ25tZW50IGluIENocm9tZSwgRmlyZWZveCwgYW5kIE9wZXJhLlxcbiAqL1xcbnByb2dyZXNzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIC8qIDEgKi9cXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgZGVmYXVsdCB2ZXJ0aWNhbCBzY3JvbGxiYXIgaW4gSUUuXFxuICovXFxudGV4dGFyZWEge1xcbiAgb3ZlcmZsb3c6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBJRSAxMC0uXFxuICogMi4gUmVtb3ZlIHRoZSBwYWRkaW5nIGluIElFIDEwLS5cXG4gKi9cXG5bdHlwZT1cXFwiY2hlY2tib3hcXFwiXSxcXG5bdHlwZT1cXFwicmFkaW9cXFwiXSB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgY3Vyc29yIHN0eWxlIG9mIGluY3JlbWVudCBhbmQgZGVjcmVtZW50IGJ1dHRvbnMgaW4gQ2hyb21lLlxcbiAqL1xcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixcXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xcbiAgaGVpZ2h0OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgb2RkIGFwcGVhcmFuY2UgaW4gQ2hyb21lIGFuZCBTYWZhcmkuXFxuICogMi4gQ29ycmVjdCB0aGUgb3V0bGluZSBzdHlsZSBpbiBTYWZhcmkuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4dGZpZWxkO1xcbiAgLyogMSAqL1xcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIHBhZGRpbmcgYW5kIGNhbmNlbCBidXR0b25zIGluIENocm9tZSBhbmQgU2FmYXJpIG9uIG1hY09TLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtY2FuY2VsLWJ1dHRvbixcXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKiAyLiBDaGFuZ2UgZm9udCBwcm9wZXJ0aWVzIHRvIGBpbmhlcml0YCBpbiBTYWZhcmkuXFxuICovXFxuOjotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQ6IGluaGVyaXQ7XFxuICAvKiAyICovIH1cXG4vKiBJbnRlcmFjdGl2ZVxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBFZGdlLCBJRSwgYW5kIEZpcmVmb3guXFxuICovXFxuZGV0YWlscyxcXG5tZW51IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3VtbWFyeSB7XFxuICBkaXNwbGF5OiBsaXN0LWl0ZW07IH1cXG4vKiBTY3JpcHRpbmdcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmNhbnZhcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG50ZW1wbGF0ZSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuLyogSGlkZGVuXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSAxMC0uXFxuICovXFxuW2hpZGRlbl0ge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zb2NpYWwtbmV0d29yay1pY29uIHtcXG4gIG1heC13aWR0aDogMjBweDsgfVxcbi5zaXRlLWZvb3RlciB7XFxuICBwYWRkaW5nOiAxJSA0JTtcXG4gIGhlaWdodDogOHZoO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc29jaWFsLWljb25zLWxpc3QgPiBsaSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuc2l0ZS1mb290ZXIge1xcbiAgICBoZWlnaHQ6IDEzdmg7IH0gfVxcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLnNpdGUtZm9vdGVyID4gLnJvdyA+IC5jb2wteHMtMTI6Zmlyc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0lBQ0kscUJBQXFCO0NBQ3hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxjQUFjO0NBQ2pCO0FBQ0Q7SUFDSSx5QkFBeUI7Q0FDNUI7QUFDRDtJQUNJLHdCQUF3QjtDQUMzQjtBQUNEO0lBQ0ksMEJBQTBCO0NBQzdCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQywwQkFBMEI7UUFDdEIsdUJBQXVCO1lBQ25CLG9CQUFvQjtDQUMvQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsNkJBQTZCO0lBQzdCLDhCQUE4QjtRQUMxQiwyQkFBMkI7WUFDdkIsdUJBQXVCO0NBQ2xDO0FBQ0Q7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGNBQWM7SUFDZCxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGdEQUFnRDtJQUNoRCx3REFBd0Q7SUFDeEQsZ0RBQWdEO0lBQ2hELDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFDeEMsNkVBQTZFO0NBQ2hGO0FBQ0Q7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLGVBQWU7RUFDZixZQUFZO0VBQ1osZUFBZSxFQUFFO0FBQ25CO0VBQ0Usc0JBQXNCO0VBQ3RCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0U7SUFDRSxhQUFhLEVBQUUsRUFBRTtBQUNyQjtFQUNFO0lBQ0UsZUFBZSxFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG4uc29jaWFsLW5ldHdvcmstaWNvbiB7XFxuICBtYXgtd2lkdGg6IDIwcHg7IH1cXG4uc2l0ZS1mb290ZXIge1xcbiAgcGFkZGluZzogMSUgNCU7XFxuICBoZWlnaHQ6IDh2aDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLnNvY2lhbC1pY29ucy1saXN0ID4gbGkge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLnNpdGUtZm9vdGVyIHtcXG4gICAgaGVpZ2h0OiAxM3ZoOyB9IH1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gIC5zaXRlLWZvb3RlciA+IC5yb3cgPiAuY29sLXhzLTEyOmZpcnN0LWNoaWxkIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwidmFyIGVzY2FwZSA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvdXJsL2VzY2FwZS5qc1wiKTtcbmV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuYnV0dG9uOmZvY3VzIHtvdXRsaW5lOjA7fVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJde1xcbiAgb3V0bGluZTpub25lO1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJde1xcbiAgb3V0bGluZTpub25lO1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbi5mbGV4LWNlbnRlcntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uZmxleC1taWRkbGV7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG4ucHVycGxlLWZvbnR7XFxuICBjb2xvcjogcmdiKDkxLDg1LDE1NSk7XFxufVxcbi5ibHVlLWZvbnR7XFxuICBjb2xvcjogcmdiKDY4LDIwOSwyMDIpO1xcbn1cXG4uZ3JleS1mb250e1xcbiAgY29sb3I6cmdiKDE1MCwxNTAsMTUwKTtcXG5cXG59XFxuLmxpZ2h0LWdyZXktZm9udHtcXG4gIGNvbG9yOiAgcmdiKDE0OCwxNDgsMTQ4KTtcXG59XFxuLmJsYWNrLWZvbnR7XFxuICBjb2xvcjogYmxhY2s7XFxufVxcbi53aGl0ZS1mb250e1xcbiAgY29sb3I6IHdoaXRlO1xcbn1cXG4ucGluay1mb250e1xcbiAgY29sb3I6IHJnYigyNDAsOTMsMTA4KTtcXG59XFxuLnBpbmstYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYigyNDAsOTMsMTA4KTtcXG59XFxuLnB1cnBsZS1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDkwLDg1LDE1NSk7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlcntcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi5tYXJnaW4tYXV0b3tcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuLm5vLXBhZGRpbmd7XFxuICBwYWRkaW5nOiAwO1xcbn1cXG4uaWNvbi1jbHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3O1xcbn1cXG4uZmxleC1yb3ctcmV2e1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IHJldmVyc2U7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbn1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG59XFxuLnNlYXJjaC10aGVtZXtcXG4gIGJhY2tncm91bmQ6IHVybChcIiArIGVzY2FwZShyZXF1aXJlKFwiLi9nZW9tZXRyeTIucG5nXCIpKSArIFwiKTtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5ib2R5IHtcXG4gICAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICAgIGNvbG9yOiB3aGl0ZTtcXG59XFxuLmJsYWNrIHtcXG4gICAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gICAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzYWQyY2JcXG59XFxuLndoaXRlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgICBmb250LXdlaWdodDogODAwO1xcbn1cXG4uZm9udC1saWdodCB7XFxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xcbiAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5uby1saXN0LXN0eWxlIHtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gICAgb3V0bGluZTogbm9uZTtcXG4gICAgYm9yZGVyOiBub25lO1xcbn1cXG4uYm9yZGVyLXJhZGl1cyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVcXG57XFxuICAgIDAlICAge29wYWNpdHk6MX1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMH1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDF9XFxufVxcbkBrZXlmcmFtZXMgZmFkZVxcbntcXG4gICAgMCUgICB7b3BhY2l0eToxfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAwfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMX1cXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGUyXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDF9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5Aa2V5ZnJhbWVzIGZhZGUyXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDF9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZTNcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMX1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkBrZXlmcmFtZXMgZmFkZTNcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMX1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlNFxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAxfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuQGtleWZyYW1lcyBmYWRlNFxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAxfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuLm1haW4tZm9ybSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDE0dmg7XFxufVxcbi5zZWFyY2gtYnV0dG9uIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtICFpbXBvcnRhbnQ7XFxufVxcbi5zZWFyY2gtc2VjdGlvbiwgLnN1Ym1pdC1ob21lLWxpbmsge1xcbiAgY29sb3I6IHdoaXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgcGFkZGluZzogMiU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG59XFxuLnNlY29uZC1wYXJ0IHtcXG4gIG1hcmdpbi10b3A6IDQlO1xcbn1cXG4uc3VibWl0LWhvbWUtbGluayB7XFxuICBtYXJnaW4tdG9wOiAyJTtcXG4gIHBhZGRpbmc6IDVweDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICB3aWR0aDogODAlO1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxufVxcbi5zZWFyY2gtYnV0dG9uOnZpc2l0ZWQge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbn1cXG4uc2VhcmNoLWJ1dHRvbjpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbn1cXG4uc2VhcmNoLWlucHV0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLnNlYXJjaC1pbnB1dDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcntcXG4gIHBhZGRpbmctcmlnaHQ6IDQlO1xcbn1cXG4uc2VhcmNoLWlucHV0Oi1tcy1pbnB1dC1wbGFjZWhvbGRlcntcXG4gIHBhZGRpbmctcmlnaHQ6IDQlO1xcbn1cXG4uc2VhcmNoLWlucHV0OjotbXMtaW5wdXQtcGxhY2Vob2xkZXJ7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTtcXG59XFxuLnNlYXJjaC1pbnB1dDo6cGxhY2Vob2xkZXJ7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTtcXG59XFxuc2VsZWN0IHtcXG4gIGhlaWdodDogNXZoO1xcbiAgY29sb3I6IGJsYWNrO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi5kZWFsLXR5cGUtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaW5wdXQtbGFiZWwge1xcbiAgICBtYXJnaW4tbGVmdDogMSU7XFxuICB9XFxuICAuc2VhcmNoLWJ1dHRvbiB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBtYXJnaW46IDUlIGF1dG87XFxuICB9XFxufVxcbmJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbn1cXG4ubG9nby1zZWN0aW9uIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNyU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLmxvZ28taW1hZ2Uge1xcbiAgICBtYXgtd2lkdGg6IDEyMHB4O1xcbn1cXG4uc2xpZGVyIHtcXG4gICAgaGVpZ2h0OiAxMDB2aDtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmhlYWRlci1tYWluIHtcXG4gICAgei1pbmRleDogOTk5O1xcbiAgICB0b3A6IDN2aDtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbn1cXG4uaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICAgIGJvcmRlcjogdGhpbiBzb2xpZCB3aGl0ZTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDA7XFxuICAgIHRvcDogNnZoO1xcbn1cXG4uc2xpZGVyLWNvbHVtbiB7XFxuICAgIHBhZGRpbmc6IDA7XFxufVxcbi5zbGlkZTEsLnNsaWRlMiwuc2xpZGUzLC5zbGlkZTQge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxufVxcbi5zbGlkZTEge1xcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCIgKyBlc2NhcGUocmVxdWlyZShcIi4vbWljaGFsLWt1YmFsY3p5ay0yNjA5MDktdW5zcGxhc2guanBnXCIpKSArIFwiKSBuby1yZXBlYXQgOTAlIDgwJTtcXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcXG4gICAgYW5pbWF0aW9uOmZhZGUgMTBzIGluZmluaXRlO1xcbiAgICAtd2Via2l0LWFuaW1hdGlvbjpmYWRlIDEwcyBpbmZpbml0ZTtcXG5cXG5cXG59XFxuLnNsaWRlMiB7XFxuICAgIGJhY2tncm91bmQ6IHVybChcIiArIGVzY2FwZShyZXF1aXJlKFwiLi9tYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGdcIikpICsgXCIpIG5vLXJlcGVhdCBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XFxuICAgIGFuaW1hdGlvbjpmYWRlMiAxMHMgaW5maW5pdGU7XFxuICAgIC13ZWJraXQtYW5pbWF0aW9uOmZhZGUyIDEwcyBpbmZpbml0ZTtcXG59XFxuLnNsaWRlMyB7XFxuICAgIGJhY2tncm91bmQ6IHVybChcIiArIGVzY2FwZShyZXF1aXJlKFwiLi9jYXNleS1ob3JuZXItNTMzNTg2LXVuc3BsYXNoLmpwZ1wiKSkgKyBcIikgbm8tcmVwZWF0IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcXG4gICAgYW5pbWF0aW9uOmZhZGUzIDEwcyBpbmZpbml0ZTtcXG4gICAgLXdlYmtpdC1hbmltYXRpb246ZmFkZTMgMTBzIGluZmluaXRlO1xcbn1cXG4uc2xpZGU0IHtcXG4gICAgYmFja2dyb3VuZDogdXJsKFwiICsgZXNjYXBlKHJlcXVpcmUoXCIuL2x1a2UtdmFuLXp5bC01MDQwMzItdW5zcGxhc2guanBnXCIpKSArIFwiKSBuby1yZXBlYXQgY2VudGVyO1xcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xcbiAgICBhbmltYXRpb246ZmFkZTQgMTBzIGluZmluaXRlO1xcbiAgICAtd2Via2l0LWFuaW1hdGlvbjpmYWRlNCAxMHMgaW5maW5pdGU7XFxufVxcbi5zdWJtaXQtaG91c2UtYnV0dG9uLFxcbi5zdWJtaXQtaG91c2UtYnV0dG9uOnZpc2l0ZWQsXFxuLnN1Ym1pdC1ob3VzZS1idXR0b246aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuLmZlYXR1cmVzLXNlY3Rpb24ge1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIG1hcmdpbi10b3A6IC01dmg7XFxufVxcbmlucHV0W3R5cGU9cmFkaW9dIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbn1cXG4uaW5wdXQtbGFiZWwge1xcbiAgICBmb250LXdlaWdodDogbGlnaHRlcjtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbn1cXG4uZmVhdHVyZS1ib3gge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICBtYXJnaW4tbGVmdDogMiU7XFxuICAgIGhlaWdodDogMzZ2aDtcXG4gICAgd2lkdGg6IDI5JSAhaW1wb3J0YW50O1xcbiAgICBtaW4taGVpZ2h0OiA0MnZoO1xcbn1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgyKSB7XFxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcXG59XFxuLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgICBtYXJnaW4tbGVmdDogNSU7XFxufVxcbi5mZWF0dXJlLWJveCA+IGRpdiA+IGltZyB7XFxuICAgIG1heC13aWR0aDogNzBweDtcXG59XFxuLndoeS11cyB7XFxuICAgIG1hcmdpbi10b3A6IDYlO1xcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcXG59XFxuLndoeS11cy1pbWcge1xcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbn1cXG4ud2h5LWxpc3Qge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgY29sb3I6IGJsYWNrO1xcbiAgICBwYWRkaW5nOiAwO1xcbn1cXG4ud2h5LWxpc3QgPiBsaSB7XFxuICAgIG1hcmdpbi10b3A6IDMlO1xcbn1cXG4ud2h5LWxpc3QgPiBsaSA+IGkge1xcbiAgICBtYXJnaW4tbGVmdDogMiU7XFxufVxcbi53aHktdXMtaW1nLWNvbHVtbiB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4ucmVhc29ucy1zZWN0aW9uIHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICAgIGRpcmVjdGlvbjogcnRsO1xcbiAgICBwYWRkaW5nOiAwO1xcbn1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gICAgLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgICAgICAgd2lkdGg6IDQwJTtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxJTtcXG4gICAgfVxcbiAgICAubW9iaWxlLWhlYWRlciB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgICB9XFxuICAgIC5sb2dvLWltYWdlIHtcXG4gICAgICAgIG1heC13aWR0aDogOTBweDtcXG4gICAgfVxcbiAgICAubWFpbi10aXRsZSB7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIH1cXG4gICAgLmZlYXR1cmUtYm94IHtcXG4gICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1JTtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICB9XFxuICAgIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDIpIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICB9XFxuICAgIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDMpIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICB9XFxuICAgIC53aHktdXMtaW1nIHtcXG4gICAgICAgIG1heC13aWR0aDogMjQ3cHg7XFxuICAgIH1cXG4gIC5mZWF0dXJlcy1zZWN0aW9uIHtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICAgbWFyZ2luLXRvcDogMnZoO1xcbiAgfVxcbn1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gICAgLm1vYmlsZS1oZWFkZXIge1xcbiAgICAgICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gICAgfVxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgLypiYWNrZ3JvdW5kOiB1cmwoXFxcIi4uL2Fzc2V0cy9pbWFnZXMvcGF0dGVybi9nZW9tZXRyeTIucG5nXFxcIik7Ki9cXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLXRhZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNjAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XFxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiA2dmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLnByaWNlLWRpdiB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDg1JTtcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbi5tYXJnaW4tMSB7XFxuICBtYXJnaW4tbGVmdDogMyU7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuLmltZy1ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMHB4OyB9XFxuLm1haW4tZm9ybS1zZWFyY2gtcmVzdWx0IHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIG1hcmdpbjogYXV0bztcXG4gIG1hcmdpbi10b3A6IC03JTtcXG4gIG1hcmdpbi1ib3R0b206IDYlOyB9XFxuLyojaW1nLTF7Ki9cXG4vKmJhY2tncm91bmQtaW1hZ2U6IHVybCguLi9hc3NldHMvaW1hZ2VzL3ZpbGxhMS5qcGcpOyovXFxuLyp9Ki9cXG4vKiNpbWctMnsqL1xcbi8qYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2Fzc2V0cy9pbWFnZXMvdmlsbGEyLmpwZyk7Ki9cXG4vKn0qL1xcbi8qI2ltZy0zLCAjaW1nLTQsICNpbWctNXsqL1xcbi8qYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2Fzc2V0cy9pbWFnZXMvdW5kZXJ3YXRlci5qcGcpOyovXFxuLyp9Ki9cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7IH1cXG4uZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTsgfVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlOyB9XFxuICAubWFyLWxlZnQge1xcbiAgICBtYXJnaW46IDA7IH1cXG4gIC5pY29ucy1jbCB7XFxuICAgIHdpZHRoOiAxM3Z3O1xcbiAgICBoZWlnaHQ6IDd2aDsgfVxcbiAgLmZvb3Rlci10ZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDEycHg7IH1cXG4gIC5pY29ucy1tYXIge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICBtYXJnaW4tYm90dG9tOiA1JTsgfVxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICAgIHBhZGRpbmc6IDMlOyB9XFxuICAuZHJvcGRvd24tY29udGVudCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gICAgbWluLXdpZHRoOiAyMDBweDtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgICB6LWluZGV4OiAxO1xcbiAgICBsZWZ0OiAxMnZ3O1xcbiAgICB0b3A6IDEwdmg7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgIGhlaWdodDogMzUwcHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDV2aDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICBmbG9hdDogcmlnaHQ7IH0gfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxxQkFBcUI7Q0FDdEI7QUFDRDtFQUNFLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxjQUFjO0NBQ2Y7QUFDRDtFQUNFLHlCQUF5QjtDQUMxQjtBQUNEO0VBQ0Usd0JBQXdCO0NBQ3pCO0FBQ0Q7RUFDRSwwQkFBMEI7Q0FDM0I7QUFDRDtFQUNFLGlCQUFpQjtDQUNsQjtBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCO0FBQ0Q7RUFDRSxpQkFBaUI7Q0FDbEI7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHlCQUF5QjtNQUNyQixzQkFBc0I7VUFDbEIsd0JBQXdCO0VBQ2hDLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0NBQzdCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QjtFQUNoQyw2QkFBNkI7RUFDN0IsOEJBQThCO01BQzFCLDJCQUEyQjtVQUN2Qix1QkFBdUI7Q0FDaEM7QUFDRDtFQUNFLGtCQUFrQjtDQUNuQjtBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCO0FBQ0Q7RUFDRSxtQkFBbUI7Q0FDcEI7QUFDRDtFQUNFLGlCQUFpQjtDQUNsQjtBQUNEO0VBQ0UsY0FBYztFQUNkLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxnREFBZ0Q7RUFDaEQsd0RBQXdEO0VBQ3hELGdEQUFnRDtFQUNoRCwyQ0FBMkM7RUFDM0Msd0NBQXdDO0VBQ3hDLDZFQUE2RTtDQUM5RTtBQUNELGNBQWMsVUFBVSxDQUFDO0FBQ3pCO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLGFBQWE7RUFDYiwrQ0FBK0M7VUFDdkMsdUNBQXVDO0VBQy9DLHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkU7Q0FDOUU7QUFDRDtFQUNFLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0NBQzdCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QjtDQUNqQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLHVCQUF1Qjs7Q0FFeEI7QUFDRDtFQUNFLHlCQUF5QjtDQUMxQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHVCQUF1QjtDQUN4QjtBQUNEO0VBQ0UsNEJBQTRCO0NBQzdCO0FBQ0Q7RUFDRSwyQkFBMkI7Q0FDNUI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsK0JBQStCO0VBQy9CLCtCQUErQjtNQUMzQixnQ0FBZ0M7VUFDNUIsNEJBQTRCO0NBQ3JDO0FBQ0Q7RUFDRSxzQkFBc0I7Q0FDdkI7QUFDRDtFQUNFLDBDQUFtQztFQUNuQyxhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjtBQUNEO0VBQ0UsZ0JBQWdCO0NBQ2pCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLG9CQUFvQjtFQUNwQixZQUFZO0NBQ2I7QUFDRDtFQUNFLFdBQVc7RUFDWCxZQUFZOztDQUViO0FBQ0Q7RUFDRSxlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Qsc0JBQXNCO0FBQ3RCO0VBQ0UsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFVBQVU7RUFDVixXQUFXO0NBQ1o7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0NBQ2I7QUFDRCwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7RUFDRDtJQUNFLFVBQVU7R0FDWDs7RUFFRDtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1osYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixvQkFBb0I7SUFDcEIsWUFBWTtHQUNiO0VBQ0Q7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVO0dBQ1g7O0NBRUY7QUFDRDtJQUNJLHFCQUFxQjtDQUN4QjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksY0FBYztDQUNqQjtBQUNEO0lBQ0kseUJBQXlCO0NBQzVCO0FBQ0Q7SUFDSSx3QkFBd0I7Q0FDM0I7QUFDRDtJQUNJLDBCQUEwQjtDQUM3QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsMEJBQTBCO1FBQ3RCLHVCQUF1QjtZQUNuQixvQkFBb0I7Q0FDL0I7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDZCQUE2QjtJQUM3Qiw4QkFBOEI7UUFDMUIsMkJBQTJCO1lBQ3ZCLHVCQUF1QjtDQUNsQztBQUNEO0lBQ0ksa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxnREFBZ0Q7SUFDaEQsd0RBQXdEO0lBQ3hELGdEQUFnRDtJQUNoRCwyQ0FBMkM7SUFDM0Msd0NBQXdDO0lBQ3hDLDZFQUE2RTtDQUNoRjtBQUNEOztJQUVJLE1BQU0sU0FBUyxDQUFDO0lBQ2hCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE9BQU8sVUFBVSxDQUFDO0NBQ3JCO0FBQ0Q7O0lBRUksTUFBTSxTQUFTLENBQUM7SUFDaEIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsT0FBTyxVQUFVLENBQUM7Q0FDckI7QUFDRDs7SUFFSSxNQUFNLFNBQVMsQ0FBQztJQUNoQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixPQUFPLFVBQVUsQ0FBQztDQUNyQjtBQUNEOztJQUVJLE1BQU0sU0FBUyxDQUFDO0lBQ2hCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE9BQU8sVUFBVSxDQUFDO0NBQ3JCO0FBQ0Q7O0lBRUksTUFBTSxTQUFTLENBQUM7SUFDaEIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsT0FBTyxVQUFVLENBQUM7Q0FDckI7QUFDRDs7SUFFSSxNQUFNLFNBQVMsQ0FBQztJQUNoQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixNQUFNLFVBQVUsQ0FBQztJQUNqQixPQUFPLFVBQVUsQ0FBQztDQUNyQjtBQUNEOztJQUVJLE1BQU0sU0FBUyxDQUFDO0lBQ2hCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE1BQU0sVUFBVSxDQUFDO0lBQ2pCLE9BQU8sVUFBVSxDQUFDO0NBQ3JCO0FBQ0Q7O0lBRUksTUFBTSxTQUFTLENBQUM7SUFDaEIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsTUFBTSxVQUFVLENBQUM7SUFDakIsT0FBTyxVQUFVLENBQUM7Q0FDckI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixVQUFVO0NBQ1g7QUFDRDtFQUNFLGdDQUFnQztDQUNqQztBQUNEO0VBQ0UsYUFBYTtFQUNiLHFDQUFxQztFQUNyQyxZQUFZO0VBQ1osa0RBQWtEO1VBQzFDLDBDQUEwQztDQUNuRDtBQUNEO0VBQ0UsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsZUFBZTtFQUNmLGFBQWE7RUFDYixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGtEQUFrRDtVQUMxQywwQ0FBMEM7Q0FDbkQ7QUFDRDtFQUNFLGlEQUFpRDtVQUN6Qyx5Q0FBeUM7Q0FDbEQ7QUFDRDtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkM7Q0FDcEQ7QUFDRDtFQUNFLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCLGFBQWE7Q0FDZDtBQUNEO0VBQ0Usa0JBQWtCO0NBQ25CO0FBQ0Q7RUFDRSxrQkFBa0I7Q0FDbkI7QUFDRDtFQUNFLGtCQUFrQjtDQUNuQjtBQUNEO0VBQ0Usa0JBQWtCO0NBQ25CO0FBQ0Q7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGtCQUFrQjtDQUNuQjtBQUNEO0VBQ0U7SUFDRSxnQkFBZ0I7R0FDakI7RUFDRDtJQUNFLGVBQWU7SUFDZixnQkFBZ0I7R0FDakI7Q0FDRjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxjQUFjO0lBQ2QsZUFBZTtJQUNmLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksYUFBYTtJQUNiLFNBQVM7SUFDVCxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLHlCQUF5QjtJQUN6QixvQkFBb0I7Q0FDdkI7QUFDRDtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixpREFBaUQ7WUFDekMseUNBQXlDO0lBQ2pELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsUUFBUTtJQUNSLFNBQVM7Q0FDWjtBQUNEO0lBQ0ksV0FBVztDQUNkO0FBQ0Q7SUFDSSxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLDREQUF3RTtJQUN4RSx1QkFBdUI7SUFDdkIsNEJBQTRCO0lBQzVCLG9DQUFvQzs7O0NBR3ZDO0FBQ0Q7SUFDSSwyREFBdUU7SUFDdkUsdUJBQXVCO0lBQ3ZCLDZCQUE2QjtJQUM3QixxQ0FBcUM7Q0FDeEM7QUFDRDtJQUNJLDJEQUFtRTtJQUNuRSx1QkFBdUI7SUFDdkIsNkJBQTZCO0lBQzdCLHFDQUFxQztDQUN4QztBQUNEO0lBQ0ksMkRBQW1FO0lBQ25FLHVCQUF1QjtJQUN2Qiw2QkFBNkI7SUFDN0IscUNBQXFDO0NBQ3hDO0FBQ0Q7OztJQUdJLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QsZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxtQkFBbUI7SUFDbkIsaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxnQkFBZ0I7Q0FDbkI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixnQkFBZ0I7Q0FDbkI7QUFDRDtJQUNJLGtEQUFrRDtZQUMxQywwQ0FBMEM7SUFDbEQsZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxnQkFBZ0I7Q0FDbkI7QUFDRDtJQUNJLGdCQUFnQjtDQUNuQjtBQUNEO0lBQ0ksZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxlQUFlO0lBQ2Ysa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsYUFBYTtJQUNiLFdBQVc7Q0FDZDtBQUNEO0lBQ0ksZUFBZTtDQUNsQjtBQUNEO0lBQ0ksZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsV0FBVztDQUNkO0FBQ0Q7SUFDSTtRQUNJLFdBQVc7UUFDWCxnQkFBZ0I7S0FDbkI7SUFDRDtRQUNJLGlCQUFpQjtLQUNwQjtJQUNEO1FBQ0ksZ0JBQWdCO0tBQ25CO0lBQ0Q7UUFDSSxnQkFBZ0I7S0FDbkI7SUFDRDtRQUNJLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsZUFBZTtLQUNsQjtJQUNEO1FBQ0ksZUFBZTtLQUNsQjtJQUNEO1FBQ0ksZUFBZTtLQUNsQjtJQUNEO1FBQ0ksaUJBQWlCO0tBQ3BCO0VBQ0g7SUFDRSxtQkFBbUI7S0FDbEIsZ0JBQWdCO0dBQ2xCO0NBQ0Y7QUFDRDtJQUNJO1FBQ0ksaUJBQWlCO0tBQ3BCO0NBQ0o7QUFDRDtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0RBQWtEO1VBQzFDLDBDQUEwQztFQUNsRCx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFLEVBQUU7QUFDakY7RUFDRSxXQUFXO0VBQ1gsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYTtFQUNiLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsK0JBQStCO0VBQy9CLDhCQUE4QjtNQUMxQix3QkFBd0I7VUFDcEIsb0JBQW9CO0VBQzVCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixjQUFjLEVBQUU7QUFDbEI7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsdUJBQXVCO1VBQ25CLG9CQUFvQixFQUFFO0FBQ2hDO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QseUJBQXlCO01BQ3JCLHNCQUFzQjtVQUNsQix3QkFBd0IsRUFBRTtBQUNwQztFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBRTtBQUNuQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtFQUN0QiwrQkFBK0I7RUFDL0IsK0JBQStCO01BQzNCLGdDQUFnQztVQUM1Qiw0QkFBNEIsRUFBRTtBQUN4QztFQUNFLHNCQUFzQixFQUFFO0FBQzFCO0VBQ0UsOERBQThEO0VBQzlELGFBQWE7RUFDYixpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsWUFBWSxFQUFFO0FBQ2hCO0VBQ0Usb0RBQW9EO1VBQzVDLDRDQUE0QztFQUNwRCxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLG9CQUFvQjtFQUNwQixhQUFhLEVBQUU7QUFDakI7RUFDRSxvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0IscUNBQXFDO0VBQ3JDLDRCQUE0QixFQUFFO0FBQ2hDO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3Qiw4QkFBOEIsRUFBRTtBQUNsQztFQUNFLGNBQWMsRUFBRTtBQUNsQjtFQUNFLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixnQ0FBZ0MsRUFBRTtBQUNwQztFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsWUFBWSxFQUFFO0FBQ2hCO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQixFQUFFO0FBQzFCO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2Isb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixZQUFZLEVBQUU7QUFDaEI7RUFDRSxXQUFXO0VBQ1gsWUFBWSxFQUFFO0FBQ2hCO0VBQ0UsbURBQW1EO1VBQzNDLDJDQUEyQyxFQUFFO0FBQ3ZEO0VBQ0UsbURBQW1EO1VBQzNDLDJDQUEyQyxFQUFFO0FBQ3ZEO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsaUNBQWlDLEVBQUU7QUFDckM7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBRTtBQUN0QixXQUFXO0FBQ1gsdURBQXVEO0FBQ3ZELEtBQUs7QUFDTCxXQUFXO0FBQ1gsdURBQXVEO0FBQ3ZELEtBQUs7QUFDTCwyQkFBMkI7QUFDM0IsMkRBQTJEO0FBQzNELEtBQUs7QUFDTCxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQix3REFBd0Q7VUFDaEQsZ0RBQWdEO0VBQ3hELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVksRUFBRTtBQUNoQiwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUIsRUFBRTtFQUNyQjtJQUNFLFVBQVUsRUFBRTtFQUNkO0lBQ0UsWUFBWTtJQUNaLFlBQVksRUFBRTtFQUNoQjtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGtCQUFrQixFQUFFO0VBQ3RCO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2Isb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixZQUFZLEVBQUU7RUFDaEI7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsd0RBQXdEO1lBQ2hELGdEQUFnRDtJQUN4RCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVLEVBQUU7RUFDZDtJQUNFLG9EQUFvRDtZQUM1Qyw0Q0FBNEM7SUFDcEQsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsYUFBYSxFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgb3V0bGluZTogbm9uZTtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4uc2VhcmNoLXRoZW1le1xcbiAgYmFja2dyb3VuZDogdXJsKFxcXCIuL2dlb21ldHJ5Mi5wbmdcXFwiKTtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5ib2R5IHtcXG4gICAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICAgIGNvbG9yOiB3aGl0ZTtcXG59XFxuLmJsYWNrIHtcXG4gICAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gICAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzYWQyY2JcXG59XFxuLndoaXRlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgICBmb250LXdlaWdodDogODAwO1xcbn1cXG4uZm9udC1saWdodCB7XFxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xcbiAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5uby1saXN0LXN0eWxlIHtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gICAgb3V0bGluZTogbm9uZTtcXG4gICAgYm9yZGVyOiBub25lO1xcbn1cXG4uYm9yZGVyLXJhZGl1cyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVcXG57XFxuICAgIDAlICAge29wYWNpdHk6MX1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMH1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDF9XFxufVxcbkBrZXlmcmFtZXMgZmFkZVxcbntcXG4gICAgMCUgICB7b3BhY2l0eToxfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAwfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMX1cXG59XFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGUyXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDF9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5Aa2V5ZnJhbWVzIGZhZGUyXFxue1xcbiAgICAwJSAgIHtvcGFjaXR5OjB9XFxuICAgIDI1JSB7IG9wYWNpdHk6IDF9XFxuICAgIDUwJSB7IG9wYWNpdHk6IDB9XFxuICAgIDc1JSB7IG9wYWNpdHk6IDB9XFxuICAgIDEwMCUgeyBvcGFjaXR5OiAwfVxcbn1cXG5ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZTNcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMX1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkBrZXlmcmFtZXMgZmFkZTNcXG57XFxuICAgIDAlICAge29wYWNpdHk6MH1cXG4gICAgMjUlIHsgb3BhY2l0eTogMH1cXG4gICAgNTAlIHsgb3BhY2l0eTogMX1cXG4gICAgNzUlIHsgb3BhY2l0eTogMH1cXG4gICAgMTAwJSB7IG9wYWNpdHk6IDB9XFxufVxcbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlNFxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAxfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuQGtleWZyYW1lcyBmYWRlNFxcbntcXG4gICAgMCUgICB7b3BhY2l0eTowfVxcbiAgICAyNSUgeyBvcGFjaXR5OiAwfVxcbiAgICA1MCUgeyBvcGFjaXR5OiAwfVxcbiAgICA3NSUgeyBvcGFjaXR5OiAxfVxcbiAgICAxMDAlIHsgb3BhY2l0eTogMH1cXG59XFxuLm1haW4tZm9ybSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDE0dmg7XFxufVxcbi5zZWFyY2gtYnV0dG9uIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtICFpbXBvcnRhbnQ7XFxufVxcbi5zZWFyY2gtc2VjdGlvbiwgLnN1Ym1pdC1ob21lLWxpbmsge1xcbiAgY29sb3I6IHdoaXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgcGFkZGluZzogMiU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG59XFxuLnNlY29uZC1wYXJ0IHtcXG4gIG1hcmdpbi10b3A6IDQlO1xcbn1cXG4uc3VibWl0LWhvbWUtbGluayB7XFxuICBtYXJnaW4tdG9wOiAyJTtcXG4gIHBhZGRpbmc6IDVweDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICB3aWR0aDogODAlO1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxufVxcbi5zZWFyY2gtYnV0dG9uOnZpc2l0ZWQge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbn1cXG4uc2VhcmNoLWJ1dHRvbjpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbn1cXG4uc2VhcmNoLWlucHV0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLnNlYXJjaC1pbnB1dDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcntcXG4gIHBhZGRpbmctcmlnaHQ6IDQlO1xcbn1cXG4uc2VhcmNoLWlucHV0Oi1tcy1pbnB1dC1wbGFjZWhvbGRlcntcXG4gIHBhZGRpbmctcmlnaHQ6IDQlO1xcbn1cXG4uc2VhcmNoLWlucHV0OjotbXMtaW5wdXQtcGxhY2Vob2xkZXJ7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTtcXG59XFxuLnNlYXJjaC1pbnB1dDo6cGxhY2Vob2xkZXJ7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTtcXG59XFxuc2VsZWN0IHtcXG4gIGhlaWdodDogNXZoO1xcbiAgY29sb3I6IGJsYWNrO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi5kZWFsLXR5cGUtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaW5wdXQtbGFiZWwge1xcbiAgICBtYXJnaW4tbGVmdDogMSU7XFxuICB9XFxuICAuc2VhcmNoLWJ1dHRvbiB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBtYXJnaW46IDUlIGF1dG87XFxuICB9XFxufVxcbmJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbn1cXG4ubG9nby1zZWN0aW9uIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNyU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLmxvZ28taW1hZ2Uge1xcbiAgICBtYXgtd2lkdGg6IDEyMHB4O1xcbn1cXG4uc2xpZGVyIHtcXG4gICAgaGVpZ2h0OiAxMDB2aDtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmhlYWRlci1tYWluIHtcXG4gICAgei1pbmRleDogOTk5O1xcbiAgICB0b3A6IDN2aDtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbn1cXG4uaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICAgIGJvcmRlcjogdGhpbiBzb2xpZCB3aGl0ZTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDA7XFxuICAgIHRvcDogNnZoO1xcbn1cXG4uc2xpZGVyLWNvbHVtbiB7XFxuICAgIHBhZGRpbmc6IDA7XFxufVxcbi5zbGlkZTEsLnNsaWRlMiwuc2xpZGUzLC5zbGlkZTQge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxufVxcbi5zbGlkZTEge1xcbiAgICBiYWNrZ3JvdW5kOiB1cmwobWljaGFsLWt1YmFsY3p5ay0yNjA5MDktdW5zcGxhc2guanBnKSBuby1yZXBlYXQgOTAlIDgwJTtcXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcXG4gICAgYW5pbWF0aW9uOmZhZGUgMTBzIGluZmluaXRlO1xcbiAgICAtd2Via2l0LWFuaW1hdGlvbjpmYWRlIDEwcyBpbmZpbml0ZTtcXG5cXG5cXG59XFxuLnNsaWRlMiB7XFxuICAgIGJhY2tncm91bmQ6IHVybChtYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XFxuICAgIGFuaW1hdGlvbjpmYWRlMiAxMHMgaW5maW5pdGU7XFxuICAgIC13ZWJraXQtYW5pbWF0aW9uOmZhZGUyIDEwcyBpbmZpbml0ZTtcXG59XFxuLnNsaWRlMyB7XFxuICAgIGJhY2tncm91bmQ6IHVybChjYXNleS1ob3JuZXItNTMzNTg2LXVuc3BsYXNoLmpwZykgbm8tcmVwZWF0IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcXG4gICAgYW5pbWF0aW9uOmZhZGUzIDEwcyBpbmZpbml0ZTtcXG4gICAgLXdlYmtpdC1hbmltYXRpb246ZmFkZTMgMTBzIGluZmluaXRlO1xcbn1cXG4uc2xpZGU0IHtcXG4gICAgYmFja2dyb3VuZDogdXJsKGx1a2UtdmFuLXp5bC01MDQwMzItdW5zcGxhc2guanBnKSBuby1yZXBlYXQgY2VudGVyO1xcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xcbiAgICBhbmltYXRpb246ZmFkZTQgMTBzIGluZmluaXRlO1xcbiAgICAtd2Via2l0LWFuaW1hdGlvbjpmYWRlNCAxMHMgaW5maW5pdGU7XFxufVxcbi5zdWJtaXQtaG91c2UtYnV0dG9uLFxcbi5zdWJtaXQtaG91c2UtYnV0dG9uOnZpc2l0ZWQsXFxuLnN1Ym1pdC1ob3VzZS1idXR0b246aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuLmZlYXR1cmVzLXNlY3Rpb24ge1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIG1hcmdpbi10b3A6IC01dmg7XFxufVxcbmlucHV0W3R5cGU9cmFkaW9dIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbn1cXG4uaW5wdXQtbGFiZWwge1xcbiAgICBmb250LXdlaWdodDogbGlnaHRlcjtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbn1cXG4uZmVhdHVyZS1ib3gge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICBtYXJnaW4tbGVmdDogMiU7XFxuICAgIGhlaWdodDogMzZ2aDtcXG4gICAgd2lkdGg6IDI5JSAhaW1wb3J0YW50O1xcbiAgICBtaW4taGVpZ2h0OiA0MnZoO1xcbn1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgyKSB7XFxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcXG59XFxuLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgICBtYXJnaW4tbGVmdDogNSU7XFxufVxcbi5mZWF0dXJlLWJveCA+IGRpdiA+IGltZyB7XFxuICAgIG1heC13aWR0aDogNzBweDtcXG59XFxuLndoeS11cyB7XFxuICAgIG1hcmdpbi10b3A6IDYlO1xcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcXG59XFxuLndoeS11cy1pbWcge1xcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbn1cXG4ud2h5LWxpc3Qge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgY29sb3I6IGJsYWNrO1xcbiAgICBwYWRkaW5nOiAwO1xcbn1cXG4ud2h5LWxpc3QgPiBsaSB7XFxuICAgIG1hcmdpbi10b3A6IDMlO1xcbn1cXG4ud2h5LWxpc3QgPiBsaSA+IGkge1xcbiAgICBtYXJnaW4tbGVmdDogMiU7XFxufVxcbi53aHktdXMtaW1nLWNvbHVtbiB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4ucmVhc29ucy1zZWN0aW9uIHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICAgIGRpcmVjdGlvbjogcnRsO1xcbiAgICBwYWRkaW5nOiAwO1xcbn1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gICAgLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgICAgICAgd2lkdGg6IDQwJTtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxJTtcXG4gICAgfVxcbiAgICAubW9iaWxlLWhlYWRlciB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgICB9XFxuICAgIC5sb2dvLWltYWdlIHtcXG4gICAgICAgIG1heC13aWR0aDogOTBweDtcXG4gICAgfVxcbiAgICAubWFpbi10aXRsZSB7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIH1cXG4gICAgLmZlYXR1cmUtYm94IHtcXG4gICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1JTtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICB9XFxuICAgIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDIpIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICB9XFxuICAgIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDMpIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICB9XFxuICAgIC53aHktdXMtaW1nIHtcXG4gICAgICAgIG1heC13aWR0aDogMjQ3cHg7XFxuICAgIH1cXG4gIC5mZWF0dXJlcy1zZWN0aW9uIHtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICAgbWFyZ2luLXRvcDogMnZoO1xcbiAgfVxcbn1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gICAgLm1vYmlsZS1oZWFkZXIge1xcbiAgICAgICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gICAgfVxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgLypiYWNrZ3JvdW5kOiB1cmwoXFxcIi4uL2Fzc2V0cy9pbWFnZXMvcGF0dGVybi9nZW9tZXRyeTIucG5nXFxcIik7Ki9cXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLXRhZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNjAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XFxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiA2dmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLnByaWNlLWRpdiB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDg1JTtcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbi5tYXJnaW4tMSB7XFxuICBtYXJnaW4tbGVmdDogMyU7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuLmltZy1ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMHB4OyB9XFxuLm1haW4tZm9ybS1zZWFyY2gtcmVzdWx0IHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIG1hcmdpbjogYXV0bztcXG4gIG1hcmdpbi10b3A6IC03JTtcXG4gIG1hcmdpbi1ib3R0b206IDYlOyB9XFxuLyojaW1nLTF7Ki9cXG4vKmJhY2tncm91bmQtaW1hZ2U6IHVybCguLi9hc3NldHMvaW1hZ2VzL3ZpbGxhMS5qcGcpOyovXFxuLyp9Ki9cXG4vKiNpbWctMnsqL1xcbi8qYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2Fzc2V0cy9pbWFnZXMvdmlsbGEyLmpwZyk7Ki9cXG4vKn0qL1xcbi8qI2ltZy0zLCAjaW1nLTQsICNpbWctNXsqL1xcbi8qYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2Fzc2V0cy9pbWFnZXMvdW5kZXJ3YXRlci5qcGcpOyovXFxuLyp9Ki9cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7IH1cXG4uZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTsgfVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlOyB9XFxuICAubWFyLWxlZnQge1xcbiAgICBtYXJnaW46IDA7IH1cXG4gIC5pY29ucy1jbCB7XFxuICAgIHdpZHRoOiAxM3Z3O1xcbiAgICBoZWlnaHQ6IDd2aDsgfVxcbiAgLmZvb3Rlci10ZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDEycHg7IH1cXG4gIC5pY29ucy1tYXIge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICBtYXJnaW4tYm90dG9tOiA1JTsgfVxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICAgIHBhZGRpbmc6IDMlOyB9XFxuICAuZHJvcGRvd24tY29udGVudCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gICAgbWluLXdpZHRoOiAyMDBweDtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgICB6LWluZGV4OiAxO1xcbiAgICBsZWZ0OiAxMnZ3O1xcbiAgICB0b3A6IDEwdmg7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgIGhlaWdodDogMzUwcHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDV2aDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICBmbG9hdDogcmlnaHQ7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJAY2hhcnNldCBcXFwiVVRGLThcXFwiO1xcbi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG46cm9vdCB7XFxuICAvKlxcbiAgICogVHlwb2dyYXBoeVxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLWZvbnQtZmFtaWx5LWJhc2U6ICdTZWdvZSBVSScsICdIZWx2ZXRpY2FOZXVlLUxpZ2h0Jywgc2Fucy1zZXJpZjtcXG5cXG4gIC8qXFxuICAgKiBMYXlvdXRcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1tYXgtY29udGVudC13aWR0aDogMTAwMHB4O1xcblxcbiAgLypcXG4gICAqIE1lZGlhIHF1ZXJpZXMgYnJlYWtwb2ludHNcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1zY3JlZW4teHMtbWluOiA0ODBweDsgIC8qIEV4dHJhIHNtYWxsIHNjcmVlbiAvIHBob25lICovXFxuICAtLXNjcmVlbi1zbS1taW46IDc2OHB4OyAgLyogU21hbGwgc2NyZWVuIC8gdGFibGV0ICovXFxuICAtLXNjcmVlbi1tZC1taW46IDk5MnB4OyAgLyogTWVkaXVtIHNjcmVlbiAvIGRlc2t0b3AgKi9cXG4gIC0tc2NyZWVuLWxnLW1pbjogMTIwMHB4OyAvKiBMYXJnZSBzY3JlZW4gLyB3aWRlIGRlc2t0b3AgKi9cXG59XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0ud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7IH1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA3MDA7IH1cXG4vKlxcbiAqIG5vcm1hbGl6ZS5jc3MgaXMgaW1wb3J0ZWQgaW4gSlMgZmlsZS5cXG4gKiBJZiB5b3UgaW1wb3J0IGV4dGVybmFsIENTUyBmaWxlIGZyb20geW91ciBpbnRlcm5hbCBDU1NcXG4gKiB0aGVuIGl0IHdpbGwgYmUgaW5saW5lZCBhbmQgcHJvY2Vzc2VkIHdpdGggQ1NTIG1vZHVsZXMuXFxuICovXFxuLypcXG4gKiBCYXNlIHN0eWxlc1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuaHRtbCB7XFxuICBjb2xvcjogIzIyMjtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIH4xNnB4OyAqL1xcbiAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJhc2UpO1xcbiAgbGluZS1oZWlnaHQ6IDEuMzc1O1xcbiAgLyogfjIycHggKi8gfVxcbmJvZHkge1xcbiAgbWFyZ2luOiAwO1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07IH1cXG5hIHtcXG4gIGNvbG9yOiAjMDA3NGMyOyB9XFxuLypcXG4gKiBSZW1vdmUgdGV4dC1zaGFkb3cgaW4gc2VsZWN0aW9uIGhpZ2hsaWdodDpcXG4gKiBodHRwczovL3R3aXR0ZXIuY29tL21pa2V0YXlsci9zdGF0dXMvMTIyMjg4MDUzMDFcXG4gKlxcbiAqIFRoZXNlIHNlbGVjdGlvbiBydWxlIHNldHMgaGF2ZSB0byBiZSBzZXBhcmF0ZS5cXG4gKiBDdXN0b21pemUgdGhlIGJhY2tncm91bmQgY29sb3IgdG8gbWF0Y2ggeW91ciBkZXNpZ24uXFxuICovXFxuOjotbW96LXNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7IH1cXG46OnNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7IH1cXG4vKlxcbiAqIEEgYmV0dGVyIGxvb2tpbmcgZGVmYXVsdCBob3Jpem9udGFsIHJ1bGVcXG4gKi9cXG5ociB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGhlaWdodDogMXB4O1xcbiAgYm9yZGVyOiAwO1xcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XFxuICBtYXJnaW46IDFlbSAwO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qXFxuICogUmVtb3ZlIHRoZSBnYXAgYmV0d2VlbiBhdWRpbywgY2FudmFzLCBpZnJhbWVzLFxcbiAqIGltYWdlcywgdmlkZW9zIGFuZCB0aGUgYm90dG9tIG9mIHRoZWlyIGNvbnRhaW5lcnM6XFxuICogaHR0cHM6Ly9naXRodWIuY29tL2g1YnAvaHRtbDUtYm9pbGVycGxhdGUvaXNzdWVzLzQ0MFxcbiAqL1xcbmF1ZGlvLFxcbmNhbnZhcyxcXG5pZnJhbWUsXFxuaW1nLFxcbnN2ZyxcXG52aWRlbyB7XFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyB9XFxuLypcXG4gKiBSZW1vdmUgZGVmYXVsdCBmaWVsZHNldCBzdHlsZXMuXFxuICovXFxuZmllbGRzZXQge1xcbiAgYm9yZGVyOiAwO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qXFxuICogQWxsb3cgb25seSB2ZXJ0aWNhbCByZXNpemluZyBvZiB0ZXh0YXJlYXMuXFxuICovXFxudGV4dGFyZWEge1xcbiAgcmVzaXplOiB2ZXJ0aWNhbDsgfVxcbi8qXFxuICogQnJvd3NlciB1cGdyYWRlIHByb21wdFxcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLmJyb3dzZXJ1cGdyYWRlIHtcXG4gIG1hcmdpbjogMC4yZW0gMDtcXG4gIGJhY2tncm91bmQ6ICNjY2M7XFxuICBjb2xvcjogIzAwMDtcXG4gIHBhZGRpbmc6IDAuMmVtIDA7IH1cXG4vKlxcbiAqIFByaW50IHN0eWxlc1xcbiAqIElubGluZWQgdG8gYXZvaWQgdGhlIGFkZGl0aW9uYWwgSFRUUCByZXF1ZXN0OlxcbiAqIGh0dHA6Ly93d3cucGhwaWVkLmNvbS9kZWxheS1sb2FkaW5nLXlvdXItcHJpbnQtY3NzL1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuQG1lZGlhIHByaW50IHtcXG4gICosXFxuICAqOjpiZWZvcmUsXFxuICAqOjphZnRlciB7XFxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XFxuICAgIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XFxuICAgIC8qIEJsYWNrIHByaW50cyBmYXN0ZXI6IGh0dHA6Ly93d3cuc2FuYmVpamkuY29tL2FyY2hpdmVzLzk1MyAqL1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIHRleHQtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7IH1cXG4gIGEsXFxuICBhOnZpc2l0ZWQge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTsgfVxcbiAgYVtocmVmXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiBcXFwiIChcXFwiIGF0dHIoaHJlZikgXFxcIilcXFwiOyB9XFxuICBhYmJyW3RpdGxlXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiBcXFwiIChcXFwiIGF0dHIodGl0bGUpIFxcXCIpXFxcIjsgfVxcbiAgLypcXG4gICAqIERvbid0IHNob3cgbGlua3MgdGhhdCBhcmUgZnJhZ21lbnQgaWRlbnRpZmllcnMsXFxuICAgKiBvciB1c2UgdGhlIGBqYXZhc2NyaXB0OmAgcHNldWRvIHByb3RvY29sXFxuICAgKi9cXG4gIGFbaHJlZl49JyMnXTo6YWZ0ZXIsXFxuICBhW2hyZWZePSdqYXZhc2NyaXB0OiddOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6ICcnOyB9XFxuICBwcmUsXFxuICBibG9ja3F1b3RlIHtcXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICAvKlxcbiAgICogUHJpbnRpbmcgVGFibGVzOlxcbiAgICogaHR0cDovL2Nzcy1kaXNjdXNzLmluY3V0aW8uY29tL3dpa2kvUHJpbnRpbmdfVGFibGVzXFxuICAgKi9cXG4gIHRoZWFkIHtcXG4gICAgZGlzcGxheTogdGFibGUtaGVhZGVyLWdyb3VwOyB9XFxuICB0cixcXG4gIGltZyB7XFxuICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdm9pZDsgfVxcbiAgaW1nIHtcXG4gICAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7IH1cXG4gIHAsXFxuICBoMixcXG4gIGgzIHtcXG4gICAgb3JwaGFuczogMztcXG4gICAgd2lkb3dzOiAzOyB9XFxuICBoMixcXG4gIGgzIHtcXG4gICAgcGFnZS1icmVhay1hZnRlcjogYXZvaWQ7IH0gfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBLGlCQUFpQjtBQUNqQjs7Ozs7OztHQU9HO0FBQ0g7Ozs7Ozs7R0FPRztBQUNIO0VBQ0U7O2dGQUU4RTs7RUFFOUUsa0VBQWtFOztFQUVsRTs7Z0ZBRThFOztFQUU5RSw0QkFBNEI7O0VBRTVCOztnRkFFOEU7O0VBRTlFLHVCQUF1QixFQUFFLGdDQUFnQztFQUN6RCx1QkFBdUIsRUFBRSwyQkFBMkI7RUFDcEQsdUJBQXVCLEVBQUUsNkJBQTZCO0VBQ3RELHdCQUF3QixDQUFDLGlDQUFpQztDQUMzRDtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLG1DQUFtQztFQUNuQyx1S0FBdUs7RUFDdkssaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxxQkFBcUI7RUFDckIsNkJBQTZCO0VBQzdCLHFKQUFxSjtFQUNySixpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLHFCQUFxQjtFQUNyQixrQ0FBa0M7RUFDbEMsb0tBQW9LO0VBQ3BLLGlCQUFpQixFQUFFO0FBQ3JCOzs7O0dBSUc7QUFDSDs7Z0ZBRWdGO0FBQ2hGO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsWUFBWTtFQUNaLHFDQUFxQztFQUNyQyxtQkFBbUI7RUFDbkIsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxVQUFVO0VBQ1YscUJBQXFCLEVBQUU7QUFDekI7RUFDRSxlQUFlLEVBQUU7QUFDbkI7Ozs7OztHQU1HO0FBQ0g7RUFDRSxvQkFBb0I7RUFDcEIsa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxvQkFBb0I7RUFDcEIsa0JBQWtCLEVBQUU7QUFDdEI7O0dBRUc7QUFDSDtFQUNFLGVBQWU7RUFDZixZQUFZO0VBQ1osVUFBVTtFQUNWLDJCQUEyQjtFQUMzQixjQUFjO0VBQ2QsV0FBVyxFQUFFO0FBQ2Y7Ozs7R0FJRztBQUNIOzs7Ozs7RUFNRSx1QkFBdUIsRUFBRTtBQUMzQjs7R0FFRztBQUNIO0VBQ0UsVUFBVTtFQUNWLFVBQVU7RUFDVixXQUFXLEVBQUU7QUFDZjs7R0FFRztBQUNIO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7O2dGQUVnRjtBQUNoRjtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGlCQUFpQixFQUFFO0FBQ3JCOzs7O2dGQUlnRjtBQUNoRjtFQUNFOzs7SUFHRSxtQ0FBbUM7SUFDbkMsdUJBQXVCO0lBQ3ZCLCtEQUErRDtJQUMvRCxvQ0FBb0M7WUFDNUIsNEJBQTRCO0lBQ3BDLDZCQUE2QixFQUFFO0VBQ2pDOztJQUVFLDJCQUEyQixFQUFFO0VBQy9CO0lBQ0UsNkJBQTZCLEVBQUU7RUFDakM7SUFDRSw4QkFBOEIsRUFBRTtFQUNsQzs7O0tBR0c7RUFDSDs7SUFFRSxZQUFZLEVBQUU7RUFDaEI7O0lBRUUsdUJBQXVCO0lBQ3ZCLHlCQUF5QixFQUFFO0VBQzdCOzs7S0FHRztFQUNIO0lBQ0UsNEJBQTRCLEVBQUU7RUFDaEM7O0lBRUUseUJBQXlCLEVBQUU7RUFDN0I7SUFDRSwyQkFBMkIsRUFBRTtFQUMvQjs7O0lBR0UsV0FBVztJQUNYLFVBQVUsRUFBRTtFQUNkOztJQUVFLHdCQUF3QixFQUFFLEVBQUVcIixcImZpbGVcIjpcIkxheW91dC5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiQGNoYXJzZXQgXFxcIlVURi04XFxcIjtcXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuOnJvb3Qge1xcbiAgLypcXG4gICAqIFR5cG9ncmFwaHlcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1mb250LWZhbWlseS1iYXNlOiAnU2Vnb2UgVUknLCAnSGVsdmV0aWNhTmV1ZS1MaWdodCcsIHNhbnMtc2VyaWY7XFxuXFxuICAvKlxcbiAgICogTGF5b3V0XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tbWF4LWNvbnRlbnQtd2lkdGg6IDEwMDBweDtcXG5cXG4gIC8qXFxuICAgKiBNZWRpYSBxdWVyaWVzIGJyZWFrcG9pbnRzXFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tc2NyZWVuLXhzLW1pbjogNDgwcHg7ICAvKiBFeHRyYSBzbWFsbCBzY3JlZW4gLyBwaG9uZSAqL1xcbiAgLS1zY3JlZW4tc20tbWluOiA3NjhweDsgIC8qIFNtYWxsIHNjcmVlbiAvIHRhYmxldCAqL1xcbiAgLS1zY3JlZW4tbWQtbWluOiA5OTJweDsgIC8qIE1lZGl1bSBzY3JlZW4gLyBkZXNrdG9wICovXFxuICAtLXNjcmVlbi1sZy1taW46IDEyMDBweDsgLyogTGFyZ2Ugc2NyZWVuIC8gd2lkZSBkZXNrdG9wICovXFxufVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7IH1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0udHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNjAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNzAwOyB9XFxuLypcXG4gKiBub3JtYWxpemUuY3NzIGlzIGltcG9ydGVkIGluIEpTIGZpbGUuXFxuICogSWYgeW91IGltcG9ydCBleHRlcm5hbCBDU1MgZmlsZSBmcm9tIHlvdXIgaW50ZXJuYWwgQ1NTXFxuICogdGhlbiBpdCB3aWxsIGJlIGlubGluZWQgYW5kIHByb2Nlc3NlZCB3aXRoIENTUyBtb2R1bGVzLlxcbiAqL1xcbi8qXFxuICogQmFzZSBzdHlsZXNcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbmh0bWwge1xcbiAgY29sb3I6ICMyMjI7XFxuICBmb250LXdlaWdodDogMTAwO1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiB+MTZweDsgKi9cXG4gIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1iYXNlKTtcXG4gIGxpbmUtaGVpZ2h0OiAxLjM3NTtcXG4gIC8qIH4yMnB4ICovIH1cXG5ib2R5IHtcXG4gIG1hcmdpbjogMDtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtOyB9XFxuYSB7XFxuICBjb2xvcjogIzAwNzRjMjsgfVxcbi8qXFxuICogUmVtb3ZlIHRleHQtc2hhZG93IGluIHNlbGVjdGlvbiBoaWdobGlnaHQ6XFxuICogaHR0cHM6Ly90d2l0dGVyLmNvbS9taWtldGF5bHIvc3RhdHVzLzEyMjI4ODA1MzAxXFxuICpcXG4gKiBUaGVzZSBzZWxlY3Rpb24gcnVsZSBzZXRzIGhhdmUgdG8gYmUgc2VwYXJhdGUuXFxuICogQ3VzdG9taXplIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIHRvIG1hdGNoIHlvdXIgZGVzaWduLlxcbiAqL1xcbjo6LW1vei1zZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lOyB9XFxuOjpzZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lOyB9XFxuLypcXG4gKiBBIGJldHRlciBsb29raW5nIGRlZmF1bHQgaG9yaXpvbnRhbCBydWxlXFxuICovXFxuaHIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDFweDtcXG4gIGJvcmRlcjogMDtcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xcbiAgbWFyZ2luOiAxZW0gMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKlxcbiAqIFJlbW92ZSB0aGUgZ2FwIGJldHdlZW4gYXVkaW8sIGNhbnZhcywgaWZyYW1lcyxcXG4gKiBpbWFnZXMsIHZpZGVvcyBhbmQgdGhlIGJvdHRvbSBvZiB0aGVpciBjb250YWluZXJzOlxcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9oNWJwL2h0bWw1LWJvaWxlcnBsYXRlL2lzc3Vlcy80NDBcXG4gKi9cXG5hdWRpbyxcXG5jYW52YXMsXFxuaWZyYW1lLFxcbmltZyxcXG5zdmcsXFxudmlkZW8ge1xcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTsgfVxcbi8qXFxuICogUmVtb3ZlIGRlZmF1bHQgZmllbGRzZXQgc3R5bGVzLlxcbiAqL1xcbmZpZWxkc2V0IHtcXG4gIGJvcmRlcjogMDtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKlxcbiAqIEFsbG93IG9ubHkgdmVydGljYWwgcmVzaXppbmcgb2YgdGV4dGFyZWFzLlxcbiAqL1xcbnRleHRhcmVhIHtcXG4gIHJlc2l6ZTogdmVydGljYWw7IH1cXG4vKlxcbiAqIEJyb3dzZXIgdXBncmFkZSBwcm9tcHRcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbjpnbG9iYWwoLmJyb3dzZXJ1cGdyYWRlKSB7XFxuICBtYXJnaW46IDAuMmVtIDA7XFxuICBiYWNrZ3JvdW5kOiAjY2NjO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAwLjJlbSAwOyB9XFxuLypcXG4gKiBQcmludCBzdHlsZXNcXG4gKiBJbmxpbmVkIHRvIGF2b2lkIHRoZSBhZGRpdGlvbmFsIEhUVFAgcmVxdWVzdDpcXG4gKiBodHRwOi8vd3d3LnBocGllZC5jb20vZGVsYXktbG9hZGluZy15b3VyLXByaW50LWNzcy9cXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbkBtZWRpYSBwcmludCB7XFxuICAqLFxcbiAgKjo6YmVmb3JlLFxcbiAgKjo6YWZ0ZXIge1xcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xcbiAgICAvKiBCbGFjayBwcmludHMgZmFzdGVyOiBodHRwOi8vd3d3LnNhbmJlaWppLmNvbS9hcmNoaXZlcy85NTMgKi9cXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICB0ZXh0LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50OyB9XFxuICBhLFxcbiAgYTp2aXNpdGVkIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7IH1cXG4gIGFbaHJlZl06OmFmdGVyIHtcXG4gICAgY29udGVudDogXFxcIiAoXFxcIiBhdHRyKGhyZWYpIFxcXCIpXFxcIjsgfVxcbiAgYWJiclt0aXRsZV06OmFmdGVyIHtcXG4gICAgY29udGVudDogXFxcIiAoXFxcIiBhdHRyKHRpdGxlKSBcXFwiKVxcXCI7IH1cXG4gIC8qXFxuICAgKiBEb24ndCBzaG93IGxpbmtzIHRoYXQgYXJlIGZyYWdtZW50IGlkZW50aWZpZXJzLFxcbiAgICogb3IgdXNlIHRoZSBgamF2YXNjcmlwdDpgIHBzZXVkbyBwcm90b2NvbFxcbiAgICovXFxuICBhW2hyZWZePScjJ106OmFmdGVyLFxcbiAgYVtocmVmXj0namF2YXNjcmlwdDonXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiAnJzsgfVxcbiAgcHJlLFxcbiAgYmxvY2txdW90ZSB7XFxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM5OTk7XFxuICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdm9pZDsgfVxcbiAgLypcXG4gICAqIFByaW50aW5nIFRhYmxlczpcXG4gICAqIGh0dHA6Ly9jc3MtZGlzY3Vzcy5pbmN1dGlvLmNvbS93aWtpL1ByaW50aW5nX1RhYmxlc1xcbiAgICovXFxuICB0aGVhZCB7XFxuICAgIGRpc3BsYXk6IHRhYmxlLWhlYWRlci1ncm91cDsgfVxcbiAgdHIsXFxuICBpbWcge1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIGltZyB7XFxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50OyB9XFxuICBwLFxcbiAgaDIsXFxuICBoMyB7XFxuICAgIG9ycGhhbnM6IDM7XFxuICAgIHdpZG93czogMzsgfVxcbiAgaDIsXFxuICBoMyB7XFxuICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkOyB9IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtJQUNJLHFCQUFxQjtDQUN4QjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksY0FBYztDQUNqQjtBQUNEO0lBQ0kseUJBQXlCO0NBQzVCO0FBQ0Q7SUFDSSx3QkFBd0I7Q0FDM0I7QUFDRDtJQUNJLDBCQUEwQjtDQUM3QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsMEJBQTBCO1FBQ3RCLHVCQUF1QjtZQUNuQixvQkFBb0I7Q0FDL0I7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDZCQUE2QjtJQUM3Qiw4QkFBOEI7UUFDMUIsMkJBQTJCO1lBQ3ZCLHVCQUF1QjtDQUNsQztBQUNEO0lBQ0ksa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxnREFBZ0Q7SUFDaEQsd0RBQXdEO0lBQ3hELGdEQUFnRDtJQUNoRCwyQ0FBMkM7SUFDM0Msd0NBQXdDO0lBQ3hDLDZFQUE2RTtDQUNoRjtBQUNELGNBQWMsVUFBVSxDQUFDO0FBQ3pCO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLGFBQWE7RUFDYiwrQ0FBK0M7VUFDdkMsdUNBQXVDO0VBQy9DLHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkU7Q0FDOUU7QUFDRDtFQUNFLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0NBQzdCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QjtDQUNqQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLHVCQUF1Qjs7Q0FFeEI7QUFDRDtFQUNFLHlCQUF5QjtDQUMxQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHVCQUF1QjtDQUN4QjtBQUNEO0VBQ0UsNEJBQTRCO0NBQzdCO0FBQ0Q7RUFDRSwyQkFBMkI7Q0FDNUI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsK0JBQStCO0VBQy9CLCtCQUErQjtNQUMzQixnQ0FBZ0M7VUFDNUIsNEJBQTRCO0NBQ3JDO0FBQ0Q7RUFDRSxzQkFBc0I7Q0FDdkI7QUFDRDtFQUNFLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsWUFBWTtDQUNiO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxnQkFBZ0I7Q0FDakI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYiw0QkFBNEI7RUFDNUIsb0JBQW9CO0VBQ3BCLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsV0FBVztFQUNYLFlBQVk7O0NBRWI7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRDtFQUNFLGdEQUFnRDtVQUN4Qyx3Q0FBd0M7Q0FDakQ7QUFDRCxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUI7Q0FDcEI7QUFDRDtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVc7Q0FDWjtBQUNEO0VBQ0UsZUFBZTtDQUNoQjtBQUNEO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVk7Q0FDYjtBQUNELDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjtFQUNEO0lBQ0UsVUFBVTtHQUNYOztFQUVEO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLG9CQUFvQjtJQUNwQixZQUFZO0dBQ2I7RUFDRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixxREFBcUQ7WUFDN0MsNkNBQTZDO0lBQ3JELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7R0FDWDs7Q0FFRjtBQUNEO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7RUFDYixrREFBa0Q7VUFDMUMsMENBQTBDO0VBQ2xELHdEQUF3RDtFQUN4RCxnREFBZ0Q7RUFDaEQsMkNBQTJDO0VBQzNDLHdDQUF3QztFQUN4Qyw2RUFBNkUsRUFBRTtBQUNqRjtFQUNFLFdBQVc7RUFDWCxhQUFhLEVBQUU7QUFDakI7RUFDRSxhQUFhO0VBQ2Isb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxhQUFhLEVBQUU7QUFDakI7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwrQkFBK0I7RUFDL0IsOEJBQThCO01BQzFCLHdCQUF3QjtVQUNwQixvQkFBb0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYix3QkFBd0I7RUFDeEIsb0RBQW9EO1VBQzVDLDRDQUE0QztFQUNwRCxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGNBQWMsRUFBRTtBQUNsQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDBCQUEwQjtNQUN0Qix1QkFBdUI7VUFDbkIsb0JBQW9CLEVBQUU7QUFDaEM7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx5QkFBeUI7TUFDckIsc0JBQXNCO1VBQ2xCLHdCQUF3QixFQUFFO0FBQ3BDO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLG1CQUFtQjtFQUNuQixlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFFO0FBQ25CO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLCtCQUErQjtFQUMvQiwrQkFBK0I7TUFDM0IsZ0NBQWdDO1VBQzVCLDRCQUE0QixFQUFFO0FBQ3hDO0VBQ0Usc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxXQUFXLEVBQUU7QUFDZjtFQUNFLFlBQVksRUFBRTtBQUNoQjtFQUNFLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsYUFBYSxFQUFFO0FBQ2pCO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLHFDQUFxQztFQUNyQyw0QkFBNEIsRUFBRTtBQUNoQztFQUNFLGNBQWMsRUFBRTtBQUNsQjtFQUNFLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixnQ0FBZ0MsRUFBRTtBQUNwQztFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixzQkFBc0IsRUFBRTtBQUMxQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFvQjtFQUNwQixvQkFBb0I7RUFDcEIsWUFBWSxFQUFFO0FBQ2hCO0VBQ0UsV0FBVztFQUNYLFlBQVksRUFBRTtBQUNoQjtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkMsRUFBRTtBQUN2RDtFQUNFLG1EQUFtRDtVQUMzQywyQ0FBMkMsRUFBRTtBQUN2RDtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsaUNBQWlDLEVBQUU7QUFDckM7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGdDQUFnQyxFQUFFO0FBQ3BDLHNCQUFzQjtBQUN0QjtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLHdEQUF3RDtVQUNoRCxnREFBZ0Q7RUFDeEQsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxVQUFVO0VBQ1YsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxzQkFBc0I7RUFDdEIsWUFBWSxFQUFFO0FBQ2hCLDBDQUEwQztBQUMxQztFQUNFO0lBQ0UsbUJBQW1CO0lBQ25CLGlCQUFpQixFQUFFO0VBQ3JCO0lBQ0UsVUFBVSxFQUFFO0VBQ2Q7SUFDRSxZQUFZO0lBQ1osWUFBWSxFQUFFO0VBQ2hCO0lBQ0UsbUJBQW1CO0lBQ25CLGdCQUFnQixFQUFFO0VBQ3BCO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0Usa0JBQWtCLEVBQUU7RUFDdEI7SUFDRSxZQUFZO0lBQ1osWUFBWTtJQUNaLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLFlBQVksRUFBRTtFQUNoQjtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQix3REFBd0Q7WUFDaEQsZ0RBQWdEO0lBQ3hELG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVUsRUFBRTtFQUNkO0lBQ0Usb0RBQW9EO1lBQzVDLDRDQUE0QztJQUNwRCxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixhQUFhLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvbjpmb2N1cyB7b3V0bGluZTowO31cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXXtcXG4gIG91dGxpbmU6bm9uZTtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLDAsMCwwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4uZmxleC1jZW50ZXJ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmZsZXgtbWlkZGxle1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuLnB1cnBsZS1mb250e1xcbiAgY29sb3I6IHJnYig5MSw4NSwxNTUpO1xcbn1cXG4uYmx1ZS1mb250e1xcbiAgY29sb3I6IHJnYig2OCwyMDksMjAyKTtcXG59XFxuLmdyZXktZm9udHtcXG4gIGNvbG9yOnJnYigxNTAsMTUwLDE1MCk7XFxuXFxufVxcbi5saWdodC1ncmV5LWZvbnR7XFxuICBjb2xvcjogIHJnYigxNDgsMTQ4LDE0OCk7XFxufVxcbi5ibGFjay1mb250e1xcbiAgY29sb3I6IGJsYWNrO1xcbn1cXG4ud2hpdGUtZm9udHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG59XFxuLnBpbmstZm9udHtcXG4gIGNvbG9yOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5waW5rLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoMjQwLDkzLDEwOCk7XFxufVxcbi5wdXJwbGUtYmFja2dyb3VuZHtcXG4gIGJhY2tncm91bmQ6IHJnYig5MCw4NSwxNTUpO1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXJ7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHR7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbn1cXG4ubWFyZ2luLWF1dG97XFxuICBtYXJnaW46IGF1dG87XFxufVxcbi5uby1wYWRkaW5ne1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmljb24tY2x7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dztcXG59XFxuLmZsZXgtcm93LXJldntcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiByZXZlcnNlO1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG59XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcbi5tYXItNXtcXG4gIG1hcmdpbjogNSU7XFxufVxcbi5wYWRkLTV7XFxuICBwYWRkaW5nOiA1JTtcXG59XFxuLm1hci1sZWZ0e1xcbiAgbWFyZ2luLWxlZnQ6IDUuNXZ3O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbn1cXG4ubWFyLXRvcHtcXG4gIG1hcmdpbi10b3A6IDN2aDtcXG59XFxuLm1hci1ib3R0b217XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7XFxufVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG4uaWNvbnMtY2x7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7XFxuXFxufVxcbi5tYXItdG9wLTEwe1xcbiAgbWFyZ2luLXRvcDogMiU7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmhvdmVye1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwwLDAsMC4yKTtcXG59XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Zm9jdXN7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsMCwwLDAuMTUpO1xcbn1cXG4vKmRyb3Bkb3duIGNzcyBjb2RlcyovXFxuLmRyb3Bkb3duIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDtcXG59XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ucHJpY2UtaGVhZGluZyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo3NjhweCl7XFxuICAubWFpbi1mb3JtIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzQlO1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlO1xcbiAgfVxcbiAgLm1hci1sZWZ0e1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogcmdiKDY4LDIwOSwyMDIpO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTtcXG4gIH1cXG4gIC5kcm9wZG93bi1jb250ZW50e1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoO1xcbiAgfVxcblxcbn1cXG5idXR0b246Zm9jdXMge1xcbiAgb3V0bGluZTogMDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTsgfVxcbmZvb3RlciB7XFxuICBoZWlnaHQ6IDEzdmg7XFxuICBiYWNrZ3JvdW5kOiAjMWM1OTU2OyB9XFxuLm5hdi1sb2dvIHtcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5uYXYtY2wge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGhlaWdodDogMTB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDMwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgb3ZlcmZsb3c6IHZpc2libGU7XFxuICB3aWR0aDogMTAwJTtcXG4gIHotaW5kZXg6IDk5OTk7IH1cXG4uZmxleC1jZW50ZXIge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cXG4uZmxleC1taWRkbGUge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgfVxcbi5wdXJwbGUtZm9udCB7XFxuICBjb2xvcjogIzViNTU5YjsgfVxcbi5ibHVlLWZvbnQge1xcbiAgY29sb3I6ICM0NGQxY2E7IH1cXG4uZ3JleS1mb250IHtcXG4gIGNvbG9yOiAjOTY5Njk2OyB9XFxuLmxpZ2h0LWdyZXktZm9udCB7XFxuICBjb2xvcjogIzk0OTQ5NDsgfVxcbi5ibGFjay1mb250IHtcXG4gIGNvbG9yOiBibGFjazsgfVxcbi53aGl0ZS1mb250IHtcXG4gIGNvbG9yOiB3aGl0ZTsgfVxcbi5waW5rLWZvbnQge1xcbiAgY29sb3I6ICNmMDVkNmM7IH1cXG4ucGluay1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICNmMDVkNmM7IH1cXG4ucHVycGxlLWJhY2tncm91bmQge1xcbiAgYmFja2dyb3VuZDogIzVhNTU5YjsgfVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4ubWFyZ2luLWF1dG8ge1xcbiAgbWFyZ2luOiBhdXRvOyB9XFxuLm5vLXBhZGRpbmcge1xcbiAgcGFkZGluZzogMDsgfVxcbi5pY29uLWNsIHtcXG4gIG1heC1oZWlnaHQ6IDd2aDtcXG4gIG1heC13aWR0aDogN3Z3OyB9XFxuLmZsZXgtcm93LXJldiB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7IH1cXG4uZmxleC1yb3ctcmV2OmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxcbi5zZWFyY2gtdGhlbWUge1xcbiAgaGVpZ2h0OiAxNXZoO1xcbiAgbWFyZ2luLXRvcDogMTB2aDsgfVxcbi5tYXItNSB7XFxuICBtYXJnaW46IDUlOyB9XFxuLnBhZGQtNSB7XFxuICBwYWRkaW5nOiA1JTsgfVxcbi5ob3VzZS1jYXJkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICBtYXJnaW4tcmlnaHQ6IDUuNXZ3O1xcbiAgZmxvYXQ6IHJpZ2h0OyB9XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5ob3VzZS1pbWcge1xcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xcbiAgLyogbWF4LWhlaWdodDogMTAwJTsgKi9cXG4gIGhlaWdodDogMTAwJTtcXG4gIC8qIGhlaWdodDogOTclOyAqL1xcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIC8qIGJhY2tncm91bmQtb3JpZ2luOiBjb250ZW50LWJveDsgKi9cXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgfVxcbi5pbWctaGVpZ2h0IHtcXG4gIGhlaWdodDogMjAwcHg7IH1cXG4uYm9yZGVyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAydmg7XFxuICBtYXJnaW4tbGVmdDogMXZ3O1xcbiAgbWFyZ2luLXJpZ2h0OiAxdnc7XFxuICBtYXJnaW4tdG9wOiAydmg7XFxuICBib3JkZXItYm90dG9tOiB0aGluIHNvbGlkIGJsYWNrOyB9XFxuLm1hci1sZWZ0IHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDsgfVxcbi5tYXItdG9wIHtcXG4gIG1hcmdpbi10b3A6IDN2aDsgfVxcbi5tYXItYm90dG9tIHtcXG4gIG1hcmdpbi1ib3R0b206IDEzJTtcXG4gIG1hcmdpbi1sZWZ0OiAyLjV2dztcXG4gIHdpZHRoOiA5MyUgIWltcG9ydGFudDsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICB3aWR0aDogMTN2dztcXG4gIGhlaWdodDogNXZoO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTsgfVxcbi5pY29ucy1jbCB7XFxuICB3aWR0aDogM3Z3O1xcbiAgaGVpZ2h0OiA1dmg7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06Zm9jdXMge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5tYXItdG9wLTEwIHtcXG4gIG1hcmdpbi10b3A6IDIlOyB9XFxuLnBhZGRpbmctcmlnaHQtcHJpY2Uge1xcbiAgcGFkZGluZy1yaWdodDogNyU7IH1cXG4ubWFyZ2luLTEge1xcbiAgbWFyZ2luLWxlZnQ6IDMlO1xcbiAgbWFyZ2luLXJpZ2h0OiAzJTsgfVxcbi5pbWctYm9yZGVyLXJhZGl1cyB7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDsgfVxcbi5tYWluLWZvcm0tc2VhcmNoLXJlc3VsdCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxuICBtYXJnaW4tdG9wOiAtNyU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi5ibHVlLWJ1dHRvbiB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50OyB9XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZjlmOTtcXG4gIG1pbi13aWR0aDogMjAwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAydnc7XFxuICB0b3A6IDcuNXZoOyB9XFxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7IH1cXG4vKm1lZGlhIHF1ZXJpZXMgZm9yIHJlc3BvbnNpdmUgdXRpbGl0aWVzKi9cXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLm1hci1sZWZ0IHtcXG4gICAgbWFyZ2luOiAwOyB9XFxuICAuaWNvbnMtY2wge1xcbiAgICB3aWR0aDogMTN2dztcXG4gICAgaGVpZ2h0OiA3dmg7IH1cXG4gIC5mb290ZXItdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxMnB4OyB9XFxuICAuaWNvbnMtbWFyIHtcXG4gICAgbWFyZ2luLXRvcDogNSU7IH1cXG4gIC5ob3VzZS1jYXJkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7IH1cXG4gIC5ibHVlLWJ1dHRvbiB7XFxuICAgIHdpZHRoOiA1MHZ3O1xcbiAgICBoZWlnaHQ6IDV2aDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kOiAjNDRkMWNhO1xcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgICBwYWRkaW5nOiAzJTsgfVxcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICAgIG1pbi13aWR0aDogMjAwcHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIHBhZGRpbmc6IDEycHggMTZweDtcXG4gICAgei1pbmRleDogMTtcXG4gICAgbGVmdDogMTJ2dztcXG4gICAgdG9wOiAxMHZoOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgICBoZWlnaHQ6IDM1MHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1dmg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgZmxvYXQ6IHJpZ2h0OyB9IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zZWFyY2gtc2VjdGlvbiwgLnN1Ym1pdC1ob21lLWxpbmsge1xcbiAgY29sb3I6IHdoaXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xcbiAgcGFkZGluZzogMiU7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxcbi5zZWNvbmQtcGFydCB7XFxuICBtYXJnaW4tdG9wOiA0JTsgfVxcbi5zdWJtaXQtaG9tZS1saW5rIHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbiAgcGFkZGluZzogNXB4O1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc2VhcmNoLWJ1dHRvbiB7XFxuICB3aWR0aDogODAlO1xcbiAgbWluLWhlaWdodDogNXZoO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cXG4uc2VhcmNoLWJ1dHRvbjp2aXNpdGVkIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDQwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTsgfVxcbi5zZWFyY2gtYnV0dG9uOmhvdmVyIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7IH1cXG4uc2VhcmNoLWlucHV0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtICFpbXBvcnRhbnQ7XFxuICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDsgfVxcbi5zZWFyY2gtaW5wdXQ6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5zZWFyY2gtaW5wdXQ6OnBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuLmVycm9yIHtcXG4gIGJvcmRlcjogMXB4IHNvbGlkIHJlZCAhaW1wb3J0YW50OyB9XFxuLmVyci1tc2cge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG4gIGNvbG9yOiAjZjE2NDY0OyB9XFxuLnNlYXJjaC1idXR0b246ZGlzYWJsZWQge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2IzYWVhZTsgfVxcbnNlbGVjdCB7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIGNvbG9yOiBibGFjaztcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLmRlYWwtdHlwZS1zZWN0aW9uIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0OyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaW5wdXQtbGFiZWwge1xcbiAgICBtYXJnaW4tbGVmdDogMSU7IH1cXG4gIC5zZWFyY2gtYnV0dG9uIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIG1hcmdpbjogNSUgYXV0bzsgfSB9XFxuYm9keSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIG92ZXJmbG93LXg6IGhpZGRlbjsgfVxcbi5sb2dvLXNlY3Rpb24ge1xcbiAgbWFyZ2luLWJvdHRvbTogNyU7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cXG4ubG9nby1pbWFnZSB7XFxuICBtYXgtd2lkdGg6IDEyMHB4OyB9XFxuLnNsaWRlciB7XFxuICBoZWlnaHQ6IDEwMHZoO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cXG4uaGVhZGVyLW1haW4ge1xcbiAgei1pbmRleDogOTk5O1xcbiAgdG9wOiAzdmg7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7IH1cXG4uaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICBib3JkZXI6IHRoaW4gc29saWQgd2hpdGU7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4OyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDA7XFxuICB0b3A6IDZ2aDsgfVxcbi5zbGlkZXItY29sdW1uIHtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uc3VibWl0LWhvdXNlLWJ1dHRvbixcXG4uc3VibWl0LWhvdXNlLWJ1dHRvbjp2aXNpdGVkLFxcbi5zdWJtaXQtaG91c2UtYnV0dG9uOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBjdXJzb3I6IHBvaW50ZXI7IH1cXG4uZmVhdHVyZXMtc2VjdGlvbiB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW4tdG9wOiAtNXZoOyB9XFxuaW5wdXRbdHlwZT1yYWRpb10ge1xcbiAgZm9udC1zaXplOiAxNnB4OyB9XFxuLmlucHV0LWxhYmVsIHtcXG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbWFyZ2luLWxlZnQ6IDE1JTsgfVxcbi5mZWF0dXJlLWJveCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG1hcmdpbi1sZWZ0OiAyJTtcXG4gIGhlaWdodDogMzZ2aDtcXG4gIHdpZHRoOiAyOSUgIWltcG9ydGFudDtcXG4gIG1pbi1oZWlnaHQ6IDQydmg7IH1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgyKSB7XFxuICBtYXJnaW4tbGVmdDogNSU7IH1cXG4uZmVhdHVyZXMtY29udGFpbmVyID4gLmZlYXR1cmUtYm94Om50aC1jaGlsZCgzKSB7XFxuICBtYXJnaW4tbGVmdDogNSU7IH1cXG4uZmVhdHVyZS1ib3ggPiBkaXYgPiBpbWcge1xcbiAgbWF4LXdpZHRoOiA3MHB4OyB9XFxuLndoeS11cyB7XFxuICBtYXJnaW4tdG9wOiA2JTtcXG4gIG1hcmdpbi1ib3R0b206IDYlOyB9XFxuLndoeS11cy1pbWcge1xcbiAgbWF4LXdpZHRoOiAzMDBweDsgfVxcbi53aHktbGlzdCB7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBjb2xvcjogYmxhY2s7XFxuICBwYWRkaW5nOiAwOyB9XFxuLndoeS1saXN0ID4gbGkge1xcbiAgbWFyZ2luLXRvcDogMyU7IH1cXG4ud2h5LWxpc3QgPiBsaSA+IGkge1xcbiAgbWFyZ2luLWxlZnQ6IDIlOyB9XFxuLndoeS11cy1pbWctY29sdW1uIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0OyB9XFxuLnJlYXNvbnMtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbiAgcGFkZGluZzogMDsgfVxcbi5idWlsZGluZy10eXBlLWxhYmVsIHtcXG4gIGhlaWdodDogNXZoOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuaGVhZGVyLW1haW4tYm9hcmRlciB7XFxuICAgIHdpZHRoOiA0MCU7XFxuICAgIG1hcmdpbi1sZWZ0OiAxJTsgfVxcbiAgLm1vYmlsZS1oZWFkZXIge1xcbiAgICBtYXJnaW4tdG9wOiAtMTIlOyB9XFxuICAubG9nby1pbWFnZSB7XFxuICAgIG1heC13aWR0aDogOTBweDsgfVxcbiAgLm1haW4tdGl0bGUge1xcbiAgICBmb250LXNpemU6IDIwcHg7IH1cXG4gIC5mZWF0dXJlLWJveCB7XFxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlO1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMikge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgICBtYXJnaW4tbGVmdDogMDsgfVxcbiAgLndoeS11cy1pbWcge1xcbiAgICBtYXgtd2lkdGg6IDI0N3B4OyB9IH1cXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcXG4gIC5tb2JpbGUtaGVhZGVyIHtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L21haW4uY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0lBQ0kscUJBQXFCO0NBQ3hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxjQUFjO0NBQ2pCO0FBQ0Q7SUFDSSx5QkFBeUI7Q0FDNUI7QUFDRDtJQUNJLHdCQUF3QjtDQUMzQjtBQUNEO0lBQ0ksMEJBQTBCO0NBQzdCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQywwQkFBMEI7UUFDdEIsdUJBQXVCO1lBQ25CLG9CQUFvQjtDQUMvQjtBQUNEO0lBQ0kscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QseUJBQXlCO1FBQ3JCLHNCQUFzQjtZQUNsQix3QkFBd0I7SUFDaEMsNkJBQTZCO0lBQzdCLDhCQUE4QjtRQUMxQiwyQkFBMkI7WUFDdkIsdUJBQXVCO0NBQ2xDO0FBQ0Q7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLGNBQWM7SUFDZCxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGdEQUFnRDtJQUNoRCx3REFBd0Q7SUFDeEQsZ0RBQWdEO0lBQ2hELDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFDeEMsNkVBQTZFO0NBQ2hGO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IscUNBQXFDO0VBQ3JDLFlBQVk7RUFDWixrREFBa0Q7VUFDMUMsMENBQTBDLEVBQUU7QUFDdEQ7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxlQUFlO0VBQ2YsYUFBYTtFQUNiLGVBQWUsRUFBRTtBQUNuQjtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsa0RBQWtEO1VBQzFDLDBDQUEwQyxFQUFFO0FBQ3REO0VBQ0UsaURBQWlEO1VBQ3pDLHlDQUF5QyxFQUFFO0FBQ3JEO0VBQ0UsbURBQW1EO1VBQzNDLDJDQUEyQyxFQUFFO0FBQ3ZEO0VBQ0UsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixnQkFBZ0I7RUFDaEIsZ0NBQWdDO0VBQ2hDLHdCQUF3QixFQUFFO0FBQzVCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxpQ0FBaUMsRUFBRTtBQUNyQztFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZUFBZSxFQUFFO0FBQ25CO0VBQ0UsMEJBQTBCLEVBQUU7QUFDOUI7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0U7SUFDRSxnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLGVBQWU7SUFDZixnQkFBZ0IsRUFBRSxFQUFFO0FBQ3hCO0VBQ0UscUJBQXFCO0VBQ3JCLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUFtQixFQUFFO0FBQ3ZCO0VBQ0UsYUFBYTtFQUNiLFNBQVM7RUFDVCxtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLHlCQUF5QjtFQUN6QixvQkFBb0IsRUFBRTtBQUN4QjtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixvREFBb0Q7VUFDNUMsNENBQTRDO0VBQ3BELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsUUFBUTtFQUNSLFNBQVMsRUFBRTtBQUNiO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7OztFQUdFLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxtQkFBbUI7RUFDbkIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLHFCQUFxQjtFQUNyQixnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxrREFBa0Q7VUFDMUMsMENBQTBDO0VBQ2xELGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsZ0JBQWdCLEVBQUU7QUFDcEI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGFBQWE7RUFDYixXQUFXLEVBQUU7QUFDZjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0Usa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFdBQVcsRUFBRTtBQUNmO0VBQ0UsWUFBWSxFQUFFO0FBQ2hCO0VBQ0U7SUFDRSxXQUFXO0lBQ1gsZ0JBQWdCLEVBQUU7RUFDcEI7SUFDRSxpQkFBaUIsRUFBRTtFQUNyQjtJQUNFLGdCQUFnQixFQUFFO0VBQ3BCO0lBQ0UsZ0JBQWdCLEVBQUU7RUFDcEI7SUFDRSx1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGlCQUFpQixFQUFFLEVBQUU7QUFDekI7RUFDRTtJQUNFLGlCQUFpQixFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG4uc2VhcmNoLXNlY3Rpb24sIC5zdWJtaXQtaG9tZS1saW5rIHtcXG4gIGNvbG9yOiB3aGl0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gIHBhZGRpbmc6IDIlO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cXG4uc2Vjb25kLXBhcnQge1xcbiAgbWFyZ2luLXRvcDogNCU7IH1cXG4uc3VibWl0LWhvbWUtbGluayB7XFxuICBtYXJnaW4tdG9wOiAyJTtcXG4gIHBhZGRpbmc6IDVweDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLnNlYXJjaC1idXR0b24ge1xcbiAgd2lkdGg6IDgwJTtcXG4gIG1pbi1oZWlnaHQ6IDV2aDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpOyB9XFxuLnNlYXJjaC1idXR0b246dmlzaXRlZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4uc2VhcmNoLWJ1dHRvbjpob3ZlciB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLCAwLCAwLCAwLjIpOyB9XFxuLnNlYXJjaC1pbnB1dCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBtaW4taGVpZ2h0OiA1dmg7XFxuICBmb250LWZhbWlseTogU2hhYm5hbSAhaW1wb3J0YW50O1xcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7IH1cXG4uc2VhcmNoLWlucHV0Ojotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDQlOyB9XFxuLnNlYXJjaC1pbnB1dDotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0OjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xcbiAgcGFkZGluZy1yaWdodDogNCU7IH1cXG4uc2VhcmNoLWlucHV0OjpwbGFjZWhvbGRlciB7XFxuICBwYWRkaW5nLXJpZ2h0OiA0JTsgfVxcbi5lcnJvciB7XFxuICBib3JkZXI6IDFweCBzb2xpZCByZWQgIWltcG9ydGFudDsgfVxcbi5lcnItbXNnIHtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgZGlyZWN0aW9uOiBydGw7XFxuICBjb2xvcjogI2YxNjQ2NDsgfVxcbi5zZWFyY2gtYnV0dG9uOmRpc2FibGVkIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNiM2FlYWU7IH1cXG5zZWxlY3Qge1xcbiAgaGVpZ2h0OiA1dmg7XFxuICBjb2xvcjogYmxhY2s7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5kZWFsLXR5cGUtc2VjdGlvbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmlucHV0LWxhYmVsIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDElOyB9XFxuICAuc2VhcmNoLWJ1dHRvbiB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBtYXJnaW46IDUlIGF1dG87IH0gfVxcbmJvZHkge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBvdmVyZmxvdy14OiBoaWRkZW47IH1cXG4ubG9nby1zZWN0aW9uIHtcXG4gIG1hcmdpbi1ib3R0b206IDclO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XFxuLmxvZ28taW1hZ2Uge1xcbiAgbWF4LXdpZHRoOiAxMjBweDsgfVxcbi5zbGlkZXIge1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmhlYWRlci1tYWluIHtcXG4gIHotaW5kZXg6IDk5OTtcXG4gIHRvcDogM3ZoO1xcbiAgcG9zaXRpb246IGFic29sdXRlOyB9XFxuLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgYm9yZGVyOiB0aGluIHNvbGlkIHdoaXRlO1xcbiAgYm9yZGVyLXJhZGl1czogMTBweDsgfVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA4cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIHBhZGRpbmc6IDEycHggMTZweDtcXG4gIHotaW5kZXg6IDE7XFxuICBsZWZ0OiAwO1xcbiAgdG9wOiA2dmg7IH1cXG4uc2xpZGVyLWNvbHVtbiB7XFxuICBwYWRkaW5nOiAwOyB9XFxuLnN1Ym1pdC1ob3VzZS1idXR0b24sXFxuLnN1Ym1pdC1ob3VzZS1idXR0b246dmlzaXRlZCxcXG4uc3VibWl0LWhvdXNlLWJ1dHRvbjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgY3Vyc29yOiBwb2ludGVyOyB9XFxuLmZlYXR1cmVzLXNlY3Rpb24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luLXRvcDogLTV2aDsgfVxcbmlucHV0W3R5cGU9cmFkaW9dIHtcXG4gIGZvbnQtc2l6ZTogMTZweDsgfVxcbi5pbnB1dC1sYWJlbCB7XFxuICBmb250LXdlaWdodDogbGlnaHRlcjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIG1hcmdpbi1sZWZ0OiAxNSU7IH1cXG4uZmVhdHVyZS1ib3gge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICBtYXJnaW4tbGVmdDogMiU7XFxuICBoZWlnaHQ6IDM2dmg7XFxuICB3aWR0aDogMjklICFpbXBvcnRhbnQ7XFxuICBtaW4taGVpZ2h0OiA0MnZoOyB9XFxuLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMikge1xcbiAgbWFyZ2luLWxlZnQ6IDUlOyB9XFxuLmZlYXR1cmVzLWNvbnRhaW5lciA+IC5mZWF0dXJlLWJveDpudGgtY2hpbGQoMykge1xcbiAgbWFyZ2luLWxlZnQ6IDUlOyB9XFxuLmZlYXR1cmUtYm94ID4gZGl2ID4gaW1nIHtcXG4gIG1heC13aWR0aDogNzBweDsgfVxcbi53aHktdXMge1xcbiAgbWFyZ2luLXRvcDogNiU7XFxuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxcbi53aHktdXMtaW1nIHtcXG4gIG1heC13aWR0aDogMzAwcHg7IH1cXG4ud2h5LWxpc3Qge1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgY29sb3I6IGJsYWNrO1xcbiAgcGFkZGluZzogMDsgfVxcbi53aHktbGlzdCA+IGxpIHtcXG4gIG1hcmdpbi10b3A6IDMlOyB9XFxuLndoeS1saXN0ID4gbGkgPiBpIHtcXG4gIG1hcmdpbi1sZWZ0OiAyJTsgfVxcbi53aHktdXMtaW1nLWNvbHVtbiB7XFxuICB0ZXh0LWFsaWduOiByaWdodDsgfVxcbi5yZWFzb25zLXNlY3Rpb24ge1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uYnVpbGRpbmctdHlwZS1sYWJlbCB7XFxuICBoZWlnaHQ6IDV2aDsgfVxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmhlYWRlci1tYWluLWJvYXJkZXIge1xcbiAgICB3aWR0aDogNDAlO1xcbiAgICBtYXJnaW4tbGVmdDogMSU7IH1cXG4gIC5tb2JpbGUtaGVhZGVyIHtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTsgfVxcbiAgLmxvZ28taW1hZ2Uge1xcbiAgICBtYXgtd2lkdGg6IDkwcHg7IH1cXG4gIC5tYWluLXRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAyMHB4OyB9XFxuICAuZmVhdHVyZS1ib3gge1xcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiA1JTtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDIpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC5mZWF0dXJlcy1jb250YWluZXIgPiAuZmVhdHVyZS1ib3g6bnRoLWNoaWxkKDMpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7IH1cXG4gIC53aHktdXMtaW1nIHtcXG4gICAgbWF4LXdpZHRoOiAyNDdweDsgfSB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDMyMHB4KSB7XFxuICAubW9iaWxlLWhlYWRlciB7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH0gfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9TZWFyY2hCb3gvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1NlYXJjaEJveC9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZXNjYXBlKHVybCkge1xuICAgIGlmICh0eXBlb2YgdXJsICE9PSAnc3RyaW5nJykge1xuICAgICAgICByZXR1cm4gdXJsXG4gICAgfVxuICAgIC8vIElmIHVybCBpcyBhbHJlYWR5IHdyYXBwZWQgaW4gcXVvdGVzLCByZW1vdmUgdGhlbVxuICAgIGlmICgvXlsnXCJdLipbJ1wiXSQvLnRlc3QodXJsKSkge1xuICAgICAgICB1cmwgPSB1cmwuc2xpY2UoMSwgLTEpO1xuICAgIH1cbiAgICAvLyBTaG91bGQgdXJsIGJlIHdyYXBwZWQ/XG4gICAgLy8gU2VlIGh0dHBzOi8vZHJhZnRzLmNzc3dnLm9yZy9jc3MtdmFsdWVzLTMvI3VybHNcbiAgICBpZiAoL1tcIicoKSBcXHRcXG5dLy50ZXN0KHVybCkpIHtcbiAgICAgICAgcmV0dXJuICdcIicgKyB1cmwucmVwbGFjZSgvXCIvZywgJ1xcXFxcIicpLnJlcGxhY2UoL1xcbi9nLCAnXFxcXG4nKSArICdcIidcbiAgICB9XG5cbiAgICByZXR1cm4gdXJsXG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi91cmwvZXNjYXBlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi91cmwvZXNjYXBlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub3JtYWxpemUuY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbm9ybWFsaXplLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vcm1hbGl6ZS5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImltcG9ydCB7IG5vdGlmeVN1Y2Nlc3MsIG5vdGlmeUVycm9yLCBnZXRMb2dpbkluZm8gfSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQge1xuICBHRVRfQ1VSUl9VU0VSX1JFUVVFU1QsXG4gIEdFVF9DVVJSX1VTRVJfU1VDQ0VTUyxcbiAgR0VUX0NVUlJfVVNFUl9GQUlMVVJFLFxuICBJTkNSRUFTRV9DUkVESVRfUkVRVUVTVCxcbiAgSU5DUkVBU0VfQ1JFRElUX1NVQ0NFU1MsXG4gIElOQ1JFQVNFX0NSRURJVF9GQUlMVVJFLFxuICBVU0VSX0xPR0lOX1JFUVVFU1QsXG4gIFVTRVJfTE9HSU5fU1VDQ0VTUyxcbiAgVVNFUl9MT0dJTl9GQUlMVVJFLFxuICBVU0VSX0xPR09VVF9SRVFVRVNULFxuICBVU0VSX0xPR09VVF9TVUNDRVNTLFxuICBVU0VSX0xPR09VVF9GQUlMVVJFLFxuICBVU0VSX0lTX05PVF9MT0dHRURfSU4sXG4gIEdFVF9JTklUX0ZBSUxVUkUsXG4gIEdFVF9JTklUX1JFUVVFU1QsXG4gIEdFVF9JTklUX1NVQ0NFU1MsXG4gIEdFVF9JTklUX1VTRVJfRkFJTFVSRSxcbiAgR0VUX0lOSVRfVVNFUl9SRVFVRVNULFxuICBHRVRfSU5JVF9VU0VSX1NVQ0NFU1MsXG59IGZyb20gJy4uL2NvbnN0YW50cyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi9oaXN0b3J5JztcbmltcG9ydCB7XG4gIHNlbmRnZXRDdXJyZW50VXNlclJlcXVlc3QsXG4gIHNlbmRpbmNyZWFzZUNyZWRpdFJlcXVlc3QsXG4gIHNlbmRJbml0aWF0ZVJlcXVlc3QsXG4gIHNlbmRJbml0aWF0ZVVzZXJzUmVxdWVzdCxcbiAgc2VuZExvZ2luUmVxdWVzdCxcbn0gZnJvbSAnLi4vYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uJztcblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR0lOX1JFUVVFU1QsXG4gICAgcGF5bG9hZDoge1xuICAgICAgdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5TdWNjZXNzKGluZm8pIHtcbiAgY29uc29sZS5sb2coaW5mbyk7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dJTl9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgaW5mbyB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5GYWlsdXJlKGVycm9yKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dJTl9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyb3IgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRMb2dpblN0YXR1cygpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGlmIChsb2dpbkluZm8pIHtcbiAgICByZXR1cm4gbG9naW5TdWNjZXNzKGxvZ2luSW5mbyk7XG4gIH1cbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0lTX05PVF9MT0dHRURfSU4sXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldENhcHRjaGFUb2tlbigpIHtcbiAgaWYgKHByb2Nlc3MuZW52LkJST1dTRVIpIHtcbiAgICBjb25zdCBsb2dpbkZvcm0gPSAkKCcjbG9naW5Gb3JtJykuZ2V0KDApO1xuICAgIGlmIChsb2dpbkZvcm0gJiYgdHlwZW9mIGxvZ2luRm9ybVsnZy1yZWNhcHRjaGEtcmVzcG9uc2UnXSA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJldHVybiBsb2dpbkZvcm1bJ2ctcmVjYXB0Y2hhLXJlc3BvbnNlJ10udmFsdWU7XG4gICAgfVxuICB9XG4gIHJldHVybiBudWxsO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5Vc2VyKHVzZXJuYW1lLCBwYXNzd29yZCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gsIGdldFN0YXRlKSB7XG4gICAgZGlzcGF0Y2gobG9naW5SZXF1ZXN0KHVzZXJuYW1lLCBwYXNzd29yZCkpO1xuICAgIHRyeSB7XG4gICAgICAvLyBjb25zdCBjYXB0Y2hhVG9rZW4gPSBnZXRDYXB0Y2hhVG9rZW4oKTtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRMb2dpblJlcXVlc3QodXNlcm5hbWUsIHBhc3N3b3JkKTtcbiAgICAgIGRpc3BhdGNoKGxvZ2luU3VjY2VzcyhyZXMpKTtcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdsb2dpbkluZm8nLCBKU09OLnN0cmluZ2lmeShyZXMpKTtcbiAgICAgIC8vIGNvbnNvbGUubG9nKGxvY2F0aW9uLnBhdGhuYW1lKTtcbiAgICAgIGNvbnN0IHsgcGF0aG5hbWUgfSA9IGdldFN0YXRlKCkuaG91c2U7XG4gICAgICBjb25zb2xlLmxvZyhwYXRobmFtZSk7XG4gICAgICBoaXN0b3J5LnB1c2gocGF0aG5hbWUpO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgZGlzcGF0Y2gobG9naW5GYWlsdXJlKGVycikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dFJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dPVVRfUkVRVUVTVCxcbiAgICBwYXlsb2FkOiB7fSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dFN1Y2Nlc3MoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dPVVRfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7XG4gICAgICBtZXNzYWdlOiAn2K7YsdmI2Kwg2YXZiNmB2YLbjNiqINii2YXbjNiyJyxcbiAgICB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0RmFpbHVyZShlcnJvcikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HT1VUX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnJvciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0VXNlcigpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2gobG9nb3V0UmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgZGlzcGF0Y2gobG9nb3V0U3VjY2VzcygpKTtcbiAgICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xuICAgICAgbG9jYXRpb24uYXNzaWduKCcvbG9naW4nKTtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIGRpc3BhdGNoKGxvZ291dEZhaWx1cmUoZXJyKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZSgpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goaW5pdGlhdGVSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kSW5pdGlhdGVSZXF1ZXN0KCk7XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZVN1Y2Nlc3MocmVzKSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goaW5pdGlhdGVGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfVVNFUl9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2Vyc1N1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9VU0VSX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVVc2Vyc0ZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfVVNFUl9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzKCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChpbml0aWF0ZVVzZXJzUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZEluaXRpYXRlVXNlcnNSZXF1ZXN0KCk7XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZVVzZXJzU3VjY2VzcyhyZXMpKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZVVzZXJzRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfQ1VSUl9VU0VSX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlclN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfQ1VSUl9VU0VSX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXJGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9DVVJSX1VTRVJfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXIodXNlcklkKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCkge1xuICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZGdldEN1cnJlbnRVc2VyUmVxdWVzdCh1c2VySWQpO1xuICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXJTdWNjZXNzKHJlcykpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0UmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBJTkNSRUFTRV9DUkVESVRfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0U3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IElOQ1JFQVNFX0NSRURJVF9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluY3JlYXNlQ3JlZGl0RmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBJTkNSRUFTRV9DUkVESVRfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVyciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXQoYW1vdW50KSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChpbmNyZWFzZUNyZWRpdFJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgY3VyclVzZXIgfSA9IGdldFN0YXRlKCkuYXV0aGVudGljYXRpb247XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KGFtb3VudCwgY3VyclVzZXIudXNlcklkKTtcbiAgICAgIGRpc3BhdGNoKGluY3JlYXNlQ3JlZGl0U3VjY2VzcyhyZXMpKTtcbiAgICAgIGRpc3BhdGNoKGdldEN1cnJlbnRVc2VyKGN1cnJVc2VyLnVzZXJJZCkpO1xuICAgICAgbm90aWZ5U3VjY2Vzcygn2KfZgdiy2KfbjNi0INin2LnYqtio2KfYsSDYqNinINmF2YjZgdmC24zYqiDYp9mG2KzYp9mFINi02K8uJyk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgbm90aWZ5RXJyb3IoJ9iu2LfYpyDZhdis2K/YryDYqtmE2KfYtCDaqdmG24zYrycpO1xuICAgICAgZGlzcGF0Y2goaW5jcmVhc2VDcmVkaXRGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uLmpzIiwiaW1wb3J0IHtcbiAgR0VUX0hPVVNFU19SRVFVRVNULFxuICBHRVRfSE9VU0VTX1NVQ0NFU1MsXG4gIEdFVF9IT1VTRVNfRkFJTFVSRSxcbiAgUE9MTF9IT1VTRVNfUkVRVUVTVCxcbiAgUE9MTF9IT1VTRVNfU1VDQ0VTUyxcbiAgUE9MTF9IT1VTRVNfRkFJTFVSRSxcbiAgU0VUX1NFQVJDSF9PQkpfUkVRVUVTVCxcbiAgU0VUX1NFQVJDSF9PQkpfU1VDQ0VTUyxcbiAgU0VUX1NFQVJDSF9PQkpfRkFJTFVSRSxcbn0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uL2hpc3RvcnknO1xuaW1wb3J0IHtcbiAgc2VuZFNlYXJjaEhvdXNlc1JlcXVlc3QsXG4gIHNlbmRwb2xsSG91c2VzUmVxdWVzdCxcbn0gZnJvbSAnLi4vYXBpLWhhbmRsZXJzL3NlYXJjaCc7XG5pbXBvcnQgeyBub3RpZnlFcnJvciwgbm90aWZ5U3VjY2VzcywgZ2V0Rm9ybURhdGEgfSBmcm9tICcuLi91dGlscyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRTZWFyY2hPYmpSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFNFVF9TRUFSQ0hfT0JKX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRTZWFyY2hPYmpTdWNjZXNzKG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogU0VUX1NFQVJDSF9PQkpfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRTZWFyY2hPYmpGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFNFVF9TRUFSQ0hfT0JKX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNldFNlYXJjaE9iaihtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChzZXRTZWFyY2hPYmpTdWNjZXNzKG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlKSk7XG4gICAgaGlzdG9yeS5wdXNoKCcvaG91c2VzJyk7XG4gIH07XG59XG5leHBvcnQgZnVuY3Rpb24gc2VhcmNoSG91c2VzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSE9VU0VTX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hIb3VzZXNTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0hPVVNFU19TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlYXJjaEhvdXNlc0ZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0hPVVNFU19GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hIb3VzZXMobWluQXJlYSwgZGVhbFR5cGUsIGJ1aWxkaW5nVHlwZSwgbWF4UHJpY2UpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goc2VhcmNoSG91c2VzUmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgaWYgKCFidWlsZGluZ1R5cGUpIHtcbiAgICAgICAgYnVpbGRpbmdUeXBlID0gbnVsbDtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRTZWFyY2hIb3VzZXNSZXF1ZXN0KFxuICAgICAgICBwYXJzZUZsb2F0KG1pbkFyZWEpLFxuICAgICAgICBwYXJzZUZsb2F0KGRlYWxUeXBlKSxcbiAgICAgICAgYnVpbGRpbmdUeXBlLFxuICAgICAgICBwYXJzZUZsb2F0KG1heFByaWNlKSxcbiAgICAgICk7XG4gICAgICBkaXNwYXRjaChzZWFyY2hIb3VzZXNTdWNjZXNzKHJlcykpO1xuICAgICAgZGlzcGF0Y2goc2V0U2VhcmNoT2JqKG1pbkFyZWEsIGRlYWxUeXBlLCBidWlsZGluZ1R5cGUsIG1heFByaWNlKSk7XG4gICAgICBpZiAocmVzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICBub3RpZnlTdWNjZXNzKCfYrtin2YbZhyDYp9uMINio2Kcg2KfbjNmGINin24zZhiDZhdi02K7Ytdin2Kog2KvYqNiqINmG2LTYr9mHINin2LPYqicpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKHNlYXJjaEhvdXNlc0ZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBwb2xsSG91c2VzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBQT0xMX0hPVVNFU19SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcG9sbEhvdXNlc1N1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBQT0xMX0hPVVNFU19TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHsgcmVzcG9uc2UgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBvbGxIb3VzZXNGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFBPTExfSE9VU0VTX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBvbGxIb3VzZXMobWluQXJlYSwgZGVhbFR5cGUsIGJ1aWxkaW5nVHlwZSwgbWF4UHJpY2UpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKHBvbGxIb3VzZXNSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBpZiAoIWJ1aWxkaW5nVHlwZSkge1xuICAgICAgICBidWlsZGluZ1R5cGUgPSBudWxsO1xuICAgICAgfVxuICAgICAgbGV0IHJlcyA9IGF3YWl0IHNlbmRwb2xsSG91c2VzUmVxdWVzdChcbiAgICAgICAgcGFyc2VGbG9hdChtaW5BcmVhKSxcbiAgICAgICAgcGFyc2VGbG9hdChkZWFsVHlwZSksXG4gICAgICAgIGJ1aWxkaW5nVHlwZSxcbiAgICAgICAgcGFyc2VGbG9hdChtYXhQcmljZSksXG4gICAgICApO1xuICAgICAgY29uc29sZS5sb2cocmVzKTtcbiAgICAgIGlmICghcmVzKSB7XG4gICAgICAgIHJlcyA9IGdldFN0YXRlKCkuc2VhcmNoLmhvdXNlcztcbiAgICAgICAgY29uc29sZS5sb2coJ2hhbGEnKTtcbiAgICAgICAgY29uc29sZS5sb2cocmVzKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKHBvbGxIb3VzZXNTdWNjZXNzKHJlcykpO1xuICAgICAgbm90aWZ5RXJyb3IoJ9in2LfZhNin2LnYp9iqINis2LPYqiDZiCDYrNmI24wg2LTZhdinINio2LHZiNiyINi02K8uJyk7XG5cbiAgICAgIGRpc3BhdGNoKHNldFNlYXJjaE9iaihtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSkpO1xuICAgICAgaWYgKHJlcy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgbm90aWZ5U3VjY2Vzcygn2K7Yp9mG2Ycg2KfbjCDYqNinINin24zZhiDYp9uM2YYg2YXYtNiu2LXYp9iqINir2KjYqiDZhti02K/ZhyDYp9iz2KonKTtcbiAgICAgIH1cbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChwb2xsSG91c2VzRmFpbHVyZShlcnJvcikpO1xuICAgIH1cbiAgfTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYWN0aW9ucy9zZWFyY2guanMiLCJpbXBvcnQgeyBmZXRjaEFwaSwgZ2V0R2V0Q29uZmlnLCBnZXRQb3N0Q29uZmlnIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHtcbiAgZ2V0Q3VycmVudFVzZXJVcmwsXG4gIGdldEluaXRpYXRlVXJsLFxuICBsb2dpbkFwaVVybCxcbiAgaW5jcmVhc2VDcmVkaXRVcmwsXG59IGZyb20gJy4uL2NvbmZpZy9wYXRocyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KGFtb3VudCwgaWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGluY3JlYXNlQ3JlZGl0VXJsKGFtb3VudCwgaWQpLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KHVzZXJJZCkge1xuICByZXR1cm4gZmV0Y2hBcGkoZ2V0Q3VycmVudFVzZXJVcmwodXNlcklkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZEluaXRpYXRlUmVxdWVzdCgpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEluaXRpYXRlVXJsLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSB7XG4gIHJldHVybiBmZXRjaEFwaShsb2dpbkFwaVVybCwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZExvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKFxuICAgIGxvZ2luQXBpVXJsLFxuICAgIGdldFBvc3RDb25maWcoe1xuICAgICAgdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICB9KSxcbiAgKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwiaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7XG4gIGZldGNoQXBpLFxuICBnZXRHZXRDb25maWcsXG4gIGdldFB1dENvbmZpZyxcbiAgZ2V0UG9zdENvbmZpZyxcbiAgZ2V0TG9naW5JbmZvLFxufSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQgeyBnZXRTZWFyY2hIb3VzZXNVcmwsIGdldEluaXRpYXRlVXJsIH0gZnJvbSAnLi4vY29uZmlnL3BhdGhzJztcbi8qKlxuICogQGFwaSB7Z2V0fSAvdjIvYWNxdWlzaXRpb24vZHJpdmVyL3Bob25lTnVtYmVyLzpwaG9uZU51bWJlclxuICogQGFwaU5hbWUgRHJpdmVyQWNxdWlzaXRpb25TZWFyY2hCeVBob25lTnVtYmVyXG4gKiBAYXBpVmVyc2lvbiAyLjAuMFxuICogQGFwaUdyb3VwIERyaXZlclxuICogQGFwaVBlcm1pc3Npb24gRklFTEQtQUdFTlQsIFRSQUlORVIsIEFETUlOXG4gKiBAYXBpUGFyYW0ge1N0cmluZ30gcGhvbmVOdW1iZXIgcGhvbmUgbnVtYmVyIHRvIHNlYXJjaCAoKzk4OTEyMzQ1Njc4OSwgMDkxMjM0NTY3ODkpXG4gKiBAYXBpU3VjY2VzcyB7U3RyaW5nfSByZXN1bHQgQVBJIHJlcXVlc3QgcmVzdWx0XG4gKiBAYXBpU3VjY2Vzc0V4YW1wbGUge2pzb259IFN1Y2Nlc3MtUmVzcG9uc2U6XG4ge1xuICAgXCJyZXN1bHRcIjogXCJPS1wiLFxuICAgXCJkYXRhXCI6IHtcbiAgICAgICBcImRyaXZlcnNcIjogW1xuICAgICAgICB7XG4gICAgICAgICAgXCJpZFwiOiAxMCxcbiAgICAgICAgICBcImZ1bGxOYW1lXCI6IFwi2LnZhNuM2LHYttinINmC2LHYqNin2YbbjFwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkXCI6IDEyLFxuICAgICAgICAgIFwiZnVsbE5hbWVcIjogXCLYudmE24zYsdi22Kcg2YLYsdio2KfZhtuMXCJcbiAgICAgICAgfVxuICAgICAgIF1cbiAgIH1cbiB9XG4gKi9cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRTZWFyY2hIb3VzZXNSZXF1ZXN0KFxuICBtaW5BcmVhLFxuICBkZWFsVHlwZSxcbiAgYnVpbGRpbmdUeXBlLFxuICBtYXhQcmljZSxcbikge1xuICByZXR1cm4gZmV0Y2hBcGkoXG4gICAgZ2V0U2VhcmNoSG91c2VzVXJsLFxuICAgIGdldFBvc3RDb25maWcoeyBtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSB9KSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNlbmRwb2xsSG91c2VzUmVxdWVzdChcbiAgbWluQXJlYSxcbiAgZGVhbFR5cGUsXG4gIGJ1aWxkaW5nVHlwZSxcbiAgbWF4UHJpY2UsXG4pIHtcbiAgcmV0dXJuIGZldGNoQXBpKFxuICAgIGdldEluaXRpYXRlVXJsLFxuICAgIGdldFBvc3RDb25maWcoeyBtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSB9KSxcbiAgKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL3NlYXJjaC5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmc/MmZjMmQ1YzFcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZz82ZmEyODA0Y1wiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHR3aXR0ZXJJY29uIGZyb20gJy4vVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nJztcbmltcG9ydCBpbnN0YWdyYW1JY29uIGZyb20gJy4vMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nJztcbmltcG9ydCB0ZWxlZ3JhbUljb24gZnJvbSAnLi8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmcnO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5cbmNsYXNzIEZvb3RlciBleHRlbmRzIENvbXBvbmVudCB7XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGZvb3RlciBjbGFzc05hbWU9XCJzaXRlLWZvb3RlciBjZW50ZXItY29sdW1uIGRhcmstZ3JlZW4tYmFja2dyb3VuZCB3aGl0ZVwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS00IHRleHQtYWxpZ24tbGVmdFwiPlxuICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cInNvY2lhbC1pY29ucy1saXN0IG5vLWxpc3Qtc3R5bGVcIj5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17aW5zdGFncmFtSWNvbn1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNvY2lhbC1uZXR3b3JrLWljb25cIlxuICAgICAgICAgICAgICAgICAgYWx0PVwiaW5zdGFncmFtLWljb25cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e3R3aXR0ZXJJY29ufVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic29jaWFsLW5ldHdvcmstaWNvblwiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJ0d2l0dGVyLWljb25cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e3RlbGVncmFtSWNvbn1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNvY2lhbC1uZXR3b3JrLWljb25cIlxuICAgICAgICAgICAgICAgICAgYWx0PVwidGVsZWdyYW0taWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTggdGV4dC1hbGlnbi1yaWdodFwiPlxuICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgINiq2YXYp9mF24wg2K3ZgtmI2YIg2KfbjNmGINmI2KjigIzYs9in24zYqiDZhdiq2LnZhNmCINio2Ycg2YXZh9ix2YbYp9iyINir2KfYqNiqINmIINmH2YjZhdmGINi02LHbjNmBINiy2KfYr9mHXG4gICAgICAgICAgICAgINmF24zigIzYqNin2LTYry5cbiAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Zvb3Rlcj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoRm9vdGVyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvRm9vdGVyL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZz8yNDhiZmE4ZFwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL1R3aXR0ZXJfYmlyZF9sb2dvXzIwMTIuc3ZnLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5pbXBvcnQgbG9nbyBmcm9tICcuL2xvZ28ucG5nJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uLy4uL2hpc3RvcnknO1xuaW1wb3J0IFNlYXJjaEJveCBmcm9tICcuLi9TZWFyY2hCb3gnO1xuXG5jbGFzcyBIb3VzZXNQYWdlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBob3VzZXM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxuICAgIGhhbmRsZVN1Ym1pdDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgfTtcbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpe1xuICAgIGNvbnNvbGUubG9nKG5leHRQcm9wcyk7XG4gIH1cbiAgZ2V0UHJpY2VDb21wb25lbnQgPSBob3VzZSA9PiB7XG4gICAgaWYgKGhvdXNlLmRlYWxUeXBlKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMlwiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTYgZmxleC1jZW50ZXIgZmxleC1yb3ctcmV2IHBhZGRpbmctcmlnaHQtcHJpY2UgXCI+XG4gICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwicGVyc2lhbiBibGFjay1mb250IG1hcmdpbi0xIHByaWNlLWhlYWRpbmdcIj5cbiAgICAgICAgICAgICAg2LHZh9mGIDxzcGFuIGNsYXNzTmFtZT1cIm1hcmdpbi0xXCI+e2hvdXNlLmJhc2VQcmljZX08L3NwYW4+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInBlcnNpYW4gbGlnaHQtZ3JleS1mb250IG1hcmdpbi0xXCI+INiq2YjZhdin2YYgPC9zcGFuPlxuICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IGZsZXgtY2VudGVyIGZsZXgtcm93LXJldiBcIj5cbiAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJwZXJzaWFuIGJsYWNrLWZvbnQgbWFyZ2luLTEgcHJpY2UtaGVhZGluZ1wiPlxuICAgICAgICAgICAgICDYp9is2KfYsdmHIDxzcGFuIGNsYXNzTmFtZT1cIm1hcmdpbi0xXCI+e2hvdXNlLnJlbnRQcmljZX08L3NwYW4+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInBlcnNpYW4gbGlnaHQtZ3JleS1mb250IG1hcmdpbi0xXCI+INiq2YjZhdin2YYgPC9zcGFuPlxuICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNyBmbGV4LWNlbnRlciBmbGV4LXJvdy1yZXYgcGFkZGluZy1yaWdodC1wcmljZVwiPlxuICAgICAgICA8aDQgY2xhc3NOYW1lPVwicGVyc2lhbiBibGFjay1mb250IG1hcmdpbi0xIHByaWNlLWhlYWRpbmdcIj5cbiAgICAgICAgICDZgtuM2YXYqlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm1hcmdpbi0xXCI+e2hvdXNlLnNlbGxQcmljZX08L3NwYW4+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicGVyc2lhbiBsaWdodC1ncmV5LWZvbnQgbWFyZ2luLTFcIj7YqtmI2YXYp9mGPC9zcGFuPlxuICAgICAgICA8L2g0PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImhvbWUtcGFuZWxcIj5cbiAgICAgICAgPHNlY3Rpb24+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzZWFyY2gtdGhlbWUgZmxleC1jZW50ZXJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgICA8aDMgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBwZXJzaWFuLWJvbGQgYmx1ZS1mb250IGZsZXgtY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgINmG2KrYp9uM2Kwg2KzYs9iq2KzZiFxuICAgICAgICAgICAgICAgICAgPC9oMz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4gZ3JleS1mb250IHRleHQtYWxpZ24tY2VudGVyIG1hci01XCI+XG4gICAgICAgICAgICAgICAg2KjYsdin24wg2YXYtNin2YfYr9mHINin2LfZhNin2LnYp9iqINio24zYtNiq2LEg2K/Ysdio2KfYsdmHINuMINmH2LEg2YXZhNqpINix2YjbjCDYotmGINqp2YTbjNqpINqp2YbbjNivXG4gICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IFwiPlxuICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5ob3VzZXMubWFwKGhvdXNlID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaChgL2hvdXNlcy8ke2hvdXNlLmlkfWApO1xuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImNvbC14cy0xMiAgY29sLW1kLTUgbm8tcGFkZGluZyBob3VzZS1jYXJkIGJvcmRlci1yYWRpdXNcIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkIGltZy1oZWlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaG91c2UtaW1nIGltZy1ib3JkZXItcmFkaXVzICBcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cImltZy10YWdcIiBzcmM9e2hvdXNlLmltYWdlVVJMfSBhbHQ9XCJob3VzZS1pbWdcIiAvPlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLW1kLTQgd2hpdGUtZm9udCBtYXItdG9wLTEwIGNvbC14cy04IGJvcmRlci1yYWRpdXNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxoNlxuICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2BtYXItNSB3aGl0ZS1mb250IHBhZGQtNSBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyIGJvcmRlci1yYWRpdXMgJHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3VzZS5kZWFsVHlwZSA9PT0gMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyAncGluay1iYWNrZ3JvdW5kJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAncHVycGxlLWJhY2tncm91bmQnXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICB7aG91c2UuZGVhbFR5cGUgPT09IDEgPyAn2KfYrNin2LHZhycgOiAn2YHYsdmI2LQnfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9oNj5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGJvcmRlci1ib3R0b21cIj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02IGZsZXgtY2VudGVyIHRleHQtYWxpZ24tbGVmdFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4gYmxhY2stZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1tYXAtbWFya2VyIHB1cnBsZS1mb250XCIgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgJm5ic3A7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtob3VzZS5hZGRyZXNzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAmbmJzcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNiBwdWxsLXJpZ2h0IGZsZXgtY2VudGVyIGZsZXgtcm93LXJldlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4gYmxhY2stZm9udCB0ZXh0LWFsaWduLXJpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtob3VzZS5hcmVhfSDZhdiq2LEg2YXYsdio2LlcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBmbGV4LXJvdy1yZXZcIj5cbiAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5nZXRQcmljZUNvbXBvbmVudChob3VzZSl9XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInBlcnNpYW4gbGlnaHQtZ3JleS1mb250IHRleHQtYWxpZ24tY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICDYrNiz2KrYrNmI24wg2YXYrNiv2K9cbiAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgZmxleC1jZW50ZXIgbWFyLWJvdHRvbSBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgbWFpbi1mb3JtIG1haW4tZm9ybS1zZWFyY2gtcmVzdWx0IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICAgIDxTZWFyY2hCb3ggaGFuZGxlU3VibWl0PXt0aGlzLnByb3BzLmhhbmRsZVN1Ym1pdH0gLz5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIHN1Ym1pdC1ob21lLWxpbmsgYm9yZGVyLXJhZGl1cyB0ZXh0LWFsaWduLWNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwic3VibWl0LWhvbWUtaGVhZGluZyB3aGl0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgINi12KfYrdioINiu2KfZhtmHINmH2LPYqtuM2K/YnyDYrtin2YbZhyDYrtmI2K8g2LHYp1xuICAgICAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzdWJtaXQtaG91c2UtYnV0dG9uIHdoaXRlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvaG91c2VzL25ldycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICDYq9io2KpcbiAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAg2qnZhtuM2K8uXG4gICAgICAgICAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEhvdXNlc1BhZ2UpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvSG91c2VzUGFnZS5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvY2FzZXktaG9ybmVyLTUzMzU4Ni11bnNwbGFzaC5qcGc/ZDUxMTNjODJcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvY2FzZXktaG9ybmVyLTUzMzU4Ni11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9jYXNleS1ob3JuZXItNTMzNTg2LXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cyA9IFwiZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFaQUFBQUdRQ0FNQUFBQzNZY2IrQUFBQU0xQk1WRVg1K3Z2cjdPM3k4L1R1Ny9ENCtmbnM3ZTczK1BqdDcvRDI5dmYzK2ZudDd1LzA5Zlh4OHZQdjhQSDE5dmZ5OHZQejlQVVE2VldZQUFBSDRrbEVRVlI0WHUzZDJYWVR2eExGNGRvYWh4N2YvMm1QblVBNmVKbi9TYUFkS2ViMzNRQyt5WUtOcWlSMVc3Si9Ed0FBQUFBQUFBQUFBQUFnMkZEZ2ZiRnhvRWk2Um9KMTh4ZkxYcTJyT3VzYlJWSldlNUNwNlNlZmlPU2ppdUw4aUV5QzEwWDIza2RkT1BzZ0lvblNBekpKVFlxdTJsV1pKYzMyTVVReVN6bzlrNUFsSCt5bkVqK2NDSkZNMHJ5Y25jbHlFMENLMHNjQ0o1SWdiUmFtVXpNcFVyWmZKS21aRFJMSmJFUExhbVoyWkxLZk1rQ1MvY3BKa3cwU1NiV2hPU2tkTXlQNWNNYWdXKzUrOWhIc3BDUnB0NnV3bkRRL0xmZEd3NkpvSDRJb2J4Y3BTM0U5YWN6Vit4OStCR1lwbUUxUnl2V3NJbmgzMkJUN0NFelNhcHVrT2RnQWdTQklTNWJpWkllZUpRdFpGeTNab1d0VHh5NXBDWGJvTysxRmtwd2R1aThNa1lzZEJ0ODZRZi9OUmZUZmZzZTRENmpRL3hFdUh2K1NBM2dONlBFQUFBQUFBTUM2TEtzTkE2c3V1aVZTM2lSN2dVVVgzdm9vT2lUcmprQ3lEdDdlVUxJbTYyS1g1dkpxK1ZrM3NYcmZLWThRRllPOXFsS3p2ckJKdS8za0pHZjk0SFpRaEtaWXJTTjRxZHg4TjlQNlFaR1dPd0gxZ2liVjI0UzhkVWJGR3FabW9VclpiTkN1enF5My83d1h4N3J3cWtZMTZ3cTd0TmxQZyt5ZE1ORWFhSXFGSW1YM0trdkplc09pdzJ6ZG9lck5qLzRPQUFBQUFBQUFBQUFBQUFBQUFKdmtnNDBDa3k0V0d3V2NGQ1ViQlhaZFpCc0ZRcGEwMmpBUXByMWFSd0FBQUFBQUFBQUFBS2h0c29IQVM1dDlBb3E3VmV4RWUveGNJbkM2NWV4TUtRNzEySjlBTEgzMmVEQUNzZmNrOTRDTFUyd2NCR0plcXZZUWF5V1FQN0JLdXoxQW1CVlhBdmtEVGQ3T2w1b3U1a0FnbnpaTGozbVB0a25LaVVBK2E1S1NuU3Q0U1Z0SVdaK3Zod1JTcEdLbktsR3YvU1BNa3BaZ1hSSElKc2xYZTdGR0tSYnJoMEJxbHVSdS85Z0xQV1NLVWlzM28xeTVXZ2ZNc282bWNhZWxkTUE2cEVseHQxdGhVWjlkWlZicVRUSDlacVR2N1BaK1RKYkNtUk9zK3NIUDd5T1FtalhiYWRKdlJrSlQ1b25oUjdsZzU3bi9MNTk2dFJEY3IwMmJsS3dIM0o4aU5EWHJBMUY1cElxRitVN04ydnRWTEt6U1pEZHl4NHFGcU1WK1ZYdFdMTXhTR0xsaVViUDZWeXhxMWtnVkM3UGszdXQ4K1RaVzNZcldFWUp1emZZZEFBQUF3RjBWR3dYKzYvVUNFQWdrZjFvZ1lJUVFDQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBUUNyRnhvQzBMMDBYTmdCVTEvU0RkWWM2NnlwNnR3OVFzdUNpcExZbEd3RlNsdVJYKzc3VzFaN0lHcVU0MlZkNFZEbk15dlkwcGkrNy83VTJLUWM3WDVXMnA4cGpzaTh4UCtvSWxsMUs1UEY1WGhmZXp1Y1Y3VW1rZU9UeGJVZElrR1o3RHFFZGVYemZIakpKcXoySDdZdi9iNVZrRDdCSTloeVMxTUp6M0FEeUhMeFVudVNPbktlUUpQOHN0MGc5aGZrSnB1K24zU1FjK20vbGhlY1lJRW5hVDZyZm5UT1pucUQybm5qMytTN3BoRXlvdlZudHJKRVcrMmJTT2xTc2tUY1dtM0xkczY1eTZOTkNuSDEvdTVUT3JCaE9VcTdXUVhtT0RRZXZlT1o2WnBNMGgxN3JxV0lkREx1eEdLUWxxOTlNeDBuV3djQWJpMTRYTFJtQkRMS3h1Qi9QVG51b3BkaDdiQ3pXd2FZNWJDem1ZaytGeFMxY3pEWVVWUHMwQUFBQUFBQUFBQUFBQUFBQUFNa3RYc3ArVzYwL1RFMXZvZ3ZXSDRmUEtmclorZGZmN2RZUkprbHhTL1lpVEUzU0hLd1hUTGNCN0ZIS0pETFFZV2MxUzdOMWdSU2xaRGRDbGxickFmbnV5UTgxcWxrSG1ONFZweWxMV2w3amNYMU82RUJURE1mYzk0VVBaaGE2bkIrR1NYTEhRWTF4YzNPVXZGMHNpdmJsc0x3TmtQbkgzTGYrYUNwT3F2YlY4TlpCcXVURFN5SkZXbDUvS2ZiRlVONWF0NVBLL3JvY2JHcWRBc0ZSbDd5YWVla0l4RW5CME8wZ0RpLy80d2llVlZvNk5YWE12d1N5U1g2ZG1qUjFtdmJDS3gvUmhCcjF3dmRmR0ZLeUpzbFphcnBZZ2xucXZIVkNJTllVazFseFUrMjN1WWpwbU51dVV2eVpRV3Bzdi9jL3JuaVQ1S2RrZFowbFpVUHZBNzJucURlYmRZSDUvWTVWbmFOZUxNWDZRTHBwRm1WM3JnVERtSmVOZ090NHNFbUxqUVBoLzd6eTQ1SjlLWVQ0WHdjTno0b2RFdUhOckp6c251bzFkQ0I3c0lkYnZWODdWQzI1Y1A5QzlqWndIcFBpL3ZpZmNmSGxpY3lTb3F2Mlh0aWJqbUkySkMrcGxZZi9qQjdUbnRKMHNlekpYcVZwMFVXY2JHaFRsT1RyRXdaeWZHV25lZS96dC9ubVRuQ1N5bE9Wck1NNlJ4MjBUUFlORk1rL3Vxa3ZxL1dTZHVldjNGN3NlMmhTdFdGZ0grcEpBVUpVRERZTWJGSXJOZ3k0S01tUEV3bnFySXU1MmxDSXBOZ3dNUDErSVEwbVdwaWwzWWFCSW1VYkI4S3NaQ05CdFJFQkFBQUFBQUFBQUFBQUFBQUFBSUJhcm13VWNMcXl1MEFnY0pML1pDQmdoQkFJbUdVQkFBQUFBQUFBQUFBQUFBQUFrckwzZm5iT1RhVlU2d3o2aGJNL0V1d3NrUHpWM3dYU1Rrc0U3MEpJZjFxeWtweWRpRUQrMWk1Vkd3ZUJ6SkszY1JCSWxyVGFLQWdrNkdLY3ZrNGdSZkk2cTYvN3E0bEEvb2FUNm1tM3FPcktFY2pmV0JUUHUyZFlhdjlhSUtHa2N3T0pXc3lXUCszckJGTDBrNy80KzBEcVM2WTFxZ1ZLMXQ4Rm9sTUs5aXFWMTA3aWFPcC9HTWpzcmpaL3l0OStrOEpmM0k2T1NVcDJJcTlzRjMvYTErRWtPNU0wMjhYUjE3c2lrQ1I1dHhZNytucFhCTExyVmZSekhtQitSQ0JsOWpwMDdPc0VjZ2hsY292WGxiZStDT1EybDJxRFN2dm1YeXh1cllNSDh2eldKZXE5dG8yVHlhSm0vNWJnb2w1a2Y5WDB3aGNiZy8vWDZ2eHJITXVlanRLNk5WMzRaQ1BZOGo4VlNNcVMyaDV1UDU1MXNka1h3eVFwVG5aSG5TWGxhbDhKczZRdDJIMmxTVEhaRjhLc3VOcHZoZm1yRThHVzdMOXNVck9CWVByUlJRQUFBQUFBQUFBQUFQNEh3ZDR3VmM4WGY5b0FBQUFBU1VWT1JLNUNZSUk9XCJcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvZ2VvbWV0cnkyLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL2dlb21ldHJ5Mi5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9sb2dvLnBuZz8yYWY3M2Q4YVwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9sb2dvLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL2xvZ28ucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvbHVrZS12YW4tenlsLTUwNDAzMi11bnNwbGFzaC5qcGc/MjhjYmNmOWJcIjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvbHVrZS12YW4tenlsLTUwNDAzMi11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9sdWtlLXZhbi16eWwtNTA0MDMyLXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21haGRpYXItbWFobW9vZGktNDUyNDg5LXVuc3BsYXNoLmpwZz81ODcyNzZkMlwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9tYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9tYWhkaWFyLW1haG1vb2RpLTQ1MjQ4OS11bnNwbGFzaC5qcGdcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvSG91c2VzUGFnZS9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL0hvdXNlc1BhZ2UvbWljaGFsLWt1YmFsY3p5ay0yNjA5MDktdW5zcGxhc2guanBnPzNhMjQ2YTE4XCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21pY2hhbC1rdWJhbGN6eWstMjYwOTA5LXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Ib3VzZXNQYWdlL21pY2hhbC1rdWJhbGN6eWstMjYwOTA5LXVuc3BsYXNoLmpwZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xheW91dC5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL0xheW91dC5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiLyoqXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXG4gKlxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBub3JtYWxpemVDc3MgZnJvbSAnbm9ybWFsaXplLmNzcyc7XG5pbXBvcnQgcyBmcm9tICcuL0xheW91dC5jc3MnO1xuaW1wb3J0IEZvb3RlciBmcm9tICcuLi9Gb290ZXInO1xuaW1wb3J0IFBhbmVsIGZyb20gJy4uL1BhbmVsJztcblxuY2xhc3MgTGF5b3V0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBtYWluUGFuZWw6IFByb3BUeXBlcy5ib29sLFxuICB9O1xuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIG1haW5QYW5lbDogZmFsc2UsXG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbWQtMTIgY29sLXhzLTEyXCIgc3R5bGU9e3sgcGFkZGluZzogJzAnIH19PlxuICAgICAgICA8UGFuZWwgbWFpblBhbmVsPXt0aGlzLnByb3BzLm1haW5QYW5lbH0gLz5cbiAgICAgICAge3RoaXMucHJvcHMuY2hpbGRyZW59XG4gICAgICAgIDxGb290ZXIgLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhub3JtYWxpemVDc3MsIHMpKExheW91dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5qcyIsIi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L25vLWRhbmdlciAqL1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0ICogYXMgYXV0aEFjdGlvbnMgZnJvbSAnLi4vLi4vYWN0aW9ucy9hdXRoZW50aWNhdGlvbic7XG5pbXBvcnQgcyBmcm9tICcuL21haW4uY3NzJztcbmltcG9ydCBsb2dvSW1nIGZyb20gJy4vbG9nby5wbmcnO1xuaW1wb3J0IGhpc3RvcnkgZnJvbSAnLi4vLi4vaGlzdG9yeSc7XG5cbmNsYXNzIFBhbmVsIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjdXJyVXNlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIGF1dGhBY3Rpb25zOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgbWFpblBhbmVsOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICAgIGlzTG9nZ2VkSW46IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnNvbGUubG9nKHRoaXMucHJvcHMuY3VyclVzZXIpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMucHJvcHMuaXNMb2dnZWRJbik7XG4gICAgaWYgKHRoaXMucHJvcHMubWFpblBhbmVsKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8aGVhZGVyIGNsYXNzTmFtZT1cImhpZGRlbi14c1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBoZWFkZXItbWFpblwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgtY2VudGVyIGNvbC1tZC0yICAgbmF2LXVzZXIgY29sLXhzLTEyIGRyb3Bkb3duIFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBmbGV4LWNlbnRlciBoZWFkZXItbWFpbi1ib2FyZGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTkgXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGg1IGNsYXNzTmFtZT1cInRleHQtYWxpZ24tcmlnaHQgd2hpdGUtZm9udCBwZXJzaWFuIFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAg2YbYp9it24zZhyDaqdin2LHYqNix24xcbiAgICAgICAgICAgICAgICAgICAgICA8L2g1PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZHJvcGRvd24tY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwicGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsYWNrLWZvbnQgXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIudXNlcm5hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTYgcHVsbC1sZWZ0IHBlcnNpYW4gbGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYqtmI2YXYp9mGXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlci5iYWxhbmNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTYgbGlnaHQtZ3JleS1mb250IHB1bGwtcmlnaHQgIHBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KfYudiq2KjYp9ixXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2KfZgdiy2KfbjNi0INin2LnYqtio2KfYsVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvaW5jcmVhc2VDcmVkaXQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYrtix2YjYrFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5hdXRoQWN0aW9ucy5sb2dvdXRVc2VyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHshdGhpcy5wcm9wcy5jdXJyVXNlciAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItmI2LHZiNivXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnL2xvZ2luJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibm8tcGFkZGluZyAgY29sLXhzLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1zbWlsZS1vIGZhLTJ4IHdoaXRlLWZvbnRcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2hlYWRlcj5cbiAgICAgICk7XG4gICAgfVxuICAgIHJldHVybiAoXG4gICAgICA8aGVhZGVyPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG5hdi1jbFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4LWNlbnRlciBjb2wtbWQtMiAgIG5hdi11c2VyIGNvbC14cy0xMiBkcm9wZG93blwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGZsZXgtY2VudGVyIFwiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtOSBcIj5cbiAgICAgICAgICAgICAgICAgICAgPGg1IGNsYXNzTmFtZT1cInRleHQtYWxpZ24tcmlnaHQgcGVyc2lhbiBwdXJwbGUtZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgINmG2KfYrduM2Ycg2qnYp9ix2KjYsduMXG4gICAgICAgICAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZHJvcGRvd24tY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlciAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJibGFjay1mb250IHBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBibHVlLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMuY3VyclVzZXIudXNlcm5hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIiBsaWdodC1ncmV5LWZvbnQgY29sLXhzLTYgcHVsbC1sZWZ0IGJsdWUtZm9udCBwZXJzaWFuXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImxpZ2h0LWdyZXktZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINiq2YjZhdin2YZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlci5iYWxhbmNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGlnaHQtZ3JleS1mb250IGNvbC14cy02IHB1bGwtcmlnaHQgYmx1ZS1mb250IHBlcnNpYW4gdGV4dC1hbGlnbi1yaWdodCBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINin2LnYqtio2KfYsVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItin2YHYstin24zYtCDYp9i52KrYqNin2LFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnL2luY3JlYXNlQ3JlZGl0Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYrtix2YjYrFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmF1dGhBY3Rpb25zLmxvZ291dFVzZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHshdGhpcy5wcm9wcy5jdXJyVXNlciAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLZiNix2YjYr1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnL2xvZ2luJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibm8tcGFkZGluZyAgY29sLXhzLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtc21pbGUtbyBmYS0yeCBwdXJwbGUtZm9udFwiIC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleC1jZW50ZXIgY29sLW1kLTQgY29sLW1kLW9mZnNldC02IG5hdi1sb2dvIGNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93ICBcIj5cbiAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImZsZXgtcm93LXJldlwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy8nKTtcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuby1wYWRkaW5nIHRleHQtYWxpZ24tY2VudGVyIGNvbC14cy0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGltZyBjbGFzc05hbWU9XCJpY29uLWNsXCIgc3JjPXtsb2dvSW1nfSBhbHQ9XCJsb2dvXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTlcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwicGVyc2lhbi1ib2xkIHRleHQtYWxpZ24tcmlnaHQgIHBlcnNpYW4gYmx1ZS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICDYrtin2YbZhyDYqNmHINiv2YjYtFxuICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvaGVhZGVyPlxuICAgICk7XG4gIH1cbn1cbmZ1bmN0aW9uIG1hcERpc3BhdGNoVG9Qcm9wcyhkaXNwYXRjaCkge1xuICByZXR1cm4ge1xuICAgIGF1dGhBY3Rpb25zOiBiaW5kQWN0aW9uQ3JlYXRvcnMoYXV0aEFjdGlvbnMsIGRpc3BhdGNoKSxcbiAgfTtcbn1cblxuZnVuY3Rpb24gbWFwU3RhdGVUb1Byb3BzKHN0YXRlKSB7XG4gIGNvbnN0IHsgYXV0aGVudGljYXRpb24gfSA9IHN0YXRlO1xuICBjb25zdCB7IGN1cnJVc2VyLGlzTG9nZ2VkSW4gfSA9IGF1dGhlbnRpY2F0aW9uO1xuICByZXR1cm4ge1xuICAgIGN1cnJVc2VyLFxuICAgIGlzTG9nZ2VkSW4sXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHMpKFBhbmVsKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvUGFuZWwvUGFuZWwuanMiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9QYW5lbC9sb2dvLnBuZz8yYWY3M2Q4YVwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmdcbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbG9nby5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCIvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9uby1kYW5nZXIgKi9cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9tYWluLmNzcyc7XG5cbmNsYXNzIFNlYXJjaEJveCBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlU3VibWl0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgbWF4UHJpY2U6ICcnLFxuICAgICAgbWluQXJlYTogJycsXG4gICAgICBidWlsZGluZ1R5cGU6ICcnLFxuICAgICAgZGVhbFR5cGU6ICcnLFxuICAgIH07XG4gIH1cbiAgaGFuZGxlU3VibWl0ID0gZSA9PiB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIHRoaXMucHJvcHMuaGFuZGxlU3VibWl0KFxuICAgICAgdGhpcy5zdGF0ZS5taW5BcmVhLFxuICAgICAgdGhpcy5zdGF0ZS5kZWFsVHlwZSxcbiAgICAgIHRoaXMuc3RhdGUuYnVpbGRpbmdUeXBlLFxuICAgICAgdGhpcy5zdGF0ZS5tYXhQcmljZSxcbiAgICApO1xuICB9O1xuICB2YWxpZGF0ZSA9IChtYXhQcmljZSwgbWluQXJlYSkgPT4ge1xuICAgIC8vIHRydWUgbWVhbnMgaW52YWxpZCwgc28gb3VyIGNvbmRpdGlvbnMgZ290IHJldmVyc2VkXG4gICAgY29uc3QgcmVnZXggPSBuZXcgUmVnRXhwKC9bXjAtOV0vLCAnZycpO1xuICAgIHJldHVybiB7XG4gICAgICBtYXhQcmljZTogbWF4UHJpY2UubWF0Y2gocmVnZXgpLFxuICAgICAgbWluQXJlYTogbWluQXJlYS5tYXRjaChyZWdleCksXG4gICAgfTtcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IGVycm9ycyA9IHRoaXMudmFsaWRhdGUodGhpcy5zdGF0ZS5tYXhQcmljZSwgdGhpcy5zdGF0ZS5taW5BcmVhKTtcbiAgICBjb25zdCBpc0VuYWJsZWQgPSAhT2JqZWN0LmtleXMoZXJyb3JzKS5zb21lKHggPT4gZXJyb3JzW3hdKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBzZWFyY2gtc2VjdGlvbiBib3JkZXItcmFkaXVzIG5vLXBhZGRpbmdcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgZmlyc3QtcGFydCBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTRcIj5cbiAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJsYWJlbFwiPtmF2KrYsSDZhdix2KjYuTwvbGFiZWw+XG4gICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCLYrdiv2KfZgtmEINmF2KrYsdin2phcIlxuICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IFtlLnRhcmdldC5uYW1lXTogZS50YXJnZXQudmFsdWUgfSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIG5hbWU9XCJtaW5BcmVhXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgc2VhcmNoLWlucHV0IGJvcmRlci1yYWRpdXMgY2xlYXItaW5wdXQgJHtcbiAgICAgICAgICAgICAgICBlcnJvcnMubWluQXJlYSA/ICdlcnJvcicgOiAnJ1xuICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICBkaXI9XCJydGxcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIHtlcnJvcnMubWluQXJlYSAmJiA8cCBjbGFzc05hbWU9XCJlcnItbXNnXCI+2YTYt9mB2Kcg2LnYr9ivINmI2KfYsdivINqp2YbbjNivLjwvcD59XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTRcIj5cbiAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJsYWJlbFwiPtiq2YjZhdin2YY8L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwi2K3Yr9in2qnYq9ixINmC24zZhdiqXCJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBuYW1lPVwibWF4UHJpY2VcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2BzZWFyY2gtaW5wdXQgYm9yZGVyLXJhZGl1cyBjbGVhci1pbnB1dCAke1xuICAgICAgICAgICAgICAgIGVycm9ycy5tYXhQcmljZSA/ICdlcnJvcicgOiAnJ1xuICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICBkaXI9XCJydGxcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIHtlcnJvcnMubWF4UHJpY2UgJiYgPHAgY2xhc3NOYW1lPVwiZXJyLW1zZ1wiPtmE2LfZgdinINi52K/YryDZiNin2LHYryDaqdmG24zYry48L3A+fVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIGNvbC1zbS00XCI+XG4gICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwiYnVpbGRpbmctdHlwZS1sYWJlbFwiIC8+XG4gICAgICAgICAgICA8c2VsZWN0XG4gICAgICAgICAgICAgIHRpdGxlPVwiYnVpbGRpbmdUeXBlXCJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBuYW1lPVwiYnVpbGRpbmdUeXBlXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2VhcmNoLWlucHV0IGJvcmRlci1yYWRpdXMgY2xlYXItaW5wdXRcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8b3B0aW9uIGRpc2FibGVkIHNlbGVjdGVkIGNsYXNzTmFtZT1cInNlbGVjdC1wbGFjZS1ob2xkZXJcIj5cbiAgICAgICAgICAgICAgICDZhtmI2Lkg2YXZhNqpXG4gICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICA8b3B0aW9uPtii2b7Yp9ix2KrZhdin2YYgPC9vcHRpb24+XG4gICAgICAgICAgICAgIDxvcHRpb24+2YjbjNmE2KfbjNuMPC9vcHRpb24+XG4gICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTEyIHNlY29uZC1wYXJ0XCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ2aXNpYmxlLW1kICB2aXNpYmxlLWxnIGNvbC1tZC02IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLmhhbmRsZVN1Ym1pdH0+XG4gICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICBkaXNhYmxlZD17IWlzRW5hYmxlZH1cbiAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzZWFyY2gtYnV0dG9uIGhhcy10cmFuc2l0aW9uIGJvcmRlci1yYWRpdXMgY2xlYXItaW5wdXQgd2hpdGUgbGlnaHQtYmx1ZS1iYWNrZ3JvdW5kXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgINis2LPYqtis2YhcbiAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8L2Zvcm0+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLW1kLTYgZGVhbC10eXBlLXNlY3Rpb24gbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJyYWhuXCIgY2xhc3NOYW1lPVwibGFiZWwgZm9udC1tZWRpdW0gaW5wdXQtbGFiZWxcIj7YsdmH2YYg2Ygg2KfYrNin2LHZhzwvbGFiZWw+XG4gICAgICAgICAgICA8aW5wdXQgaWQ9XCJraGFyaWRcIlxuICAgICAgICAgICAgICB2YWx1ZT17MX1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC5jaGVja2VkID8gMCA6IDEsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndoaXRlIGZvbnQtbGlnaHRcIlxuICAgICAgICAgICAgICB0eXBlPVwicmFkaW9cIlxuICAgICAgICAgICAgICBuYW1lPVwiZGVhbFR5cGVcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwia2hhcmlkXCIgY2xhc3NOYW1lPVwibGFiZWwgZm9udC1tZWRpdW0gaW5wdXQtbGFiZWxcIj7Yrtix24zYrzwvbGFiZWw+XG4gICAgICAgICAgICA8aW5wdXQgaWQ9XCJyYWhuXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2hpdGUgZm9udC1saWdodFwiXG4gICAgICAgICAgICAgIHR5cGU9XCJyYWRpb1wiXG4gICAgICAgICAgICAgIHZhbHVlPXswfVxuICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LmNoZWNrZWQgPyAxIDogMCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgbmFtZT1cImRlYWxUeXBlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ2aXNpYmxlLXhzIHZpc2libGUtc20gaGlkZGVuLW1kIGhpZGRlbi1sZyBjb2wteHMtMTJcIj5cbiAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLmhhbmRsZVN1Ym1pdH0+XG4gICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzZWFyY2gtYnV0dG9uIGhhcy10cmFuc2l0aW9uIGJvcmRlci1yYWRpdXMgY2xlYXItaW5wdXQgd2hpdGUgbGlnaHQtYmx1ZS1iYWNrZ3JvdW5kXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgINis2LPYqtis2YhcbiAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8L2Zvcm0+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFNlYXJjaEJveCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbXBvbmVudHMvU2VhcmNoQm94L1NlYXJjaEJveC5qcyIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL1NlYXJjaEJveC9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9TZWFyY2hCb3gvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIvLyBub2luc3BlY3Rpb24gSlNBbm5vdGF0b3JcbmNvbnN0IGxldmVscyA9IFtcbiAge1xuICAgIE1BWF9XSURUSDogNjAwLFxuICAgIE1BWF9IRUlHSFQ6IDYwMCxcbiAgICBET0NVTUVOVF9TSVpFOiAzMDAwMDAsXG4gIH0sXG4gIHtcbiAgICBNQVhfV0lEVEg6IDgwMCxcbiAgICBNQVhfSEVJR0hUOiA4MDAsXG4gICAgRE9DVU1FTlRfU0laRTogNTAwMDAwLFxuICB9LFxuXTtcblxuZXhwb3J0IGRlZmF1bHQge1xuICBnZXRMZXZlbChsZXZlbCkge1xuICAgIHJldHVybiBsZXZlbHNbbGV2ZWwgLSAxXTtcbiAgfSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbmZpZy9jb21wcmVzc2lvbkNvbmZpZy5qcyIsIi8qIGVzbGludC1kaXNhYmxlIHByZXR0aWVyL3ByZXR0aWVyICovXG5pbXBvcnQgeyBhcGkgfSBmcm9tICcuL3VybHMnO1xuXG5leHBvcnQgY29uc3QgbG9naW5BcGlVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9sb2dpbmA7XG5leHBvcnQgY29uc3QgbG9nb3V0QXBpVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvbG9naW5gO1xuZXhwb3J0IGNvbnN0IGdldFNlYXJjaEhvdXNlc1VybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL1NlYXJjaFNlcnZsZXRgO1xuZXhwb3J0IGNvbnN0IGdldENyZWF0ZUhvdXNlVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0hvdXNlU2VydmxldGA7XG5leHBvcnQgY29uc3QgZ2V0SW5pdGlhdGVVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9ob21lYDtcbmV4cG9ydCBjb25zdCBnZXRDdXJyZW50VXNlclVybCA9ICh1c2VySWQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9JbmRpdmlkdWFsU2VydmxldD91c2VySWQ9JHt1c2VySWR9YDtcbmV4cG9ydCBjb25zdCBnZXRIb3VzZURldGFpbFVybCA9IChob3VzZUlkKSA9PiBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvR2V0U2luZ2xlSG91c2U/aWQ9JHtob3VzZUlkfWA7XG5leHBvcnQgY29uc3QgZ2V0SG91c2VQaG9uZVVybCA9IChob3VzZUlkKSA9PiBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvUGF5bWVudFNlcnZsZXQ/aWQ9JHtob3VzZUlkfWA7XG5leHBvcnQgY29uc3QgaW5jcmVhc2VDcmVkaXRVcmwgPSAoYW1vdW50KSA9PiBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvQ3JlZGl0U2VydmxldD92YWx1ZT0ke2Ftb3VudH1gO1xuXG5leHBvcnQgY29uc3Qgc2VhcmNoRHJpdmVyVXJsID0gcGhvbmVOdW1iZXIgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL3Bob25lTnVtYmVyLyR7cGhvbmVOdW1iZXJ9YDtcbmV4cG9ydCBjb25zdCBnZXREcml2ZXJBY3F1aXNpdGlvbkluZm9VcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IHVwZGF0ZURyaXZlckFjcXVpc2l0aW9uSW5mb1VybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgdXBkYXRlRHJpdmVyRG9jc1VybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9kb2N1bWVudHMvJHtkcml2ZXJJZH1gO1xuZXhwb3J0IGNvbnN0IGFwcHJvdmVEcml2ZXJVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvYXBwcm92ZS8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgcmVqZWN0RHJpdmVyVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL3JlamVjdC8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgZ2V0RHJpdmVyRG9jdW1lbnRVcmwgPSBkb2N1bWVudElkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9kb2N1bWVudC8ke2RvY3VtZW50SWR9YDtcbmV4cG9ydCBjb25zdCBnZXREcml2ZXJEb2N1bWVudElkc1VybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9kb2N1bWVudHMvJHtkcml2ZXJJZH1gO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb25maWcvcGF0aHMuanMiLCIvKipcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcbiAqXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICovXG5cbmltcG9ydCBjcmVhdGVCcm93c2VySGlzdG9yeSBmcm9tICdoaXN0b3J5L2NyZWF0ZUJyb3dzZXJIaXN0b3J5JztcblxuLy8gTmF2aWdhdGlvbkJhciBtYW5hZ2VyLCBlLmcuIGhpc3RvcnkucHVzaCgnL2hvbWUnKVxuLy8gaHR0cHM6Ly9naXRodWIuY29tL21qYWNrc29uL2hpc3RvcnlcbmV4cG9ydCBkZWZhdWx0IHByb2Nlc3MuZW52LkJST1dTRVIgJiYgY3JlYXRlQnJvd3Nlckhpc3RvcnkoKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvaGlzdG9yeS5qcyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCAqIGFzIHNlYXJjaEFjdGlvbnMgZnJvbSAnLi4vLi4vYWN0aW9ucy9zZWFyY2gnO1xuaW1wb3J0IEhvdXNlc1BhZ2UgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9Ib3VzZXNQYWdlJztcbmltcG9ydCB7IG5vdGlmeUVycm9yIH0gZnJvbSAnLi4vLi4vdXRpbHMnO1xuXG5jbGFzcyBIb3VzZXNDb250YWluZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIHNlYXJjaEFjdGlvbnM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBob3VzZXM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxuICAgIG1pbkFyZWE6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgICBkZWFsVHlwZTogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAgIGJ1aWxkaW5nVHlwZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIG1heFByaWNlOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gICAgaXNGZXRjaGluZzogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgfTtcbiAgY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgIHRoaXMucHJvcHMuc2VhcmNoQWN0aW9ucy5zZWFyY2hIb3VzZXMoXG4gICAgICB0aGlzLnByb3BzLm1pbkFyZWEsXG4gICAgICB0aGlzLnByb3BzLmRlYWxUeXBlLFxuICAgICAgdGhpcy5wcm9wcy5idWlsZGluZ1R5cGUsXG4gICAgICB0aGlzLnByb3BzLm1heFByaWNlLFxuICAgICk7XG4gIH1cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcbiAgICBpZiAobmV4dFByb3BzLmhvdXNlcyAhPT0gdGhpcy5wcm9wcy5ob3VzZXMpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRpbWVvdXQpO1xuXG4gICAgICAvLyBPcHRpb25hbGx5IGRvIHNvbWV0aGluZyB3aXRoIGRhdGFcblxuICAgICAgaWYgKCFuZXh0UHJvcHMuaXNGZXRjaGluZykge1xuICAgICAgICB0aGlzLnN0YXJ0UG9sbCgpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KTtcbiAgfVxuXG4gIGhhbmRsZVN1Ym1pdCA9IChtaW5BcmVhLCBkZWFsVHlwZSwgYnVpbGRpbmdUeXBlLCBtYXhQcmljZSkgPT4ge1xuICAgIHRoaXMucHJvcHMuc2VhcmNoQWN0aW9ucy5zZWFyY2hIb3VzZXMoXG4gICAgICBtaW5BcmVhLFxuICAgICAgZGVhbFR5cGUsXG4gICAgICBidWlsZGluZ1R5cGUsXG4gICAgICBtYXhQcmljZSxcbiAgICApO1xuICB9O1xuICBzdGFydFBvbGwoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5taW5BcmVhKTtcbiAgICB0aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMucHJvcHMuc2VhcmNoQWN0aW9ucy5zZWFyY2hIb3VzZXMoXG4gICAgICAgIHRoaXMucHJvcHMubWluQXJlYSxcbiAgICAgICAgdGhpcy5wcm9wcy5kZWFsVHlwZSxcbiAgICAgICAgdGhpcy5wcm9wcy5idWlsZGluZ1R5cGUsXG4gICAgICAgIHRoaXMucHJvcHMubWF4UHJpY2UsXG4gICAgICApO1xuICAgIH0sIDI1MDAwKTtcbiAgfVxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxIb3VzZXNQYWdlXG4gICAgICAgIGxvYWRpbmc9e3RoaXMucHJvcHMuaXNGZXRjaGluZ31cbiAgICAgICAgaG91c2VzPXt0aGlzLnByb3BzLmhvdXNlc31cbiAgICAgICAgaGFuZGxlU3VibWl0PXt0aGlzLmhhbmRsZVN1Ym1pdH1cbiAgICAgIC8+XG4gICAgKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBtYXBEaXNwYXRjaFRvUHJvcHMoZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIHtcbiAgICBzZWFyY2hBY3Rpb25zOiBiaW5kQWN0aW9uQ3JlYXRvcnMoc2VhcmNoQWN0aW9ucywgZGlzcGF0Y2gpLFxuICB9O1xufVxuXG5mdW5jdGlvbiBtYXBTdGF0ZVRvUHJvcHMoc3RhdGUpIHtcbiAgY29uc3QgeyBzZWFyY2ggfSA9IHN0YXRlO1xuICBjb25zdCB7XG4gICAgaXNGZXRjaGluZyxcbiAgICBob3VzZXMsXG4gICAgbWluQXJlYSxcbiAgICBkZWFsVHlwZSxcbiAgICBidWlsZGluZ1R5cGUsXG4gICAgbWF4UHJpY2UsXG4gIH0gPSBzZWFyY2g7XG4gIHJldHVybiB7XG4gICAgaXNGZXRjaGluZyxcbiAgICBob3VzZXMsXG4gICAgbWluQXJlYSxcbiAgICBkZWFsVHlwZSxcbiAgICBidWlsZGluZ1R5cGUsXG4gICAgbWF4UHJpY2UsXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKEhvdXNlc0NvbnRhaW5lcik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3JvdXRlcy9ob3VzZXMvSG91c2VzQ29udGFpbmVyLmpzIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBIb3VzZXNDb250YWluZXIgZnJvbSAnLi9Ib3VzZXNDb250YWluZXInO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmNvbnN0IHRpdGxlID0gJ9iu2KfZhtmHINmH2KcnO1xuXG5mdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgY2h1bmtzOiBbJ2hvdXNlcyddLFxuICAgIHRpdGxlLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPEhvdXNlc0NvbnRhaW5lciAvPixcbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvcm91dGVzL2hvdXNlcy9pbmRleC5qcyIsImltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgRVhJRiBmcm9tICdleGlmLWpzJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uL2hpc3RvcnknO1xuaW1wb3J0IGNvbXByZXNzaW9uQ29uZmlnIGZyb20gJy4uL2NvbmZpZy9jb21wcmVzc2lvbkNvbmZpZyc7XG5pbXBvcnQgeyBsb2FkTG9naW5TdGF0dXMgfSBmcm9tICcuLi9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uJztcblxuY29uc3QgY29tcHJlc3Npb25GaXJzdExldmVsID0gY29tcHJlc3Npb25Db25maWcuZ2V0TGV2ZWwoMSk7XG5jb25zdCBjb21wcmVzc2lvblNlY29uZExldmVsID0gY29tcHJlc3Npb25Db25maWcuZ2V0TGV2ZWwoMik7XG5jb25zdCBkb2N1bWVudHNDb21wcmVzc2lvbkxldmVsID0ge1xuICBwcm9maWxlUGljdHVyZTogY29tcHJlc3Npb25GaXJzdExldmVsLFxuICBkcml2ZXJMaWNlbmNlOiBjb21wcmVzc2lvbkZpcnN0TGV2ZWwsXG4gIGNhcklkQ2FyZDogY29tcHJlc3Npb25GaXJzdExldmVsLFxuICBpbnN1cmFuY2U6IGNvbXByZXNzaW9uU2Vjb25kTGV2ZWwsXG59O1xuXG5mdW5jdGlvbiBnZXRDb21wcmVzc2lvbkxldmVsKGRvY3VtZW50VHlwZSkge1xuICByZXR1cm4gZG9jdW1lbnRzQ29tcHJlc3Npb25MZXZlbFtkb2N1bWVudFR5cGVdIHx8IGNvbXByZXNzaW9uRmlyc3RMZXZlbDtcbn1cblxuZnVuY3Rpb24gZGF0YVVSTHRvQmxvYihkYXRhVVJMLCBmaWxlVHlwZSkge1xuICBjb25zdCBiaW5hcnkgPSBhdG9iKGRhdGFVUkwuc3BsaXQoJywnKVsxXSk7XG4gIGNvbnN0IGFycmF5ID0gW107XG4gIGNvbnN0IGxlbmd0aCA9IGJpbmFyeS5sZW5ndGg7XG4gIGxldCBpO1xuICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICBhcnJheS5wdXNoKGJpbmFyeS5jaGFyQ29kZUF0KGkpKTtcbiAgfVxuICByZXR1cm4gbmV3IEJsb2IoW25ldyBVaW50OEFycmF5KGFycmF5KV0sIHsgdHlwZTogZmlsZVR5cGUgfSk7XG59XG5cbmZ1bmN0aW9uIGdldFNjYWxlZEltYWdlTWVhc3VyZW1lbnRzKHdpZHRoLCBoZWlnaHQsIGRvY3VtZW50VHlwZSkge1xuICBjb25zdCB7IE1BWF9IRUlHSFQsIE1BWF9XSURUSCB9ID0gZ2V0Q29tcHJlc3Npb25MZXZlbChkb2N1bWVudFR5cGUpO1xuXG4gIGlmICh3aWR0aCA+IGhlaWdodCkge1xuICAgIGhlaWdodCAqPSBNQVhfV0lEVEggLyB3aWR0aDtcbiAgICB3aWR0aCA9IE1BWF9XSURUSDtcbiAgfSBlbHNlIHtcbiAgICB3aWR0aCAqPSBNQVhfSEVJR0hUIC8gaGVpZ2h0O1xuICAgIGhlaWdodCA9IE1BWF9IRUlHSFQ7XG4gIH1cbiAgcmV0dXJuIHtcbiAgICBoZWlnaHQsXG4gICAgd2lkdGgsXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldEltYWdlT3JpZW50YXRpb24oaW1hZ2UpIHtcbiAgbGV0IG9yaWVudGF0aW9uID0gMDtcbiAgRVhJRi5nZXREYXRhKGltYWdlLCBmdW5jdGlvbigpIHtcbiAgICBvcmllbnRhdGlvbiA9IEVYSUYuZ2V0VGFnKHRoaXMsICdPcmllbnRhdGlvbicpO1xuICB9KTtcbiAgcmV0dXJuIG9yaWVudGF0aW9uO1xufVxuXG5mdW5jdGlvbiBnZXRJbWFnZU1lYXN1cmVtZW50cyhpbWFnZSkge1xuICBjb25zdCBvcmllbnRhdGlvbiA9IGdldEltYWdlT3JpZW50YXRpb24oaW1hZ2UpO1xuICBsZXQgdHJhbnNmb3JtO1xuICBsZXQgc3dhcCA9IGZhbHNlO1xuICBzd2l0Y2ggKG9yaWVudGF0aW9uKSB7XG4gICAgY2FzZSA4OlxuICAgICAgc3dhcCA9IHRydWU7XG4gICAgICB0cmFuc2Zvcm0gPSAnbGVmdCc7XG4gICAgICBicmVhaztcbiAgICBjYXNlIDY6XG4gICAgICBzd2FwID0gdHJ1ZTtcbiAgICAgIHRyYW5zZm9ybSA9ICdyaWdodCc7XG4gICAgICBicmVhaztcbiAgICBjYXNlIDM6XG4gICAgICB0cmFuc2Zvcm0gPSAnZmxpcCc7XG4gICAgICBicmVhaztcbiAgICBkZWZhdWx0OlxuICB9XG4gIGNvbnN0IHdpZHRoID0gc3dhcCA/IGltYWdlLmhlaWdodCA6IGltYWdlLndpZHRoO1xuICBjb25zdCBoZWlnaHQgPSBzd2FwID8gaW1hZ2Uud2lkdGggOiBpbWFnZS5oZWlnaHQ7XG4gIHJldHVybiB7XG4gICAgd2lkdGgsXG4gICAgaGVpZ2h0LFxuICAgIHRyYW5zZm9ybSxcbiAgfTtcbn1cblxuZnVuY3Rpb24gZ2V0U2NhbGVkSW1hZ2VGaWxlRnJvbUNhbnZhcyhcbiAgaW1hZ2UsXG4gIHdpZHRoLFxuICBoZWlnaHQsXG4gIHRyYW5zZm9ybSxcbiAgZmlsZVR5cGUsXG4pIHtcbiAgY29uc3QgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XG4gIGNhbnZhcy53aWR0aCA9IHdpZHRoO1xuICBjYW52YXMuaGVpZ2h0ID0gaGVpZ2h0O1xuICBjb25zdCBjb250ZXh0ID0gY2FudmFzLmdldENvbnRleHQoJzJkJyk7XG4gIGxldCB0cmFuc2Zvcm1QYXJhbXM7XG4gIGxldCBzd2FwTWVhc3VyZSA9IGZhbHNlO1xuICBsZXQgaW1hZ2VXaWR0aCA9IHdpZHRoO1xuICBsZXQgaW1hZ2VIZWlnaHQgPSBoZWlnaHQ7XG4gIHN3aXRjaCAodHJhbnNmb3JtKSB7XG4gICAgY2FzZSAnbGVmdCc6XG4gICAgICB0cmFuc2Zvcm1QYXJhbXMgPSBbMCwgLTEsIDEsIDAsIDAsIGhlaWdodF07XG4gICAgICBzd2FwTWVhc3VyZSA9IHRydWU7XG4gICAgICBicmVhaztcbiAgICBjYXNlICdyaWdodCc6XG4gICAgICB0cmFuc2Zvcm1QYXJhbXMgPSBbMCwgMSwgLTEsIDAsIHdpZHRoLCAwXTtcbiAgICAgIHN3YXBNZWFzdXJlID0gdHJ1ZTtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ2ZsaXAnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzEsIDAsIDAsIC0xLCAwLCBoZWlnaHRdO1xuICAgICAgYnJlYWs7XG4gICAgZGVmYXVsdDpcbiAgICAgIHRyYW5zZm9ybVBhcmFtcyA9IFsxLCAwLCAwLCAxLCAwLCAwXTtcbiAgfVxuICBjb250ZXh0LnNldFRyYW5zZm9ybSguLi50cmFuc2Zvcm1QYXJhbXMpO1xuICBpZiAoc3dhcE1lYXN1cmUpIHtcbiAgICBpbWFnZUhlaWdodCA9IHdpZHRoO1xuICAgIGltYWdlV2lkdGggPSBoZWlnaHQ7XG4gIH1cbiAgY29udGV4dC5kcmF3SW1hZ2UoaW1hZ2UsIDAsIDAsIGltYWdlV2lkdGgsIGltYWdlSGVpZ2h0KTtcbiAgY29udGV4dC5zZXRUcmFuc2Zvcm0oMSwgMCwgMCwgMSwgMCwgMCk7XG4gIGNvbnN0IGRhdGFVUkwgPSBjYW52YXMudG9EYXRhVVJMKGZpbGVUeXBlKTtcbiAgcmV0dXJuIGRhdGFVUkx0b0Jsb2IoZGF0YVVSTCwgZmlsZVR5cGUpO1xufVxuXG5mdW5jdGlvbiBzaG91bGRSZXNpemUoaW1hZ2VNZWFzdXJlbWVudHMsIGZpbGVTaXplLCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgeyBNQVhfSEVJR0hULCBNQVhfV0lEVEgsIERPQ1VNRU5UX1NJWkUgfSA9IGdldENvbXByZXNzaW9uTGV2ZWwoXG4gICAgZG9jdW1lbnRUeXBlLFxuICApO1xuICBpZiAoZG9jdW1lbnRUeXBlID09PSAnaW5zdXJhbmNlJyAmJiBmaWxlU2l6ZSA8IERPQ1VNRU5UX1NJWkUpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH0gZWxzZSBpZiAoZmlsZVNpemUgPCBET0NVTUVOVF9TSVpFKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHJldHVybiAoXG4gICAgaW1hZ2VNZWFzdXJlbWVudHMud2lkdGggPiBNQVhfV0lEVEggfHwgaW1hZ2VNZWFzdXJlbWVudHMuaGVpZ2h0ID4gTUFYX0hFSUdIVFxuICApO1xufVxuXG5hc3luYyBmdW5jdGlvbiBwcm9jZXNzRmlsZShkYXRhVVJMLCBmaWxlU2l6ZSwgZmlsZVR5cGUsIGRvY3VtZW50VHlwZSkge1xuICBjb25zdCBpbWFnZSA9IG5ldyBJbWFnZSgpO1xuICBpbWFnZS5zcmMgPSBkYXRhVVJMO1xuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIGltYWdlLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgY29uc3QgaW1hZ2VNZWFzdXJlbWVudHMgPSBnZXRJbWFnZU1lYXN1cmVtZW50cyhpbWFnZSk7XG4gICAgICBpZiAoIXNob3VsZFJlc2l6ZShpbWFnZU1lYXN1cmVtZW50cywgZmlsZVNpemUsIGRvY3VtZW50VHlwZSkpIHtcbiAgICAgICAgcmVzb2x2ZShkYXRhVVJMdG9CbG9iKGRhdGFVUkwsIGZpbGVUeXBlKSk7XG4gICAgICB9XG4gICAgICBjb25zdCBzY2FsZWRJbWFnZU1lYXN1cmVtZW50cyA9IGdldFNjYWxlZEltYWdlTWVhc3VyZW1lbnRzKFxuICAgICAgICBpbWFnZU1lYXN1cmVtZW50cy53aWR0aCxcbiAgICAgICAgaW1hZ2VNZWFzdXJlbWVudHMuaGVpZ2h0LFxuICAgICAgICBkb2N1bWVudFR5cGUsXG4gICAgICApO1xuICAgICAgY29uc3Qgc2NhbGVkSW1hZ2VGaWxlID0gZ2V0U2NhbGVkSW1hZ2VGaWxlRnJvbUNhbnZhcyhcbiAgICAgICAgaW1hZ2UsXG4gICAgICAgIHNjYWxlZEltYWdlTWVhc3VyZW1lbnRzLndpZHRoLFxuICAgICAgICBzY2FsZWRJbWFnZU1lYXN1cmVtZW50cy5oZWlnaHQsXG4gICAgICAgIGltYWdlTWVhc3VyZW1lbnRzLnRyYW5zZm9ybSxcbiAgICAgICAgZmlsZVR5cGUsXG4gICAgICApO1xuICAgICAgcmVzb2x2ZShzY2FsZWRJbWFnZUZpbGUpO1xuICAgIH07XG4gICAgaW1hZ2Uub25lcnJvciA9IHJlamVjdDtcbiAgfSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjb21wcmVzc0ZpbGUoZmlsZSwgZG9jdW1lbnRUeXBlID0gJ2RlZmF1bHQnKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG4gICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcbiAgICAgIHJlYWRlci5vbmxvYWRlbmQgPSBhc3luYyBmdW5jdGlvbigpIHtcbiAgICAgICAgY29uc3QgY29tcHJlc3NlZEZpbGUgPSBhd2FpdCBwcm9jZXNzRmlsZShcbiAgICAgICAgICByZWFkZXIucmVzdWx0LFxuICAgICAgICAgIGZpbGUuc2l6ZSxcbiAgICAgICAgICBmaWxlLnR5cGUsXG4gICAgICAgICAgZG9jdW1lbnRUeXBlLFxuICAgICAgICApO1xuICAgICAgICByZXNvbHZlKGNvbXByZXNzZWRGaWxlKTtcbiAgICAgIH07XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICByZWplY3QoZXJyKTtcbiAgICB9XG4gIH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TG9naW5JbmZvKCkge1xuICB0cnkge1xuICAgIGNvbnN0IGxvZ2luSW5mbyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2dpbkluZm8nKTtcbiAgICBpZiAoXy5pc1N0cmluZyhsb2dpbkluZm8pKSB7XG4gICAgICByZXR1cm4gSlNPTi5wYXJzZShsb2dpbkluZm8pO1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coJ0Vycm9yIHdoZW4gcGFyc2luZyBsb2dpbkluZm8gb2JqZWN0OiAnLCBlcnIpO1xuICB9XG4gIHJldHVybiBudWxsO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldFBvc3RDb25maWcoYm9keSkge1xuICBjb25zdCBsb2dpbkluZm8gPSBnZXRMb2dpbkluZm8oKTtcbiAgY29uc3QgaGVhZGVycyA9IGxvZ2luSW5mb1xuICAgID8ge1xuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBtaW1lVHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9naW5JbmZvLnRva2VufWAsXG4gICAgICB9XG4gICAgOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsIG1pbWVUeXBlOiAnYXBwbGljYXRpb24vanNvbicgfTtcbiAgcmV0dXJuIHtcbiAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAvLyBtb2RlOiAnbm8tY29ycycsXG4gICAgaGVhZGVycyxcbiAgICAuLi4oXy5pc0VtcHR5KGJvZHkpID8ge30gOiB7IGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpIH0pLFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UHV0Q29uZmlnKGJvZHkpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ1gtQXV0aG9yaXphdGlvbic6IGxvZ2luSW5mby50b2tlbixcbiAgICAgIH1cbiAgICA6IHsgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyB9O1xuICByZXR1cm4ge1xuICAgIG1ldGhvZDogJ1BVVCcsXG4gICAgaGVhZGVycyxcbiAgICAuLi4oXy5pc0VtcHR5KGJvZHkpID8ge30gOiB7IGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpIH0pLFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0R2V0Q29uZmlnKCkge1xuICBjb25zdCBsb2dpbkluZm8gPSBnZXRMb2dpbkluZm8oKTtcbiAgY29uc3QgaGVhZGVycyA9IGxvZ2luSW5mb1xuICAgID8ge1xuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9naW5JbmZvLnRva2VufWAsXG4gICAgICB9XG4gICAgOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfTtcbiAgcmV0dXJuIHtcbiAgICBtZXRob2Q6ICdHRVQnLFxuICAgIGhlYWRlcnMsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjb252ZXJ0TnVtYmVyc1RvUGVyc2lhbihzdHIpIHtcbiAgY29uc3QgcGVyc2lhbk51bWJlcnMgPSAn27Dbsduy27PbtNu127bbt9u427knO1xuICBsZXQgcmVzdWx0ID0gJyc7XG4gIGlmICghc3RyICYmIHN0ciAhPT0gMCkge1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbiAgc3RyID0gc3RyLnRvU3RyaW5nKCk7XG4gIGxldCBjb252ZXJ0ZWRDaGFyO1xuICBmb3IgKGxldCBpID0gMDsgaSA8IHN0ci5sZW5ndGg7IGkrKykge1xuICAgIHRyeSB7XG4gICAgICBjb252ZXJ0ZWRDaGFyID0gcGVyc2lhbk51bWJlcnNbc3RyW2ldXTtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICBjb252ZXJ0ZWRDaGFyID0gc3RyW2ldO1xuICAgIH1cbiAgICByZXN1bHQgKz0gY29udmVydGVkQ2hhcjtcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZmV0Y2hBcGkodXJsLCBjb25maWcpIHtcbiAgbGV0IGZsb3dFcnJvcjtcbiAgdHJ5IHtcbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIGNvbmZpZyk7XG4gICAgY29uc29sZS5sb2cocmVzKTtcbiAgICBjb25zb2xlLmxvZyhyZXMuc3RhdHVzKTtcbiAgICBsZXQgcmVzdWx0ID0gYXdhaXQgcmVzLmpzb24oKTtcbiAgICBjb25zb2xlLmxvZyhyZXN1bHQpO1xuICAgIGlmICghXy5pc0VtcHR5KF8uZ2V0KHJlc3VsdCwgJ2RhdGEnKSkpIHtcbiAgICAgIHJlc3VsdCA9IHJlc3VsdC5kYXRhO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyhyZXMuc3RhdHVzKTtcblxuICAgIGlmIChyZXMuc3RhdHVzICE9PSAyMDApIHtcbiAgICAgIGZsb3dFcnJvciA9IHJlc3VsdDtcbiAgICAgIGNvbnNvbGUubG9nKHJlcy5zdGF0dXMpO1xuXG4gICAgICBpZiAocmVzLnN0YXR1cyA9PT0gNDAzKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xuICAgICAgICBmbG93RXJyb3IubWVzc2FnZSA9ICfZhNi32YHYpyDYr9mI2KjYp9ix2Ycg2YjYp9ix2K8g2LTZiNuM2K8hJztcbiAgICAgICAgY29uc29sZS5sb2coJ3llZWVzJyk7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4gaGlzdG9yeS5wdXNoKCcvbG9naW4nKSwgMzAwMCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgIGNvbnNvbGUuZGVidWcoYEVycm9yIHdoZW4gZmV0Y2hpbmcgYXBpOiAke3VybH1gLCBlcnIpO1xuICAgIGZsb3dFcnJvciA9IHtcbiAgICAgIGNvZGU6ICdVTktOT1dOX0VSUk9SJyxcbiAgICAgIG1lc3NhZ2U6ICfYrti32KfbjCDZhtin2YXYtNiu2LUg2YTYt9mB2Kcg2K/ZiNio2KfYsdmHINiz2LnbjCDaqdmG24zYrycsXG4gICAgfTtcbiAgfVxuICBpZiAoZmxvd0Vycm9yKSB7XG4gICAgdGhyb3cgZmxvd0Vycm9yO1xuICB9XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBzZXRJbW1lZGlhdGUoZm4pIHtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKHJlc29sdmUgPT4ge1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgbGV0IHZhbHVlID0gbnVsbDtcbiAgICAgIGlmIChfLmlzRnVuY3Rpb24oZm4pKSB7XG4gICAgICAgIHZhbHVlID0gZm4oKTtcbiAgICAgIH1cbiAgICAgIHJlc29sdmUodmFsdWUpO1xuICAgIH0sIDApO1xuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNldENvb2tpZShuYW1lLCB2YWx1ZSwgZGF5cyA9IDcpIHtcbiAgbGV0IGV4cGlyZXMgPSAnJztcbiAgaWYgKGRheXMpIHtcbiAgICBjb25zdCBkYXRlID0gbmV3IERhdGUoKTtcbiAgICBkYXRlLnNldFRpbWUoZGF0ZS5nZXRUaW1lKCkgKyBkYXlzICogMjQgKiA2MCAqIDYwICogMTAwMCk7XG4gICAgZXhwaXJlcyA9IGA7IGV4cGlyZXM9JHtkYXRlLnRvVVRDU3RyaW5nKCl9YDtcbiAgfVxuICBkb2N1bWVudC5jb29raWUgPSBgJHtuYW1lfT0ke3ZhbHVlIHx8ICcnfSR7ZXhwaXJlc307IHBhdGg9L2A7XG59XG5leHBvcnQgZnVuY3Rpb24gZ2V0Q29va2llKG5hbWUpIHtcbiAgY29uc3QgbmFtZUVRID0gYCR7bmFtZX09YDtcbiAgY29uc3QgY2EgPSBkb2N1bWVudC5jb29raWUuc3BsaXQoJzsnKTtcbiAgY29uc3QgbGVuZ3RoID0gY2EubGVuZ3RoO1xuICBsZXQgaSA9IDA7XG4gIGZvciAoOyBpIDwgbGVuZ3RoOyBpICs9IDEpIHtcbiAgICBsZXQgYyA9IGNhW2ldO1xuICAgIHdoaWxlIChjLmNoYXJBdCgwKSA9PT0gJyAnKSB7XG4gICAgICBjID0gYy5zdWJzdHJpbmcoMSwgYy5sZW5ndGgpO1xuICAgIH1cbiAgICBpZiAoYy5pbmRleE9mKG5hbWVFUSkgPT09IDApIHtcbiAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lRVEubGVuZ3RoLCBjLmxlbmd0aCk7XG4gICAgfVxuICB9XG4gIHJldHVybiBudWxsO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZXJhc2VDb29raWUobmFtZSkge1xuICBkb2N1bWVudC5jb29raWUgPSBgJHtuYW1lfT07IE1heC1BZ2U9LTk5OTk5OTk5O2A7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBub3RpZnlFcnJvcihtZXNzYWdlKSB7XG4gIHRvYXN0ci5vcHRpb25zID0ge1xuICAgIGNsb3NlQnV0dG9uOiBmYWxzZSxcbiAgICBkZWJ1ZzogZmFsc2UsXG4gICAgbmV3ZXN0T25Ub3A6IGZhbHNlLFxuICAgIHByb2dyZXNzQmFyOiBmYWxzZSxcbiAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtdG9wLWNlbnRlcicsXG4gICAgcHJldmVudER1cGxpY2F0ZXM6IHRydWUsXG4gICAgb25jbGljazogbnVsbCxcbiAgICBzaG93RHVyYXRpb246ICczMDAnLFxuICAgIGhpZGVEdXJhdGlvbjogJzEwMDAnLFxuICAgIHRpbWVPdXQ6ICcxMDAwMCcsXG4gICAgZXh0ZW5kZWRUaW1lT3V0OiAnMTAwMCcsXG4gICAgc2hvd0Vhc2luZzogJ3N3aW5nJyxcbiAgICBoaWRlRWFzaW5nOiAnbGluZWFyJyxcbiAgICBzaG93TWV0aG9kOiAnc2xpZGVEb3duJyxcbiAgICBoaWRlTWV0aG9kOiAnc2xpZGVVcCcsXG4gIH07XG4gIHRvYXN0ci5lcnJvcihtZXNzYWdlLCAn2K7Yt9inJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBub3RpZnlTdWNjZXNzKG1lc3NhZ2UpIHtcbiAgdG9hc3RyLm9wdGlvbnMgPSB7XG4gICAgY2xvc2VCdXR0b246IGZhbHNlLFxuICAgIGRlYnVnOiBmYWxzZSxcbiAgICBuZXdlc3RPblRvcDogZmFsc2UsXG4gICAgcHJvZ3Jlc3NCYXI6IGZhbHNlLFxuICAgIHBvc2l0aW9uQ2xhc3M6ICd0b2FzdC10b3AtY2VudGVyJyxcbiAgICBwcmV2ZW50RHVwbGljYXRlczogdHJ1ZSxcbiAgICBvbmNsaWNrOiBudWxsLFxuICAgIHNob3dEdXJhdGlvbjogJzMwMCcsXG4gICAgaGlkZUR1cmF0aW9uOiAnMTAwMCcsXG4gICAgdGltZU91dDogJzUwMDAnLFxuICAgIGV4dGVuZGVkVGltZU91dDogJzEwMDAnLFxuICAgIHNob3dFYXNpbmc6ICdzd2luZycsXG4gICAgaGlkZUVhc2luZzogJ2xpbmVhcicsXG4gICAgc2hvd01ldGhvZDogJ3NsaWRlRG93bicsXG4gICAgaGlkZU1ldGhvZDogJ3NsaWRlVXAnLFxuICB9O1xuICB0b2FzdHIuc3VjY2VzcyhtZXNzYWdlLCAnJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiByZWxvYWRTZWxlY3RQaWNrZXIoKSB7XG4gIHRyeSB7XG4gICAgJCgnLnNlbGVjdHBpY2tlcicpLnNlbGVjdHBpY2tlcigncmVuZGVyJyk7XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUubG9nKGVycik7XG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRTZWxlY3RQaWNrZXIoKSB7XG4gIHRyeSB7XG4gICAgJCgnLnNlbGVjdHBpY2tlcicpLnNlbGVjdHBpY2tlcih7fSk7XG4gICAgaWYgKFxuICAgICAgL0FuZHJvaWR8d2ViT1N8aVBob25lfGlQYWR8aVBvZHxCbGFja0JlcnJ5L2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KVxuICAgICkge1xuICAgICAgJCgnLnNlbGVjdHBpY2tlcicpLnNlbGVjdHBpY2tlcignbW9iaWxlJyk7XG4gICAgfVxuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmxvZyhlcnIpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRGb3JtRGF0YShkYXRhKSB7XG4gIGlmIChfLmlzRW1wdHkoZGF0YSkpIHtcbiAgICB0aHJvdyBFcnJvcignSW52YWxpZCBpbnB1dCBkYXRhIHRvIGNyZWF0ZSBhIEZvcm1EYXRhIG9iamVjdCcpO1xuICB9XG4gIGxldCBmb3JtRGF0YTtcbiAgdHJ5IHtcbiAgICBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgIGNvbnN0IGtleXMgPSBfLmtleXMoZGF0YSk7XG4gICAgbGV0IGk7XG4gICAgY29uc3QgbGVuZ3RoID0ga2V5cy5sZW5ndGg7XG4gICAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICBmb3JtRGF0YS5hcHBlbmQoa2V5c1tpXSwgZGF0YVtrZXlzW2ldXSk7XG4gICAgfVxuICAgIHJldHVybiBmb3JtRGF0YTtcbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5kZWJ1ZyhlcnIpO1xuICAgIHRocm93IEVycm9yKCdGT1JNX0RBVEFfQlJPV1NFUl9TVVBQT1JUX0ZBSUxVUkUnKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9hZFVzZXJMb2dpblN0YXR1cyhzdG9yZSkge1xuICBjb25zdCBsb2dpblN0YXR1c0FjdGlvbiA9IGxvYWRMb2dpblN0YXR1cygpO1xuICBzdG9yZS5kaXNwYXRjaChsb2dpblN0YXR1c0FjdGlvbik7XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3V0aWxzL2luZGV4LmpzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQXFCQTtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUZBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hSQTtBQVdBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhCQTtBQUFBO0FBQUE7QUFBQTtBQXdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhDQTtBQUFBO0FBQUE7QUFBQTtBQWdDQTs7Ozs7Ozs7QUNoSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBRkE7QUFLQTs7Ozs7Ozs7QUNoQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU9BO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBO0FBTUE7QUFJQTtBQUNBO0FBQ0E7QUFNQTtBQUlBOzs7Ozs7O0FDekRBOzs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFmQTtBQURBO0FBeUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUExQkE7QUFEQTtBQW9DQTtBQXZDQTtBQUNBO0FBeUNBOzs7Ozs7O0FDbkRBOzs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQVNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFEQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFEQTtBQVBBO0FBZUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBREE7QUFRQTtBQXBDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUE2QkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQURBO0FBREE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBUEE7QUFEQTtBQUZBO0FBY0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFHQTtBQUhBO0FBQUE7QUFEQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBREE7QUFUQTtBQWVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBOUJBO0FBTkE7QUFGQTtBQTZDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBREE7QUFGQTtBQURBO0FBTkE7QUFuREE7QUFaQTtBQURBO0FBOEZBO0FBcklBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFGQTtBQXVJQTs7Ozs7OztBQ2pKQTs7Ozs7OztBQ0FBOzs7Ozs7O0FDQUE7Ozs7Ozs7QUNBQTs7Ozs7OztBQ0FBOzs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDN0JBOzs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIQTtBQU1BO0FBaEJBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFEQTtBQWNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUZBO0FBREE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTNCQTtBQXVDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFDQTtBQURBO0FBSkE7QUE4REE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBL0RBO0FBREE7QUFEQTtBQURBO0FBREE7QUFEQTtBQThFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFGQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEzQkE7QUF1Q0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUExQ0E7QUFEQTtBQUpBO0FBOERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQS9EQTtBQURBO0FBREE7QUF1RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVRBO0FBREE7QUFEQTtBQURBO0FBeEVBO0FBREE7QUFEQTtBQW1HQTtBQWhNQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQWlNQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTs7Ozs7OztBQzVOQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQVVBO0FBQ0E7QUFNQTtBQUNBO0FBbEJBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUF4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkQTtBQWdCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWRBO0FBZ0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWkE7QUFGQTtBQWpDQTtBQW1EQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWRBO0FBMEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQURBO0FBdENBO0FBcERBO0FBdUdBO0FBMUlBO0FBQ0E7QUFEQTtBQUVBO0FBREE7QUE0SUE7Ozs7Ozs7QUNuSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQzdCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTs7Ozs7Ozs7QUNkQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7QUNwQkE7QUFBQTtBQUFBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDYkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQWtDQTtBQU1BO0FBeENBO0FBQ0E7QUFTQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUE1REE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQThEQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDbEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBSkE7QUFTQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDc0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBS0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExQkE7Ozs7Ozs7QUF4SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBc0NBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==