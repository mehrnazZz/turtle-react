require("source-map-support").install();
exports.ids = [5];
exports.modules = {

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/node_modules/normalize.css/normalize.css"],"names":[],"mappings":"AAAA,4EAA4E;AAC5E;gFACgF;AAChF;;;;GAIG;AACH;EACE,kBAAkB;EAClB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,+BAA+B;EAC/B,OAAO,EAAE;AACX;gFACgF;AAChF;;GAEG;AACH;EACE,UAAU,EAAE;AACd;;GAEG;AACH;;;;;;EAME,eAAe,EAAE;AACnB;;;GAGG;AACH;EACE,eAAe;EACf,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;EAGE,OAAO;EACP,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;;GAGG;AACH;EACE,gCAAgC;UACxB,wBAAwB;EAChC,OAAO;EACP,UAAU;EACV,OAAO;EACP,kBAAkB;EAClB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,sCAAsC;EACtC,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,oBAAoB;EACpB,OAAO;EACP,2BAA2B;EAC3B,OAAO;EACP,0CAA0C;UAClC,kCAAkC;EAC1C,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,qBAAqB,EAAE;AACzB;;GAEG;AACH;;EAEE,oBAAoB,EAAE;AACxB;;;GAGG;AACH;;;EAGE,kCAAkC;EAClC,OAAO;EACP,eAAe;EACf,OAAO,EAAE;AACX;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,uBAAuB;EACvB,YAAY,EAAE;AAChB;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,eAAe;EACf,eAAe;EACf,mBAAmB;EACnB,yBAAyB,EAAE;AAC7B;EACE,gBAAgB,EAAE;AACpB;EACE,YAAY,EAAE;AAChB;gFACgF;AAChF;;GAEG;AACH;;EAEE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc;EACd,UAAU,EAAE;AACd;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;gFACgF;AAChF;;;GAGG;AACH;;;;;EAKE,wBAAwB;EACxB,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,kBAAkB;EAClB,OAAO;EACP,UAAU;EACV,OAAO,EAAE;AACX;;;GAGG;AACH;;EAEE,OAAO;EACP,kBAAkB,EAAE;AACtB;;;GAGG;AACH;;EAEE,OAAO;EACP,qBAAqB,EAAE;AACzB;;;;GAIG;AACH;;;;EAIE,2BAA2B;EAC3B,OAAO,EAAE;AACX;;GAEG;AACH;;;;EAIE,mBAAmB;EACnB,WAAW,EAAE;AACf;;GAEG;AACH;;;;EAIE,+BAA+B,EAAE;AACnC;;GAEG;AACH;EACE,+BAA+B,EAAE;AACnC;;;;;GAKG;AACH;EACE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,eAAe;EACf,OAAO;EACP,eAAe;EACf,OAAO;EACP,gBAAgB;EAChB,OAAO;EACP,WAAW;EACX,OAAO;EACP,oBAAoB;EACpB,OAAO,EAAE;AACX;;;GAGG;AACH;EACE,sBAAsB;EACtB,OAAO;EACP,yBAAyB;EACzB,OAAO,EAAE;AACX;;GAEG;AACH;EACE,eAAe,EAAE;AACnB;;;GAGG;AACH;;EAEE,+BAA+B;UACvB,uBAAuB;EAC/B,OAAO;EACP,WAAW;EACX,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,aAAa,EAAE;AACjB;;;GAGG;AACH;EACE,8BAA8B;EAC9B,OAAO;EACP,qBAAqB;EACrB,OAAO,EAAE;AACX;;GAEG;AACH;;EAEE,yBAAyB,EAAE;AAC7B;;;GAGG;AACH;EACE,2BAA2B;EAC3B,OAAO;EACP,cAAc;EACd,OAAO,EAAE;AACX;gFACgF;AAChF;;;GAGG;AACH;;EAEE,eAAe,EAAE;AACnB;;GAEG;AACH;EACE,mBAAmB,EAAE;AACvB;gFACgF;AAChF;;GAEG;AACH;EACE,sBAAsB,EAAE;AAC1B;;GAEG;AACH;EACE,cAAc,EAAE;AAClB;gFACgF;AAChF;;GAEG;AACH;EACE,cAAc,EAAE","file":"normalize.css","sourcesContent":["/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0; }\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  -webkit-box-sizing: content-box;\n          box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n  /* 2 */ }\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\nsub {\n  bottom: -0.25em; }\nsup {\n  top: -0.5em; }\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */ }\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em; }\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD;EACE,gBAAgB,EAAE;AACpB;EACE,eAAe;EACf,YAAY;EACZ,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,iBAAiB,EAAE;AACrB;EACE;IACE,aAAa,EAAE,EAAE;AACrB;EACE;IACE,eAAe,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\n.social-network-icon {\n  max-width: 20px; }\n.site-footer {\n  padding: 1% 4%;\n  height: 8vh;\n  direction: rtl; }\n.social-icons-list > li {\n  display: inline-block;\n  margin-right: 3%; }\n@media (max-width: 768px) {\n  .site-footer {\n    height: 13vh; } }\n@media (max-width: 320px) {\n  .site-footer > .row > .col-xs-12:first-child {\n    margin-top: 5%; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n.browserupgrade {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA,iBAAiB;AACjB;;;;;;;GAOG;AACH;;;;;;;GAOG;AACH;EACE;;gFAE8E;;EAE9E,kEAAkE;;EAElE;;gFAE8E;;EAE9E,4BAA4B;;EAE5B;;gFAE8E;;EAE9E,uBAAuB,EAAE,gCAAgC;EACzD,uBAAuB,EAAE,2BAA2B;EACpD,uBAAuB,EAAE,6BAA6B;EACtD,wBAAwB,CAAC,iCAAiC;CAC3D;AACD;EACE,qBAAqB;EACrB,mCAAmC;EACnC,uKAAuK;EACvK,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,6BAA6B;EAC7B,qJAAqJ;EACrJ,iBAAiB,EAAE;AACrB;EACE,qBAAqB;EACrB,kCAAkC;EAClC,oKAAoK;EACpK,iBAAiB,EAAE;AACrB;;;;GAIG;AACH;;gFAEgF;AAChF;EACE,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;EACnB,WAAW,EAAE;AACf;EACE,UAAU;EACV,qBAAqB,EAAE;AACzB;EACE,eAAe,EAAE;AACnB;;;;;;GAMG;AACH;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;EACE,oBAAoB;EACpB,kBAAkB,EAAE;AACtB;;GAEG;AACH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW,EAAE;AACf;;;;GAIG;AACH;;;;;;EAME,uBAAuB,EAAE;AAC3B;;GAEG;AACH;EACE,UAAU;EACV,UAAU;EACV,WAAW,EAAE;AACf;;GAEG;AACH;EACE,iBAAiB,EAAE;AACrB;;gFAEgF;AAChF;EACE,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,iBAAiB,EAAE;AACrB;;;;gFAIgF;AAChF;EACE;;;IAGE,mCAAmC;IACnC,uBAAuB;IACvB,+DAA+D;IAC/D,oCAAoC;YAC5B,4BAA4B;IACpC,6BAA6B,EAAE;EACjC;;IAEE,2BAA2B,EAAE;EAC/B;IACE,6BAA6B,EAAE;EACjC;IACE,8BAA8B,EAAE;EAClC;;;KAGG;EACH;;IAEE,YAAY,EAAE;EAChB;;IAEE,uBAAuB;IACvB,yBAAyB,EAAE;EAC7B;;;KAGG;EACH;IACE,4BAA4B,EAAE;EAChC;;IAEE,yBAAyB,EAAE;EAC7B;IACE,2BAA2B,EAAE;EAC/B;;;IAGE,WAAW;IACX,UAAU,EAAE;EACd;;IAEE,wBAAwB,EAAE,EAAE","file":"Layout.css","sourcesContent":["@charset \"UTF-8\";\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n/*\n * normalize.css is imported in JS file.\n * If you import external CSS file from your internal CSS\n * then it will be inlined and processed with CSS modules.\n */\n/*\n * Base styles\n * ========================================================================== */\nhtml {\n  color: #222;\n  font-weight: 100;\n  font-size: 1em;\n  /* ~16px; */\n  font-family: var(--font-family-base);\n  line-height: 1.375;\n  /* ~22px */ }\nbody {\n  margin: 0;\n  font-family: Shabnam; }\na {\n  color: #0074c2; }\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n::selection {\n  background: #b3d4fc;\n  text-shadow: none; }\n/*\n * A better looking default horizontal rule\n */\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0; }\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle; }\n/*\n * Remove default fieldset styles.\n */\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0; }\n/*\n * Allow only vertical resizing of textareas.\n */\ntextarea {\n  resize: vertical; }\n/*\n * Browser upgrade prompt\n * ========================================================================== */\n:global(.browserupgrade) {\n  margin: 0.2em 0;\n  background: #ccc;\n  color: #000;\n  padding: 0.2em 0; }\n/*\n * Print styles\n * Inlined to avoid the additional HTTP request:\n * http://www.phpied.com/delay-loading-your-print-css/\n * ========================================================================== */\n@media print {\n  *,\n  *::before,\n  *::after {\n    background: transparent !important;\n    color: #000 !important;\n    /* Black prints faster: http://www.sanbeiji.com/archives/953 */\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n    text-shadow: none !important; }\n  a,\n  a:visited {\n    text-decoration: underline; }\n  a[href]::after {\n    content: \" (\" attr(href) \")\"; }\n  abbr[title]::after {\n    content: \" (\" attr(title) \")\"; }\n  /*\n   * Don't show links that are fragment identifiers,\n   * or use the `javascript:` pseudo protocol\n   */\n  a[href^='#']::after,\n  a[href^='javascript:']::after {\n    content: ''; }\n  pre,\n  blockquote {\n    border: 1px solid #999;\n    page-break-inside: avoid; }\n  /*\n   * Printing Tables:\n   * http://css-discuss.incutio.com/wiki/Printing_Tables\n   */\n  thead {\n    display: table-header-group; }\n  tr,\n  img {\n    page-break-inside: avoid; }\n  img {\n    max-width: 100% !important; }\n  p,\n  h2,\n  h3 {\n    orphans: 3;\n    widows: 3; }\n  h2,\n  h3 {\n    page-break-after: avoid; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/main.css"],"names":[],"mappings":"AAAA;IACI,qBAAqB;CACxB;AACD;IACI,aAAa;CAChB;AACD;IACI,aAAa;CAChB;AACD;IACI,cAAc;CACjB;AACD;IACI,yBAAyB;CAC5B;AACD;IACI,wBAAwB;CAC3B;AACD;IACI,0BAA0B;CAC7B;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,0BAA0B;QACtB,uBAAuB;YACnB,oBAAoB;CAC/B;AACD;IACI,qBAAqB;IACrB,qBAAqB;IACrB,cAAc;IACd,yBAAyB;QACrB,sBAAsB;YAClB,wBAAwB;IAChC,6BAA6B;IAC7B,8BAA8B;QAC1B,2BAA2B;YACvB,uBAAuB;CAClC;AACD;IACI,kBAAkB;CACrB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,iBAAiB;CACpB;AACD;IACI,cAAc;IACd,aAAa;CAChB;AACD;IACI,mBAAmB;CACtB;AACD;IACI,gDAAgD;IAChD,wDAAwD;IACxD,gDAAgD;IAChD,2CAA2C;IAC3C,wCAAwC;IACxC,6EAA6E;CAChF;AACD,cAAc,UAAU,CAAC;AACzB;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,aAAa;EACb,WAAW;EACX,aAAa;EACb,+CAA+C;UACvC,uCAAuC;EAC/C,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E;CAC9E;AACD;EACE,WAAW;EACX,aAAa;CACd;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB;CAC7B;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB;CACjC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,uBAAuB;CACxB;AACD;EACE,uBAAuB;;CAExB;AACD;EACE,yBAAyB;CAC1B;AACD;EACE,aAAa;CACd;AACD;EACE,aAAa;CACd;AACD;EACE,uBAAuB;CACxB;AACD;EACE,4BAA4B;CAC7B;AACD;EACE,2BAA2B;CAC5B;AACD;EACE,mBAAmB;EACnB,eAAe;CAChB;AACD;EACE,kBAAkB;EAClB,eAAe;CAChB;AACD;EACE,aAAa;CACd;AACD;EACE,WAAW;CACZ;AACD;EACE,gBAAgB;EAChB,eAAe;CAChB;AACD;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B;CACrC;AACD;EACE,sBAAsB;CACvB;AACD;EACE,WAAW;CACZ;AACD;EACE,YAAY;CACb;AACD;EACE,mBAAmB;EACnB,mBAAmB;CACpB;AACD;EACE,gBAAgB;CACjB;AACD;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;AACD;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,4BAA4B;EAC5B,oBAAoB;EACpB,YAAY;CACb;AACD;EACE,WAAW;EACX,YAAY;;CAEb;AACD;EACE,eAAe;CAChB;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD;EACE,gDAAgD;UACxC,wCAAwC;CACjD;AACD,sBAAsB;AACtB;EACE,mBAAmB;CACpB;AACD;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW;CACZ;AACD;EACE,eAAe;CAChB;AACD;EACE,sBAAsB;EACtB,YAAY;CACb;AACD,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB;GAClB;EACD;IACE,UAAU;GACX;;EAED;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,oBAAoB;IACpB,YAAY;GACb;EACD;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU;GACX;;CAEF;AACD;EACE,WAAW,EAAE;AACf;EACE,cAAc;EACd,WAAW;EACX,aAAa;EACb,kDAAkD;UAC1C,0CAA0C;EAClD,wDAAwD;EACxD,gDAAgD;EAChD,2CAA2C;EAC3C,wCAAwC;EACxC,6EAA6E,EAAE;AACjF;EACE,WAAW;EACX,aAAa,EAAE;AACjB;EACE,aAAa;EACb,oBAAoB,EAAE;AACxB;EACE,aAAa,EAAE;AACjB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,+BAA+B;EAC/B,8BAA8B;MAC1B,wBAAwB;UACpB,oBAAoB;EAC5B,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,oDAAoD;UAC5C,4CAA4C;EACpD,kBAAkB;EAClB,YAAY;EACZ,cAAc,EAAE;AAClB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,uBAAuB;UACnB,oBAAoB,EAAE;AAChC;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,yBAAyB;MACrB,sBAAsB;UAClB,wBAAwB,EAAE;AACpC;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,aAAa,EAAE;AACjB;EACE,eAAe,EAAE;AACnB;EACE,oBAAoB,EAAE;AACxB;EACE,oBAAoB,EAAE;AACxB;EACE,mBAAmB;EACnB,eAAe,EAAE;AACnB;EACE,kBAAkB;EAClB,eAAe,EAAE;AACnB;EACE,aAAa,EAAE;AACjB;EACE,WAAW,EAAE;AACf;EACE,gBAAgB;EAChB,eAAe,EAAE;AACnB;EACE,qBAAqB;EACrB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;EACtB,+BAA+B;EAC/B,+BAA+B;MAC3B,gCAAgC;UAC5B,4BAA4B,EAAE;AACxC;EACE,sBAAsB,EAAE;AAC1B;EACE,aAAa;EACb,iBAAiB,EAAE;AACrB;EACE,WAAW,EAAE;AACf;EACE,YAAY,EAAE;AAChB;EACE,oDAAoD;UAC5C,4CAA4C;EACpD,cAAc;EACd,mBAAmB;EACnB,oBAAoB;EACpB,aAAa,EAAE;AACjB;EACE,oBAAoB,EAAE;AACxB;EACE,sBAAsB;EACtB,uBAAuB;EACvB,aAAa;EACb,kBAAkB;EAClB,6BAA6B;EAC7B,qCAAqC;EACrC,4BAA4B,EAAE;AAChC;EACE,cAAc,EAAE;AAClB;EACE,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,gCAAgC,EAAE;AACpC;EACE,mBAAmB;EACnB,mBAAmB,EAAE;AACvB;EACE,gBAAgB,EAAE;AACpB;EACE,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB,EAAE;AAC1B;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,YAAY,EAAE;AAChB;EACE,WAAW;EACX,YAAY,EAAE;AAChB;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,mDAAmD;UAC3C,2CAA2C,EAAE;AACvD;EACE,eAAe,EAAE;AACnB;EACE,kBAAkB,EAAE;AACtB;EACE,gBAAgB;EAChB,iBAAiB,EAAE;AACrB;EACE,iCAAiC,EAAE;AACrC;EACE,mBAAmB;EACnB,aAAa;EACb,gBAAgB;EAChB,kBAAkB,EAAE;AACtB;EACE,gCAAgC,EAAE;AACpC,sBAAsB;AACtB;EACE,mBAAmB,EAAE;AACvB;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,iBAAiB;EACjB,wDAAwD;UAChD,gDAAgD;EACxD,mBAAmB;EACnB,WAAW;EACX,UAAU;EACV,WAAW,EAAE;AACf;EACE,eAAe,EAAE;AACnB;EACE,sBAAsB;EACtB,YAAY,EAAE;AAChB,0CAA0C;AAC1C;EACE;IACE,mBAAmB;IACnB,iBAAiB,EAAE;EACrB;IACE,UAAU,EAAE;EACd;IACE,YAAY;IACZ,YAAY,EAAE;EAChB;IACE,mBAAmB;IACnB,gBAAgB,EAAE;EACpB;IACE,eAAe,EAAE;EACnB;IACE,kBAAkB,EAAE;EACtB;IACE,YAAY;IACZ,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,oBAAoB;IACpB,YAAY,EAAE;EAChB;IACE,cAAc;IACd,mBAAmB;IACnB,0BAA0B;IAC1B,iBAAiB;IACjB,wDAAwD;YAChD,gDAAgD;IACxD,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,UAAU,EAAE;EACd;IACE,oDAAoD;YAC5C,4CAA4C;IACpD,cAAc;IACd,mBAAmB;IACnB,gBAAgB;IAChB,aAAa,EAAE,EAAE","file":"main.css","sourcesContent":["body {\n    font-family: Shabnam;\n}\n.white {\n    color: white;\n}\n.black {\n    color: black;\n}\n.purple {\n    color: #4c0d6b\n}\n.light-blue-background {\n    background-color: #3ad2cb\n}\n.white-background {\n    background-color: white;\n}\n.dark-green-background {\n    background-color: #185956;\n}\n.font-bold {\n    font-weight: 800;\n}\n.font-light {\n    font-weight: 200;\n}\n.font-medium {\n    font-weight: 600;\n}\n.center-content {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.center-column {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.text-align-right {\n    text-align: right;\n}\n.text-align-left {\n    text-align: left;\n}\n.text-align-center {\n    text-align: center;\n}\n.no-list-style {\n    list-style: none;\n}\n.clear-input {\n    outline: none;\n    border: none;\n}\n.border-radius {\n    border-radius: 9px;\n}\n.has-transition {\n    -webkit-transition: box-shadow 0.3s ease-in-out;\n    -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n    transition: -webkit-box-shadow 0.3s ease-in-out;\n    -o-transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out;\n    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton:focus {outline:0;}\ninput[type=\"button\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\nbutton[type=\"submit\"]{\n  outline:none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n          box-shadow: 0 2px 80px rgba(0,0,0,0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;\n}\nbutton[type=\"submit\"]::-moz-focus-inner {\n  padding: 0;\n  border: none;\n}\n.flex-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-middle{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.purple-font{\n  color: rgb(91,85,155);\n}\n.blue-font{\n  color: rgb(68,209,202);\n}\n.grey-font{\n  color:rgb(150,150,150);\n\n}\n.light-grey-font{\n  color:  rgb(148,148,148);\n}\n.black-font{\n  color: black;\n}\n.white-font{\n  color: white;\n}\n.pink-font{\n  color: rgb(240,93,108);\n}\n.pink-background{\n  background: rgb(240,93,108);\n}\n.purple-background{\n  background: rgb(90,85,155);\n}\n.text-align-center{\n  text-align: center;\n  direction: rtl;\n}\n.text-align-right{\n  text-align: right;\n  direction: rtl;\n}\n.margin-auto{\n  margin: auto;\n}\n.no-padding{\n  padding: 0;\n}\n.icon-cl{\n  max-height: 7vh;\n  max-width: 7vw;\n}\n.flex-row-rev{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n.flex-row-rev:hover {\n  text-decoration: none;\n}\n.mar-5{\n  margin: 5%;\n}\n.padd-5{\n  padding: 5%;\n}\n.mar-left{\n  margin-left: 5.5vw;\n  margin-bottom: 5vh;\n}\n.mar-top{\n  margin-top: 3vh;\n}\n.mar-bottom{\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important;\n}\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: rgb(68,209,202);\n  border-radius: 10px;\n  padding: 3%;\n}\n.icons-cl{\n  width: 3vw;\n  height: 5vh;\n\n}\n.mar-top-10{\n  margin-top: 2%;\n}\ninput[type=\"button\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\nbutton[type=\"submit\"]:hover{\n  -webkit-box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n          box-shadow: 0 10px 40px rgba(0,0,0,0.2);\n}\ninput[type=\"button\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\nbutton[type=\"submit\"]:focus{\n  -webkit-box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n          box-shadow: 0 0px 40px rgba(0,0,0,0.15);\n}\n/*dropdown css codes*/\n.dropdown {\n  position: relative;\n}\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh;\n}\n.dropdown:hover .dropdown-content {\n  display: block;\n}\n.price-heading {\n  display: inline-block;\n  width: 100%;\n}\n/*media queries for responsive utilities*/\n@media all and (max-width:768px){\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%;\n  }\n  .mar-left{\n    margin: 0;\n  }\n\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: rgb(68,209,202);\n    border-radius: 10px;\n    padding: 3%;\n  }\n  .dropdown-content{\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh;\n  }\n\n}\nbutton:focus {\n  outline: 0; }\ninput[type=\"button\"] {\n  outline: none;\n  padding: 0;\n  border: none;\n  -webkit-box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 2px 80px rgba(0, 0, 0, 0.1);\n  -webkit-transition: -webkit-box-shadow 0.3s ease-in-out;\n  transition: -webkit-box-shadow 0.3s ease-in-out;\n  -o-transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out;\n  transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out; }\ninput[type=\"button\"]::-moz-focus-inner {\n  padding: 0;\n  border: none; }\nfooter {\n  height: 13vh;\n  background: #1c5956; }\n.nav-logo {\n  float: right; }\n.nav-cl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  position: fixed;\n  height: 10vh;\n  background-color: white;\n  -webkit-box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n          box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);\n  overflow: visible;\n  width: 100%;\n  z-index: 9999; }\n.flex-center {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n.flex-middle {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n.purple-font {\n  color: #5b559b; }\n.blue-font {\n  color: #44d1ca; }\n.grey-font {\n  color: #969696; }\n.light-grey-font {\n  color: #949494; }\n.black-font {\n  color: black; }\n.white-font {\n  color: white; }\n.pink-font {\n  color: #f05d6c; }\n.pink-background {\n  background: #f05d6c; }\n.purple-background {\n  background: #5a559b; }\n.text-align-center {\n  text-align: center;\n  direction: rtl; }\n.text-align-right {\n  text-align: right;\n  direction: rtl; }\n.margin-auto {\n  margin: auto; }\n.no-padding {\n  padding: 0; }\n.icon-cl {\n  max-height: 7vh;\n  max-width: 7vw; }\n.flex-row-rev {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-decoration: none;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse; }\n.flex-row-rev:hover {\n  text-decoration: none; }\n.search-theme {\n  height: 15vh;\n  margin-top: 10vh; }\n.mar-5 {\n  margin: 5%; }\n.padd-5 {\n  padding: 5%; }\n.house-card {\n  -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n          box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n  height: 350px;\n  margin-bottom: 5vh;\n  margin-right: 5.5vw;\n  float: right; }\n.border-radius {\n  border-radius: 10px; }\n.house-img {\n  background-size: 100%;\n  /* max-height: 100%; */\n  height: 100%;\n  /* height: 97%; */\n  background-repeat: no-repeat;\n  /* background-origin: content-box; */\n  background-position: center; }\n.img-height {\n  height: 200px; }\n.border-bottom {\n  margin-bottom: 2vh;\n  margin-left: 1vw;\n  margin-right: 1vw;\n  margin-top: 2vh;\n  border-bottom: thin solid black; }\n.mar-left {\n  margin-left: 5.5vw;\n  margin-bottom: 5vh; }\n.mar-top {\n  margin-top: 3vh; }\n.mar-bottom {\n  margin-bottom: 13%;\n  margin-left: 2.5vw;\n  width: 93% !important; }\n.blue-button {\n  width: 13vw;\n  height: 5vh;\n  margin: auto;\n  background: #44d1ca;\n  border-radius: 10px;\n  padding: 3%; }\n.icons-cl {\n  width: 3vw;\n  height: 5vh; }\ninput[type=\"button\"]:hover {\n  -webkit-box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 10px 40px rgba(0, 0, 0, 0.2); }\ninput[type=\"button\"]:focus {\n  -webkit-box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 0px 40px rgba(0, 0, 0, 0.15); }\n.mar-top-10 {\n  margin-top: 2%; }\n.padding-right-price {\n  padding-right: 7%; }\n.margin-1 {\n  margin-left: 3%;\n  margin-right: 3%; }\n.img-border-radius {\n  border-radius: 10px 10px 0px 0px; }\n.main-form-search-result {\n  position: relative;\n  margin: auto;\n  margin-top: -7%;\n  margin-bottom: 6%; }\n.blue-button {\n  font-family: Shabnam !important; }\n/*dropdown css codes*/\n.dropdown {\n  position: relative; }\n.dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  min-width: 200px;\n  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  padding: 12px 16px;\n  z-index: 1;\n  left: 2vw;\n  top: 7.5vh; }\n.dropdown:hover .dropdown-content {\n  display: block; }\n.price-heading {\n  display: inline-block;\n  width: 100%; }\n/*media queries for responsive utilities*/\n@media all and (max-width: 768px) {\n  .main-form {\n    margin-bottom: 34%;\n    margin-top: -12%; }\n  .mar-left {\n    margin: 0; }\n  .icons-cl {\n    width: 13vw;\n    height: 7vh; }\n  .footer-text {\n    text-align: center;\n    font-size: 12px; }\n  .icons-mar {\n    margin-top: 5%; }\n  .house-card {\n    margin-bottom: 5%; }\n  .blue-button {\n    width: 50vw;\n    height: 5vh;\n    margin: auto;\n    background: #44d1ca;\n    border-radius: 10px;\n    padding: 3%; }\n  .dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 200px;\n    -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    padding: 12px 16px;\n    z-index: 1;\n    left: 12vw;\n    top: 10vh; }\n  .house-card {\n    -webkit-box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.4);\n    height: 350px;\n    margin-bottom: 5vh;\n    margin-right: 0;\n    float: right; } }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/routes/not-found/NotFound.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n\nbody,\nhtml {\n  height: 100%;\n  min-height: 100%; }\n\nbody {\n  font-family: IRSans, serif; }\n\ninput:-webkit-autofill {\n  -webkit-box-shadow: 0 0 0 1000px white inset; }\n\ninput:focus {\n  outline: none; }\n\n.toast {\n  direction: rtl;\n  text-align: right;\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n\n.not-found-box {\n  margin-top: 100px;\n  direction: rtl;\n  font-weight: 700;\n  text-align: center;\n  color: #a94442;\n  background-color: white;\n  border-radius: 5px;\n  padding: 20px 50px;\n  width: 100%;\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n", "", {"version":3,"sources":["/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/not-found/NotFound.scss"],"names":[],"mappings":"AAAA;;;;;;;GAOG;;AAEH;EACE;;gFAE8E;;EAE9E,kEAAkE;;EAElE;;gFAE8E;;EAE9E,4BAA4B;;EAE5B;;gFAE8E;;EAE9E,uBAAuB,EAAE,gCAAgC;EACzD,uBAAuB,EAAE,2BAA2B;EACpD,uBAAuB,EAAE,6BAA6B;EACtD,wBAAwB,CAAC,iCAAiC;CAC3D;;AAED;EACE,qBAAqB;EACrB,mCAAmC;EACnC,uKAAuK;EACvK,iBAAiB,EAAE;;AAErB;EACE,qBAAqB;EACrB,6BAA6B;EAC7B,qJAAqJ;EACrJ,iBAAiB,EAAE;;AAErB;EACE,qBAAqB;EACrB,kCAAkC;EAClC,oKAAoK;EACpK,iBAAiB,EAAE;;AAErB;;EAEE,aAAa;EACb,iBAAiB,EAAE;;AAErB;EACE,2BAA2B,EAAE;;AAE/B;EACE,6CAA6C,EAAE;;AAEjD;EACE,cAAc,EAAE;;AAElB;EACE,eAAe;EACf,kBAAkB;EAClB,wHAAwH;UAChH,gHAAgH,EAAE;;AAE5H;EACE,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;EACf,wBAAwB;EACxB,mBAAmB;EACnB,mBAAmB;EACnB,YAAY;EACZ,wHAAwH;UAChH,gHAAgH,EAAE","file":"NotFound.scss","sourcesContent":["/**\n * React Starter Kit (https://www.reactstarterkit.com/)\n *\n * Copyright © 2014-present Kriasoft, LLC. All rights reserved.\n *\n * This source code is licensed under the MIT license found in the\n * LICENSE.txt file in the root directory of this source tree.\n */\n\n:root {\n  /*\n   * Typography\n   * ======================================================================== */\n\n  --font-family-base: 'Segoe UI', 'HelveticaNeue-Light', sans-serif;\n\n  /*\n   * Layout\n   * ======================================================================== */\n\n  --max-content-width: 1000px;\n\n  /*\n   * Media queries breakpoints\n   * ======================================================================== */\n\n  --screen-xs-min: 480px;  /* Extra small screen / phone */\n  --screen-sm-min: 768px;  /* Small screen / tablet */\n  --screen-md-min: 992px;  /* Medium screen / desktop */\n  --screen-lg-min: 1200px; /* Large screen / wide desktop */\n}\n\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Light.eot);\n  src: url(/fonts/Shabnam-Light.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Light.woff) format(\"woff\"), url(/fonts/Shabnam-Light.ttf) format(\"truetype\");\n  font-weight: 100; }\n\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam.eot);\n  src: url(/fonts/Shabnam.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam.woff) format(\"woff\"), url(/fonts/Shabnam.ttf) format(\"truetype\");\n  font-weight: 600; }\n\n@font-face {\n  font-family: Shabnam;\n  src: url(/fonts/Shabnam-Bold.eot);\n  src: url(/fonts/Shabnam-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/fonts/Shabnam-Bold.woff) format(\"woff\"), url(/fonts/Shabnam-Bold.ttf) format(\"truetype\");\n  font-weight: 700; }\n\nbody,\nhtml {\n  height: 100%;\n  min-height: 100%; }\n\nbody {\n  font-family: IRSans, serif; }\n\ninput:-webkit-autofill {\n  -webkit-box-shadow: 0 0 0 1000px white inset; }\n\ninput:focus {\n  outline: none; }\n\n.toast {\n  direction: rtl;\n  text-align: right;\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n\n.not-found-box {\n  margin-top: 100px;\n  direction: rtl;\n  font-weight: 700;\n  text-align: center;\n  color: #a94442;\n  background-color: white;\n  border-radius: 5px;\n  padding: 20px 50px;\n  width: 100%;\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./node_modules/normalize.css/normalize.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/actions/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["loginRequest"] = loginRequest;
/* harmony export (immutable) */ __webpack_exports__["loginSuccess"] = loginSuccess;
/* harmony export (immutable) */ __webpack_exports__["loginFailure"] = loginFailure;
/* harmony export (immutable) */ __webpack_exports__["loadLoginStatus"] = loadLoginStatus;
/* harmony export (immutable) */ __webpack_exports__["loginUser"] = loginUser;
/* harmony export (immutable) */ __webpack_exports__["logoutRequest"] = logoutRequest;
/* harmony export (immutable) */ __webpack_exports__["logoutSuccess"] = logoutSuccess;
/* harmony export (immutable) */ __webpack_exports__["logoutFailure"] = logoutFailure;
/* harmony export (immutable) */ __webpack_exports__["logoutUser"] = logoutUser;
/* harmony export (immutable) */ __webpack_exports__["initiateRequest"] = initiateRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateSuccess"] = initiateSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateFailure"] = initiateFailure;
/* harmony export (immutable) */ __webpack_exports__["initiate"] = initiate;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersRequest"] = initiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersSuccess"] = initiateUsersSuccess;
/* harmony export (immutable) */ __webpack_exports__["initiateUsersFailure"] = initiateUsersFailure;
/* harmony export (immutable) */ __webpack_exports__["initiateUsers"] = initiateUsers;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserRequest"] = getCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserSuccess"] = getCurrentUserSuccess;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUserFailure"] = getCurrentUserFailure;
/* harmony export (immutable) */ __webpack_exports__["getCurrentUser"] = getCurrentUser;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditRequest"] = increaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditSuccess"] = increaseCreditSuccess;
/* harmony export (immutable) */ __webpack_exports__["increaseCreditFailure"] = increaseCreditFailure;
/* harmony export (immutable) */ __webpack_exports__["increaseCredit"] = increaseCredit;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants__ = __webpack_require__("./src/constants/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__ = __webpack_require__("./src/api-handlers/authentication.js");
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






function loginRequest(username, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["K" /* USER_LOGIN_REQUEST */],
    payload: {
      username,
      password
    }
  };
}

function loginSuccess(info) {
  console.log(info);
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["L" /* USER_LOGIN_SUCCESS */],
    payload: { info }
  };
}

function loginFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["J" /* USER_LOGIN_FAILURE */],
    payload: { error }
  };
}

function loadLoginStatus() {
  const loginInfo = Object(__WEBPACK_IMPORTED_MODULE_0__utils__["c" /* getLoginInfo */])();
  if (loginInfo) {
    return loginSuccess(loginInfo);
  }
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["I" /* USER_IS_NOT_LOGGED_IN */]
  };
}

function getCaptchaToken() {
  if (false) {
    const loginForm = $('#loginForm').get(0);
    if (loginForm && typeof loginForm['g-recaptcha-response'] === 'object') {
      return loginForm['g-recaptcha-response'].value;
    }
  }
  return null;
}

function loginUser(username, password) {
  return (() => {
    var _ref = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(loginRequest(username, password));
      try {
        // const captchaToken = getCaptchaToken();
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["c" /* sendLoginRequest */])(username, password);
        dispatch(loginSuccess(res));
        localStorage.setItem('loginInfo', JSON.stringify(res));
        // console.log(location.pathname);
        const { pathname } = getState().house;
        console.log(pathname);
        __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push(pathname);
      } catch (err) {
        dispatch(loginFailure(err));
      }
    });

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  })();
}

function logoutRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["N" /* USER_LOGOUT_REQUEST */],
    payload: {}
  };
}

function logoutSuccess() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["O" /* USER_LOGOUT_SUCCESS */],
    payload: {
      message: 'خروج موفقیت آمیز'
    }
  };
}

function logoutFailure(error) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["M" /* USER_LOGOUT_FAILURE */],
    payload: { error }
  };
}

function logoutUser() {
  return (() => {
    var _ref2 = _asyncToGenerator(function* (dispatch) {
      dispatch(logoutRequest());
      try {
        dispatch(logoutSuccess());
        localStorage.clear();
        location.assign('/login');
      } catch (err) {
        dispatch(logoutFailure(err));
      }
    });

    return function (_x3) {
      return _ref2.apply(this, arguments);
    };
  })();
}

function initiateRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["q" /* GET_INIT_REQUEST */]
  };
}

function initiateSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["r" /* GET_INIT_SUCCESS */],
    payload: { response }
  };
}

function initiateFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["p" /* GET_INIT_FAILURE */],
    payload: { err }
  };
}

function initiate() {
  return (() => {
    var _ref3 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["a" /* sendInitiateRequest */])();
        dispatch(initiateSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateFailure(error));
      }
    });

    return function (_x4) {
      return _ref3.apply(this, arguments);
    };
  })();
}

function initiateUsersRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["t" /* GET_INIT_USER_REQUEST */]
  };
}

function initiateUsersSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["u" /* GET_INIT_USER_SUCCESS */],
    payload: { response }
  };
}

function initiateUsersFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["s" /* GET_INIT_USER_FAILURE */],
    payload: { err }
  };
}

function initiateUsers() {
  return (() => {
    var _ref4 = _asyncToGenerator(function* (dispatch) {
      dispatch(initiateUsersRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["b" /* sendInitiateUsersRequest */])();
        dispatch(initiateUsersSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(initiateUsersFailure(error));
      }
    });

    return function (_x5) {
      return _ref4.apply(this, arguments);
    };
  })();
}

function getCurrentUserRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["e" /* GET_CURR_USER_REQUEST */]
  };
}

function getCurrentUserSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["f" /* GET_CURR_USER_SUCCESS */],
    payload: { response }
  };
}

function getCurrentUserFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["d" /* GET_CURR_USER_FAILURE */],
    payload: { err }
  };
}

function getCurrentUser(userId) {
  return (() => {
    var _ref5 = _asyncToGenerator(function* (dispatch) {
      dispatch(getCurrentUserRequest());
      try {
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["d" /* sendgetCurrentUserRequest */])(userId);
        dispatch(getCurrentUserSuccess(res));
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        dispatch(getCurrentUserFailure(error));
      }
    });

    return function (_x6) {
      return _ref5.apply(this, arguments);
    };
  })();
}

function increaseCreditRequest() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["z" /* INCREASE_CREDIT_REQUEST */]
  };
}

function increaseCreditSuccess(response) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["A" /* INCREASE_CREDIT_SUCCESS */],
    payload: { response }
  };
}

function increaseCreditFailure(err) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants__["y" /* INCREASE_CREDIT_FAILURE */],
    payload: { err }
  };
}

function increaseCredit(amount) {
  return (() => {
    var _ref6 = _asyncToGenerator(function* (dispatch, getState) {
      dispatch(increaseCreditRequest());
      try {
        const { currUser } = getState().authentication;
        const res = yield Object(__WEBPACK_IMPORTED_MODULE_3__api_handlers_authentication__["e" /* sendincreaseCreditRequest */])(amount, currUser.userId);
        dispatch(increaseCreditSuccess(res));
        dispatch(getCurrentUser(currUser.userId));
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["f" /* notifySuccess */])('افزایش اعتبار با موفقیت انجام شد.');
      } catch (error) {
        if (error.message) {
          Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])(error.message);
        }
        Object(__WEBPACK_IMPORTED_MODULE_0__utils__["e" /* notifyError */])('خطا مجدد تلاش کنید');
        dispatch(increaseCreditFailure(error));
      }
    });

    return function (_x7, _x8) {
      return _ref6.apply(this, arguments);
    };
  })();
}

/***/ }),

/***/ "./src/api-handlers/authentication.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = sendincreaseCreditRequest;
/* harmony export (immutable) */ __webpack_exports__["d"] = sendgetCurrentUserRequest;
/* harmony export (immutable) */ __webpack_exports__["a"] = sendInitiateRequest;
/* harmony export (immutable) */ __webpack_exports__["b"] = sendInitiateUsersRequest;
/* harmony export (immutable) */ __webpack_exports__["c"] = sendLoginRequest;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("./src/utils/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_paths__ = __webpack_require__("./src/config/paths.js");



function sendincreaseCreditRequest(amount, id) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["g" /* increaseCreditUrl */])(amount, id), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendgetCurrentUserRequest(userId) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(Object(__WEBPACK_IMPORTED_MODULE_1__config_paths__["b" /* getCurrentUserUrl */])(userId), Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["e" /* getInitiateUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendInitiateUsersRequest() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* getGetConfig */])());
}

function sendLoginRequest(username, password) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* fetchApi */])(__WEBPACK_IMPORTED_MODULE_1__config_paths__["h" /* loginApiUrl */], Object(__WEBPACK_IMPORTED_MODULE_0__utils__["d" /* getPostConfig */])({
    username,
    password
  }));
}

/***/ }),

/***/ "./src/components/Footer/200px-Instagram_logo_2016.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Instagram_logo_2016.svg.png?2fc2d5c1";

/***/ }),

/***/ "./src/components/Footer/200px-Telegram_logo.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/200px-Telegram_logo.svg.png?6fa2804c";

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__ = __webpack_require__("./src/components/Footer/Twitter_bird_logo_2012.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__ = __webpack_require__("./src/components/Footer/200px-Instagram_logo_2016.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__ = __webpack_require__("./src/components/Footer/200px-Telegram_logo.svg.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Footer/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Footer/Footer.js';
/* eslint-disable react/no-danger */








class Footer extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'footer',
      { className: 'site-footer center-column dark-green-background white', __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'row', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-4 text-align-left', __source: {
              fileName: _jsxFileName,
              lineNumber: 15
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'ul',
            { className: 'social-icons-list no-list-style', __source: {
                fileName: _jsxFileName,
                lineNumber: 16
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 17
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_4__200px_Instagram_logo_2016_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'instagram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 18
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 24
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_3__Twitter_bird_logo_2012_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'twitter-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 25
                },
                __self: this
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'li',
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 31
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', {
                src: __WEBPACK_IMPORTED_MODULE_5__200px_Telegram_logo_svg_png___default.a,
                className: 'social-network-icon',
                alt: 'telegram-icon',
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              })
            )
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'col-xs-12 col-sm-8 text-align-right', __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'p',
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 41
              },
              __self: this
            },
            '\u062A\u0645\u0627\u0645\u06CC \u062D\u0642\u0648\u0642 \u0627\u06CC\u0646 \u0648\u0628\u200C\u0633\u0627\u06CC\u062A \u0645\u062A\u0639\u0644\u0642 \u0628\u0647 \u0645\u0647\u0631\u0646\u0627\u0632 \u062B\u0627\u0628\u062A \u0648 \u0647\u0648\u0645\u0646 \u0634\u0631\u06CC\u0641 \u0632\u0627\u062F\u0647 \u0645\u06CC\u200C\u0628\u0627\u0634\u062F.'
          )
        )
      )
    );
  }
}

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Twitter_bird_logo_2012.svg.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Footer/Twitter_bird_logo_2012.svg.png?248bfa8d";

/***/ }),

/***/ "./src/components/Footer/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Footer/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Layout/Layout.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css__ = __webpack_require__("./node_modules/normalize.css/normalize.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_normalize_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_normalize_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Layout_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Layout_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Footer__ = __webpack_require__("./src/components/Footer/Footer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Panel__ = __webpack_require__("./src/components/Panel/Panel.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Layout/Layout.js';
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */









class Layout extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'col-md-12 col-xs-12', style: { padding: '0' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__Panel__["a" /* default */], { mainPanel: this.props.mainPanel, __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }),
      this.props.children,
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__Footer__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      })
    );
  }
}

Layout.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool
};
Layout.defaultProps = {
  mainPanel: false
};
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_normalize_css___default.a, __WEBPACK_IMPORTED_MODULE_4__Layout_css___default.a)(Layout));

/***/ }),

/***/ "./src/components/Panel/Panel.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__("react-redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("redux");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css__ = __webpack_require__("./src/components/Panel/main.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png__ = __webpack_require__("./src/components/Panel/logo.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logo_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__logo_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__history__ = __webpack_require__("./src/history.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/components/Panel/Panel.js';
/* eslint-disable react/no-danger */










class Panel extends __WEBPACK_IMPORTED_MODULE_0_react__["Component"] {

  render() {
    console.log(this.props.currUser);
    console.log(this.props.isLoggedIn);
    if (this.props.mainPanel) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'header',
        { className: 'hidden-xs', __source: {
            fileName: _jsxFileName,
            lineNumber: 25
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'container-fluid', __source: {
              fileName: _jsxFileName,
              lineNumber: 26
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'row header-main', __source: {
                fileName: _jsxFileName,
                lineNumber: 27
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown ', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 28
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'container', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'row flex-center header-main-boarder', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9 ', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 31
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h5',
                      { className: 'text-align-right white-font persian ', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 32
                        },
                        __self: this
                      },
                      '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                    ),
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'dropdown-content', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 35
                        },
                        __self: this
                      },
                      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'container-fluid', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 36
                          },
                          __self: this
                        },
                        this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          {
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 38
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 39
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'h4',
                              { className: 'persian text-align-right black-font ', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 40
                                },
                                __self: this
                              },
                              this.props.currUser.username
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 44
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 pull-left persian light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 45
                                },
                                __self: this
                              },
                              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                                'span',
                                { className: 'light-grey-font', __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 46
                                  },
                                  __self: this
                                },
                                '\u062A\u0648\u0645\u0627\u0646',
                                this.props.currUser.balance
                              )
                            ),
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'div',
                              { className: 'col-xs-6 light-grey-font pull-right  persian text-align-right no-padding', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 51
                                },
                                __self: this
                              },
                              '\u0627\u0639\u062A\u0628\u0627\u0631'
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 55
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                              onClick: () => {
                                __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 56
                              },
                              __self: this
                            })
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'row mar-top', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 65
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                              type: 'button',
                              value: '\u062E\u0631\u0648\u062C',
                              onClick: () => {
                                this.props.authActions.logoutUser();
                              },
                              className: '  white-font  blue-button persian text-align-center',
                              __source: {
                                fileName: _jsxFileName,
                                lineNumber: 66
                              },
                              __self: this
                            })
                          )
                        ),
                        !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 78
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0648\u0631\u0648\u062F',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 79
                            },
                            __self: this
                          })
                        )
                      )
                    )
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding  col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 93
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x white-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 94
                      },
                      __self: this
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'header',
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'container-fluid', __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'row nav-cl', __source: {
              fileName: _jsxFileName,
              lineNumber: 107
            },
            __self: this
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-2   nav-user col-xs-12 dropdown', __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 109
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row flex-center ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 110
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'col-xs-9 ', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 111
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'h5',
                    { className: 'text-align-right persian purple-font', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 112
                      },
                      __self: this
                    },
                    '\u0646\u0627\u062D\u06CC\u0647 \u06A9\u0627\u0631\u0628\u0631\u06CC'
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'dropdown-content', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 115
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'div',
                      { className: 'container-fluid', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 116
                        },
                        __self: this
                      },
                      this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        {
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 118
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 119
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'h4',
                            { className: 'black-font persian text-align-right blue-font', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 120
                              },
                              __self: this
                            },
                            this.props.currUser.username
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 124
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: ' light-grey-font col-xs-6 pull-left blue-font persian', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 125
                              },
                              __self: this
                            },
                            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                              'span',
                              { className: 'light-grey-font', __source: {
                                  fileName: _jsxFileName,
                                  lineNumber: 126
                                },
                                __self: this
                              },
                              '\u062A\u0648\u0645\u0627\u0646',
                              this.props.currUser.balance
                            )
                          ),
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                            'div',
                            { className: 'light-grey-font col-xs-6 pull-right blue-font persian text-align-right no-padding', __source: {
                                fileName: _jsxFileName,
                                lineNumber: 131
                              },
                              __self: this
                            },
                            '\u0627\u0639\u062A\u0628\u0627\u0631'
                          )
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 135
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u0627\u0641\u0632\u0627\u06CC\u0634 \u0627\u0639\u062A\u0628\u0627\u0631',
                            className: 'white-font  blue-button persian text-align-center',
                            onClick: () => {
                              __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/increaseCredit');
                            },
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 136
                            },
                            __self: this
                          })
                        ),
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                          'div',
                          { className: 'row mar-top', __source: {
                              fileName: _jsxFileName,
                              lineNumber: 145
                            },
                            __self: this
                          },
                          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                            type: 'button',
                            value: '\u062E\u0631\u0648\u062C',
                            onClick: () => {
                              this.props.authActions.logoutUser();
                            },
                            className: '  white-font  blue-button persian text-align-center',
                            __source: {
                              fileName: _jsxFileName,
                              lineNumber: 146
                            },
                            __self: this
                          })
                        )
                      ),
                      !this.props.currUser && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                        'div',
                        { className: 'row mar-top', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 158
                          },
                          __self: this
                        },
                        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', {
                          type: 'button',
                          value: '\u0648\u0631\u0648\u062F',
                          onClick: () => {
                            __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/login');
                          },
                          className: '  white-font  blue-button persian text-align-center',
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 159
                          },
                          __self: this
                        })
                      )
                    )
                  )
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'div',
                  { className: 'no-padding  col-xs-3', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 173
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('i', { className: 'fa fa-smile-o fa-2x purple-font', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 174
                    },
                    __self: this
                  })
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'flex-center col-md-4 col-md-offset-6 nav-logo col-xs-12', __source: {
                fileName: _jsxFileName,
                lineNumber: 179
              },
              __self: this
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'container', __source: {
                  fileName: _jsxFileName,
                  lineNumber: 180
                },
                __self: this
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'div',
                { className: 'row  ', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 181
                  },
                  __self: this
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  'a',
                  {
                    className: 'flex-row-rev',
                    onClick: () => {
                      __WEBPACK_IMPORTED_MODULE_8__history__["a" /* default */].push('/');
                    },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 182
                    },
                    __self: this
                  },
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'no-padding text-align-center col-xs-3', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 188
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { className: 'icon-cl', src: __WEBPACK_IMPORTED_MODULE_7__logo_png___default.a, alt: 'logo', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 189
                      },
                      __self: this
                    })
                  ),
                  __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    'div',
                    { className: 'col-xs-9', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 191
                      },
                      __self: this
                    },
                    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      'h4',
                      { className: 'persian-bold text-align-right  persian blue-font', __source: {
                          fileName: _jsxFileName,
                          lineNumber: 192
                        },
                        __self: this
                      },
                      '\u062E\u0627\u0646\u0647 \u0628\u0647 \u062F\u0648\u0634'
                    )
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}
Panel.propTypes = {
  currUser: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  authActions: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired,
  mainPanel: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired,
  isLoggedIn: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.bool.isRequired
};
function mapDispatchToProps(dispatch) {
  return {
    authActions: Object(__WEBPACK_IMPORTED_MODULE_3_redux__["bindActionCreators"])(__WEBPACK_IMPORTED_MODULE_5__actions_authentication__, dispatch)
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser, isLoggedIn } = authentication;
  return {
    currUser,
    isLoggedIn
  };
}

/* harmony default export */ __webpack_exports__["a"] = (Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(__WEBPACK_IMPORTED_MODULE_4_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_6__main_css___default.a)(Panel)));

/***/ }),

/***/ "./src/components/Panel/logo.png":
/***/ (function(module, exports) {

module.exports = "/assets/src/components/Panel/logo.png?2af73d8a";

/***/ }),

/***/ "./src/components/Panel/main.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/components/Panel/main.css");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/config/compressionConfig.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// noinspection JSAnnotator
const levels = [{
  MAX_WIDTH: 600,
  MAX_HEIGHT: 600,
  DOCUMENT_SIZE: 300000
}, {
  MAX_WIDTH: 800,
  MAX_HEIGHT: 800,
  DOCUMENT_SIZE: 500000
}];

/* harmony default export */ __webpack_exports__["a"] = ({
  getLevel(level) {
    return levels[level - 1];
  }
});

/***/ }),

/***/ "./src/config/paths.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__urls__ = __webpack_require__("./src/config/urls.js");
/* eslint-disable prettier/prettier */


const loginApiUrl = `http://localhost:8080/Turtle/login`;
/* harmony export (immutable) */ __webpack_exports__["h"] = loginApiUrl;

const logoutApiUrl = `http://localhost:8080/Turtle/login`;
/* unused harmony export logoutApiUrl */

const getSearchHousesUrl = `http://localhost:8080/Turtle/SearchServlet`;
/* harmony export (immutable) */ __webpack_exports__["f"] = getSearchHousesUrl;

const getCreateHouseUrl = `http://localhost:8080/Turtle/api/HouseServlet`;
/* harmony export (immutable) */ __webpack_exports__["a"] = getCreateHouseUrl;

const getInitiateUrl = `http://localhost:8080/Turtle/home`;
/* harmony export (immutable) */ __webpack_exports__["e"] = getInitiateUrl;

const getCurrentUserUrl = userId => `http://localhost:8080/Turtle/api/IndividualServlet?userId=${userId}`;
/* harmony export (immutable) */ __webpack_exports__["b"] = getCurrentUserUrl;

const getHouseDetailUrl = houseId => `http://localhost:8080/Turtle/api/GetSingleHouse?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["c"] = getHouseDetailUrl;

const getHousePhoneUrl = houseId => `http://localhost:8080/Turtle/api/PaymentServlet?id=${houseId}`;
/* harmony export (immutable) */ __webpack_exports__["d"] = getHousePhoneUrl;

const increaseCreditUrl = amount => `http://localhost:8080/Turtle/api/CreditServlet?value=${amount}`;
/* harmony export (immutable) */ __webpack_exports__["g"] = increaseCreditUrl;


const searchDriverUrl = phoneNumber => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/phoneNumber/${phoneNumber}`;
/* unused harmony export searchDriverUrl */

const getDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export getDriverAcquisitionInfoUrl */

const updateDriverAcquisitionInfoUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/${driverId}`;
/* unused harmony export updateDriverAcquisitionInfoUrl */

const updateDriverDocsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export updateDriverDocsUrl */

const approveDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/approve/${driverId}`;
/* unused harmony export approveDriverUrl */

const rejectDriverUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/reject/${driverId}`;
/* unused harmony export rejectDriverUrl */

const getDriverDocumentUrl = documentId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/document/${documentId}`;
/* unused harmony export getDriverDocumentUrl */

const getDriverDocumentIdsUrl = driverId => `${__WEBPACK_IMPORTED_MODULE_0__urls__["api"].clientUrl}/v2/acquisition/driver/documents/${driverId}`;
/* unused harmony export getDriverDocumentIdsUrl */


/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__ = __webpack_require__("history/createBrowserHistory");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__);
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */



// NavigationBar manager, e.g. history.push('/home')
// https://github.com/mjackson/history
/* harmony default export */ __webpack_exports__["a"] = (false && __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default()());

/***/ }),

/***/ "./src/routes/not-found/NotFound.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__NotFound_scss__ = __webpack_require__("./src/routes/not-found/NotFound.scss");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__NotFound_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__NotFound_scss__);
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/not-found/NotFound.js';




class NotFound extends __WEBPACK_IMPORTED_MODULE_0_react___default.a.Component {
  render() {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'not-found-box', __source: {
          fileName: _jsxFileName,
          lineNumber: 8
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'not-found-text', __source: {
            fileName: _jsxFileName,
            lineNumber: 9
          },
          __self: this
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'p',
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 10
            },
            __self: this
          },
          ' \u0635\u0641\u062D\u0647\u200C\u06CC \u0645\u0648\u0631\u062F \u0646\u0638\u0631 \u0634\u0645\u0627 \u06CC\u0627\u0641\u062A \u0646\u0634\u062F.'
        )
      )
    );
  }
}

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_2__NotFound_scss___default.a)(NotFound));

/***/ }),

/***/ "./src/routes/not-found/NotFound.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/routes/not-found/NotFound.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (module.hot && typeof window !== 'undefined' && window.document) {
      var removeCss = function() {};
      module.hot.accept("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/routes/not-found/NotFound.scss", function() {
        content = __webpack_require__("./node_modules/css-loader/index.js??ref--1-1!./node_modules/postcss-loader/lib/index.js??ref--1-2!./node_modules/sass-loader/lib/loader.js!./src/routes/not-found/NotFound.scss");

        if (typeof content === 'string') {
          content = [[module.i, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),

/***/ "./src/routes/not-found/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_Layout__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__NotFound__ = __webpack_require__("./src/routes/not-found/NotFound.js");
var _jsxFileName = '/Users/mehrnaz/Documents/turtleReact/turtle-react/src/routes/not-found/index.js';




const title = 'صفحه‌ی مورد نظر یافت نشد';

function action() {
  return {
    chunks: ['not-found'],
    title,
    component: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_1__components_Layout__["a" /* default */],
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        __self: this
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__NotFound__["a" /* default */], { title: title, __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        __self: this
      })
    ),
    status: 404
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/utils/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export compressFile */
/* harmony export (immutable) */ __webpack_exports__["c"] = getLoginInfo;
/* harmony export (immutable) */ __webpack_exports__["d"] = getPostConfig;
/* unused harmony export getPutConfig */
/* harmony export (immutable) */ __webpack_exports__["b"] = getGetConfig;
/* unused harmony export convertNumbersToPersian */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchApi; });
/* unused harmony export setImmediate */
/* unused harmony export setCookie */
/* unused harmony export getCookie */
/* unused harmony export eraseCookie */
/* harmony export (immutable) */ __webpack_exports__["e"] = notifyError;
/* harmony export (immutable) */ __webpack_exports__["f"] = notifySuccess;
/* unused harmony export reloadSelectPicker */
/* unused harmony export loadSelectPicker */
/* unused harmony export getFormData */
/* unused harmony export loadUserLoginStatus */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("lodash");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js__ = __webpack_require__("exif-js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_exif_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_exif_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__("./src/history.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__ = __webpack_require__("./src/config/compressionConfig.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_authentication__ = __webpack_require__("./src/actions/authentication.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

let processFile = (() => {
  var _ref = _asyncToGenerator(function* (dataURL, fileSize, fileType, documentType) {
    const image = new Image();
    image.src = dataURL;
    return new Promise(function (resolve, reject) {
      image.onload = function () {
        const imageMeasurements = getImageMeasurements(image);
        if (!shouldResize(imageMeasurements, fileSize, documentType)) {
          resolve(dataURLtoBlob(dataURL, fileType));
        }
        const scaledImageMeasurements = getScaledImageMeasurements(imageMeasurements.width, imageMeasurements.height, documentType);
        const scaledImageFile = getScaledImageFileFromCanvas(image, scaledImageMeasurements.width, scaledImageMeasurements.height, imageMeasurements.transform, fileType);
        resolve(scaledImageFile);
      };
      image.onerror = reject;
    });
  });

  return function processFile(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }







const compressionFirstLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(1);
const compressionSecondLevel = __WEBPACK_IMPORTED_MODULE_3__config_compressionConfig__["a" /* default */].getLevel(2);
const documentsCompressionLevel = {
  profilePicture: compressionFirstLevel,
  driverLicence: compressionFirstLevel,
  carIdCard: compressionFirstLevel,
  insurance: compressionSecondLevel
};

function getCompressionLevel(documentType) {
  return documentsCompressionLevel[documentType] || compressionFirstLevel;
}

function dataURLtoBlob(dataURL, fileType) {
  const binary = atob(dataURL.split(',')[1]);
  const array = [];
  const length = binary.length;
  let i;
  for (i = 0; i < length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: fileType });
}

function getScaledImageMeasurements(width, height, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH } = getCompressionLevel(documentType);

  if (width > height) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  } else {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
  return {
    height,
    width
  };
}

function getImageOrientation(image) {
  let orientation = 0;
  __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getData(image, function () {
    orientation = __WEBPACK_IMPORTED_MODULE_1_exif_js___default.a.getTag(this, 'Orientation');
  });
  return orientation;
}

function getImageMeasurements(image) {
  const orientation = getImageOrientation(image);
  let transform;
  let swap = false;
  switch (orientation) {
    case 8:
      swap = true;
      transform = 'left';
      break;
    case 6:
      swap = true;
      transform = 'right';
      break;
    case 3:
      transform = 'flip';
      break;
    default:
  }
  const width = swap ? image.height : image.width;
  const height = swap ? image.width : image.height;
  return {
    width,
    height,
    transform
  };
}

function getScaledImageFileFromCanvas(image, width, height, transform, fileType) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const context = canvas.getContext('2d');
  let transformParams;
  let swapMeasure = false;
  let imageWidth = width;
  let imageHeight = height;
  switch (transform) {
    case 'left':
      transformParams = [0, -1, 1, 0, 0, height];
      swapMeasure = true;
      break;
    case 'right':
      transformParams = [0, 1, -1, 0, width, 0];
      swapMeasure = true;
      break;
    case 'flip':
      transformParams = [1, 0, 0, -1, 0, height];
      break;
    default:
      transformParams = [1, 0, 0, 1, 0, 0];
  }
  context.setTransform(...transformParams);
  if (swapMeasure) {
    imageHeight = width;
    imageWidth = height;
  }
  context.drawImage(image, 0, 0, imageWidth, imageHeight);
  context.setTransform(1, 0, 0, 1, 0, 0);
  const dataURL = canvas.toDataURL(fileType);
  return dataURLtoBlob(dataURL, fileType);
}

function shouldResize(imageMeasurements, fileSize, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH, DOCUMENT_SIZE } = getCompressionLevel(documentType);
  if (documentType === 'insurance' && fileSize < DOCUMENT_SIZE) {
    return false;
  } else if (fileSize < DOCUMENT_SIZE) {
    return false;
  }
  return imageMeasurements.width > MAX_WIDTH || imageMeasurements.height > MAX_HEIGHT;
}

function compressFile(file, documentType = 'default') {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = _asyncToGenerator(function* () {
        const compressedFile = yield processFile(reader.result, file.size, file.type, documentType);
        resolve(compressedFile);
      });
    } catch (err) {
      reject(err);
    }
  });
}

function getLoginInfo() {
  try {
    const loginInfo = localStorage.getItem('loginInfo');
    if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isString(loginInfo)) {
      return JSON.parse(loginInfo);
    }
  } catch (err) {
    console.log('Error when parsing loginInfo object: ', err);
  }
  return null;
}
function getPostConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    mimeType: 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json', mimeType: 'application/json' };
  return _extends({
    method: 'POST',
    // mode: 'no-cors',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getPutConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    'X-Authorization': loginInfo.token
  } : { 'Content-Type': 'application/json' };
  return _extends({
    method: 'PUT',
    headers
  }, __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(body) ? {} : { body: JSON.stringify(body) });
}

function getGetConfig() {
  const loginInfo = getLoginInfo();
  const headers = loginInfo ? {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${loginInfo.token}`
  } : { 'Content-Type': 'application/json' };
  return {
    method: 'GET',
    headers
  };
}

function convertNumbersToPersian(str) {
  const persianNumbers = '۰۱۲۳۴۵۶۷۸۹';
  let result = '';
  if (!str && str !== 0) {
    return result;
  }
  str = str.toString();
  let convertedChar;
  for (let i = 0; i < str.length; i++) {
    try {
      convertedChar = persianNumbers[str[i]];
    } catch (err) {
      console.log(err);
      convertedChar = str[i];
    }
    result += convertedChar;
  }
  return result;
}

let fetchApi = (() => {
  var _ref3 = _asyncToGenerator(function* (url, config) {
    let flowError;
    try {
      const res = yield fetch(url, config);
      console.log(res);
      console.log(res.status);
      let result = yield res.json();
      console.log(result);
      if (!__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.get(result, 'data'))) {
        result = result.data;
      }
      console.log(res.status);

      if (res.status !== 200) {
        flowError = result;
        console.log(res.status);

        if (res.status === 403) {
          localStorage.clear();
          flowError.message = 'لطفا دوباره وارد شوید!';
          console.log('yeees');
          setTimeout(function () {
            return __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push('/login');
          }, 3000);
        }
      } else {
        return result;
      }
    } catch (err) {
      console.log(err);
      console.debug(`Error when fetching api: ${url}`, err);
      flowError = {
        code: 'UNKNOWN_ERROR',
        message: 'خطای نامشخص لطفا دوباره سعی کنید'
      };
    }
    if (flowError) {
      throw flowError;
    }
  });

  return function fetchApi(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
})();

let setImmediate = (() => {
  var _ref4 = _asyncToGenerator(function* (fn) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        let value = null;
        if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isFunction(fn)) {
          value = fn();
        }
        resolve(value);
      }, 0);
    });
  });

  return function setImmediate(_x7) {
    return _ref4.apply(this, arguments);
  };
})();

function setCookie(name, value, days = 7) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
function getCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  const length = ca.length;
  let i = 0;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

function notifyError(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '10000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.error(message, 'خطا');
}

function notifySuccess(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp'
  };
  toastr.success(message, '');
}

function reloadSelectPicker() {
  try {
    $('.selectpicker').selectpicker('render');
  } catch (err) {
    console.log(err);
  }
}

function loadSelectPicker() {
  try {
    $('.selectpicker').selectpicker({});
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      $('.selectpicker').selectpicker('mobile');
    }
  } catch (err) {
    console.log(err);
  }
}

function getFormData(data) {
  if (__WEBPACK_IMPORTED_MODULE_0_lodash___default.a.isEmpty(data)) {
    throw Error('Invalid input data to create a FormData object');
  }
  let formData;
  try {
    formData = new FormData();
    const keys = __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.keys(data);
    let i;
    const length = keys.length;
    for (i = 0; i < length; i++) {
      formData.append(keys[i], data[keys[i]]);
    }
    return formData;
  } catch (err) {
    console.debug(err);
    throw Error('FORM_DATA_BROWSER_SUPPORT_FAILURE');
  }
}

function loadUserLoginStatus(store) {
  const loginStatusAction = Object(__WEBPACK_IMPORTED_MODULE_4__actions_authentication__["loadLoginStatus"])();
  store.dispatch(loginStatusAction);
}

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL25vdC1mb3VuZC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3MiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL3JvdXRlcy9ub3QtZm91bmQvTm90Rm91bmQuc2NzcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLmNzcy9ub3JtYWxpemUuY3NzPzgyMWIiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzcz85YjRlIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzPzUwNzkiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuanMiLCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L3NyYy9jb21wb25lbnRzL1BhbmVsL1BhbmVsLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9QYW5lbC9sb2dvLnBuZyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzcz8xY2Y0IiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29uZmlnL2NvbXByZXNzaW9uQ29uZmlnLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29uZmlnL3BhdGhzLmpzIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvaGlzdG9yeS5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL3JvdXRlcy9ub3QtZm91bmQvTm90Rm91bmQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9ub3QtZm91bmQvTm90Rm91bmQuc2Nzcz8zYjBmIiwiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvcm91dGVzL25vdC1mb3VuZC9pbmRleC5qcyIsIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL3V0aWxzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi8qISBub3JtYWxpemUuY3NzIHY3LjAuMCB8IE1JVCBMaWNlbnNlIHwgZ2l0aHViLmNvbS9uZWNvbGFzL25vcm1hbGl6ZS5jc3MgKi9cXG4vKiBEb2N1bWVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgbGluZSBoZWlnaHQgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIFByZXZlbnQgYWRqdXN0bWVudHMgb2YgZm9udCBzaXplIGFmdGVyIG9yaWVudGF0aW9uIGNoYW5nZXMgaW5cXG4gKiAgICBJRSBvbiBXaW5kb3dzIFBob25lIGFuZCBpbiBpT1MuXFxuICovXFxuaHRtbCB7XFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIC1tcy10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xcbiAgLyogMiAqLyB9XFxuLyogU2VjdGlvbnNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIFJlbW92ZSB0aGUgbWFyZ2luIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqL1xcbmJvZHkge1xcbiAgbWFyZ2luOiAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXJ0aWNsZSxcXG5hc2lkZSxcXG5mb290ZXIsXFxuaGVhZGVyLFxcbm5hdixcXG5zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgZm9udCBzaXplIGFuZCBtYXJnaW4gb24gYGgxYCBlbGVtZW50cyB3aXRoaW4gYHNlY3Rpb25gIGFuZFxcbiAqIGBhcnRpY2xlYCBjb250ZXh0cyBpbiBDaHJvbWUsIEZpcmVmb3gsIGFuZCBTYWZhcmkuXFxuICovXFxuaDEge1xcbiAgZm9udC1zaXplOiAyZW07XFxuICBtYXJnaW46IDAuNjdlbSAwOyB9XFxuLyogR3JvdXBpbmcgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUuXFxuICovXFxuZmlnY2FwdGlvbixcXG5maWd1cmUsXFxubWFpbiB7XFxuICAvKiAxICovXFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBtYXJnaW4gaW4gSUUgOC5cXG4gKi9cXG5maWd1cmUge1xcbiAgbWFyZ2luOiAxZW0gNDBweDsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIEZpcmVmb3guXFxuICogMi4gU2hvdyB0aGUgb3ZlcmZsb3cgaW4gRWRnZSBhbmQgSUUuXFxuICovXFxuaHIge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogY29udGVudC1ib3g7XFxuICAvKiAxICovXFxuICBoZWlnaHQ6IDA7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIDEuIENvcnJlY3QgdGhlIGluaGVyaXRhbmNlIGFuZCBzY2FsaW5nIG9mIGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gQ29ycmVjdCB0aGUgb2RkIGBlbWAgZm9udCBzaXppbmcgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbnByZSB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qIFRleHQtbGV2ZWwgc2VtYW50aWNzXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGdyYXkgYmFja2dyb3VuZCBvbiBhY3RpdmUgbGlua3MgaW4gSUUgMTAuXFxuICogMi4gUmVtb3ZlIGdhcHMgaW4gbGlua3MgdW5kZXJsaW5lIGluIGlPUyA4KyBhbmQgU2FmYXJpIDgrLlxcbiAqL1xcbmEge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAvKiAxICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbi1za2lwOiBvYmplY3RzO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gUmVtb3ZlIHRoZSBib3R0b20gYm9yZGVyIGluIENocm9tZSA1Ny0gYW5kIEZpcmVmb3ggMzktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB0ZXh0IGRlY29yYXRpb24gaW4gQ2hyb21lLCBFZGdlLCBJRSwgT3BlcmEsIGFuZCBTYWZhcmkuXFxuICovXFxuYWJiclt0aXRsZV0ge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gIC8qIDEgKi9cXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgLyogMiAqL1xcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSBkb3R0ZWQ7XFxuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFByZXZlbnQgdGhlIGR1cGxpY2F0ZSBhcHBsaWNhdGlvbiBvZiBgYm9sZGVyYCBieSB0aGUgbmV4dCBydWxlIGluIFNhZmFyaSA2LlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBpbmhlcml0OyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgd2VpZ2h0IGluIENocm9tZSwgRWRnZSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5iLFxcbnN0cm9uZyB7XFxuICBmb250LXdlaWdodDogYm9sZGVyOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuY29kZSxcXG5rYmQsXFxuc2FtcCB7XFxuICBmb250LWZhbWlseTogbW9ub3NwYWNlLCBtb25vc3BhY2U7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDFlbTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBmb250IHN0eWxlIGluIEFuZHJvaWQgNC4zLS5cXG4gKi9cXG5kZm4ge1xcbiAgZm9udC1zdHlsZTogaXRhbGljOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGJhY2tncm91bmQgYW5kIGNvbG9yIGluIElFIDktLlxcbiAqL1xcbm1hcmsge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmMDtcXG4gIGNvbG9yOiAjMDAwOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc2l6ZSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc21hbGwge1xcbiAgZm9udC1zaXplOiA4MCU7IH1cXG4vKipcXG4gKiBQcmV2ZW50IGBzdWJgIGFuZCBgc3VwYCBlbGVtZW50cyBmcm9tIGFmZmVjdGluZyB0aGUgbGluZSBoZWlnaHQgaW5cXG4gKiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3ViLFxcbnN1cCB7XFxuICBmb250LXNpemU6IDc1JTtcXG4gIGxpbmUtaGVpZ2h0OiAwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lOyB9XFxuc3ViIHtcXG4gIGJvdHRvbTogLTAuMjVlbTsgfVxcbnN1cCB7XFxuICB0b3A6IC0wLjVlbTsgfVxcbi8qIEVtYmVkZGVkIGNvbnRlbnRcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmF1ZGlvLFxcbnZpZGVvIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGlPUyA0LTcuXFxuICovXFxuYXVkaW86bm90KFtjb250cm9sc10pIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBoZWlnaHQ6IDA7IH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGJvcmRlciBvbiBpbWFnZXMgaW5zaWRlIGxpbmtzIGluIElFIDEwLS5cXG4gKi9cXG5pbWcge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lOyB9XFxuLyoqXFxuICogSGlkZSB0aGUgb3ZlcmZsb3cgaW4gSUUuXFxuICovXFxuc3ZnOm5vdCg6cm9vdCkge1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjsgfVxcbi8qIEZvcm1zXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDaGFuZ2UgdGhlIGZvbnQgc3R5bGVzIGluIGFsbCBicm93c2VycyAob3BpbmlvbmF0ZWQpLlxcbiAqIDIuIFJlbW92ZSB0aGUgbWFyZ2luIGluIEZpcmVmb3ggYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaW5wdXQsXFxub3B0Z3JvdXAsXFxuc2VsZWN0LFxcbnRleHRhcmVhIHtcXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xcbiAgLyogMSAqL1xcbiAgZm9udC1zaXplOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgbGluZS1oZWlnaHQ6IDEuMTU7XFxuICAvKiAxICovXFxuICBtYXJnaW46IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBTaG93IHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKiAxLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCB7XFxuICAvKiAxICovXFxuICBvdmVyZmxvdzogdmlzaWJsZTsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRWRnZSwgRmlyZWZveCwgYW5kIElFLlxcbiAqIDEuIFJlbW92ZSB0aGUgaW5oZXJpdGFuY2Ugb2YgdGV4dCB0cmFuc2Zvcm0gaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b24sXFxuc2VsZWN0IHtcXG4gIC8qIDEgKi9cXG4gIHRleHQtdHJhbnNmb3JtOiBub25lOyB9XFxuLyoqXFxuICogMS4gUHJldmVudCBhIFdlYktpdCBidWcgd2hlcmUgKDIpIGRlc3Ryb3lzIG5hdGl2ZSBgYXVkaW9gIGFuZCBgdmlkZW9gXFxuICogICAgY29udHJvbHMgaW4gQW5kcm9pZCA0LlxcbiAqIDIuIENvcnJlY3QgdGhlIGluYWJpbGl0eSB0byBzdHlsZSBjbGlja2FibGUgdHlwZXMgaW4gaU9TIGFuZCBTYWZhcmkuXFxuICovXFxuYnV0dG9uLFxcbmh0bWwgW3R5cGU9XFxcImJ1dHRvblxcXCJdLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXSB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgYm9yZGVyIGFuZCBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuYnV0dG9uOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwicmVzZXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lcixcXG5bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgcGFkZGluZzogMDsgfVxcbi8qKlxcbiAqIFJlc3RvcmUgdGhlIGZvY3VzIHN0eWxlcyB1bnNldCBieSB0aGUgcHJldmlvdXMgcnVsZS5cXG4gKi9cXG5idXR0b246LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOi1tb3otZm9jdXNyaW5nLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTotbW96LWZvY3VzcmluZyB7XFxuICBvdXRsaW5lOiAxcHggZG90dGVkIEJ1dHRvblRleHQ7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBwYWRkaW5nIGluIEZpcmVmb3guXFxuICovXFxuZmllbGRzZXQge1xcbiAgcGFkZGluZzogMC4zNWVtIDAuNzVlbSAwLjYyNWVtOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgdGV4dCB3cmFwcGluZyBpbiBFZGdlIGFuZCBJRS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBjb2xvciBpbmhlcml0YW5jZSBmcm9tIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gSUUuXFxuICogMy4gUmVtb3ZlIHRoZSBwYWRkaW5nIHNvIGRldmVsb3BlcnMgYXJlIG5vdCBjYXVnaHQgb3V0IHdoZW4gdGhleSB6ZXJvIG91dFxcbiAqICAgIGBmaWVsZHNldGAgZWxlbWVudHMgaW4gYWxsIGJyb3dzZXJzLlxcbiAqL1xcbmxlZ2VuZCB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBjb2xvcjogaW5oZXJpdDtcXG4gIC8qIDIgKi9cXG4gIGRpc3BsYXk6IHRhYmxlO1xcbiAgLyogMSAqL1xcbiAgbWF4LXdpZHRoOiAxMDAlO1xcbiAgLyogMSAqL1xcbiAgcGFkZGluZzogMDtcXG4gIC8qIDMgKi9cXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XFxuICAvKiAxICovIH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAyLiBBZGQgdGhlIGNvcnJlY3QgdmVydGljYWwgYWxpZ25tZW50IGluIENocm9tZSwgRmlyZWZveCwgYW5kIE9wZXJhLlxcbiAqL1xcbnByb2dyZXNzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIC8qIDEgKi9cXG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgZGVmYXVsdCB2ZXJ0aWNhbCBzY3JvbGxiYXIgaW4gSUUuXFxuICovXFxudGV4dGFyZWEge1xcbiAgb3ZlcmZsb3c6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgYm94IHNpemluZyBpbiBJRSAxMC0uXFxuICogMi4gUmVtb3ZlIHRoZSBwYWRkaW5nIGluIElFIDEwLS5cXG4gKi9cXG5bdHlwZT1cXFwiY2hlY2tib3hcXFwiXSxcXG5bdHlwZT1cXFwicmFkaW9cXFwiXSB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQ29ycmVjdCB0aGUgY3Vyc29yIHN0eWxlIG9mIGluY3JlbWVudCBhbmQgZGVjcmVtZW50IGJ1dHRvbnMgaW4gQ2hyb21lLlxcbiAqL1xcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixcXG5bdHlwZT1cXFwibnVtYmVyXFxcIl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xcbiAgaGVpZ2h0OiBhdXRvOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgb2RkIGFwcGVhcmFuY2UgaW4gQ2hyb21lIGFuZCBTYWZhcmkuXFxuICogMi4gQ29ycmVjdCB0aGUgb3V0bGluZSBzdHlsZSBpbiBTYWZhcmkuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4dGZpZWxkO1xcbiAgLyogMSAqL1xcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBSZW1vdmUgdGhlIGlubmVyIHBhZGRpbmcgYW5kIGNhbmNlbCBidXR0b25zIGluIENocm9tZSBhbmQgU2FmYXJpIG9uIG1hY09TLlxcbiAqL1xcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtY2FuY2VsLWJ1dHRvbixcXG5bdHlwZT1cXFwic2VhcmNoXFxcIl06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lOyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKiAyLiBDaGFuZ2UgZm9udCBwcm9wZXJ0aWVzIHRvIGBpbmhlcml0YCBpbiBTYWZhcmkuXFxuICovXFxuOjotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcXG4gIC8qIDEgKi9cXG4gIGZvbnQ6IGluaGVyaXQ7XFxuICAvKiAyICovIH1cXG4vKiBJbnRlcmFjdGl2ZVxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBFZGdlLCBJRSwgYW5kIEZpcmVmb3guXFxuICovXFxuZGV0YWlscyxcXG5tZW51IHtcXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLypcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxuc3VtbWFyeSB7XFxuICBkaXNwbGF5OiBsaXN0LWl0ZW07IH1cXG4vKiBTY3JpcHRpbmdcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqL1xcbmNhbnZhcyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG50ZW1wbGF0ZSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuLyogSGlkZGVuXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSAxMC0uXFxuICovXFxuW2hpZGRlbl0ge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvbWVocm5hei9Eb2N1bWVudHMvdHVydGxlUmVhY3QvdHVydGxlLXJlYWN0L25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsNEVBQTRFO0FBQzVFO2dGQUNnRjtBQUNoRjs7OztHQUlHO0FBQ0g7RUFDRSxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLDJCQUEyQjtFQUMzQixPQUFPO0VBQ1AsK0JBQStCO0VBQy9CLE9BQU8sRUFBRTtBQUNYO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIO0VBQ0UsVUFBVSxFQUFFO0FBQ2Q7O0dBRUc7QUFDSDs7Ozs7O0VBTUUsZUFBZSxFQUFFO0FBQ25COzs7R0FHRztBQUNIO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFFO0FBQ3JCO2dGQUNnRjtBQUNoRjs7O0dBR0c7QUFDSDs7O0VBR0UsT0FBTztFQUNQLGVBQWUsRUFBRTtBQUNuQjs7R0FFRztBQUNIO0VBQ0UsaUJBQWlCLEVBQUU7QUFDckI7OztHQUdHO0FBQ0g7RUFDRSxnQ0FBZ0M7VUFDeEIsd0JBQXdCO0VBQ2hDLE9BQU87RUFDUCxVQUFVO0VBQ1YsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPLEVBQUU7QUFDWDs7O0dBR0c7QUFDSDtFQUNFLGtDQUFrQztFQUNsQyxPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU8sRUFBRTtBQUNYO2dGQUNnRjtBQUNoRjs7O0dBR0c7QUFDSDtFQUNFLDhCQUE4QjtFQUM5QixPQUFPO0VBQ1Asc0NBQXNDO0VBQ3RDLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIO0VBQ0Usb0JBQW9CO0VBQ3BCLE9BQU87RUFDUCwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLDBDQUEwQztVQUNsQyxrQ0FBa0M7RUFDMUMsT0FBTyxFQUFFO0FBQ1g7O0dBRUc7QUFDSDs7RUFFRSxxQkFBcUIsRUFBRTtBQUN6Qjs7R0FFRztBQUNIOztFQUVFLG9CQUFvQixFQUFFO0FBQ3hCOzs7R0FHRztBQUNIOzs7RUFHRSxrQ0FBa0M7RUFDbEMsT0FBTztFQUNQLGVBQWU7RUFDZixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIO0VBQ0UsbUJBQW1CLEVBQUU7QUFDdkI7O0dBRUc7QUFDSDtFQUNFLHVCQUF1QjtFQUN2QixZQUFZLEVBQUU7QUFDaEI7O0dBRUc7QUFDSDtFQUNFLGVBQWUsRUFBRTtBQUNuQjs7O0dBR0c7QUFDSDs7RUFFRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQix5QkFBeUIsRUFBRTtBQUM3QjtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsWUFBWSxFQUFFO0FBQ2hCO2dGQUNnRjtBQUNoRjs7R0FFRztBQUNIOztFQUVFLHNCQUFzQixFQUFFO0FBQzFCOztHQUVHO0FBQ0g7RUFDRSxjQUFjO0VBQ2QsVUFBVSxFQUFFO0FBQ2Q7O0dBRUc7QUFDSDtFQUNFLG1CQUFtQixFQUFFO0FBQ3ZCOztHQUVHO0FBQ0g7RUFDRSxpQkFBaUIsRUFBRTtBQUNyQjtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7Ozs7O0VBS0Usd0JBQXdCO0VBQ3hCLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsVUFBVTtFQUNWLE9BQU8sRUFBRTtBQUNYOzs7R0FHRztBQUNIOztFQUVFLE9BQU87RUFDUCxrQkFBa0IsRUFBRTtBQUN0Qjs7O0dBR0c7QUFDSDs7RUFFRSxPQUFPO0VBQ1AscUJBQXFCLEVBQUU7QUFDekI7Ozs7R0FJRztBQUNIOzs7O0VBSUUsMkJBQTJCO0VBQzNCLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7Ozs7RUFJRSxtQkFBbUI7RUFDbkIsV0FBVyxFQUFFO0FBQ2Y7O0dBRUc7QUFDSDs7OztFQUlFLCtCQUErQixFQUFFO0FBQ25DOztHQUVHO0FBQ0g7RUFDRSwrQkFBK0IsRUFBRTtBQUNuQzs7Ozs7R0FLRztBQUNIO0VBQ0UsK0JBQStCO1VBQ3ZCLHVCQUF1QjtFQUMvQixPQUFPO0VBQ1AsZUFBZTtFQUNmLE9BQU87RUFDUCxlQUFlO0VBQ2YsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixPQUFPO0VBQ1AsV0FBVztFQUNYLE9BQU87RUFDUCxvQkFBb0I7RUFDcEIsT0FBTyxFQUFFO0FBQ1g7OztHQUdHO0FBQ0g7RUFDRSxzQkFBc0I7RUFDdEIsT0FBTztFQUNQLHlCQUF5QjtFQUN6QixPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIO0VBQ0UsZUFBZSxFQUFFO0FBQ25COzs7R0FHRztBQUNIOztFQUVFLCtCQUErQjtVQUN2Qix1QkFBdUI7RUFDL0IsT0FBTztFQUNQLFdBQVc7RUFDWCxPQUFPLEVBQUU7QUFDWDs7R0FFRztBQUNIOztFQUVFLGFBQWEsRUFBRTtBQUNqQjs7O0dBR0c7QUFDSDtFQUNFLDhCQUE4QjtFQUM5QixPQUFPO0VBQ1AscUJBQXFCO0VBQ3JCLE9BQU8sRUFBRTtBQUNYOztHQUVHO0FBQ0g7O0VBRUUseUJBQXlCLEVBQUU7QUFDN0I7OztHQUdHO0FBQ0g7RUFDRSwyQkFBMkI7RUFDM0IsT0FBTztFQUNQLGNBQWM7RUFDZCxPQUFPLEVBQUU7QUFDWDtnRkFDZ0Y7QUFDaEY7OztHQUdHO0FBQ0g7O0VBRUUsZUFBZSxFQUFFO0FBQ25COztHQUVHO0FBQ0g7RUFDRSxtQkFBbUIsRUFBRTtBQUN2QjtnRkFDZ0Y7QUFDaEY7O0dBRUc7QUFDSDtFQUNFLHNCQUFzQixFQUFFO0FBQzFCOztHQUVHO0FBQ0g7RUFDRSxjQUFjLEVBQUU7QUFDbEI7Z0ZBQ2dGO0FBQ2hGOztHQUVHO0FBQ0g7RUFDRSxjQUFjLEVBQUVcIixcImZpbGVcIjpcIm5vcm1hbGl6ZS5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLyohIG5vcm1hbGl6ZS5jc3MgdjcuMC4wIHwgTUlUIExpY2Vuc2UgfCBnaXRodWIuY29tL25lY29sYXMvbm9ybWFsaXplLmNzcyAqL1xcbi8qIERvY3VtZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBsaW5lIGhlaWdodCBpbiBhbGwgYnJvd3NlcnMuXFxuICogMi4gUHJldmVudCBhZGp1c3RtZW50cyBvZiBmb250IHNpemUgYWZ0ZXIgb3JpZW50YXRpb24gY2hhbmdlcyBpblxcbiAqICAgIElFIG9uIFdpbmRvd3MgUGhvbmUgYW5kIGluIGlPUy5cXG4gKi9cXG5odG1sIHtcXG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xcbiAgLyogMSAqL1xcbiAgLW1zLXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7XFxuICAvKiAyICovXFxuICAtd2Via2l0LXRleHQtc2l6ZS1hZGp1c3Q6IDEwMCU7XFxuICAvKiAyICovIH1cXG4vKiBTZWN0aW9uc1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogUmVtb3ZlIHRoZSBtYXJnaW4gaW4gYWxsIGJyb3dzZXJzIChvcGluaW9uYXRlZCkuXFxuICovXFxuYm9keSB7XFxuICBtYXJnaW46IDA7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKi9cXG5hcnRpY2xlLFxcbmFzaWRlLFxcbmZvb3RlcixcXG5oZWFkZXIsXFxubmF2LFxcbnNlY3Rpb24ge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBmb250IHNpemUgYW5kIG1hcmdpbiBvbiBgaDFgIGVsZW1lbnRzIHdpdGhpbiBgc2VjdGlvbmAgYW5kXFxuICogYGFydGljbGVgIGNvbnRleHRzIGluIENocm9tZSwgRmlyZWZveCwgYW5kIFNhZmFyaS5cXG4gKi9cXG5oMSB7XFxuICBmb250LXNpemU6IDJlbTtcXG4gIG1hcmdpbjogMC42N2VtIDA7IH1cXG4vKiBHcm91cGluZyBjb250ZW50XFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRSA5LS5cXG4gKiAxLiBBZGQgdGhlIGNvcnJlY3QgZGlzcGxheSBpbiBJRS5cXG4gKi9cXG5maWdjYXB0aW9uLFxcbmZpZ3VyZSxcXG5tYWluIHtcXG4gIC8qIDEgKi9cXG4gIGRpc3BsYXk6IGJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IG1hcmdpbiBpbiBJRSA4LlxcbiAqL1xcbmZpZ3VyZSB7XFxuICBtYXJnaW46IDFlbSA0MHB4OyB9XFxuLyoqXFxuICogMS4gQWRkIHRoZSBjb3JyZWN0IGJveCBzaXppbmcgaW4gRmlyZWZveC5cXG4gKiAyLiBTaG93IHRoZSBvdmVyZmxvdyBpbiBFZGdlIGFuZCBJRS5cXG4gKi9cXG5ociB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcXG4gIC8qIDEgKi9cXG4gIGhlaWdodDogMDtcXG4gIC8qIDEgKi9cXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogMS4gQ29ycmVjdCB0aGUgaW5oZXJpdGFuY2UgYW5kIHNjYWxpbmcgb2YgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvZGQgYGVtYCBmb250IHNpemluZyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxucHJlIHtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogMiAqLyB9XFxuLyogVGV4dC1sZXZlbCBzZW1hbnRpY3NcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIFJlbW92ZSB0aGUgZ3JheSBiYWNrZ3JvdW5kIG9uIGFjdGl2ZSBsaW5rcyBpbiBJRSAxMC5cXG4gKiAyLiBSZW1vdmUgZ2FwcyBpbiBsaW5rcyB1bmRlcmxpbmUgaW4gaU9TIDgrIGFuZCBTYWZhcmkgOCsuXFxuICovXFxuYSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gIC8qIDEgKi9cXG4gIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLXNraXA6IG9iamVjdHM7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiAxLiBSZW1vdmUgdGhlIGJvdHRvbSBib3JkZXIgaW4gQ2hyb21lIDU3LSBhbmQgRmlyZWZveCAzOS0uXFxuICogMi4gQWRkIHRoZSBjb3JyZWN0IHRleHQgZGVjb3JhdGlvbiBpbiBDaHJvbWUsIEVkZ2UsIElFLCBPcGVyYSwgYW5kIFNhZmFyaS5cXG4gKi9cXG5hYmJyW3RpdGxlXSB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgLyogMSAqL1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAvKiAyICovXFxuICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lIGRvdHRlZDtcXG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgZG90dGVkO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUHJldmVudCB0aGUgZHVwbGljYXRlIGFwcGxpY2F0aW9uIG9mIGBib2xkZXJgIGJ5IHRoZSBuZXh0IHJ1bGUgaW4gU2FmYXJpIDYuXFxuICovXFxuYixcXG5zdHJvbmcge1xcbiAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCB3ZWlnaHQgaW4gQ2hyb21lLCBFZGdlLCBhbmQgU2FmYXJpLlxcbiAqL1xcbmIsXFxuc3Ryb25nIHtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmhlcml0YW5jZSBhbmQgc2NhbGluZyBvZiBmb250IHNpemUgaW4gYWxsIGJyb3dzZXJzLlxcbiAqIDIuIENvcnJlY3QgdGhlIG9kZCBgZW1gIGZvbnQgc2l6aW5nIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5jb2RlLFxcbmtiZCxcXG5zYW1wIHtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcXG4gIC8qIDEgKi9cXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGZvbnQgc3R5bGUgaW4gQW5kcm9pZCA0LjMtLlxcbiAqL1xcbmRmbiB7XFxuICBmb250LXN0eWxlOiBpdGFsaWM7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgYmFja2dyb3VuZCBhbmQgY29sb3IgaW4gSUUgOS0uXFxuICovXFxubWFyayB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYwO1xcbiAgY29sb3I6ICMwMDA7IH1cXG4vKipcXG4gKiBBZGQgdGhlIGNvcnJlY3QgZm9udCBzaXplIGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zbWFsbCB7XFxuICBmb250LXNpemU6IDgwJTsgfVxcbi8qKlxcbiAqIFByZXZlbnQgYHN1YmAgYW5kIGBzdXBgIGVsZW1lbnRzIGZyb20gYWZmZWN0aW5nIHRoZSBsaW5lIGhlaWdodCBpblxcbiAqIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zdWIsXFxuc3VwIHtcXG4gIGZvbnQtc2l6ZTogNzUlO1xcbiAgbGluZS1oZWlnaHQ6IDA7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB2ZXJ0aWNhbC1hbGlnbjogYmFzZWxpbmU7IH1cXG5zdWIge1xcbiAgYm90dG9tOiAtMC4yNWVtOyB9XFxuc3VwIHtcXG4gIHRvcDogLTAuNWVtOyB9XFxuLyogRW1iZWRkZWQgY29udGVudFxcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuYXVkaW8sXFxudmlkZW8ge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB9XFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gaU9TIDQtNy5cXG4gKi9cXG5hdWRpbzpub3QoW2NvbnRyb2xzXSkge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGhlaWdodDogMDsgfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgYm9yZGVyIG9uIGltYWdlcyBpbnNpZGUgbGlua3MgaW4gSUUgMTAtLlxcbiAqL1xcbmltZyB7XFxuICBib3JkZXItc3R5bGU6IG5vbmU7IH1cXG4vKipcXG4gKiBIaWRlIHRoZSBvdmVyZmxvdyBpbiBJRS5cXG4gKi9cXG5zdmc6bm90KDpyb290KSB7XFxuICBvdmVyZmxvdzogaGlkZGVuOyB9XFxuLyogRm9ybXNcXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIDEuIENoYW5nZSB0aGUgZm9udCBzdHlsZXMgaW4gYWxsIGJyb3dzZXJzIChvcGluaW9uYXRlZCkuXFxuICogMi4gUmVtb3ZlIHRoZSBtYXJnaW4gaW4gRmlyZWZveCBhbmQgU2FmYXJpLlxcbiAqL1xcbmJ1dHRvbixcXG5pbnB1dCxcXG5vcHRncm91cCxcXG5zZWxlY3QsXFxudGV4dGFyZWEge1xcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XFxuICAvKiAxICovXFxuICBmb250LXNpemU6IDEwMCU7XFxuICAvKiAxICovXFxuICBsaW5lLWhlaWdodDogMS4xNTtcXG4gIC8qIDEgKi9cXG4gIG1hcmdpbjogMDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFNob3cgdGhlIG92ZXJmbG93IGluIElFLlxcbiAqIDEuIFNob3cgdGhlIG92ZXJmbG93IGluIEVkZ2UuXFxuICovXFxuYnV0dG9uLFxcbmlucHV0IHtcXG4gIC8qIDEgKi9cXG4gIG92ZXJmbG93OiB2aXNpYmxlOyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbmhlcml0YW5jZSBvZiB0ZXh0IHRyYW5zZm9ybSBpbiBFZGdlLCBGaXJlZm94LCBhbmQgSUUuXFxuICogMS4gUmVtb3ZlIHRoZSBpbmhlcml0YW5jZSBvZiB0ZXh0IHRyYW5zZm9ybSBpbiBGaXJlZm94LlxcbiAqL1xcbmJ1dHRvbixcXG5zZWxlY3Qge1xcbiAgLyogMSAqL1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7IH1cXG4vKipcXG4gKiAxLiBQcmV2ZW50IGEgV2ViS2l0IGJ1ZyB3aGVyZSAoMikgZGVzdHJveXMgbmF0aXZlIGBhdWRpb2AgYW5kIGB2aWRlb2BcXG4gKiAgICBjb250cm9scyBpbiBBbmRyb2lkIDQuXFxuICogMi4gQ29ycmVjdCB0aGUgaW5hYmlsaXR5IHRvIHN0eWxlIGNsaWNrYWJsZSB0eXBlcyBpbiBpT1MgYW5kIFNhZmFyaS5cXG4gKi9cXG5idXR0b24sXFxuaHRtbCBbdHlwZT1cXFwiYnV0dG9uXFxcIl0sXFxuW3R5cGU9XFxcInJlc2V0XFxcIl0sXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBpbm5lciBib3JkZXIgYW5kIHBhZGRpbmcgaW4gRmlyZWZveC5cXG4gKi9cXG5idXR0b246Oi1tb3otZm9jdXMtaW5uZXIsXFxuW3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJyZXNldFxcXCJdOjotbW96LWZvY3VzLWlubmVyLFxcblt0eXBlPVxcXCJzdWJtaXRcXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBib3JkZXItc3R5bGU6IG5vbmU7XFxuICBwYWRkaW5nOiAwOyB9XFxuLyoqXFxuICogUmVzdG9yZSB0aGUgZm9jdXMgc3R5bGVzIHVuc2V0IGJ5IHRoZSBwcmV2aW91cyBydWxlLlxcbiAqL1xcbmJ1dHRvbjotbW96LWZvY3VzcmluZyxcXG5bdHlwZT1cXFwiYnV0dG9uXFxcIl06LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcInJlc2V0XFxcIl06LW1vei1mb2N1c3JpbmcsXFxuW3R5cGU9XFxcInN1Ym1pdFxcXCJdOi1tb3otZm9jdXNyaW5nIHtcXG4gIG91dGxpbmU6IDFweCBkb3R0ZWQgQnV0dG9uVGV4dDsgfVxcbi8qKlxcbiAqIENvcnJlY3QgdGhlIHBhZGRpbmcgaW4gRmlyZWZveC5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBwYWRkaW5nOiAwLjM1ZW0gMC43NWVtIDAuNjI1ZW07IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSB0ZXh0IHdyYXBwaW5nIGluIEVkZ2UgYW5kIElFLlxcbiAqIDIuIENvcnJlY3QgdGhlIGNvbG9yIGluaGVyaXRhbmNlIGZyb20gYGZpZWxkc2V0YCBlbGVtZW50cyBpbiBJRS5cXG4gKiAzLiBSZW1vdmUgdGhlIHBhZGRpbmcgc28gZGV2ZWxvcGVycyBhcmUgbm90IGNhdWdodCBvdXQgd2hlbiB0aGV5IHplcm8gb3V0XFxuICogICAgYGZpZWxkc2V0YCBlbGVtZW50cyBpbiBhbGwgYnJvd3NlcnMuXFxuICovXFxubGVnZW5kIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIC8qIDEgKi9cXG4gIGNvbG9yOiBpbmhlcml0O1xcbiAgLyogMiAqL1xcbiAgZGlzcGxheTogdGFibGU7XFxuICAvKiAxICovXFxuICBtYXgtd2lkdGg6IDEwMCU7XFxuICAvKiAxICovXFxuICBwYWRkaW5nOiAwO1xcbiAgLyogMyAqL1xcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcXG4gIC8qIDEgKi8gfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDIuIEFkZCB0aGUgY29ycmVjdCB2ZXJ0aWNhbCBhbGlnbm1lbnQgaW4gQ2hyb21lLCBGaXJlZm94LCBhbmQgT3BlcmEuXFxuICovXFxucHJvZ3Jlc3Mge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgLyogMSAqL1xcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lO1xcbiAgLyogMiAqLyB9XFxuLyoqXFxuICogUmVtb3ZlIHRoZSBkZWZhdWx0IHZlcnRpY2FsIHNjcm9sbGJhciBpbiBJRS5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICBvdmVyZmxvdzogYXV0bzsgfVxcbi8qKlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBib3ggc2l6aW5nIGluIElFIDEwLS5cXG4gKiAyLiBSZW1vdmUgdGhlIHBhZGRpbmcgaW4gSUUgMTAtLlxcbiAqL1xcblt0eXBlPVxcXCJjaGVja2JveFxcXCJdLFxcblt0eXBlPVxcXCJyYWRpb1xcXCJdIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIC8qIDEgKi9cXG4gIHBhZGRpbmc6IDA7XFxuICAvKiAyICovIH1cXG4vKipcXG4gKiBDb3JyZWN0IHRoZSBjdXJzb3Igc3R5bGUgb2YgaW5jcmVtZW50IGFuZCBkZWNyZW1lbnQgYnV0dG9ucyBpbiBDaHJvbWUuXFxuICovXFxuW3R5cGU9XFxcIm51bWJlclxcXCJdOjotd2Via2l0LWlubmVyLXNwaW4tYnV0dG9uLFxcblt0eXBlPVxcXCJudW1iZXJcXFwiXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7XFxuICBoZWlnaHQ6IGF1dG87IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBvZGQgYXBwZWFyYW5jZSBpbiBDaHJvbWUgYW5kIFNhZmFyaS5cXG4gKiAyLiBDb3JyZWN0IHRoZSBvdXRsaW5lIHN0eWxlIGluIFNhZmFyaS5cXG4gKi9cXG5bdHlwZT1cXFwic2VhcmNoXFxcIl0ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQ7XFxuICAvKiAxICovXFxuICBvdXRsaW5lLW9mZnNldDogLTJweDtcXG4gIC8qIDIgKi8gfVxcbi8qKlxcbiAqIFJlbW92ZSB0aGUgaW5uZXIgcGFkZGluZyBhbmQgY2FuY2VsIGJ1dHRvbnMgaW4gQ2hyb21lIGFuZCBTYWZhcmkgb24gbWFjT1MuXFxuICovXFxuW3R5cGU9XFxcInNlYXJjaFxcXCJdOjotd2Via2l0LXNlYXJjaC1jYW5jZWwtYnV0dG9uLFxcblt0eXBlPVxcXCJzZWFyY2hcXFwiXTo6LXdlYmtpdC1zZWFyY2gtZGVjb3JhdGlvbiB7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7IH1cXG4vKipcXG4gKiAxLiBDb3JyZWN0IHRoZSBpbmFiaWxpdHkgdG8gc3R5bGUgY2xpY2thYmxlIHR5cGVzIGluIGlPUyBhbmQgU2FmYXJpLlxcbiAqIDIuIENoYW5nZSBmb250IHByb3BlcnRpZXMgdG8gYGluaGVyaXRgIGluIFNhZmFyaS5cXG4gKi9cXG46Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9uIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xcbiAgLyogMSAqL1xcbiAgZm9udDogaW5oZXJpdDtcXG4gIC8qIDIgKi8gfVxcbi8qIEludGVyYWN0aXZlXFxuICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG4vKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDktLlxcbiAqIDEuIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIEVkZ2UsIElFLCBhbmQgRmlyZWZveC5cXG4gKi9cXG5kZXRhaWxzLFxcbm1lbnUge1xcbiAgZGlzcGxheTogYmxvY2s7IH1cXG4vKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIGFsbCBicm93c2Vycy5cXG4gKi9cXG5zdW1tYXJ5IHtcXG4gIGRpc3BsYXk6IGxpc3QtaXRlbTsgfVxcbi8qIFNjcmlwdGluZ1xcbiAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuLyoqXFxuICogQWRkIHRoZSBjb3JyZWN0IGRpc3BsYXkgaW4gSUUgOS0uXFxuICovXFxuY2FudmFzIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFLlxcbiAqL1xcbnRlbXBsYXRlIHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG4vKiBIaWRkZW5cXG4gICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi8qKlxcbiAqIEFkZCB0aGUgY29ycmVjdCBkaXNwbGF5IGluIElFIDEwLS5cXG4gKi9cXG5baGlkZGVuXSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG4gICAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxufVxcbi53aGl0ZSB7XFxuICAgIGNvbG9yOiB3aGl0ZTtcXG59XFxuLmJsYWNrIHtcXG4gICAgY29sb3I6IGJsYWNrO1xcbn1cXG4ucHVycGxlIHtcXG4gICAgY29sb3I6ICM0YzBkNmJcXG59XFxuLmxpZ2h0LWJsdWUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzYWQyY2JcXG59XFxuLndoaXRlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG59XFxuLmRhcmstZ3JlZW4tYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxODU5NTY7XFxufVxcbi5mb250LWJvbGQge1xcbiAgICBmb250LXdlaWdodDogODAwO1xcbn1cXG4uZm9udC1saWdodCB7XFxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcbi5mb250LW1lZGl1bSB7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcbi5jZW50ZXItY29udGVudCB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4uY2VudGVyLWNvbHVtbiB7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xcbiAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0IHtcXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XFxufVxcbi50ZXh0LWFsaWduLWxlZnQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5uby1saXN0LXN0eWxlIHtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG59XFxuLmNsZWFyLWlucHV0IHtcXG4gICAgb3V0bGluZTogbm9uZTtcXG4gICAgYm9yZGVyOiBub25lO1xcbn1cXG4uYm9yZGVyLXJhZGl1cyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDlweDtcXG59XFxuLmhhcy10cmFuc2l0aW9uIHtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG59XFxuLnNvY2lhbC1uZXR3b3JrLWljb24ge1xcbiAgbWF4LXdpZHRoOiAyMHB4OyB9XFxuLnNpdGUtZm9vdGVyIHtcXG4gIHBhZGRpbmc6IDElIDQlO1xcbiAgaGVpZ2h0OiA4dmg7XFxuICBkaXJlY3Rpb246IHJ0bDsgfVxcbi5zb2NpYWwtaWNvbnMtbGlzdCA+IGxpIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5zaXRlLWZvb3RlciB7XFxuICAgIGhlaWdodDogMTN2aDsgfSB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDMyMHB4KSB7XFxuICAuc2l0ZS1mb290ZXIgPiAucm93ID4gLmNvbC14cy0xMjpmaXJzdC1jaGlsZCB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7SUFDSSxxQkFBcUI7Q0FDeEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGNBQWM7Q0FDakI7QUFDRDtJQUNJLHlCQUF5QjtDQUM1QjtBQUNEO0lBQ0ksd0JBQXdCO0NBQzNCO0FBQ0Q7SUFDSSwwQkFBMEI7Q0FDN0I7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDBCQUEwQjtRQUN0Qix1QkFBdUI7WUFDbkIsb0JBQW9CO0NBQy9CO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQyw2QkFBNkI7SUFDN0IsOEJBQThCO1FBQzFCLDJCQUEyQjtZQUN2Qix1QkFBdUI7Q0FDbEM7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksZ0RBQWdEO0lBQ2hELHdEQUF3RDtJQUN4RCxnREFBZ0Q7SUFDaEQsMkNBQTJDO0lBQzNDLHdDQUF3QztJQUN4Qyw2RUFBNkU7Q0FDaEY7QUFDRDtFQUNFLGdCQUFnQixFQUFFO0FBQ3BCO0VBQ0UsZUFBZTtFQUNmLFlBQVk7RUFDWixlQUFlLEVBQUU7QUFDbkI7RUFDRSxzQkFBc0I7RUFDdEIsaUJBQWlCLEVBQUU7QUFDckI7RUFDRTtJQUNFLGFBQWEsRUFBRSxFQUFFO0FBQ3JCO0VBQ0U7SUFDRSxlQUFlLEVBQUUsRUFBRVwiLFwiZmlsZVwiOlwibWFpbi5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiYm9keSB7XFxuICAgIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbn1cXG4ud2hpdGUge1xcbiAgICBjb2xvcjogd2hpdGU7XFxufVxcbi5ibGFjayB7XFxuICAgIGNvbG9yOiBibGFjaztcXG59XFxuLnB1cnBsZSB7XFxuICAgIGNvbG9yOiAjNGMwZDZiXFxufVxcbi5saWdodC1ibHVlLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2FkMmNiXFxufVxcbi53aGl0ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbi5kYXJrLWdyZWVuLWJhY2tncm91bmQge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg1OTU2O1xcbn1cXG4uZm9udC1ib2xkIHtcXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcXG59XFxuLmZvbnQtbGlnaHQge1xcbiAgICBmb250LXdlaWdodDogMjAwO1xcbn1cXG4uZm9udC1tZWRpdW0ge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG4uY2VudGVyLWNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLmNlbnRlci1jb2x1bW4ge1xcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcXG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG4udGV4dC1hbGlnbi1sZWZ0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVyIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4ubm8tbGlzdC1zdHlsZSB7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jbGVhci1pbnB1dCB7XFxuICAgIG91dGxpbmU6IG5vbmU7XFxuICAgIGJvcmRlcjogbm9uZTtcXG59XFxuLmJvcmRlci1yYWRpdXMge1xcbiAgICBib3JkZXItcmFkaXVzOiA5cHg7XFxufVxcbi5oYXMtdHJhbnNpdGlvbiB7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dCwgLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxufVxcbi5zb2NpYWwtbmV0d29yay1pY29uIHtcXG4gIG1heC13aWR0aDogMjBweDsgfVxcbi5zaXRlLWZvb3RlciB7XFxuICBwYWRkaW5nOiAxJSA0JTtcXG4gIGhlaWdodDogOHZoO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4uc29jaWFsLWljb25zLWxpc3QgPiBsaSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tcmlnaHQ6IDMlOyB9XFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuc2l0ZS1mb290ZXIge1xcbiAgICBoZWlnaHQ6IDEzdmg7IH0gfVxcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLnNpdGUtZm9vdGVyID4gLnJvdyA+IC5jb2wteHMtMTI6Zmlyc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tdG9wOiA1JTsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL21haW4uY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiQGNoYXJzZXQgXFxcIlVURi04XFxcIjtcXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuOnJvb3Qge1xcbiAgLypcXG4gICAqIFR5cG9ncmFwaHlcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1mb250LWZhbWlseS1iYXNlOiAnU2Vnb2UgVUknLCAnSGVsdmV0aWNhTmV1ZS1MaWdodCcsIHNhbnMtc2VyaWY7XFxuXFxuICAvKlxcbiAgICogTGF5b3V0XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tbWF4LWNvbnRlbnQtd2lkdGg6IDEwMDBweDtcXG5cXG4gIC8qXFxuICAgKiBNZWRpYSBxdWVyaWVzIGJyZWFrcG9pbnRzXFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tc2NyZWVuLXhzLW1pbjogNDgwcHg7ICAvKiBFeHRyYSBzbWFsbCBzY3JlZW4gLyBwaG9uZSAqL1xcbiAgLS1zY3JlZW4tc20tbWluOiA3NjhweDsgIC8qIFNtYWxsIHNjcmVlbiAvIHRhYmxldCAqL1xcbiAgLS1zY3JlZW4tbWQtbWluOiA5OTJweDsgIC8qIE1lZGl1bSBzY3JlZW4gLyBkZXNrdG9wICovXFxuICAtLXNjcmVlbi1sZy1taW46IDEyMDBweDsgLyogTGFyZ2Ugc2NyZWVuIC8gd2lkZSBkZXNrdG9wICovXFxufVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7IH1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0udHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNjAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNzAwOyB9XFxuLypcXG4gKiBub3JtYWxpemUuY3NzIGlzIGltcG9ydGVkIGluIEpTIGZpbGUuXFxuICogSWYgeW91IGltcG9ydCBleHRlcm5hbCBDU1MgZmlsZSBmcm9tIHlvdXIgaW50ZXJuYWwgQ1NTXFxuICogdGhlbiBpdCB3aWxsIGJlIGlubGluZWQgYW5kIHByb2Nlc3NlZCB3aXRoIENTUyBtb2R1bGVzLlxcbiAqL1xcbi8qXFxuICogQmFzZSBzdHlsZXNcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbmh0bWwge1xcbiAgY29sb3I6ICMyMjI7XFxuICBmb250LXdlaWdodDogMTAwO1xcbiAgZm9udC1zaXplOiAxZW07XFxuICAvKiB+MTZweDsgKi9cXG4gIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1iYXNlKTtcXG4gIGxpbmUtaGVpZ2h0OiAxLjM3NTtcXG4gIC8qIH4yMnB4ICovIH1cXG5ib2R5IHtcXG4gIG1hcmdpbjogMDtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtOyB9XFxuYSB7XFxuICBjb2xvcjogIzAwNzRjMjsgfVxcbi8qXFxuICogUmVtb3ZlIHRleHQtc2hhZG93IGluIHNlbGVjdGlvbiBoaWdobGlnaHQ6XFxuICogaHR0cHM6Ly90d2l0dGVyLmNvbS9taWtldGF5bHIvc3RhdHVzLzEyMjI4ODA1MzAxXFxuICpcXG4gKiBUaGVzZSBzZWxlY3Rpb24gcnVsZSBzZXRzIGhhdmUgdG8gYmUgc2VwYXJhdGUuXFxuICogQ3VzdG9taXplIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIHRvIG1hdGNoIHlvdXIgZGVzaWduLlxcbiAqL1xcbjo6LW1vei1zZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lOyB9XFxuOjpzZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lOyB9XFxuLypcXG4gKiBBIGJldHRlciBsb29raW5nIGRlZmF1bHQgaG9yaXpvbnRhbCBydWxlXFxuICovXFxuaHIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDFweDtcXG4gIGJvcmRlcjogMDtcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xcbiAgbWFyZ2luOiAxZW0gMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKlxcbiAqIFJlbW92ZSB0aGUgZ2FwIGJldHdlZW4gYXVkaW8sIGNhbnZhcywgaWZyYW1lcyxcXG4gKiBpbWFnZXMsIHZpZGVvcyBhbmQgdGhlIGJvdHRvbSBvZiB0aGVpciBjb250YWluZXJzOlxcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9oNWJwL2h0bWw1LWJvaWxlcnBsYXRlL2lzc3Vlcy80NDBcXG4gKi9cXG5hdWRpbyxcXG5jYW52YXMsXFxuaWZyYW1lLFxcbmltZyxcXG5zdmcsXFxudmlkZW8ge1xcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTsgfVxcbi8qXFxuICogUmVtb3ZlIGRlZmF1bHQgZmllbGRzZXQgc3R5bGVzLlxcbiAqL1xcbmZpZWxkc2V0IHtcXG4gIGJvcmRlcjogMDtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG4vKlxcbiAqIEFsbG93IG9ubHkgdmVydGljYWwgcmVzaXppbmcgb2YgdGV4dGFyZWFzLlxcbiAqL1xcbnRleHRhcmVhIHtcXG4gIHJlc2l6ZTogdmVydGljYWw7IH1cXG4vKlxcbiAqIEJyb3dzZXIgdXBncmFkZSBwcm9tcHRcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbi5icm93c2VydXBncmFkZSB7XFxuICBtYXJnaW46IDAuMmVtIDA7XFxuICBiYWNrZ3JvdW5kOiAjY2NjO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAwLjJlbSAwOyB9XFxuLypcXG4gKiBQcmludCBzdHlsZXNcXG4gKiBJbmxpbmVkIHRvIGF2b2lkIHRoZSBhZGRpdGlvbmFsIEhUVFAgcmVxdWVzdDpcXG4gKiBodHRwOi8vd3d3LnBocGllZC5jb20vZGVsYXktbG9hZGluZy15b3VyLXByaW50LWNzcy9cXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcbkBtZWRpYSBwcmludCB7XFxuICAqLFxcbiAgKjo6YmVmb3JlLFxcbiAgKjo6YWZ0ZXIge1xcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xcbiAgICAvKiBCbGFjayBwcmludHMgZmFzdGVyOiBodHRwOi8vd3d3LnNhbmJlaWppLmNvbS9hcmNoaXZlcy85NTMgKi9cXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICB0ZXh0LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50OyB9XFxuICBhLFxcbiAgYTp2aXNpdGVkIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7IH1cXG4gIGFbaHJlZl06OmFmdGVyIHtcXG4gICAgY29udGVudDogXFxcIiAoXFxcIiBhdHRyKGhyZWYpIFxcXCIpXFxcIjsgfVxcbiAgYWJiclt0aXRsZV06OmFmdGVyIHtcXG4gICAgY29udGVudDogXFxcIiAoXFxcIiBhdHRyKHRpdGxlKSBcXFwiKVxcXCI7IH1cXG4gIC8qXFxuICAgKiBEb24ndCBzaG93IGxpbmtzIHRoYXQgYXJlIGZyYWdtZW50IGlkZW50aWZpZXJzLFxcbiAgICogb3IgdXNlIHRoZSBgamF2YXNjcmlwdDpgIHBzZXVkbyBwcm90b2NvbFxcbiAgICovXFxuICBhW2hyZWZePScjJ106OmFmdGVyLFxcbiAgYVtocmVmXj0namF2YXNjcmlwdDonXTo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiAnJzsgfVxcbiAgcHJlLFxcbiAgYmxvY2txdW90ZSB7XFxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM5OTk7XFxuICAgIHBhZ2UtYnJlYWstaW5zaWRlOiBhdm9pZDsgfVxcbiAgLypcXG4gICAqIFByaW50aW5nIFRhYmxlczpcXG4gICAqIGh0dHA6Ly9jc3MtZGlzY3Vzcy5pbmN1dGlvLmNvbS93aWtpL1ByaW50aW5nX1RhYmxlc1xcbiAgICovXFxuICB0aGVhZCB7XFxuICAgIGRpc3BsYXk6IHRhYmxlLWhlYWRlci1ncm91cDsgfVxcbiAgdHIsXFxuICBpbWcge1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIGltZyB7XFxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50OyB9XFxuICBwLFxcbiAgaDIsXFxuICBoMyB7XFxuICAgIG9ycGhhbnM6IDM7XFxuICAgIHdpZG93czogMzsgfVxcbiAgaDIsXFxuICBoMyB7XFxuICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGF2b2lkOyB9IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQSxpQkFBaUI7QUFDakI7Ozs7Ozs7R0FPRztBQUNIOzs7Ozs7O0dBT0c7QUFDSDtFQUNFOztnRkFFOEU7O0VBRTlFLGtFQUFrRTs7RUFFbEU7O2dGQUU4RTs7RUFFOUUsNEJBQTRCOztFQUU1Qjs7Z0ZBRThFOztFQUU5RSx1QkFBdUIsRUFBRSxnQ0FBZ0M7RUFDekQsdUJBQXVCLEVBQUUsMkJBQTJCO0VBQ3BELHVCQUF1QixFQUFFLDZCQUE2QjtFQUN0RCx3QkFBd0IsQ0FBQyxpQ0FBaUM7Q0FDM0Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixtQ0FBbUM7RUFDbkMsdUtBQXVLO0VBQ3ZLLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UscUJBQXFCO0VBQ3JCLDZCQUE2QjtFQUM3QixxSkFBcUo7RUFDckosaUJBQWlCLEVBQUU7QUFDckI7RUFDRSxxQkFBcUI7RUFDckIsa0NBQWtDO0VBQ2xDLG9LQUFvSztFQUNwSyxpQkFBaUIsRUFBRTtBQUNyQjs7OztHQUlHO0FBQ0g7O2dGQUVnRjtBQUNoRjtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLFlBQVk7RUFDWixxQ0FBcUM7RUFDckMsbUJBQW1CO0VBQ25CLFdBQVcsRUFBRTtBQUNmO0VBQ0UsVUFBVTtFQUNWLHFCQUFxQixFQUFFO0FBQ3pCO0VBQ0UsZUFBZSxFQUFFO0FBQ25COzs7Ozs7R0FNRztBQUNIO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQixFQUFFO0FBQ3RCO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQixFQUFFO0FBQ3RCOztHQUVHO0FBQ0g7RUFDRSxlQUFlO0VBQ2YsWUFBWTtFQUNaLFVBQVU7RUFDViwyQkFBMkI7RUFDM0IsY0FBYztFQUNkLFdBQVcsRUFBRTtBQUNmOzs7O0dBSUc7QUFDSDs7Ozs7O0VBTUUsdUJBQXVCLEVBQUU7QUFDM0I7O0dBRUc7QUFDSDtFQUNFLFVBQVU7RUFDVixVQUFVO0VBQ1YsV0FBVyxFQUFFO0FBQ2Y7O0dBRUc7QUFDSDtFQUNFLGlCQUFpQixFQUFFO0FBQ3JCOztnRkFFZ0Y7QUFDaEY7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixpQkFBaUIsRUFBRTtBQUNyQjs7OztnRkFJZ0Y7QUFDaEY7RUFDRTs7O0lBR0UsbUNBQW1DO0lBQ25DLHVCQUF1QjtJQUN2QiwrREFBK0Q7SUFDL0Qsb0NBQW9DO1lBQzVCLDRCQUE0QjtJQUNwQyw2QkFBNkIsRUFBRTtFQUNqQzs7SUFFRSwyQkFBMkIsRUFBRTtFQUMvQjtJQUNFLDZCQUE2QixFQUFFO0VBQ2pDO0lBQ0UsOEJBQThCLEVBQUU7RUFDbEM7OztLQUdHO0VBQ0g7O0lBRUUsWUFBWSxFQUFFO0VBQ2hCOztJQUVFLHVCQUF1QjtJQUN2Qix5QkFBeUIsRUFBRTtFQUM3Qjs7O0tBR0c7RUFDSDtJQUNFLDRCQUE0QixFQUFFO0VBQ2hDOztJQUVFLHlCQUF5QixFQUFFO0VBQzdCO0lBQ0UsMkJBQTJCLEVBQUU7RUFDL0I7OztJQUdFLFdBQVc7SUFDWCxVQUFVLEVBQUU7RUFDZDs7SUFFRSx3QkFBd0IsRUFBRSxFQUFFXCIsXCJmaWxlXCI6XCJMYXlvdXQuY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIkBjaGFyc2V0IFxcXCJVVEYtOFxcXCI7XFxuLyoqXFxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxcbiAqXFxuICogQ29weXJpZ2h0IMKpIDIwMTQtcHJlc2VudCBLcmlhc29mdCwgTExDLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxcbiAqXFxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXFxuICogTElDRU5TRS50eHQgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cXG4gKi9cXG4vKipcXG4gKiBSZWFjdCBTdGFydGVyIEtpdCAoaHR0cHM6Ly93d3cucmVhY3RzdGFydGVya2l0LmNvbS8pXFxuICpcXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXFxuICpcXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxcbiAqL1xcbjpyb290IHtcXG4gIC8qXFxuICAgKiBUeXBvZ3JhcGh5XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tZm9udC1mYW1pbHktYmFzZTogJ1NlZ29lIFVJJywgJ0hlbHZldGljYU5ldWUtTGlnaHQnLCBzYW5zLXNlcmlmO1xcblxcbiAgLypcXG4gICAqIExheW91dFxcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLW1heC1jb250ZW50LXdpZHRoOiAxMDAwcHg7XFxuXFxuICAvKlxcbiAgICogTWVkaWEgcXVlcmllcyBicmVha3BvaW50c1xcbiAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuICAtLXNjcmVlbi14cy1taW46IDQ4MHB4OyAgLyogRXh0cmEgc21hbGwgc2NyZWVuIC8gcGhvbmUgKi9cXG4gIC0tc2NyZWVuLXNtLW1pbjogNzY4cHg7ICAvKiBTbWFsbCBzY3JlZW4gLyB0YWJsZXQgKi9cXG4gIC0tc2NyZWVuLW1kLW1pbjogOTkycHg7ICAvKiBNZWRpdW0gc2NyZWVuIC8gZGVza3RvcCAqL1xcbiAgLS1zY3JlZW4tbGctbWluOiAxMjAwcHg7IC8qIExhcmdlIHNjcmVlbiAvIHdpZGUgZGVza3RvcCAqL1xcbn1cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUxpZ2h0LndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogMTAwOyB9XFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS5lb3Q/I2llZml4KSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDsgfVxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdCk7XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1Cb2xkLnR0ZikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDcwMDsgfVxcbi8qXFxuICogbm9ybWFsaXplLmNzcyBpcyBpbXBvcnRlZCBpbiBKUyBmaWxlLlxcbiAqIElmIHlvdSBpbXBvcnQgZXh0ZXJuYWwgQ1NTIGZpbGUgZnJvbSB5b3VyIGludGVybmFsIENTU1xcbiAqIHRoZW4gaXQgd2lsbCBiZSBpbmxpbmVkIGFuZCBwcm9jZXNzZWQgd2l0aCBDU1MgbW9kdWxlcy5cXG4gKi9cXG4vKlxcbiAqIEJhc2Ugc3R5bGVzXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5odG1sIHtcXG4gIGNvbG9yOiAjMjIyO1xcbiAgZm9udC13ZWlnaHQ6IDEwMDtcXG4gIGZvbnQtc2l6ZTogMWVtO1xcbiAgLyogfjE2cHg7ICovXFxuICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktYmFzZSk7XFxuICBsaW5lLWhlaWdodDogMS4zNzU7XFxuICAvKiB+MjJweCAqLyB9XFxuYm9keSB7XFxuICBtYXJnaW46IDA7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTsgfVxcbmEge1xcbiAgY29sb3I6ICMwMDc0YzI7IH1cXG4vKlxcbiAqIFJlbW92ZSB0ZXh0LXNoYWRvdyBpbiBzZWxlY3Rpb24gaGlnaGxpZ2h0OlxcbiAqIGh0dHBzOi8vdHdpdHRlci5jb20vbWlrZXRheWxyL3N0YXR1cy8xMjIyODgwNTMwMVxcbiAqXFxuICogVGhlc2Ugc2VsZWN0aW9uIHJ1bGUgc2V0cyBoYXZlIHRvIGJlIHNlcGFyYXRlLlxcbiAqIEN1c3RvbWl6ZSB0aGUgYmFja2dyb3VuZCBjb2xvciB0byBtYXRjaCB5b3VyIGRlc2lnbi5cXG4gKi9cXG46Oi1tb3otc2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbjo6c2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTsgfVxcbi8qXFxuICogQSBiZXR0ZXIgbG9va2luZyBkZWZhdWx0IGhvcml6b250YWwgcnVsZVxcbiAqL1xcbmhyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBib3JkZXI6IDA7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcXG4gIG1hcmdpbjogMWVtIDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBSZW1vdmUgdGhlIGdhcCBiZXR3ZWVuIGF1ZGlvLCBjYW52YXMsIGlmcmFtZXMsXFxuICogaW1hZ2VzLCB2aWRlb3MgYW5kIHRoZSBib3R0b20gb2YgdGhlaXIgY29udGFpbmVyczpcXG4gKiBodHRwczovL2dpdGh1Yi5jb20vaDVicC9odG1sNS1ib2lsZXJwbGF0ZS9pc3N1ZXMvNDQwXFxuICovXFxuYXVkaW8sXFxuY2FudmFzLFxcbmlmcmFtZSxcXG5pbWcsXFxuc3ZnLFxcbnZpZGVvIHtcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7IH1cXG4vKlxcbiAqIFJlbW92ZSBkZWZhdWx0IGZpZWxkc2V0IHN0eWxlcy5cXG4gKi9cXG5maWVsZHNldCB7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiAwOyB9XFxuLypcXG4gKiBBbGxvdyBvbmx5IHZlcnRpY2FsIHJlc2l6aW5nIG9mIHRleHRhcmVhcy5cXG4gKi9cXG50ZXh0YXJlYSB7XFxuICByZXNpemU6IHZlcnRpY2FsOyB9XFxuLypcXG4gKiBCcm93c2VyIHVwZ3JhZGUgcHJvbXB0XFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG46Z2xvYmFsKC5icm93c2VydXBncmFkZSkge1xcbiAgbWFyZ2luOiAwLjJlbSAwO1xcbiAgYmFja2dyb3VuZDogI2NjYztcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMC4yZW0gMDsgfVxcbi8qXFxuICogUHJpbnQgc3R5bGVzXFxuICogSW5saW5lZCB0byBhdm9pZCB0aGUgYWRkaXRpb25hbCBIVFRQIHJlcXVlc3Q6XFxuICogaHR0cDovL3d3dy5waHBpZWQuY29tL2RlbGF5LWxvYWRpbmcteW91ci1wcmludC1jc3MvXFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5AbWVkaWEgcHJpbnQge1xcbiAgKixcXG4gICo6OmJlZm9yZSxcXG4gICo6OmFmdGVyIHtcXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcXG4gICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcXG4gICAgLyogQmxhY2sgcHJpbnRzIGZhc3RlcjogaHR0cDovL3d3dy5zYW5iZWlqaS5jb20vYXJjaGl2ZXMvOTUzICovXFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgdGV4dC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDsgfVxcbiAgYSxcXG4gIGE6dmlzaXRlZCB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyB9XFxuICBhW2hyZWZdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cihocmVmKSBcXFwiKVxcXCI7IH1cXG4gIGFiYnJbdGl0bGVdOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgKFxcXCIgYXR0cih0aXRsZSkgXFxcIilcXFwiOyB9XFxuICAvKlxcbiAgICogRG9uJ3Qgc2hvdyBsaW5rcyB0aGF0IGFyZSBmcmFnbWVudCBpZGVudGlmaWVycyxcXG4gICAqIG9yIHVzZSB0aGUgYGphdmFzY3JpcHQ6YCBwc2V1ZG8gcHJvdG9jb2xcXG4gICAqL1xcbiAgYVtocmVmXj0nIyddOjphZnRlcixcXG4gIGFbaHJlZl49J2phdmFzY3JpcHQ6J106OmFmdGVyIHtcXG4gICAgY29udGVudDogJyc7IH1cXG4gIHByZSxcXG4gIGJsb2NrcXVvdGUge1xcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjOTk5O1xcbiAgICBwYWdlLWJyZWFrLWluc2lkZTogYXZvaWQ7IH1cXG4gIC8qXFxuICAgKiBQcmludGluZyBUYWJsZXM6XFxuICAgKiBodHRwOi8vY3NzLWRpc2N1c3MuaW5jdXRpby5jb20vd2lraS9QcmludGluZ19UYWJsZXNcXG4gICAqL1xcbiAgdGhlYWQge1xcbiAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7IH1cXG4gIHRyLFxcbiAgaW1nIHtcXG4gICAgcGFnZS1icmVhay1pbnNpZGU6IGF2b2lkOyB9XFxuICBpbWcge1xcbiAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDsgfVxcbiAgcCxcXG4gIGgyLFxcbiAgaDMge1xcbiAgICBvcnBoYW5zOiAzO1xcbiAgICB3aWRvd3M6IDM7IH1cXG4gIGgyLFxcbiAgaDMge1xcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhdm9pZDsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4ubWFyLTV7XFxuICBtYXJnaW46IDUlO1xcbn1cXG4ucGFkZC01e1xcbiAgcGFkZGluZzogNSU7XFxufVxcbi5tYXItbGVmdHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDtcXG59XFxuLm1hci10b3B7XFxuICBtYXJnaW4tdG9wOiAzdmg7XFxufVxcbi5tYXItYm90dG9te1xcbiAgbWFyZ2luLWJvdHRvbTogMTMlO1xcbiAgbWFyZ2luLWxlZnQ6IDIuNXZ3O1xcbiAgd2lkdGg6IDkzJSAhaW1wb3J0YW50O1xcbn1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTtcXG59XFxuLmljb25zLWNse1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoO1xcblxcbn1cXG4ubWFyLXRvcC0xMHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7XFxufVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gIH1cXG4gIC5tYXItbGVmdHtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7XFxuICB9XFxuICAuZHJvcGRvd24tY29udGVudHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDtcXG4gIH1cXG5cXG59XFxuYnV0dG9uOmZvY3VzIHtcXG4gIG91dGxpbmU6IDA7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXSB7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7IH1cXG5mb290ZXIge1xcbiAgaGVpZ2h0OiAxM3ZoO1xcbiAgYmFja2dyb3VuZDogIzFjNTk1NjsgfVxcbi5uYXYtbG9nbyB7XFxuICBmbG9hdDogcmlnaHQ7IH1cXG4ubmF2LWNsIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBoZWlnaHQ6IDEwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICB6LWluZGV4OiA5OTk5OyB9XFxuLmZsZXgtY2VudGVyIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyOyB9XFxuLmZsZXgtbWlkZGxlIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IH1cXG4ucHVycGxlLWZvbnQge1xcbiAgY29sb3I6ICM1YjU1OWI7IH1cXG4uYmx1ZS1mb250IHtcXG4gIGNvbG9yOiAjNDRkMWNhOyB9XFxuLmdyZXktZm9udCB7XFxuICBjb2xvcjogIzk2OTY5NjsgfVxcbi5saWdodC1ncmV5LWZvbnQge1xcbiAgY29sb3I6ICM5NDk0OTQ7IH1cXG4uYmxhY2stZm9udCB7XFxuICBjb2xvcjogYmxhY2s7IH1cXG4ud2hpdGUtZm9udCB7XFxuICBjb2xvcjogd2hpdGU7IH1cXG4ucGluay1mb250IHtcXG4gIGNvbG9yOiAjZjA1ZDZjOyB9XFxuLnBpbmstYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kOiAjZjA1ZDZjOyB9XFxuLnB1cnBsZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICM1YTU1OWI7IH1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLm1hcmdpbi1hdXRvIHtcXG4gIG1hcmdpbjogYXV0bzsgfVxcbi5uby1wYWRkaW5nIHtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uaWNvbi1jbCB7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dzsgfVxcbi5mbGV4LXJvdy1yZXYge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IHJldmVyc2U7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlOyB9XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7IH1cXG4uc2VhcmNoLXRoZW1lIHtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLWhlaWdodCB7XFxuICBoZWlnaHQ6IDIwMHB4OyB9XFxuLmJvcmRlci1ib3R0b20ge1xcbiAgbWFyZ2luLWJvdHRvbTogMnZoO1xcbiAgbWFyZ2luLWxlZnQ6IDF2dztcXG4gIG1hcmdpbi1yaWdodDogMXZ3O1xcbiAgbWFyZ2luLXRvcDogMnZoO1xcbiAgYm9yZGVyLWJvdHRvbTogdGhpbiBzb2xpZCBibGFjazsgfVxcbi5tYXItbGVmdCB7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7IH1cXG4ubWFyLXRvcCB7XFxuICBtYXJnaW4tdG9wOiAzdmg7IH1cXG4ubWFyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7IH1cXG4uaWNvbnMtY2wge1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXIge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3VzIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4ubWFyLXRvcC0xMCB7XFxuICBtYXJnaW4tdG9wOiAyJTsgfVxcbi5wYWRkaW5nLXJpZ2h0LXByaWNlIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDclOyB9XFxuLm1hcmdpbi0xIHtcXG4gIG1hcmdpbi1sZWZ0OiAzJTtcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG4uaW1nLWJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7IH1cXG4ubWFpbi1mb3JtLXNlYXJjaC1yZXN1bHQge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgbWFyZ2luLXRvcDogLTclO1xcbiAgbWFyZ2luLWJvdHRvbTogNiU7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW0gIWltcG9ydGFudDsgfVxcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDsgfVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH1cXG4gIC5tYXItbGVmdCB7XFxuICAgIG1hcmdpbjogMDsgfVxcbiAgLmljb25zLWNsIHtcXG4gICAgd2lkdGg6IDEzdnc7XFxuICAgIGhlaWdodDogN3ZoOyB9XFxuICAuZm9vdGVyLXRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTJweDsgfVxcbiAgLmljb25zLW1hciB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlOyB9XFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7IH1cXG4gIC5kcm9wZG93bi1jb250ZW50IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgaGVpZ2h0OiAzNTBweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIGZsb2F0OiByaWdodDsgfSB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy9tZWhybmF6L0RvY3VtZW50cy90dXJ0bGVSZWFjdC90dXJ0bGUtcmVhY3Qvc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7SUFDSSxxQkFBcUI7Q0FDeEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGNBQWM7Q0FDakI7QUFDRDtJQUNJLHlCQUF5QjtDQUM1QjtBQUNEO0lBQ0ksd0JBQXdCO0NBQzNCO0FBQ0Q7SUFDSSwwQkFBMEI7Q0FDN0I7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHlCQUF5QjtRQUNyQixzQkFBc0I7WUFDbEIsd0JBQXdCO0lBQ2hDLDBCQUEwQjtRQUN0Qix1QkFBdUI7WUFDbkIsb0JBQW9CO0NBQy9CO0FBQ0Q7SUFDSSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx5QkFBeUI7UUFDckIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtJQUNoQyw2QkFBNkI7SUFDN0IsOEJBQThCO1FBQzFCLDJCQUEyQjtZQUN2Qix1QkFBdUI7Q0FDbEM7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksaUJBQWlCO0NBQ3BCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QjtBQUNEO0lBQ0ksZ0RBQWdEO0lBQ2hELHdEQUF3RDtJQUN4RCxnREFBZ0Q7SUFDaEQsMkNBQTJDO0lBQzNDLHdDQUF3QztJQUN4Qyw2RUFBNkU7Q0FDaEY7QUFDRCxjQUFjLFVBQVUsQ0FBQztBQUN6QjtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsYUFBYTtFQUNiLCtDQUErQztVQUN2Qyx1Q0FBdUM7RUFDL0Msd0RBQXdEO0VBQ3hELGdEQUFnRDtFQUNoRCwyQ0FBMkM7RUFDM0Msd0NBQXdDO0VBQ3hDLDZFQUE2RTtDQUM5RTtBQUNEO0VBQ0UsV0FBVztFQUNYLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0VBQ2IsK0NBQStDO1VBQ3ZDLHVDQUF1QztFQUMvQyx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFO0NBQzlFO0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsdUJBQXVCO1VBQ25CLG9CQUFvQjtDQUM3QjtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QseUJBQXlCO01BQ3JCLHNCQUFzQjtVQUNsQix3QkFBd0I7Q0FDakM7QUFDRDtFQUNFLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsdUJBQXVCO0NBQ3hCO0FBQ0Q7RUFDRSx1QkFBdUI7O0NBRXhCO0FBQ0Q7RUFDRSx5QkFBeUI7Q0FDMUI7QUFDRDtFQUNFLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSx1QkFBdUI7Q0FDeEI7QUFDRDtFQUNFLDRCQUE0QjtDQUM3QjtBQUNEO0VBQ0UsMkJBQTJCO0NBQzVCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZTtDQUNoQjtBQUNEO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLGFBQWE7Q0FDZDtBQUNEO0VBQ0UsV0FBVztDQUNaO0FBQ0Q7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtDQUNoQjtBQUNEO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLCtCQUErQjtFQUMvQiwrQkFBK0I7TUFDM0IsZ0NBQWdDO1VBQzVCLDRCQUE0QjtDQUNyQztBQUNEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0FBQ0Q7RUFDRSxXQUFXO0NBQ1o7QUFDRDtFQUNFLFlBQVk7Q0FDYjtBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjtBQUNEO0VBQ0UsZ0JBQWdCO0NBQ2pCO0FBQ0Q7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQjtDQUN2QjtBQUNEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLG9CQUFvQjtFQUNwQixZQUFZO0NBQ2I7QUFDRDtFQUNFLFdBQVc7RUFDWCxZQUFZOztDQUViO0FBQ0Q7RUFDRSxlQUFlO0NBQ2hCO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Q7RUFDRSxnREFBZ0Q7VUFDeEMsd0NBQXdDO0NBQ2pEO0FBQ0Qsc0JBQXNCO0FBQ3RCO0VBQ0UsbUJBQW1CO0NBQ3BCO0FBQ0Q7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFVBQVU7RUFDVixXQUFXO0NBQ1o7QUFDRDtFQUNFLGVBQWU7Q0FDaEI7QUFDRDtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0NBQ2I7QUFDRCwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7RUFDRDtJQUNFLFVBQVU7R0FDWDs7RUFFRDtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1osYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixvQkFBb0I7SUFDcEIsWUFBWTtHQUNiO0VBQ0Q7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVO0dBQ1g7O0NBRUY7QUFDRDtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0RBQWtEO1VBQzFDLDBDQUEwQztFQUNsRCx3REFBd0Q7RUFDeEQsZ0RBQWdEO0VBQ2hELDJDQUEyQztFQUMzQyx3Q0FBd0M7RUFDeEMsNkVBQTZFLEVBQUU7QUFDakY7RUFDRSxXQUFXO0VBQ1gsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UsYUFBYTtFQUNiLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0UsYUFBYSxFQUFFO0FBQ2pCO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsK0JBQStCO0VBQy9CLDhCQUE4QjtNQUMxQix3QkFBd0I7VUFDcEIsb0JBQW9CO0VBQzVCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7RUFDcEQsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixjQUFjLEVBQUU7QUFDbEI7RUFDRSxxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsdUJBQXVCO1VBQ25CLG9CQUFvQixFQUFFO0FBQ2hDO0VBQ0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QseUJBQXlCO01BQ3JCLHNCQUFzQjtVQUNsQix3QkFBd0IsRUFBRTtBQUNwQztFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLGVBQWUsRUFBRTtBQUNuQjtFQUNFLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0Usb0JBQW9CLEVBQUU7QUFDeEI7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWUsRUFBRTtBQUNuQjtFQUNFLGFBQWEsRUFBRTtBQUNqQjtFQUNFLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBRTtBQUNuQjtFQUNFLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtFQUN0QiwrQkFBK0I7RUFDL0IsK0JBQStCO01BQzNCLGdDQUFnQztVQUM1Qiw0QkFBNEIsRUFBRTtBQUN4QztFQUNFLHNCQUFzQixFQUFFO0FBQzFCO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQixFQUFFO0FBQ3JCO0VBQ0UsV0FBVyxFQUFFO0FBQ2Y7RUFDRSxZQUFZLEVBQUU7QUFDaEI7RUFDRSxvREFBb0Q7VUFDNUMsNENBQTRDO0VBQ3BELGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLGFBQWEsRUFBRTtBQUNqQjtFQUNFLG9CQUFvQixFQUFFO0FBQ3hCO0VBQ0Usc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixxQ0FBcUM7RUFDckMsNEJBQTRCLEVBQUU7QUFDaEM7RUFDRSxjQUFjLEVBQUU7QUFDbEI7RUFDRSxtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZ0NBQWdDLEVBQUU7QUFDcEM7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CLEVBQUU7QUFDdkI7RUFDRSxnQkFBZ0IsRUFBRTtBQUNwQjtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUU7QUFDMUI7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLFlBQVksRUFBRTtBQUNoQjtFQUNFLFdBQVc7RUFDWCxZQUFZLEVBQUU7QUFDaEI7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxtREFBbUQ7VUFDM0MsMkNBQTJDLEVBQUU7QUFDdkQ7RUFDRSxlQUFlLEVBQUU7QUFDbkI7RUFDRSxrQkFBa0IsRUFBRTtBQUN0QjtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBRTtBQUNyQjtFQUNFLGlDQUFpQyxFQUFFO0FBQ3JDO0VBQ0UsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUU7QUFDdEI7RUFDRSxnQ0FBZ0MsRUFBRTtBQUNwQyxzQkFBc0I7QUFDdEI7RUFDRSxtQkFBbUIsRUFBRTtBQUN2QjtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQix3REFBd0Q7VUFDaEQsZ0RBQWdEO0VBQ3hELG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFdBQVcsRUFBRTtBQUNmO0VBQ0UsZUFBZSxFQUFFO0FBQ25CO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVksRUFBRTtBQUNoQiwwQ0FBMEM7QUFDMUM7RUFDRTtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUIsRUFBRTtFQUNyQjtJQUNFLFVBQVUsRUFBRTtFQUNkO0lBQ0UsWUFBWTtJQUNaLFlBQVksRUFBRTtFQUNoQjtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0IsRUFBRTtFQUNwQjtJQUNFLGVBQWUsRUFBRTtFQUNuQjtJQUNFLGtCQUFrQixFQUFFO0VBQ3RCO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0lBQ2Isb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixZQUFZLEVBQUU7RUFDaEI7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsd0RBQXdEO1lBQ2hELGdEQUFnRDtJQUN4RCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVLEVBQUU7RUFDZDtJQUNFLG9EQUFvRDtZQUM1Qyw0Q0FBNEM7SUFDcEQsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsYUFBYSxFQUFFLEVBQUVcIixcImZpbGVcIjpcIm1haW4uY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImJvZHkge1xcbiAgICBmb250LWZhbWlseTogU2hhYm5hbTtcXG59XFxuLndoaXRlIHtcXG4gICAgY29sb3I6IHdoaXRlO1xcbn1cXG4uYmxhY2sge1xcbiAgICBjb2xvcjogYmxhY2s7XFxufVxcbi5wdXJwbGUge1xcbiAgICBjb2xvcjogIzRjMGQ2Ylxcbn1cXG4ubGlnaHQtYmx1ZS1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNhZDJjYlxcbn1cXG4ud2hpdGUtYmFja2dyb3VuZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xcbn1cXG4uZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE4NTk1NjtcXG59XFxuLmZvbnQtYm9sZCB7XFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XFxufVxcbi5mb250LWxpZ2h0IHtcXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcXG59XFxuLmZvbnQtbWVkaXVtIHtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuLmNlbnRlci1jb250ZW50IHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5jZW50ZXItY29sdW1uIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XFxuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuLnRleHQtYWxpZ24tcmlnaHQge1xcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcXG59XFxuLnRleHQtYWxpZ24tbGVmdCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcbi50ZXh0LWFsaWduLWNlbnRlciB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuLm5vLWxpc3Qtc3R5bGUge1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4uY2xlYXItaW5wdXQge1xcbiAgICBvdXRsaW5lOiBub25lO1xcbiAgICBib3JkZXI6IG5vbmU7XFxufVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOXB4O1xcbn1cXG4uaGFzLXRyYW5zaXRpb24ge1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAgIC1vLXRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b246Zm9jdXMge291dGxpbmU6MDt9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl17XFxuICBvdXRsaW5lOm5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiBub25lO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4MHB4IHJnYmEoMCwwLDAsMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsMCwwLDAuMSk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC1ib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDAuM3MgZWFzZS1pbi1vdXQsIC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbn1cXG5idXR0b25bdHlwZT1cXFwic3VibWl0XFxcIl06Oi1tb3otZm9jdXMtaW5uZXIge1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLmZsZXgtY2VudGVye1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcbi5mbGV4LW1pZGRsZXtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcbi5wdXJwbGUtZm9udHtcXG4gIGNvbG9yOiByZ2IoOTEsODUsMTU1KTtcXG59XFxuLmJsdWUtZm9udHtcXG4gIGNvbG9yOiByZ2IoNjgsMjA5LDIwMik7XFxufVxcbi5ncmV5LWZvbnR7XFxuICBjb2xvcjpyZ2IoMTUwLDE1MCwxNTApO1xcblxcbn1cXG4ubGlnaHQtZ3JleS1mb250e1xcbiAgY29sb3I6ICByZ2IoMTQ4LDE0OCwxNDgpO1xcbn1cXG4uYmxhY2stZm9udHtcXG4gIGNvbG9yOiBibGFjaztcXG59XFxuLndoaXRlLWZvbnR7XFxuICBjb2xvcjogd2hpdGU7XFxufVxcbi5waW5rLWZvbnR7XFxuICBjb2xvcjogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucGluay1iYWNrZ3JvdW5ke1xcbiAgYmFja2dyb3VuZDogcmdiKDI0MCw5MywxMDgpO1xcbn1cXG4ucHVycGxlLWJhY2tncm91bmR7XFxuICBiYWNrZ3JvdW5kOiByZ2IoOTAsODUsMTU1KTtcXG59XFxuLnRleHQtYWxpZ24tY2VudGVye1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7XFxufVxcbi50ZXh0LWFsaWduLXJpZ2h0e1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICBkaXJlY3Rpb246IHJ0bDtcXG59XFxuLm1hcmdpbi1hdXRve1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4ubm8tcGFkZGluZ3tcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5pY29uLWNse1xcbiAgbWF4LWhlaWdodDogN3ZoO1xcbiAgbWF4LXdpZHRoOiA3dnc7XFxufVxcbi5mbGV4LXJvdy1yZXZ7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIC13ZWJraXQtYm94LW9yaWVudDogaG9yaXpvbnRhbDtcXG4gIC13ZWJraXQtYm94LWRpcmVjdGlvbjogcmV2ZXJzZTtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbi5mbGV4LXJvdy1yZXY6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG4ubWFyLTV7XFxuICBtYXJnaW46IDUlO1xcbn1cXG4ucGFkZC01e1xcbiAgcGFkZGluZzogNSU7XFxufVxcbi5tYXItbGVmdHtcXG4gIG1hcmdpbi1sZWZ0OiA1LjV2dztcXG4gIG1hcmdpbi1ib3R0b206IDV2aDtcXG59XFxuLm1hci10b3B7XFxuICBtYXJnaW4tdG9wOiAzdmg7XFxufVxcbi5tYXItYm90dG9te1xcbiAgbWFyZ2luLWJvdHRvbTogMTMlO1xcbiAgbWFyZ2luLWxlZnQ6IDIuNXZ3O1xcbiAgd2lkdGg6IDkzJSAhaW1wb3J0YW50O1xcbn1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuICBwYWRkaW5nOiAzJTtcXG59XFxuLmljb25zLWNse1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoO1xcblxcbn1cXG4ubWFyLXRvcC0xMHtcXG4gIG1hcmdpbi10b3A6IDIlO1xcbn1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmJ1dHRvblt0eXBlPVxcXCJzdWJtaXRcXFwiXTpob3ZlcntcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAxMHB4IDQwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsMCwwLDAuMik7XFxufVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuYnV0dG9uW3R5cGU9XFxcInN1Ym1pdFxcXCJdOmZvY3Vze1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwwLDAsMC4xNSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMHB4IDQwcHggcmdiYSgwLDAsMCwwLjE1KTtcXG59XFxuLypkcm9wZG93biBjc3MgY29kZXMqL1xcbi5kcm9wZG93biB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5kcm9wZG93bi1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgbWluLXdpZHRoOiAyMDBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgcGFkZGluZzogMTJweCAxNnB4O1xcbiAgei1pbmRleDogMTtcXG4gIGxlZnQ6IDJ2dztcXG4gIHRvcDogNy41dmg7XFxufVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuLnByaWNlLWhlYWRpbmcge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi8qbWVkaWEgcXVlcmllcyBmb3IgcmVzcG9uc2l2ZSB1dGlsaXRpZXMqL1xcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xcbiAgLm1haW4tZm9ybSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDM0JTtcXG4gICAgbWFyZ2luLXRvcDogLTEyJTtcXG4gIH1cXG4gIC5tYXItbGVmdHtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmJsdWUtYnV0dG9uIHtcXG4gICAgd2lkdGg6IDUwdnc7XFxuICAgIGhlaWdodDogNXZoO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGJhY2tncm91bmQ6IHJnYig2OCwyMDksMjAyKTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7XFxuICB9XFxuICAuZHJvcGRvd24tY29udGVudHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDtcXG4gIH1cXG5cXG59XFxuYnV0dG9uOmZvY3VzIHtcXG4gIG91dGxpbmU6IDA7IH1cXG5pbnB1dFt0eXBlPVxcXCJidXR0b25cXFwiXSB7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcGFkZGluZzogMDtcXG4gIGJvcmRlcjogbm9uZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggODBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDgwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IC13ZWJraXQtYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAwLjNzIGVhc2UtaW4tb3V0LCAtd2Via2l0LWJveC1zaGFkb3cgMC4zcyBlYXNlLWluLW91dDsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOjotbW96LWZvY3VzLWlubmVyIHtcXG4gIHBhZGRpbmc6IDA7XFxuICBib3JkZXI6IG5vbmU7IH1cXG5mb290ZXIge1xcbiAgaGVpZ2h0OiAxM3ZoO1xcbiAgYmFja2dyb3VuZDogIzFjNTk1NjsgfVxcbi5uYXYtbG9nbyB7XFxuICBmbG9hdDogcmlnaHQ7IH1cXG4ubmF2LWNsIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xcbiAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBoZWlnaHQ6IDEwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICB6LWluZGV4OiA5OTk5OyB9XFxuLmZsZXgtY2VudGVyIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyOyB9XFxuLmZsZXgtbWlkZGxlIHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IH1cXG4ucHVycGxlLWZvbnQge1xcbiAgY29sb3I6ICM1YjU1OWI7IH1cXG4uYmx1ZS1mb250IHtcXG4gIGNvbG9yOiAjNDRkMWNhOyB9XFxuLmdyZXktZm9udCB7XFxuICBjb2xvcjogIzk2OTY5NjsgfVxcbi5saWdodC1ncmV5LWZvbnQge1xcbiAgY29sb3I6ICM5NDk0OTQ7IH1cXG4uYmxhY2stZm9udCB7XFxuICBjb2xvcjogYmxhY2s7IH1cXG4ud2hpdGUtZm9udCB7XFxuICBjb2xvcjogd2hpdGU7IH1cXG4ucGluay1mb250IHtcXG4gIGNvbG9yOiAjZjA1ZDZjOyB9XFxuLnBpbmstYmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kOiAjZjA1ZDZjOyB9XFxuLnB1cnBsZS1iYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQ6ICM1YTU1OWI7IH1cXG4udGV4dC1hbGlnbi1jZW50ZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZGlyZWN0aW9uOiBydGw7IH1cXG4udGV4dC1hbGlnbi1yaWdodCB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIGRpcmVjdGlvbjogcnRsOyB9XFxuLm1hcmdpbi1hdXRvIHtcXG4gIG1hcmdpbjogYXV0bzsgfVxcbi5uby1wYWRkaW5nIHtcXG4gIHBhZGRpbmc6IDA7IH1cXG4uaWNvbi1jbCB7XFxuICBtYXgtaGVpZ2h0OiA3dmg7XFxuICBtYXgtd2lkdGg6IDd2dzsgfVxcbi5mbGV4LXJvdy1yZXYge1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XFxuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IHJldmVyc2U7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlOyB9XFxuLmZsZXgtcm93LXJldjpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7IH1cXG4uc2VhcmNoLXRoZW1lIHtcXG4gIGhlaWdodDogMTV2aDtcXG4gIG1hcmdpbi10b3A6IDEwdmg7IH1cXG4ubWFyLTUge1xcbiAgbWFyZ2luOiA1JTsgfVxcbi5wYWRkLTUge1xcbiAgcGFkZGluZzogNSU7IH1cXG4uaG91c2UtY2FyZCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgbWFyZ2luLXJpZ2h0OiA1LjV2dztcXG4gIGZsb2F0OiByaWdodDsgfVxcbi5ib3JkZXItcmFkaXVzIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IH1cXG4uaG91c2UtaW1nIHtcXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcXG4gIC8qIG1heC1oZWlnaHQ6IDEwMCU7ICovXFxuICBoZWlnaHQ6IDEwMCU7XFxuICAvKiBoZWlnaHQ6IDk3JTsgKi9cXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XFxuICAvKiBiYWNrZ3JvdW5kLW9yaWdpbjogY29udGVudC1ib3g7ICovXFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IH1cXG4uaW1nLWhlaWdodCB7XFxuICBoZWlnaHQ6IDIwMHB4OyB9XFxuLmJvcmRlci1ib3R0b20ge1xcbiAgbWFyZ2luLWJvdHRvbTogMnZoO1xcbiAgbWFyZ2luLWxlZnQ6IDF2dztcXG4gIG1hcmdpbi1yaWdodDogMXZ3O1xcbiAgbWFyZ2luLXRvcDogMnZoO1xcbiAgYm9yZGVyLWJvdHRvbTogdGhpbiBzb2xpZCBibGFjazsgfVxcbi5tYXItbGVmdCB7XFxuICBtYXJnaW4tbGVmdDogNS41dnc7XFxuICBtYXJnaW4tYm90dG9tOiA1dmg7IH1cXG4ubWFyLXRvcCB7XFxuICBtYXJnaW4tdG9wOiAzdmg7IH1cXG4ubWFyLWJvdHRvbSB7XFxuICBtYXJnaW4tYm90dG9tOiAxMyU7XFxuICBtYXJnaW4tbGVmdDogMi41dnc7XFxuICB3aWR0aDogOTMlICFpbXBvcnRhbnQ7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgd2lkdGg6IDEzdnc7XFxuICBoZWlnaHQ6IDV2aDtcXG4gIG1hcmdpbjogYXV0bztcXG4gIGJhY2tncm91bmQ6ICM0NGQxY2E7XFxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xcbiAgcGFkZGluZzogMyU7IH1cXG4uaWNvbnMtY2wge1xcbiAgd2lkdGg6IDN2dztcXG4gIGhlaWdodDogNXZoOyB9XFxuaW5wdXRbdHlwZT1cXFwiYnV0dG9uXFxcIl06aG92ZXIge1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDEwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4yKTsgfVxcbmlucHV0W3R5cGU9XFxcImJ1dHRvblxcXCJdOmZvY3VzIHtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwcHggNDBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDBweCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7IH1cXG4ubWFyLXRvcC0xMCB7XFxuICBtYXJnaW4tdG9wOiAyJTsgfVxcbi5wYWRkaW5nLXJpZ2h0LXByaWNlIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDclOyB9XFxuLm1hcmdpbi0xIHtcXG4gIG1hcmdpbi1sZWZ0OiAzJTtcXG4gIG1hcmdpbi1yaWdodDogMyU7IH1cXG4uaW1nLWJvcmRlci1yYWRpdXMge1xcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7IH1cXG4ubWFpbi1mb3JtLXNlYXJjaC1yZXN1bHQge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgbWFyZ2luLXRvcDogLTclO1xcbiAgbWFyZ2luLWJvdHRvbTogNiU7IH1cXG4uYmx1ZS1idXR0b24ge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW0gIWltcG9ydGFudDsgfVxcbi8qZHJvcGRvd24gY3NzIGNvZGVzKi9cXG4uZHJvcGRvd24ge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuLmRyb3Bkb3duLWNvbnRlbnQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XFxuICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgbGVmdDogMnZ3O1xcbiAgdG9wOiA3LjV2aDsgfVxcbi5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7XFxuICBkaXNwbGF5OiBibG9jazsgfVxcbi5wcmljZS1oZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHdpZHRoOiAxMDAlOyB9XFxuLyptZWRpYSBxdWVyaWVzIGZvciByZXNwb25zaXZlIHV0aWxpdGllcyovXFxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5tYWluLWZvcm0ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAzNCU7XFxuICAgIG1hcmdpbi10b3A6IC0xMiU7IH1cXG4gIC5tYXItbGVmdCB7XFxuICAgIG1hcmdpbjogMDsgfVxcbiAgLmljb25zLWNsIHtcXG4gICAgd2lkdGg6IDEzdnc7XFxuICAgIGhlaWdodDogN3ZoOyB9XFxuICAuZm9vdGVyLXRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTJweDsgfVxcbiAgLmljb25zLW1hciB7XFxuICAgIG1hcmdpbi10b3A6IDUlOyB9XFxuICAuaG91c2UtY2FyZCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDUlOyB9XFxuICAuYmx1ZS1idXR0b24ge1xcbiAgICB3aWR0aDogNTB2dztcXG4gICAgaGVpZ2h0OiA1dmg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgYmFja2dyb3VuZDogIzQ0ZDFjYTtcXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcXG4gICAgcGFkZGluZzogMyU7IH1cXG4gIC5kcm9wZG93bi1jb250ZW50IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmOWY5O1xcbiAgICBtaW4td2lkdGg6IDIwMHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBwYWRkaW5nOiAxMnB4IDE2cHg7XFxuICAgIHotaW5kZXg6IDE7XFxuICAgIGxlZnQ6IDEydnc7XFxuICAgIHRvcDogMTB2aDsgfVxcbiAgLmhvdXNlLWNhcmQge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcXG4gICAgaGVpZ2h0OiAzNTBweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNXZoO1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIGZsb2F0OiByaWdodDsgfSB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL3NyYy9jb21wb25lbnRzL1BhbmVsL21haW4uY3NzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9zcmMvY29tcG9uZW50cy9QYW5lbC9tYWluLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuXFxuOnJvb3Qge1xcbiAgLypcXG4gICAqIFR5cG9ncmFwaHlcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1mb250LWZhbWlseS1iYXNlOiAnU2Vnb2UgVUknLCAnSGVsdmV0aWNhTmV1ZS1MaWdodCcsIHNhbnMtc2VyaWY7XFxuXFxuICAvKlxcbiAgICogTGF5b3V0XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tbWF4LWNvbnRlbnQtd2lkdGg6IDEwMDBweDtcXG5cXG4gIC8qXFxuICAgKiBNZWRpYSBxdWVyaWVzIGJyZWFrcG9pbnRzXFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tc2NyZWVuLXhzLW1pbjogNDgwcHg7ICAvKiBFeHRyYSBzbWFsbCBzY3JlZW4gLyBwaG9uZSAqL1xcbiAgLS1zY3JlZW4tc20tbWluOiA3NjhweDsgIC8qIFNtYWxsIHNjcmVlbiAvIHRhYmxldCAqL1xcbiAgLS1zY3JlZW4tbWQtbWluOiA5OTJweDsgIC8qIE1lZGl1bSBzY3JlZW4gLyBkZXNrdG9wICovXFxuICAtLXNjcmVlbi1sZy1taW46IDEyMDBweDsgLyogTGFyZ2Ugc2NyZWVuIC8gd2lkZSBkZXNrdG9wICovXFxufVxcblxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7IH1cXG5cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0udHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNjAwOyB9XFxuXFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNzAwOyB9XFxuXFxuYm9keSxcXG5odG1sIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDEwMCU7IH1cXG5cXG5ib2R5IHtcXG4gIGZvbnQtZmFtaWx5OiBJUlNhbnMsIHNlcmlmOyB9XFxuXFxuaW5wdXQ6LXdlYmtpdC1hdXRvZmlsbCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAwIDEwMDBweCB3aGl0ZSBpbnNldDsgfVxcblxcbmlucHV0OmZvY3VzIHtcXG4gIG91dGxpbmU6IG5vbmU7IH1cXG5cXG4udG9hc3Qge1xcbiAgZGlyZWN0aW9uOiBydGw7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7IH1cXG5cXG4ubm90LWZvdW5kLWJveCB7XFxuICBtYXJnaW4tdG9wOiAxMDBweDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbiAgZm9udC13ZWlnaHQ6IDcwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjYTk0NDQyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICBwYWRkaW5nOiAyMHB4IDUwcHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL1VzZXJzL21laHJuYXovRG9jdW1lbnRzL3R1cnRsZVJlYWN0L3R1cnRsZS1yZWFjdC9zcmMvcm91dGVzL25vdC1mb3VuZC9Ob3RGb3VuZC5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBOzs7Ozs7O0dBT0c7O0FBRUg7RUFDRTs7Z0ZBRThFOztFQUU5RSxrRUFBa0U7O0VBRWxFOztnRkFFOEU7O0VBRTlFLDRCQUE0Qjs7RUFFNUI7O2dGQUU4RTs7RUFFOUUsdUJBQXVCLEVBQUUsZ0NBQWdDO0VBQ3pELHVCQUF1QixFQUFFLDJCQUEyQjtFQUNwRCx1QkFBdUIsRUFBRSw2QkFBNkI7RUFDdEQsd0JBQXdCLENBQUMsaUNBQWlDO0NBQzNEOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLG1DQUFtQztFQUNuQyx1S0FBdUs7RUFDdkssaUJBQWlCLEVBQUU7O0FBRXJCO0VBQ0UscUJBQXFCO0VBQ3JCLDZCQUE2QjtFQUM3QixxSkFBcUo7RUFDckosaUJBQWlCLEVBQUU7O0FBRXJCO0VBQ0UscUJBQXFCO0VBQ3JCLGtDQUFrQztFQUNsQyxvS0FBb0s7RUFDcEssaUJBQWlCLEVBQUU7O0FBRXJCOztFQUVFLGFBQWE7RUFDYixpQkFBaUIsRUFBRTs7QUFFckI7RUFDRSwyQkFBMkIsRUFBRTs7QUFFL0I7RUFDRSw2Q0FBNkMsRUFBRTs7QUFFakQ7RUFDRSxjQUFjLEVBQUU7O0FBRWxCO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQix3SEFBd0g7VUFDaEgsZ0hBQWdILEVBQUU7O0FBRTVIO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osd0hBQXdIO1VBQ2hILGdIQUFnSCxFQUFFXCIsXCJmaWxlXCI6XCJOb3RGb3VuZC5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi8qKlxcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcXG4gKlxcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cXG4gKlxcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXFxuICovXFxuXFxuOnJvb3Qge1xcbiAgLypcXG4gICAqIFR5cG9ncmFwaHlcXG4gICAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbiAgLS1mb250LWZhbWlseS1iYXNlOiAnU2Vnb2UgVUknLCAnSGVsdmV0aWNhTmV1ZS1MaWdodCcsIHNhbnMtc2VyaWY7XFxuXFxuICAvKlxcbiAgICogTGF5b3V0XFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tbWF4LWNvbnRlbnQtd2lkdGg6IDEwMDBweDtcXG5cXG4gIC8qXFxuICAgKiBNZWRpYSBxdWVyaWVzIGJyZWFrcG9pbnRzXFxuICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cXG5cXG4gIC0tc2NyZWVuLXhzLW1pbjogNDgwcHg7ICAvKiBFeHRyYSBzbWFsbCBzY3JlZW4gLyBwaG9uZSAqL1xcbiAgLS1zY3JlZW4tc20tbWluOiA3NjhweDsgIC8qIFNtYWxsIHNjcmVlbiAvIHRhYmxldCAqL1xcbiAgLS1zY3JlZW4tbWQtbWluOiA5OTJweDsgIC8qIE1lZGl1bSBzY3JlZW4gLyBkZXNrdG9wICovXFxuICAtLXNjcmVlbi1sZy1taW46IDEyMDBweDsgLyogTGFyZ2Ugc2NyZWVuIC8gd2lkZSBkZXNrdG9wICovXFxufVxcblxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFNoYWJuYW07XFxuICBzcmM6IHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC5lb3QpO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tTGlnaHQud29mZikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybCgvZm9udHMvU2hhYm5hbS1MaWdodC50dGYpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAxMDA7IH1cXG5cXG5AZm9udC1mYWNlIHtcXG4gIGZvbnQtZmFtaWx5OiBTaGFibmFtO1xcbiAgc3JjOiB1cmwoL2ZvbnRzL1NoYWJuYW0uZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLmVvdD8jaWVmaXgpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLndvZmYpIGZvcm1hdChcXFwid29mZlxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0udHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNjAwOyB9XFxuXFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogU2hhYm5hbTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90KTtcXG4gIHNyYzogdXJsKC9mb250cy9TaGFibmFtLUJvbGQuZW90PyNpZWZpeCkgZm9ybWF0KFxcXCJlbWJlZGRlZC1vcGVudHlwZVxcXCIpLCB1cmwoL2ZvbnRzL1NoYWJuYW0tQm9sZC53b2ZmKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKC9mb250cy9TaGFibmFtLUJvbGQudHRmKSBmb3JtYXQoXFxcInRydWV0eXBlXFxcIik7XFxuICBmb250LXdlaWdodDogNzAwOyB9XFxuXFxuYm9keSxcXG5odG1sIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDEwMCU7IH1cXG5cXG5ib2R5IHtcXG4gIGZvbnQtZmFtaWx5OiBJUlNhbnMsIHNlcmlmOyB9XFxuXFxuaW5wdXQ6LXdlYmtpdC1hdXRvZmlsbCB7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAwIDEwMDBweCB3aGl0ZSBpbnNldDsgfVxcblxcbmlucHV0OmZvY3VzIHtcXG4gIG91dGxpbmU6IG5vbmU7IH1cXG5cXG4udG9hc3Qge1xcbiAgZGlyZWN0aW9uOiBydGw7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7IH1cXG5cXG4ubm90LWZvdW5kLWJveCB7XFxuICBtYXJnaW4tdG9wOiAxMDBweDtcXG4gIGRpcmVjdGlvbjogcnRsO1xcbiAgZm9udC13ZWlnaHQ6IDcwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjYTk0NDQyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICBwYWRkaW5nOiAyMHB4IDUwcHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7IH1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL3JvdXRlcy9ub3QtZm91bmQvTm90Rm91bmQuc2Nzc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vc3JjL3JvdXRlcy9ub3QtZm91bmQvTm90Rm91bmQuc2Nzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL25vcm1hbGl6ZS5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9ub3JtYWxpemUuY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbm9ybWFsaXplLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUuY3NzL25vcm1hbGl6ZS5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS5jc3Mvbm9ybWFsaXplLmNzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiaW1wb3J0IHsgbm90aWZ5U3VjY2Vzcywgbm90aWZ5RXJyb3IsIGdldExvZ2luSW5mbyB9IGZyb20gJy4uL3V0aWxzJztcbmltcG9ydCB7XG4gIEdFVF9DVVJSX1VTRVJfUkVRVUVTVCxcbiAgR0VUX0NVUlJfVVNFUl9TVUNDRVNTLFxuICBHRVRfQ1VSUl9VU0VSX0ZBSUxVUkUsXG4gIElOQ1JFQVNFX0NSRURJVF9SRVFVRVNULFxuICBJTkNSRUFTRV9DUkVESVRfU1VDQ0VTUyxcbiAgSU5DUkVBU0VfQ1JFRElUX0ZBSUxVUkUsXG4gIFVTRVJfTE9HSU5fUkVRVUVTVCxcbiAgVVNFUl9MT0dJTl9TVUNDRVNTLFxuICBVU0VSX0xPR0lOX0ZBSUxVUkUsXG4gIFVTRVJfTE9HT1VUX1JFUVVFU1QsXG4gIFVTRVJfTE9HT1VUX1NVQ0NFU1MsXG4gIFVTRVJfTE9HT1VUX0ZBSUxVUkUsXG4gIFVTRVJfSVNfTk9UX0xPR0dFRF9JTixcbiAgR0VUX0lOSVRfRkFJTFVSRSxcbiAgR0VUX0lOSVRfUkVRVUVTVCxcbiAgR0VUX0lOSVRfU1VDQ0VTUyxcbiAgR0VUX0lOSVRfVVNFUl9GQUlMVVJFLFxuICBHRVRfSU5JVF9VU0VSX1JFUVVFU1QsXG4gIEdFVF9JTklUX1VTRVJfU1VDQ0VTUyxcbn0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uL2hpc3RvcnknO1xuaW1wb3J0IHtcbiAgc2VuZGdldEN1cnJlbnRVc2VyUmVxdWVzdCxcbiAgc2VuZGluY3JlYXNlQ3JlZGl0UmVxdWVzdCxcbiAgc2VuZEluaXRpYXRlUmVxdWVzdCxcbiAgc2VuZEluaXRpYXRlVXNlcnNSZXF1ZXN0LFxuICBzZW5kTG9naW5SZXF1ZXN0LFxufSBmcm9tICcuLi9hcGktaGFuZGxlcnMvYXV0aGVudGljYXRpb24nO1xuXG5leHBvcnQgZnVuY3Rpb24gbG9naW5SZXF1ZXN0KHVzZXJuYW1lLCBwYXNzd29yZCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfTE9HSU5fUkVRVUVTVCxcbiAgICBwYXlsb2FkOiB7XG4gICAgICB1c2VybmFtZSxcbiAgICAgIHBhc3N3b3JkLFxuICAgIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpblN1Y2Nlc3MoaW5mbykge1xuICBjb25zb2xlLmxvZyhpbmZvKTtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR0lOX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyBpbmZvIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpbkZhaWx1cmUoZXJyb3IpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR0lOX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnJvciB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9hZExvZ2luU3RhdHVzKCkge1xuICBjb25zdCBsb2dpbkluZm8gPSBnZXRMb2dpbkluZm8oKTtcbiAgaWYgKGxvZ2luSW5mbykge1xuICAgIHJldHVybiBsb2dpblN1Y2Nlc3MobG9naW5JbmZvKTtcbiAgfVxuICByZXR1cm4ge1xuICAgIHR5cGU6IFVTRVJfSVNfTk9UX0xPR0dFRF9JTixcbiAgfTtcbn1cblxuZnVuY3Rpb24gZ2V0Q2FwdGNoYVRva2VuKCkge1xuICBpZiAocHJvY2Vzcy5lbnYuQlJPV1NFUikge1xuICAgIGNvbnN0IGxvZ2luRm9ybSA9ICQoJyNsb2dpbkZvcm0nKS5nZXQoMCk7XG4gICAgaWYgKGxvZ2luRm9ybSAmJiB0eXBlb2YgbG9naW5Gb3JtWydnLXJlY2FwdGNoYS1yZXNwb25zZSddID09PSAnb2JqZWN0Jykge1xuICAgICAgcmV0dXJuIGxvZ2luRm9ybVsnZy1yZWNhcHRjaGEtcmVzcG9uc2UnXS52YWx1ZTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dpblVzZXIodXNlcm5hbWUsIHBhc3N3b3JkKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUpIHtcbiAgICBkaXNwYXRjaChsb2dpblJlcXVlc3QodXNlcm5hbWUsIHBhc3N3b3JkKSk7XG4gICAgdHJ5IHtcbiAgICAgIC8vIGNvbnN0IGNhcHRjaGFUb2tlbiA9IGdldENhcHRjaGFUb2tlbigpO1xuICAgICAgY29uc3QgcmVzID0gYXdhaXQgc2VuZExvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpO1xuICAgICAgZGlzcGF0Y2gobG9naW5TdWNjZXNzKHJlcykpO1xuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2xvZ2luSW5mbycsIEpTT04uc3RyaW5naWZ5KHJlcykpO1xuICAgICAgLy8gY29uc29sZS5sb2cobG9jYXRpb24ucGF0aG5hbWUpO1xuICAgICAgY29uc3QgeyBwYXRobmFtZSB9ID0gZ2V0U3RhdGUoKS5ob3VzZTtcbiAgICAgIGNvbnNvbGUubG9nKHBhdGhuYW1lKTtcbiAgICAgIGhpc3RvcnkucHVzaChwYXRobmFtZSk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBkaXNwYXRjaChsb2dpbkZhaWx1cmUoZXJyKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0UmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR09VVF9SRVFVRVNULFxuICAgIHBheWxvYWQ6IHt9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0U3VjY2VzcygpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBVU0VSX0xPR09VVF9TVUNDRVNTLFxuICAgIHBheWxvYWQ6IHtcbiAgICAgIG1lc3NhZ2U6ICfYrtix2YjYrCDZhdmI2YHZgtuM2Kog2KLZhduM2LInLFxuICAgIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dvdXRGYWlsdXJlKGVycm9yKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogVVNFUl9MT0dPVVRfRkFJTFVSRSxcbiAgICBwYXlsb2FkOiB7IGVycm9yIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2dvdXRVc2VyKCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChsb2dvdXRSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBkaXNwYXRjaChsb2dvdXRTdWNjZXNzKCkpO1xuICAgICAgbG9jYWxTdG9yYWdlLmNsZWFyKCk7XG4gICAgICBsb2NhdGlvbi5hc3NpZ24oJy9sb2dpbicpO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgZGlzcGF0Y2gobG9nb3V0RmFpbHVyZShlcnIpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVJlcXVlc3QoKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0lOSVRfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdGlhdGVGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlKCkge1xuICByZXR1cm4gYXN5bmMgZnVuY3Rpb24oZGlzcGF0Y2gpIHtcbiAgICBkaXNwYXRjaChpbml0aWF0ZVJlcXVlc3QoKSk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRJbml0aWF0ZVJlcXVlc3QoKTtcbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlU3VjY2VzcyhyZXMpKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBkaXNwYXRjaChpbml0aWF0ZUZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzUmVxdWVzdCgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9VU0VSX1JFUVVFU1QsXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9JTklUX1VTRVJfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0aWF0ZVVzZXJzRmFpbHVyZShlcnIpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBHRVRfSU5JVF9VU0VSX0ZBSUxVUkUsXG4gICAgcGF5bG9hZDogeyBlcnIgfSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRpYXRlVXNlcnMoKSB7XG4gIHJldHVybiBhc3luYyBmdW5jdGlvbihkaXNwYXRjaCkge1xuICAgIGRpc3BhdGNoKGluaXRpYXRlVXNlcnNSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QoKTtcbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlVXNlcnNTdWNjZXNzKHJlcykpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICBub3RpZnlFcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoKGluaXRpYXRlVXNlcnNGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9DVVJSX1VTRVJfUkVRVUVTVCxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyU3VjY2VzcyhyZXNwb25zZSkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEdFVF9DVVJSX1VTRVJfU1VDQ0VTUyxcbiAgICBwYXlsb2FkOiB7IHJlc3BvbnNlIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlckZhaWx1cmUoZXJyKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogR0VUX0NVUlJfVVNFUl9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50VXNlcih1c2VySWQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoKSB7XG4gICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KCkpO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZXMgPSBhd2FpdCBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KHVzZXJJZCk7XG4gICAgICBkaXNwYXRjaChnZXRDdXJyZW50VXNlclN1Y2Nlc3MocmVzKSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGlmIChlcnJvci5tZXNzYWdlKSB7XG4gICAgICAgIG5vdGlmeUVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXJGYWlsdXJlKGVycm9yKSk7XG4gICAgfVxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IElOQ1JFQVNFX0NSRURJVF9SRVFVRVNULFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXRTdWNjZXNzKHJlc3BvbnNlKSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogSU5DUkVBU0VfQ1JFRElUX1NVQ0NFU1MsXG4gICAgcGF5bG9hZDogeyByZXNwb25zZSB9LFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5jcmVhc2VDcmVkaXRGYWlsdXJlKGVycikge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IElOQ1JFQVNFX0NSRURJVF9GQUlMVVJFLFxuICAgIHBheWxvYWQ6IHsgZXJyIH0sXG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbmNyZWFzZUNyZWRpdChhbW91bnQpIHtcbiAgcmV0dXJuIGFzeW5jIGZ1bmN0aW9uKGRpc3BhdGNoLCBnZXRTdGF0ZSkge1xuICAgIGRpc3BhdGNoKGluY3JlYXNlQ3JlZGl0UmVxdWVzdCgpKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBjdXJyVXNlciB9ID0gZ2V0U3RhdGUoKS5hdXRoZW50aWNhdGlvbjtcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlbmRpbmNyZWFzZUNyZWRpdFJlcXVlc3QoYW1vdW50LCBjdXJyVXNlci51c2VySWQpO1xuICAgICAgZGlzcGF0Y2goaW5jcmVhc2VDcmVkaXRTdWNjZXNzKHJlcykpO1xuICAgICAgZGlzcGF0Y2goZ2V0Q3VycmVudFVzZXIoY3VyclVzZXIudXNlcklkKSk7XG4gICAgICBub3RpZnlTdWNjZXNzKCfYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixINio2Kcg2YXZiNmB2YLbjNiqINin2YbYrNin2YUg2LTYry4nKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgbm90aWZ5RXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBub3RpZnlFcnJvcign2K7Yt9inINmF2KzYr9ivINiq2YTYp9i0INqp2YbbjNivJyk7XG4gICAgICBkaXNwYXRjaChpbmNyZWFzZUNyZWRpdEZhaWx1cmUoZXJyb3IpKTtcbiAgICB9XG4gIH07XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2FjdGlvbnMvYXV0aGVudGljYXRpb24uanMiLCJpbXBvcnQgeyBmZXRjaEFwaSwgZ2V0R2V0Q29uZmlnLCBnZXRQb3N0Q29uZmlnIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHtcbiAgZ2V0Q3VycmVudFVzZXJVcmwsXG4gIGdldEluaXRpYXRlVXJsLFxuICBsb2dpbkFwaVVybCxcbiAgaW5jcmVhc2VDcmVkaXRVcmwsXG59IGZyb20gJy4uL2NvbmZpZy9wYXRocyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kaW5jcmVhc2VDcmVkaXRSZXF1ZXN0KGFtb3VudCwgaWQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGluY3JlYXNlQ3JlZGl0VXJsKGFtb3VudCwgaWQpLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kZ2V0Q3VycmVudFVzZXJSZXF1ZXN0KHVzZXJJZCkge1xuICByZXR1cm4gZmV0Y2hBcGkoZ2V0Q3VycmVudFVzZXJVcmwodXNlcklkKSwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZEluaXRpYXRlUmVxdWVzdCgpIHtcbiAgcmV0dXJuIGZldGNoQXBpKGdldEluaXRpYXRlVXJsLCBnZXRHZXRDb25maWcoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZW5kSW5pdGlhdGVVc2Vyc1JlcXVlc3QoKSB7XG4gIHJldHVybiBmZXRjaEFwaShsb2dpbkFwaVVybCwgZ2V0R2V0Q29uZmlnKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2VuZExvZ2luUmVxdWVzdCh1c2VybmFtZSwgcGFzc3dvcmQpIHtcbiAgcmV0dXJuIGZldGNoQXBpKFxuICAgIGxvZ2luQXBpVXJsLFxuICAgIGdldFBvc3RDb25maWcoe1xuICAgICAgdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICB9KSxcbiAgKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvYXBpLWhhbmRsZXJzL2F1dGhlbnRpY2F0aW9uLmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIi9hc3NldHMvc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZz8yZmMyZDVjMVwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvRm9vdGVyLzIwMHB4LUluc3RhZ3JhbV9sb2dvXzIwMTYuc3ZnLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtSW5zdGFncmFtX2xvZ29fMjAxNi5zdmcucG5nXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nPzZmYTI4MDRjXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvMjAwcHgtVGVsZWdyYW1fbG9nby5zdmcucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci8yMDBweC1UZWxlZ3JhbV9sb2dvLnN2Zy5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L25vLWRhbmdlciAqL1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgdHdpdHRlckljb24gZnJvbSAnLi9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmcnO1xuaW1wb3J0IGluc3RhZ3JhbUljb24gZnJvbSAnLi8yMDBweC1JbnN0YWdyYW1fbG9nb18yMDE2LnN2Zy5wbmcnO1xuaW1wb3J0IHRlbGVncmFtSWNvbiBmcm9tICcuLzIwMHB4LVRlbGVncmFtX2xvZ28uc3ZnLnBuZyc7XG5pbXBvcnQgcyBmcm9tICcuL21haW4uY3NzJztcblxuY2xhc3MgRm9vdGVyIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8Zm9vdGVyIGNsYXNzTmFtZT1cInNpdGUtZm9vdGVyIGNlbnRlci1jb2x1bW4gZGFyay1ncmVlbi1iYWNrZ3JvdW5kIHdoaXRlXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTIgY29sLXNtLTQgdGV4dC1hbGlnbi1sZWZ0XCI+XG4gICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwic29jaWFsLWljb25zLWxpc3Qgbm8tbGlzdC1zdHlsZVwiPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXtpbnN0YWdyYW1JY29ufVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic29jaWFsLW5ldHdvcmstaWNvblwiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJpbnN0YWdyYW0taWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17dHdpdHRlckljb259XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzb2NpYWwtbmV0d29yay1pY29uXCJcbiAgICAgICAgICAgICAgICAgIGFsdD1cInR3aXR0ZXItaWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17dGVsZWdyYW1JY29ufVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic29jaWFsLW5ldHdvcmstaWNvblwiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJ0ZWxlZ3JhbS1pY29uXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMiBjb2wtc20tOCB0ZXh0LWFsaWduLXJpZ2h0XCI+XG4gICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAg2KrZhdin2YXbjCDYrdmC2YjZgiDYp9uM2YYg2YjYqOKAjNiz2KfbjNiqINmF2KrYudmE2YIg2KjZhyDZhdmH2LHZhtin2LIg2KvYp9io2Kog2Ygg2YfZiNmF2YYg2LTYsduM2YEg2LLYp9iv2YdcbiAgICAgICAgICAgICAg2YXbjOKAjNio2KfYtNivLlxuICAgICAgICAgICAgPC9wPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZm9vdGVyPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShGb290ZXIpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuanMiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL2Fzc2V0cy9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nPzI0OGJmYThkXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvVHdpdHRlcl9iaXJkX2xvZ29fMjAxMi5zdmcucG5nXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Ud2l0dGVyX2JpcmRfbG9nb18yMDEyLnN2Zy5wbmdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vbWFpbi5jc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9tYWluLmNzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9Gb290ZXIvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9MYXlvdXQuY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzIS4vTGF5b3V0LmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgaWQgPSAuL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUiLCIvKipcbiAqIFJlYWN0IFN0YXJ0ZXIgS2l0IChodHRwczovL3d3dy5yZWFjdHN0YXJ0ZXJraXQuY29tLylcbiAqXG4gKiBDb3B5cmlnaHQgwqkgMjAxNC1wcmVzZW50IEtyaWFzb2Z0LCBMTEMuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFLnR4dCBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IG5vcm1hbGl6ZUNzcyBmcm9tICdub3JtYWxpemUuY3NzJztcbmltcG9ydCBzIGZyb20gJy4vTGF5b3V0LmNzcyc7XG5pbXBvcnQgRm9vdGVyIGZyb20gJy4uL0Zvb3Rlcic7XG5pbXBvcnQgUGFuZWwgZnJvbSAnLi4vUGFuZWwnO1xuXG5jbGFzcyBMYXlvdXQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMubm9kZS5pc1JlcXVpcmVkLFxuICAgIG1haW5QYW5lbDogUHJvcFR5cGVzLmJvb2wsXG4gIH07XG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgbWFpblBhbmVsOiBmYWxzZSxcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC0xMiBjb2wteHMtMTJcIiBzdHlsZT17eyBwYWRkaW5nOiAnMCcgfX0+XG4gICAgICAgIDxQYW5lbCBtYWluUGFuZWw9e3RoaXMucHJvcHMubWFpblBhbmVsfSAvPlxuICAgICAgICB7dGhpcy5wcm9wcy5jaGlsZHJlbn1cbiAgICAgICAgPEZvb3RlciAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKG5vcm1hbGl6ZUNzcywgcykoTGF5b3V0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmpzIiwiLyogZXNsaW50LWRpc2FibGUgcmVhY3Qvbm8tZGFuZ2VyICovXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgKiBhcyBhdXRoQWN0aW9ucyBmcm9tICcuLi8uLi9hY3Rpb25zL2F1dGhlbnRpY2F0aW9uJztcbmltcG9ydCBzIGZyb20gJy4vbWFpbi5jc3MnO1xuaW1wb3J0IGxvZ29JbWcgZnJvbSAnLi9sb2dvLnBuZyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi8uLi9oaXN0b3J5JztcblxuY2xhc3MgUGFuZWwgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGN1cnJVc2VyOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgYXV0aEFjdGlvbnM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBtYWluUGFuZWw6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gICAgaXNMb2dnZWRJbjogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5jdXJyVXNlcik7XG4gICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5pc0xvZ2dlZEluKTtcbiAgICBpZiAodGhpcy5wcm9wcy5tYWluUGFuZWwpIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPVwiaGlkZGVuLXhzXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGhlYWRlci1tYWluXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleC1jZW50ZXIgY29sLW1kLTIgICBuYXYtdXNlciBjb2wteHMtMTIgZHJvcGRvd24gXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IGZsZXgtY2VudGVyIGhlYWRlci1tYWluLWJvYXJkZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtOSBcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCB3aGl0ZS1mb250IHBlcnNpYW4gXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICDZhtin2K3bjNmHINqp2KfYsdio2LHbjFxuICAgICAgICAgICAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkcm9wZG93bi1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlciAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJwZXJzaWFuIHRleHQtYWxpZ24tcmlnaHQgYmxhY2stZm9udCBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlci51c2VybmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNiBwdWxsLWxlZnQgcGVyc2lhbiBsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJsaWdodC1ncmV5LWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINiq2YjZhdin2YZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLmJhbGFuY2V9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNiBsaWdodC1ncmV5LWZvbnQgcHVsbC1yaWdodCAgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDYp9i52KrYqNin2LFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG1hci10b3BcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCLYp9mB2LLYp9uM2LQg2KfYudiq2KjYp9ixXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goJy9pbmNyZWFzZUNyZWRpdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItiu2LHZiNisXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmF1dGhBY3Rpb25zLmxvZ291dFVzZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgd2hpdGUtZm9udCAgYmx1ZS1idXR0b24gcGVyc2lhbiB0ZXh0LWFsaWduLWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgeyF0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2YjYsdmI2K9cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvbG9naW4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB3aGl0ZS1mb250ICBibHVlLWJ1dHRvbiBwZXJzaWFuIHRleHQtYWxpZ24tY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuby1wYWRkaW5nICBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXNtaWxlLW8gZmEtMnggd2hpdGUtZm9udFwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvaGVhZGVyPlxuICAgICAgKTtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgIDxoZWFkZXI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbmF2LWNsXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgtY2VudGVyIGNvbC1tZC0yICAgbmF2LXVzZXIgY29sLXhzLTEyIGRyb3Bkb3duXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgZmxleC1jZW50ZXIgXCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy05IFwiPlxuICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwidGV4dC1hbGlnbi1yaWdodCBwZXJzaWFuIHB1cnBsZS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAg2YbYp9it24zZhyDaqdin2LHYqNix24xcbiAgICAgICAgICAgICAgICAgICAgPC9oNT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkcm9wZG93bi1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cImJsYWNrLWZvbnQgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IGJsdWUtZm9udFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jdXJyVXNlci51c2VybmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiIGxpZ2h0LWdyZXktZm9udCBjb2wteHMtNiBwdWxsLWxlZnQgYmx1ZS1mb250IHBlcnNpYW5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibGlnaHQtZ3JleS1mb250XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KrZiNmF2KfZhlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmN1cnJVc2VyLmJhbGFuY2V9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsaWdodC1ncmV5LWZvbnQgY29sLXhzLTYgcHVsbC1yaWdodCBibHVlLWZvbnQgcGVyc2lhbiB0ZXh0LWFsaWduLXJpZ2h0IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg2KfYudiq2KjYp9ixXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBtYXItdG9wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwi2KfZgdiy2KfbjNi0INin2LnYqtio2KfYsVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvaW5jcmVhc2VDcmVkaXQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItiu2LHZiNisXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuYXV0aEFjdGlvbnMubG9nb3V0VXNlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgeyF0aGlzLnByb3BzLmN1cnJVc2VyICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbWFyLXRvcFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cItmI2LHZiNivXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoKCcvbG9naW4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHdoaXRlLWZvbnQgIGJsdWUtYnV0dG9uIHBlcnNpYW4gdGV4dC1hbGlnbi1jZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuby1wYWRkaW5nICBjb2wteHMtM1wiPlxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1zbWlsZS1vIGZhLTJ4IHB1cnBsZS1mb250XCIgLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4LWNlbnRlciBjb2wtbWQtNCBjb2wtbWQtb2Zmc2V0LTYgbmF2LWxvZ28gY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgIFwiPlxuICAgICAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZmxleC1yb3ctcmV2XCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaCgnLycpO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vLXBhZGRpbmcgdGV4dC1hbGlnbi1jZW50ZXIgY29sLXhzLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cImljb24tY2xcIiBzcmM9e2xvZ29JbWd9IGFsdD1cImxvZ29cIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wteHMtOVwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJwZXJzaWFuLWJvbGQgdGV4dC1hbGlnbi1yaWdodCAgcGVyc2lhbiBibHVlLWZvbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgINiu2KfZhtmHINio2Ycg2K/ZiNi0XG4gICAgICAgICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9oZWFkZXI+XG4gICAgKTtcbiAgfVxufVxuZnVuY3Rpb24gbWFwRGlzcGF0Y2hUb1Byb3BzKGRpc3BhdGNoKSB7XG4gIHJldHVybiB7XG4gICAgYXV0aEFjdGlvbnM6IGJpbmRBY3Rpb25DcmVhdG9ycyhhdXRoQWN0aW9ucywgZGlzcGF0Y2gpLFxuICB9O1xufVxuXG5mdW5jdGlvbiBtYXBTdGF0ZVRvUHJvcHMoc3RhdGUpIHtcbiAgY29uc3QgeyBhdXRoZW50aWNhdGlvbiB9ID0gc3RhdGU7XG4gIGNvbnN0IHsgY3VyclVzZXIsaXNMb2dnZWRJbiB9ID0gYXV0aGVudGljYXRpb247XG4gIHJldHVybiB7XG4gICAgY3VyclVzZXIsXG4gICAgaXNMb2dnZWRJbixcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMocykoUGFuZWwpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29tcG9uZW50cy9QYW5lbC9QYW5lbC5qcyIsIm1vZHVsZS5leHBvcnRzID0gXCIvYXNzZXRzL3NyYy9jb21wb25lbnRzL1BhbmVsL2xvZ28ucG5nPzJhZjczZDhhXCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9sb2dvLnBuZ1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvY29tcG9uZW50cy9QYW5lbC9sb2dvLnBuZ1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9tYWluLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL21haW4uY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL2NvbXBvbmVudHMvUGFuZWwvbWFpbi5jc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSIsIi8vIG5vaW5zcGVjdGlvbiBKU0Fubm90YXRvclxuY29uc3QgbGV2ZWxzID0gW1xuICB7XG4gICAgTUFYX1dJRFRIOiA2MDAsXG4gICAgTUFYX0hFSUdIVDogNjAwLFxuICAgIERPQ1VNRU5UX1NJWkU6IDMwMDAwMCxcbiAgfSxcbiAge1xuICAgIE1BWF9XSURUSDogODAwLFxuICAgIE1BWF9IRUlHSFQ6IDgwMCxcbiAgICBET0NVTUVOVF9TSVpFOiA1MDAwMDAsXG4gIH0sXG5dO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGdldExldmVsKGxldmVsKSB7XG4gICAgcmV0dXJuIGxldmVsc1tsZXZlbCAtIDFdO1xuICB9LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzcmMvY29uZmlnL2NvbXByZXNzaW9uQ29uZmlnLmpzIiwiLyogZXNsaW50LWRpc2FibGUgcHJldHRpZXIvcHJldHRpZXIgKi9cbmltcG9ydCB7IGFwaSB9IGZyb20gJy4vdXJscyc7XG5cbmV4cG9ydCBjb25zdCBsb2dpbkFwaVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2xvZ2luYDtcbmV4cG9ydCBjb25zdCBsb2dvdXRBcGlVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9sb2dpbmA7XG5leHBvcnQgY29uc3QgZ2V0U2VhcmNoSG91c2VzVXJsID0gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvU2VhcmNoU2VydmxldGA7XG5leHBvcnQgY29uc3QgZ2V0Q3JlYXRlSG91c2VVcmwgPSBgaHR0cDovL2xvY2FsaG9zdDo4MDgwL1R1cnRsZS9hcGkvSG91c2VTZXJ2bGV0YDtcbmV4cG9ydCBjb25zdCBnZXRJbml0aWF0ZVVybCA9IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2hvbWVgO1xuZXhwb3J0IGNvbnN0IGdldEN1cnJlbnRVc2VyVXJsID0gKHVzZXJJZCkgPT4gYGh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9UdXJ0bGUvYXBpL0luZGl2aWR1YWxTZXJ2bGV0P3VzZXJJZD0ke3VzZXJJZH1gO1xuZXhwb3J0IGNvbnN0IGdldEhvdXNlRGV0YWlsVXJsID0gKGhvdXNlSWQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9HZXRTaW5nbGVIb3VzZT9pZD0ke2hvdXNlSWR9YDtcbmV4cG9ydCBjb25zdCBnZXRIb3VzZVBob25lVXJsID0gKGhvdXNlSWQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9QYXltZW50U2VydmxldD9pZD0ke2hvdXNlSWR9YDtcbmV4cG9ydCBjb25zdCBpbmNyZWFzZUNyZWRpdFVybCA9IChhbW91bnQpID0+IGBodHRwOi8vbG9jYWxob3N0OjgwODAvVHVydGxlL2FwaS9DcmVkaXRTZXJ2bGV0P3ZhbHVlPSR7YW1vdW50fWA7XG5cbmV4cG9ydCBjb25zdCBzZWFyY2hEcml2ZXJVcmwgPSBwaG9uZU51bWJlciA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvcGhvbmVOdW1iZXIvJHtwaG9uZU51bWJlcn1gO1xuZXhwb3J0IGNvbnN0IGdldERyaXZlckFjcXVpc2l0aW9uSW5mb1VybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgdXBkYXRlRHJpdmVyQWNxdWlzaXRpb25JbmZvVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCB1cGRhdGVEcml2ZXJEb2NzVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2RvY3VtZW50cy8ke2RyaXZlcklkfWA7XG5leHBvcnQgY29uc3QgYXBwcm92ZURyaXZlclVybCA9IGRyaXZlcklkID0+IGAke2FwaS5jbGllbnRVcmx9L3YyL2FjcXVpc2l0aW9uL2RyaXZlci9hcHByb3ZlLyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCByZWplY3REcml2ZXJVcmwgPSBkcml2ZXJJZCA9PiBgJHthcGkuY2xpZW50VXJsfS92Mi9hY3F1aXNpdGlvbi9kcml2ZXIvcmVqZWN0LyR7ZHJpdmVySWR9YDtcbmV4cG9ydCBjb25zdCBnZXREcml2ZXJEb2N1bWVudFVybCA9IGRvY3VtZW50SWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2RvY3VtZW50LyR7ZG9jdW1lbnRJZH1gO1xuZXhwb3J0IGNvbnN0IGdldERyaXZlckRvY3VtZW50SWRzVXJsID0gZHJpdmVySWQgPT4gYCR7YXBpLmNsaWVudFVybH0vdjIvYWNxdWlzaXRpb24vZHJpdmVyL2RvY3VtZW50cy8ke2RyaXZlcklkfWA7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2NvbmZpZy9wYXRocy5qcyIsIi8qKlxuICogUmVhY3QgU3RhcnRlciBLaXQgKGh0dHBzOi8vd3d3LnJlYWN0c3RhcnRlcmtpdC5jb20vKVxuICpcbiAqIENvcHlyaWdodCDCqSAyMDE0LXByZXNlbnQgS3JpYXNvZnQsIExMQy4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UudHh0IGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKi9cblxuaW1wb3J0IGNyZWF0ZUJyb3dzZXJIaXN0b3J5IGZyb20gJ2hpc3RvcnkvY3JlYXRlQnJvd3Nlckhpc3RvcnknO1xuXG4vLyBOYXZpZ2F0aW9uQmFyIG1hbmFnZXIsIGUuZy4gaGlzdG9yeS5wdXNoKCcvaG9tZScpXG4vLyBodHRwczovL2dpdGh1Yi5jb20vbWphY2tzb24vaGlzdG9yeVxuZXhwb3J0IGRlZmF1bHQgcHJvY2Vzcy5lbnYuQlJPV1NFUiAmJiBjcmVhdGVCcm93c2VySGlzdG9yeSgpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9oaXN0b3J5LmpzIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBzIGZyb20gJy4vTm90Rm91bmQuc2Nzcyc7XG5cbmNsYXNzIE5vdEZvdW5kIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5vdC1mb3VuZC1ib3hcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJub3QtZm91bmQtdGV4dFwiPlxuICAgICAgICAgIDxwPiDYtdmB2K3Zh+KAjNuMINmF2YjYsdivINmG2LjYsSDYtNmF2Kcg24zYp9mB2Kog2YbYtNivLjwvcD5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoTm90Rm91bmQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9yb3V0ZXMvbm90LWZvdW5kL05vdEZvdW5kLmpzIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9Ob3RGb3VuZC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcyEuL05vdEZvdW5kLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanMhLi9Ob3RGb3VuZC5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gIFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL3JvdXRlcy9ub3QtZm91bmQvTm90Rm91bmQuc2Nzc1xuLy8gbW9kdWxlIGlkID0gLi9zcmMvcm91dGVzL25vdC1mb3VuZC9Ob3RGb3VuZC5zY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gNSIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvTGF5b3V0JztcbmltcG9ydCBOb3RGb3VuZCBmcm9tICcuL05vdEZvdW5kJztcblxuY29uc3QgdGl0bGUgPSAn2LXZgdit2YfigIzbjCDZhdmI2LHYryDZhti42LEg24zYp9mB2Kog2YbYtNivJztcblxuZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIGNodW5rczogWydub3QtZm91bmQnXSxcbiAgICB0aXRsZSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxOb3RGb3VuZCB0aXRsZT17dGl0bGV9IC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICAgIHN0YXR1czogNDA0LFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3JvdXRlcy9ub3QtZm91bmQvaW5kZXguanMiLCJpbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IEVYSUYgZnJvbSAnZXhpZi1qcyc7XG5pbXBvcnQgaGlzdG9yeSBmcm9tICcuLi9oaXN0b3J5JztcbmltcG9ydCBjb21wcmVzc2lvbkNvbmZpZyBmcm9tICcuLi9jb25maWcvY29tcHJlc3Npb25Db25maWcnO1xuaW1wb3J0IHsgbG9hZExvZ2luU3RhdHVzIH0gZnJvbSAnLi4vYWN0aW9ucy9hdXRoZW50aWNhdGlvbic7XG5cbmNvbnN0IGNvbXByZXNzaW9uRmlyc3RMZXZlbCA9IGNvbXByZXNzaW9uQ29uZmlnLmdldExldmVsKDEpO1xuY29uc3QgY29tcHJlc3Npb25TZWNvbmRMZXZlbCA9IGNvbXByZXNzaW9uQ29uZmlnLmdldExldmVsKDIpO1xuY29uc3QgZG9jdW1lbnRzQ29tcHJlc3Npb25MZXZlbCA9IHtcbiAgcHJvZmlsZVBpY3R1cmU6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgZHJpdmVyTGljZW5jZTogY29tcHJlc3Npb25GaXJzdExldmVsLFxuICBjYXJJZENhcmQ6IGNvbXByZXNzaW9uRmlyc3RMZXZlbCxcbiAgaW5zdXJhbmNlOiBjb21wcmVzc2lvblNlY29uZExldmVsLFxufTtcblxuZnVuY3Rpb24gZ2V0Q29tcHJlc3Npb25MZXZlbChkb2N1bWVudFR5cGUpIHtcbiAgcmV0dXJuIGRvY3VtZW50c0NvbXByZXNzaW9uTGV2ZWxbZG9jdW1lbnRUeXBlXSB8fCBjb21wcmVzc2lvbkZpcnN0TGV2ZWw7XG59XG5cbmZ1bmN0aW9uIGRhdGFVUkx0b0Jsb2IoZGF0YVVSTCwgZmlsZVR5cGUpIHtcbiAgY29uc3QgYmluYXJ5ID0gYXRvYihkYXRhVVJMLnNwbGl0KCcsJylbMV0pO1xuICBjb25zdCBhcnJheSA9IFtdO1xuICBjb25zdCBsZW5ndGggPSBiaW5hcnkubGVuZ3RoO1xuICBsZXQgaTtcbiAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgYXJyYXkucHVzaChiaW5hcnkuY2hhckNvZGVBdChpKSk7XG4gIH1cbiAgcmV0dXJuIG5ldyBCbG9iKFtuZXcgVWludDhBcnJheShhcnJheSldLCB7IHR5cGU6IGZpbGVUeXBlIH0pO1xufVxuXG5mdW5jdGlvbiBnZXRTY2FsZWRJbWFnZU1lYXN1cmVtZW50cyh3aWR0aCwgaGVpZ2h0LCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgeyBNQVhfSEVJR0hULCBNQVhfV0lEVEggfSA9IGdldENvbXByZXNzaW9uTGV2ZWwoZG9jdW1lbnRUeXBlKTtcblxuICBpZiAod2lkdGggPiBoZWlnaHQpIHtcbiAgICBoZWlnaHQgKj0gTUFYX1dJRFRIIC8gd2lkdGg7XG4gICAgd2lkdGggPSBNQVhfV0lEVEg7XG4gIH0gZWxzZSB7XG4gICAgd2lkdGggKj0gTUFYX0hFSUdIVCAvIGhlaWdodDtcbiAgICBoZWlnaHQgPSBNQVhfSEVJR0hUO1xuICB9XG4gIHJldHVybiB7XG4gICAgaGVpZ2h0LFxuICAgIHdpZHRoLFxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRJbWFnZU9yaWVudGF0aW9uKGltYWdlKSB7XG4gIGxldCBvcmllbnRhdGlvbiA9IDA7XG4gIEVYSUYuZ2V0RGF0YShpbWFnZSwgZnVuY3Rpb24oKSB7XG4gICAgb3JpZW50YXRpb24gPSBFWElGLmdldFRhZyh0aGlzLCAnT3JpZW50YXRpb24nKTtcbiAgfSk7XG4gIHJldHVybiBvcmllbnRhdGlvbjtcbn1cblxuZnVuY3Rpb24gZ2V0SW1hZ2VNZWFzdXJlbWVudHMoaW1hZ2UpIHtcbiAgY29uc3Qgb3JpZW50YXRpb24gPSBnZXRJbWFnZU9yaWVudGF0aW9uKGltYWdlKTtcbiAgbGV0IHRyYW5zZm9ybTtcbiAgbGV0IHN3YXAgPSBmYWxzZTtcbiAgc3dpdGNoIChvcmllbnRhdGlvbikge1xuICAgIGNhc2UgODpcbiAgICAgIHN3YXAgPSB0cnVlO1xuICAgICAgdHJhbnNmb3JtID0gJ2xlZnQnO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSA2OlxuICAgICAgc3dhcCA9IHRydWU7XG4gICAgICB0cmFuc2Zvcm0gPSAncmlnaHQnO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAzOlxuICAgICAgdHJhbnNmb3JtID0gJ2ZsaXAnO1xuICAgICAgYnJlYWs7XG4gICAgZGVmYXVsdDpcbiAgfVxuICBjb25zdCB3aWR0aCA9IHN3YXAgPyBpbWFnZS5oZWlnaHQgOiBpbWFnZS53aWR0aDtcbiAgY29uc3QgaGVpZ2h0ID0gc3dhcCA/IGltYWdlLndpZHRoIDogaW1hZ2UuaGVpZ2h0O1xuICByZXR1cm4ge1xuICAgIHdpZHRoLFxuICAgIGhlaWdodCxcbiAgICB0cmFuc2Zvcm0sXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldFNjYWxlZEltYWdlRmlsZUZyb21DYW52YXMoXG4gIGltYWdlLFxuICB3aWR0aCxcbiAgaGVpZ2h0LFxuICB0cmFuc2Zvcm0sXG4gIGZpbGVUeXBlLFxuKSB7XG4gIGNvbnN0IGNhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO1xuICBjYW52YXMud2lkdGggPSB3aWR0aDtcbiAgY2FudmFzLmhlaWdodCA9IGhlaWdodDtcbiAgY29uc3QgY29udGV4dCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICBsZXQgdHJhbnNmb3JtUGFyYW1zO1xuICBsZXQgc3dhcE1lYXN1cmUgPSBmYWxzZTtcbiAgbGV0IGltYWdlV2lkdGggPSB3aWR0aDtcbiAgbGV0IGltYWdlSGVpZ2h0ID0gaGVpZ2h0O1xuICBzd2l0Y2ggKHRyYW5zZm9ybSkge1xuICAgIGNhc2UgJ2xlZnQnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzAsIC0xLCAxLCAwLCAwLCBoZWlnaHRdO1xuICAgICAgc3dhcE1lYXN1cmUgPSB0cnVlO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAncmlnaHQnOlxuICAgICAgdHJhbnNmb3JtUGFyYW1zID0gWzAsIDEsIC0xLCAwLCB3aWR0aCwgMF07XG4gICAgICBzd2FwTWVhc3VyZSA9IHRydWU7XG4gICAgICBicmVhaztcbiAgICBjYXNlICdmbGlwJzpcbiAgICAgIHRyYW5zZm9ybVBhcmFtcyA9IFsxLCAwLCAwLCAtMSwgMCwgaGVpZ2h0XTtcbiAgICAgIGJyZWFrO1xuICAgIGRlZmF1bHQ6XG4gICAgICB0cmFuc2Zvcm1QYXJhbXMgPSBbMSwgMCwgMCwgMSwgMCwgMF07XG4gIH1cbiAgY29udGV4dC5zZXRUcmFuc2Zvcm0oLi4udHJhbnNmb3JtUGFyYW1zKTtcbiAgaWYgKHN3YXBNZWFzdXJlKSB7XG4gICAgaW1hZ2VIZWlnaHQgPSB3aWR0aDtcbiAgICBpbWFnZVdpZHRoID0gaGVpZ2h0O1xuICB9XG4gIGNvbnRleHQuZHJhd0ltYWdlKGltYWdlLCAwLCAwLCBpbWFnZVdpZHRoLCBpbWFnZUhlaWdodCk7XG4gIGNvbnRleHQuc2V0VHJhbnNmb3JtKDEsIDAsIDAsIDEsIDAsIDApO1xuICBjb25zdCBkYXRhVVJMID0gY2FudmFzLnRvRGF0YVVSTChmaWxlVHlwZSk7XG4gIHJldHVybiBkYXRhVVJMdG9CbG9iKGRhdGFVUkwsIGZpbGVUeXBlKTtcbn1cblxuZnVuY3Rpb24gc2hvdWxkUmVzaXplKGltYWdlTWVhc3VyZW1lbnRzLCBmaWxlU2l6ZSwgZG9jdW1lbnRUeXBlKSB7XG4gIGNvbnN0IHsgTUFYX0hFSUdIVCwgTUFYX1dJRFRILCBET0NVTUVOVF9TSVpFIH0gPSBnZXRDb21wcmVzc2lvbkxldmVsKFxuICAgIGRvY3VtZW50VHlwZSxcbiAgKTtcbiAgaWYgKGRvY3VtZW50VHlwZSA9PT0gJ2luc3VyYW5jZScgJiYgZmlsZVNpemUgPCBET0NVTUVOVF9TSVpFKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9IGVsc2UgaWYgKGZpbGVTaXplIDwgRE9DVU1FTlRfU0laRSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICByZXR1cm4gKFxuICAgIGltYWdlTWVhc3VyZW1lbnRzLndpZHRoID4gTUFYX1dJRFRIIHx8IGltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCA+IE1BWF9IRUlHSFRcbiAgKTtcbn1cblxuYXN5bmMgZnVuY3Rpb24gcHJvY2Vzc0ZpbGUoZGF0YVVSTCwgZmlsZVNpemUsIGZpbGVUeXBlLCBkb2N1bWVudFR5cGUpIHtcbiAgY29uc3QgaW1hZ2UgPSBuZXcgSW1hZ2UoKTtcbiAgaW1hZ2Uuc3JjID0gZGF0YVVSTDtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICBpbWFnZS5vbmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGNvbnN0IGltYWdlTWVhc3VyZW1lbnRzID0gZ2V0SW1hZ2VNZWFzdXJlbWVudHMoaW1hZ2UpO1xuICAgICAgaWYgKCFzaG91bGRSZXNpemUoaW1hZ2VNZWFzdXJlbWVudHMsIGZpbGVTaXplLCBkb2N1bWVudFR5cGUpKSB7XG4gICAgICAgIHJlc29sdmUoZGF0YVVSTHRvQmxvYihkYXRhVVJMLCBmaWxlVHlwZSkpO1xuICAgICAgfVxuICAgICAgY29uc3Qgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMgPSBnZXRTY2FsZWRJbWFnZU1lYXN1cmVtZW50cyhcbiAgICAgICAgaW1hZ2VNZWFzdXJlbWVudHMud2lkdGgsXG4gICAgICAgIGltYWdlTWVhc3VyZW1lbnRzLmhlaWdodCxcbiAgICAgICAgZG9jdW1lbnRUeXBlLFxuICAgICAgKTtcbiAgICAgIGNvbnN0IHNjYWxlZEltYWdlRmlsZSA9IGdldFNjYWxlZEltYWdlRmlsZUZyb21DYW52YXMoXG4gICAgICAgIGltYWdlLFxuICAgICAgICBzY2FsZWRJbWFnZU1lYXN1cmVtZW50cy53aWR0aCxcbiAgICAgICAgc2NhbGVkSW1hZ2VNZWFzdXJlbWVudHMuaGVpZ2h0LFxuICAgICAgICBpbWFnZU1lYXN1cmVtZW50cy50cmFuc2Zvcm0sXG4gICAgICAgIGZpbGVUeXBlLFxuICAgICAgKTtcbiAgICAgIHJlc29sdmUoc2NhbGVkSW1hZ2VGaWxlKTtcbiAgICB9O1xuICAgIGltYWdlLm9uZXJyb3IgPSByZWplY3Q7XG4gIH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29tcHJlc3NGaWxlKGZpbGUsIGRvY3VtZW50VHlwZSA9ICdkZWZhdWx0Jykge1xuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XG4gICAgICByZWFkZXIub25sb2FkZW5kID0gYXN5bmMgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnN0IGNvbXByZXNzZWRGaWxlID0gYXdhaXQgcHJvY2Vzc0ZpbGUoXG4gICAgICAgICAgcmVhZGVyLnJlc3VsdCxcbiAgICAgICAgICBmaWxlLnNpemUsXG4gICAgICAgICAgZmlsZS50eXBlLFxuICAgICAgICAgIGRvY3VtZW50VHlwZSxcbiAgICAgICAgKTtcbiAgICAgICAgcmVzb2x2ZShjb21wcmVzc2VkRmlsZSk7XG4gICAgICB9O1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgcmVqZWN0KGVycik7XG4gICAgfVxuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldExvZ2luSW5mbygpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCBsb2dpbkluZm8gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9naW5JbmZvJyk7XG4gICAgaWYgKF8uaXNTdHJpbmcobG9naW5JbmZvKSkge1xuICAgICAgcmV0dXJuIEpTT04ucGFyc2UobG9naW5JbmZvKTtcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUubG9nKCdFcnJvciB3aGVuIHBhcnNpbmcgbG9naW5JbmZvIG9iamVjdDogJywgZXJyKTtcbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRQb3N0Q29uZmlnKGJvZHkpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgbWltZVR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvZ2luSW5mby50b2tlbn1gLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLCBtaW1lVHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgLy8gbW9kZTogJ25vLWNvcnMnLFxuICAgIGhlYWRlcnMsXG4gICAgLi4uKF8uaXNFbXB0eShib2R5KSA/IHt9IDogeyBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KSB9KSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFB1dENvbmZpZyhib2R5KSB7XG4gIGNvbnN0IGxvZ2luSW5mbyA9IGdldExvZ2luSW5mbygpO1xuICBjb25zdCBoZWFkZXJzID0gbG9naW5JbmZvXG4gICAgPyB7XG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdYLUF1dGhvcml6YXRpb24nOiBsb2dpbkluZm8udG9rZW4sXG4gICAgICB9XG4gICAgOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfTtcbiAgcmV0dXJuIHtcbiAgICBtZXRob2Q6ICdQVVQnLFxuICAgIGhlYWRlcnMsXG4gICAgLi4uKF8uaXNFbXB0eShib2R5KSA/IHt9IDogeyBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KSB9KSxcbiAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEdldENvbmZpZygpIHtcbiAgY29uc3QgbG9naW5JbmZvID0gZ2V0TG9naW5JbmZvKCk7XG4gIGNvbnN0IGhlYWRlcnMgPSBsb2dpbkluZm9cbiAgICA/IHtcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvZ2luSW5mby50b2tlbn1gLFxuICAgICAgfVxuICAgIDogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH07XG4gIHJldHVybiB7XG4gICAgbWV0aG9kOiAnR0VUJyxcbiAgICBoZWFkZXJzLFxuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29udmVydE51bWJlcnNUb1BlcnNpYW4oc3RyKSB7XG4gIGNvbnN0IHBlcnNpYW5OdW1iZXJzID0gJ9uw27Hbstuz27Tbtdu227fbuNu5JztcbiAgbGV0IHJlc3VsdCA9ICcnO1xuICBpZiAoIXN0ciAmJiBzdHIgIT09IDApIHtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG4gIHN0ciA9IHN0ci50b1N0cmluZygpO1xuICBsZXQgY29udmVydGVkQ2hhcjtcbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyBpKyspIHtcbiAgICB0cnkge1xuICAgICAgY29udmVydGVkQ2hhciA9IHBlcnNpYW5OdW1iZXJzW3N0cltpXV07XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgY29udmVydGVkQ2hhciA9IHN0cltpXTtcbiAgICB9XG4gICAgcmVzdWx0ICs9IGNvbnZlcnRlZENoYXI7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGZldGNoQXBpKHVybCwgY29uZmlnKSB7XG4gIGxldCBmbG93RXJyb3I7XG4gIHRyeSB7XG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCBjb25maWcpO1xuICAgIGNvbnNvbGUubG9nKHJlcyk7XG4gICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG4gICAgbGV0IHJlc3VsdCA9IGF3YWl0IHJlcy5qc29uKCk7XG4gICAgY29uc29sZS5sb2cocmVzdWx0KTtcbiAgICBpZiAoIV8uaXNFbXB0eShfLmdldChyZXN1bHQsICdkYXRhJykpKSB7XG4gICAgICByZXN1bHQgPSByZXN1bHQuZGF0YTtcbiAgICB9XG4gICAgY29uc29sZS5sb2cocmVzLnN0YXR1cyk7XG5cbiAgICBpZiAocmVzLnN0YXR1cyAhPT0gMjAwKSB7XG4gICAgICBmbG93RXJyb3IgPSByZXN1bHQ7XG4gICAgICBjb25zb2xlLmxvZyhyZXMuc3RhdHVzKTtcblxuICAgICAgaWYgKHJlcy5zdGF0dXMgPT09IDQwMykge1xuICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcbiAgICAgICAgZmxvd0Vycm9yLm1lc3NhZ2UgPSAn2YTYt9mB2Kcg2K/ZiNio2KfYsdmHINmI2KfYsdivINi02YjbjNivISc7XG4gICAgICAgIGNvbnNvbGUubG9nKCd5ZWVlcycpO1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IGhpc3RvcnkucHVzaCgnL2xvZ2luJyksIDMwMDApO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgICBjb25zb2xlLmRlYnVnKGBFcnJvciB3aGVuIGZldGNoaW5nIGFwaTogJHt1cmx9YCwgZXJyKTtcbiAgICBmbG93RXJyb3IgPSB7XG4gICAgICBjb2RlOiAnVU5LTk9XTl9FUlJPUicsXG4gICAgICBtZXNzYWdlOiAn2K7Yt9in24wg2YbYp9mF2LTYrti1INmE2LfZgdinINiv2YjYqNin2LHZhyDYs9i524wg2qnZhtuM2K8nLFxuICAgIH07XG4gIH1cbiAgaWYgKGZsb3dFcnJvcikge1xuICAgIHRocm93IGZsb3dFcnJvcjtcbiAgfVxufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gc2V0SW1tZWRpYXRlKGZuKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGxldCB2YWx1ZSA9IG51bGw7XG4gICAgICBpZiAoXy5pc0Z1bmN0aW9uKGZuKSkge1xuICAgICAgICB2YWx1ZSA9IGZuKCk7XG4gICAgICB9XG4gICAgICByZXNvbHZlKHZhbHVlKTtcbiAgICB9LCAwKTtcbiAgfSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRDb29raWUobmFtZSwgdmFsdWUsIGRheXMgPSA3KSB7XG4gIGxldCBleHBpcmVzID0gJyc7XG4gIGlmIChkYXlzKSB7XG4gICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKCk7XG4gICAgZGF0ZS5zZXRUaW1lKGRhdGUuZ2V0VGltZSgpICsgZGF5cyAqIDI0ICogNjAgKiA2MCAqIDEwMDApO1xuICAgIGV4cGlyZXMgPSBgOyBleHBpcmVzPSR7ZGF0ZS50b1VUQ1N0cmluZygpfWA7XG4gIH1cbiAgZG9jdW1lbnQuY29va2llID0gYCR7bmFtZX09JHt2YWx1ZSB8fCAnJ30ke2V4cGlyZXN9OyBwYXRoPS9gO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldENvb2tpZShuYW1lKSB7XG4gIGNvbnN0IG5hbWVFUSA9IGAke25hbWV9PWA7XG4gIGNvbnN0IGNhID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XG4gIGNvbnN0IGxlbmd0aCA9IGNhLmxlbmd0aDtcbiAgbGV0IGkgPSAwO1xuICBmb3IgKDsgaSA8IGxlbmd0aDsgaSArPSAxKSB7XG4gICAgbGV0IGMgPSBjYVtpXTtcbiAgICB3aGlsZSAoYy5jaGFyQXQoMCkgPT09ICcgJykge1xuICAgICAgYyA9IGMuc3Vic3RyaW5nKDEsIGMubGVuZ3RoKTtcbiAgICB9XG4gICAgaWYgKGMuaW5kZXhPZihuYW1lRVEpID09PSAwKSB7XG4gICAgICByZXR1cm4gYy5zdWJzdHJpbmcobmFtZUVRLmxlbmd0aCwgYy5sZW5ndGgpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gbnVsbDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGVyYXNlQ29va2llKG5hbWUpIHtcbiAgZG9jdW1lbnQuY29va2llID0gYCR7bmFtZX09OyBNYXgtQWdlPS05OTk5OTk5OTtgO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm90aWZ5RXJyb3IobWVzc2FnZSkge1xuICB0b2FzdHIub3B0aW9ucyA9IHtcbiAgICBjbG9zZUJ1dHRvbjogZmFsc2UsXG4gICAgZGVidWc6IGZhbHNlLFxuICAgIG5ld2VzdE9uVG9wOiBmYWxzZSxcbiAgICBwcm9ncmVzc0JhcjogZmFsc2UsXG4gICAgcG9zaXRpb25DbGFzczogJ3RvYXN0LXRvcC1jZW50ZXInLFxuICAgIHByZXZlbnREdXBsaWNhdGVzOiB0cnVlLFxuICAgIG9uY2xpY2s6IG51bGwsXG4gICAgc2hvd0R1cmF0aW9uOiAnMzAwJyxcbiAgICBoaWRlRHVyYXRpb246ICcxMDAwJyxcbiAgICB0aW1lT3V0OiAnMTAwMDAnLFxuICAgIGV4dGVuZGVkVGltZU91dDogJzEwMDAnLFxuICAgIHNob3dFYXNpbmc6ICdzd2luZycsXG4gICAgaGlkZUVhc2luZzogJ2xpbmVhcicsXG4gICAgc2hvd01ldGhvZDogJ3NsaWRlRG93bicsXG4gICAgaGlkZU1ldGhvZDogJ3NsaWRlVXAnLFxuICB9O1xuICB0b2FzdHIuZXJyb3IobWVzc2FnZSwgJ9iu2LfYpycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm90aWZ5U3VjY2VzcyhtZXNzYWdlKSB7XG4gIHRvYXN0ci5vcHRpb25zID0ge1xuICAgIGNsb3NlQnV0dG9uOiBmYWxzZSxcbiAgICBkZWJ1ZzogZmFsc2UsXG4gICAgbmV3ZXN0T25Ub3A6IGZhbHNlLFxuICAgIHByb2dyZXNzQmFyOiBmYWxzZSxcbiAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtdG9wLWNlbnRlcicsXG4gICAgcHJldmVudER1cGxpY2F0ZXM6IHRydWUsXG4gICAgb25jbGljazogbnVsbCxcbiAgICBzaG93RHVyYXRpb246ICczMDAnLFxuICAgIGhpZGVEdXJhdGlvbjogJzEwMDAnLFxuICAgIHRpbWVPdXQ6ICc1MDAwJyxcbiAgICBleHRlbmRlZFRpbWVPdXQ6ICcxMDAwJyxcbiAgICBzaG93RWFzaW5nOiAnc3dpbmcnLFxuICAgIGhpZGVFYXNpbmc6ICdsaW5lYXInLFxuICAgIHNob3dNZXRob2Q6ICdzbGlkZURvd24nLFxuICAgIGhpZGVNZXRob2Q6ICdzbGlkZVVwJyxcbiAgfTtcbiAgdG9hc3RyLnN1Y2Nlc3MobWVzc2FnZSwgJycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVsb2FkU2VsZWN0UGlja2VyKCkge1xuICB0cnkge1xuICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoJ3JlbmRlcicpO1xuICB9IGNhdGNoIChlcnIpIHtcbiAgICBjb25zb2xlLmxvZyhlcnIpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkU2VsZWN0UGlja2VyKCkge1xuICB0cnkge1xuICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoe30pO1xuICAgIGlmIChcbiAgICAgIC9BbmRyb2lkfHdlYk9TfGlQaG9uZXxpUGFkfGlQb2R8QmxhY2tCZXJyeS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudClcbiAgICApIHtcbiAgICAgICQoJy5zZWxlY3RwaWNrZXInKS5zZWxlY3RwaWNrZXIoJ21vYmlsZScpO1xuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgY29uc29sZS5sb2coZXJyKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Rm9ybURhdGEoZGF0YSkge1xuICBpZiAoXy5pc0VtcHR5KGRhdGEpKSB7XG4gICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgaW5wdXQgZGF0YSB0byBjcmVhdGUgYSBGb3JtRGF0YSBvYmplY3QnKTtcbiAgfVxuICBsZXQgZm9ybURhdGE7XG4gIHRyeSB7XG4gICAgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICBjb25zdCBrZXlzID0gXy5rZXlzKGRhdGEpO1xuICAgIGxldCBpO1xuICAgIGNvbnN0IGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgZm9ybURhdGEuYXBwZW5kKGtleXNbaV0sIGRhdGFba2V5c1tpXV0pO1xuICAgIH1cbiAgICByZXR1cm4gZm9ybURhdGE7XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGNvbnNvbGUuZGVidWcoZXJyKTtcbiAgICB0aHJvdyBFcnJvcignRk9STV9EQVRBX0JST1dTRVJfU1VQUE9SVF9GQUlMVVJFJyk7XG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRVc2VyTG9naW5TdGF0dXMoc3RvcmUpIHtcbiAgY29uc3QgbG9naW5TdGF0dXNBY3Rpb24gPSBsb2FkTG9naW5TdGF0dXMoKTtcbiAgc3RvcmUuZGlzcGF0Y2gobG9naW5TdGF0dXNBY3Rpb24pO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy91dGlscy9pbmRleC5qcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQXFCQTtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUZBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7Ozs7Ozs7O0FDaFJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUZBO0FBS0E7Ozs7Ozs7QUNoQ0E7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQWZBO0FBREE7QUF5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFCQTtBQURBO0FBb0NBO0FBdkNBO0FBQ0E7QUF5Q0E7Ozs7Ozs7QUNuREE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIQTtBQU1BO0FBaEJBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFEQTtBQWNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUZBO0FBREE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTNCQTtBQXVDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFDQTtBQURBO0FBSkE7QUE4REE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBL0RBO0FBREE7QUFEQTtBQURBO0FBREE7QUFEQTtBQThFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFGQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUEzQkE7QUF1Q0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUExQ0E7QUFEQTtBQUpBO0FBOERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQS9EQTtBQURBO0FBREE7QUF1RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVRBO0FBREE7QUFEQTtBQURBO0FBeEVBO0FBREE7QUFEQTtBQW1HQTtBQWhNQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQWlNQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTs7Ozs7OztBQzVOQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7Ozs7Ozs7O0FDZEE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7Ozs7O0FDcEJBO0FBQUE7QUFBQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDYkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQU1BO0FBVEE7QUFDQTtBQVdBOzs7Ozs7O0FDaEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUlBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcUhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBS0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExQkE7Ozs7Ozs7QUF4SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBc0NBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==