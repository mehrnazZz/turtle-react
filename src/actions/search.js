import {
  GET_HOUSES_REQUEST,
  GET_HOUSES_SUCCESS,
  GET_HOUSES_FAILURE,
  POLL_HOUSES_REQUEST,
  POLL_HOUSES_SUCCESS,
  POLL_HOUSES_FAILURE,
  SET_SEARCH_OBJ_REQUEST,
  SET_SEARCH_OBJ_SUCCESS,
  SET_SEARCH_OBJ_FAILURE,
} from '../constants';
import history from '../history';
import {
  sendSearchHousesRequest,
  sendpollHousesRequest,
} from '../api-handlers/search';
import { notifyError, notifySuccess, getFormData } from '../utils';

export function setSearchObjRequest() {
  return {
    type: SET_SEARCH_OBJ_REQUEST,
  };
}

export function setSearchObjSuccess(minArea, dealType, buildingType, maxPrice) {
  return {
    type: SET_SEARCH_OBJ_SUCCESS,
    payload: { minArea, dealType, buildingType, maxPrice },
  };
}

export function setSearchObjFailure(err) {
  return {
    type: SET_SEARCH_OBJ_FAILURE,
    payload: { err },
  };
}

export function setSearchObj(minArea, dealType, buildingType, maxPrice) {
  return async function(dispatch) {
    dispatch(setSearchObjSuccess(minArea, dealType, buildingType, maxPrice));
    history.push('/houses');
  };
}
export function searchHousesRequest() {
  return {
    type: GET_HOUSES_REQUEST,
  };
}

export function searchHousesSuccess(response) {
  return {
    type: GET_HOUSES_SUCCESS,
    payload: { response },
  };
}

export function searchHousesFailure(err) {
  return {
    type: GET_HOUSES_FAILURE,
    payload: { err },
  };
}

export function searchHouses(minArea, dealType, buildingType, maxPrice) {
  return async function(dispatch) {
    dispatch(searchHousesRequest());
    try {
      if (!buildingType) {
        buildingType = null;
      }
      const res = await sendSearchHousesRequest(
        parseFloat(minArea),
        parseFloat(dealType),
        buildingType,
        parseFloat(maxPrice),
      );
      dispatch(searchHousesSuccess(res));
      dispatch(setSearchObj(minArea, dealType, buildingType, maxPrice));
      if (res.length === 0) {
        notifySuccess('خانه ای با این این مشخصات ثبت نشده است');
      }
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(searchHousesFailure(error));
    }
  };
}

export function pollHousesRequest() {
  return {
    type: POLL_HOUSES_REQUEST,
  };
}

export function pollHousesSuccess(response) {
  return {
    type: POLL_HOUSES_SUCCESS,
    payload: { response },
  };
}

export function pollHousesFailure(err) {
  return {
    type: POLL_HOUSES_FAILURE,
    payload: { err },
  };
}

export function pollHouses(minArea, dealType, buildingType, maxPrice) {
  return async function(dispatch, getState) {
    dispatch(pollHousesRequest());
    try {
      if (!buildingType) {
        buildingType = null;
      }
      let res = await sendpollHousesRequest(
        parseFloat(minArea),
        parseFloat(dealType),
        buildingType,
        parseFloat(maxPrice),
      );
      console.log(res);
      if (!res) {
        res = getState().search.houses;
        console.log('hala');
        console.log(res);
      }
      dispatch(pollHousesSuccess(res));
      notifyError('اطلاعات جست و جوی شما بروز شد.');

      dispatch(setSearchObj(minArea, dealType, buildingType, maxPrice));
      if (res.length === 0) {
        notifySuccess('خانه ای با این این مشخصات ثبت نشده است');
      }
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(pollHousesFailure(error));
    }
  };
}
