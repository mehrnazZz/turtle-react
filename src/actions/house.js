import {
  CREATE_HOUSE_REQUEST,
  CREATE_HOUSE_SUCCESS,
  CREATE_HOUSE_FAILURE,
  GET_HOUSE_REQUEST,
  GET_HOUSE_SUCCESS,
  GET_HOUSE_FAILURE,
  GET_HOUSE_PHONE_REQUEST,
  GET_HOUSE_PHONE_SUCCESS,
  GET_HOUSE_PHONE_FAILURE,
  GET_PHONE_STATUS_REQUEST,
  GET_PHONE_STATUS_SUCCESS,
  GET_PHONE_STATUS_FAILURE,
} from '../constants';
import { getCurrentUser } from '../actions/authentication';
import {
  sendcreateHouseRequest,
  sendgetHouseDetailRequest,
  sendgetHousePhoneNumberRequest,
  sendgetPhoneStatusRequest,
} from '../api-handlers/house';
import { notifyError, notifySuccess } from '../utils';

export function createHouseRequest() {
  return {
    type: CREATE_HOUSE_REQUEST,
  };
}

export function createHouseSuccess(response) {
  return {
    type: CREATE_HOUSE_SUCCESS,
    payload: { response },
  };
}

export function createHouseFailure(err) {
  return {
    type: CREATE_HOUSE_FAILURE,
    payload: { err },
  };
}

export function createHouse(
  area,
  buildingType,
  address,
  dealType,
  basePrice,
  rentPrice,
  sellPrice,
  phone,
  description,
  expireTime,
  imageURL,
) {
  return async function(dispatch, getState) {
    dispatch(createHouseRequest());
    try {
      const { currUser } = getState().authentication;
      const res = await sendcreateHouseRequest(
        area,
        buildingType,
        address,
        dealType,
        basePrice,
        rentPrice,
        sellPrice,
        phone,
        description,
        expireTime,
        imageURL,
      );
      dispatch(createHouseSuccess(res));
      notifySuccess('خانه با موفقیت ثبت شد');
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      notifyError('خطا مجدد تلاش کنید');
      dispatch(createHouseFailure(error));
    }
  };
}
export function getPhoneStatusRequest() {
  return {
    type: GET_PHONE_STATUS_REQUEST,
  };
}

export function getPhoneStatusSuccess(response) {
  return {
    type: GET_PHONE_STATUS_SUCCESS,
    payload: { response },
  };
}

export function getPhoneStatusFailure(err) {
  return {
    type: GET_PHONE_STATUS_FAILURE,
    payload: { err },
  };
}

export function getPhoneStatus(houseId) {
  return async function(dispatch, getState) {
    dispatch(getPhoneStatusRequest());
    try {
      const { currUser } = getState().authentication;
      const res = await sendgetPhoneStatusRequest(houseId);
      dispatch(getPhoneStatusSuccess(res));
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(getPhoneStatusFailure(error));
    }
  };
}
export function getHouseDetailRequest() {
  return {
    type: GET_HOUSE_REQUEST,
  };
}

export function getHouseDetailSuccess(response, pathname) {
  return {
    type: GET_HOUSE_SUCCESS,
    payload: { response, pathname },
  };
}

export function getHouseDetailFailure(err) {
  return {
    type: GET_HOUSE_FAILURE,
    payload: { err },
  };
}

export function getHouseDetail(houseId, pathname) {
  return async function(dispatch, getState) {
    dispatch(getHouseDetailRequest());
    try {
      const { currUser } = getState().authentication;
      const res = await sendgetHouseDetailRequest(houseId);
      dispatch(getHouseDetailSuccess(res, pathname));
      if (currUser) {
        dispatch(getPhoneStatus(houseId));
      }
    } catch (error) {
      notifyError(error.message);
      dispatch(getHouseDetailFailure(error));
    }
  };
}

export function getHousePhoneNumberRequest() {
  return {
    type: GET_HOUSE_PHONE_REQUEST,
  };
}

export function getHousePhoneNumberSuccess(response) {
  return {
    type: GET_HOUSE_PHONE_SUCCESS,
    payload: { response },
  };
}

export function getHousePhoneNumberFailure(err) {
  return {
    type: GET_HOUSE_PHONE_FAILURE,
    payload: { err },
  };
}

export function getHousePhoneNumber(houseId) {
  return async function(dispatch, getState) {
    dispatch(getHousePhoneNumberRequest());
    try {
      const { currUser } = getState().authentication;
      const res = await sendgetHousePhoneNumberRequest(houseId);
      console.log(res);
      if (!res.hasOwnProperty('message') && res !== null) {
        dispatch(getHousePhoneNumberSuccess('payed'));
        dispatch(getCurrentUser(currUser.userId));
      } else if (res.hasOwnProperty('message')) {
        dispatch(getHousePhoneNumberSuccess('no_credit'));
      }
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(getHousePhoneNumberFailure(error));
    }
  };
}
