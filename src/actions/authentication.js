import { notifySuccess, notifyError, getLoginInfo } from '../utils';
import {
  GET_CURR_USER_REQUEST,
  GET_CURR_USER_SUCCESS,
  GET_CURR_USER_FAILURE,
  INCREASE_CREDIT_REQUEST,
  INCREASE_CREDIT_SUCCESS,
  INCREASE_CREDIT_FAILURE,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
  USER_IS_NOT_LOGGED_IN,
  GET_INIT_FAILURE,
  GET_INIT_REQUEST,
  GET_INIT_SUCCESS,
  GET_INIT_USER_FAILURE,
  GET_INIT_USER_REQUEST,
  GET_INIT_USER_SUCCESS,
} from '../constants';
import history from '../history';
import {
  sendgetCurrentUserRequest,
  sendincreaseCreditRequest,
  sendInitiateRequest,
  sendInitiateUsersRequest,
  sendLoginRequest,
} from '../api-handlers/authentication';

export function loginRequest(username, password) {
  return {
    type: USER_LOGIN_REQUEST,
    payload: {
      username,
      password,
    },
  };
}

export function loginSuccess(info) {
  console.log(info);
  return {
    type: USER_LOGIN_SUCCESS,
    payload: { info },
  };
}

export function loginFailure(error) {
  return {
    type: USER_LOGIN_FAILURE,
    payload: { error },
  };
}

export function loadLoginStatus() {
  const loginInfo = getLoginInfo();
  if (loginInfo) {
    return loginSuccess(loginInfo);
  }
  return {
    type: USER_IS_NOT_LOGGED_IN,
  };
}

function getCaptchaToken() {
  if (process.env.BROWSER) {
    const loginForm = $('#loginForm').get(0);
    if (loginForm && typeof loginForm['g-recaptcha-response'] === 'object') {
      return loginForm['g-recaptcha-response'].value;
    }
  }
  return null;
}

export function loginUser(username, password) {
  return async function(dispatch, getState) {
    dispatch(loginRequest(username, password));
    try {
      // const captchaToken = getCaptchaToken();
      const res = await sendLoginRequest(username, password);
      dispatch(loginSuccess(res));
      localStorage.setItem('loginInfo', JSON.stringify(res));
      // console.log(location.pathname);
      const { pathname } = getState().house;
      console.log(pathname);
      history.push(pathname);
    } catch (err) {
      dispatch(loginFailure(err));
    }
  };
}

export function logoutRequest() {
  return {
    type: USER_LOGOUT_REQUEST,
    payload: {},
  };
}

export function logoutSuccess() {
  return {
    type: USER_LOGOUT_SUCCESS,
    payload: {
      message: 'خروج موفقیت آمیز',
    },
  };
}

export function logoutFailure(error) {
  return {
    type: USER_LOGOUT_FAILURE,
    payload: { error },
  };
}

export function logoutUser() {
  return async function(dispatch) {
    dispatch(logoutRequest());
    try {
      dispatch(logoutSuccess());
      localStorage.clear();
      location.assign('/login');
    } catch (err) {
      dispatch(logoutFailure(err));
    }
  };
}

export function initiateRequest() {
  return {
    type: GET_INIT_REQUEST,
  };
}

export function initiateSuccess(response) {
  return {
    type: GET_INIT_SUCCESS,
    payload: { response },
  };
}

export function initiateFailure(err) {
  return {
    type: GET_INIT_FAILURE,
    payload: { err },
  };
}

export function initiate() {
  return async function(dispatch) {
    dispatch(initiateRequest());
    try {
      const res = await sendInitiateRequest();
      dispatch(initiateSuccess(res));
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(initiateFailure(error));
    }
  };
}

export function initiateUsersRequest() {
  return {
    type: GET_INIT_USER_REQUEST,
  };
}

export function initiateUsersSuccess(response) {
  return {
    type: GET_INIT_USER_SUCCESS,
    payload: { response },
  };
}

export function initiateUsersFailure(err) {
  return {
    type: GET_INIT_USER_FAILURE,
    payload: { err },
  };
}

export function initiateUsers() {
  return async function(dispatch) {
    dispatch(initiateUsersRequest());
    try {
      const res = await sendInitiateUsersRequest();
      dispatch(initiateUsersSuccess(res));
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(initiateUsersFailure(error));
    }
  };
}

export function getCurrentUserRequest() {
  return {
    type: GET_CURR_USER_REQUEST,
  };
}

export function getCurrentUserSuccess(response) {
  return {
    type: GET_CURR_USER_SUCCESS,
    payload: { response },
  };
}

export function getCurrentUserFailure(err) {
  return {
    type: GET_CURR_USER_FAILURE,
    payload: { err },
  };
}

export function getCurrentUser(userId) {
  return async function(dispatch) {
    dispatch(getCurrentUserRequest());
    try {
      const res = await sendgetCurrentUserRequest(userId);
      dispatch(getCurrentUserSuccess(res));
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      dispatch(getCurrentUserFailure(error));
    }
  };
}

export function increaseCreditRequest() {
  return {
    type: INCREASE_CREDIT_REQUEST,
  };
}

export function increaseCreditSuccess(response) {
  return {
    type: INCREASE_CREDIT_SUCCESS,
    payload: { response },
  };
}

export function increaseCreditFailure(err) {
  return {
    type: INCREASE_CREDIT_FAILURE,
    payload: { err },
  };
}

export function increaseCredit(amount) {
  return async function(dispatch, getState) {
    dispatch(increaseCreditRequest());
    try {
      const { currUser } = getState().authentication;
      const res = await sendincreaseCreditRequest(amount, currUser.userId);
      dispatch(increaseCreditSuccess(res));
      dispatch(getCurrentUser(currUser.userId));
      notifySuccess('افزایش اعتبار با موفقیت انجام شد.');
    } catch (error) {
      if (error.message) {
        notifyError(error.message);
      }
      notifyError('خطا مجدد تلاش کنید');
      dispatch(increaseCreditFailure(error));
    }
  };
}
