import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './NotFound.scss';

class NotFound extends React.Component {
  render() {
    return (
      <div className="not-found-box">
        <div className="not-found-text">
          <p> صفحه‌ی مورد نظر شما یافت نشد.</p>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(NotFound);
