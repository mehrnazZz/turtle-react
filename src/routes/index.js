/* eslint-disable prettier/prettier */
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable global-require */
import _ from 'lodash';
import analytics from '../utils/analytics';

// The top-level (parent) route
const routes = {
  path: '/',

  // Keep in mind, routes are evaluated in order
  children: [
    {
      path: '/login',
      load: () => import(/* webpackChunkName: 'root' */ './login'),
    },
    {
      path: '/',
      load: () => import(/* webpackChunkName: 'home' */ './home'),
    },

    {
      path: '/increaseCredit',
      load: () => import(/* webpackChunkName: 'increaseCredit' */ './increaseCredit'),
    },
    {
      path: '/houses',
      load: () => import(/* webpackChunkName: 'houses' */ './houses'),
    },
    {
        path: '/houses/new',
        load: () => import(/* webpackChunkName: 'new-house' */ './houses/new'),
    },
    {
      path: '/houses/:houseId',
      load: () => import(/* webpackChunkName: 'houseDetail' */ './houses/houseDetail'),
    },
    // Wildcard routes, e.g. { path: '*', ... } (must go last)
    {
      path: '*',
      load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
    },
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();

    // Provide default values for title, description etc.
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';

    if(_.isFunction(route.logPageView)){
      route.logPageView();
    } else {
      analytics.logPageView();
    }

    return route;
  },
};

// The error page is available by permanent url for development mode
if (__DEV__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  });
}

export default routes;
