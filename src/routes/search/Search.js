import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import _ from 'lodash';
import * as actions from '../../actions/driver';
import s from './Search.scss';
import PhoneInput from '../../components/PhoneInput';

class Search extends React.Component {
  static propTypes = {
    drivers: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
  };

  static defaultProps = {
    searchError: null,
    getDriverError: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      phoneNumber: '',
    };
  }

  get phoneNumber() {
    const { phoneNumber = '' } = this.state;
    return phoneNumber.replace('+98', '0').replace(/ /g, '');
  }

  loadDriverInfo(e, driver) {
    e.preventDefault();
    this.props.actions.getDriverInfo(driver.id);
    this.props.actions.getDriverDocumentIds(driver.id);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.actions.searchDriver(this.phoneNumber);
  };

  render() {
    let drivers = [];
    if (!_.isEmpty(this.props.drivers)) {
      drivers = this.props.drivers.map(driver => (
        <a
          key={driver.id}
          onClick={e => this.loadDriverInfo(e, driver)}
          className="search-result"
        >
          <div className="search-result-driver-full-name">
            {driver.id} {driver.fullName}
          </div>
        </a>
      ));
    }
    return (
      <div>
        <div className="search-box">
          <form onSubmit={this.handleSubmit} autoComplete="on" noValidate>
            <div className="search-box-inputs">
              <PhoneInput
                type="tel"
                placeholder="شماره سفیر"
                autoComplete="on"
                onChange={e => this.setState({ phoneNumber: e.target.value })}
                value={this.state.phoneNumber}
              />
              <input
                className="search-box-submit-button"
                type="submit"
                value="جستجو"
              />
            </div>
          </form>
        </div>
        <div className="search-results">{drivers}</div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

function mapStateToProps(state) {
  const { driver } = state;
  const { isFetching, drivers } = driver;
  return {
    isFetching,
    drivers,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(Search),
);
