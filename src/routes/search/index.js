import React from 'react';
import Search from './Search';
import Layout from '../../components/AuthLayout';
import AuthenticatedComponent from '../../components/AuthenticatedComponent';

const title = 'جستجوی راننده';
const AuthenticatedLayout = AuthenticatedComponent(Layout);

function action() {
  return {
    chunks: ['search'],
    title,
    component: (
      <AuthenticatedLayout>
        <Search />,
      </AuthenticatedLayout>
    ),
  };
}

export default action;
