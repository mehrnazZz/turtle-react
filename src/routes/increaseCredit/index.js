import React from 'react';
import Layout from 'components/Layout';
import IncreaseCreditContainer from './increaseCreditContainer';

const title = 'خانه';

function action() {
  return {
    chunks: ['increaseCredit'],
    title,
    component: (
      <Layout>
        <IncreaseCreditContainer />
      </Layout>
    ),
  };
}

export default action;
