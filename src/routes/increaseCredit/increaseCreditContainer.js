import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as authActions from '../../actions/authentication';
import IncreaseCreditForm from '../../components/IncreaseCreditForm';
import Loader from '../../components/Loader';

class IncreaseCreditContainer extends React.Component {
  static propTypes = {
    isFetching: PropTypes.bool.isRequired,
    authActions: PropTypes.object.isRequired,
    currUser: PropTypes.object.isRequired,
  };
  static defaultProps = {
    isFetching: false,
    loginError: null,
  };
  componentDidMount() {
    if(!this.props.currUser.hasOwnProperty('userId')) {
      this.props.authActions.getCurrentUser();
    }
  }
  handleSubmit = amount => {
    this.props.authActions.increaseCredit(amount);
  };

  render() {
    return (
      <div>
        <IncreaseCreditForm
          handleSubmit={this.handleSubmit}
          currUser={this.props.currUser}
        />
        <Loader loading={this.props.isFetching} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser, isFetching } = authentication;
  return {
    currUser,
    isFetching,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  IncreaseCreditContainer,
);
