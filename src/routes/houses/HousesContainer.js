import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as searchActions from '../../actions/search';
import HousesPage from '../../components/HousesPage';
import { notifyError } from '../../utils';

class HousesContainer extends React.Component {
  static propTypes = {
    searchActions: PropTypes.object.isRequired,
    houses: PropTypes.array.isRequired,
    minArea: PropTypes.number.isRequired,
    dealType: PropTypes.number.isRequired,
    buildingType: PropTypes.string.isRequired,
    maxPrice: PropTypes.number.isRequired,
    isFetching: PropTypes.bool.isRequired,
  };
  componentWillMount() {
    this.props.searchActions.searchHouses(
      this.props.minArea,
      this.props.dealType,
      this.props.buildingType,
      this.props.maxPrice,
    );
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.houses !== this.props.houses) {
      clearTimeout(this.timeout);

      // Optionally do something with data

      if (!nextProps.isFetching) {
        this.startPoll();
      }
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  handleSubmit = (minArea, dealType, buildingType, maxPrice) => {
    this.props.searchActions.searchHouses(
      minArea,
      dealType,
      buildingType,
      maxPrice,
    );
  };
  startPoll() {
    console.log(this.props.minArea);
    this.timeout = setTimeout(() => {
      this.props.searchActions.searchHouses(
        this.props.minArea,
        this.props.dealType,
        this.props.buildingType,
        this.props.maxPrice,
      );
    }, 25000);
  }
  render() {
    return (
      <HousesPage
        loading={this.props.isFetching}
        houses={this.props.houses}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    searchActions: bindActionCreators(searchActions, dispatch),
  };
}

function mapStateToProps(state) {
  const { search } = state;
  const {
    isFetching,
    houses,
    minArea,
    dealType,
    buildingType,
    maxPrice,
  } = search;
  return {
    isFetching,
    houses,
    minArea,
    dealType,
    buildingType,
    maxPrice,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HousesContainer);
