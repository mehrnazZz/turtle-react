import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as houseActions from '../../../actions/house';
import NewHouseForm from '../../../components/NewHouseForm';
import Loader from '../../../components/Loader';

class NewHouseContainer extends React.Component {
  static propTypes = {
    houseActions: PropTypes.object.isRequired,
    isFetching: PropTypes.bool.isRequired,
  };
  handleSubmit = (
    area,
    buildingType,
    address,
    dealType,
    basePrice,
    rentPrice,
    sellPrice,
    phone,
    description,
    expireTime,
  ) => {
    this.props.houseActions.createHouse(
      area,
      buildingType,
      address,
      dealType,
      basePrice,
      rentPrice,
      sellPrice,
      phone,
      description,
      expireTime,
      'http://uupload.ir/files/jzmo_no-pic.jpg',
    );
  };
  render() {
    return (
      <div>
        <NewHouseForm handleSubmit={this.handleSubmit} />
        <Loader loading={this.props.isFetching} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    houseActions: bindActionCreators(houseActions, dispatch),
  };
}

function mapStateToProps(state) {
  const { house } = state;
  const { isFetching } = house;
  return {
    isFetching,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewHouseContainer);
