import React from 'react';
import NewHouseContainer from './NewHouseContainer';
import Layout from '../../../components/Layout';

const title = 'اضافه کردن خانه';

function action() {
  return {
    chunks: ['new-house'],
    title,
    component: (
      <Layout>
        <NewHouseContainer  />,
      </Layout>
    ),
  };
}

export default action;
