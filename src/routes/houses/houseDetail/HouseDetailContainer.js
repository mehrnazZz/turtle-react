import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as houseActions from '../../../actions/house';
import * as authActions from '../../../actions/authentication';
import HousePage from '../../../components/HousePage';
import Loader from '../../../components/Loader';

class HouseDetailContainer extends React.Component {
  static propTypes = {
    houseActions: PropTypes.object.isRequired,
    authActions: PropTypes.object.isRequired,
    isFetching: PropTypes.bool.isRequired,
    houseId: PropTypes.string.isRequired,
    houseDetail: PropTypes.object.isRequired,
    houseMessage: PropTypes.string.isRequired,
    currUser: PropTypes.object.isRequired,
    phoneStatus: PropTypes.string.isRequired,
  };
  componentDidMount() {
    this.props.houseActions.getHouseDetail(
      this.props.houseId,
      location.pathname,
    );
  }
  handleGetPhone = () => {
    this.props.houseActions.getHousePhoneNumber(this.props.houseId);
  };
  render() {
    console.log(this.props.houseMessage);
    return (
      <div>
        <HousePage
          house={this.props.houseDetail}
          currUser={this.props.currUser}
          getPhone={this.handleGetPhone}
          houseMessage={this.props.houseMessage}
          phoneStatus={this.props.phoneStatus}
        />
        <Loader loading={this.props.isFetching} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    houseActions: bindActionCreators(houseActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch),
  };
}

function mapStateToProps(state) {
  const { house } = state;
  const { authentication } = state;
  const { isFetching, houseDetail, houseMessage, phoneStatus } = house;
  const { currUser } = authentication;
  return {
    isFetching,
    houseDetail,
    houseMessage,
    currUser,
    phoneStatus,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  HouseDetailContainer,
);
