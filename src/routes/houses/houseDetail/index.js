import React from 'react';
import HouseDetailContainer from './HouseDetailContainer';
import Layout from '../../../components/Layout';

const title = 'اضافه کردن خانه';

function action({ params: { houseId } }) {
  return {
    chunks: ['houseDetail'],
    title,
    component: (
      <Layout>
        <HouseDetailContainer houseId={houseId} />,
      </Layout>
    ),
  };
}

export default action;
