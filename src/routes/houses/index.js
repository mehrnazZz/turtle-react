import React from 'react';
import HousesContainer from './HousesContainer';
import Layout from '../../components/Layout';

const title = 'خانه ها';

function action() {
  return {
    chunks: ['houses'],
    title,
    component: (
      <Layout>
        <HousesContainer />,
      </Layout>
    ),
  };
}

export default action;
