import React from 'react';
import Layout from 'components/Layout';
import HomeContainer from './homeContainer';

const title = 'خانه';

function action() {
  return {
    chunks: ['home'],
    title,
    component: (
      <Layout mainPanel>
        <HomeContainer />
      </Layout>
    ),
  };
}

export default action;
