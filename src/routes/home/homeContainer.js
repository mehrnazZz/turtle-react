import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as searchActions from '../../actions/search';
import * as authActions from '../../actions/authentication';
import HomePage from '../../components/HomePage';
import Loader from '../../components/Loader';

class HomeContainer extends React.Component {
  static propTypes = {
    isFetching: PropTypes.bool.isRequired,
    houses: PropTypes.array.isRequired,
    searchActions: PropTypes.object.isRequired,
    authActions: PropTypes.object.isRequired,
    currUser: PropTypes.object.isRequired,
  };
  static defaultProps = {
    isFetching: false,
    loginError: null,
  };
  componentDidMount() {
    this.props.authActions.initiate();
  }
  handleSubmit = (minArea, dealType, buildingType, maxPrice) => {
    this.props.searchActions.searchHouses(
      minArea,
      dealType,
      buildingType,
      maxPrice,
    );
  };

  render() {
    console.log(this.props.currUser);
    return (
      <div>
        <HomePage
          loading={this.props.isFetching}
          handleSubmit={this.handleSubmit}
        />
        <Loader loading={this.props.isFetching} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    searchActions: bindActionCreators(searchActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch),
  };
}

function mapStateToProps(state) {
  const { search } = state;
  const { authentication } = state;
  const { isFetching, houses } = search;
  const { currUser } = authentication;
  return {
    isFetching,
    houses,
    currUser,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
