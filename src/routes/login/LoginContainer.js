import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as actions from 'actions/authentication';
import Login from 'components/Login';
import Loader from 'components/Loader';
import { notifyError } from '../../utils';
import loginErrorCodes from '../../constants/loginErrorCodes';
import history from '../../history';

class LoginContainer extends React.Component {
  static propTypes = {
    isFetching: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    loginError: PropTypes.object,
    actions: PropTypes.object.isRequired,
  };
  static defaultProps = {
    next: '/',
    isFetching: false,
    loginError: null,
  };

  componentWillMount() {
    if (this.props.isLoggedIn) {
      history.push('/');
    }
  }
  componentDidMount() {
    this.props.actions.initiateUsers();
  }
  componentWillReceiveProps(nextProps) {
    if (!nextProps.isFetching && nextProps.loginError) {
      let errorMessage = nextProps.loginError.message;
      if (loginErrorCodes[nextProps.loginError.code]) {
        errorMessage = loginErrorCodes[nextProps.loginError.code];
      }
      notifyError(errorMessage);
    }
  }
  handleSubmit = (username, password) => {
    this.props.actions.loginUser(username, password);
  };

  render() {
    return (
      <div>
        <Login
          loading={this.props.isFetching}
          handleSubmit={this.handleSubmit}
        />
        <Loader loading={this.props.isFetching} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { isFetching, isLoggedIn, loginError } = authentication;
  return {
    isFetching,
    isLoggedIn,
    loginError,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
