import React from 'react';
import LoginContainer from './LoginContainer';

const title = 'Log In';

function action({ query }) {
  return {
    chunks: ['root'],
    headScripts: ['https://www.google.com/recaptcha/api.js'],
    title,
    component: <LoginContainer next={query.next} />,
  };
}

export default action;
