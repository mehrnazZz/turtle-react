import {
  CREATE_HOUSE_REQUEST,
  CREATE_HOUSE_SUCCESS,
  CREATE_HOUSE_FAILURE,
  GET_HOUSE_FAILURE,
  GET_HOUSE_SUCCESS,
  GET_HOUSE_REQUEST,
  GET_HOUSE_PHONE_SUCCESS,
  GET_HOUSE_PHONE_REQUEST,
  GET_HOUSE_PHONE_FAILURE,
  GET_PHONE_STATUS_REQUEST,
  GET_PHONE_STATUS_SUCCESS,
  GET_PHONE_STATUS_FAILURE,
} from '../constants';

const initialState = {
  isFetching: false,
  errorMessage: null,
  houseDetail: {},
  houseMessage: '',
  phoneStatus: false,
  pathname: '',
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_HOUSE_REQUEST:
      return {
        ...state,
        errorMessage: null,
        isFetching: true,
      };
    case CREATE_HOUSE_SUCCESS:
      return {
        ...state,
        isFetching: false,
      };
    case CREATE_HOUSE_FAILURE:
      return {
        ...initialState,
        errorMessage: action.payload.err,
      };
    case GET_HOUSE_REQUEST:
      return {
        ...state,
        errorMessage: null,
        isFetching: true,
      };
    case GET_HOUSE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        houseDetail: action.payload.response,
        houseMessage: '',
        pathname: action.payload.pathname,
      };
    case GET_HOUSE_FAILURE:
      return {
        ...initialState,
        errorMessage: action.payload.err,
      };
    case GET_HOUSE_PHONE_REQUEST:
      return {
        ...state,
        errorMessage: null,
        isFetching: true,
      };
    case GET_HOUSE_PHONE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        houseMessage: action.payload.response,
      };
    case GET_HOUSE_PHONE_FAILURE:
      return {
        ...state,
        errorMessage: action.payload.err,
      };
    case GET_PHONE_STATUS_REQUEST:
      return {
        ...state,
        errorMessage: null,
        isFetching: true,
      };
    case GET_PHONE_STATUS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        phoneStatus: action.payload.response,
      };
    case GET_PHONE_STATUS_FAILURE:
      return {
        ...state,
        errorMessage: action.payload.err,
      };
    default:
      return state;
  }
}
