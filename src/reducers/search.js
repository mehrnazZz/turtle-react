import {
  GET_HOUSES_REQUEST,
  GET_HOUSES_SUCCESS,
  GET_HOUSES_FAILURE,
  POLL_HOUSES_FAILURE,
  POLL_HOUSES_SUCCESS,
  POLL_HOUSES_REQUEST,
  SET_SEARCH_OBJ_REQUEST,
  SET_SEARCH_OBJ_SUCCESS,
  SET_SEARCH_OBJ_FAILURE,
} from '../constants';

const initialState = {
  houses: [],
  isFetching: false,
  getHousesError: null,
  minArea: null,
  dealType: null,
  buildingType: null,
  maxPrice: null,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_HOUSES_REQUEST:
      return {
        ...state,
        searchError: null,
        isFetching: true,
      };
    case GET_HOUSES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        houses: action.payload.response,
      };
    case GET_HOUSES_FAILURE:
      return {
        ...initialState,
        getHousesError: action.payload.err,
      };
   case POLL_HOUSES_REQUEST:
      return {
        ...state,
        searchError: null,
        isFetching: true,
      };
    case POLL_HOUSES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        houses: action.payload.response,
      };
    case POLL_HOUSES_FAILURE:
      return {
        ...initialState,
        getHousesError: action.payload.err,
      };
    case SET_SEARCH_OBJ_REQUEST:
      return {
        ...state,
        searchError: null,
        isFetching: true,
      };
    case SET_SEARCH_OBJ_SUCCESS:
      console.log(action.payload);
      return {
        ...state,
        isFetching: false,
        minArea: action.payload.minArea,
        dealType: action.payload.dealType,
        buildingType: action.payload.buildingType,
        maxPrice: action.payload.maxPrice,
      };
    case SET_SEARCH_OBJ_FAILURE:
      return {
        ...initialState,
        getHousesError: action.payload.err,
      };
    default:
      return state;
  }
}
