import {
  GET_CURR_USER_FAILURE,
  GET_CURR_USER_REQUEST,
  GET_CURR_USER_SUCCESS,
  INCREASE_CREDIT_REQUEST,
  INCREASE_CREDIT_SUCCESS,
  INCREASE_CREDIT_FAILURE,
  GET_INIT_REQUEST,
  GET_INIT_SUCCESS,
  GET_INIT_FAILURE,
  GET_INIT_USER_REQUEST,
  GET_INIT_USER_SUCCESS,
  GET_INIT_USER_FAILURE,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
} from '../constants';

const initialState = {
  isLoggedIn: false,
  isFetching: false,
  loginError: null,
  firstName: '',
  currUser: null,
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case USER_LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case USER_LOGIN_SUCCESS:
      console.log(payload.info);
      return {
        ...state,
        isFetching: false,
        isLoggedIn: true,
        currUser: payload.info,
      };
    case USER_LOGIN_FAILURE:
      return {
        ...initialState,
        loginError: payload.error,
        isFetching: false,
      };
    case USER_LOGOUT_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case USER_LOGOUT_SUCCESS:
      return {
        ...initialState,
        logoutMessage: payload.message,
      };
    case USER_LOGOUT_FAILURE:
      return {
        ...state,
        isFetching: false,
        logoutError: payload.error,
      };
    case GET_CURR_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case GET_CURR_USER_SUCCESS:
      return {
        ...initialState,
        currUser: payload.response,
      };
    case GET_CURR_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case INCREASE_CREDIT_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case INCREASE_CREDIT_SUCCESS:
      return {
        ...initialState,
        currUser: payload.response,
      };
    case INCREASE_CREDIT_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case GET_INIT_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case GET_INIT_SUCCESS:
      return {
        ...state,
        isFetching: false,
      };
    case GET_INIT_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case GET_INIT_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case GET_INIT_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
      };
    case GET_INIT_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
}
