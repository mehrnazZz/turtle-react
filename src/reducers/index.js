import { combineReducers } from 'redux';
import runtime from './runtime';
import authentication from './authentication';
import search from './search';
import house from './house';

export default combineReducers({
  runtime,
  search,
  authentication,
  house,
});
