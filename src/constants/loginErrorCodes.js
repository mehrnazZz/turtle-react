export default {
  LOGIN_FAILED: 'نام کاربری یا گذرواژه اشتباه است',
  a: 'CAPTCHA کامل نشده است.',
  CAPTCHA_VERIFICATION_FAILED: 'خطا در CAPTCHA',
  UNKNOWN_ERROR: 'خطا در برقراری ارتباط با سرور',
};
