import _ from 'lodash';
import {
  fetchApi,
  getGetConfig,
  getPutConfig,
  getPostConfig,
  getLoginInfo,
} from '../utils';
import { getSearchHousesUrl, getInitiateUrl } from '../config/paths';
/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

export function sendSearchHousesRequest(
  minArea,
  dealType,
  buildingType,
  maxPrice,
) {
  return fetchApi(
    getSearchHousesUrl,
    getPostConfig({ minArea, dealType, buildingType, maxPrice }),
  );
}

export function sendpollHousesRequest(
  minArea,
  dealType,
  buildingType,
  maxPrice,
) {
  return fetchApi(
    getInitiateUrl,
    getPostConfig({ minArea, dealType, buildingType, maxPrice }),
  );
}
