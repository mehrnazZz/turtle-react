import { fetchApi, getGetConfig, getPostConfig } from '../utils';
import {
  getCurrentUserUrl,
  getInitiateUrl,
  loginApiUrl,
  increaseCreditUrl,
} from '../config/paths';

export function sendincreaseCreditRequest(amount, id) {
  return fetchApi(increaseCreditUrl(amount, id), getGetConfig());
}

export function sendgetCurrentUserRequest(userId) {
  return fetchApi(getCurrentUserUrl(userId), getGetConfig());
}

export function sendInitiateRequest() {
  return fetchApi(getInitiateUrl, getGetConfig());
}

export function sendInitiateUsersRequest() {
  return fetchApi(loginApiUrl, getGetConfig());
}

export function sendLoginRequest(username, password) {
  return fetchApi(
    loginApiUrl,
    getPostConfig({
      username,
      password,
    }),
  );
}
