import _ from 'lodash';
import { fetchApi, getPostConfig, getGetConfig } from '../utils';
import {
  getCreateHouseUrl,
  getHouseDetailUrl,
  getHousePhoneUrl,
} from '../config/paths';
/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

export function sendcreateHouseRequest(
  area,
  buildingType,
  address,
  dealType,
  basePrice,
  rentPrice,
  sellPrice,
  phone,
  description,
  expireTime,
  imageURL,
) {
  return fetchApi(
    getCreateHouseUrl,
    getPostConfig({
      area,
      buildingType,
      address,
      dealType,
      basePrice,
      rentPrice,
      sellPrice,
      phone,
      description,
      expireTime,
      imageURL,
    }),
  );
}

/**
 * @api {get} /v2/acquisition/driver/phoneNumber/:phoneNumber
 * @apiName DriverAcquisitionSearchByPhoneNumber
 * @apiVersion 2.0.0
 * @apiGroup Driver
 * @apiPermission FIELD-AGENT, TRAINER, ADMIN
 * @apiParam {String} phoneNumber phone number to search (+989123456789, 09123456789)
 * @apiSuccess {String} result API request result
 * @apiSuccessExample {json} Success-Response:
 {
   "result": "OK",
   "data": {
       "drivers": [
        {
          "id": 10,
          "fullName": "علیرضا قربانی"
        },
        {
          "id": 12,
          "fullName": "علیرضا قربانی"
        }
       ]
   }
 }
 */

export function sendgetHouseDetailRequest(houseId) {
  return fetchApi(getHouseDetailUrl(houseId), getGetConfig());
}

export function sendgetHousePhoneNumberRequest(houseId) {
  return fetchApi(getHousePhoneUrl(houseId), getPostConfig());
}

export function sendgetPhoneStatusRequest(houseId) {
  return fetchApi(getHousePhoneUrl(houseId), getGetConfig());
}
