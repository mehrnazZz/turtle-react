import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Loader.scss';
import gifFile from './loader.gif';

class Loader extends Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
  };
  render() {
    return (
      <div className={this.props.loading ? 'loader loading' : 'loader'}>
        <div>
          <img src={gifFile} alt="loader gif" />
        </div>
        <div className="loader-info">منتظر باشید</div>
      </div>
    );
  }
}

export default withStyles(s)(Loader);
