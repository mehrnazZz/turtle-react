/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './main.css';
import logo from './logo.png';
import featureIcon1 from './726499.svg';
import featureIcon2 from './726488.svg';
import featureIcon3 from './726446.svg';
import whyUsImg from './why-khanebedoosh.jpg';
import history from '../../history';
import SearchBox from '../SearchBox';

class HomePage extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
  };
  render() {
    return (
      <div className="home-panel">
        <div className="row">
          <div className="col-xs-12 slider-column">
            <div className="slider center-content">
              <div className="slide1" />
              <div className="slide2" />
              <div className="slide3" />
              <div className="slide4" />
              <div className="col-xs-12 col-sm-12 col-md-8 main-form">
                <div className="row hidden-sm visible-xs mobile-header">
                  <div className="flex-center col-md-2   nav-user col-xs-12 dropdown">
                    <div className="row flex-center header-main-boarder">
                      <div className="col-xs-9 ">
                        <h5 className="text-align-right persian ">
                          ناحیه کاربری
                        </h5>
                        <div className="dropdown-content">
                          <div className="container-fluid">
                            <div className="row">
                              <h4 className="persian text-align-right blue-font ">
                                بهنام همایون
                              </h4>
                            </div>
                            <div className="row">
                              <div className="col-xs-6 pull-left blue-font persian">
                                <span>۲۰۰۰۰</span>تومان
                              </div>
                              <div className="col-xs-6 pull-right blue-font persian text-align-right no-padding">
                                اعتبار
                              </div>
                            </div>
                            <div className="row mar-top">
                              <input
                                type="button"
                                onClick={() => {
                                  history.push('/increaseCredit');
                                }}
                                value="افزایش اعتبار"
                                className="  blue-button persian text-align-center"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="no-padding  col-xs-3">
                        <i className="fa fa-smile-o fa-2x white-font" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xs-12 logo-section">
                  <img src={logo} alt="logo" className="logo-image" />
                  <h2 className="main-title white font-medium">خانه به دوش</h2>
                </div>

                <SearchBox handleSubmit={this.props.handleSubmit} />
                <div className="col-xs-12 submit-home-link border-radius text-align-center">
                  <h5 className="submit-home-heading white">
                    صاحب خانه هستید؟ خانه خود را
                    <a
                      className="submit-house-button white"
                      onClick={() => {
                        history.push('/houses/new');
                      }}
                    >
                      ثبت
                    </a>
                    کنید.
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12 features-section center-content">
              <div className="col-xs-12 col-sm-12 col-md-8 features-container">
                <div className="col-xs-12 col-sm-12 col-md-3 feature-box text-align-center border-radius white-background center-column">
                  <div>
                    <img src={featureIcon1} alt="feature-icon" />
                    <h1>گسترده</h1>
                    <p dir="rtl">در منطقه مورد علاقه خود صاحب خانه شوید</p>
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-3 feature-box text-align-center border-radius white-background center-column">
                  <div>
                    <img src={featureIcon2} alt="feature-icon" />
                    <h1>مطمئن</h1>
                    <p dir="rtl">با خیال راحت به دنبال خانه بگردید</p>
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-3 feature-box text-align-center border-radius white-background center-column">
                  <div>
                    <img src={featureIcon3} alt="feature-icon" />
                    <h1>آسان</h1>
                    <p dir="rtl">به سادگی صاحب خانه شوید</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid why-us">
          <div className="row">
            <div className="col-xs-12 center-content">
              <div className="col-xs-12 col-sm-12 col-md-8 why-us-title text-align-right">
                <h1 className="reasons-head font-bold">چرا خانه به دوش؟</h1>
              </div>
            </div>
            <div className="col-xs-12 center-content">
              <div className="col-xs-12 col-sm-12 col-md-8">
                <div className="col-xs-12 col-sm-12 col-md-6 why-us-img-column">
                  <img src={whyUsImg} alt="why-img" className="why-us-img" />
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 reasons-section">
                  <ul className="why-list">
                    <li>
                      <i className="fa fa-check-circle purple" />اطلاعات کامل و
                      صحیح از املاک قابل معامله
                    </li>
                    <li>
                      <i className="fa fa-check-circle purple" />بدون محدودیت،
                      ۲۴ ساعته و در تمام ایام هفته
                    </li>
                    <li>
                      <i className="fa fa-check-circle purple" />جستجوی هوشمند
                      ملک، صرفه‌جویی در زمان
                    </li>
                    <li>
                      <i className="fa fa-check-circle purple" />تنوع در املاک،
                      افزایش قدرت انتخاب مشتریان
                    </li>
                    <li>
                      <i className="fa fa-check-circle purple" />بانکی جامع از
                      اطلاعات هزاران آگهی به روز
                    </li>
                    <li>
                      <i className="fa fa-check-circle purple" />دستیابی به
                      نتیجه مطلوب در کمترین زمان ممکن
                    </li>
                    <li>
                      <i className="fa fa-check-circle purple" />همکاری با
                      مشاوران متخصص در حوزه املاک
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(HomePage);
