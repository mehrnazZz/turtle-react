import React from 'react';
import InputMask from 'react-input-mask';

export default function(props) {
  return <InputMask {...props} mask="+\9\8 999 999 9999" maskChar="-" />;
}
