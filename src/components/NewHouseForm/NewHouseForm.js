/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './main.css';

class NewHouseForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      area: '',
      buildingType: '',
      address: '',
      dealType: 'RENT',
      basePrice: '',
      rentPrice: '',
      sellPrice: '',
      phone: '',
      description: '',
      expireTime: '',
      errors: false,
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    const errors = this.validate(
      this.state.buildingType,
      this.state.address,
      this.state.basePrice,
      this.state.rentPrice,
      this.state.sellPrice,
      this.state.phone,
      this.state.area,
    );
    const isEnabled = !Object.keys(errors).some(x => errors[x]);
    if (isEnabled) {
      this.setState({ errors: false });
      const dealType = this.state.dealType === 'RENT' ? 1 : 0;
      let rentPrice = 0;
      let sellPrice = 0;
      let basePrice = 0;
      if (dealType) {
        sellPrice = 0;
        rentPrice = this.state.rentPrice;
        basePrice = this.state.basePrice;
      } else {
        rentPrice = 0;
        basePrice = 0;
        sellPrice = this.state.sellPrice;
      }
      this.props.handleSubmit(
        this.state.area,
        this.state.buildingType,
        this.state.address,
        dealType,
        basePrice,
        rentPrice,
        sellPrice,
        this.state.phone,
        this.state.description,
        '',
      );
    } else {
      this.setState({ errors: true });
    }
  };

  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.handleSubmit(e);
    }
  };
  validate = (
    buildingType,
    address,
    basePrice,
    rentPrice,
    sellPrice,
    phone,
    area,
  ) => {
    // true means invalid, so our conditions got reversed
    const regex = new RegExp(/[^0-9]/, 'g');
    if (this.state.dealType === 'RENT') {
      return {
        buildingType: buildingType.length === 0,
        address: address.length === 0,
        basePrice: basePrice.match(regex),
        rentPrice: rentPrice.match(regex) || rentPrice.length === 0,
        sellPrice: false,
        phone: phone.match(regex) || phone.length === 0,
        area: area.match(regex) || area.length === 0,
      };
    }
    return {
      buildingType: buildingType.length === 0,
      address: !address.match(regex) || address.length === 0,
      basePrice: false,
      rentPrice: false,
      sellPrice: sellPrice.match(regex) || sellPrice.length === 0,
      phone: phone.match(regex) || phone.length === 0,
      area: area.match(regex) || area.length === 0,
    };
  };
  render() {
    const errors = this.validate(
      this.state.buildingType,
      this.state.address,
      this.state.basePrice,
      this.state.rentPrice,
      this.state.sellPrice,
      this.state.phone,
      this.state.area,
    );

    return (
      <div className="home-panel no-padding">
        <section>
          <div className="search-theme flex-center">
            <div className="container">
              <div className="row ">
                <div className="col-xs-12">
                  <h3 className="text-align-right register-headline persian-bold blue-font flex-center">
                    ثبت ملک جدید در خانه به دوش
                  </h3>
                </div>
              </div>
            </div>
          </div>
          <div className="container-fluid form-container">
            <div className="row radio">
              <div className="container-fluid pull-right">
                <label className="radio-inline  persian black-font radio-label">
                  <input
                    type="radio"
                    name="dealType"
                    id="inlineRadio1"
                    checked={this.state.dealType === 'RENT'}
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    value="RENT"
                  />{' '}
                  رهن و اجاره
                </label>
                <label className="radio-inline persian black-font radio-label">
                  <input
                    type="radio"
                    name="dealType"
                    id="inlineRadio2"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    value="SELL"
                  />{' '}
                  خرید
                </label>
              </div>
            </div>
            <div className="row flex-middle">
              <div className="col-xs-10 flex-middle-respon">
                <div className="col-xs-12 col-md-5">
                  <label
                    htmlFor="meter"
                    className="persian light-grey-font add-label"
                  >
                    متر مربع
                  </label>
                  <input
                    id="meter"
                    type="text"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    name="area"

                    className={`text-align-right persian black-font add-input ${
                      errors.area && this.state.errors ? 'error' : ''
                    }`}
                    placeholder="متراژ"
                  />
                  {errors.area &&
                    this.state.errors && (
                      <p className="err-msg">لطفا متراژ را به عدد وارد کنید.</p>
                    )}
                </div>
                <div className="col-xs-12  col-md-5">
                  <label
                    htmlFor="type-inp"
                    className="persian light-grey-font add-label empty-label-height"
                  />
                  <select
                    id="type-inp"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    name="buildingType"

                    className={`persian text-align-right light-grey-font add-input pull-left ${
                      errors.buildingType && this.state.errors ? 'error' : ''
                    }`}
                  >
                    <option disabled selected>
                      نوع ملک
                    </option>
                    <option>ویلایی</option>
                    <option>آپارتمان</option>
                  </select>
                  {errors.buildingType &&
                    this.state.errors && (
                      <p className="err-msg">لطفا نوع ملک را انتخاب کنید.</p>
                    )}
                </div>
              </div>
            </div>
            <div className="row flex-middle">
              <div className="col-xs-10 flex-middle-respon">
                {this.state.dealType === 'RENT' && (
                  <div className="col-xs-12 col-md-5">
                    <label
                      htmlFor="price1"
                      className="persian light-grey-font add-label"
                    >
                      متر مربع
                    </label>
                    <input
                      id="price1"
                      type="text"
                      onChange={e => {
                        this.setState({ [e.target.name]: e.target.value });
                      }}
                      name="basePrice"

                      className={`persian text-align-right black-font add-input ${
                        errors.basePrice && this.state.errors ? 'error' : ''
                      }`}
                      placeholder="قیمت رهن"
                    />
                    {errors.basePrice &&
                      this.state.errors && (
                        <p className="err-msg">لطفا قیمت رهن را به عدد کنید.</p>
                      )}
                  </div>
                )}
                {this.state.dealType === 'SELL' && (
                  <div className="col-xs-12 col-md-5">
                    <label
                      htmlFor="price1"
                      className="persian light-grey-font add-label"
                    >
                      متر مربع
                    </label>
                    <input
                      id="price1"
                      type="text"
                      onChange={e => {
                        this.setState({ [e.target.name]: e.target.value });
                      }}
                      name="sellPrice"

                      className={`persian text-align-right black-font add-input ${
                        errors.sellPrice && this.state.errors ? 'error' : ''
                      }`}
                      placeholder="قیمت فروش"
                    />
                    {errors.sellPrice &&
                      this.state.errors && (
                        <p className="err-msg">
                          لطفا قیمت فروش را به عدد کنید.
                        </p>
                      )}
                  </div>
                )}
                <div className="col-xs-12  col-md-5">
                  <label
                    htmlFor="address"
                    className="persian empty-label-height light-grey-font add-label"
                  />
                  <input
                    id="address"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    name="address"
                    type="text"

                    className={`persian text-align-right black-font add-input ${
                      errors.address && this.state.errors ? 'error' : ''
                    }`}
                    placeholder="آدرس"
                  />
                  {errors.address &&
                    this.state.errors && (
                      <p className="err-msg">لطفا آدرس را وارد کنید.</p>
                    )}
                </div>
              </div>
            </div>
            <div className="row flex-middle">
              <div className="col-xs-10 flex-middle-respon">
                {this.state.dealType === 'RENT' && (
                  <div className="col-xs-12 col-md-5">
                    <label
                      htmlFor="price2"
                      className="persian light-grey-font add-label"
                    >
                      متر مربع
                    </label>
                    <input
                      id="price2"
                      onChange={e => {
                        this.setState({ [e.target.name]: e.target.value });
                      }}
                      name="rentPrice"
                      type="text"

                      className={`persian text-align-right black-font add-input ${
                        errors.rentPrice && this.state.errors ? 'error' : ''
                      }`}
                      placeholder="قیمت اجاره"
                    />
                    {errors.rentPrice &&
                      this.state.errors && (
                        <p className="err-msg">لطفا قیمت اجاره را وارد کنید.</p>
                      )}
                  </div>
                )}
                <div className="col-xs-12  col-md-5">
                  <label
                    htmlFor="phone"
                    className="persian empty-label-height light-grey-font add-label"
                  />
                  <input
                    id="phone"
                    type="tel"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    name="phone"

                    className={`persian black-font  text-align-right  add-input ${
                      errors.phone && this.state.errors ? 'error' : ''
                    }`}
                    placeholder="شماره تماس"
                  />
                  {errors.phone &&
                    this.state.errors && (
                      <p className="err-msg">
                        لطفا شماره تماس را به عدد وارد کنید.
                      </p>
                    )}
                </div>
              </div>
            </div>
            <div className="row flex-middle">
              <div className="col-xs-10 flex-middle-respon">
                <div className="col-xs-12 col-md-10 ">
                  <label
                    htmlFor="expl"
                    className="persian light-grey-font add-label"
                  />
                  <input
                    id="expl"
                    type="text"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                    name="description"
                    className="persian text-align-right black-font add-input"
                    placeholder="توضیحات"
                  />
                </div>
              </div>
            </div>
            <div className="row flex-middle">
              <div className="col-xs-10 flex-middle-respon">
                <div className="col-xs-10 ">
                  <button
                    type="submit"
                    onClick={this.handleSubmit}
                    className="  blue-button  text-align-center persian white-font"
                  >
                    ثبت ملک
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(s)(NewHouseForm);
