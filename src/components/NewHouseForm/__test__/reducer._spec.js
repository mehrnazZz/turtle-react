import expect from 'expect';
import Auth from '../../../reducers/authentication';
import { USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS } from '../../../constants';

describe('auth reducer', () => {
  it('should return the initial state', () => {
    expect(Auth(undefined, {})).toEqual({
      info: {
        token: null,
        userId: null,
      },
      isLoggedIn: false,
      isFetching: false,
      loginError: null,
    });
  });

  it('should handle login', () => {
    expect(
      Auth(
        {},
        {
          type: USER_LOGIN_REQUEST,
        },
      ),
    ).toEqual({
      isFetching: true,
    });

    expect(
      Auth(
        {
          isFetching: true,
          isLoggedIn: false,
        },
        {
          type: USER_LOGIN_SUCCESS,
          payload: {
            info: {
              token: 'aaa',
              userId: 12321,
            },
          },
        },
      ),
    ).toEqual({
      info: {
        token: 'aaa',
        userId: 12321,
      },
      isLoggedIn: true,
      isFetching: false,
    });
  });
});
