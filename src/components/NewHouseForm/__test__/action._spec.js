import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import expect from 'expect';
import { USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS } from '../../../constants';
import { loginUser } from '../../../actions/authentication';
import * as paths from '../../../config/paths';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('auth actions', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('authenticates user when user login', () => {
    nock(paths.loginApiUrl)
      .post({ username: 'amir', password: 'amir' })
      .reply(200, {
        token: 'TOKEN_123',
        user: {
          id: 123,
          firstName: 'amir',
          lastName: 'ghaf',
        },
      });

    const expectedActions = [
      {
        type: USER_LOGIN_REQUEST,
        payload: { username: 'amir', password: 'amir' },
      },
      {
        type: USER_LOGIN_SUCCESS,
        payload: {
          info: {
            token: 'TOKEN_123',
            user: {
              id: 123,
              firstName: 'amir',
              lastName: 'ghaf',
            },
          },
        },
      },
    ];
    const store = mockStore();
    return store.dispatch(loginUser('amir', 'amir')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
