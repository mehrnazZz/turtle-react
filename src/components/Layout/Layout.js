/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import normalizeCss from 'normalize.css';
import s from './Layout.css';
import Footer from '../Footer';
import Panel from '../Panel';

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    mainPanel: PropTypes.bool,
  };
  static defaultProps = {
    mainPanel: false,
  };
  render() {
    return (
      <div className="col-md-12 col-xs-12" style={{ padding: '0' }}>
        <Panel mainPanel={this.props.mainPanel} />
        {this.props.children}
        <Footer />
      </div>
    );
  }
}

export default withStyles(normalizeCss, s)(Layout);
