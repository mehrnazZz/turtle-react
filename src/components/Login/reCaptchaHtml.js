import universalKeys from '../../config/universalKeys';

export default `<div class="g-recaptcha" style="width: 100%; height:100%;" data-theme="light" data-sitekey="${universalKeys.reCaptcha.siteKey}"></div>`;
