/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Col, Row } from 'react-bootstrap';
import s from './Login.scss';
import logo from './logo.png';
import reCaptchaHtml from './reCaptchaHtml';

class Login extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.handleSubmit(this.username.value, this.password.value);
    this.username.value = '';
    this.password.value = '';
  };

  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.handleSubmit();
    }
  };

  render() {
    const loginContainerClassName = this.props.loading ? 'login-panel-container loading' : 'login-panel-container';
    return (
      <div className={loginContainerClassName}>
        <div className="login-panel-background" />
        <Row>
          <Col xs={12} md={5} style={{ marginLeft: '28%' }}>
            <div className="login-panel">
              <div className="login-panel-body">
                <form id="loginForm" onSubmit={this.handleSubmit}>
                  <div className="login-panel-inputs">
                    {/*<img alt="boLOGO" style={{ maxWidth: '100px', marginBottom: '15%' }} src={logo} />*/}
                    <input
                      placeholder="نام کاربری"
                      id="username"
                      ref={input => {
                        this.username = input;
                      }}
                    />
                    <input
                      type="password"
                      placeholder="رمز عبور"
                      id="password"
                      ref={input => {
                        this.password = input;
                      }}
                      onKeyPress={this.handleKeyPress}
                    />
                  </div>
                  <div dangerouslySetInnerHTML={{ __html: reCaptchaHtml }} />
                  <input type="submit" className="submit-btn" value="ورود" />
                </form>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withStyles(s)(Login);
