/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './main.css';

class HousePage extends Component {
  static propTypes = {
    house: PropTypes.object.isRequired,
    currUser: PropTypes.object.isRequired,
    houseMessage: PropTypes.string.isRequired,
    getPhone: PropTypes.func.isRequired,
    phoneStatus: PropTypes.string.isRequired,
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.getPhone();
  };
  render() {
    console.log(this.props.houseMessage);
    return (
      <div className="home-panel">
        <section>
          <div className="search-theme flex-center">
            <div className="container">
              <div className="row ">
                <div className="col-xs-12">
                  <h3 className="text-align-right persian-bold blue-font flex-center">
                    مشخصات کامل ملک{' '}
                  </h3>
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-md-7 house-img-mar">
                <div className="row">
                  <img
                    alt="house-img"
                    src={this.props.house.imageURL}
                    className="img house-img"
                  />
                </div>
                <div className="row flex-middle">
                  {!this.props.phoneStatus &&
                    this.props.houseMessage === 'no_credit' && (
                      <div className="search-button persian text-align-center padd border-radius clear-input black-font yellow-background phone-display">
                        اعتبار شما برای مشاهده شماره مالک\مشاور این ملک کافی
                        نیست
                      </div>
                    )}

                  {!this.props.phoneStatus &&
                    this.props.houseMessage !== 'no_credit' &&
                    this.props.houseMessage !== 'payed' && (
                      <button
                        onClick={this.handleSubmit}
                        type="submit"
                        className="padd search-button persian has-transition text-align-center padd border-radius clear-input white light-blue-background phone-display"
                      >
                        مشاهده شماره مالک\مشاور
                      </button>
                    )}
                </div>
              </div>
              <div className="col-xs-12 col-md-4 house-info pull-right">
                <div className="row">
                  <div className="white-font col-xs-5 col-md-3 pull-right text-align-center purple-background persian info-label">
                    {this.props.house.dealType === 1 ? 'اجاره' : 'فروش'}
                  </div>
                </div>
                <div className="row info-lines">
                  <div className=" text-align-right col-xs-7 persian black-font">
                    {this.props.phoneStatus ||
                    this.props.houseMessage === 'payed'
                      ? this.props.house.phone
                      : '۰۹۲۳۳***۲۳'}
                  </div>
                  <div className=" text-align-right col-xs-5 persian light-grey-font">
                    شماره مالک
                  </div>
                </div>
                <div className="row info-lines">
                  <div className="text-align-right col-xs-7 persian black-font">
                    {this.props.house.buildingType}
                  </div>
                  <div className="text-align-right col-xs-5 persian light-grey-font">
                    نوع ساختمان
                  </div>
                </div>
                {/* <div className="row info-lines"> */}
                {/* <div className="col-xs-7 persian black-font"> */}
                {/* ۲۰۰۰۰۰۰ <span className="persian black-font">تومان</span> */}
                {/* </div> */}
                {/* <div className="col-xs-5 persian light-grey-font">رهن</div> */}
                {/* </div> */}
                {this.props.house.dealType === 1 && (
                  <div className="row info-lines">
                    <div className="text-align-right col-xs-7 persian black-font">
                      {this.props.house.rentPrice}{' '}
                      <span className="persian black-font">تومان</span>
                    </div>
                    <div className="text-align-right col-xs-5 persian light-grey-font">
                      اجاره
                    </div>
                  </div>
                )}
                {this.props.house.dealType === 0 && (
                  <div className="row info-lines">
                    <div className="text-align-right col-xs-7 persian black-font">
                      {this.props.house.sellPrice}{' '}
                      <span className="persian black-font">تومان</span>
                    </div>
                    <div className="text-align-right col-xs-5 persian light-grey-font">
                      قیمت
                    </div>
                  </div>
                )}
                <div className="row info-lines">
                  <div className="text-align-right col-xs-7 persian black-font">
                    {this.props.house.address}
                  </div>
                  <div className="text-align-right col-xs-5 persian light-grey-font">
                    آدرس
                  </div>
                </div>
                <div className="row info-lines">
                  <div className="text-align-right col-xs-7 persian black-font">
                    {this.props.house.area}{' '}
                    <span className="persian black-font">متر</span>
                  </div>
                  <div className="text-align-right col-xs-5 persian light-grey-font">
                    متراژ
                  </div>
                </div>
                <div className="row info-lines">
                  <div className="text-align-right col-xs-7 persian black-font">
                    {this.props.house.description}
                  </div>
                  <div className="text-align-right col-xs-5 persian light-grey-font">
                    توضیحات
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(s)(HousePage);
