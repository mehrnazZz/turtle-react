import universalKeys from '../../config/universalKeys';

export default `<div class="g-recaptcha" data-size="compact" data-sitekey="${
  universalKeys.reCaptcha.siteKey
}"></div>`;
