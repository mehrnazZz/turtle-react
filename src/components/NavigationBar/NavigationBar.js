import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import s from './NavigationBar.scss';
import { logoutUser } from '../../actions/authentication';

class NavigationBar extends Component {
  static propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired,
  };
  logout = () => {
    this.props.actions.logoutUser();
  };

  render() {
    const cred = `${this.props.firstName} ${this.props.lastName}`;
    return (
      <div className="navigation-bar">
        <div className="col-md-6 col-md-push-3 col-lg-6 col-lg-push-3 col-sm-8 col-sm-push-2 col-xs-12">
          <div className="navigation-bar-item" onClick={this.logout}>
            خروج
          </div>
          <div className="navigation-bar-name">{cred}</div>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ logoutUser }, dispatch),
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { lastName, firstName } = authentication;
  return {
    firstName,
    lastName,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(NavigationBar),
);
