/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import twitterIcon from './Twitter_bird_logo_2012.svg.png';
import instagramIcon from './200px-Instagram_logo_2016.svg.png';
import telegramIcon from './200px-Telegram_logo.svg.png';
import s from './main.css';

class Footer extends Component {
  render() {
    return (
      <footer className="site-footer center-column dark-green-background white">
        <div className="row">
          <div className="col-xs-12 col-sm-4 text-align-left">
            <ul className="social-icons-list no-list-style">
              <li>
                <img
                  src={instagramIcon}
                  className="social-network-icon"
                  alt="instagram-icon"
                />
              </li>
              <li>
                <img
                  src={twitterIcon}
                  className="social-network-icon"
                  alt="twitter-icon"
                />
              </li>
              <li>
                <img
                  src={telegramIcon}
                  className="social-network-icon"
                  alt="telegram-icon"
                />
              </li>
            </ul>
          </div>
          <div className="col-xs-12 col-sm-8 text-align-right">
            <p>
              تمامی حقوق این وب‌سایت متعلق به مهرناز ثابت و هومن شریف زاده
              می‌باشد.
            </p>
          </div>
        </div>
      </footer>
    );
  }
}

export default withStyles(s)(Footer);
