/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './main.css';

class IncreaseCreditForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    currUser: PropTypes.object.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      errors: false,
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.amount);
    const isEnabled = !Object.keys(errors).some(x => errors[x]);
    if (isEnabled) {
      this.setState({ errors: false });
      this.props.handleSubmit(this.state.amount);
    } else {
      this.setState({ errors: true });
    }
  };

  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.handleSubmit(e);
    }
  };
  validate = amount => {
    // true means invalid, so our conditions got reversed
    const regex = new RegExp(/[^0-9]/, 'g');
    return {
      amount: amount.match(regex) || amount.length === 0,
    };
  };
  render() {
    const errors = this.validate(this.state.amount);
    return (
      <div className="home-panel">
        <section>
          <div className="search-theme flex-center">
            <div className="container">
              <div className="row ">
                <div className="col-xs-12">
                  <h3 className="text-align-right persian-bold blue-font flex-center">
                    افزایش موجودی
                  </h3>
                </div>
              </div>
            </div>
          </div>
          <div className="container fund-height">
            <div className="row">
              <div className="col-xs-12 col-md-5 inc-fund">
                <div className="row">
                  <label
                    htmlFor="inc-fund-inp"
                    className="persian light-grey-font add-label "
                  >
                    تومان
                  </label>
                  <input
                    placeholder="  مبلغ مورد نظر  "
                    id="inc-fund-inp"

                    className={`text-align-right persian black-font add-input ${
                      errors.amount && this.state.errors ? 'error' : ''
                    }`}
                    type="text"
                    name="amount"
                    onChange={e => {
                      this.setState({ [e.target.name]: e.target.value });
                    }}
                  />
                  {errors.amount &&
                    this.state.errors && (
                      <p className="err-msg">لطفا عدد وارد کنید.</p>
                    )}
                </div>
                <div className="row flex-middle">
                  <button
                    onClick={this.handleSubmit}
                    type="submit"
                    className=" white-font blue-button persian text-align-center inc-fund-butt"
                  >
                  افزایش اعتبار
                  </button>
                </div>
              </div>
              <div className="col-xs-12 col-md-5 curr-fund flex-middle">
                <span className="persian light-grey-font"> تومان</span> &nbsp;
                {this.props.currUser.balance} &nbsp;{' '}
                <span className="persian light-grey-font">اعتبار کنونی</span>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(s)(IncreaseCreditForm);
