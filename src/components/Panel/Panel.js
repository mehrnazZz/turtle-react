/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import * as authActions from '../../actions/authentication';
import s from './main.css';
import logoImg from './logo.png';
import history from '../../history';

class Panel extends Component {
  static propTypes = {
    currUser: PropTypes.object.isRequired,
    authActions: PropTypes.object.isRequired,
    mainPanel: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
  };

  render() {
    console.log(this.props.currUser);
    console.log(this.props.isLoggedIn);
    if (this.props.mainPanel) {
      return (
        <header className="hidden-xs">
          <div className="container-fluid">
            <div className="row header-main">
              <div className="flex-center col-md-2   nav-user col-xs-12 dropdown ">
                <div className="container">
                  <div className="row flex-center header-main-boarder">
                    <div className="col-xs-9 ">
                      <h5 className="text-align-right white-font persian ">
                        ناحیه کاربری
                      </h5>
                      <div className="dropdown-content">
                        <div className="container-fluid">
                          {this.props.currUser && (
                            <div>
                              <div className="row">
                                <h4 className="persian text-align-right black-font ">
                                  {this.props.currUser.username}
                                </h4>
                              </div>
                              <div className="row">
                                <div className="col-xs-6 pull-left persian light-grey-font">
                                  <span className="light-grey-font">
                                    تومان
                                    {this.props.currUser.balance}
                                  </span>
                                </div>
                                <div className="col-xs-6 light-grey-font pull-right  persian text-align-right no-padding">
                                  اعتبار
                                </div>
                              </div>
                              <div className="row mar-top">
                                <input
                                  type="button"
                                  value="افزایش اعتبار"
                                  onClick={() => {
                                    history.push('/increaseCredit');
                                  }}
                                  className="  white-font  blue-button persian text-align-center"
                                />
                              </div>
                              <div className="row mar-top">
                                <input
                                  type="button"
                                  value="خروج"
                                  onClick={() => {
                                    this.props.authActions.logoutUser();
                                  }}
                                  className="  white-font  blue-button persian text-align-center"
                                />
                              </div>
                            </div>
                          )}
                          {!this.props.currUser && (
                            <div className="row mar-top">
                              <input
                                type="button"
                                value="ورود"
                                onClick={() => {
                                  history.push('/login');
                                }}
                                className="  white-font  blue-button persian text-align-center"
                              />
                            </div>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="no-padding  col-xs-3">
                      <i className="fa fa-smile-o fa-2x white-font" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      );
    }
    return (
      <header>
        <div className="container-fluid">
          <div className="row nav-cl">
            <div className="flex-center col-md-2   nav-user col-xs-12 dropdown">
              <div className="container">
                <div className="row flex-center ">
                  <div className="col-xs-9 ">
                    <h5 className="text-align-right persian purple-font">
                      ناحیه کاربری
                    </h5>
                    <div className="dropdown-content">
                      <div className="container-fluid">
                        {this.props.currUser && (
                          <div>
                            <div className="row">
                              <h4 className="black-font persian text-align-right blue-font">
                                {this.props.currUser.username}
                              </h4>
                            </div>
                            <div className="row">
                              <div className=" light-grey-font col-xs-6 pull-left blue-font persian">
                                <span className="light-grey-font">
                                  تومان
                                  {this.props.currUser.balance}
                                </span>
                              </div>
                              <div className="light-grey-font col-xs-6 pull-right blue-font persian text-align-right no-padding">
                                اعتبار
                              </div>
                            </div>
                            <div className="row mar-top">
                              <input
                                type="button"
                                value="افزایش اعتبار"
                                className="white-font  blue-button persian text-align-center"
                                onClick={() => {
                                  history.push('/increaseCredit');
                                }}
                              />
                            </div>
                            <div className="row mar-top">
                              <input
                                type="button"
                                value="خروج"
                                onClick={() => {
                                  this.props.authActions.logoutUser();
                                }}
                                className="  white-font  blue-button persian text-align-center"
                              />
                            </div>
                          </div>
                        )}
                        {!this.props.currUser && (
                          <div className="row mar-top">
                            <input
                              type="button"
                              value="ورود"
                              onClick={() => {
                                history.push('/login');
                              }}
                              className="  white-font  blue-button persian text-align-center"
                            />
                          </div>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="no-padding  col-xs-3">
                    <i className="fa fa-smile-o fa-2x purple-font" />
                  </div>
                </div>
              </div>
            </div>
            <div className="flex-center col-md-4 col-md-offset-6 nav-logo col-xs-12">
              <div className="container">
                <div className="row  ">
                  <a
                    className="flex-row-rev"
                    onClick={() => {
                      history.push('/');
                    }}
                  >
                    <div className="no-padding text-align-center col-xs-3">
                      <img className="icon-cl" src={logoImg} alt="logo" />
                    </div>
                    <div className="col-xs-9">
                      <h4 className="persian-bold text-align-right  persian blue-font">
                        خانه به دوش
                      </h4>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
  };
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { currUser,isLoggedIn } = authentication;
  return {
    currUser,
    isLoggedIn,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(Panel),
);
