/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './main.css';

class SearchBox extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      maxPrice: '',
      minArea: '',
      buildingType: '',
      dealType: '',
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.handleSubmit(
      this.state.minArea,
      this.state.dealType,
      this.state.buildingType,
      this.state.maxPrice,
    );
  };
  validate = (maxPrice, minArea) => {
    // true means invalid, so our conditions got reversed
    const regex = new RegExp(/[^0-9]/, 'g');
    return {
      maxPrice: maxPrice.match(regex),
      minArea: minArea.match(regex),
    };
  };
  render() {
    const errors = this.validate(this.state.maxPrice, this.state.minArea);
    const isEnabled = !Object.keys(errors).some(x => errors[x]);

    return (
      <div className="col-xs-12 search-section border-radius no-padding">
        <div className="col-xs-12 first-part no-padding">
          <div className="col-xs-12 col-sm-4">
            <label className="label">متر مربع</label>
            <input
              placeholder="حداقل متراژ"
              onChange={e => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              name="minArea"
              className={`search-input border-radius clear-input ${
                errors.minArea ? 'error' : ''
              }`}
              type="text"
              dir="rtl"
            />
            {errors.minArea && <p className="err-msg">لطفا عدد وارد کنید.</p>}
          </div>
          <div className="col-xs-12 col-sm-4">
            <label className="label">تومان</label>
            <input
              placeholder="حداکثر قیمت"
              onChange={e => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              name="maxPrice"
              className={`search-input border-radius clear-input ${
                errors.maxPrice ? 'error' : ''
              }`}
              type="text"
              dir="rtl"
            />
            {errors.maxPrice && <p className="err-msg">لطفا عدد وارد کنید.</p>}
          </div>
          <div className="col-xs-12 col-sm-4">
            <label className="building-type-label" />
            <select
              title="buildingType"
              onChange={e => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              name="buildingType"
              className="search-input border-radius clear-input"
            >
              <option disabled selected className="select-place-holder">
                نوع ملک
              </option>
              <option>آپارتمان </option>
              <option>ویلایی</option>
            </select>
          </div>
        </div>
        <div className="col-xs-12 second-part">
          <div className="visible-md  visible-lg col-md-6 no-padding">
            <form onSubmit={this.handleSubmit}>
              <button
                disabled={!isEnabled}
                type="submit"
                className="search-button has-transition border-radius clear-input white light-blue-background"
              >
                جستجو
              </button>
            </form>
          </div>
          <div className="col-xs-12 col-md-6 deal-type-section no-padding">
            <label htmlFor="rahn" className="label font-medium input-label">رهن و اجاره</label>
            <input id="kharid"
              value={1}
              onChange={e => {
                this.setState({
                  [e.target.name]: e.target.checked ? 0 : 1,
                });
              }}
              className="white font-light"
              type="radio"
              name="dealType"
            />
            <label htmlFor="kharid" className="label font-medium input-label">خرید</label>
            <input id="rahn"
              className="white font-light"
              type="radio"
              value={0}
              onChange={e => {
                this.setState({
                  [e.target.name]: e.target.checked ? 1 : 0,
                });
              }}
              name="dealType"
            />
          </div>
          <div className="visible-xs visible-sm hidden-md hidden-lg col-xs-12">
            <form onSubmit={this.handleSubmit}>
              <button
                type="submit"
                className="search-button has-transition border-radius clear-input white light-blue-background"
              >
                جستجو
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(SearchBox);
