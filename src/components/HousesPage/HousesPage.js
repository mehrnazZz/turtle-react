/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './main.css';
import logo from './logo.png';
import history from '../../history';
import SearchBox from '../SearchBox';

class HousesPage extends Component {
  static propTypes = {
    houses: PropTypes.array.isRequired,
    handleSubmit: PropTypes.func.isRequired,
  };
  componentWillReceiveProps(nextProps){
    console.log(nextProps);
  }
  getPriceComponent = house => {
    if (house.dealType) {
      return (
        <div className="col-xs-12">
          <div className="col-xs-6 flex-center flex-row-rev padding-right-price ">
            <h4 className="persian black-font margin-1 price-heading">
              رهن <span className="margin-1">{house.basePrice}</span>
              <span className="persian light-grey-font margin-1"> تومان </span>
            </h4>
          </div>
          <div className="col-xs-6 flex-center flex-row-rev ">
            <h4 className="persian black-font margin-1 price-heading">
              اجاره <span className="margin-1">{house.rentPrice}</span>
              <span className="persian light-grey-font margin-1"> تومان </span>
            </h4>
          </div>
        </div>
      );
    }
    return (
      <div className="col-xs-7 flex-center flex-row-rev padding-right-price">
        <h4 className="persian black-font margin-1 price-heading">
          قیمت
          <span className="margin-1">{house.sellPrice}</span>
          <span className="persian light-grey-font margin-1">تومان</span>
        </h4>
      </div>
    );
  };
  render() {
    return (
      <div className="home-panel">
        <section>
          <div className="search-theme flex-center">
            <div className="container">
              <div className="row ">
                <div className="col-xs-12">
                  <h3 className="text-align-right persian-bold blue-font flex-center">
                    نتایج جستجو
                  </h3>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <h4 className="persian grey-font text-align-center mar-5">
                برای مشاهده اطلاعات بیشتر درباره ی هر ملک روی آن کلیک کنید
              </h4>
            </div>
            <div className="row ">
              {this.props.houses.map(house => (
                <div
                  onClick={() => {
                    history.push(`/houses/${house.id}`);
                  }}
                  className="col-xs-12  col-md-5 no-padding house-card border-radius"
                >
                  <div className="container-fluid img-height">
                    <div className="row house-img img-border-radius  ">
                      <img className="img-tag" src={house.imageURL} alt="house-img" />
                      <div className="col-md-4 white-font mar-top-10 col-xs-8 border-radius">
                        <h6
                          className={`mar-5 white-font padd-5 persian text-align-center border-radius ${
                            house.dealType === 1
                              ? 'pink-background'
                              : 'purple-background'
                          }`}
                        >
                          {house.dealType === 1 ? 'اجاره' : 'فروش'}
                        </h6>
                      </div>
                    </div>
                    <div className="row border-bottom">
                      <div className="col-xs-6 flex-center text-align-left">
                        <h4 className="persian black-font">
                          <i className="fa fa-map-marker purple-font" />
                          &nbsp;
                          {house.address}
                          &nbsp;
                        </h4>
                      </div>
                      <div className="col-xs-6 pull-right flex-center flex-row-rev">
                        <h4 className="persian black-font text-align-right">
                          {house.area} متر مربع
                        </h4>
                      </div>
                    </div>
                    <div className="row flex-row-rev">
                      {this.getPriceComponent(house)}
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="row mar-top">
              <div className="col-xs-12">
                <h4 className="persian light-grey-font text-align-center">
                  جستجوی مجدد
                </h4>
              </div>
              <div className="col-xs-12 flex-center mar-bottom no-padding">
                <div className="col-xs-12 main-form main-form-search-result no-padding">
                  <SearchBox handleSubmit={this.props.handleSubmit} />
                  <div className="col-xs-12 submit-home-link border-radius text-align-center">
                    <h5 className="submit-home-heading white">
                      صاحب خانه هستید؟ خانه خود را
                      <a
                        className="submit-house-button white"
                        onClick={() => {
                          history.push('/houses/new');
                        }}
                      >
                        ثبت
                      </a>
                      کنید.
                    </h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(s)(HousesPage);
