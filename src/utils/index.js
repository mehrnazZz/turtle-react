import _ from 'lodash';
import EXIF from 'exif-js';
import history from '../history';
import compressionConfig from '../config/compressionConfig';
import { loadLoginStatus } from '../actions/authentication';

const compressionFirstLevel = compressionConfig.getLevel(1);
const compressionSecondLevel = compressionConfig.getLevel(2);
const documentsCompressionLevel = {
  profilePicture: compressionFirstLevel,
  driverLicence: compressionFirstLevel,
  carIdCard: compressionFirstLevel,
  insurance: compressionSecondLevel,
};

function getCompressionLevel(documentType) {
  return documentsCompressionLevel[documentType] || compressionFirstLevel;
}

function dataURLtoBlob(dataURL, fileType) {
  const binary = atob(dataURL.split(',')[1]);
  const array = [];
  const length = binary.length;
  let i;
  for (i = 0; i < length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: fileType });
}

function getScaledImageMeasurements(width, height, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH } = getCompressionLevel(documentType);

  if (width > height) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  } else {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
  return {
    height,
    width,
  };
}

function getImageOrientation(image) {
  let orientation = 0;
  EXIF.getData(image, function() {
    orientation = EXIF.getTag(this, 'Orientation');
  });
  return orientation;
}

function getImageMeasurements(image) {
  const orientation = getImageOrientation(image);
  let transform;
  let swap = false;
  switch (orientation) {
    case 8:
      swap = true;
      transform = 'left';
      break;
    case 6:
      swap = true;
      transform = 'right';
      break;
    case 3:
      transform = 'flip';
      break;
    default:
  }
  const width = swap ? image.height : image.width;
  const height = swap ? image.width : image.height;
  return {
    width,
    height,
    transform,
  };
}

function getScaledImageFileFromCanvas(
  image,
  width,
  height,
  transform,
  fileType,
) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const context = canvas.getContext('2d');
  let transformParams;
  let swapMeasure = false;
  let imageWidth = width;
  let imageHeight = height;
  switch (transform) {
    case 'left':
      transformParams = [0, -1, 1, 0, 0, height];
      swapMeasure = true;
      break;
    case 'right':
      transformParams = [0, 1, -1, 0, width, 0];
      swapMeasure = true;
      break;
    case 'flip':
      transformParams = [1, 0, 0, -1, 0, height];
      break;
    default:
      transformParams = [1, 0, 0, 1, 0, 0];
  }
  context.setTransform(...transformParams);
  if (swapMeasure) {
    imageHeight = width;
    imageWidth = height;
  }
  context.drawImage(image, 0, 0, imageWidth, imageHeight);
  context.setTransform(1, 0, 0, 1, 0, 0);
  const dataURL = canvas.toDataURL(fileType);
  return dataURLtoBlob(dataURL, fileType);
}

function shouldResize(imageMeasurements, fileSize, documentType) {
  const { MAX_HEIGHT, MAX_WIDTH, DOCUMENT_SIZE } = getCompressionLevel(
    documentType,
  );
  if (documentType === 'insurance' && fileSize < DOCUMENT_SIZE) {
    return false;
  } else if (fileSize < DOCUMENT_SIZE) {
    return false;
  }
  return (
    imageMeasurements.width > MAX_WIDTH || imageMeasurements.height > MAX_HEIGHT
  );
}

async function processFile(dataURL, fileSize, fileType, documentType) {
  const image = new Image();
  image.src = dataURL;
  return new Promise((resolve, reject) => {
    image.onload = function() {
      const imageMeasurements = getImageMeasurements(image);
      if (!shouldResize(imageMeasurements, fileSize, documentType)) {
        resolve(dataURLtoBlob(dataURL, fileType));
      }
      const scaledImageMeasurements = getScaledImageMeasurements(
        imageMeasurements.width,
        imageMeasurements.height,
        documentType,
      );
      const scaledImageFile = getScaledImageFileFromCanvas(
        image,
        scaledImageMeasurements.width,
        scaledImageMeasurements.height,
        imageMeasurements.transform,
        fileType,
      );
      resolve(scaledImageFile);
    };
    image.onerror = reject;
  });
}

export function compressFile(file, documentType = 'default') {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = async function() {
        const compressedFile = await processFile(
          reader.result,
          file.size,
          file.type,
          documentType,
        );
        resolve(compressedFile);
      };
    } catch (err) {
      reject(err);
    }
  });
}

export function getLoginInfo() {
  try {
    const loginInfo = localStorage.getItem('loginInfo');
    if (_.isString(loginInfo)) {
      return JSON.parse(loginInfo);
    }
  } catch (err) {
    console.log('Error when parsing loginInfo object: ', err);
  }
  return null;
}
export function getPostConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo
    ? {
        'Content-Type': 'application/json',
        mimeType: 'application/json',
        Authorization: `Bearer ${loginInfo.token}`,
      }
    : { 'Content-Type': 'application/json', mimeType: 'application/json' };
  return {
    method: 'POST',
    // mode: 'no-cors',
    headers,
    ...(_.isEmpty(body) ? {} : { body: JSON.stringify(body) }),
  };
}

export function getPutConfig(body) {
  const loginInfo = getLoginInfo();
  const headers = loginInfo
    ? {
        'Content-Type': 'application/json',
        'X-Authorization': loginInfo.token,
      }
    : { 'Content-Type': 'application/json' };
  return {
    method: 'PUT',
    headers,
    ...(_.isEmpty(body) ? {} : { body: JSON.stringify(body) }),
  };
}

export function getGetConfig() {
  const loginInfo = getLoginInfo();
  const headers = loginInfo
    ? {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${loginInfo.token}`,
      }
    : { 'Content-Type': 'application/json' };
  return {
    method: 'GET',
    headers,
  };
}

export function convertNumbersToPersian(str) {
  const persianNumbers = '۰۱۲۳۴۵۶۷۸۹';
  let result = '';
  if (!str && str !== 0) {
    return result;
  }
  str = str.toString();
  let convertedChar;
  for (let i = 0; i < str.length; i++) {
    try {
      convertedChar = persianNumbers[str[i]];
    } catch (err) {
      console.log(err);
      convertedChar = str[i];
    }
    result += convertedChar;
  }
  return result;
}

export async function fetchApi(url, config) {
  let flowError;
  try {
    const res = await fetch(url, config);
    console.log(res);
    console.log(res.status);
    let result = await res.json();
    console.log(result);
    if (!_.isEmpty(_.get(result, 'data'))) {
      result = result.data;
    }
    console.log(res.status);

    if (res.status !== 200) {
      flowError = result;
      console.log(res.status);

      if (res.status === 403) {
        localStorage.clear();
        flowError.message = 'لطفا دوباره وارد شوید!';
        console.log('yeees');
        setTimeout(() => history.push('/login'), 3000);
      }
    } else {
      return result;
    }
  } catch (err) {
    console.log(err);
    console.debug(`Error when fetching api: ${url}`, err);
    flowError = {
      code: 'UNKNOWN_ERROR',
      message: 'خطای نامشخص لطفا دوباره سعی کنید',
    };
  }
  if (flowError) {
    throw flowError;
  }
}

export async function setImmediate(fn) {
  return new Promise(resolve => {
    setTimeout(() => {
      let value = null;
      if (_.isFunction(fn)) {
        value = fn();
      }
      resolve(value);
    }, 0);
  });
}

export function setCookie(name, value, days = 7) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
export function getCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  const length = ca.length;
  let i = 0;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

export function eraseCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

export function notifyError(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '10000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp',
  };
  toastr.error(message, 'خطا');
}

export function notifySuccess(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'slideDown',
    hideMethod: 'slideUp',
  };
  toastr.success(message, '');
}

export function reloadSelectPicker() {
  try {
    $('.selectpicker').selectpicker('render');
  } catch (err) {
    console.log(err);
  }
}

export function loadSelectPicker() {
  try {
    $('.selectpicker').selectpicker({});
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)
    ) {
      $('.selectpicker').selectpicker('mobile');
    }
  } catch (err) {
    console.log(err);
  }
}

export function getFormData(data) {
  if (_.isEmpty(data)) {
    throw Error('Invalid input data to create a FormData object');
  }
  let formData;
  try {
    formData = new FormData();
    const keys = _.keys(data);
    let i;
    const length = keys.length;
    for (i = 0; i < length; i++) {
      formData.append(keys[i], data[keys[i]]);
    }
    return formData;
  } catch (err) {
    console.debug(err);
    throw Error('FORM_DATA_BROWSER_SUPPORT_FAILURE');
  }
}

export function loadUserLoginStatus(store) {
  const loginStatusAction = loadLoginStatus();
  store.dispatch(loginStatusAction);
}
