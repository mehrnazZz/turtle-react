import ReactGA from 'react-ga';

const debug = process.env.NODE_ENV === 'development';

const excludeRegExp = /###/;

const analytics = {
  sendEvent(category, action, label, value) {
    if (analytics.isGAEnabled()) {
      try {
        ReactGA.event({
          category,
          action,
          label,
          value,
          nonInteraction: true,
        });
      } catch (err) {
        console.debug('Error when sending analytics events: ', err);
      }
    }
  },

  isGAEnabled() {
    if (process.env.BROWSER) {
      return !location.href.match(excludeRegExp);
    }
    return false;
  },

  startTracking() {
    if (analytics.isGAEnabled()) {
      try {
        ReactGA.initialize('UA-113061296-1', { debug });
        ReactGA.pageview(window.location.pathname + window.location.search);
      } catch (err) {
        console.debug(
          'Error when calling initialize and pageView report: ',
          err,
        );
      }
    }
  },

  sendLoginSuccessEvent(userId) {
    try {
      ReactGA.set({ userId });
    } catch (err) {
      console.debug('Error when calling set user: ', err);
    }
    analytics.sendEvent('Login', 'Login Success', 'Onsite', {
      dimension1: userId, // For seeing User-ID (If Privacy Policy Supports)
      dimension2: 'Yes', // Cohort for Registered User
      dimension3: 'Onsite', // Dimension for Login/Signup Source
      metric4: '1', // Metric for Logins
    });
  },

  sendLoginFailureEvent() {
    analytics.sendEvent('Login', 'Login Success', 'Onsite', {
      dimension3: 'Onsite', // Dimension for Login/Signup Source
      metric5: '1', // Metric for Failed Logins
    });
  },

  sendLogoutEvent() {
    analytics.sendEvent('Logout', 'Logout Success', 'Onsite', {
      dimension3: 'Onsite', // Dimension for Login/Signup Source
      metric5: '1', // Metric for Failed Logins
    });
  },

  logPageView(pathname) {
    if (analytics.isGAEnabled()) {
      const page = pathname || location.pathname;
      try {
        ReactGA.set({ page });
        ReactGA.pageview(page);
      } catch (err) {
        console.debug(
          'Error when calling initialize and pageView report: ',
          err,
        );
      }
    }
  },
};

export default analytics;
