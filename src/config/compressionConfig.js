// noinspection JSAnnotator
const levels = [
  {
    MAX_WIDTH: 600,
    MAX_HEIGHT: 600,
    DOCUMENT_SIZE: 300000,
  },
  {
    MAX_WIDTH: 800,
    MAX_HEIGHT: 800,
    DOCUMENT_SIZE: 500000,
  },
];

export default {
  getLevel(level) {
    return levels[level - 1];
  },
};
