const isProduction = process.env.NODE_ENV === 'production';

const defaultStagingServer = 'http://192.168.111.68:3000';
// export const defaultStagingServer = 'https://carpino.webdooz.com';

const defaultClientUrl = isProduction
  ? 'https://tap33.me/api'
  : `https://tap33.me/api`;

const defaultServerUrl = isProduction
  ? 'https://ac.tap30.ir'
  : `http://localhost:${process.env.PORT || 3000}`;

/*

PORT=4000 API_SERVER_URL="http://localhost:4000" API_CLIENT_URL="http://localhost:3001/api"

*/

const api = {
  // API URL to be used in the client-side code
  clientUrl: process.env.API_CLIENT_URL || defaultClientUrl,
  // API URL to be used in the server-side code
  serverUrl: process.env.API_SERVER_URL || defaultServerUrl,
};

export { defaultStagingServer, defaultClientUrl, defaultServerUrl, api };
