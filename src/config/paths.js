/* eslint-disable prettier/prettier */
import { api } from './urls';

export const loginApiUrl = `http://localhost:8080/Turtle/login`;
export const logoutApiUrl = `http://localhost:8080/Turtle/login`;
export const getSearchHousesUrl = `http://localhost:8080/Turtle/SearchServlet`;
export const getCreateHouseUrl = `http://localhost:8080/Turtle/api/HouseServlet`;
export const getInitiateUrl = `http://localhost:8080/Turtle/home`;
export const getCurrentUserUrl = (userId) => `http://localhost:8080/Turtle/api/IndividualServlet?userId=${userId}`;
export const getHouseDetailUrl = (houseId) => `http://localhost:8080/Turtle/api/GetSingleHouse?id=${houseId}`;
export const getHousePhoneUrl = (houseId) => `http://localhost:8080/Turtle/api/PaymentServlet?id=${houseId}`;
export const increaseCreditUrl = (amount) => `http://localhost:8080/Turtle/api/CreditServlet?value=${amount}`;

export const searchDriverUrl = phoneNumber => `${api.clientUrl}/v2/acquisition/driver/phoneNumber/${phoneNumber}`;
export const getDriverAcquisitionInfoUrl = driverId => `${api.clientUrl}/v2/acquisition/driver/${driverId}`;
export const updateDriverAcquisitionInfoUrl = driverId => `${api.clientUrl}/v2/acquisition/driver/${driverId}`;
export const updateDriverDocsUrl = driverId => `${api.clientUrl}/v2/acquisition/driver/documents/${driverId}`;
export const approveDriverUrl = driverId => `${api.clientUrl}/v2/acquisition/driver/approve/${driverId}`;
export const rejectDriverUrl = driverId => `${api.clientUrl}/v2/acquisition/driver/reject/${driverId}`;
export const getDriverDocumentUrl = documentId => `${api.clientUrl}/v2/acquisition/driver/document/${documentId}`;
export const getDriverDocumentIdsUrl = driverId => `${api.clientUrl}/v2/acquisition/driver/documents/${driverId}`;
